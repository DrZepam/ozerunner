﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3395709193;
// MoreMountains.Tools.Wiggle
struct Wiggle_t1237850810;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<MoreMountains.Tools.MMEventListenerBase>>
struct Dictionary_2_t3611956414;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// MoreMountains.Tools.MMAchievementDisplayItem
struct MMAchievementDisplayItem_t1206168000;
// MoreMountains.Tools.MMAchievement
struct MMAchievement_t2522936773;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// MoreMountains.Tools.MMAchievementDisplayer
struct MMAchievementDisplayer_t237347715;
// System.Collections.Generic.List`1<MoreMountains.Tools.MMAchievement>
struct List_1_t3995011515;
// System.String
struct String_t;
// MoreMountains.Tools.TimedAutoDestroy
struct TimedAutoDestroy_t483462573;
// MoreMountains.Tools.SerializedMMAchievement[]
struct SerializedMMAchievementU5BU5D_t935736105;
// MoreMountains.InfiniteRunnerEngine.LevelManager
struct LevelManager_t1115010890;
// MoreMountains.InfiniteRunnerEngine.PlayableCharacter
struct PlayableCharacter_t2738907193;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// MoreMountains.InfiniteRunnerEngine.GameManager
struct GameManager_t51443477;
// MoreMountains.InfiniteRunnerEngine.LoadingSceneManager
struct LoadingSceneManager_t1484615109;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// MoreMountains.Tools.PathMovement
struct PathMovement_t1036546472;
// MoreMountains.InfiniteRunnerEngine.Jumper
struct Jumper_t898605922;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// MoreMountains.InfiniteRunnerEngine.LaneRunner
struct LaneRunner_t2456357099;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Action
struct Action_t1264377477;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.List`1<MoreMountains.InfiniteRunnerEngine.ScenarioEvent>
struct List_1_t1679827946;
// MoreMountains.InfiniteRunnerEngine.InputManager
struct InputManager_t3769144452;
// MoreMountains.InfiniteRunnerEngine.GUIManager
struct GUIManager_t1233035445;
// MoreMountains.Tools.ObjectPooler
struct ObjectPooler_t2790389498;
// MoreMountains.InfiniteRunnerEngine.SoundManager
struct SoundManager_t1622142048;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// MoreMountains.Tools.RigidbodyInterface
struct RigidbodyInterface_t2126323197;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// MoreMountains.Tools.JoystickEvent
struct JoystickEvent_t500143420;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// System.Collections.Generic.List`1<MoreMountains.Tools.PathMovementElement>
struct List_1_t3650174854;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4154883932;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// MoreMountains.Tools.SwipeEvent
struct SwipeEvent_t1828135924;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// MoreMountains.Tools.AxisEvent
struct AxisEvent_t2672331441;
// UnityEngine.UI.Image
struct Image_t2670269651;
// ProgressBar
struct ProgressBar_t1074804577;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// System.Collections.Generic.List`1<MoreMountains.InfiniteRunnerEngine.PlayableCharacter>
struct List_1_t4210981935;
// LinkedSpawnedObject
struct LinkedSpawnedObject_t3952174992;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EXTENSIONMETHODS_T3763603995_H
#define EXTENSIONMETHODS_T3763603995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.ExtensionMethods
struct  ExtensionMethods_t3763603995  : public RuntimeObject
{
public:

public:
};

struct ExtensionMethods_t3763603995_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Component> MoreMountains.Tools.ExtensionMethods::m_ComponentCache
	List_1_t3395709193 * ___m_ComponentCache_0;

public:
	inline static int32_t get_offset_of_m_ComponentCache_0() { return static_cast<int32_t>(offsetof(ExtensionMethods_t3763603995_StaticFields, ___m_ComponentCache_0)); }
	inline List_1_t3395709193 * get_m_ComponentCache_0() const { return ___m_ComponentCache_0; }
	inline List_1_t3395709193 ** get_address_of_m_ComponentCache_0() { return &___m_ComponentCache_0; }
	inline void set_m_ComponentCache_0(List_1_t3395709193 * value)
	{
		___m_ComponentCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentCache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONMETHODS_T3763603995_H
#ifndef U3CCOMPUTEPOSITIONU3EC__ITERATOR0_T3591811775_H
#define U3CCOMPUTEPOSITIONU3EC__ITERATOR0_T3591811775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.Wiggle/<ComputePosition>c__Iterator0
struct  U3CComputePositionU3Ec__Iterator0_t3591811775  : public RuntimeObject
{
public:
	// MoreMountains.Tools.Wiggle MoreMountains.Tools.Wiggle/<ComputePosition>c__Iterator0::$this
	Wiggle_t1237850810 * ___U24this_0;
	// System.Object MoreMountains.Tools.Wiggle/<ComputePosition>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MoreMountains.Tools.Wiggle/<ComputePosition>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MoreMountains.Tools.Wiggle/<ComputePosition>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CComputePositionU3Ec__Iterator0_t3591811775, ___U24this_0)); }
	inline Wiggle_t1237850810 * get_U24this_0() const { return ___U24this_0; }
	inline Wiggle_t1237850810 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Wiggle_t1237850810 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CComputePositionU3Ec__Iterator0_t3591811775, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CComputePositionU3Ec__Iterator0_t3591811775, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CComputePositionU3Ec__Iterator0_t3591811775, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOMPUTEPOSITIONU3EC__ITERATOR0_T3591811775_H
#ifndef MMEVENTMANAGER_T383372512_H
#define MMEVENTMANAGER_T383372512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMEventManager
struct  MMEventManager_t383372512  : public RuntimeObject
{
public:

public:
};

struct MMEventManager_t383372512_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<MoreMountains.Tools.MMEventListenerBase>> MoreMountains.Tools.MMEventManager::_subscribersList
	Dictionary_2_t3611956414 * ____subscribersList_0;

public:
	inline static int32_t get_offset_of__subscribersList_0() { return static_cast<int32_t>(offsetof(MMEventManager_t383372512_StaticFields, ____subscribersList_0)); }
	inline Dictionary_2_t3611956414 * get__subscribersList_0() const { return ____subscribersList_0; }
	inline Dictionary_2_t3611956414 ** get_address_of__subscribersList_0() { return &____subscribersList_0; }
	inline void set__subscribersList_0(Dictionary_2_t3611956414 * value)
	{
		____subscribersList_0 = value;
		Il2CppCodeGenWriteBarrier((&____subscribersList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMEVENTMANAGER_T383372512_H
#ifndef EVENTREGISTER_T3454741100_H
#define EVENTREGISTER_T3454741100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.EventRegister
struct  EventRegister_t3454741100  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTREGISTER_T3454741100_H
#ifndef U3CDISPLAYACHIEVEMENTU3EC__ITERATOR0_T2970070073_H
#define U3CDISPLAYACHIEVEMENTU3EC__ITERATOR0_T2970070073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0
struct  U3CDisplayAchievementU3Ec__Iterator0_t2970070073  : public RuntimeObject
{
public:
	// UnityEngine.GameObject MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0::<instance>__0
	GameObject_t1113636619 * ___U3CinstanceU3E__0_0;
	// MoreMountains.Tools.MMAchievementDisplayItem MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0::<achievementDisplay>__0
	MMAchievementDisplayItem_t1206168000 * ___U3CachievementDisplayU3E__0_1;
	// MoreMountains.Tools.MMAchievement MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0::achievement
	MMAchievement_t2522936773 * ___achievement_2;
	// UnityEngine.CanvasGroup MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0::<achievementCanvasGroup>__0
	CanvasGroup_t4083511760 * ___U3CachievementCanvasGroupU3E__0_3;
	// MoreMountains.Tools.MMAchievementDisplayer MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0::$this
	MMAchievementDisplayer_t237347715 * ___U24this_4;
	// System.Object MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 MoreMountains.Tools.MMAchievementDisplayer/<DisplayAchievement>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CinstanceU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayAchievementU3Ec__Iterator0_t2970070073, ___U3CinstanceU3E__0_0)); }
	inline GameObject_t1113636619 * get_U3CinstanceU3E__0_0() const { return ___U3CinstanceU3E__0_0; }
	inline GameObject_t1113636619 ** get_address_of_U3CinstanceU3E__0_0() { return &___U3CinstanceU3E__0_0; }
	inline void set_U3CinstanceU3E__0_0(GameObject_t1113636619 * value)
	{
		___U3CinstanceU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CachievementDisplayU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayAchievementU3Ec__Iterator0_t2970070073, ___U3CachievementDisplayU3E__0_1)); }
	inline MMAchievementDisplayItem_t1206168000 * get_U3CachievementDisplayU3E__0_1() const { return ___U3CachievementDisplayU3E__0_1; }
	inline MMAchievementDisplayItem_t1206168000 ** get_address_of_U3CachievementDisplayU3E__0_1() { return &___U3CachievementDisplayU3E__0_1; }
	inline void set_U3CachievementDisplayU3E__0_1(MMAchievementDisplayItem_t1206168000 * value)
	{
		___U3CachievementDisplayU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CachievementDisplayU3E__0_1), value);
	}

	inline static int32_t get_offset_of_achievement_2() { return static_cast<int32_t>(offsetof(U3CDisplayAchievementU3Ec__Iterator0_t2970070073, ___achievement_2)); }
	inline MMAchievement_t2522936773 * get_achievement_2() const { return ___achievement_2; }
	inline MMAchievement_t2522936773 ** get_address_of_achievement_2() { return &___achievement_2; }
	inline void set_achievement_2(MMAchievement_t2522936773 * value)
	{
		___achievement_2 = value;
		Il2CppCodeGenWriteBarrier((&___achievement_2), value);
	}

	inline static int32_t get_offset_of_U3CachievementCanvasGroupU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayAchievementU3Ec__Iterator0_t2970070073, ___U3CachievementCanvasGroupU3E__0_3)); }
	inline CanvasGroup_t4083511760 * get_U3CachievementCanvasGroupU3E__0_3() const { return ___U3CachievementCanvasGroupU3E__0_3; }
	inline CanvasGroup_t4083511760 ** get_address_of_U3CachievementCanvasGroupU3E__0_3() { return &___U3CachievementCanvasGroupU3E__0_3; }
	inline void set_U3CachievementCanvasGroupU3E__0_3(CanvasGroup_t4083511760 * value)
	{
		___U3CachievementCanvasGroupU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CachievementCanvasGroupU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CDisplayAchievementU3Ec__Iterator0_t2970070073, ___U24this_4)); }
	inline MMAchievementDisplayer_t237347715 * get_U24this_4() const { return ___U24this_4; }
	inline MMAchievementDisplayer_t237347715 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MMAchievementDisplayer_t237347715 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDisplayAchievementU3Ec__Iterator0_t2970070073, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDisplayAchievementU3Ec__Iterator0_t2970070073, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDisplayAchievementU3Ec__Iterator0_t2970070073, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYACHIEVEMENTU3EC__ITERATOR0_T2970070073_H
#ifndef MMACHIEVEMENTMANAGER_T3982398824_H
#define MMACHIEVEMENTMANAGER_T3982398824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAchievementManager
struct  MMAchievementManager_t3982398824  : public RuntimeObject
{
public:

public:
};

struct MMAchievementManager_t3982398824_StaticFields
{
public:
	// System.Collections.Generic.List`1<MoreMountains.Tools.MMAchievement> MoreMountains.Tools.MMAchievementManager::Achievements
	List_1_t3995011515 * ___Achievements_0;
	// MoreMountains.Tools.MMAchievement MoreMountains.Tools.MMAchievementManager::_achievement
	MMAchievement_t2522936773 * ____achievement_1;
	// System.String MoreMountains.Tools.MMAchievementManager::_savePath
	String_t* ____savePath_4;
	// System.String MoreMountains.Tools.MMAchievementManager::_saveFileName
	String_t* ____saveFileName_5;
	// System.String MoreMountains.Tools.MMAchievementManager::_listID
	String_t* ____listID_6;

public:
	inline static int32_t get_offset_of_Achievements_0() { return static_cast<int32_t>(offsetof(MMAchievementManager_t3982398824_StaticFields, ___Achievements_0)); }
	inline List_1_t3995011515 * get_Achievements_0() const { return ___Achievements_0; }
	inline List_1_t3995011515 ** get_address_of_Achievements_0() { return &___Achievements_0; }
	inline void set_Achievements_0(List_1_t3995011515 * value)
	{
		___Achievements_0 = value;
		Il2CppCodeGenWriteBarrier((&___Achievements_0), value);
	}

	inline static int32_t get_offset_of__achievement_1() { return static_cast<int32_t>(offsetof(MMAchievementManager_t3982398824_StaticFields, ____achievement_1)); }
	inline MMAchievement_t2522936773 * get__achievement_1() const { return ____achievement_1; }
	inline MMAchievement_t2522936773 ** get_address_of__achievement_1() { return &____achievement_1; }
	inline void set__achievement_1(MMAchievement_t2522936773 * value)
	{
		____achievement_1 = value;
		Il2CppCodeGenWriteBarrier((&____achievement_1), value);
	}

	inline static int32_t get_offset_of__savePath_4() { return static_cast<int32_t>(offsetof(MMAchievementManager_t3982398824_StaticFields, ____savePath_4)); }
	inline String_t* get__savePath_4() const { return ____savePath_4; }
	inline String_t** get_address_of__savePath_4() { return &____savePath_4; }
	inline void set__savePath_4(String_t* value)
	{
		____savePath_4 = value;
		Il2CppCodeGenWriteBarrier((&____savePath_4), value);
	}

	inline static int32_t get_offset_of__saveFileName_5() { return static_cast<int32_t>(offsetof(MMAchievementManager_t3982398824_StaticFields, ____saveFileName_5)); }
	inline String_t* get__saveFileName_5() const { return ____saveFileName_5; }
	inline String_t** get_address_of__saveFileName_5() { return &____saveFileName_5; }
	inline void set__saveFileName_5(String_t* value)
	{
		____saveFileName_5 = value;
		Il2CppCodeGenWriteBarrier((&____saveFileName_5), value);
	}

	inline static int32_t get_offset_of__listID_6() { return static_cast<int32_t>(offsetof(MMAchievementManager_t3982398824_StaticFields, ____listID_6)); }
	inline String_t* get__listID_6() const { return ____listID_6; }
	inline String_t** get_address_of__listID_6() { return &____listID_6; }
	inline void set__listID_6(String_t* value)
	{
		____listID_6 = value;
		Il2CppCodeGenWriteBarrier((&____listID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMACHIEVEMENTMANAGER_T3982398824_H
#ifndef SERIALIZEDMMACHIEVEMENT_T770899448_H
#define SERIALIZEDMMACHIEVEMENT_T770899448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.SerializedMMAchievement
struct  SerializedMMAchievement_t770899448  : public RuntimeObject
{
public:
	// System.String MoreMountains.Tools.SerializedMMAchievement::AchievementID
	String_t* ___AchievementID_0;
	// System.Boolean MoreMountains.Tools.SerializedMMAchievement::UnlockedStatus
	bool ___UnlockedStatus_1;
	// System.Int32 MoreMountains.Tools.SerializedMMAchievement::ProgressCurrent
	int32_t ___ProgressCurrent_2;

public:
	inline static int32_t get_offset_of_AchievementID_0() { return static_cast<int32_t>(offsetof(SerializedMMAchievement_t770899448, ___AchievementID_0)); }
	inline String_t* get_AchievementID_0() const { return ___AchievementID_0; }
	inline String_t** get_address_of_AchievementID_0() { return &___AchievementID_0; }
	inline void set_AchievementID_0(String_t* value)
	{
		___AchievementID_0 = value;
		Il2CppCodeGenWriteBarrier((&___AchievementID_0), value);
	}

	inline static int32_t get_offset_of_UnlockedStatus_1() { return static_cast<int32_t>(offsetof(SerializedMMAchievement_t770899448, ___UnlockedStatus_1)); }
	inline bool get_UnlockedStatus_1() const { return ___UnlockedStatus_1; }
	inline bool* get_address_of_UnlockedStatus_1() { return &___UnlockedStatus_1; }
	inline void set_UnlockedStatus_1(bool value)
	{
		___UnlockedStatus_1 = value;
	}

	inline static int32_t get_offset_of_ProgressCurrent_2() { return static_cast<int32_t>(offsetof(SerializedMMAchievement_t770899448, ___ProgressCurrent_2)); }
	inline int32_t get_ProgressCurrent_2() const { return ___ProgressCurrent_2; }
	inline int32_t* get_address_of_ProgressCurrent_2() { return &___ProgressCurrent_2; }
	inline void set_ProgressCurrent_2(int32_t value)
	{
		___ProgressCurrent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDMMACHIEVEMENT_T770899448_H
#ifndef U3CDESTRUCTIONU3EC__ITERATOR0_T2359794738_H
#define U3CDESTRUCTIONU3EC__ITERATOR0_T2359794738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.TimedAutoDestroy/<Destruction>c__Iterator0
struct  U3CDestructionU3Ec__Iterator0_t2359794738  : public RuntimeObject
{
public:
	// MoreMountains.Tools.TimedAutoDestroy MoreMountains.Tools.TimedAutoDestroy/<Destruction>c__Iterator0::$this
	TimedAutoDestroy_t483462573 * ___U24this_0;
	// System.Object MoreMountains.Tools.TimedAutoDestroy/<Destruction>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MoreMountains.Tools.TimedAutoDestroy/<Destruction>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MoreMountains.Tools.TimedAutoDestroy/<Destruction>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDestructionU3Ec__Iterator0_t2359794738, ___U24this_0)); }
	inline TimedAutoDestroy_t483462573 * get_U24this_0() const { return ___U24this_0; }
	inline TimedAutoDestroy_t483462573 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TimedAutoDestroy_t483462573 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDestructionU3Ec__Iterator0_t2359794738, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDestructionU3Ec__Iterator0_t2359794738, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDestructionU3Ec__Iterator0_t2359794738, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTRUCTIONU3EC__ITERATOR0_T2359794738_H
#ifndef SERIALIZEDMMACHIEVEMENTMANAGER_T2451628177_H
#define SERIALIZEDMMACHIEVEMENTMANAGER_T2451628177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.SerializedMMAchievementManager
struct  SerializedMMAchievementManager_t2451628177  : public RuntimeObject
{
public:
	// MoreMountains.Tools.SerializedMMAchievement[] MoreMountains.Tools.SerializedMMAchievementManager::Achievements
	SerializedMMAchievementU5BU5D_t935736105* ___Achievements_0;

public:
	inline static int32_t get_offset_of_Achievements_0() { return static_cast<int32_t>(offsetof(SerializedMMAchievementManager_t2451628177, ___Achievements_0)); }
	inline SerializedMMAchievementU5BU5D_t935736105* get_Achievements_0() const { return ___Achievements_0; }
	inline SerializedMMAchievementU5BU5D_t935736105** get_address_of_Achievements_0() { return &___Achievements_0; }
	inline void set_Achievements_0(SerializedMMAchievementU5BU5D_t935736105* value)
	{
		___Achievements_0 = value;
		Il2CppCodeGenWriteBarrier((&___Achievements_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDMMACHIEVEMENTMANAGER_T2451628177_H
#ifndef SINGLEHIGHSCOREMANAGER_T860903255_H
#define SINGLEHIGHSCOREMANAGER_T860903255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.SingleHighScoreManager
struct  SingleHighScoreManager_t860903255  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEHIGHSCOREMANAGER_T860903255_H
#ifndef U3CPREPARESTARTCOUNTDOWNU3EC__ITERATOR0_T3347106311_H
#define U3CPREPARESTARTCOUNTDOWNU3EC__ITERATOR0_T3347106311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LevelManager/<PrepareStartCountdown>c__Iterator0
struct  U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311  : public RuntimeObject
{
public:
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LevelManager/<PrepareStartCountdown>c__Iterator0::<countdown>__0
	int32_t ___U3CcountdownU3E__0_0;
	// MoreMountains.InfiniteRunnerEngine.LevelManager MoreMountains.InfiniteRunnerEngine.LevelManager/<PrepareStartCountdown>c__Iterator0::$this
	LevelManager_t1115010890 * ___U24this_1;
	// System.Object MoreMountains.InfiniteRunnerEngine.LevelManager/<PrepareStartCountdown>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.LevelManager/<PrepareStartCountdown>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LevelManager/<PrepareStartCountdown>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcountdownU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311, ___U3CcountdownU3E__0_0)); }
	inline int32_t get_U3CcountdownU3E__0_0() const { return ___U3CcountdownU3E__0_0; }
	inline int32_t* get_address_of_U3CcountdownU3E__0_0() { return &___U3CcountdownU3E__0_0; }
	inline void set_U3CcountdownU3E__0_0(int32_t value)
	{
		___U3CcountdownU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311, ___U24this_1)); }
	inline LevelManager_t1115010890 * get_U24this_1() const { return ___U24this_1; }
	inline LevelManager_t1115010890 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LevelManager_t1115010890 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPREPARESTARTCOUNTDOWNU3EC__ITERATOR0_T3347106311_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef U3CCOMPUTEROTATIONU3EC__ITERATOR1_T2080689703_H
#define U3CCOMPUTEROTATIONU3EC__ITERATOR1_T2080689703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.Wiggle/<ComputeRotation>c__Iterator1
struct  U3CComputeRotationU3Ec__Iterator1_t2080689703  : public RuntimeObject
{
public:
	// MoreMountains.Tools.Wiggle MoreMountains.Tools.Wiggle/<ComputeRotation>c__Iterator1::$this
	Wiggle_t1237850810 * ___U24this_0;
	// System.Object MoreMountains.Tools.Wiggle/<ComputeRotation>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MoreMountains.Tools.Wiggle/<ComputeRotation>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 MoreMountains.Tools.Wiggle/<ComputeRotation>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CComputeRotationU3Ec__Iterator1_t2080689703, ___U24this_0)); }
	inline Wiggle_t1237850810 * get_U24this_0() const { return ___U24this_0; }
	inline Wiggle_t1237850810 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Wiggle_t1237850810 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CComputeRotationU3Ec__Iterator1_t2080689703, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CComputeRotationU3Ec__Iterator1_t2080689703, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CComputeRotationU3Ec__Iterator1_t2080689703, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOMPUTEROTATIONU3EC__ITERATOR1_T2080689703_H
#ifndef U3CGOTOLEVELCOU3EC__ITERATOR1_T1210678941_H
#define U3CGOTOLEVELCOU3EC__ITERATOR1_T1210678941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LevelManager/<GotoLevelCo>c__Iterator1
struct  U3CGotoLevelCoU3Ec__Iterator1_t1210678941  : public RuntimeObject
{
public:
	// System.String MoreMountains.InfiniteRunnerEngine.LevelManager/<GotoLevelCo>c__Iterator1::levelName
	String_t* ___levelName_0;
	// MoreMountains.InfiniteRunnerEngine.LevelManager MoreMountains.InfiniteRunnerEngine.LevelManager/<GotoLevelCo>c__Iterator1::$this
	LevelManager_t1115010890 * ___U24this_1;
	// System.Object MoreMountains.InfiniteRunnerEngine.LevelManager/<GotoLevelCo>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.LevelManager/<GotoLevelCo>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LevelManager/<GotoLevelCo>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_levelName_0() { return static_cast<int32_t>(offsetof(U3CGotoLevelCoU3Ec__Iterator1_t1210678941, ___levelName_0)); }
	inline String_t* get_levelName_0() const { return ___levelName_0; }
	inline String_t** get_address_of_levelName_0() { return &___levelName_0; }
	inline void set_levelName_0(String_t* value)
	{
		___levelName_0 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGotoLevelCoU3Ec__Iterator1_t1210678941, ___U24this_1)); }
	inline LevelManager_t1115010890 * get_U24this_1() const { return ___U24this_1; }
	inline LevelManager_t1115010890 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LevelManager_t1115010890 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGotoLevelCoU3Ec__Iterator1_t1210678941, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGotoLevelCoU3Ec__Iterator1_t1210678941, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGotoLevelCoU3Ec__Iterator1_t1210678941, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGOTOLEVELCOU3EC__ITERATOR1_T1210678941_H
#ifndef U3CKILLCHARACTERCOU3EC__ITERATOR2_T374224669_H
#define U3CKILLCHARACTERCOU3EC__ITERATOR2_T374224669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LevelManager/<KillCharacterCo>c__Iterator2
struct  U3CKillCharacterCoU3Ec__Iterator2_t374224669  : public RuntimeObject
{
public:
	// MoreMountains.InfiniteRunnerEngine.PlayableCharacter MoreMountains.InfiniteRunnerEngine.LevelManager/<KillCharacterCo>c__Iterator2::player
	PlayableCharacter_t2738907193 * ___player_0;
	// MoreMountains.InfiniteRunnerEngine.LevelManager MoreMountains.InfiniteRunnerEngine.LevelManager/<KillCharacterCo>c__Iterator2::$this
	LevelManager_t1115010890 * ___U24this_1;
	// System.Object MoreMountains.InfiniteRunnerEngine.LevelManager/<KillCharacterCo>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.LevelManager/<KillCharacterCo>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LevelManager/<KillCharacterCo>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(U3CKillCharacterCoU3Ec__Iterator2_t374224669, ___player_0)); }
	inline PlayableCharacter_t2738907193 * get_player_0() const { return ___player_0; }
	inline PlayableCharacter_t2738907193 ** get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(PlayableCharacter_t2738907193 * value)
	{
		___player_0 = value;
		Il2CppCodeGenWriteBarrier((&___player_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CKillCharacterCoU3Ec__Iterator2_t374224669, ___U24this_1)); }
	inline LevelManager_t1115010890 * get_U24this_1() const { return ___U24this_1; }
	inline LevelManager_t1115010890 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LevelManager_t1115010890 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CKillCharacterCoU3Ec__Iterator2_t374224669, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CKillCharacterCoU3Ec__Iterator2_t374224669, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CKillCharacterCoU3Ec__Iterator2_t374224669, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CKILLCHARACTERCOU3EC__ITERATOR2_T374224669_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CCOMPUTESCALEU3EC__ITERATOR2_T1730042883_H
#define U3CCOMPUTESCALEU3EC__ITERATOR2_T1730042883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.Wiggle/<ComputeScale>c__Iterator2
struct  U3CComputeScaleU3Ec__Iterator2_t1730042883  : public RuntimeObject
{
public:
	// MoreMountains.Tools.Wiggle MoreMountains.Tools.Wiggle/<ComputeScale>c__Iterator2::$this
	Wiggle_t1237850810 * ___U24this_0;
	// System.Object MoreMountains.Tools.Wiggle/<ComputeScale>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MoreMountains.Tools.Wiggle/<ComputeScale>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 MoreMountains.Tools.Wiggle/<ComputeScale>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CComputeScaleU3Ec__Iterator2_t1730042883, ___U24this_0)); }
	inline Wiggle_t1237850810 * get_U24this_0() const { return ___U24this_0; }
	inline Wiggle_t1237850810 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Wiggle_t1237850810 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CComputeScaleU3Ec__Iterator2_t1730042883, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CComputeScaleU3Ec__Iterator2_t1730042883, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CComputeScaleU3Ec__Iterator2_t1730042883, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOMPUTESCALEU3EC__ITERATOR2_T1730042883_H
#ifndef U3CINCREMENTSCOREU3EC__ITERATOR0_T3972351830_H
#define U3CINCREMENTSCOREU3EC__ITERATOR0_T3972351830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.GameManager/<IncrementScore>c__Iterator0
struct  U3CIncrementScoreU3Ec__Iterator0_t3972351830  : public RuntimeObject
{
public:
	// MoreMountains.InfiniteRunnerEngine.GameManager MoreMountains.InfiniteRunnerEngine.GameManager/<IncrementScore>c__Iterator0::$this
	GameManager_t51443477 * ___U24this_0;
	// System.Object MoreMountains.InfiniteRunnerEngine.GameManager/<IncrementScore>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.GameManager/<IncrementScore>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.GameManager/<IncrementScore>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CIncrementScoreU3Ec__Iterator0_t3972351830, ___U24this_0)); }
	inline GameManager_t51443477 * get_U24this_0() const { return ___U24this_0; }
	inline GameManager_t51443477 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameManager_t51443477 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CIncrementScoreU3Ec__Iterator0_t3972351830, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CIncrementScoreU3Ec__Iterator0_t3972351830, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CIncrementScoreU3Ec__Iterator0_t3972351830, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINCREMENTSCOREU3EC__ITERATOR0_T3972351830_H
#ifndef U3CLOADASYNCHRONOUSLYU3EC__ITERATOR0_T3995192867_H
#define U3CLOADASYNCHRONOUSLYU3EC__ITERATOR0_T3995192867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LoadingSceneManager/<LoadAsynchronously>c__Iterator0
struct  U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867  : public RuntimeObject
{
public:
	// MoreMountains.InfiniteRunnerEngine.LoadingSceneManager MoreMountains.InfiniteRunnerEngine.LoadingSceneManager/<LoadAsynchronously>c__Iterator0::$this
	LoadingSceneManager_t1484615109 * ___U24this_0;
	// System.Object MoreMountains.InfiniteRunnerEngine.LoadingSceneManager/<LoadAsynchronously>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.LoadingSceneManager/<LoadAsynchronously>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LoadingSceneManager/<LoadAsynchronously>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867, ___U24this_0)); }
	inline LoadingSceneManager_t1484615109 * get_U24this_0() const { return ___U24this_0; }
	inline LoadingSceneManager_t1484615109 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(LoadingSceneManager_t1484615109 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCHRONOUSLYU3EC__ITERATOR0_T3995192867_H
#ifndef MMACHIEVEMENTUNLOCKEDEVENT_T4238862435_H
#define MMACHIEVEMENTUNLOCKEDEVENT_T4238862435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAchievementUnlockedEvent
struct  MMAchievementUnlockedEvent_t4238862435 
{
public:
	// MoreMountains.Tools.MMAchievement MoreMountains.Tools.MMAchievementUnlockedEvent::Achievement
	MMAchievement_t2522936773 * ___Achievement_0;

public:
	inline static int32_t get_offset_of_Achievement_0() { return static_cast<int32_t>(offsetof(MMAchievementUnlockedEvent_t4238862435, ___Achievement_0)); }
	inline MMAchievement_t2522936773 * get_Achievement_0() const { return ___Achievement_0; }
	inline MMAchievement_t2522936773 ** get_address_of_Achievement_0() { return &___Achievement_0; }
	inline void set_Achievement_0(MMAchievement_t2522936773 * value)
	{
		___Achievement_0 = value;
		Il2CppCodeGenWriteBarrier((&___Achievement_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MoreMountains.Tools.MMAchievementUnlockedEvent
struct MMAchievementUnlockedEvent_t4238862435_marshaled_pinvoke
{
	MMAchievement_t2522936773 * ___Achievement_0;
};
// Native definition for COM marshalling of MoreMountains.Tools.MMAchievementUnlockedEvent
struct MMAchievementUnlockedEvent_t4238862435_marshaled_com
{
	MMAchievement_t2522936773 * ___Achievement_0;
};
#endif // MMACHIEVEMENTUNLOCKEDEVENT_T4238862435_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef MMGAMEEVENT_T4039509544_H
#define MMGAMEEVENT_T4039509544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMGameEvent
struct  MMGameEvent_t4039509544 
{
public:
	// System.String MoreMountains.Tools.MMGameEvent::EventName
	String_t* ___EventName_0;

public:
	inline static int32_t get_offset_of_EventName_0() { return static_cast<int32_t>(offsetof(MMGameEvent_t4039509544, ___EventName_0)); }
	inline String_t* get_EventName_0() const { return ___EventName_0; }
	inline String_t** get_address_of_EventName_0() { return &___EventName_0; }
	inline void set_EventName_0(String_t* value)
	{
		___EventName_0 = value;
		Il2CppCodeGenWriteBarrier((&___EventName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MoreMountains.Tools.MMGameEvent
struct MMGameEvent_t4039509544_marshaled_pinvoke
{
	char* ___EventName_0;
};
// Native definition for COM marshalling of MoreMountains.Tools.MMGameEvent
struct MMGameEvent_t4039509544_marshaled_com
{
	Il2CppChar* ___EventName_0;
};
#endif // MMGAMEEVENT_T4039509544_H
#ifndef UNITYEVENT_1_T2278926278_H
#define UNITYEVENT_1_T2278926278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2278926278  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2278926278, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2278926278_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef MMSFXEVENT_T4198710224_H
#define MMSFXEVENT_T4198710224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMSfxEvent
struct  MMSfxEvent_t4198710224 
{
public:
	// UnityEngine.AudioClip MoreMountains.Tools.MMSfxEvent::ClipToPlay
	AudioClip_t3680889665 * ___ClipToPlay_0;

public:
	inline static int32_t get_offset_of_ClipToPlay_0() { return static_cast<int32_t>(offsetof(MMSfxEvent_t4198710224, ___ClipToPlay_0)); }
	inline AudioClip_t3680889665 * get_ClipToPlay_0() const { return ___ClipToPlay_0; }
	inline AudioClip_t3680889665 ** get_address_of_ClipToPlay_0() { return &___ClipToPlay_0; }
	inline void set_ClipToPlay_0(AudioClip_t3680889665 * value)
	{
		___ClipToPlay_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClipToPlay_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MoreMountains.Tools.MMSfxEvent
struct MMSfxEvent_t4198710224_marshaled_pinvoke
{
	AudioClip_t3680889665 * ___ClipToPlay_0;
};
// Native definition for COM marshalling of MoreMountains.Tools.MMSfxEvent
struct MMSfxEvent_t4198710224_marshaled_com
{
	AudioClip_t3680889665 * ___ClipToPlay_0;
};
#endif // MMSFXEVENT_T4198710224_H
#ifndef UNITYEVENT_1_T4125415485_H
#define UNITYEVENT_1_T4125415485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<MoreMountains.Tools.MMSwipeEvent>
struct  UnityEvent_1_t4125415485  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4125415485, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4125415485_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef UNITYEVENT_1_T3037889027_H
#define UNITYEVENT_1_T3037889027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t3037889027  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3037889027, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3037889027_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef MOVEMENTDIRECTION_T3398153413_H
#define MOVEMENTDIRECTION_T3398153413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PathMovement/MovementDirection
struct  MovementDirection_t3398153413 
{
public:
	// System.Int32 MoreMountains.Tools.PathMovement/MovementDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementDirection_t3398153413, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTDIRECTION_T3398153413_H
#ifndef HIDDENATTRIBUTE_T1779691839_H
#define HIDDENATTRIBUTE_T1779691839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.HiddenAttribute
struct  HiddenAttribute_t1779691839  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDDENATTRIBUTE_T1779691839_H
#ifndef U3CGETPATHENUMERATORU3EC__ITERATOR0_T4098586918_H
#define U3CGETPATHENUMERATORU3EC__ITERATOR0_T4098586918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PathMovement/<GetPathEnumerator>c__Iterator0
struct  U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918  : public RuntimeObject
{
public:
	// System.Int32 MoreMountains.Tools.PathMovement/<GetPathEnumerator>c__Iterator0::<index>__0
	int32_t ___U3CindexU3E__0_0;
	// MoreMountains.Tools.PathMovement MoreMountains.Tools.PathMovement/<GetPathEnumerator>c__Iterator0::$this
	PathMovement_t1036546472 * ___U24this_1;
	// UnityEngine.Vector3 MoreMountains.Tools.PathMovement/<GetPathEnumerator>c__Iterator0::$current
	Vector3_t3722313464  ___U24current_2;
	// System.Boolean MoreMountains.Tools.PathMovement/<GetPathEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MoreMountains.Tools.PathMovement/<GetPathEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CindexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918, ___U3CindexU3E__0_0)); }
	inline int32_t get_U3CindexU3E__0_0() const { return ___U3CindexU3E__0_0; }
	inline int32_t* get_address_of_U3CindexU3E__0_0() { return &___U3CindexU3E__0_0; }
	inline void set_U3CindexU3E__0_0(int32_t value)
	{
		___U3CindexU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918, ___U24this_1)); }
	inline PathMovement_t1036546472 * get_U24this_1() const { return ___U24this_1; }
	inline PathMovement_t1036546472 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PathMovement_t1036546472 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918, ___U24current_2)); }
	inline Vector3_t3722313464  get_U24current_2() const { return ___U24current_2; }
	inline Vector3_t3722313464 * get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Vector3_t3722313464  value)
	{
		___U24current_2 = value;
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPATHENUMERATORU3EC__ITERATOR0_T4098586918_H
#ifndef PATHMOVEMENTELEMENT_T2178100112_H
#define PATHMOVEMENTELEMENT_T2178100112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PathMovementElement
struct  PathMovementElement_t2178100112  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 MoreMountains.Tools.PathMovementElement::PathElementPosition
	Vector3_t3722313464  ___PathElementPosition_0;
	// System.Single MoreMountains.Tools.PathMovementElement::Delay
	float ___Delay_1;

public:
	inline static int32_t get_offset_of_PathElementPosition_0() { return static_cast<int32_t>(offsetof(PathMovementElement_t2178100112, ___PathElementPosition_0)); }
	inline Vector3_t3722313464  get_PathElementPosition_0() const { return ___PathElementPosition_0; }
	inline Vector3_t3722313464 * get_address_of_PathElementPosition_0() { return &___PathElementPosition_0; }
	inline void set_PathElementPosition_0(Vector3_t3722313464  value)
	{
		___PathElementPosition_0 = value;
	}

	inline static int32_t get_offset_of_Delay_1() { return static_cast<int32_t>(offsetof(PathMovementElement_t2178100112, ___Delay_1)); }
	inline float get_Delay_1() const { return ___Delay_1; }
	inline float* get_address_of_Delay_1() { return &___Delay_1; }
	inline void set_Delay_1(float value)
	{
		___Delay_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMOVEMENTELEMENT_T2178100112_H
#ifndef SWIPEEVENT_T1828135924_H
#define SWIPEEVENT_T1828135924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.SwipeEvent
struct  SwipeEvent_t1828135924  : public UnityEvent_1_t4125415485
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEEVENT_T1828135924_H
#ifndef READONLYATTRIBUTE_T657102582_H
#define READONLYATTRIBUTE_T657102582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.ReadOnlyAttribute
struct  ReadOnlyAttribute_t657102582  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T657102582_H
#ifndef MMPOSSIBLESWIPEDIRECTIONS_T661446340_H
#define MMPOSSIBLESWIPEDIRECTIONS_T661446340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMPossibleSwipeDirections
struct  MMPossibleSwipeDirections_t661446340 
{
public:
	// System.Int32 MoreMountains.Tools.MMPossibleSwipeDirections::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MMPossibleSwipeDirections_t661446340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMPOSSIBLESWIPEDIRECTIONS_T661446340_H
#ifndef CYCLEOPTIONS_T470408444_H
#define CYCLEOPTIONS_T470408444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PathMovement/CycleOptions
struct  CycleOptions_t470408444 
{
public:
	// System.Int32 MoreMountains.Tools.PathMovement/CycleOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CycleOptions_t470408444, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYCLEOPTIONS_T470408444_H
#ifndef POSSIBLEACCELERATIONTYPE_T1228029181_H
#define POSSIBLEACCELERATIONTYPE_T1228029181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PathMovement/PossibleAccelerationType
struct  PossibleAccelerationType_t1228029181 
{
public:
	// System.Int32 MoreMountains.Tools.PathMovement/PossibleAccelerationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PossibleAccelerationType_t1228029181, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSSIBLEACCELERATIONTYPE_T1228029181_H
#ifndef INFORMATIONATTRIBUTE_T4007568514_H
#define INFORMATIONATTRIBUTE_T4007568514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.InformationAttribute
struct  InformationAttribute_t4007568514  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFORMATIONATTRIBUTE_T4007568514_H
#ifndef INFORMATIONTYPE_T2282927356_H
#define INFORMATIONTYPE_T2282927356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.InformationAttribute/InformationType
struct  InformationType_t2282927356 
{
public:
	// System.Int32 MoreMountains.Tools.InformationAttribute/InformationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InformationType_t2282927356, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFORMATIONTYPE_T2282927356_H
#ifndef INSPECTORBUTTONATTRIBUTE_T3716718695_H
#define INSPECTORBUTTONATTRIBUTE_T3716718695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.InspectorButtonAttribute
struct  InspectorButtonAttribute_t3716718695  : public PropertyAttribute_t3677895545
{
public:
	// System.String MoreMountains.Tools.InspectorButtonAttribute::MethodName
	String_t* ___MethodName_0;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(InspectorButtonAttribute_t3716718695, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MethodName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSPECTORBUTTONATTRIBUTE_T3716718695_H
#ifndef U3CJUMPSLOWU3EC__ITERATOR0_T4153544754_H
#define U3CJUMPSLOWU3EC__ITERATOR0_T4153544754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.Jumper/<JumpSlow>c__Iterator0
struct  U3CJumpSlowU3Ec__Iterator0_t4153544754  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Jumper/<JumpSlow>c__Iterator0::<newGravity>__1
	Vector3_t3722313464  ___U3CnewGravityU3E__1_0;
	// MoreMountains.InfiniteRunnerEngine.Jumper MoreMountains.InfiniteRunnerEngine.Jumper/<JumpSlow>c__Iterator0::$this
	Jumper_t898605922 * ___U24this_1;
	// System.Object MoreMountains.InfiniteRunnerEngine.Jumper/<JumpSlow>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.Jumper/<JumpSlow>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.Jumper/<JumpSlow>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CnewGravityU3E__1_0() { return static_cast<int32_t>(offsetof(U3CJumpSlowU3Ec__Iterator0_t4153544754, ___U3CnewGravityU3E__1_0)); }
	inline Vector3_t3722313464  get_U3CnewGravityU3E__1_0() const { return ___U3CnewGravityU3E__1_0; }
	inline Vector3_t3722313464 * get_address_of_U3CnewGravityU3E__1_0() { return &___U3CnewGravityU3E__1_0; }
	inline void set_U3CnewGravityU3E__1_0(Vector3_t3722313464  value)
	{
		___U3CnewGravityU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CJumpSlowU3Ec__Iterator0_t4153544754, ___U24this_1)); }
	inline Jumper_t898605922 * get_U24this_1() const { return ___U24this_1; }
	inline Jumper_t898605922 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Jumper_t898605922 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CJumpSlowU3Ec__Iterator0_t4153544754, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CJumpSlowU3Ec__Iterator0_t4153544754, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CJumpSlowU3Ec__Iterator0_t4153544754, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CJUMPSLOWU3EC__ITERATOR0_T4153544754_H
#ifndef POSSIBLEMOVEMENTAXIS_T588859528_H
#define POSSIBLEMOVEMENTAXIS_T588859528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement/PossibleMovementAxis
struct  PossibleMovementAxis_t588859528 
{
public:
	// System.Int32 MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement/PossibleMovementAxis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PossibleMovementAxis_t588859528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSSIBLEMOVEMENTAXIS_T588859528_H
#ifndef WAYSTODETERMINEBOUNDS_T264359748_H
#define WAYSTODETERMINEBOUNDS_T264359748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.ObjectBounds/WaysToDetermineBounds
struct  WaysToDetermineBounds_t264359748 
{
public:
	// System.Int32 MoreMountains.Tools.ObjectBounds/WaysToDetermineBounds::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaysToDetermineBounds_t264359748, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYSTODETERMINEBOUNDS_T264359748_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SCENARIOEVENTTYPES_T2859657042_H
#define SCENARIOEVENTTYPES_T2859657042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.ScenarioEvent/ScenarioEventTypes
struct  ScenarioEventTypes_t2859657042 
{
public:
	// System.Int32 MoreMountains.InfiniteRunnerEngine.ScenarioEvent/ScenarioEventTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScenarioEventTypes_t2859657042, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENARIOEVENTTYPES_T2859657042_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef RENDERMODE_T4077056833_H
#define RENDERMODE_T4077056833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4077056833 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t4077056833, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4077056833_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef POSSIBLEDIRECTIONS_T3381919105_H
#define POSSIBLEDIRECTIONS_T3381919105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.Parallax/PossibleDirections
struct  PossibleDirections_t3381919105 
{
public:
	// System.Int32 MoreMountains.InfiniteRunnerEngine.Parallax/PossibleDirections::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PossibleDirections_t3381919105, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSSIBLEDIRECTIONS_T3381919105_H
#ifndef GAPORIGINS_T138053510_H
#define GAPORIGINS_T138053510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.DistanceSpawner/GapOrigins
struct  GapOrigins_t138053510 
{
public:
	// System.Int32 MoreMountains.InfiniteRunnerEngine.DistanceSpawner/GapOrigins::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GapOrigins_t138053510, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAPORIGINS_T138053510_H
#ifndef BUTTONSTATES_T3760686147_H
#define BUTTONSTATES_T3760686147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTouchButton/ButtonStates
struct  ButtonStates_t3760686147 
{
public:
	// System.Int32 MoreMountains.Tools.MMTouchButton/ButtonStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ButtonStates_t3760686147, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATES_T3760686147_H
#ifndef ACHIEVEMENTTYPES_T2802848312_H
#define ACHIEVEMENTTYPES_T2802848312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.AchievementTypes
struct  AchievementTypes_t2802848312 
{
public:
	// System.Int32 MoreMountains.Tools.AchievementTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AchievementTypes_t2802848312, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACHIEVEMENTTYPES_T2802848312_H
#ifndef BUTTONSTATES_T2679489820_H
#define BUTTONSTATES_T2679489820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTouchAxis/ButtonStates
struct  ButtonStates_t2679489820 
{
public:
	// System.Int32 MoreMountains.Tools.MMTouchAxis/ButtonStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ButtonStates_t2679489820, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATES_T2679489820_H
#ifndef CONTROLS_T3636651702_H
#define CONTROLS_T3636651702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LevelManager/Controls
struct  Controls_t3636651702 
{
public:
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LevelManager/Controls::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Controls_t3636651702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLS_T3636651702_H
#ifndef AXISEVENT_T2672331441_H
#define AXISEVENT_T2672331441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.AxisEvent
struct  AxisEvent_t2672331441  : public UnityEvent_1_t2278926278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISEVENT_T2672331441_H
#ifndef JOYSTICKEVENT_T500143420_H
#define JOYSTICKEVENT_T500143420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.JoystickEvent
struct  JoystickEvent_t500143420  : public UnityEvent_1_t3037889027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKEVENT_T500143420_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef U3CMOVETOU3EC__ITERATOR0_T4290519188_H
#define U3CMOVETOU3EC__ITERATOR0_T4290519188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0
struct  U3CMoveToU3Ec__Iterator0_t4290519188  : public RuntimeObject
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::<initialPosition>__0
	Vector3_t3722313464  ___U3CinitialPositionU3E__0_1;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::destination
	Vector3_t3722313464  ___destination_2;
	// System.Single MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::<sqrRemainingDistance>__0
	float ___U3CsqrRemainingDistanceU3E__0_3;
	// System.Single MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::movementDuration
	float ___movementDuration_4;
	// MoreMountains.InfiniteRunnerEngine.LaneRunner MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::$this
	LaneRunner_t2456357099 * ___U24this_5;
	// System.Object MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LaneRunner/<MoveTo>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CinitialPositionU3E__0_1() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___U3CinitialPositionU3E__0_1)); }
	inline Vector3_t3722313464  get_U3CinitialPositionU3E__0_1() const { return ___U3CinitialPositionU3E__0_1; }
	inline Vector3_t3722313464 * get_address_of_U3CinitialPositionU3E__0_1() { return &___U3CinitialPositionU3E__0_1; }
	inline void set_U3CinitialPositionU3E__0_1(Vector3_t3722313464  value)
	{
		___U3CinitialPositionU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_destination_2() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___destination_2)); }
	inline Vector3_t3722313464  get_destination_2() const { return ___destination_2; }
	inline Vector3_t3722313464 * get_address_of_destination_2() { return &___destination_2; }
	inline void set_destination_2(Vector3_t3722313464  value)
	{
		___destination_2 = value;
	}

	inline static int32_t get_offset_of_U3CsqrRemainingDistanceU3E__0_3() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___U3CsqrRemainingDistanceU3E__0_3)); }
	inline float get_U3CsqrRemainingDistanceU3E__0_3() const { return ___U3CsqrRemainingDistanceU3E__0_3; }
	inline float* get_address_of_U3CsqrRemainingDistanceU3E__0_3() { return &___U3CsqrRemainingDistanceU3E__0_3; }
	inline void set_U3CsqrRemainingDistanceU3E__0_3(float value)
	{
		___U3CsqrRemainingDistanceU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_movementDuration_4() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___movementDuration_4)); }
	inline float get_movementDuration_4() const { return ___movementDuration_4; }
	inline float* get_address_of_movementDuration_4() { return &___movementDuration_4; }
	inline void set_movementDuration_4(float value)
	{
		___movementDuration_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___U24this_5)); }
	inline LaneRunner_t2456357099 * get_U24this_5() const { return ___U24this_5; }
	inline LaneRunner_t2456357099 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(LaneRunner_t2456357099 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CMoveToU3Ec__Iterator0_t4290519188, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVETOU3EC__ITERATOR0_T4290519188_H
#ifndef INPUTFORCEDMODE_T1115639122_H
#define INPUTFORCEDMODE_T1115639122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTouchControls/InputForcedMode
struct  InputForcedMode_t1115639122 
{
public:
	// System.Int32 MoreMountains.Tools.MMTouchControls/InputForcedMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputForcedMode_t1115639122, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFORCEDMODE_T1115639122_H
#ifndef MMSWIPEEVENT_T3243755981_H
#define MMSWIPEEVENT_T3243755981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMSwipeEvent
struct  MMSwipeEvent_t3243755981 
{
public:
	// MoreMountains.Tools.MMPossibleSwipeDirections MoreMountains.Tools.MMSwipeEvent::SwipeDirection
	int32_t ___SwipeDirection_0;
	// System.Single MoreMountains.Tools.MMSwipeEvent::SwipeAngle
	float ___SwipeAngle_1;
	// System.Single MoreMountains.Tools.MMSwipeEvent::SwipeLength
	float ___SwipeLength_2;
	// UnityEngine.Vector2 MoreMountains.Tools.MMSwipeEvent::SwipeOrigin
	Vector2_t2156229523  ___SwipeOrigin_3;
	// UnityEngine.Vector2 MoreMountains.Tools.MMSwipeEvent::SwipeDestination
	Vector2_t2156229523  ___SwipeDestination_4;

public:
	inline static int32_t get_offset_of_SwipeDirection_0() { return static_cast<int32_t>(offsetof(MMSwipeEvent_t3243755981, ___SwipeDirection_0)); }
	inline int32_t get_SwipeDirection_0() const { return ___SwipeDirection_0; }
	inline int32_t* get_address_of_SwipeDirection_0() { return &___SwipeDirection_0; }
	inline void set_SwipeDirection_0(int32_t value)
	{
		___SwipeDirection_0 = value;
	}

	inline static int32_t get_offset_of_SwipeAngle_1() { return static_cast<int32_t>(offsetof(MMSwipeEvent_t3243755981, ___SwipeAngle_1)); }
	inline float get_SwipeAngle_1() const { return ___SwipeAngle_1; }
	inline float* get_address_of_SwipeAngle_1() { return &___SwipeAngle_1; }
	inline void set_SwipeAngle_1(float value)
	{
		___SwipeAngle_1 = value;
	}

	inline static int32_t get_offset_of_SwipeLength_2() { return static_cast<int32_t>(offsetof(MMSwipeEvent_t3243755981, ___SwipeLength_2)); }
	inline float get_SwipeLength_2() const { return ___SwipeLength_2; }
	inline float* get_address_of_SwipeLength_2() { return &___SwipeLength_2; }
	inline void set_SwipeLength_2(float value)
	{
		___SwipeLength_2 = value;
	}

	inline static int32_t get_offset_of_SwipeOrigin_3() { return static_cast<int32_t>(offsetof(MMSwipeEvent_t3243755981, ___SwipeOrigin_3)); }
	inline Vector2_t2156229523  get_SwipeOrigin_3() const { return ___SwipeOrigin_3; }
	inline Vector2_t2156229523 * get_address_of_SwipeOrigin_3() { return &___SwipeOrigin_3; }
	inline void set_SwipeOrigin_3(Vector2_t2156229523  value)
	{
		___SwipeOrigin_3 = value;
	}

	inline static int32_t get_offset_of_SwipeDestination_4() { return static_cast<int32_t>(offsetof(MMSwipeEvent_t3243755981, ___SwipeDestination_4)); }
	inline Vector2_t2156229523  get_SwipeDestination_4() const { return ___SwipeDestination_4; }
	inline Vector2_t2156229523 * get_address_of_SwipeDestination_4() { return &___SwipeDestination_4; }
	inline void set_SwipeDestination_4(Vector2_t2156229523  value)
	{
		___SwipeDestination_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMSWIPEEVENT_T3243755981_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MMACHIEVEMENT_T2522936773_H
#define MMACHIEVEMENT_T2522936773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAchievement
struct  MMAchievement_t2522936773  : public RuntimeObject
{
public:
	// System.String MoreMountains.Tools.MMAchievement::AchievementID
	String_t* ___AchievementID_0;
	// MoreMountains.Tools.AchievementTypes MoreMountains.Tools.MMAchievement::AchievementType
	int32_t ___AchievementType_1;
	// System.Boolean MoreMountains.Tools.MMAchievement::HiddenAchievement
	bool ___HiddenAchievement_2;
	// System.Boolean MoreMountains.Tools.MMAchievement::UnlockedStatus
	bool ___UnlockedStatus_3;
	// System.String MoreMountains.Tools.MMAchievement::Title
	String_t* ___Title_4;
	// System.String MoreMountains.Tools.MMAchievement::Description
	String_t* ___Description_5;
	// System.Int32 MoreMountains.Tools.MMAchievement::Points
	int32_t ___Points_6;
	// UnityEngine.Sprite MoreMountains.Tools.MMAchievement::LockedImage
	Sprite_t280657092 * ___LockedImage_7;
	// UnityEngine.Sprite MoreMountains.Tools.MMAchievement::UnlockedImage
	Sprite_t280657092 * ___UnlockedImage_8;
	// UnityEngine.AudioClip MoreMountains.Tools.MMAchievement::UnlockedSound
	AudioClip_t3680889665 * ___UnlockedSound_9;
	// System.Int32 MoreMountains.Tools.MMAchievement::ProgressTarget
	int32_t ___ProgressTarget_10;
	// System.Int32 MoreMountains.Tools.MMAchievement::ProgressCurrent
	int32_t ___ProgressCurrent_11;
	// MoreMountains.Tools.MMAchievementDisplayItem MoreMountains.Tools.MMAchievement::_achievementDisplayItem
	MMAchievementDisplayItem_t1206168000 * ____achievementDisplayItem_12;

public:
	inline static int32_t get_offset_of_AchievementID_0() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___AchievementID_0)); }
	inline String_t* get_AchievementID_0() const { return ___AchievementID_0; }
	inline String_t** get_address_of_AchievementID_0() { return &___AchievementID_0; }
	inline void set_AchievementID_0(String_t* value)
	{
		___AchievementID_0 = value;
		Il2CppCodeGenWriteBarrier((&___AchievementID_0), value);
	}

	inline static int32_t get_offset_of_AchievementType_1() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___AchievementType_1)); }
	inline int32_t get_AchievementType_1() const { return ___AchievementType_1; }
	inline int32_t* get_address_of_AchievementType_1() { return &___AchievementType_1; }
	inline void set_AchievementType_1(int32_t value)
	{
		___AchievementType_1 = value;
	}

	inline static int32_t get_offset_of_HiddenAchievement_2() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___HiddenAchievement_2)); }
	inline bool get_HiddenAchievement_2() const { return ___HiddenAchievement_2; }
	inline bool* get_address_of_HiddenAchievement_2() { return &___HiddenAchievement_2; }
	inline void set_HiddenAchievement_2(bool value)
	{
		___HiddenAchievement_2 = value;
	}

	inline static int32_t get_offset_of_UnlockedStatus_3() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___UnlockedStatus_3)); }
	inline bool get_UnlockedStatus_3() const { return ___UnlockedStatus_3; }
	inline bool* get_address_of_UnlockedStatus_3() { return &___UnlockedStatus_3; }
	inline void set_UnlockedStatus_3(bool value)
	{
		___UnlockedStatus_3 = value;
	}

	inline static int32_t get_offset_of_Title_4() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___Title_4)); }
	inline String_t* get_Title_4() const { return ___Title_4; }
	inline String_t** get_address_of_Title_4() { return &___Title_4; }
	inline void set_Title_4(String_t* value)
	{
		___Title_4 = value;
		Il2CppCodeGenWriteBarrier((&___Title_4), value);
	}

	inline static int32_t get_offset_of_Description_5() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___Description_5)); }
	inline String_t* get_Description_5() const { return ___Description_5; }
	inline String_t** get_address_of_Description_5() { return &___Description_5; }
	inline void set_Description_5(String_t* value)
	{
		___Description_5 = value;
		Il2CppCodeGenWriteBarrier((&___Description_5), value);
	}

	inline static int32_t get_offset_of_Points_6() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___Points_6)); }
	inline int32_t get_Points_6() const { return ___Points_6; }
	inline int32_t* get_address_of_Points_6() { return &___Points_6; }
	inline void set_Points_6(int32_t value)
	{
		___Points_6 = value;
	}

	inline static int32_t get_offset_of_LockedImage_7() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___LockedImage_7)); }
	inline Sprite_t280657092 * get_LockedImage_7() const { return ___LockedImage_7; }
	inline Sprite_t280657092 ** get_address_of_LockedImage_7() { return &___LockedImage_7; }
	inline void set_LockedImage_7(Sprite_t280657092 * value)
	{
		___LockedImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___LockedImage_7), value);
	}

	inline static int32_t get_offset_of_UnlockedImage_8() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___UnlockedImage_8)); }
	inline Sprite_t280657092 * get_UnlockedImage_8() const { return ___UnlockedImage_8; }
	inline Sprite_t280657092 ** get_address_of_UnlockedImage_8() { return &___UnlockedImage_8; }
	inline void set_UnlockedImage_8(Sprite_t280657092 * value)
	{
		___UnlockedImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___UnlockedImage_8), value);
	}

	inline static int32_t get_offset_of_UnlockedSound_9() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___UnlockedSound_9)); }
	inline AudioClip_t3680889665 * get_UnlockedSound_9() const { return ___UnlockedSound_9; }
	inline AudioClip_t3680889665 ** get_address_of_UnlockedSound_9() { return &___UnlockedSound_9; }
	inline void set_UnlockedSound_9(AudioClip_t3680889665 * value)
	{
		___UnlockedSound_9 = value;
		Il2CppCodeGenWriteBarrier((&___UnlockedSound_9), value);
	}

	inline static int32_t get_offset_of_ProgressTarget_10() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___ProgressTarget_10)); }
	inline int32_t get_ProgressTarget_10() const { return ___ProgressTarget_10; }
	inline int32_t* get_address_of_ProgressTarget_10() { return &___ProgressTarget_10; }
	inline void set_ProgressTarget_10(int32_t value)
	{
		___ProgressTarget_10 = value;
	}

	inline static int32_t get_offset_of_ProgressCurrent_11() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ___ProgressCurrent_11)); }
	inline int32_t get_ProgressCurrent_11() const { return ___ProgressCurrent_11; }
	inline int32_t* get_address_of_ProgressCurrent_11() { return &___ProgressCurrent_11; }
	inline void set_ProgressCurrent_11(int32_t value)
	{
		___ProgressCurrent_11 = value;
	}

	inline static int32_t get_offset_of__achievementDisplayItem_12() { return static_cast<int32_t>(offsetof(MMAchievement_t2522936773, ____achievementDisplayItem_12)); }
	inline MMAchievementDisplayItem_t1206168000 * get__achievementDisplayItem_12() const { return ____achievementDisplayItem_12; }
	inline MMAchievementDisplayItem_t1206168000 ** get_address_of__achievementDisplayItem_12() { return &____achievementDisplayItem_12; }
	inline void set__achievementDisplayItem_12(MMAchievementDisplayItem_t1206168000 * value)
	{
		____achievementDisplayItem_12 = value;
		Il2CppCodeGenWriteBarrier((&____achievementDisplayItem_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMACHIEVEMENT_T2522936773_H
#ifndef SCENARIOEVENT_T207753204_H
#define SCENARIOEVENT_T207753204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.ScenarioEvent
struct  ScenarioEvent_t207753204  : public RuntimeObject
{
public:
	// MoreMountains.InfiniteRunnerEngine.ScenarioEvent/ScenarioEventTypes MoreMountains.InfiniteRunnerEngine.ScenarioEvent::ScenarioEventType
	int32_t ___ScenarioEventType_0;
	// System.Single MoreMountains.InfiniteRunnerEngine.ScenarioEvent::StartTime
	float ___StartTime_1;
	// System.Single MoreMountains.InfiniteRunnerEngine.ScenarioEvent::StartScore
	float ___StartScore_2;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.ScenarioEvent::Status
	bool ___Status_3;
	// System.String MoreMountains.InfiniteRunnerEngine.ScenarioEvent::MMEventName
	String_t* ___MMEventName_4;
	// System.Action MoreMountains.InfiniteRunnerEngine.ScenarioEvent::ScenarioEventAction
	Action_t1264377477 * ___ScenarioEventAction_5;

public:
	inline static int32_t get_offset_of_ScenarioEventType_0() { return static_cast<int32_t>(offsetof(ScenarioEvent_t207753204, ___ScenarioEventType_0)); }
	inline int32_t get_ScenarioEventType_0() const { return ___ScenarioEventType_0; }
	inline int32_t* get_address_of_ScenarioEventType_0() { return &___ScenarioEventType_0; }
	inline void set_ScenarioEventType_0(int32_t value)
	{
		___ScenarioEventType_0 = value;
	}

	inline static int32_t get_offset_of_StartTime_1() { return static_cast<int32_t>(offsetof(ScenarioEvent_t207753204, ___StartTime_1)); }
	inline float get_StartTime_1() const { return ___StartTime_1; }
	inline float* get_address_of_StartTime_1() { return &___StartTime_1; }
	inline void set_StartTime_1(float value)
	{
		___StartTime_1 = value;
	}

	inline static int32_t get_offset_of_StartScore_2() { return static_cast<int32_t>(offsetof(ScenarioEvent_t207753204, ___StartScore_2)); }
	inline float get_StartScore_2() const { return ___StartScore_2; }
	inline float* get_address_of_StartScore_2() { return &___StartScore_2; }
	inline void set_StartScore_2(float value)
	{
		___StartScore_2 = value;
	}

	inline static int32_t get_offset_of_Status_3() { return static_cast<int32_t>(offsetof(ScenarioEvent_t207753204, ___Status_3)); }
	inline bool get_Status_3() const { return ___Status_3; }
	inline bool* get_address_of_Status_3() { return &___Status_3; }
	inline void set_Status_3(bool value)
	{
		___Status_3 = value;
	}

	inline static int32_t get_offset_of_MMEventName_4() { return static_cast<int32_t>(offsetof(ScenarioEvent_t207753204, ___MMEventName_4)); }
	inline String_t* get_MMEventName_4() const { return ___MMEventName_4; }
	inline String_t** get_address_of_MMEventName_4() { return &___MMEventName_4; }
	inline void set_MMEventName_4(String_t* value)
	{
		___MMEventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___MMEventName_4), value);
	}

	inline static int32_t get_offset_of_ScenarioEventAction_5() { return static_cast<int32_t>(offsetof(ScenarioEvent_t207753204, ___ScenarioEventAction_5)); }
	inline Action_t1264377477 * get_ScenarioEventAction_5() const { return ___ScenarioEventAction_5; }
	inline Action_t1264377477 ** get_address_of_ScenarioEventAction_5() { return &___ScenarioEventAction_5; }
	inline void set_ScenarioEventAction_5(Action_t1264377477 * value)
	{
		___ScenarioEventAction_5 = value;
		Il2CppCodeGenWriteBarrier((&___ScenarioEventAction_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENARIOEVENT_T207753204_H
#ifndef MMACHIEVEMENTLIST_T2729535630_H
#define MMACHIEVEMENTLIST_T2729535630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAchievementList
struct  MMAchievementList_t2729535630  : public ScriptableObject_t2528358522
{
public:
	// System.String MoreMountains.Tools.MMAchievementList::AchievementsListID
	String_t* ___AchievementsListID_2;
	// System.Collections.Generic.List`1<MoreMountains.Tools.MMAchievement> MoreMountains.Tools.MMAchievementList::Achievements
	List_1_t3995011515 * ___Achievements_3;

public:
	inline static int32_t get_offset_of_AchievementsListID_2() { return static_cast<int32_t>(offsetof(MMAchievementList_t2729535630, ___AchievementsListID_2)); }
	inline String_t* get_AchievementsListID_2() const { return ___AchievementsListID_2; }
	inline String_t** get_address_of_AchievementsListID_2() { return &___AchievementsListID_2; }
	inline void set_AchievementsListID_2(String_t* value)
	{
		___AchievementsListID_2 = value;
		Il2CppCodeGenWriteBarrier((&___AchievementsListID_2), value);
	}

	inline static int32_t get_offset_of_Achievements_3() { return static_cast<int32_t>(offsetof(MMAchievementList_t2729535630, ___Achievements_3)); }
	inline List_1_t3995011515 * get_Achievements_3() const { return ___Achievements_3; }
	inline List_1_t3995011515 ** get_address_of_Achievements_3() { return &___Achievements_3; }
	inline void set_Achievements_3(List_1_t3995011515 * value)
	{
		___Achievements_3 = value;
		Il2CppCodeGenWriteBarrier((&___Achievements_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMACHIEVEMENTLIST_T2729535630_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef GAMEMANAGERINSPECTORREDRAW_T3388984653_H
#define GAMEMANAGERINSPECTORREDRAW_T3388984653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.GameManager/GameManagerInspectorRedraw
struct  GameManagerInspectorRedraw_t3388984653  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGERINSPECTORREDRAW_T3388984653_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SCENARIOMANAGER_T2117133370_H
#define SCENARIOMANAGER_T2117133370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.ScenarioManager
struct  ScenarioManager_t2117133370  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.ScenarioManager::EvaluationFrequency
	float ___EvaluationFrequency_2;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.ScenarioManager::UseEventManager
	bool ___UseEventManager_3;
	// System.Collections.Generic.List`1<MoreMountains.InfiniteRunnerEngine.ScenarioEvent> MoreMountains.InfiniteRunnerEngine.ScenarioManager::_scenario
	List_1_t1679827946 * ____scenario_4;

public:
	inline static int32_t get_offset_of_EvaluationFrequency_2() { return static_cast<int32_t>(offsetof(ScenarioManager_t2117133370, ___EvaluationFrequency_2)); }
	inline float get_EvaluationFrequency_2() const { return ___EvaluationFrequency_2; }
	inline float* get_address_of_EvaluationFrequency_2() { return &___EvaluationFrequency_2; }
	inline void set_EvaluationFrequency_2(float value)
	{
		___EvaluationFrequency_2 = value;
	}

	inline static int32_t get_offset_of_UseEventManager_3() { return static_cast<int32_t>(offsetof(ScenarioManager_t2117133370, ___UseEventManager_3)); }
	inline bool get_UseEventManager_3() const { return ___UseEventManager_3; }
	inline bool* get_address_of_UseEventManager_3() { return &___UseEventManager_3; }
	inline void set_UseEventManager_3(bool value)
	{
		___UseEventManager_3 = value;
	}

	inline static int32_t get_offset_of__scenario_4() { return static_cast<int32_t>(offsetof(ScenarioManager_t2117133370, ____scenario_4)); }
	inline List_1_t1679827946 * get__scenario_4() const { return ____scenario_4; }
	inline List_1_t1679827946 ** get_address_of__scenario_4() { return &____scenario_4; }
	inline void set__scenario_4(List_1_t1679827946 * value)
	{
		____scenario_4 = value;
		Il2CppCodeGenWriteBarrier((&____scenario_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENARIOMANAGER_T2117133370_H
#ifndef SINGLETON_1_T2323601197_H
#define SINGLETON_1_T2323601197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.Singleton`1<MoreMountains.InfiniteRunnerEngine.InputManager>
struct  Singleton_1_t2323601197  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t2323601197_StaticFields
{
public:
	// T MoreMountains.Tools.Singleton`1::_instance
	InputManager_t3769144452 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2323601197_StaticFields, ____instance_2)); }
	inline InputManager_t3769144452 * get__instance_2() const { return ____instance_2; }
	inline InputManager_t3769144452 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(InputManager_t3769144452 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2323601197_H
#ifndef LINKEDSPAWNEDOBJECT_T3952174992_H
#define LINKEDSPAWNEDOBJECT_T3952174992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LinkedSpawnedObject
struct  LinkedSpawnedObject_t3952174992  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 LinkedSpawnedObject::In
	Vector3_t3722313464  ___In_2;
	// UnityEngine.Vector3 LinkedSpawnedObject::Out
	Vector3_t3722313464  ___Out_3;
	// System.Boolean LinkedSpawnedObject::<InOutSetup>k__BackingField
	bool ___U3CInOutSetupU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_In_2() { return static_cast<int32_t>(offsetof(LinkedSpawnedObject_t3952174992, ___In_2)); }
	inline Vector3_t3722313464  get_In_2() const { return ___In_2; }
	inline Vector3_t3722313464 * get_address_of_In_2() { return &___In_2; }
	inline void set_In_2(Vector3_t3722313464  value)
	{
		___In_2 = value;
	}

	inline static int32_t get_offset_of_Out_3() { return static_cast<int32_t>(offsetof(LinkedSpawnedObject_t3952174992, ___Out_3)); }
	inline Vector3_t3722313464  get_Out_3() const { return ___Out_3; }
	inline Vector3_t3722313464 * get_address_of_Out_3() { return &___Out_3; }
	inline void set_Out_3(Vector3_t3722313464  value)
	{
		___Out_3 = value;
	}

	inline static int32_t get_offset_of_U3CInOutSetupU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LinkedSpawnedObject_t3952174992, ___U3CInOutSetupU3Ek__BackingField_4)); }
	inline bool get_U3CInOutSetupU3Ek__BackingField_4() const { return ___U3CInOutSetupU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CInOutSetupU3Ek__BackingField_4() { return &___U3CInOutSetupU3Ek__BackingField_4; }
	inline void set_U3CInOutSetupU3Ek__BackingField_4(bool value)
	{
		___U3CInOutSetupU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDSPAWNEDOBJECT_T3952174992_H
#ifndef OUTOFBOUNDSRECYCLE_T3009250907_H
#define OUTOFBOUNDSRECYCLE_T3009250907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.OutOfBoundsRecycle
struct  OutOfBoundsRecycle_t3009250907  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.OutOfBoundsRecycle::DestroyDistanceBehindBounds
	float ___DestroyDistanceBehindBounds_2;

public:
	inline static int32_t get_offset_of_DestroyDistanceBehindBounds_2() { return static_cast<int32_t>(offsetof(OutOfBoundsRecycle_t3009250907, ___DestroyDistanceBehindBounds_2)); }
	inline float get_DestroyDistanceBehindBounds_2() const { return ___DestroyDistanceBehindBounds_2; }
	inline float* get_address_of_DestroyDistanceBehindBounds_2() { return &___DestroyDistanceBehindBounds_2; }
	inline void set_DestroyDistanceBehindBounds_2(float value)
	{
		___DestroyDistanceBehindBounds_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTOFBOUNDSRECYCLE_T3009250907_H
#ifndef SINGLETON_1_T4082459486_H
#define SINGLETON_1_T4082459486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.Singleton`1<MoreMountains.InfiniteRunnerEngine.GUIManager>
struct  Singleton_1_t4082459486  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t4082459486_StaticFields
{
public:
	// T MoreMountains.Tools.Singleton`1::_instance
	GUIManager_t1233035445 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t4082459486_StaticFields, ____instance_2)); }
	inline GUIManager_t1233035445 * get__instance_2() const { return ____instance_2; }
	inline GUIManager_t1233035445 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(GUIManager_t1233035445 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T4082459486_H
#ifndef SINGLETON_1_T3964434931_H
#define SINGLETON_1_T3964434931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.Singleton`1<MoreMountains.InfiniteRunnerEngine.LevelManager>
struct  Singleton_1_t3964434931  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_1_t3964434931_StaticFields
{
public:
	// T MoreMountains.Tools.Singleton`1::_instance
	LevelManager_t1115010890 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t3964434931_StaticFields, ____instance_2)); }
	inline LevelManager_t1115010890 * get__instance_2() const { return ____instance_2; }
	inline LevelManager_t1115010890 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(LevelManager_t1115010890 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T3964434931_H
#ifndef SPAWNER_T3426627155_H
#define SPAWNER_T3426627155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.Spawner
struct  Spawner_t3426627155  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Spawner::MinimumSize
	Vector3_t3722313464  ___MinimumSize_2;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Spawner::MaximumSize
	Vector3_t3722313464  ___MaximumSize_3;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.Spawner::PreserveRatio
	bool ___PreserveRatio_4;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Spawner::MinimumRotation
	Vector3_t3722313464  ___MinimumRotation_5;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Spawner::MaximumRotation
	Vector3_t3722313464  ___MaximumRotation_6;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.Spawner::Spawning
	bool ___Spawning_7;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.Spawner::OnlySpawnWhileGameInProgress
	bool ___OnlySpawnWhileGameInProgress_8;
	// System.Single MoreMountains.InfiniteRunnerEngine.Spawner::InitialDelay
	float ___InitialDelay_9;
	// MoreMountains.Tools.ObjectPooler MoreMountains.InfiniteRunnerEngine.Spawner::_objectPooler
	ObjectPooler_t2790389498 * ____objectPooler_10;
	// System.Single MoreMountains.InfiniteRunnerEngine.Spawner::_startTime
	float ____startTime_11;

public:
	inline static int32_t get_offset_of_MinimumSize_2() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ___MinimumSize_2)); }
	inline Vector3_t3722313464  get_MinimumSize_2() const { return ___MinimumSize_2; }
	inline Vector3_t3722313464 * get_address_of_MinimumSize_2() { return &___MinimumSize_2; }
	inline void set_MinimumSize_2(Vector3_t3722313464  value)
	{
		___MinimumSize_2 = value;
	}

	inline static int32_t get_offset_of_MaximumSize_3() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ___MaximumSize_3)); }
	inline Vector3_t3722313464  get_MaximumSize_3() const { return ___MaximumSize_3; }
	inline Vector3_t3722313464 * get_address_of_MaximumSize_3() { return &___MaximumSize_3; }
	inline void set_MaximumSize_3(Vector3_t3722313464  value)
	{
		___MaximumSize_3 = value;
	}

	inline static int32_t get_offset_of_PreserveRatio_4() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ___PreserveRatio_4)); }
	inline bool get_PreserveRatio_4() const { return ___PreserveRatio_4; }
	inline bool* get_address_of_PreserveRatio_4() { return &___PreserveRatio_4; }
	inline void set_PreserveRatio_4(bool value)
	{
		___PreserveRatio_4 = value;
	}

	inline static int32_t get_offset_of_MinimumRotation_5() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ___MinimumRotation_5)); }
	inline Vector3_t3722313464  get_MinimumRotation_5() const { return ___MinimumRotation_5; }
	inline Vector3_t3722313464 * get_address_of_MinimumRotation_5() { return &___MinimumRotation_5; }
	inline void set_MinimumRotation_5(Vector3_t3722313464  value)
	{
		___MinimumRotation_5 = value;
	}

	inline static int32_t get_offset_of_MaximumRotation_6() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ___MaximumRotation_6)); }
	inline Vector3_t3722313464  get_MaximumRotation_6() const { return ___MaximumRotation_6; }
	inline Vector3_t3722313464 * get_address_of_MaximumRotation_6() { return &___MaximumRotation_6; }
	inline void set_MaximumRotation_6(Vector3_t3722313464  value)
	{
		___MaximumRotation_6 = value;
	}

	inline static int32_t get_offset_of_Spawning_7() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ___Spawning_7)); }
	inline bool get_Spawning_7() const { return ___Spawning_7; }
	inline bool* get_address_of_Spawning_7() { return &___Spawning_7; }
	inline void set_Spawning_7(bool value)
	{
		___Spawning_7 = value;
	}

	inline static int32_t get_offset_of_OnlySpawnWhileGameInProgress_8() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ___OnlySpawnWhileGameInProgress_8)); }
	inline bool get_OnlySpawnWhileGameInProgress_8() const { return ___OnlySpawnWhileGameInProgress_8; }
	inline bool* get_address_of_OnlySpawnWhileGameInProgress_8() { return &___OnlySpawnWhileGameInProgress_8; }
	inline void set_OnlySpawnWhileGameInProgress_8(bool value)
	{
		___OnlySpawnWhileGameInProgress_8 = value;
	}

	inline static int32_t get_offset_of_InitialDelay_9() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ___InitialDelay_9)); }
	inline float get_InitialDelay_9() const { return ___InitialDelay_9; }
	inline float* get_address_of_InitialDelay_9() { return &___InitialDelay_9; }
	inline void set_InitialDelay_9(float value)
	{
		___InitialDelay_9 = value;
	}

	inline static int32_t get_offset_of__objectPooler_10() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ____objectPooler_10)); }
	inline ObjectPooler_t2790389498 * get__objectPooler_10() const { return ____objectPooler_10; }
	inline ObjectPooler_t2790389498 ** get_address_of__objectPooler_10() { return &____objectPooler_10; }
	inline void set__objectPooler_10(ObjectPooler_t2790389498 * value)
	{
		____objectPooler_10 = value;
		Il2CppCodeGenWriteBarrier((&____objectPooler_10), value);
	}

	inline static int32_t get_offset_of__startTime_11() { return static_cast<int32_t>(offsetof(Spawner_t3426627155, ____startTime_11)); }
	inline float get__startTime_11() const { return ____startTime_11; }
	inline float* get_address_of__startTime_11() { return &____startTime_11; }
	inline void set__startTime_11(float value)
	{
		____startTime_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNER_T3426627155_H
#ifndef PERSISTENTSINGLETON_1_T2668794452_H
#define PERSISTENTSINGLETON_1_T2668794452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PersistentSingleton`1<MoreMountains.InfiniteRunnerEngine.SoundManager>
struct  PersistentSingleton_1_t2668794452  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MoreMountains.Tools.PersistentSingleton`1::_enabled
	bool ____enabled_3;

public:
	inline static int32_t get_offset_of__enabled_3() { return static_cast<int32_t>(offsetof(PersistentSingleton_1_t2668794452, ____enabled_3)); }
	inline bool get__enabled_3() const { return ____enabled_3; }
	inline bool* get_address_of__enabled_3() { return &____enabled_3; }
	inline void set__enabled_3(bool value)
	{
		____enabled_3 = value;
	}
};

struct PersistentSingleton_1_t2668794452_StaticFields
{
public:
	// T MoreMountains.Tools.PersistentSingleton`1::_instance
	SoundManager_t1622142048 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(PersistentSingleton_1_t2668794452_StaticFields, ____instance_2)); }
	inline SoundManager_t1622142048 * get__instance_2() const { return ____instance_2; }
	inline SoundManager_t1622142048 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(SoundManager_t1622142048 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTSINGLETON_1_T2668794452_H
#ifndef MOVINGOBJECT_T2316572705_H
#define MOVINGOBJECT_T2316572705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.MovingObject
struct  MovingObject_t2316572705  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.MovingObject::Speed
	float ___Speed_2;
	// System.Single MoreMountains.InfiniteRunnerEngine.MovingObject::Acceleration
	float ___Acceleration_3;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.MovingObject::Direction
	Vector3_t3722313464  ___Direction_4;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.MovingObject::DirectionCanBeChangedBySpawner
	bool ___DirectionCanBeChangedBySpawner_5;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.MovingObject::_movement
	Vector3_t3722313464  ____movement_6;
	// System.Single MoreMountains.InfiniteRunnerEngine.MovingObject::_initialSpeed
	float ____initialSpeed_7;

public:
	inline static int32_t get_offset_of_Speed_2() { return static_cast<int32_t>(offsetof(MovingObject_t2316572705, ___Speed_2)); }
	inline float get_Speed_2() const { return ___Speed_2; }
	inline float* get_address_of_Speed_2() { return &___Speed_2; }
	inline void set_Speed_2(float value)
	{
		___Speed_2 = value;
	}

	inline static int32_t get_offset_of_Acceleration_3() { return static_cast<int32_t>(offsetof(MovingObject_t2316572705, ___Acceleration_3)); }
	inline float get_Acceleration_3() const { return ___Acceleration_3; }
	inline float* get_address_of_Acceleration_3() { return &___Acceleration_3; }
	inline void set_Acceleration_3(float value)
	{
		___Acceleration_3 = value;
	}

	inline static int32_t get_offset_of_Direction_4() { return static_cast<int32_t>(offsetof(MovingObject_t2316572705, ___Direction_4)); }
	inline Vector3_t3722313464  get_Direction_4() const { return ___Direction_4; }
	inline Vector3_t3722313464 * get_address_of_Direction_4() { return &___Direction_4; }
	inline void set_Direction_4(Vector3_t3722313464  value)
	{
		___Direction_4 = value;
	}

	inline static int32_t get_offset_of_DirectionCanBeChangedBySpawner_5() { return static_cast<int32_t>(offsetof(MovingObject_t2316572705, ___DirectionCanBeChangedBySpawner_5)); }
	inline bool get_DirectionCanBeChangedBySpawner_5() const { return ___DirectionCanBeChangedBySpawner_5; }
	inline bool* get_address_of_DirectionCanBeChangedBySpawner_5() { return &___DirectionCanBeChangedBySpawner_5; }
	inline void set_DirectionCanBeChangedBySpawner_5(bool value)
	{
		___DirectionCanBeChangedBySpawner_5 = value;
	}

	inline static int32_t get_offset_of__movement_6() { return static_cast<int32_t>(offsetof(MovingObject_t2316572705, ____movement_6)); }
	inline Vector3_t3722313464  get__movement_6() const { return ____movement_6; }
	inline Vector3_t3722313464 * get_address_of__movement_6() { return &____movement_6; }
	inline void set__movement_6(Vector3_t3722313464  value)
	{
		____movement_6 = value;
	}

	inline static int32_t get_offset_of__initialSpeed_7() { return static_cast<int32_t>(offsetof(MovingObject_t2316572705, ____initialSpeed_7)); }
	inline float get__initialSpeed_7() const { return ____initialSpeed_7; }
	inline float* get_address_of__initialSpeed_7() { return &____initialSpeed_7; }
	inline void set__initialSpeed_7(float value)
	{
		____initialSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVINGOBJECT_T2316572705_H
#ifndef REACTIVATEONSPAWN_T2603330824_H
#define REACTIVATEONSPAWN_T2603330824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.ReactivateOnSpawn
struct  ReactivateOnSpawn_t2603330824  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MoreMountains.InfiniteRunnerEngine.ReactivateOnSpawn::ShouldReactivate
	bool ___ShouldReactivate_2;

public:
	inline static int32_t get_offset_of_ShouldReactivate_2() { return static_cast<int32_t>(offsetof(ReactivateOnSpawn_t2603330824, ___ShouldReactivate_2)); }
	inline bool get_ShouldReactivate_2() const { return ___ShouldReactivate_2; }
	inline bool* get_address_of_ShouldReactivate_2() { return &___ShouldReactivate_2; }
	inline void set_ShouldReactivate_2(bool value)
	{
		___ShouldReactivate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVATEONSPAWN_T2603330824_H
#ifndef CAMERAASPECTRATIO_T3762038886_H
#define CAMERAASPECTRATIO_T3762038886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.CameraAspectRatio
struct  CameraAspectRatio_t3762038886  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 MoreMountains.Tools.CameraAspectRatio::AspectRatio
	Vector2_t2156229523  ___AspectRatio_2;
	// UnityEngine.Camera MoreMountains.Tools.CameraAspectRatio::_camera
	Camera_t4157153871 * ____camera_3;

public:
	inline static int32_t get_offset_of_AspectRatio_2() { return static_cast<int32_t>(offsetof(CameraAspectRatio_t3762038886, ___AspectRatio_2)); }
	inline Vector2_t2156229523  get_AspectRatio_2() const { return ___AspectRatio_2; }
	inline Vector2_t2156229523 * get_address_of_AspectRatio_2() { return &___AspectRatio_2; }
	inline void set_AspectRatio_2(Vector2_t2156229523  value)
	{
		___AspectRatio_2 = value;
	}

	inline static int32_t get_offset_of__camera_3() { return static_cast<int32_t>(offsetof(CameraAspectRatio_t3762038886, ____camera_3)); }
	inline Camera_t4157153871 * get__camera_3() const { return ____camera_3; }
	inline Camera_t4157153871 ** get_address_of__camera_3() { return &____camera_3; }
	inline void set__camera_3(Camera_t4157153871 * value)
	{
		____camera_3 = value;
		Il2CppCodeGenWriteBarrier((&____camera_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAASPECTRATIO_T3762038886_H
#ifndef LOADINGSCENEMANAGER_T1484615109_H
#define LOADINGSCENEMANAGER_T1484615109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LoadingSceneManager
struct  LoadingSceneManager_t1484615109  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::LoadingText
	Text_t1901882714 * ___LoadingText_3;
	// UnityEngine.CanvasGroup MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::LoadingProgressBar
	CanvasGroup_t4083511760 * ___LoadingProgressBar_4;
	// UnityEngine.CanvasGroup MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::LoadingAnimation
	CanvasGroup_t4083511760 * ___LoadingAnimation_5;
	// UnityEngine.CanvasGroup MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::LoadingCompleteAnimation
	CanvasGroup_t4083511760 * ___LoadingCompleteAnimation_6;
	// System.Single MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::StartFadeDuration
	float ___StartFadeDuration_7;
	// System.Single MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::ProgressBarSpeed
	float ___ProgressBarSpeed_8;
	// System.Single MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::ExitFadeDuration
	float ___ExitFadeDuration_9;
	// System.Single MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::LoadCompleteDelay
	float ___LoadCompleteDelay_10;
	// UnityEngine.AsyncOperation MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::_asyncOperation
	AsyncOperation_t1445031843 * ____asyncOperation_11;
	// System.Single MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::_fadeDuration
	float ____fadeDuration_13;
	// System.Single MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::_fillTarget
	float ____fillTarget_14;
	// System.String MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::_loadingTextValue
	String_t* ____loadingTextValue_15;

public:
	inline static int32_t get_offset_of_LoadingText_3() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ___LoadingText_3)); }
	inline Text_t1901882714 * get_LoadingText_3() const { return ___LoadingText_3; }
	inline Text_t1901882714 ** get_address_of_LoadingText_3() { return &___LoadingText_3; }
	inline void set_LoadingText_3(Text_t1901882714 * value)
	{
		___LoadingText_3 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingText_3), value);
	}

	inline static int32_t get_offset_of_LoadingProgressBar_4() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ___LoadingProgressBar_4)); }
	inline CanvasGroup_t4083511760 * get_LoadingProgressBar_4() const { return ___LoadingProgressBar_4; }
	inline CanvasGroup_t4083511760 ** get_address_of_LoadingProgressBar_4() { return &___LoadingProgressBar_4; }
	inline void set_LoadingProgressBar_4(CanvasGroup_t4083511760 * value)
	{
		___LoadingProgressBar_4 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingProgressBar_4), value);
	}

	inline static int32_t get_offset_of_LoadingAnimation_5() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ___LoadingAnimation_5)); }
	inline CanvasGroup_t4083511760 * get_LoadingAnimation_5() const { return ___LoadingAnimation_5; }
	inline CanvasGroup_t4083511760 ** get_address_of_LoadingAnimation_5() { return &___LoadingAnimation_5; }
	inline void set_LoadingAnimation_5(CanvasGroup_t4083511760 * value)
	{
		___LoadingAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingAnimation_5), value);
	}

	inline static int32_t get_offset_of_LoadingCompleteAnimation_6() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ___LoadingCompleteAnimation_6)); }
	inline CanvasGroup_t4083511760 * get_LoadingCompleteAnimation_6() const { return ___LoadingCompleteAnimation_6; }
	inline CanvasGroup_t4083511760 ** get_address_of_LoadingCompleteAnimation_6() { return &___LoadingCompleteAnimation_6; }
	inline void set_LoadingCompleteAnimation_6(CanvasGroup_t4083511760 * value)
	{
		___LoadingCompleteAnimation_6 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingCompleteAnimation_6), value);
	}

	inline static int32_t get_offset_of_StartFadeDuration_7() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ___StartFadeDuration_7)); }
	inline float get_StartFadeDuration_7() const { return ___StartFadeDuration_7; }
	inline float* get_address_of_StartFadeDuration_7() { return &___StartFadeDuration_7; }
	inline void set_StartFadeDuration_7(float value)
	{
		___StartFadeDuration_7 = value;
	}

	inline static int32_t get_offset_of_ProgressBarSpeed_8() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ___ProgressBarSpeed_8)); }
	inline float get_ProgressBarSpeed_8() const { return ___ProgressBarSpeed_8; }
	inline float* get_address_of_ProgressBarSpeed_8() { return &___ProgressBarSpeed_8; }
	inline void set_ProgressBarSpeed_8(float value)
	{
		___ProgressBarSpeed_8 = value;
	}

	inline static int32_t get_offset_of_ExitFadeDuration_9() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ___ExitFadeDuration_9)); }
	inline float get_ExitFadeDuration_9() const { return ___ExitFadeDuration_9; }
	inline float* get_address_of_ExitFadeDuration_9() { return &___ExitFadeDuration_9; }
	inline void set_ExitFadeDuration_9(float value)
	{
		___ExitFadeDuration_9 = value;
	}

	inline static int32_t get_offset_of_LoadCompleteDelay_10() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ___LoadCompleteDelay_10)); }
	inline float get_LoadCompleteDelay_10() const { return ___LoadCompleteDelay_10; }
	inline float* get_address_of_LoadCompleteDelay_10() { return &___LoadCompleteDelay_10; }
	inline void set_LoadCompleteDelay_10(float value)
	{
		___LoadCompleteDelay_10 = value;
	}

	inline static int32_t get_offset_of__asyncOperation_11() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ____asyncOperation_11)); }
	inline AsyncOperation_t1445031843 * get__asyncOperation_11() const { return ____asyncOperation_11; }
	inline AsyncOperation_t1445031843 ** get_address_of__asyncOperation_11() { return &____asyncOperation_11; }
	inline void set__asyncOperation_11(AsyncOperation_t1445031843 * value)
	{
		____asyncOperation_11 = value;
		Il2CppCodeGenWriteBarrier((&____asyncOperation_11), value);
	}

	inline static int32_t get_offset_of__fadeDuration_13() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ____fadeDuration_13)); }
	inline float get__fadeDuration_13() const { return ____fadeDuration_13; }
	inline float* get_address_of__fadeDuration_13() { return &____fadeDuration_13; }
	inline void set__fadeDuration_13(float value)
	{
		____fadeDuration_13 = value;
	}

	inline static int32_t get_offset_of__fillTarget_14() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ____fillTarget_14)); }
	inline float get__fillTarget_14() const { return ____fillTarget_14; }
	inline float* get_address_of__fillTarget_14() { return &____fillTarget_14; }
	inline void set__fillTarget_14(float value)
	{
		____fillTarget_14 = value;
	}

	inline static int32_t get_offset_of__loadingTextValue_15() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109, ____loadingTextValue_15)); }
	inline String_t* get__loadingTextValue_15() const { return ____loadingTextValue_15; }
	inline String_t** get_address_of__loadingTextValue_15() { return &____loadingTextValue_15; }
	inline void set__loadingTextValue_15(String_t* value)
	{
		____loadingTextValue_15 = value;
		Il2CppCodeGenWriteBarrier((&____loadingTextValue_15), value);
	}
};

struct LoadingSceneManager_t1484615109_StaticFields
{
public:
	// System.String MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::LoadingScreenSceneName
	String_t* ___LoadingScreenSceneName_2;
	// System.String MoreMountains.InfiniteRunnerEngine.LoadingSceneManager::_sceneToLoad
	String_t* ____sceneToLoad_12;

public:
	inline static int32_t get_offset_of_LoadingScreenSceneName_2() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109_StaticFields, ___LoadingScreenSceneName_2)); }
	inline String_t* get_LoadingScreenSceneName_2() const { return ___LoadingScreenSceneName_2; }
	inline String_t** get_address_of_LoadingScreenSceneName_2() { return &___LoadingScreenSceneName_2; }
	inline void set_LoadingScreenSceneName_2(String_t* value)
	{
		___LoadingScreenSceneName_2 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingScreenSceneName_2), value);
	}

	inline static int32_t get_offset_of__sceneToLoad_12() { return static_cast<int32_t>(offsetof(LoadingSceneManager_t1484615109_StaticFields, ____sceneToLoad_12)); }
	inline String_t* get__sceneToLoad_12() const { return ____sceneToLoad_12; }
	inline String_t** get_address_of__sceneToLoad_12() { return &____sceneToLoad_12; }
	inline void set__sceneToLoad_12(String_t* value)
	{
		____sceneToLoad_12 = value;
		Il2CppCodeGenWriteBarrier((&____sceneToLoad_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGSCENEMANAGER_T1484615109_H
#ifndef REPOSITIONONSTART2D_T4190716172_H
#define REPOSITIONONSTART2D_T4190716172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.RepositionOnStart2D
struct  RepositionOnStart2D_t4190716172  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.RepositionOnStart2D::PositionOffset
	Vector3_t3722313464  ___PositionOffset_2;
	// System.Single MoreMountains.InfiniteRunnerEngine.RepositionOnStart2D::RaycastLength
	float ___RaycastLength_3;

public:
	inline static int32_t get_offset_of_PositionOffset_2() { return static_cast<int32_t>(offsetof(RepositionOnStart2D_t4190716172, ___PositionOffset_2)); }
	inline Vector3_t3722313464  get_PositionOffset_2() const { return ___PositionOffset_2; }
	inline Vector3_t3722313464 * get_address_of_PositionOffset_2() { return &___PositionOffset_2; }
	inline void set_PositionOffset_2(Vector3_t3722313464  value)
	{
		___PositionOffset_2 = value;
	}

	inline static int32_t get_offset_of_RaycastLength_3() { return static_cast<int32_t>(offsetof(RepositionOnStart2D_t4190716172, ___RaycastLength_3)); }
	inline float get_RaycastLength_3() const { return ___RaycastLength_3; }
	inline float* get_address_of_RaycastLength_3() { return &___RaycastLength_3; }
	inline void set_RaycastLength_3(float value)
	{
		___RaycastLength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPOSITIONONSTART2D_T4190716172_H
#ifndef PLAYABLECHARACTER_T2738907193_H
#define PLAYABLECHARACTER_T2738907193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.PlayableCharacter
struct  PlayableCharacter_t2738907193  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MoreMountains.InfiniteRunnerEngine.PlayableCharacter::UseDefaultMecanim
	bool ___UseDefaultMecanim_2;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.PlayableCharacter::ShouldResetPosition
	bool ___ShouldResetPosition_3;
	// System.Single MoreMountains.InfiniteRunnerEngine.PlayableCharacter::ResetPositionSpeed
	float ___ResetPositionSpeed_4;
	// System.Single MoreMountains.InfiniteRunnerEngine.PlayableCharacter::<DistanceToTheGround>k__BackingField
	float ___U3CDistanceToTheGroundU3Ek__BackingField_5;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_initialPosition
	Vector3_t3722313464  ____initialPosition_6;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_grounded
	bool ____grounded_7;
	// MoreMountains.Tools.RigidbodyInterface MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_rigidbodyInterface
	RigidbodyInterface_t2126323197 * ____rigidbodyInterface_8;
	// UnityEngine.Animator MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_animator
	Animator_t434523843 * ____animator_9;
	// System.Single MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_distanceToTheGroundRaycastLength
	float ____distanceToTheGroundRaycastLength_10;
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_ground
	GameObject_t1113636619 * ____ground_11;
	// System.Single MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_groundDistanceTolerance
	float ____groundDistanceTolerance_12;
	// UnityEngine.LayerMask MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_collisionMaskSave
	LayerMask_t3493934918  ____collisionMaskSave_13;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_raycastLeftOrigin
	Vector3_t3722313464  ____raycastLeftOrigin_14;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.PlayableCharacter::_raycastRightOrigin
	Vector3_t3722313464  ____raycastRightOrigin_15;

public:
	inline static int32_t get_offset_of_UseDefaultMecanim_2() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ___UseDefaultMecanim_2)); }
	inline bool get_UseDefaultMecanim_2() const { return ___UseDefaultMecanim_2; }
	inline bool* get_address_of_UseDefaultMecanim_2() { return &___UseDefaultMecanim_2; }
	inline void set_UseDefaultMecanim_2(bool value)
	{
		___UseDefaultMecanim_2 = value;
	}

	inline static int32_t get_offset_of_ShouldResetPosition_3() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ___ShouldResetPosition_3)); }
	inline bool get_ShouldResetPosition_3() const { return ___ShouldResetPosition_3; }
	inline bool* get_address_of_ShouldResetPosition_3() { return &___ShouldResetPosition_3; }
	inline void set_ShouldResetPosition_3(bool value)
	{
		___ShouldResetPosition_3 = value;
	}

	inline static int32_t get_offset_of_ResetPositionSpeed_4() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ___ResetPositionSpeed_4)); }
	inline float get_ResetPositionSpeed_4() const { return ___ResetPositionSpeed_4; }
	inline float* get_address_of_ResetPositionSpeed_4() { return &___ResetPositionSpeed_4; }
	inline void set_ResetPositionSpeed_4(float value)
	{
		___ResetPositionSpeed_4 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceToTheGroundU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ___U3CDistanceToTheGroundU3Ek__BackingField_5)); }
	inline float get_U3CDistanceToTheGroundU3Ek__BackingField_5() const { return ___U3CDistanceToTheGroundU3Ek__BackingField_5; }
	inline float* get_address_of_U3CDistanceToTheGroundU3Ek__BackingField_5() { return &___U3CDistanceToTheGroundU3Ek__BackingField_5; }
	inline void set_U3CDistanceToTheGroundU3Ek__BackingField_5(float value)
	{
		___U3CDistanceToTheGroundU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of__initialPosition_6() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____initialPosition_6)); }
	inline Vector3_t3722313464  get__initialPosition_6() const { return ____initialPosition_6; }
	inline Vector3_t3722313464 * get_address_of__initialPosition_6() { return &____initialPosition_6; }
	inline void set__initialPosition_6(Vector3_t3722313464  value)
	{
		____initialPosition_6 = value;
	}

	inline static int32_t get_offset_of__grounded_7() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____grounded_7)); }
	inline bool get__grounded_7() const { return ____grounded_7; }
	inline bool* get_address_of__grounded_7() { return &____grounded_7; }
	inline void set__grounded_7(bool value)
	{
		____grounded_7 = value;
	}

	inline static int32_t get_offset_of__rigidbodyInterface_8() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____rigidbodyInterface_8)); }
	inline RigidbodyInterface_t2126323197 * get__rigidbodyInterface_8() const { return ____rigidbodyInterface_8; }
	inline RigidbodyInterface_t2126323197 ** get_address_of__rigidbodyInterface_8() { return &____rigidbodyInterface_8; }
	inline void set__rigidbodyInterface_8(RigidbodyInterface_t2126323197 * value)
	{
		____rigidbodyInterface_8 = value;
		Il2CppCodeGenWriteBarrier((&____rigidbodyInterface_8), value);
	}

	inline static int32_t get_offset_of__animator_9() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____animator_9)); }
	inline Animator_t434523843 * get__animator_9() const { return ____animator_9; }
	inline Animator_t434523843 ** get_address_of__animator_9() { return &____animator_9; }
	inline void set__animator_9(Animator_t434523843 * value)
	{
		____animator_9 = value;
		Il2CppCodeGenWriteBarrier((&____animator_9), value);
	}

	inline static int32_t get_offset_of__distanceToTheGroundRaycastLength_10() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____distanceToTheGroundRaycastLength_10)); }
	inline float get__distanceToTheGroundRaycastLength_10() const { return ____distanceToTheGroundRaycastLength_10; }
	inline float* get_address_of__distanceToTheGroundRaycastLength_10() { return &____distanceToTheGroundRaycastLength_10; }
	inline void set__distanceToTheGroundRaycastLength_10(float value)
	{
		____distanceToTheGroundRaycastLength_10 = value;
	}

	inline static int32_t get_offset_of__ground_11() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____ground_11)); }
	inline GameObject_t1113636619 * get__ground_11() const { return ____ground_11; }
	inline GameObject_t1113636619 ** get_address_of__ground_11() { return &____ground_11; }
	inline void set__ground_11(GameObject_t1113636619 * value)
	{
		____ground_11 = value;
		Il2CppCodeGenWriteBarrier((&____ground_11), value);
	}

	inline static int32_t get_offset_of__groundDistanceTolerance_12() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____groundDistanceTolerance_12)); }
	inline float get__groundDistanceTolerance_12() const { return ____groundDistanceTolerance_12; }
	inline float* get_address_of__groundDistanceTolerance_12() { return &____groundDistanceTolerance_12; }
	inline void set__groundDistanceTolerance_12(float value)
	{
		____groundDistanceTolerance_12 = value;
	}

	inline static int32_t get_offset_of__collisionMaskSave_13() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____collisionMaskSave_13)); }
	inline LayerMask_t3493934918  get__collisionMaskSave_13() const { return ____collisionMaskSave_13; }
	inline LayerMask_t3493934918 * get_address_of__collisionMaskSave_13() { return &____collisionMaskSave_13; }
	inline void set__collisionMaskSave_13(LayerMask_t3493934918  value)
	{
		____collisionMaskSave_13 = value;
	}

	inline static int32_t get_offset_of__raycastLeftOrigin_14() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____raycastLeftOrigin_14)); }
	inline Vector3_t3722313464  get__raycastLeftOrigin_14() const { return ____raycastLeftOrigin_14; }
	inline Vector3_t3722313464 * get_address_of__raycastLeftOrigin_14() { return &____raycastLeftOrigin_14; }
	inline void set__raycastLeftOrigin_14(Vector3_t3722313464  value)
	{
		____raycastLeftOrigin_14 = value;
	}

	inline static int32_t get_offset_of__raycastRightOrigin_15() { return static_cast<int32_t>(offsetof(PlayableCharacter_t2738907193, ____raycastRightOrigin_15)); }
	inline Vector3_t3722313464  get__raycastRightOrigin_15() const { return ____raycastRightOrigin_15; }
	inline Vector3_t3722313464 * get_address_of__raycastRightOrigin_15() { return &____raycastRightOrigin_15; }
	inline void set__raycastRightOrigin_15(Vector3_t3722313464  value)
	{
		____raycastRightOrigin_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLECHARACTER_T2738907193_H
#ifndef OBJECTBOUNDS_T107927819_H
#define OBJECTBOUNDS_T107927819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.ObjectBounds
struct  ObjectBounds_t107927819  : public MonoBehaviour_t3962482529
{
public:
	// MoreMountains.Tools.ObjectBounds/WaysToDetermineBounds MoreMountains.Tools.ObjectBounds::BoundsBasedOn
	int32_t ___BoundsBasedOn_2;
	// UnityEngine.Vector3 MoreMountains.Tools.ObjectBounds::<Size>k__BackingField
	Vector3_t3722313464  ___U3CSizeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_BoundsBasedOn_2() { return static_cast<int32_t>(offsetof(ObjectBounds_t107927819, ___BoundsBasedOn_2)); }
	inline int32_t get_BoundsBasedOn_2() const { return ___BoundsBasedOn_2; }
	inline int32_t* get_address_of_BoundsBasedOn_2() { return &___BoundsBasedOn_2; }
	inline void set_BoundsBasedOn_2(int32_t value)
	{
		___BoundsBasedOn_2 = value;
	}

	inline static int32_t get_offset_of_U3CSizeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectBounds_t107927819, ___U3CSizeU3Ek__BackingField_3)); }
	inline Vector3_t3722313464  get_U3CSizeU3Ek__BackingField_3() const { return ___U3CSizeU3Ek__BackingField_3; }
	inline Vector3_t3722313464 * get_address_of_U3CSizeU3Ek__BackingField_3() { return &___U3CSizeU3Ek__BackingField_3; }
	inline void set_U3CSizeU3Ek__BackingField_3(Vector3_t3722313464  value)
	{
		___U3CSizeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTBOUNDS_T107927819_H
#ifndef PARALLAXOFFSET_T133287567_H
#define PARALLAXOFFSET_T133287567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.ParallaxOffset
struct  ParallaxOffset_t133287567  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.ParallaxOffset::Speed
	float ___Speed_2;
	// UnityEngine.UI.RawImage MoreMountains.InfiniteRunnerEngine.ParallaxOffset::_rawImage
	RawImage_t3182918964 * ____rawImage_4;
	// UnityEngine.Renderer MoreMountains.InfiniteRunnerEngine.ParallaxOffset::_renderer
	Renderer_t2627027031 * ____renderer_5;
	// UnityEngine.Vector2 MoreMountains.InfiniteRunnerEngine.ParallaxOffset::_newOffset
	Vector2_t2156229523  ____newOffset_6;
	// System.Single MoreMountains.InfiniteRunnerEngine.ParallaxOffset::_position
	float ____position_7;
	// System.Single MoreMountains.InfiniteRunnerEngine.ParallaxOffset::yOffset
	float ___yOffset_8;

public:
	inline static int32_t get_offset_of_Speed_2() { return static_cast<int32_t>(offsetof(ParallaxOffset_t133287567, ___Speed_2)); }
	inline float get_Speed_2() const { return ___Speed_2; }
	inline float* get_address_of_Speed_2() { return &___Speed_2; }
	inline void set_Speed_2(float value)
	{
		___Speed_2 = value;
	}

	inline static int32_t get_offset_of__rawImage_4() { return static_cast<int32_t>(offsetof(ParallaxOffset_t133287567, ____rawImage_4)); }
	inline RawImage_t3182918964 * get__rawImage_4() const { return ____rawImage_4; }
	inline RawImage_t3182918964 ** get_address_of__rawImage_4() { return &____rawImage_4; }
	inline void set__rawImage_4(RawImage_t3182918964 * value)
	{
		____rawImage_4 = value;
		Il2CppCodeGenWriteBarrier((&____rawImage_4), value);
	}

	inline static int32_t get_offset_of__renderer_5() { return static_cast<int32_t>(offsetof(ParallaxOffset_t133287567, ____renderer_5)); }
	inline Renderer_t2627027031 * get__renderer_5() const { return ____renderer_5; }
	inline Renderer_t2627027031 ** get_address_of__renderer_5() { return &____renderer_5; }
	inline void set__renderer_5(Renderer_t2627027031 * value)
	{
		____renderer_5 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_5), value);
	}

	inline static int32_t get_offset_of__newOffset_6() { return static_cast<int32_t>(offsetof(ParallaxOffset_t133287567, ____newOffset_6)); }
	inline Vector2_t2156229523  get__newOffset_6() const { return ____newOffset_6; }
	inline Vector2_t2156229523 * get_address_of__newOffset_6() { return &____newOffset_6; }
	inline void set__newOffset_6(Vector2_t2156229523  value)
	{
		____newOffset_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(ParallaxOffset_t133287567, ____position_7)); }
	inline float get__position_7() const { return ____position_7; }
	inline float* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(float value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of_yOffset_8() { return static_cast<int32_t>(offsetof(ParallaxOffset_t133287567, ___yOffset_8)); }
	inline float get_yOffset_8() const { return ___yOffset_8; }
	inline float* get_address_of_yOffset_8() { return &___yOffset_8; }
	inline void set_yOffset_8(float value)
	{
		___yOffset_8 = value;
	}
};

struct ParallaxOffset_t133287567_StaticFields
{
public:
	// MoreMountains.InfiniteRunnerEngine.ParallaxOffset MoreMountains.InfiniteRunnerEngine.ParallaxOffset::CurrentParallaxOffset
	ParallaxOffset_t133287567 * ___CurrentParallaxOffset_3;

public:
	inline static int32_t get_offset_of_CurrentParallaxOffset_3() { return static_cast<int32_t>(offsetof(ParallaxOffset_t133287567_StaticFields, ___CurrentParallaxOffset_3)); }
	inline ParallaxOffset_t133287567 * get_CurrentParallaxOffset_3() const { return ___CurrentParallaxOffset_3; }
	inline ParallaxOffset_t133287567 ** get_address_of_CurrentParallaxOffset_3() { return &___CurrentParallaxOffset_3; }
	inline void set_CurrentParallaxOffset_3(ParallaxOffset_t133287567 * value)
	{
		___CurrentParallaxOffset_3 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentParallaxOffset_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARALLAXOFFSET_T133287567_H
#ifndef PICKABLEOBJECT_T2542163765_H
#define PICKABLEOBJECT_T2542163765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.PickableObject
struct  PickableObject_t2542163765  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.PickableObject::PickEffect
	GameObject_t1113636619 * ___PickEffect_2;
	// UnityEngine.AudioClip MoreMountains.InfiniteRunnerEngine.PickableObject::PickSoundFx
	AudioClip_t3680889665 * ___PickSoundFx_3;

public:
	inline static int32_t get_offset_of_PickEffect_2() { return static_cast<int32_t>(offsetof(PickableObject_t2542163765, ___PickEffect_2)); }
	inline GameObject_t1113636619 * get_PickEffect_2() const { return ___PickEffect_2; }
	inline GameObject_t1113636619 ** get_address_of_PickEffect_2() { return &___PickEffect_2; }
	inline void set_PickEffect_2(GameObject_t1113636619 * value)
	{
		___PickEffect_2 = value;
		Il2CppCodeGenWriteBarrier((&___PickEffect_2), value);
	}

	inline static int32_t get_offset_of_PickSoundFx_3() { return static_cast<int32_t>(offsetof(PickableObject_t2542163765, ___PickSoundFx_3)); }
	inline AudioClip_t3680889665 * get_PickSoundFx_3() const { return ___PickSoundFx_3; }
	inline AudioClip_t3680889665 ** get_address_of_PickSoundFx_3() { return &___PickSoundFx_3; }
	inline void set_PickSoundFx_3(AudioClip_t3680889665 * value)
	{
		___PickSoundFx_3 = value;
		Il2CppCodeGenWriteBarrier((&___PickSoundFx_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKABLEOBJECT_T2542163765_H
#ifndef MMTOUCHJOYSTICK_T2762864105_H
#define MMTOUCHJOYSTICK_T2762864105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTouchJoystick
struct  MMTouchJoystick_t2762864105  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera MoreMountains.Tools.MMTouchJoystick::TargetCamera
	Camera_t4157153871 * ___TargetCamera_2;
	// System.Single MoreMountains.Tools.MMTouchJoystick::PressedOpacity
	float ___PressedOpacity_3;
	// System.Boolean MoreMountains.Tools.MMTouchJoystick::HorizontalAxisEnabled
	bool ___HorizontalAxisEnabled_4;
	// System.Boolean MoreMountains.Tools.MMTouchJoystick::VerticalAxisEnabled
	bool ___VerticalAxisEnabled_5;
	// System.Single MoreMountains.Tools.MMTouchJoystick::MaxRange
	float ___MaxRange_6;
	// MoreMountains.Tools.JoystickEvent MoreMountains.Tools.MMTouchJoystick::JoystickValue
	JoystickEvent_t500143420 * ___JoystickValue_7;
	// UnityEngine.RenderMode MoreMountains.Tools.MMTouchJoystick::<ParentCanvasRenderMode>k__BackingField
	int32_t ___U3CParentCanvasRenderModeU3Ek__BackingField_8;
	// UnityEngine.Vector2 MoreMountains.Tools.MMTouchJoystick::_neutralPosition
	Vector2_t2156229523  ____neutralPosition_9;
	// UnityEngine.Vector2 MoreMountains.Tools.MMTouchJoystick::_joystickValue
	Vector2_t2156229523  ____joystickValue_10;
	// UnityEngine.RectTransform MoreMountains.Tools.MMTouchJoystick::_canvasRectTransform
	RectTransform_t3704657025 * ____canvasRectTransform_11;
	// UnityEngine.Vector2 MoreMountains.Tools.MMTouchJoystick::_newTargetPosition
	Vector2_t2156229523  ____newTargetPosition_12;
	// UnityEngine.Vector3 MoreMountains.Tools.MMTouchJoystick::_newJoystickPosition
	Vector3_t3722313464  ____newJoystickPosition_13;
	// System.Single MoreMountains.Tools.MMTouchJoystick::_initialZPosition
	float ____initialZPosition_14;
	// UnityEngine.CanvasGroup MoreMountains.Tools.MMTouchJoystick::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_15;
	// System.Single MoreMountains.Tools.MMTouchJoystick::_initialOpacity
	float ____initialOpacity_16;

public:
	inline static int32_t get_offset_of_TargetCamera_2() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ___TargetCamera_2)); }
	inline Camera_t4157153871 * get_TargetCamera_2() const { return ___TargetCamera_2; }
	inline Camera_t4157153871 ** get_address_of_TargetCamera_2() { return &___TargetCamera_2; }
	inline void set_TargetCamera_2(Camera_t4157153871 * value)
	{
		___TargetCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetCamera_2), value);
	}

	inline static int32_t get_offset_of_PressedOpacity_3() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ___PressedOpacity_3)); }
	inline float get_PressedOpacity_3() const { return ___PressedOpacity_3; }
	inline float* get_address_of_PressedOpacity_3() { return &___PressedOpacity_3; }
	inline void set_PressedOpacity_3(float value)
	{
		___PressedOpacity_3 = value;
	}

	inline static int32_t get_offset_of_HorizontalAxisEnabled_4() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ___HorizontalAxisEnabled_4)); }
	inline bool get_HorizontalAxisEnabled_4() const { return ___HorizontalAxisEnabled_4; }
	inline bool* get_address_of_HorizontalAxisEnabled_4() { return &___HorizontalAxisEnabled_4; }
	inline void set_HorizontalAxisEnabled_4(bool value)
	{
		___HorizontalAxisEnabled_4 = value;
	}

	inline static int32_t get_offset_of_VerticalAxisEnabled_5() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ___VerticalAxisEnabled_5)); }
	inline bool get_VerticalAxisEnabled_5() const { return ___VerticalAxisEnabled_5; }
	inline bool* get_address_of_VerticalAxisEnabled_5() { return &___VerticalAxisEnabled_5; }
	inline void set_VerticalAxisEnabled_5(bool value)
	{
		___VerticalAxisEnabled_5 = value;
	}

	inline static int32_t get_offset_of_MaxRange_6() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ___MaxRange_6)); }
	inline float get_MaxRange_6() const { return ___MaxRange_6; }
	inline float* get_address_of_MaxRange_6() { return &___MaxRange_6; }
	inline void set_MaxRange_6(float value)
	{
		___MaxRange_6 = value;
	}

	inline static int32_t get_offset_of_JoystickValue_7() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ___JoystickValue_7)); }
	inline JoystickEvent_t500143420 * get_JoystickValue_7() const { return ___JoystickValue_7; }
	inline JoystickEvent_t500143420 ** get_address_of_JoystickValue_7() { return &___JoystickValue_7; }
	inline void set_JoystickValue_7(JoystickEvent_t500143420 * value)
	{
		___JoystickValue_7 = value;
		Il2CppCodeGenWriteBarrier((&___JoystickValue_7), value);
	}

	inline static int32_t get_offset_of_U3CParentCanvasRenderModeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ___U3CParentCanvasRenderModeU3Ek__BackingField_8)); }
	inline int32_t get_U3CParentCanvasRenderModeU3Ek__BackingField_8() const { return ___U3CParentCanvasRenderModeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CParentCanvasRenderModeU3Ek__BackingField_8() { return &___U3CParentCanvasRenderModeU3Ek__BackingField_8; }
	inline void set_U3CParentCanvasRenderModeU3Ek__BackingField_8(int32_t value)
	{
		___U3CParentCanvasRenderModeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of__neutralPosition_9() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ____neutralPosition_9)); }
	inline Vector2_t2156229523  get__neutralPosition_9() const { return ____neutralPosition_9; }
	inline Vector2_t2156229523 * get_address_of__neutralPosition_9() { return &____neutralPosition_9; }
	inline void set__neutralPosition_9(Vector2_t2156229523  value)
	{
		____neutralPosition_9 = value;
	}

	inline static int32_t get_offset_of__joystickValue_10() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ____joystickValue_10)); }
	inline Vector2_t2156229523  get__joystickValue_10() const { return ____joystickValue_10; }
	inline Vector2_t2156229523 * get_address_of__joystickValue_10() { return &____joystickValue_10; }
	inline void set__joystickValue_10(Vector2_t2156229523  value)
	{
		____joystickValue_10 = value;
	}

	inline static int32_t get_offset_of__canvasRectTransform_11() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ____canvasRectTransform_11)); }
	inline RectTransform_t3704657025 * get__canvasRectTransform_11() const { return ____canvasRectTransform_11; }
	inline RectTransform_t3704657025 ** get_address_of__canvasRectTransform_11() { return &____canvasRectTransform_11; }
	inline void set__canvasRectTransform_11(RectTransform_t3704657025 * value)
	{
		____canvasRectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRectTransform_11), value);
	}

	inline static int32_t get_offset_of__newTargetPosition_12() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ____newTargetPosition_12)); }
	inline Vector2_t2156229523  get__newTargetPosition_12() const { return ____newTargetPosition_12; }
	inline Vector2_t2156229523 * get_address_of__newTargetPosition_12() { return &____newTargetPosition_12; }
	inline void set__newTargetPosition_12(Vector2_t2156229523  value)
	{
		____newTargetPosition_12 = value;
	}

	inline static int32_t get_offset_of__newJoystickPosition_13() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ____newJoystickPosition_13)); }
	inline Vector3_t3722313464  get__newJoystickPosition_13() const { return ____newJoystickPosition_13; }
	inline Vector3_t3722313464 * get_address_of__newJoystickPosition_13() { return &____newJoystickPosition_13; }
	inline void set__newJoystickPosition_13(Vector3_t3722313464  value)
	{
		____newJoystickPosition_13 = value;
	}

	inline static int32_t get_offset_of__initialZPosition_14() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ____initialZPosition_14)); }
	inline float get__initialZPosition_14() const { return ____initialZPosition_14; }
	inline float* get_address_of__initialZPosition_14() { return &____initialZPosition_14; }
	inline void set__initialZPosition_14(float value)
	{
		____initialZPosition_14 = value;
	}

	inline static int32_t get_offset_of__canvasGroup_15() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ____canvasGroup_15)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_15() const { return ____canvasGroup_15; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_15() { return &____canvasGroup_15; }
	inline void set__canvasGroup_15(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_15 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_15), value);
	}

	inline static int32_t get_offset_of__initialOpacity_16() { return static_cast<int32_t>(offsetof(MMTouchJoystick_t2762864105, ____initialOpacity_16)); }
	inline float get__initialOpacity_16() const { return ____initialOpacity_16; }
	inline float* get_address_of__initialOpacity_16() { return &____initialOpacity_16; }
	inline void set__initialOpacity_16(float value)
	{
		____initialOpacity_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMTOUCHJOYSTICK_T2762864105_H
#ifndef MMCONTROLSTESTINPUTMANAGER_T8107686_H
#define MMCONTROLSTESTINPUTMANAGER_T8107686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMControlsTestInputManager
struct  MMControlsTestInputManager_t8107686  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMCONTROLSTESTINPUTMANAGER_T8107686_H
#ifndef AUTOROTATE_T2431756326_H
#define AUTOROTATE_T2431756326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.AutoRotate
struct  AutoRotate_t2431756326  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Space MoreMountains.Tools.AutoRotate::RotationSpace
	int32_t ___RotationSpace_2;
	// UnityEngine.Vector3 MoreMountains.Tools.AutoRotate::RotationSpeed
	Vector3_t3722313464  ___RotationSpeed_3;

public:
	inline static int32_t get_offset_of_RotationSpace_2() { return static_cast<int32_t>(offsetof(AutoRotate_t2431756326, ___RotationSpace_2)); }
	inline int32_t get_RotationSpace_2() const { return ___RotationSpace_2; }
	inline int32_t* get_address_of_RotationSpace_2() { return &___RotationSpace_2; }
	inline void set_RotationSpace_2(int32_t value)
	{
		___RotationSpace_2 = value;
	}

	inline static int32_t get_offset_of_RotationSpeed_3() { return static_cast<int32_t>(offsetof(AutoRotate_t2431756326, ___RotationSpeed_3)); }
	inline Vector3_t3722313464  get_RotationSpeed_3() const { return ___RotationSpeed_3; }
	inline Vector3_t3722313464 * get_address_of_RotationSpeed_3() { return &___RotationSpeed_3; }
	inline void set_RotationSpeed_3(Vector3_t3722313464  value)
	{
		___RotationSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOROTATE_T2431756326_H
#ifndef PATHMOVEMENT_T1036546472_H
#define PATHMOVEMENT_T1036546472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PathMovement
struct  PathMovement_t1036546472  : public MonoBehaviour_t3962482529
{
public:
	// MoreMountains.Tools.PathMovement/CycleOptions MoreMountains.Tools.PathMovement::CycleOption
	int32_t ___CycleOption_2;
	// MoreMountains.Tools.PathMovement/MovementDirection MoreMountains.Tools.PathMovement::LoopInitialMovementDirection
	int32_t ___LoopInitialMovementDirection_3;
	// System.Collections.Generic.List`1<MoreMountains.Tools.PathMovementElement> MoreMountains.Tools.PathMovement::PathElements
	List_1_t3650174854 * ___PathElements_4;
	// System.Single MoreMountains.Tools.PathMovement::MovementSpeed
	float ___MovementSpeed_5;
	// UnityEngine.Vector3 MoreMountains.Tools.PathMovement::<CurrentSpeed>k__BackingField
	Vector3_t3722313464  ___U3CCurrentSpeedU3Ek__BackingField_6;
	// MoreMountains.Tools.PathMovement/PossibleAccelerationType MoreMountains.Tools.PathMovement::AccelerationType
	int32_t ___AccelerationType_7;
	// UnityEngine.AnimationCurve MoreMountains.Tools.PathMovement::Acceleration
	AnimationCurve_t3046754366 * ___Acceleration_8;
	// System.Single MoreMountains.Tools.PathMovement::MinDistanceToGoal
	float ___MinDistanceToGoal_9;
	// UnityEngine.Vector3 MoreMountains.Tools.PathMovement::_originalTransformPosition
	Vector3_t3722313464  ____originalTransformPosition_10;
	// System.Boolean MoreMountains.Tools.PathMovement::_originalTransformPositionStatus
	bool ____originalTransformPositionStatus_11;
	// System.Boolean MoreMountains.Tools.PathMovement::_active
	bool ____active_12;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3> MoreMountains.Tools.PathMovement::_currentPoint
	RuntimeObject* ____currentPoint_13;
	// System.Int32 MoreMountains.Tools.PathMovement::_direction
	int32_t ____direction_14;
	// UnityEngine.Vector3 MoreMountains.Tools.PathMovement::_initialPosition
	Vector3_t3722313464  ____initialPosition_15;
	// UnityEngine.Vector3 MoreMountains.Tools.PathMovement::_finalPosition
	Vector3_t3722313464  ____finalPosition_16;
	// UnityEngine.Vector3 MoreMountains.Tools.PathMovement::_previousPoint
	Vector3_t3722313464  ____previousPoint_17;
	// System.Single MoreMountains.Tools.PathMovement::_waiting
	float ____waiting_18;
	// System.Int32 MoreMountains.Tools.PathMovement::_currentIndex
	int32_t ____currentIndex_19;
	// System.Single MoreMountains.Tools.PathMovement::_distanceToNextPoint
	float ____distanceToNextPoint_20;
	// System.Boolean MoreMountains.Tools.PathMovement::_endReached
	bool ____endReached_21;

public:
	inline static int32_t get_offset_of_CycleOption_2() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ___CycleOption_2)); }
	inline int32_t get_CycleOption_2() const { return ___CycleOption_2; }
	inline int32_t* get_address_of_CycleOption_2() { return &___CycleOption_2; }
	inline void set_CycleOption_2(int32_t value)
	{
		___CycleOption_2 = value;
	}

	inline static int32_t get_offset_of_LoopInitialMovementDirection_3() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ___LoopInitialMovementDirection_3)); }
	inline int32_t get_LoopInitialMovementDirection_3() const { return ___LoopInitialMovementDirection_3; }
	inline int32_t* get_address_of_LoopInitialMovementDirection_3() { return &___LoopInitialMovementDirection_3; }
	inline void set_LoopInitialMovementDirection_3(int32_t value)
	{
		___LoopInitialMovementDirection_3 = value;
	}

	inline static int32_t get_offset_of_PathElements_4() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ___PathElements_4)); }
	inline List_1_t3650174854 * get_PathElements_4() const { return ___PathElements_4; }
	inline List_1_t3650174854 ** get_address_of_PathElements_4() { return &___PathElements_4; }
	inline void set_PathElements_4(List_1_t3650174854 * value)
	{
		___PathElements_4 = value;
		Il2CppCodeGenWriteBarrier((&___PathElements_4), value);
	}

	inline static int32_t get_offset_of_MovementSpeed_5() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ___MovementSpeed_5)); }
	inline float get_MovementSpeed_5() const { return ___MovementSpeed_5; }
	inline float* get_address_of_MovementSpeed_5() { return &___MovementSpeed_5; }
	inline void set_MovementSpeed_5(float value)
	{
		___MovementSpeed_5 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentSpeedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ___U3CCurrentSpeedU3Ek__BackingField_6)); }
	inline Vector3_t3722313464  get_U3CCurrentSpeedU3Ek__BackingField_6() const { return ___U3CCurrentSpeedU3Ek__BackingField_6; }
	inline Vector3_t3722313464 * get_address_of_U3CCurrentSpeedU3Ek__BackingField_6() { return &___U3CCurrentSpeedU3Ek__BackingField_6; }
	inline void set_U3CCurrentSpeedU3Ek__BackingField_6(Vector3_t3722313464  value)
	{
		___U3CCurrentSpeedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_AccelerationType_7() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ___AccelerationType_7)); }
	inline int32_t get_AccelerationType_7() const { return ___AccelerationType_7; }
	inline int32_t* get_address_of_AccelerationType_7() { return &___AccelerationType_7; }
	inline void set_AccelerationType_7(int32_t value)
	{
		___AccelerationType_7 = value;
	}

	inline static int32_t get_offset_of_Acceleration_8() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ___Acceleration_8)); }
	inline AnimationCurve_t3046754366 * get_Acceleration_8() const { return ___Acceleration_8; }
	inline AnimationCurve_t3046754366 ** get_address_of_Acceleration_8() { return &___Acceleration_8; }
	inline void set_Acceleration_8(AnimationCurve_t3046754366 * value)
	{
		___Acceleration_8 = value;
		Il2CppCodeGenWriteBarrier((&___Acceleration_8), value);
	}

	inline static int32_t get_offset_of_MinDistanceToGoal_9() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ___MinDistanceToGoal_9)); }
	inline float get_MinDistanceToGoal_9() const { return ___MinDistanceToGoal_9; }
	inline float* get_address_of_MinDistanceToGoal_9() { return &___MinDistanceToGoal_9; }
	inline void set_MinDistanceToGoal_9(float value)
	{
		___MinDistanceToGoal_9 = value;
	}

	inline static int32_t get_offset_of__originalTransformPosition_10() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____originalTransformPosition_10)); }
	inline Vector3_t3722313464  get__originalTransformPosition_10() const { return ____originalTransformPosition_10; }
	inline Vector3_t3722313464 * get_address_of__originalTransformPosition_10() { return &____originalTransformPosition_10; }
	inline void set__originalTransformPosition_10(Vector3_t3722313464  value)
	{
		____originalTransformPosition_10 = value;
	}

	inline static int32_t get_offset_of__originalTransformPositionStatus_11() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____originalTransformPositionStatus_11)); }
	inline bool get__originalTransformPositionStatus_11() const { return ____originalTransformPositionStatus_11; }
	inline bool* get_address_of__originalTransformPositionStatus_11() { return &____originalTransformPositionStatus_11; }
	inline void set__originalTransformPositionStatus_11(bool value)
	{
		____originalTransformPositionStatus_11 = value;
	}

	inline static int32_t get_offset_of__active_12() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____active_12)); }
	inline bool get__active_12() const { return ____active_12; }
	inline bool* get_address_of__active_12() { return &____active_12; }
	inline void set__active_12(bool value)
	{
		____active_12 = value;
	}

	inline static int32_t get_offset_of__currentPoint_13() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____currentPoint_13)); }
	inline RuntimeObject* get__currentPoint_13() const { return ____currentPoint_13; }
	inline RuntimeObject** get_address_of__currentPoint_13() { return &____currentPoint_13; }
	inline void set__currentPoint_13(RuntimeObject* value)
	{
		____currentPoint_13 = value;
		Il2CppCodeGenWriteBarrier((&____currentPoint_13), value);
	}

	inline static int32_t get_offset_of__direction_14() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____direction_14)); }
	inline int32_t get__direction_14() const { return ____direction_14; }
	inline int32_t* get_address_of__direction_14() { return &____direction_14; }
	inline void set__direction_14(int32_t value)
	{
		____direction_14 = value;
	}

	inline static int32_t get_offset_of__initialPosition_15() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____initialPosition_15)); }
	inline Vector3_t3722313464  get__initialPosition_15() const { return ____initialPosition_15; }
	inline Vector3_t3722313464 * get_address_of__initialPosition_15() { return &____initialPosition_15; }
	inline void set__initialPosition_15(Vector3_t3722313464  value)
	{
		____initialPosition_15 = value;
	}

	inline static int32_t get_offset_of__finalPosition_16() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____finalPosition_16)); }
	inline Vector3_t3722313464  get__finalPosition_16() const { return ____finalPosition_16; }
	inline Vector3_t3722313464 * get_address_of__finalPosition_16() { return &____finalPosition_16; }
	inline void set__finalPosition_16(Vector3_t3722313464  value)
	{
		____finalPosition_16 = value;
	}

	inline static int32_t get_offset_of__previousPoint_17() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____previousPoint_17)); }
	inline Vector3_t3722313464  get__previousPoint_17() const { return ____previousPoint_17; }
	inline Vector3_t3722313464 * get_address_of__previousPoint_17() { return &____previousPoint_17; }
	inline void set__previousPoint_17(Vector3_t3722313464  value)
	{
		____previousPoint_17 = value;
	}

	inline static int32_t get_offset_of__waiting_18() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____waiting_18)); }
	inline float get__waiting_18() const { return ____waiting_18; }
	inline float* get_address_of__waiting_18() { return &____waiting_18; }
	inline void set__waiting_18(float value)
	{
		____waiting_18 = value;
	}

	inline static int32_t get_offset_of__currentIndex_19() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____currentIndex_19)); }
	inline int32_t get__currentIndex_19() const { return ____currentIndex_19; }
	inline int32_t* get_address_of__currentIndex_19() { return &____currentIndex_19; }
	inline void set__currentIndex_19(int32_t value)
	{
		____currentIndex_19 = value;
	}

	inline static int32_t get_offset_of__distanceToNextPoint_20() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____distanceToNextPoint_20)); }
	inline float get__distanceToNextPoint_20() const { return ____distanceToNextPoint_20; }
	inline float* get_address_of__distanceToNextPoint_20() { return &____distanceToNextPoint_20; }
	inline void set__distanceToNextPoint_20(float value)
	{
		____distanceToNextPoint_20 = value;
	}

	inline static int32_t get_offset_of__endReached_21() { return static_cast<int32_t>(offsetof(PathMovement_t1036546472, ____endReached_21)); }
	inline bool get__endReached_21() const { return ____endReached_21; }
	inline bool* get_address_of__endReached_21() { return &____endReached_21; }
	inline void set__endReached_21(bool value)
	{
		____endReached_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMOVEMENT_T1036546472_H
#ifndef MMACHIEVEMENTRULES_T226002105_H
#define MMACHIEVEMENTRULES_T226002105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAchievementRules
struct  MMAchievementRules_t226002105  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMACHIEVEMENTRULES_T226002105_H
#ifndef TIMEDAUTODESTROY_T483462573_H
#define TIMEDAUTODESTROY_T483462573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.TimedAutoDestroy
struct  TimedAutoDestroy_t483462573  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoreMountains.Tools.TimedAutoDestroy::TimeBeforeDestruction
	float ___TimeBeforeDestruction_2;
	// UnityEngine.WaitForSeconds MoreMountains.Tools.TimedAutoDestroy::_timeBeforeDestructionWFS
	WaitForSeconds_t1699091251 * ____timeBeforeDestructionWFS_3;

public:
	inline static int32_t get_offset_of_TimeBeforeDestruction_2() { return static_cast<int32_t>(offsetof(TimedAutoDestroy_t483462573, ___TimeBeforeDestruction_2)); }
	inline float get_TimeBeforeDestruction_2() const { return ___TimeBeforeDestruction_2; }
	inline float* get_address_of_TimeBeforeDestruction_2() { return &___TimeBeforeDestruction_2; }
	inline void set_TimeBeforeDestruction_2(float value)
	{
		___TimeBeforeDestruction_2 = value;
	}

	inline static int32_t get_offset_of__timeBeforeDestructionWFS_3() { return static_cast<int32_t>(offsetof(TimedAutoDestroy_t483462573, ____timeBeforeDestructionWFS_3)); }
	inline WaitForSeconds_t1699091251 * get__timeBeforeDestructionWFS_3() const { return ____timeBeforeDestructionWFS_3; }
	inline WaitForSeconds_t1699091251 ** get_address_of__timeBeforeDestructionWFS_3() { return &____timeBeforeDestructionWFS_3; }
	inline void set__timeBeforeDestructionWFS_3(WaitForSeconds_t1699091251 * value)
	{
		____timeBeforeDestructionWFS_3 = value;
		Il2CppCodeGenWriteBarrier((&____timeBeforeDestructionWFS_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDAUTODESTROY_T483462573_H
#ifndef SCENEVIEWICON_T2894348385_H
#define SCENEVIEWICON_T2894348385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.SceneViewIcon
struct  SceneViewIcon_t2894348385  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEVIEWICON_T2894348385_H
#ifndef PROGRESSBAR_T1074804577_H
#define PROGRESSBAR_T1074804577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar
struct  ProgressBar_t1074804577  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ProgressBar::ForegroundBar
	Transform_t3600365921 * ___ForegroundBar_2;
	// System.String ProgressBar::PlayerID
	String_t* ___PlayerID_3;
	// UnityEngine.Vector3 ProgressBar::_newLocalScale
	Vector3_t3722313464  ____newLocalScale_4;
	// System.Single ProgressBar::_newPercent
	float ____newPercent_5;

public:
	inline static int32_t get_offset_of_ForegroundBar_2() { return static_cast<int32_t>(offsetof(ProgressBar_t1074804577, ___ForegroundBar_2)); }
	inline Transform_t3600365921 * get_ForegroundBar_2() const { return ___ForegroundBar_2; }
	inline Transform_t3600365921 ** get_address_of_ForegroundBar_2() { return &___ForegroundBar_2; }
	inline void set_ForegroundBar_2(Transform_t3600365921 * value)
	{
		___ForegroundBar_2 = value;
		Il2CppCodeGenWriteBarrier((&___ForegroundBar_2), value);
	}

	inline static int32_t get_offset_of_PlayerID_3() { return static_cast<int32_t>(offsetof(ProgressBar_t1074804577, ___PlayerID_3)); }
	inline String_t* get_PlayerID_3() const { return ___PlayerID_3; }
	inline String_t** get_address_of_PlayerID_3() { return &___PlayerID_3; }
	inline void set_PlayerID_3(String_t* value)
	{
		___PlayerID_3 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerID_3), value);
	}

	inline static int32_t get_offset_of__newLocalScale_4() { return static_cast<int32_t>(offsetof(ProgressBar_t1074804577, ____newLocalScale_4)); }
	inline Vector3_t3722313464  get__newLocalScale_4() const { return ____newLocalScale_4; }
	inline Vector3_t3722313464 * get_address_of__newLocalScale_4() { return &____newLocalScale_4; }
	inline void set__newLocalScale_4(Vector3_t3722313464  value)
	{
		____newLocalScale_4 = value;
	}

	inline static int32_t get_offset_of__newPercent_5() { return static_cast<int32_t>(offsetof(ProgressBar_t1074804577, ____newPercent_5)); }
	inline float get__newPercent_5() const { return ____newPercent_5; }
	inline float* get_address_of__newPercent_5() { return &____newPercent_5; }
	inline void set__newPercent_5(float value)
	{
		____newPercent_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBAR_T1074804577_H
#ifndef GETFOCUSONENABLE_T3625362509_H
#define GETFOCUSONENABLE_T3625362509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetFocusOnEnable
struct  GetFocusOnEnable_t3625362509  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFOCUSONENABLE_T3625362509_H
#ifndef AUTOORDERINLAYER_T2678432318_H
#define AUTOORDERINLAYER_T2678432318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.AutoOrderInLayer
struct  AutoOrderInLayer_t2678432318  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 MoreMountains.Tools.AutoOrderInLayer::GlobalCounterIncrement
	int32_t ___GlobalCounterIncrement_3;
	// System.Boolean MoreMountains.Tools.AutoOrderInLayer::BasedOnParentOrder
	bool ___BasedOnParentOrder_4;
	// System.Int32 MoreMountains.Tools.AutoOrderInLayer::ParentIncrement
	int32_t ___ParentIncrement_5;
	// System.Boolean MoreMountains.Tools.AutoOrderInLayer::ApplyNewOrderToChildren
	bool ___ApplyNewOrderToChildren_6;
	// System.Int32 MoreMountains.Tools.AutoOrderInLayer::ChildrenIncrement
	int32_t ___ChildrenIncrement_7;
	// UnityEngine.SpriteRenderer MoreMountains.Tools.AutoOrderInLayer::_spriteRenderer
	SpriteRenderer_t3235626157 * ____spriteRenderer_8;

public:
	inline static int32_t get_offset_of_GlobalCounterIncrement_3() { return static_cast<int32_t>(offsetof(AutoOrderInLayer_t2678432318, ___GlobalCounterIncrement_3)); }
	inline int32_t get_GlobalCounterIncrement_3() const { return ___GlobalCounterIncrement_3; }
	inline int32_t* get_address_of_GlobalCounterIncrement_3() { return &___GlobalCounterIncrement_3; }
	inline void set_GlobalCounterIncrement_3(int32_t value)
	{
		___GlobalCounterIncrement_3 = value;
	}

	inline static int32_t get_offset_of_BasedOnParentOrder_4() { return static_cast<int32_t>(offsetof(AutoOrderInLayer_t2678432318, ___BasedOnParentOrder_4)); }
	inline bool get_BasedOnParentOrder_4() const { return ___BasedOnParentOrder_4; }
	inline bool* get_address_of_BasedOnParentOrder_4() { return &___BasedOnParentOrder_4; }
	inline void set_BasedOnParentOrder_4(bool value)
	{
		___BasedOnParentOrder_4 = value;
	}

	inline static int32_t get_offset_of_ParentIncrement_5() { return static_cast<int32_t>(offsetof(AutoOrderInLayer_t2678432318, ___ParentIncrement_5)); }
	inline int32_t get_ParentIncrement_5() const { return ___ParentIncrement_5; }
	inline int32_t* get_address_of_ParentIncrement_5() { return &___ParentIncrement_5; }
	inline void set_ParentIncrement_5(int32_t value)
	{
		___ParentIncrement_5 = value;
	}

	inline static int32_t get_offset_of_ApplyNewOrderToChildren_6() { return static_cast<int32_t>(offsetof(AutoOrderInLayer_t2678432318, ___ApplyNewOrderToChildren_6)); }
	inline bool get_ApplyNewOrderToChildren_6() const { return ___ApplyNewOrderToChildren_6; }
	inline bool* get_address_of_ApplyNewOrderToChildren_6() { return &___ApplyNewOrderToChildren_6; }
	inline void set_ApplyNewOrderToChildren_6(bool value)
	{
		___ApplyNewOrderToChildren_6 = value;
	}

	inline static int32_t get_offset_of_ChildrenIncrement_7() { return static_cast<int32_t>(offsetof(AutoOrderInLayer_t2678432318, ___ChildrenIncrement_7)); }
	inline int32_t get_ChildrenIncrement_7() const { return ___ChildrenIncrement_7; }
	inline int32_t* get_address_of_ChildrenIncrement_7() { return &___ChildrenIncrement_7; }
	inline void set_ChildrenIncrement_7(int32_t value)
	{
		___ChildrenIncrement_7 = value;
	}

	inline static int32_t get_offset_of__spriteRenderer_8() { return static_cast<int32_t>(offsetof(AutoOrderInLayer_t2678432318, ____spriteRenderer_8)); }
	inline SpriteRenderer_t3235626157 * get__spriteRenderer_8() const { return ____spriteRenderer_8; }
	inline SpriteRenderer_t3235626157 ** get_address_of__spriteRenderer_8() { return &____spriteRenderer_8; }
	inline void set__spriteRenderer_8(SpriteRenderer_t3235626157 * value)
	{
		____spriteRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&____spriteRenderer_8), value);
	}
};

struct AutoOrderInLayer_t2678432318_StaticFields
{
public:
	// System.Int32 MoreMountains.Tools.AutoOrderInLayer::CurrentMaxCharacterOrderInLayer
	int32_t ___CurrentMaxCharacterOrderInLayer_2;

public:
	inline static int32_t get_offset_of_CurrentMaxCharacterOrderInLayer_2() { return static_cast<int32_t>(offsetof(AutoOrderInLayer_t2678432318_StaticFields, ___CurrentMaxCharacterOrderInLayer_2)); }
	inline int32_t get_CurrentMaxCharacterOrderInLayer_2() const { return ___CurrentMaxCharacterOrderInLayer_2; }
	inline int32_t* get_address_of_CurrentMaxCharacterOrderInLayer_2() { return &___CurrentMaxCharacterOrderInLayer_2; }
	inline void set_CurrentMaxCharacterOrderInLayer_2(int32_t value)
	{
		___CurrentMaxCharacterOrderInLayer_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOORDERINLAYER_T2678432318_H
#ifndef FPSCOUNTER_T4258725281_H
#define FPSCOUNTER_T4258725281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSCounter
struct  FPSCounter_t4258725281  : public MonoBehaviour_t3962482529
{
public:
	// System.Single FPSCounter::UpdateInterval
	float ___UpdateInterval_2;
	// System.Single FPSCounter::_framesAccumulated
	float ____framesAccumulated_3;
	// System.Single FPSCounter::_framesDrawnInTheInterval
	float ____framesDrawnInTheInterval_4;
	// System.Single FPSCounter::_timeLeft
	float ____timeLeft_5;
	// UnityEngine.UI.Text FPSCounter::_text
	Text_t1901882714 * ____text_6;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(FPSCounter_t4258725281, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of__framesAccumulated_3() { return static_cast<int32_t>(offsetof(FPSCounter_t4258725281, ____framesAccumulated_3)); }
	inline float get__framesAccumulated_3() const { return ____framesAccumulated_3; }
	inline float* get_address_of__framesAccumulated_3() { return &____framesAccumulated_3; }
	inline void set__framesAccumulated_3(float value)
	{
		____framesAccumulated_3 = value;
	}

	inline static int32_t get_offset_of__framesDrawnInTheInterval_4() { return static_cast<int32_t>(offsetof(FPSCounter_t4258725281, ____framesDrawnInTheInterval_4)); }
	inline float get__framesDrawnInTheInterval_4() const { return ____framesDrawnInTheInterval_4; }
	inline float* get_address_of__framesDrawnInTheInterval_4() { return &____framesDrawnInTheInterval_4; }
	inline void set__framesDrawnInTheInterval_4(float value)
	{
		____framesDrawnInTheInterval_4 = value;
	}

	inline static int32_t get_offset_of__timeLeft_5() { return static_cast<int32_t>(offsetof(FPSCounter_t4258725281, ____timeLeft_5)); }
	inline float get__timeLeft_5() const { return ____timeLeft_5; }
	inline float* get_address_of__timeLeft_5() { return &____timeLeft_5; }
	inline void set__timeLeft_5(float value)
	{
		____timeLeft_5 = value;
	}

	inline static int32_t get_offset_of__text_6() { return static_cast<int32_t>(offsetof(FPSCounter_t4258725281, ____text_6)); }
	inline Text_t1901882714 * get__text_6() const { return ____text_6; }
	inline Text_t1901882714 ** get_address_of__text_6() { return &____text_6; }
	inline void set__text_6(Text_t1901882714 * value)
	{
		____text_6 = value;
		Il2CppCodeGenWriteBarrier((&____text_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T4258725281_H
#ifndef MMSWIPEZONE_T2755251325_H
#define MMSWIPEZONE_T2755251325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMSwipeZone
struct  MMSwipeZone_t2755251325  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoreMountains.Tools.MMSwipeZone::MinimalSwipeLength
	float ___MinimalSwipeLength_2;
	// System.Single MoreMountains.Tools.MMSwipeZone::MaximumPressLength
	float ___MaximumPressLength_3;
	// MoreMountains.Tools.SwipeEvent MoreMountains.Tools.MMSwipeZone::ZoneSwiped
	SwipeEvent_t1828135924 * ___ZoneSwiped_4;
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMSwipeZone::ZonePressed
	UnityEvent_t2581268647 * ___ZonePressed_5;
	// System.Boolean MoreMountains.Tools.MMSwipeZone::MouseMode
	bool ___MouseMode_6;
	// UnityEngine.Vector2 MoreMountains.Tools.MMSwipeZone::_firstTouchPosition
	Vector2_t2156229523  ____firstTouchPosition_7;
	// System.Single MoreMountains.Tools.MMSwipeZone::_angle
	float ____angle_8;
	// System.Single MoreMountains.Tools.MMSwipeZone::_length
	float ____length_9;
	// UnityEngine.Vector2 MoreMountains.Tools.MMSwipeZone::_destination
	Vector2_t2156229523  ____destination_10;
	// UnityEngine.Vector2 MoreMountains.Tools.MMSwipeZone::_deltaSwipe
	Vector2_t2156229523  ____deltaSwipe_11;
	// MoreMountains.Tools.MMPossibleSwipeDirections MoreMountains.Tools.MMSwipeZone::_swipeDirection
	int32_t ____swipeDirection_12;

public:
	inline static int32_t get_offset_of_MinimalSwipeLength_2() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ___MinimalSwipeLength_2)); }
	inline float get_MinimalSwipeLength_2() const { return ___MinimalSwipeLength_2; }
	inline float* get_address_of_MinimalSwipeLength_2() { return &___MinimalSwipeLength_2; }
	inline void set_MinimalSwipeLength_2(float value)
	{
		___MinimalSwipeLength_2 = value;
	}

	inline static int32_t get_offset_of_MaximumPressLength_3() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ___MaximumPressLength_3)); }
	inline float get_MaximumPressLength_3() const { return ___MaximumPressLength_3; }
	inline float* get_address_of_MaximumPressLength_3() { return &___MaximumPressLength_3; }
	inline void set_MaximumPressLength_3(float value)
	{
		___MaximumPressLength_3 = value;
	}

	inline static int32_t get_offset_of_ZoneSwiped_4() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ___ZoneSwiped_4)); }
	inline SwipeEvent_t1828135924 * get_ZoneSwiped_4() const { return ___ZoneSwiped_4; }
	inline SwipeEvent_t1828135924 ** get_address_of_ZoneSwiped_4() { return &___ZoneSwiped_4; }
	inline void set_ZoneSwiped_4(SwipeEvent_t1828135924 * value)
	{
		___ZoneSwiped_4 = value;
		Il2CppCodeGenWriteBarrier((&___ZoneSwiped_4), value);
	}

	inline static int32_t get_offset_of_ZonePressed_5() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ___ZonePressed_5)); }
	inline UnityEvent_t2581268647 * get_ZonePressed_5() const { return ___ZonePressed_5; }
	inline UnityEvent_t2581268647 ** get_address_of_ZonePressed_5() { return &___ZonePressed_5; }
	inline void set_ZonePressed_5(UnityEvent_t2581268647 * value)
	{
		___ZonePressed_5 = value;
		Il2CppCodeGenWriteBarrier((&___ZonePressed_5), value);
	}

	inline static int32_t get_offset_of_MouseMode_6() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ___MouseMode_6)); }
	inline bool get_MouseMode_6() const { return ___MouseMode_6; }
	inline bool* get_address_of_MouseMode_6() { return &___MouseMode_6; }
	inline void set_MouseMode_6(bool value)
	{
		___MouseMode_6 = value;
	}

	inline static int32_t get_offset_of__firstTouchPosition_7() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ____firstTouchPosition_7)); }
	inline Vector2_t2156229523  get__firstTouchPosition_7() const { return ____firstTouchPosition_7; }
	inline Vector2_t2156229523 * get_address_of__firstTouchPosition_7() { return &____firstTouchPosition_7; }
	inline void set__firstTouchPosition_7(Vector2_t2156229523  value)
	{
		____firstTouchPosition_7 = value;
	}

	inline static int32_t get_offset_of__angle_8() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ____angle_8)); }
	inline float get__angle_8() const { return ____angle_8; }
	inline float* get_address_of__angle_8() { return &____angle_8; }
	inline void set__angle_8(float value)
	{
		____angle_8 = value;
	}

	inline static int32_t get_offset_of__length_9() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ____length_9)); }
	inline float get__length_9() const { return ____length_9; }
	inline float* get_address_of__length_9() { return &____length_9; }
	inline void set__length_9(float value)
	{
		____length_9 = value;
	}

	inline static int32_t get_offset_of__destination_10() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ____destination_10)); }
	inline Vector2_t2156229523  get__destination_10() const { return ____destination_10; }
	inline Vector2_t2156229523 * get_address_of__destination_10() { return &____destination_10; }
	inline void set__destination_10(Vector2_t2156229523  value)
	{
		____destination_10 = value;
	}

	inline static int32_t get_offset_of__deltaSwipe_11() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ____deltaSwipe_11)); }
	inline Vector2_t2156229523  get__deltaSwipe_11() const { return ____deltaSwipe_11; }
	inline Vector2_t2156229523 * get_address_of__deltaSwipe_11() { return &____deltaSwipe_11; }
	inline void set__deltaSwipe_11(Vector2_t2156229523  value)
	{
		____deltaSwipe_11 = value;
	}

	inline static int32_t get_offset_of__swipeDirection_12() { return static_cast<int32_t>(offsetof(MMSwipeZone_t2755251325, ____swipeDirection_12)); }
	inline int32_t get__swipeDirection_12() const { return ____swipeDirection_12; }
	inline int32_t* get_address_of__swipeDirection_12() { return &____swipeDirection_12; }
	inline void set__swipeDirection_12(int32_t value)
	{
		____swipeDirection_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMSWIPEZONE_T2755251325_H
#ifndef MMTOUCHBUTTON_T1318972791_H
#define MMTOUCHBUTTON_T1318972791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTouchButton
struct  MMTouchButton_t1318972791  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMTouchButton::ButtonPressedFirstTime
	UnityEvent_t2581268647 * ___ButtonPressedFirstTime_2;
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMTouchButton::ButtonReleased
	UnityEvent_t2581268647 * ___ButtonReleased_3;
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMTouchButton::ButtonPressed
	UnityEvent_t2581268647 * ___ButtonPressed_4;
	// System.Single MoreMountains.Tools.MMTouchButton::PressedOpacity
	float ___PressedOpacity_5;
	// System.Boolean MoreMountains.Tools.MMTouchButton::MouseMode
	bool ___MouseMode_6;
	// MoreMountains.Tools.MMTouchButton/ButtonStates MoreMountains.Tools.MMTouchButton::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_7;
	// System.Boolean MoreMountains.Tools.MMTouchButton::_zonePressed
	bool ____zonePressed_8;
	// UnityEngine.CanvasGroup MoreMountains.Tools.MMTouchButton::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_9;
	// System.Single MoreMountains.Tools.MMTouchButton::_initialOpacity
	float ____initialOpacity_10;

public:
	inline static int32_t get_offset_of_ButtonPressedFirstTime_2() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ___ButtonPressedFirstTime_2)); }
	inline UnityEvent_t2581268647 * get_ButtonPressedFirstTime_2() const { return ___ButtonPressedFirstTime_2; }
	inline UnityEvent_t2581268647 ** get_address_of_ButtonPressedFirstTime_2() { return &___ButtonPressedFirstTime_2; }
	inline void set_ButtonPressedFirstTime_2(UnityEvent_t2581268647 * value)
	{
		___ButtonPressedFirstTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonPressedFirstTime_2), value);
	}

	inline static int32_t get_offset_of_ButtonReleased_3() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ___ButtonReleased_3)); }
	inline UnityEvent_t2581268647 * get_ButtonReleased_3() const { return ___ButtonReleased_3; }
	inline UnityEvent_t2581268647 ** get_address_of_ButtonReleased_3() { return &___ButtonReleased_3; }
	inline void set_ButtonReleased_3(UnityEvent_t2581268647 * value)
	{
		___ButtonReleased_3 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonReleased_3), value);
	}

	inline static int32_t get_offset_of_ButtonPressed_4() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ___ButtonPressed_4)); }
	inline UnityEvent_t2581268647 * get_ButtonPressed_4() const { return ___ButtonPressed_4; }
	inline UnityEvent_t2581268647 ** get_address_of_ButtonPressed_4() { return &___ButtonPressed_4; }
	inline void set_ButtonPressed_4(UnityEvent_t2581268647 * value)
	{
		___ButtonPressed_4 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonPressed_4), value);
	}

	inline static int32_t get_offset_of_PressedOpacity_5() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ___PressedOpacity_5)); }
	inline float get_PressedOpacity_5() const { return ___PressedOpacity_5; }
	inline float* get_address_of_PressedOpacity_5() { return &___PressedOpacity_5; }
	inline void set_PressedOpacity_5(float value)
	{
		___PressedOpacity_5 = value;
	}

	inline static int32_t get_offset_of_MouseMode_6() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ___MouseMode_6)); }
	inline bool get_MouseMode_6() const { return ___MouseMode_6; }
	inline bool* get_address_of_MouseMode_6() { return &___MouseMode_6; }
	inline void set_MouseMode_6(bool value)
	{
		___MouseMode_6 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ___U3CCurrentStateU3Ek__BackingField_7)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_7() const { return ___U3CCurrentStateU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_7() { return &___U3CCurrentStateU3Ek__BackingField_7; }
	inline void set_U3CCurrentStateU3Ek__BackingField_7(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of__zonePressed_8() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ____zonePressed_8)); }
	inline bool get__zonePressed_8() const { return ____zonePressed_8; }
	inline bool* get_address_of__zonePressed_8() { return &____zonePressed_8; }
	inline void set__zonePressed_8(bool value)
	{
		____zonePressed_8 = value;
	}

	inline static int32_t get_offset_of__canvasGroup_9() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ____canvasGroup_9)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_9() const { return ____canvasGroup_9; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_9() { return &____canvasGroup_9; }
	inline void set__canvasGroup_9(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_9 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_9), value);
	}

	inline static int32_t get_offset_of__initialOpacity_10() { return static_cast<int32_t>(offsetof(MMTouchButton_t1318972791, ____initialOpacity_10)); }
	inline float get__initialOpacity_10() const { return ____initialOpacity_10; }
	inline float* get_address_of__initialOpacity_10() { return &____initialOpacity_10; }
	inline void set__initialOpacity_10(float value)
	{
		____initialOpacity_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMTOUCHBUTTON_T1318972791_H
#ifndef MMACHIEVEMENTDISPLAYER_T237347715_H
#define MMACHIEVEMENTDISPLAYER_T237347715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAchievementDisplayer
struct  MMAchievementDisplayer_t237347715  : public MonoBehaviour_t3962482529
{
public:
	// MoreMountains.Tools.MMAchievementDisplayItem MoreMountains.Tools.MMAchievementDisplayer::AchievementDisplayPrefab
	MMAchievementDisplayItem_t1206168000 * ___AchievementDisplayPrefab_2;
	// System.Single MoreMountains.Tools.MMAchievementDisplayer::AchievementDisplayDuration
	float ___AchievementDisplayDuration_3;
	// System.Single MoreMountains.Tools.MMAchievementDisplayer::AchievementFadeDuration
	float ___AchievementFadeDuration_4;
	// UnityEngine.WaitForSeconds MoreMountains.Tools.MMAchievementDisplayer::_achievementFadeOutWFS
	WaitForSeconds_t1699091251 * ____achievementFadeOutWFS_5;

public:
	inline static int32_t get_offset_of_AchievementDisplayPrefab_2() { return static_cast<int32_t>(offsetof(MMAchievementDisplayer_t237347715, ___AchievementDisplayPrefab_2)); }
	inline MMAchievementDisplayItem_t1206168000 * get_AchievementDisplayPrefab_2() const { return ___AchievementDisplayPrefab_2; }
	inline MMAchievementDisplayItem_t1206168000 ** get_address_of_AchievementDisplayPrefab_2() { return &___AchievementDisplayPrefab_2; }
	inline void set_AchievementDisplayPrefab_2(MMAchievementDisplayItem_t1206168000 * value)
	{
		___AchievementDisplayPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___AchievementDisplayPrefab_2), value);
	}

	inline static int32_t get_offset_of_AchievementDisplayDuration_3() { return static_cast<int32_t>(offsetof(MMAchievementDisplayer_t237347715, ___AchievementDisplayDuration_3)); }
	inline float get_AchievementDisplayDuration_3() const { return ___AchievementDisplayDuration_3; }
	inline float* get_address_of_AchievementDisplayDuration_3() { return &___AchievementDisplayDuration_3; }
	inline void set_AchievementDisplayDuration_3(float value)
	{
		___AchievementDisplayDuration_3 = value;
	}

	inline static int32_t get_offset_of_AchievementFadeDuration_4() { return static_cast<int32_t>(offsetof(MMAchievementDisplayer_t237347715, ___AchievementFadeDuration_4)); }
	inline float get_AchievementFadeDuration_4() const { return ___AchievementFadeDuration_4; }
	inline float* get_address_of_AchievementFadeDuration_4() { return &___AchievementFadeDuration_4; }
	inline void set_AchievementFadeDuration_4(float value)
	{
		___AchievementFadeDuration_4 = value;
	}

	inline static int32_t get_offset_of__achievementFadeOutWFS_5() { return static_cast<int32_t>(offsetof(MMAchievementDisplayer_t237347715, ____achievementFadeOutWFS_5)); }
	inline WaitForSeconds_t1699091251 * get__achievementFadeOutWFS_5() const { return ____achievementFadeOutWFS_5; }
	inline WaitForSeconds_t1699091251 ** get_address_of__achievementFadeOutWFS_5() { return &____achievementFadeOutWFS_5; }
	inline void set__achievementFadeOutWFS_5(WaitForSeconds_t1699091251 * value)
	{
		____achievementFadeOutWFS_5 = value;
		Il2CppCodeGenWriteBarrier((&____achievementFadeOutWFS_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMACHIEVEMENTDISPLAYER_T237347715_H
#ifndef WIGGLE_T1237850810_H
#define WIGGLE_T1237850810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.Wiggle
struct  Wiggle_t1237850810  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoreMountains.Tools.Wiggle::PositionFrequencyMin
	float ___PositionFrequencyMin_2;
	// System.Single MoreMountains.Tools.Wiggle::PositionFrequencyMax
	float ___PositionFrequencyMax_3;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::PositionAmplitudeMin
	Vector3_t3722313464  ___PositionAmplitudeMin_4;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::PositionAmplitudeMax
	Vector3_t3722313464  ___PositionAmplitudeMax_5;
	// System.Single MoreMountains.Tools.Wiggle::RotationFrequencyMin
	float ___RotationFrequencyMin_6;
	// System.Single MoreMountains.Tools.Wiggle::RotationFrequencyMax
	float ___RotationFrequencyMax_7;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::RotationAmplitudeMin
	Vector3_t3722313464  ___RotationAmplitudeMin_8;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::RotationAmplitudeMax
	Vector3_t3722313464  ___RotationAmplitudeMax_9;
	// System.Single MoreMountains.Tools.Wiggle::ScaleFrequencyMin
	float ___ScaleFrequencyMin_10;
	// System.Single MoreMountains.Tools.Wiggle::ScaleFrequencyMax
	float ___ScaleFrequencyMax_11;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::ScaleAmplitudeMin
	Vector3_t3722313464  ___ScaleAmplitudeMin_12;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::ScaleAmplitudeMax
	Vector3_t3722313464  ___ScaleAmplitudeMax_13;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::_startPosition
	Vector3_t3722313464  ____startPosition_14;
	// UnityEngine.Quaternion MoreMountains.Tools.Wiggle::_startRotation
	Quaternion_t2301928331  ____startRotation_15;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::_startScale
	Vector3_t3722313464  ____startScale_16;
	// System.Single MoreMountains.Tools.Wiggle::_randomPositionFrequency
	float ____randomPositionFrequency_17;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::_randomPositionAmplitude
	Vector3_t3722313464  ____randomPositionAmplitude_18;
	// System.Single MoreMountains.Tools.Wiggle::_randomRotationFrequency
	float ____randomRotationFrequency_19;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::_randomRotationAmplitude
	Vector3_t3722313464  ____randomRotationAmplitude_20;
	// System.Single MoreMountains.Tools.Wiggle::_randomScaleFrequency
	float ____randomScaleFrequency_21;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::_randomScaleAmplitude
	Vector3_t3722313464  ____randomScaleAmplitude_22;
	// System.Single MoreMountains.Tools.Wiggle::_positionTimer
	float ____positionTimer_23;
	// System.Single MoreMountains.Tools.Wiggle::_rotationTimer
	float ____rotationTimer_24;
	// System.Single MoreMountains.Tools.Wiggle::_scaleTimer
	float ____scaleTimer_25;
	// System.Single MoreMountains.Tools.Wiggle::_positionT
	float ____positionT_26;
	// System.Single MoreMountains.Tools.Wiggle::_rotationT
	float ____rotationT_27;
	// System.Single MoreMountains.Tools.Wiggle::_scaleT
	float ____scaleT_28;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::_newPosition
	Vector3_t3722313464  ____newPosition_29;
	// UnityEngine.Quaternion MoreMountains.Tools.Wiggle::_newRotation
	Quaternion_t2301928331  ____newRotation_30;
	// UnityEngine.Vector3 MoreMountains.Tools.Wiggle::_newScale
	Vector3_t3722313464  ____newScale_31;

public:
	inline static int32_t get_offset_of_PositionFrequencyMin_2() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___PositionFrequencyMin_2)); }
	inline float get_PositionFrequencyMin_2() const { return ___PositionFrequencyMin_2; }
	inline float* get_address_of_PositionFrequencyMin_2() { return &___PositionFrequencyMin_2; }
	inline void set_PositionFrequencyMin_2(float value)
	{
		___PositionFrequencyMin_2 = value;
	}

	inline static int32_t get_offset_of_PositionFrequencyMax_3() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___PositionFrequencyMax_3)); }
	inline float get_PositionFrequencyMax_3() const { return ___PositionFrequencyMax_3; }
	inline float* get_address_of_PositionFrequencyMax_3() { return &___PositionFrequencyMax_3; }
	inline void set_PositionFrequencyMax_3(float value)
	{
		___PositionFrequencyMax_3 = value;
	}

	inline static int32_t get_offset_of_PositionAmplitudeMin_4() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___PositionAmplitudeMin_4)); }
	inline Vector3_t3722313464  get_PositionAmplitudeMin_4() const { return ___PositionAmplitudeMin_4; }
	inline Vector3_t3722313464 * get_address_of_PositionAmplitudeMin_4() { return &___PositionAmplitudeMin_4; }
	inline void set_PositionAmplitudeMin_4(Vector3_t3722313464  value)
	{
		___PositionAmplitudeMin_4 = value;
	}

	inline static int32_t get_offset_of_PositionAmplitudeMax_5() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___PositionAmplitudeMax_5)); }
	inline Vector3_t3722313464  get_PositionAmplitudeMax_5() const { return ___PositionAmplitudeMax_5; }
	inline Vector3_t3722313464 * get_address_of_PositionAmplitudeMax_5() { return &___PositionAmplitudeMax_5; }
	inline void set_PositionAmplitudeMax_5(Vector3_t3722313464  value)
	{
		___PositionAmplitudeMax_5 = value;
	}

	inline static int32_t get_offset_of_RotationFrequencyMin_6() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___RotationFrequencyMin_6)); }
	inline float get_RotationFrequencyMin_6() const { return ___RotationFrequencyMin_6; }
	inline float* get_address_of_RotationFrequencyMin_6() { return &___RotationFrequencyMin_6; }
	inline void set_RotationFrequencyMin_6(float value)
	{
		___RotationFrequencyMin_6 = value;
	}

	inline static int32_t get_offset_of_RotationFrequencyMax_7() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___RotationFrequencyMax_7)); }
	inline float get_RotationFrequencyMax_7() const { return ___RotationFrequencyMax_7; }
	inline float* get_address_of_RotationFrequencyMax_7() { return &___RotationFrequencyMax_7; }
	inline void set_RotationFrequencyMax_7(float value)
	{
		___RotationFrequencyMax_7 = value;
	}

	inline static int32_t get_offset_of_RotationAmplitudeMin_8() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___RotationAmplitudeMin_8)); }
	inline Vector3_t3722313464  get_RotationAmplitudeMin_8() const { return ___RotationAmplitudeMin_8; }
	inline Vector3_t3722313464 * get_address_of_RotationAmplitudeMin_8() { return &___RotationAmplitudeMin_8; }
	inline void set_RotationAmplitudeMin_8(Vector3_t3722313464  value)
	{
		___RotationAmplitudeMin_8 = value;
	}

	inline static int32_t get_offset_of_RotationAmplitudeMax_9() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___RotationAmplitudeMax_9)); }
	inline Vector3_t3722313464  get_RotationAmplitudeMax_9() const { return ___RotationAmplitudeMax_9; }
	inline Vector3_t3722313464 * get_address_of_RotationAmplitudeMax_9() { return &___RotationAmplitudeMax_9; }
	inline void set_RotationAmplitudeMax_9(Vector3_t3722313464  value)
	{
		___RotationAmplitudeMax_9 = value;
	}

	inline static int32_t get_offset_of_ScaleFrequencyMin_10() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___ScaleFrequencyMin_10)); }
	inline float get_ScaleFrequencyMin_10() const { return ___ScaleFrequencyMin_10; }
	inline float* get_address_of_ScaleFrequencyMin_10() { return &___ScaleFrequencyMin_10; }
	inline void set_ScaleFrequencyMin_10(float value)
	{
		___ScaleFrequencyMin_10 = value;
	}

	inline static int32_t get_offset_of_ScaleFrequencyMax_11() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___ScaleFrequencyMax_11)); }
	inline float get_ScaleFrequencyMax_11() const { return ___ScaleFrequencyMax_11; }
	inline float* get_address_of_ScaleFrequencyMax_11() { return &___ScaleFrequencyMax_11; }
	inline void set_ScaleFrequencyMax_11(float value)
	{
		___ScaleFrequencyMax_11 = value;
	}

	inline static int32_t get_offset_of_ScaleAmplitudeMin_12() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___ScaleAmplitudeMin_12)); }
	inline Vector3_t3722313464  get_ScaleAmplitudeMin_12() const { return ___ScaleAmplitudeMin_12; }
	inline Vector3_t3722313464 * get_address_of_ScaleAmplitudeMin_12() { return &___ScaleAmplitudeMin_12; }
	inline void set_ScaleAmplitudeMin_12(Vector3_t3722313464  value)
	{
		___ScaleAmplitudeMin_12 = value;
	}

	inline static int32_t get_offset_of_ScaleAmplitudeMax_13() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ___ScaleAmplitudeMax_13)); }
	inline Vector3_t3722313464  get_ScaleAmplitudeMax_13() const { return ___ScaleAmplitudeMax_13; }
	inline Vector3_t3722313464 * get_address_of_ScaleAmplitudeMax_13() { return &___ScaleAmplitudeMax_13; }
	inline void set_ScaleAmplitudeMax_13(Vector3_t3722313464  value)
	{
		___ScaleAmplitudeMax_13 = value;
	}

	inline static int32_t get_offset_of__startPosition_14() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____startPosition_14)); }
	inline Vector3_t3722313464  get__startPosition_14() const { return ____startPosition_14; }
	inline Vector3_t3722313464 * get_address_of__startPosition_14() { return &____startPosition_14; }
	inline void set__startPosition_14(Vector3_t3722313464  value)
	{
		____startPosition_14 = value;
	}

	inline static int32_t get_offset_of__startRotation_15() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____startRotation_15)); }
	inline Quaternion_t2301928331  get__startRotation_15() const { return ____startRotation_15; }
	inline Quaternion_t2301928331 * get_address_of__startRotation_15() { return &____startRotation_15; }
	inline void set__startRotation_15(Quaternion_t2301928331  value)
	{
		____startRotation_15 = value;
	}

	inline static int32_t get_offset_of__startScale_16() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____startScale_16)); }
	inline Vector3_t3722313464  get__startScale_16() const { return ____startScale_16; }
	inline Vector3_t3722313464 * get_address_of__startScale_16() { return &____startScale_16; }
	inline void set__startScale_16(Vector3_t3722313464  value)
	{
		____startScale_16 = value;
	}

	inline static int32_t get_offset_of__randomPositionFrequency_17() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____randomPositionFrequency_17)); }
	inline float get__randomPositionFrequency_17() const { return ____randomPositionFrequency_17; }
	inline float* get_address_of__randomPositionFrequency_17() { return &____randomPositionFrequency_17; }
	inline void set__randomPositionFrequency_17(float value)
	{
		____randomPositionFrequency_17 = value;
	}

	inline static int32_t get_offset_of__randomPositionAmplitude_18() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____randomPositionAmplitude_18)); }
	inline Vector3_t3722313464  get__randomPositionAmplitude_18() const { return ____randomPositionAmplitude_18; }
	inline Vector3_t3722313464 * get_address_of__randomPositionAmplitude_18() { return &____randomPositionAmplitude_18; }
	inline void set__randomPositionAmplitude_18(Vector3_t3722313464  value)
	{
		____randomPositionAmplitude_18 = value;
	}

	inline static int32_t get_offset_of__randomRotationFrequency_19() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____randomRotationFrequency_19)); }
	inline float get__randomRotationFrequency_19() const { return ____randomRotationFrequency_19; }
	inline float* get_address_of__randomRotationFrequency_19() { return &____randomRotationFrequency_19; }
	inline void set__randomRotationFrequency_19(float value)
	{
		____randomRotationFrequency_19 = value;
	}

	inline static int32_t get_offset_of__randomRotationAmplitude_20() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____randomRotationAmplitude_20)); }
	inline Vector3_t3722313464  get__randomRotationAmplitude_20() const { return ____randomRotationAmplitude_20; }
	inline Vector3_t3722313464 * get_address_of__randomRotationAmplitude_20() { return &____randomRotationAmplitude_20; }
	inline void set__randomRotationAmplitude_20(Vector3_t3722313464  value)
	{
		____randomRotationAmplitude_20 = value;
	}

	inline static int32_t get_offset_of__randomScaleFrequency_21() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____randomScaleFrequency_21)); }
	inline float get__randomScaleFrequency_21() const { return ____randomScaleFrequency_21; }
	inline float* get_address_of__randomScaleFrequency_21() { return &____randomScaleFrequency_21; }
	inline void set__randomScaleFrequency_21(float value)
	{
		____randomScaleFrequency_21 = value;
	}

	inline static int32_t get_offset_of__randomScaleAmplitude_22() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____randomScaleAmplitude_22)); }
	inline Vector3_t3722313464  get__randomScaleAmplitude_22() const { return ____randomScaleAmplitude_22; }
	inline Vector3_t3722313464 * get_address_of__randomScaleAmplitude_22() { return &____randomScaleAmplitude_22; }
	inline void set__randomScaleAmplitude_22(Vector3_t3722313464  value)
	{
		____randomScaleAmplitude_22 = value;
	}

	inline static int32_t get_offset_of__positionTimer_23() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____positionTimer_23)); }
	inline float get__positionTimer_23() const { return ____positionTimer_23; }
	inline float* get_address_of__positionTimer_23() { return &____positionTimer_23; }
	inline void set__positionTimer_23(float value)
	{
		____positionTimer_23 = value;
	}

	inline static int32_t get_offset_of__rotationTimer_24() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____rotationTimer_24)); }
	inline float get__rotationTimer_24() const { return ____rotationTimer_24; }
	inline float* get_address_of__rotationTimer_24() { return &____rotationTimer_24; }
	inline void set__rotationTimer_24(float value)
	{
		____rotationTimer_24 = value;
	}

	inline static int32_t get_offset_of__scaleTimer_25() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____scaleTimer_25)); }
	inline float get__scaleTimer_25() const { return ____scaleTimer_25; }
	inline float* get_address_of__scaleTimer_25() { return &____scaleTimer_25; }
	inline void set__scaleTimer_25(float value)
	{
		____scaleTimer_25 = value;
	}

	inline static int32_t get_offset_of__positionT_26() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____positionT_26)); }
	inline float get__positionT_26() const { return ____positionT_26; }
	inline float* get_address_of__positionT_26() { return &____positionT_26; }
	inline void set__positionT_26(float value)
	{
		____positionT_26 = value;
	}

	inline static int32_t get_offset_of__rotationT_27() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____rotationT_27)); }
	inline float get__rotationT_27() const { return ____rotationT_27; }
	inline float* get_address_of__rotationT_27() { return &____rotationT_27; }
	inline void set__rotationT_27(float value)
	{
		____rotationT_27 = value;
	}

	inline static int32_t get_offset_of__scaleT_28() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____scaleT_28)); }
	inline float get__scaleT_28() const { return ____scaleT_28; }
	inline float* get_address_of__scaleT_28() { return &____scaleT_28; }
	inline void set__scaleT_28(float value)
	{
		____scaleT_28 = value;
	}

	inline static int32_t get_offset_of__newPosition_29() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____newPosition_29)); }
	inline Vector3_t3722313464  get__newPosition_29() const { return ____newPosition_29; }
	inline Vector3_t3722313464 * get_address_of__newPosition_29() { return &____newPosition_29; }
	inline void set__newPosition_29(Vector3_t3722313464  value)
	{
		____newPosition_29 = value;
	}

	inline static int32_t get_offset_of__newRotation_30() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____newRotation_30)); }
	inline Quaternion_t2301928331  get__newRotation_30() const { return ____newRotation_30; }
	inline Quaternion_t2301928331 * get_address_of__newRotation_30() { return &____newRotation_30; }
	inline void set__newRotation_30(Quaternion_t2301928331  value)
	{
		____newRotation_30 = value;
	}

	inline static int32_t get_offset_of__newScale_31() { return static_cast<int32_t>(offsetof(Wiggle_t1237850810, ____newScale_31)); }
	inline Vector3_t3722313464  get__newScale_31() const { return ____newScale_31; }
	inline Vector3_t3722313464 * get_address_of__newScale_31() { return &____newScale_31; }
	inline void set__newScale_31(Vector3_t3722313464  value)
	{
		____newScale_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIGGLE_T1237850810_H
#ifndef MMTOUCHCONTROLS_T1924831068_H
#define MMTOUCHCONTROLS_T1924831068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTouchControls
struct  MMTouchControls_t1924831068  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MoreMountains.Tools.MMTouchControls::AutoMobileDetection
	bool ___AutoMobileDetection_2;
	// MoreMountains.Tools.MMTouchControls/InputForcedMode MoreMountains.Tools.MMTouchControls::ForcedMode
	int32_t ___ForcedMode_3;
	// System.Boolean MoreMountains.Tools.MMTouchControls::<IsMobile>k__BackingField
	bool ___U3CIsMobileU3Ek__BackingField_4;
	// UnityEngine.CanvasGroup MoreMountains.Tools.MMTouchControls::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_5;
	// System.Single MoreMountains.Tools.MMTouchControls::_initialMobileControlsAlpha
	float ____initialMobileControlsAlpha_6;

public:
	inline static int32_t get_offset_of_AutoMobileDetection_2() { return static_cast<int32_t>(offsetof(MMTouchControls_t1924831068, ___AutoMobileDetection_2)); }
	inline bool get_AutoMobileDetection_2() const { return ___AutoMobileDetection_2; }
	inline bool* get_address_of_AutoMobileDetection_2() { return &___AutoMobileDetection_2; }
	inline void set_AutoMobileDetection_2(bool value)
	{
		___AutoMobileDetection_2 = value;
	}

	inline static int32_t get_offset_of_ForcedMode_3() { return static_cast<int32_t>(offsetof(MMTouchControls_t1924831068, ___ForcedMode_3)); }
	inline int32_t get_ForcedMode_3() const { return ___ForcedMode_3; }
	inline int32_t* get_address_of_ForcedMode_3() { return &___ForcedMode_3; }
	inline void set_ForcedMode_3(int32_t value)
	{
		___ForcedMode_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsMobileU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MMTouchControls_t1924831068, ___U3CIsMobileU3Ek__BackingField_4)); }
	inline bool get_U3CIsMobileU3Ek__BackingField_4() const { return ___U3CIsMobileU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsMobileU3Ek__BackingField_4() { return &___U3CIsMobileU3Ek__BackingField_4; }
	inline void set_U3CIsMobileU3Ek__BackingField_4(bool value)
	{
		___U3CIsMobileU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__canvasGroup_5() { return static_cast<int32_t>(offsetof(MMTouchControls_t1924831068, ____canvasGroup_5)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_5() const { return ____canvasGroup_5; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_5() { return &____canvasGroup_5; }
	inline void set__canvasGroup_5(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_5), value);
	}

	inline static int32_t get_offset_of__initialMobileControlsAlpha_6() { return static_cast<int32_t>(offsetof(MMTouchControls_t1924831068, ____initialMobileControlsAlpha_6)); }
	inline float get__initialMobileControlsAlpha_6() const { return ____initialMobileControlsAlpha_6; }
	inline float* get_address_of__initialMobileControlsAlpha_6() { return &____initialMobileControlsAlpha_6; }
	inline void set__initialMobileControlsAlpha_6(float value)
	{
		____initialMobileControlsAlpha_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMTOUCHCONTROLS_T1924831068_H
#ifndef MMTOUCHAXIS_T2941413966_H
#define MMTOUCHAXIS_T2941413966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTouchAxis
struct  MMTouchAxis_t2941413966  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMTouchAxis::AxisPressedFirstTime
	UnityEvent_t2581268647 * ___AxisPressedFirstTime_2;
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMTouchAxis::AxisReleased
	UnityEvent_t2581268647 * ___AxisReleased_3;
	// MoreMountains.Tools.AxisEvent MoreMountains.Tools.MMTouchAxis::AxisPressed
	AxisEvent_t2672331441 * ___AxisPressed_4;
	// System.Single MoreMountains.Tools.MMTouchAxis::PressedOpacity
	float ___PressedOpacity_5;
	// System.Single MoreMountains.Tools.MMTouchAxis::AxisValue
	float ___AxisValue_6;
	// System.Boolean MoreMountains.Tools.MMTouchAxis::MouseMode
	bool ___MouseMode_7;
	// MoreMountains.Tools.MMTouchAxis/ButtonStates MoreMountains.Tools.MMTouchAxis::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_8;
	// UnityEngine.CanvasGroup MoreMountains.Tools.MMTouchAxis::_canvasGroup
	CanvasGroup_t4083511760 * ____canvasGroup_9;
	// System.Single MoreMountains.Tools.MMTouchAxis::_initialOpacity
	float ____initialOpacity_10;

public:
	inline static int32_t get_offset_of_AxisPressedFirstTime_2() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ___AxisPressedFirstTime_2)); }
	inline UnityEvent_t2581268647 * get_AxisPressedFirstTime_2() const { return ___AxisPressedFirstTime_2; }
	inline UnityEvent_t2581268647 ** get_address_of_AxisPressedFirstTime_2() { return &___AxisPressedFirstTime_2; }
	inline void set_AxisPressedFirstTime_2(UnityEvent_t2581268647 * value)
	{
		___AxisPressedFirstTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___AxisPressedFirstTime_2), value);
	}

	inline static int32_t get_offset_of_AxisReleased_3() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ___AxisReleased_3)); }
	inline UnityEvent_t2581268647 * get_AxisReleased_3() const { return ___AxisReleased_3; }
	inline UnityEvent_t2581268647 ** get_address_of_AxisReleased_3() { return &___AxisReleased_3; }
	inline void set_AxisReleased_3(UnityEvent_t2581268647 * value)
	{
		___AxisReleased_3 = value;
		Il2CppCodeGenWriteBarrier((&___AxisReleased_3), value);
	}

	inline static int32_t get_offset_of_AxisPressed_4() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ___AxisPressed_4)); }
	inline AxisEvent_t2672331441 * get_AxisPressed_4() const { return ___AxisPressed_4; }
	inline AxisEvent_t2672331441 ** get_address_of_AxisPressed_4() { return &___AxisPressed_4; }
	inline void set_AxisPressed_4(AxisEvent_t2672331441 * value)
	{
		___AxisPressed_4 = value;
		Il2CppCodeGenWriteBarrier((&___AxisPressed_4), value);
	}

	inline static int32_t get_offset_of_PressedOpacity_5() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ___PressedOpacity_5)); }
	inline float get_PressedOpacity_5() const { return ___PressedOpacity_5; }
	inline float* get_address_of_PressedOpacity_5() { return &___PressedOpacity_5; }
	inline void set_PressedOpacity_5(float value)
	{
		___PressedOpacity_5 = value;
	}

	inline static int32_t get_offset_of_AxisValue_6() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ___AxisValue_6)); }
	inline float get_AxisValue_6() const { return ___AxisValue_6; }
	inline float* get_address_of_AxisValue_6() { return &___AxisValue_6; }
	inline void set_AxisValue_6(float value)
	{
		___AxisValue_6 = value;
	}

	inline static int32_t get_offset_of_MouseMode_7() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ___MouseMode_7)); }
	inline bool get_MouseMode_7() const { return ___MouseMode_7; }
	inline bool* get_address_of_MouseMode_7() { return &___MouseMode_7; }
	inline void set_MouseMode_7(bool value)
	{
		___MouseMode_7 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ___U3CCurrentStateU3Ek__BackingField_8)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_8() const { return ___U3CCurrentStateU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_8() { return &___U3CCurrentStateU3Ek__BackingField_8; }
	inline void set_U3CCurrentStateU3Ek__BackingField_8(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of__canvasGroup_9() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ____canvasGroup_9)); }
	inline CanvasGroup_t4083511760 * get__canvasGroup_9() const { return ____canvasGroup_9; }
	inline CanvasGroup_t4083511760 ** get_address_of__canvasGroup_9() { return &____canvasGroup_9; }
	inline void set__canvasGroup_9(CanvasGroup_t4083511760 * value)
	{
		____canvasGroup_9 = value;
		Il2CppCodeGenWriteBarrier((&____canvasGroup_9), value);
	}

	inline static int32_t get_offset_of__initialOpacity_10() { return static_cast<int32_t>(offsetof(MMTouchAxis_t2941413966, ____initialOpacity_10)); }
	inline float get__initialOpacity_10() const { return ____initialOpacity_10; }
	inline float* get_address_of__initialOpacity_10() { return &____initialOpacity_10; }
	inline void set__initialOpacity_10(float value)
	{
		____initialOpacity_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMTOUCHAXIS_T2941413966_H
#ifndef MMACHIEVEMENTDISPLAYITEM_T1206168000_H
#define MMACHIEVEMENTDISPLAYITEM_T1206168000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAchievementDisplayItem
struct  MMAchievementDisplayItem_t1206168000  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image MoreMountains.Tools.MMAchievementDisplayItem::BackgroundLocked
	Image_t2670269651 * ___BackgroundLocked_2;
	// UnityEngine.UI.Image MoreMountains.Tools.MMAchievementDisplayItem::BackgroundUnlocked
	Image_t2670269651 * ___BackgroundUnlocked_3;
	// UnityEngine.UI.Image MoreMountains.Tools.MMAchievementDisplayItem::Icon
	Image_t2670269651 * ___Icon_4;
	// UnityEngine.UI.Text MoreMountains.Tools.MMAchievementDisplayItem::Title
	Text_t1901882714 * ___Title_5;
	// UnityEngine.UI.Text MoreMountains.Tools.MMAchievementDisplayItem::Description
	Text_t1901882714 * ___Description_6;
	// ProgressBar MoreMountains.Tools.MMAchievementDisplayItem::ProgressBarDisplay
	ProgressBar_t1074804577 * ___ProgressBarDisplay_7;

public:
	inline static int32_t get_offset_of_BackgroundLocked_2() { return static_cast<int32_t>(offsetof(MMAchievementDisplayItem_t1206168000, ___BackgroundLocked_2)); }
	inline Image_t2670269651 * get_BackgroundLocked_2() const { return ___BackgroundLocked_2; }
	inline Image_t2670269651 ** get_address_of_BackgroundLocked_2() { return &___BackgroundLocked_2; }
	inline void set_BackgroundLocked_2(Image_t2670269651 * value)
	{
		___BackgroundLocked_2 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundLocked_2), value);
	}

	inline static int32_t get_offset_of_BackgroundUnlocked_3() { return static_cast<int32_t>(offsetof(MMAchievementDisplayItem_t1206168000, ___BackgroundUnlocked_3)); }
	inline Image_t2670269651 * get_BackgroundUnlocked_3() const { return ___BackgroundUnlocked_3; }
	inline Image_t2670269651 ** get_address_of_BackgroundUnlocked_3() { return &___BackgroundUnlocked_3; }
	inline void set_BackgroundUnlocked_3(Image_t2670269651 * value)
	{
		___BackgroundUnlocked_3 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundUnlocked_3), value);
	}

	inline static int32_t get_offset_of_Icon_4() { return static_cast<int32_t>(offsetof(MMAchievementDisplayItem_t1206168000, ___Icon_4)); }
	inline Image_t2670269651 * get_Icon_4() const { return ___Icon_4; }
	inline Image_t2670269651 ** get_address_of_Icon_4() { return &___Icon_4; }
	inline void set_Icon_4(Image_t2670269651 * value)
	{
		___Icon_4 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_4), value);
	}

	inline static int32_t get_offset_of_Title_5() { return static_cast<int32_t>(offsetof(MMAchievementDisplayItem_t1206168000, ___Title_5)); }
	inline Text_t1901882714 * get_Title_5() const { return ___Title_5; }
	inline Text_t1901882714 ** get_address_of_Title_5() { return &___Title_5; }
	inline void set_Title_5(Text_t1901882714 * value)
	{
		___Title_5 = value;
		Il2CppCodeGenWriteBarrier((&___Title_5), value);
	}

	inline static int32_t get_offset_of_Description_6() { return static_cast<int32_t>(offsetof(MMAchievementDisplayItem_t1206168000, ___Description_6)); }
	inline Text_t1901882714 * get_Description_6() const { return ___Description_6; }
	inline Text_t1901882714 ** get_address_of_Description_6() { return &___Description_6; }
	inline void set_Description_6(Text_t1901882714 * value)
	{
		___Description_6 = value;
		Il2CppCodeGenWriteBarrier((&___Description_6), value);
	}

	inline static int32_t get_offset_of_ProgressBarDisplay_7() { return static_cast<int32_t>(offsetof(MMAchievementDisplayItem_t1206168000, ___ProgressBarDisplay_7)); }
	inline ProgressBar_t1074804577 * get_ProgressBarDisplay_7() const { return ___ProgressBarDisplay_7; }
	inline ProgressBar_t1074804577 ** get_address_of_ProgressBarDisplay_7() { return &___ProgressBarDisplay_7; }
	inline void set_ProgressBarDisplay_7(ProgressBar_t1074804577 * value)
	{
		___ProgressBarDisplay_7 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressBarDisplay_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMACHIEVEMENTDISPLAYITEM_T1206168000_H
#ifndef TIMEDSPAWNER_T239769417_H
#define TIMEDSPAWNER_T239769417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.TimedSpawner
struct  TimedSpawner_t239769417  : public Spawner_t3426627155
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.TimedSpawner::MinSpawnTime
	float ___MinSpawnTime_12;
	// System.Single MoreMountains.InfiniteRunnerEngine.TimedSpawner::MaxSpawnTime
	float ___MaxSpawnTime_13;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.TimedSpawner::MinPosition
	Vector3_t3722313464  ___MinPosition_14;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.TimedSpawner::MaxPosition
	Vector3_t3722313464  ___MaxPosition_15;

public:
	inline static int32_t get_offset_of_MinSpawnTime_12() { return static_cast<int32_t>(offsetof(TimedSpawner_t239769417, ___MinSpawnTime_12)); }
	inline float get_MinSpawnTime_12() const { return ___MinSpawnTime_12; }
	inline float* get_address_of_MinSpawnTime_12() { return &___MinSpawnTime_12; }
	inline void set_MinSpawnTime_12(float value)
	{
		___MinSpawnTime_12 = value;
	}

	inline static int32_t get_offset_of_MaxSpawnTime_13() { return static_cast<int32_t>(offsetof(TimedSpawner_t239769417, ___MaxSpawnTime_13)); }
	inline float get_MaxSpawnTime_13() const { return ___MaxSpawnTime_13; }
	inline float* get_address_of_MaxSpawnTime_13() { return &___MaxSpawnTime_13; }
	inline void set_MaxSpawnTime_13(float value)
	{
		___MaxSpawnTime_13 = value;
	}

	inline static int32_t get_offset_of_MinPosition_14() { return static_cast<int32_t>(offsetof(TimedSpawner_t239769417, ___MinPosition_14)); }
	inline Vector3_t3722313464  get_MinPosition_14() const { return ___MinPosition_14; }
	inline Vector3_t3722313464 * get_address_of_MinPosition_14() { return &___MinPosition_14; }
	inline void set_MinPosition_14(Vector3_t3722313464  value)
	{
		___MinPosition_14 = value;
	}

	inline static int32_t get_offset_of_MaxPosition_15() { return static_cast<int32_t>(offsetof(TimedSpawner_t239769417, ___MaxPosition_15)); }
	inline Vector3_t3722313464  get_MaxPosition_15() const { return ___MaxPosition_15; }
	inline Vector3_t3722313464 * get_address_of_MaxPosition_15() { return &___MaxPosition_15; }
	inline void set_MaxPosition_15(Vector3_t3722313464  value)
	{
		___MaxPosition_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDSPAWNER_T239769417_H
#ifndef PARALLAX_T3067417749_H
#define PARALLAX_T3067417749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.Parallax
struct  Parallax_t3067417749  : public ObjectBounds_t107927819
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.Parallax::ParallaxSpeed
	float ___ParallaxSpeed_4;
	// MoreMountains.InfiniteRunnerEngine.Parallax/PossibleDirections MoreMountains.InfiniteRunnerEngine.Parallax::ParallaxDirection
	int32_t ___ParallaxDirection_5;
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.Parallax::_clone
	GameObject_t1113636619 * ____clone_6;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Parallax::_movement
	Vector3_t3722313464  ____movement_7;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Parallax::_initialPosition
	Vector3_t3722313464  ____initialPosition_8;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Parallax::_newPosition
	Vector3_t3722313464  ____newPosition_9;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.Parallax::_direction
	Vector3_t3722313464  ____direction_10;
	// System.Single MoreMountains.InfiniteRunnerEngine.Parallax::_width
	float ____width_11;

public:
	inline static int32_t get_offset_of_ParallaxSpeed_4() { return static_cast<int32_t>(offsetof(Parallax_t3067417749, ___ParallaxSpeed_4)); }
	inline float get_ParallaxSpeed_4() const { return ___ParallaxSpeed_4; }
	inline float* get_address_of_ParallaxSpeed_4() { return &___ParallaxSpeed_4; }
	inline void set_ParallaxSpeed_4(float value)
	{
		___ParallaxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_ParallaxDirection_5() { return static_cast<int32_t>(offsetof(Parallax_t3067417749, ___ParallaxDirection_5)); }
	inline int32_t get_ParallaxDirection_5() const { return ___ParallaxDirection_5; }
	inline int32_t* get_address_of_ParallaxDirection_5() { return &___ParallaxDirection_5; }
	inline void set_ParallaxDirection_5(int32_t value)
	{
		___ParallaxDirection_5 = value;
	}

	inline static int32_t get_offset_of__clone_6() { return static_cast<int32_t>(offsetof(Parallax_t3067417749, ____clone_6)); }
	inline GameObject_t1113636619 * get__clone_6() const { return ____clone_6; }
	inline GameObject_t1113636619 ** get_address_of__clone_6() { return &____clone_6; }
	inline void set__clone_6(GameObject_t1113636619 * value)
	{
		____clone_6 = value;
		Il2CppCodeGenWriteBarrier((&____clone_6), value);
	}

	inline static int32_t get_offset_of__movement_7() { return static_cast<int32_t>(offsetof(Parallax_t3067417749, ____movement_7)); }
	inline Vector3_t3722313464  get__movement_7() const { return ____movement_7; }
	inline Vector3_t3722313464 * get_address_of__movement_7() { return &____movement_7; }
	inline void set__movement_7(Vector3_t3722313464  value)
	{
		____movement_7 = value;
	}

	inline static int32_t get_offset_of__initialPosition_8() { return static_cast<int32_t>(offsetof(Parallax_t3067417749, ____initialPosition_8)); }
	inline Vector3_t3722313464  get__initialPosition_8() const { return ____initialPosition_8; }
	inline Vector3_t3722313464 * get_address_of__initialPosition_8() { return &____initialPosition_8; }
	inline void set__initialPosition_8(Vector3_t3722313464  value)
	{
		____initialPosition_8 = value;
	}

	inline static int32_t get_offset_of__newPosition_9() { return static_cast<int32_t>(offsetof(Parallax_t3067417749, ____newPosition_9)); }
	inline Vector3_t3722313464  get__newPosition_9() const { return ____newPosition_9; }
	inline Vector3_t3722313464 * get_address_of__newPosition_9() { return &____newPosition_9; }
	inline void set__newPosition_9(Vector3_t3722313464  value)
	{
		____newPosition_9 = value;
	}

	inline static int32_t get_offset_of__direction_10() { return static_cast<int32_t>(offsetof(Parallax_t3067417749, ____direction_10)); }
	inline Vector3_t3722313464  get__direction_10() const { return ____direction_10; }
	inline Vector3_t3722313464 * get_address_of__direction_10() { return &____direction_10; }
	inline void set__direction_10(Vector3_t3722313464  value)
	{
		____direction_10 = value;
	}

	inline static int32_t get_offset_of__width_11() { return static_cast<int32_t>(offsetof(Parallax_t3067417749, ____width_11)); }
	inline float get__width_11() const { return ____width_11; }
	inline float* get_address_of__width_11() { return &____width_11; }
	inline void set__width_11(float value)
	{
		____width_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARALLAX_T3067417749_H
#ifndef SOUNDMANAGER_T1622142048_H
#define SOUNDMANAGER_T1622142048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.SoundManager
struct  SoundManager_t1622142048  : public PersistentSingleton_1_t2668794452
{
public:
	// System.Boolean MoreMountains.InfiniteRunnerEngine.SoundManager::MusicOn
	bool ___MusicOn_4;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.SoundManager::SfxOn
	bool ___SfxOn_5;
	// System.Single MoreMountains.InfiniteRunnerEngine.SoundManager::MusicVolume
	float ___MusicVolume_6;
	// System.Single MoreMountains.InfiniteRunnerEngine.SoundManager::SfxVolume
	float ___SfxVolume_7;
	// UnityEngine.AudioSource MoreMountains.InfiniteRunnerEngine.SoundManager::_backgroundMusic
	AudioSource_t3935305588 * ____backgroundMusic_8;

public:
	inline static int32_t get_offset_of_MusicOn_4() { return static_cast<int32_t>(offsetof(SoundManager_t1622142048, ___MusicOn_4)); }
	inline bool get_MusicOn_4() const { return ___MusicOn_4; }
	inline bool* get_address_of_MusicOn_4() { return &___MusicOn_4; }
	inline void set_MusicOn_4(bool value)
	{
		___MusicOn_4 = value;
	}

	inline static int32_t get_offset_of_SfxOn_5() { return static_cast<int32_t>(offsetof(SoundManager_t1622142048, ___SfxOn_5)); }
	inline bool get_SfxOn_5() const { return ___SfxOn_5; }
	inline bool* get_address_of_SfxOn_5() { return &___SfxOn_5; }
	inline void set_SfxOn_5(bool value)
	{
		___SfxOn_5 = value;
	}

	inline static int32_t get_offset_of_MusicVolume_6() { return static_cast<int32_t>(offsetof(SoundManager_t1622142048, ___MusicVolume_6)); }
	inline float get_MusicVolume_6() const { return ___MusicVolume_6; }
	inline float* get_address_of_MusicVolume_6() { return &___MusicVolume_6; }
	inline void set_MusicVolume_6(float value)
	{
		___MusicVolume_6 = value;
	}

	inline static int32_t get_offset_of_SfxVolume_7() { return static_cast<int32_t>(offsetof(SoundManager_t1622142048, ___SfxVolume_7)); }
	inline float get_SfxVolume_7() const { return ___SfxVolume_7; }
	inline float* get_address_of_SfxVolume_7() { return &___SfxVolume_7; }
	inline void set_SfxVolume_7(float value)
	{
		___SfxVolume_7 = value;
	}

	inline static int32_t get_offset_of__backgroundMusic_8() { return static_cast<int32_t>(offsetof(SoundManager_t1622142048, ____backgroundMusic_8)); }
	inline AudioSource_t3935305588 * get__backgroundMusic_8() const { return ____backgroundMusic_8; }
	inline AudioSource_t3935305588 ** get_address_of__backgroundMusic_8() { return &____backgroundMusic_8; }
	inline void set__backgroundMusic_8(AudioSource_t3935305588 * value)
	{
		____backgroundMusic_8 = value;
		Il2CppCodeGenWriteBarrier((&____backgroundMusic_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T1622142048_H
#ifndef LEVELMANAGER_T1115010890_H
#define LEVELMANAGER_T1115010890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LevelManager
struct  LevelManager_t1115010890  : public Singleton_1_t3964434931
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::<Speed>k__BackingField
	float ___U3CSpeedU3Ek__BackingField_3;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::<DistanceTraveled>k__BackingField
	float ___U3CDistanceTraveledU3Ek__BackingField_4;
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.LevelManager::StartingPosition
	GameObject_t1113636619 * ___StartingPosition_5;
	// System.Collections.Generic.List`1<MoreMountains.InfiniteRunnerEngine.PlayableCharacter> MoreMountains.InfiniteRunnerEngine.LevelManager::PlayableCharacters
	List_1_t4210981935 * ___PlayableCharacters_6;
	// System.Collections.Generic.List`1<MoreMountains.InfiniteRunnerEngine.PlayableCharacter> MoreMountains.InfiniteRunnerEngine.LevelManager::<CurrentPlayableCharacters>k__BackingField
	List_1_t4210981935 * ___U3CCurrentPlayableCharactersU3Ek__BackingField_7;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::DistanceBetweenCharacters
	float ___DistanceBetweenCharacters_8;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::<RunningTime>k__BackingField
	float ___U3CRunningTimeU3Ek__BackingField_9;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::PointsPerSecond
	float ___PointsPerSecond_10;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::levelTime
	float ___levelTime_11;
	// System.String MoreMountains.InfiniteRunnerEngine.LevelManager::InstructionsText
	String_t* ___InstructionsText_12;
	// UnityEngine.Bounds MoreMountains.InfiniteRunnerEngine.LevelManager::RecycleBounds
	Bounds_t2266837910  ___RecycleBounds_13;
	// UnityEngine.Bounds MoreMountains.InfiniteRunnerEngine.LevelManager::DeathBounds
	Bounds_t2266837910  ___DeathBounds_14;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::InitialSpeed
	float ___InitialSpeed_15;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::MaximumSpeed
	float ___MaximumSpeed_16;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::SpeedAcceleration
	float ___SpeedAcceleration_17;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::IntroFadeDuration
	float ___IntroFadeDuration_18;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::OutroFadeDuration
	float ___OutroFadeDuration_19;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LevelManager::StartCountdown
	int32_t ___StartCountdown_20;
	// System.String MoreMountains.InfiniteRunnerEngine.LevelManager::StartText
	String_t* ___StartText_21;
	// MoreMountains.InfiniteRunnerEngine.LevelManager/Controls MoreMountains.InfiniteRunnerEngine.LevelManager::ControlScheme
	int32_t ___ControlScheme_22;
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.LevelManager::LifeLostExplosion
	GameObject_t1113636619 * ___LifeLostExplosion_23;
	// System.DateTime MoreMountains.InfiniteRunnerEngine.LevelManager::_started
	DateTime_t3738529785  ____started_24;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::_savedPoints
	float ____savedPoints_25;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::_recycleX
	float ____recycleX_26;
	// UnityEngine.Bounds MoreMountains.InfiniteRunnerEngine.LevelManager::_tmpRecycleBounds
	Bounds_t2266837910  ____tmpRecycleBounds_27;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.LevelManager::_temporarySpeedFactorActive
	bool ____temporarySpeedFactorActive_28;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::_temporarySpeedFactor
	float ____temporarySpeedFactor_29;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::_temporarySpeedFactorRemainingTime
	float ____temporarySpeedFactorRemainingTime_30;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::_temporarySavedSpeed
	float ____temporarySavedSpeed_31;
	// System.Single MoreMountains.InfiniteRunnerEngine.LevelManager::_timeCounter
	float ____timeCounter_32;

public:
	inline static int32_t get_offset_of_U3CSpeedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___U3CSpeedU3Ek__BackingField_3)); }
	inline float get_U3CSpeedU3Ek__BackingField_3() const { return ___U3CSpeedU3Ek__BackingField_3; }
	inline float* get_address_of_U3CSpeedU3Ek__BackingField_3() { return &___U3CSpeedU3Ek__BackingField_3; }
	inline void set_U3CSpeedU3Ek__BackingField_3(float value)
	{
		___U3CSpeedU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceTraveledU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___U3CDistanceTraveledU3Ek__BackingField_4)); }
	inline float get_U3CDistanceTraveledU3Ek__BackingField_4() const { return ___U3CDistanceTraveledU3Ek__BackingField_4; }
	inline float* get_address_of_U3CDistanceTraveledU3Ek__BackingField_4() { return &___U3CDistanceTraveledU3Ek__BackingField_4; }
	inline void set_U3CDistanceTraveledU3Ek__BackingField_4(float value)
	{
		___U3CDistanceTraveledU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_StartingPosition_5() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___StartingPosition_5)); }
	inline GameObject_t1113636619 * get_StartingPosition_5() const { return ___StartingPosition_5; }
	inline GameObject_t1113636619 ** get_address_of_StartingPosition_5() { return &___StartingPosition_5; }
	inline void set_StartingPosition_5(GameObject_t1113636619 * value)
	{
		___StartingPosition_5 = value;
		Il2CppCodeGenWriteBarrier((&___StartingPosition_5), value);
	}

	inline static int32_t get_offset_of_PlayableCharacters_6() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___PlayableCharacters_6)); }
	inline List_1_t4210981935 * get_PlayableCharacters_6() const { return ___PlayableCharacters_6; }
	inline List_1_t4210981935 ** get_address_of_PlayableCharacters_6() { return &___PlayableCharacters_6; }
	inline void set_PlayableCharacters_6(List_1_t4210981935 * value)
	{
		___PlayableCharacters_6 = value;
		Il2CppCodeGenWriteBarrier((&___PlayableCharacters_6), value);
	}

	inline static int32_t get_offset_of_U3CCurrentPlayableCharactersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___U3CCurrentPlayableCharactersU3Ek__BackingField_7)); }
	inline List_1_t4210981935 * get_U3CCurrentPlayableCharactersU3Ek__BackingField_7() const { return ___U3CCurrentPlayableCharactersU3Ek__BackingField_7; }
	inline List_1_t4210981935 ** get_address_of_U3CCurrentPlayableCharactersU3Ek__BackingField_7() { return &___U3CCurrentPlayableCharactersU3Ek__BackingField_7; }
	inline void set_U3CCurrentPlayableCharactersU3Ek__BackingField_7(List_1_t4210981935 * value)
	{
		___U3CCurrentPlayableCharactersU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentPlayableCharactersU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_DistanceBetweenCharacters_8() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___DistanceBetweenCharacters_8)); }
	inline float get_DistanceBetweenCharacters_8() const { return ___DistanceBetweenCharacters_8; }
	inline float* get_address_of_DistanceBetweenCharacters_8() { return &___DistanceBetweenCharacters_8; }
	inline void set_DistanceBetweenCharacters_8(float value)
	{
		___DistanceBetweenCharacters_8 = value;
	}

	inline static int32_t get_offset_of_U3CRunningTimeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___U3CRunningTimeU3Ek__BackingField_9)); }
	inline float get_U3CRunningTimeU3Ek__BackingField_9() const { return ___U3CRunningTimeU3Ek__BackingField_9; }
	inline float* get_address_of_U3CRunningTimeU3Ek__BackingField_9() { return &___U3CRunningTimeU3Ek__BackingField_9; }
	inline void set_U3CRunningTimeU3Ek__BackingField_9(float value)
	{
		___U3CRunningTimeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_PointsPerSecond_10() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___PointsPerSecond_10)); }
	inline float get_PointsPerSecond_10() const { return ___PointsPerSecond_10; }
	inline float* get_address_of_PointsPerSecond_10() { return &___PointsPerSecond_10; }
	inline void set_PointsPerSecond_10(float value)
	{
		___PointsPerSecond_10 = value;
	}

	inline static int32_t get_offset_of_levelTime_11() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___levelTime_11)); }
	inline float get_levelTime_11() const { return ___levelTime_11; }
	inline float* get_address_of_levelTime_11() { return &___levelTime_11; }
	inline void set_levelTime_11(float value)
	{
		___levelTime_11 = value;
	}

	inline static int32_t get_offset_of_InstructionsText_12() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___InstructionsText_12)); }
	inline String_t* get_InstructionsText_12() const { return ___InstructionsText_12; }
	inline String_t** get_address_of_InstructionsText_12() { return &___InstructionsText_12; }
	inline void set_InstructionsText_12(String_t* value)
	{
		___InstructionsText_12 = value;
		Il2CppCodeGenWriteBarrier((&___InstructionsText_12), value);
	}

	inline static int32_t get_offset_of_RecycleBounds_13() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___RecycleBounds_13)); }
	inline Bounds_t2266837910  get_RecycleBounds_13() const { return ___RecycleBounds_13; }
	inline Bounds_t2266837910 * get_address_of_RecycleBounds_13() { return &___RecycleBounds_13; }
	inline void set_RecycleBounds_13(Bounds_t2266837910  value)
	{
		___RecycleBounds_13 = value;
	}

	inline static int32_t get_offset_of_DeathBounds_14() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___DeathBounds_14)); }
	inline Bounds_t2266837910  get_DeathBounds_14() const { return ___DeathBounds_14; }
	inline Bounds_t2266837910 * get_address_of_DeathBounds_14() { return &___DeathBounds_14; }
	inline void set_DeathBounds_14(Bounds_t2266837910  value)
	{
		___DeathBounds_14 = value;
	}

	inline static int32_t get_offset_of_InitialSpeed_15() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___InitialSpeed_15)); }
	inline float get_InitialSpeed_15() const { return ___InitialSpeed_15; }
	inline float* get_address_of_InitialSpeed_15() { return &___InitialSpeed_15; }
	inline void set_InitialSpeed_15(float value)
	{
		___InitialSpeed_15 = value;
	}

	inline static int32_t get_offset_of_MaximumSpeed_16() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___MaximumSpeed_16)); }
	inline float get_MaximumSpeed_16() const { return ___MaximumSpeed_16; }
	inline float* get_address_of_MaximumSpeed_16() { return &___MaximumSpeed_16; }
	inline void set_MaximumSpeed_16(float value)
	{
		___MaximumSpeed_16 = value;
	}

	inline static int32_t get_offset_of_SpeedAcceleration_17() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___SpeedAcceleration_17)); }
	inline float get_SpeedAcceleration_17() const { return ___SpeedAcceleration_17; }
	inline float* get_address_of_SpeedAcceleration_17() { return &___SpeedAcceleration_17; }
	inline void set_SpeedAcceleration_17(float value)
	{
		___SpeedAcceleration_17 = value;
	}

	inline static int32_t get_offset_of_IntroFadeDuration_18() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___IntroFadeDuration_18)); }
	inline float get_IntroFadeDuration_18() const { return ___IntroFadeDuration_18; }
	inline float* get_address_of_IntroFadeDuration_18() { return &___IntroFadeDuration_18; }
	inline void set_IntroFadeDuration_18(float value)
	{
		___IntroFadeDuration_18 = value;
	}

	inline static int32_t get_offset_of_OutroFadeDuration_19() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___OutroFadeDuration_19)); }
	inline float get_OutroFadeDuration_19() const { return ___OutroFadeDuration_19; }
	inline float* get_address_of_OutroFadeDuration_19() { return &___OutroFadeDuration_19; }
	inline void set_OutroFadeDuration_19(float value)
	{
		___OutroFadeDuration_19 = value;
	}

	inline static int32_t get_offset_of_StartCountdown_20() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___StartCountdown_20)); }
	inline int32_t get_StartCountdown_20() const { return ___StartCountdown_20; }
	inline int32_t* get_address_of_StartCountdown_20() { return &___StartCountdown_20; }
	inline void set_StartCountdown_20(int32_t value)
	{
		___StartCountdown_20 = value;
	}

	inline static int32_t get_offset_of_StartText_21() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___StartText_21)); }
	inline String_t* get_StartText_21() const { return ___StartText_21; }
	inline String_t** get_address_of_StartText_21() { return &___StartText_21; }
	inline void set_StartText_21(String_t* value)
	{
		___StartText_21 = value;
		Il2CppCodeGenWriteBarrier((&___StartText_21), value);
	}

	inline static int32_t get_offset_of_ControlScheme_22() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___ControlScheme_22)); }
	inline int32_t get_ControlScheme_22() const { return ___ControlScheme_22; }
	inline int32_t* get_address_of_ControlScheme_22() { return &___ControlScheme_22; }
	inline void set_ControlScheme_22(int32_t value)
	{
		___ControlScheme_22 = value;
	}

	inline static int32_t get_offset_of_LifeLostExplosion_23() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ___LifeLostExplosion_23)); }
	inline GameObject_t1113636619 * get_LifeLostExplosion_23() const { return ___LifeLostExplosion_23; }
	inline GameObject_t1113636619 ** get_address_of_LifeLostExplosion_23() { return &___LifeLostExplosion_23; }
	inline void set_LifeLostExplosion_23(GameObject_t1113636619 * value)
	{
		___LifeLostExplosion_23 = value;
		Il2CppCodeGenWriteBarrier((&___LifeLostExplosion_23), value);
	}

	inline static int32_t get_offset_of__started_24() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____started_24)); }
	inline DateTime_t3738529785  get__started_24() const { return ____started_24; }
	inline DateTime_t3738529785 * get_address_of__started_24() { return &____started_24; }
	inline void set__started_24(DateTime_t3738529785  value)
	{
		____started_24 = value;
	}

	inline static int32_t get_offset_of__savedPoints_25() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____savedPoints_25)); }
	inline float get__savedPoints_25() const { return ____savedPoints_25; }
	inline float* get_address_of__savedPoints_25() { return &____savedPoints_25; }
	inline void set__savedPoints_25(float value)
	{
		____savedPoints_25 = value;
	}

	inline static int32_t get_offset_of__recycleX_26() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____recycleX_26)); }
	inline float get__recycleX_26() const { return ____recycleX_26; }
	inline float* get_address_of__recycleX_26() { return &____recycleX_26; }
	inline void set__recycleX_26(float value)
	{
		____recycleX_26 = value;
	}

	inline static int32_t get_offset_of__tmpRecycleBounds_27() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____tmpRecycleBounds_27)); }
	inline Bounds_t2266837910  get__tmpRecycleBounds_27() const { return ____tmpRecycleBounds_27; }
	inline Bounds_t2266837910 * get_address_of__tmpRecycleBounds_27() { return &____tmpRecycleBounds_27; }
	inline void set__tmpRecycleBounds_27(Bounds_t2266837910  value)
	{
		____tmpRecycleBounds_27 = value;
	}

	inline static int32_t get_offset_of__temporarySpeedFactorActive_28() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____temporarySpeedFactorActive_28)); }
	inline bool get__temporarySpeedFactorActive_28() const { return ____temporarySpeedFactorActive_28; }
	inline bool* get_address_of__temporarySpeedFactorActive_28() { return &____temporarySpeedFactorActive_28; }
	inline void set__temporarySpeedFactorActive_28(bool value)
	{
		____temporarySpeedFactorActive_28 = value;
	}

	inline static int32_t get_offset_of__temporarySpeedFactor_29() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____temporarySpeedFactor_29)); }
	inline float get__temporarySpeedFactor_29() const { return ____temporarySpeedFactor_29; }
	inline float* get_address_of__temporarySpeedFactor_29() { return &____temporarySpeedFactor_29; }
	inline void set__temporarySpeedFactor_29(float value)
	{
		____temporarySpeedFactor_29 = value;
	}

	inline static int32_t get_offset_of__temporarySpeedFactorRemainingTime_30() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____temporarySpeedFactorRemainingTime_30)); }
	inline float get__temporarySpeedFactorRemainingTime_30() const { return ____temporarySpeedFactorRemainingTime_30; }
	inline float* get_address_of__temporarySpeedFactorRemainingTime_30() { return &____temporarySpeedFactorRemainingTime_30; }
	inline void set__temporarySpeedFactorRemainingTime_30(float value)
	{
		____temporarySpeedFactorRemainingTime_30 = value;
	}

	inline static int32_t get_offset_of__temporarySavedSpeed_31() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____temporarySavedSpeed_31)); }
	inline float get__temporarySavedSpeed_31() const { return ____temporarySavedSpeed_31; }
	inline float* get_address_of__temporarySavedSpeed_31() { return &____temporarySavedSpeed_31; }
	inline void set__temporarySavedSpeed_31(float value)
	{
		____temporarySavedSpeed_31 = value;
	}

	inline static int32_t get_offset_of__timeCounter_32() { return static_cast<int32_t>(offsetof(LevelManager_t1115010890, ____timeCounter_32)); }
	inline float get__timeCounter_32() const { return ____timeCounter_32; }
	inline float* get_address_of__timeCounter_32() { return &____timeCounter_32; }
	inline void set__timeCounter_32(float value)
	{
		____timeCounter_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELMANAGER_T1115010890_H
#ifndef INPUTMANAGER_T3769144452_H
#define INPUTMANAGER_T3769144452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.InputManager
struct  InputManager_t3769144452  : public Singleton_1_t2323601197
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMANAGER_T3769144452_H
#ifndef GUIMANAGER_T1233035445_H
#define GUIMANAGER_T1233035445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.GUIManager
struct  GUIManager_t1233035445  : public Singleton_1_t4082459486
{
public:
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.GUIManager::PauseScreen
	GameObject_t1113636619 * ___PauseScreen_3;
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.GUIManager::GameOverScreen
	GameObject_t1113636619 * ___GameOverScreen_4;
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.GUIManager::HeartsContainer
	GameObject_t1113636619 * ___HeartsContainer_5;
	// UnityEngine.UI.Text MoreMountains.InfiniteRunnerEngine.GUIManager::PointsText
	Text_t1901882714 * ___PointsText_6;
	// UnityEngine.UI.Text MoreMountains.InfiniteRunnerEngine.GUIManager::LevelText
	Text_t1901882714 * ___LevelText_7;
	// UnityEngine.UI.Text MoreMountains.InfiniteRunnerEngine.GUIManager::CountdownText
	Text_t1901882714 * ___CountdownText_8;
	// UnityEngine.UI.Image MoreMountains.InfiniteRunnerEngine.GUIManager::Fader
	Image_t2670269651 * ___Fader_9;
	// UnityEngine.UI.Image MoreMountains.InfiniteRunnerEngine.GUIManager::Timer
	Image_t2670269651 * ___Timer_10;

public:
	inline static int32_t get_offset_of_PauseScreen_3() { return static_cast<int32_t>(offsetof(GUIManager_t1233035445, ___PauseScreen_3)); }
	inline GameObject_t1113636619 * get_PauseScreen_3() const { return ___PauseScreen_3; }
	inline GameObject_t1113636619 ** get_address_of_PauseScreen_3() { return &___PauseScreen_3; }
	inline void set_PauseScreen_3(GameObject_t1113636619 * value)
	{
		___PauseScreen_3 = value;
		Il2CppCodeGenWriteBarrier((&___PauseScreen_3), value);
	}

	inline static int32_t get_offset_of_GameOverScreen_4() { return static_cast<int32_t>(offsetof(GUIManager_t1233035445, ___GameOverScreen_4)); }
	inline GameObject_t1113636619 * get_GameOverScreen_4() const { return ___GameOverScreen_4; }
	inline GameObject_t1113636619 ** get_address_of_GameOverScreen_4() { return &___GameOverScreen_4; }
	inline void set_GameOverScreen_4(GameObject_t1113636619 * value)
	{
		___GameOverScreen_4 = value;
		Il2CppCodeGenWriteBarrier((&___GameOverScreen_4), value);
	}

	inline static int32_t get_offset_of_HeartsContainer_5() { return static_cast<int32_t>(offsetof(GUIManager_t1233035445, ___HeartsContainer_5)); }
	inline GameObject_t1113636619 * get_HeartsContainer_5() const { return ___HeartsContainer_5; }
	inline GameObject_t1113636619 ** get_address_of_HeartsContainer_5() { return &___HeartsContainer_5; }
	inline void set_HeartsContainer_5(GameObject_t1113636619 * value)
	{
		___HeartsContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___HeartsContainer_5), value);
	}

	inline static int32_t get_offset_of_PointsText_6() { return static_cast<int32_t>(offsetof(GUIManager_t1233035445, ___PointsText_6)); }
	inline Text_t1901882714 * get_PointsText_6() const { return ___PointsText_6; }
	inline Text_t1901882714 ** get_address_of_PointsText_6() { return &___PointsText_6; }
	inline void set_PointsText_6(Text_t1901882714 * value)
	{
		___PointsText_6 = value;
		Il2CppCodeGenWriteBarrier((&___PointsText_6), value);
	}

	inline static int32_t get_offset_of_LevelText_7() { return static_cast<int32_t>(offsetof(GUIManager_t1233035445, ___LevelText_7)); }
	inline Text_t1901882714 * get_LevelText_7() const { return ___LevelText_7; }
	inline Text_t1901882714 ** get_address_of_LevelText_7() { return &___LevelText_7; }
	inline void set_LevelText_7(Text_t1901882714 * value)
	{
		___LevelText_7 = value;
		Il2CppCodeGenWriteBarrier((&___LevelText_7), value);
	}

	inline static int32_t get_offset_of_CountdownText_8() { return static_cast<int32_t>(offsetof(GUIManager_t1233035445, ___CountdownText_8)); }
	inline Text_t1901882714 * get_CountdownText_8() const { return ___CountdownText_8; }
	inline Text_t1901882714 ** get_address_of_CountdownText_8() { return &___CountdownText_8; }
	inline void set_CountdownText_8(Text_t1901882714 * value)
	{
		___CountdownText_8 = value;
		Il2CppCodeGenWriteBarrier((&___CountdownText_8), value);
	}

	inline static int32_t get_offset_of_Fader_9() { return static_cast<int32_t>(offsetof(GUIManager_t1233035445, ___Fader_9)); }
	inline Image_t2670269651 * get_Fader_9() const { return ___Fader_9; }
	inline Image_t2670269651 ** get_address_of_Fader_9() { return &___Fader_9; }
	inline void set_Fader_9(Image_t2670269651 * value)
	{
		___Fader_9 = value;
		Il2CppCodeGenWriteBarrier((&___Fader_9), value);
	}

	inline static int32_t get_offset_of_Timer_10() { return static_cast<int32_t>(offsetof(GUIManager_t1233035445, ___Timer_10)); }
	inline Image_t2670269651 * get_Timer_10() const { return ___Timer_10; }
	inline Image_t2670269651 ** get_address_of_Timer_10() { return &___Timer_10; }
	inline void set_Timer_10(Image_t2670269651 * value)
	{
		___Timer_10 = value;
		Il2CppCodeGenWriteBarrier((&___Timer_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIMANAGER_T1233035445_H
#ifndef DISTANCESPAWNER_T2744940552_H
#define DISTANCESPAWNER_T2744940552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.DistanceSpawner
struct  DistanceSpawner_t2744940552  : public Spawner_t3426627155
{
public:
	// MoreMountains.InfiniteRunnerEngine.DistanceSpawner/GapOrigins MoreMountains.InfiniteRunnerEngine.DistanceSpawner::GapOrigin
	int32_t ___GapOrigin_12;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.DistanceSpawner::MinimumGap
	Vector3_t3722313464  ___MinimumGap_13;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.DistanceSpawner::MaximumGap
	Vector3_t3722313464  ___MaximumGap_14;
	// System.Single MoreMountains.InfiniteRunnerEngine.DistanceSpawner::MinimumYClamp
	float ___MinimumYClamp_15;
	// System.Single MoreMountains.InfiniteRunnerEngine.DistanceSpawner::MaximumYClamp
	float ___MaximumYClamp_16;
	// System.Single MoreMountains.InfiniteRunnerEngine.DistanceSpawner::MinimumZClamp
	float ___MinimumZClamp_17;
	// System.Single MoreMountains.InfiniteRunnerEngine.DistanceSpawner::MaximumZClamp
	float ___MaximumZClamp_18;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.DistanceSpawner::SpawnRotatedToDirection
	bool ___SpawnRotatedToDirection_19;
	// UnityEngine.Transform MoreMountains.InfiniteRunnerEngine.DistanceSpawner::_lastSpawnedTransform
	Transform_t3600365921 * ____lastSpawnedTransform_20;
	// System.Single MoreMountains.InfiniteRunnerEngine.DistanceSpawner::_nextSpawnDistance
	float ____nextSpawnDistance_21;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.DistanceSpawner::_gap
	Vector3_t3722313464  ____gap_22;

public:
	inline static int32_t get_offset_of_GapOrigin_12() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ___GapOrigin_12)); }
	inline int32_t get_GapOrigin_12() const { return ___GapOrigin_12; }
	inline int32_t* get_address_of_GapOrigin_12() { return &___GapOrigin_12; }
	inline void set_GapOrigin_12(int32_t value)
	{
		___GapOrigin_12 = value;
	}

	inline static int32_t get_offset_of_MinimumGap_13() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ___MinimumGap_13)); }
	inline Vector3_t3722313464  get_MinimumGap_13() const { return ___MinimumGap_13; }
	inline Vector3_t3722313464 * get_address_of_MinimumGap_13() { return &___MinimumGap_13; }
	inline void set_MinimumGap_13(Vector3_t3722313464  value)
	{
		___MinimumGap_13 = value;
	}

	inline static int32_t get_offset_of_MaximumGap_14() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ___MaximumGap_14)); }
	inline Vector3_t3722313464  get_MaximumGap_14() const { return ___MaximumGap_14; }
	inline Vector3_t3722313464 * get_address_of_MaximumGap_14() { return &___MaximumGap_14; }
	inline void set_MaximumGap_14(Vector3_t3722313464  value)
	{
		___MaximumGap_14 = value;
	}

	inline static int32_t get_offset_of_MinimumYClamp_15() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ___MinimumYClamp_15)); }
	inline float get_MinimumYClamp_15() const { return ___MinimumYClamp_15; }
	inline float* get_address_of_MinimumYClamp_15() { return &___MinimumYClamp_15; }
	inline void set_MinimumYClamp_15(float value)
	{
		___MinimumYClamp_15 = value;
	}

	inline static int32_t get_offset_of_MaximumYClamp_16() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ___MaximumYClamp_16)); }
	inline float get_MaximumYClamp_16() const { return ___MaximumYClamp_16; }
	inline float* get_address_of_MaximumYClamp_16() { return &___MaximumYClamp_16; }
	inline void set_MaximumYClamp_16(float value)
	{
		___MaximumYClamp_16 = value;
	}

	inline static int32_t get_offset_of_MinimumZClamp_17() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ___MinimumZClamp_17)); }
	inline float get_MinimumZClamp_17() const { return ___MinimumZClamp_17; }
	inline float* get_address_of_MinimumZClamp_17() { return &___MinimumZClamp_17; }
	inline void set_MinimumZClamp_17(float value)
	{
		___MinimumZClamp_17 = value;
	}

	inline static int32_t get_offset_of_MaximumZClamp_18() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ___MaximumZClamp_18)); }
	inline float get_MaximumZClamp_18() const { return ___MaximumZClamp_18; }
	inline float* get_address_of_MaximumZClamp_18() { return &___MaximumZClamp_18; }
	inline void set_MaximumZClamp_18(float value)
	{
		___MaximumZClamp_18 = value;
	}

	inline static int32_t get_offset_of_SpawnRotatedToDirection_19() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ___SpawnRotatedToDirection_19)); }
	inline bool get_SpawnRotatedToDirection_19() const { return ___SpawnRotatedToDirection_19; }
	inline bool* get_address_of_SpawnRotatedToDirection_19() { return &___SpawnRotatedToDirection_19; }
	inline void set_SpawnRotatedToDirection_19(bool value)
	{
		___SpawnRotatedToDirection_19 = value;
	}

	inline static int32_t get_offset_of__lastSpawnedTransform_20() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ____lastSpawnedTransform_20)); }
	inline Transform_t3600365921 * get__lastSpawnedTransform_20() const { return ____lastSpawnedTransform_20; }
	inline Transform_t3600365921 ** get_address_of__lastSpawnedTransform_20() { return &____lastSpawnedTransform_20; }
	inline void set__lastSpawnedTransform_20(Transform_t3600365921 * value)
	{
		____lastSpawnedTransform_20 = value;
		Il2CppCodeGenWriteBarrier((&____lastSpawnedTransform_20), value);
	}

	inline static int32_t get_offset_of__nextSpawnDistance_21() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ____nextSpawnDistance_21)); }
	inline float get__nextSpawnDistance_21() const { return ____nextSpawnDistance_21; }
	inline float* get_address_of__nextSpawnDistance_21() { return &____nextSpawnDistance_21; }
	inline void set__nextSpawnDistance_21(float value)
	{
		____nextSpawnDistance_21 = value;
	}

	inline static int32_t get_offset_of__gap_22() { return static_cast<int32_t>(offsetof(DistanceSpawner_t2744940552, ____gap_22)); }
	inline Vector3_t3722313464  get__gap_22() const { return ____gap_22; }
	inline Vector3_t3722313464 * get_address_of__gap_22() { return &____gap_22; }
	inline void set__gap_22(Vector3_t3722313464  value)
	{
		____gap_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTANCESPAWNER_T2744940552_H
#ifndef EXAMPLESCENARIO_T2931033563_H
#define EXAMPLESCENARIO_T2931033563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.ExampleScenario
struct  ExampleScenario_t2931033563  : public ScenarioManager_t2117133370
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESCENARIO_T2931033563_H
#ifndef LANERUNNER_T2456357099_H
#define LANERUNNER_T2456357099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LaneRunner
struct  LaneRunner_t2456357099  : public PlayableCharacter_t2738907193
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.LaneRunner::LaneWidth
	float ___LaneWidth_16;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LaneRunner::NumberOfLanes
	int32_t ___NumberOfLanes_17;
	// System.Single MoreMountains.InfiniteRunnerEngine.LaneRunner::ChangingLaneSpeed
	float ___ChangingLaneSpeed_18;
	// UnityEngine.GameObject MoreMountains.InfiniteRunnerEngine.LaneRunner::Explosion
	GameObject_t1113636619 * ___Explosion_19;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.LaneRunner::_currentLane
	int32_t ____currentLane_20;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.LaneRunner::_isMoving
	bool ____isMoving_21;

public:
	inline static int32_t get_offset_of_LaneWidth_16() { return static_cast<int32_t>(offsetof(LaneRunner_t2456357099, ___LaneWidth_16)); }
	inline float get_LaneWidth_16() const { return ___LaneWidth_16; }
	inline float* get_address_of_LaneWidth_16() { return &___LaneWidth_16; }
	inline void set_LaneWidth_16(float value)
	{
		___LaneWidth_16 = value;
	}

	inline static int32_t get_offset_of_NumberOfLanes_17() { return static_cast<int32_t>(offsetof(LaneRunner_t2456357099, ___NumberOfLanes_17)); }
	inline int32_t get_NumberOfLanes_17() const { return ___NumberOfLanes_17; }
	inline int32_t* get_address_of_NumberOfLanes_17() { return &___NumberOfLanes_17; }
	inline void set_NumberOfLanes_17(int32_t value)
	{
		___NumberOfLanes_17 = value;
	}

	inline static int32_t get_offset_of_ChangingLaneSpeed_18() { return static_cast<int32_t>(offsetof(LaneRunner_t2456357099, ___ChangingLaneSpeed_18)); }
	inline float get_ChangingLaneSpeed_18() const { return ___ChangingLaneSpeed_18; }
	inline float* get_address_of_ChangingLaneSpeed_18() { return &___ChangingLaneSpeed_18; }
	inline void set_ChangingLaneSpeed_18(float value)
	{
		___ChangingLaneSpeed_18 = value;
	}

	inline static int32_t get_offset_of_Explosion_19() { return static_cast<int32_t>(offsetof(LaneRunner_t2456357099, ___Explosion_19)); }
	inline GameObject_t1113636619 * get_Explosion_19() const { return ___Explosion_19; }
	inline GameObject_t1113636619 ** get_address_of_Explosion_19() { return &___Explosion_19; }
	inline void set_Explosion_19(GameObject_t1113636619 * value)
	{
		___Explosion_19 = value;
		Il2CppCodeGenWriteBarrier((&___Explosion_19), value);
	}

	inline static int32_t get_offset_of__currentLane_20() { return static_cast<int32_t>(offsetof(LaneRunner_t2456357099, ____currentLane_20)); }
	inline int32_t get__currentLane_20() const { return ____currentLane_20; }
	inline int32_t* get_address_of__currentLane_20() { return &____currentLane_20; }
	inline void set__currentLane_20(int32_t value)
	{
		____currentLane_20 = value;
	}

	inline static int32_t get_offset_of__isMoving_21() { return static_cast<int32_t>(offsetof(LaneRunner_t2456357099, ____isMoving_21)); }
	inline bool get__isMoving_21() const { return ____isMoving_21; }
	inline bool* get_address_of__isMoving_21() { return &____isMoving_21; }
	inline void set__isMoving_21(bool value)
	{
		____isMoving_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANERUNNER_T2456357099_H
#ifndef MMTOUCHDYNAMICJOYSTICK_T1438601146_H
#define MMTOUCHDYNAMICJOYSTICK_T1438601146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTouchDynamicJoystick
struct  MMTouchDynamicJoystick_t1438601146  : public MMTouchJoystick_t2762864105
{
public:
	// UnityEngine.Sprite MoreMountains.Tools.MMTouchDynamicJoystick::JoystickKnobImage
	Sprite_t280657092 * ___JoystickKnobImage_17;
	// System.Boolean MoreMountains.Tools.MMTouchDynamicJoystick::RestorePosition
	bool ___RestorePosition_18;
	// UnityEngine.Vector3 MoreMountains.Tools.MMTouchDynamicJoystick::_initialPosition
	Vector3_t3722313464  ____initialPosition_19;
	// UnityEngine.Vector3 MoreMountains.Tools.MMTouchDynamicJoystick::_newPosition
	Vector3_t3722313464  ____newPosition_20;
	// UnityEngine.CanvasGroup MoreMountains.Tools.MMTouchDynamicJoystick::_knobCanvasGroup
	CanvasGroup_t4083511760 * ____knobCanvasGroup_21;

public:
	inline static int32_t get_offset_of_JoystickKnobImage_17() { return static_cast<int32_t>(offsetof(MMTouchDynamicJoystick_t1438601146, ___JoystickKnobImage_17)); }
	inline Sprite_t280657092 * get_JoystickKnobImage_17() const { return ___JoystickKnobImage_17; }
	inline Sprite_t280657092 ** get_address_of_JoystickKnobImage_17() { return &___JoystickKnobImage_17; }
	inline void set_JoystickKnobImage_17(Sprite_t280657092 * value)
	{
		___JoystickKnobImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___JoystickKnobImage_17), value);
	}

	inline static int32_t get_offset_of_RestorePosition_18() { return static_cast<int32_t>(offsetof(MMTouchDynamicJoystick_t1438601146, ___RestorePosition_18)); }
	inline bool get_RestorePosition_18() const { return ___RestorePosition_18; }
	inline bool* get_address_of_RestorePosition_18() { return &___RestorePosition_18; }
	inline void set_RestorePosition_18(bool value)
	{
		___RestorePosition_18 = value;
	}

	inline static int32_t get_offset_of__initialPosition_19() { return static_cast<int32_t>(offsetof(MMTouchDynamicJoystick_t1438601146, ____initialPosition_19)); }
	inline Vector3_t3722313464  get__initialPosition_19() const { return ____initialPosition_19; }
	inline Vector3_t3722313464 * get_address_of__initialPosition_19() { return &____initialPosition_19; }
	inline void set__initialPosition_19(Vector3_t3722313464  value)
	{
		____initialPosition_19 = value;
	}

	inline static int32_t get_offset_of__newPosition_20() { return static_cast<int32_t>(offsetof(MMTouchDynamicJoystick_t1438601146, ____newPosition_20)); }
	inline Vector3_t3722313464  get__newPosition_20() const { return ____newPosition_20; }
	inline Vector3_t3722313464 * get_address_of__newPosition_20() { return &____newPosition_20; }
	inline void set__newPosition_20(Vector3_t3722313464  value)
	{
		____newPosition_20 = value;
	}

	inline static int32_t get_offset_of__knobCanvasGroup_21() { return static_cast<int32_t>(offsetof(MMTouchDynamicJoystick_t1438601146, ____knobCanvasGroup_21)); }
	inline CanvasGroup_t4083511760 * get__knobCanvasGroup_21() const { return ____knobCanvasGroup_21; }
	inline CanvasGroup_t4083511760 ** get_address_of__knobCanvasGroup_21() { return &____knobCanvasGroup_21; }
	inline void set__knobCanvasGroup_21(CanvasGroup_t4083511760 * value)
	{
		____knobCanvasGroup_21 = value;
		Il2CppCodeGenWriteBarrier((&____knobCanvasGroup_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMTOUCHDYNAMICJOYSTICK_T1438601146_H
#ifndef LINKEDSPAWNER_T1772930446_H
#define LINKEDSPAWNER_T1772930446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LinkedSpawner
struct  LinkedSpawner_t1772930446  : public Spawner_t3426627155
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.LinkedSpawner::SpawnDistanceFromPlayer
	float ___SpawnDistanceFromPlayer_12;
	// LinkedSpawnedObject MoreMountains.InfiniteRunnerEngine.LinkedSpawner::_lastLinkedSpawnedObject
	LinkedSpawnedObject_t3952174992 * ____lastLinkedSpawnedObject_13;
	// UnityEngine.Transform MoreMountains.InfiniteRunnerEngine.LinkedSpawner::_lastSpawnedTransform
	Transform_t3600365921 * ____lastSpawnedTransform_14;
	// System.Single MoreMountains.InfiniteRunnerEngine.LinkedSpawner::_nextSpawnDistance
	float ____nextSpawnDistance_15;

public:
	inline static int32_t get_offset_of_SpawnDistanceFromPlayer_12() { return static_cast<int32_t>(offsetof(LinkedSpawner_t1772930446, ___SpawnDistanceFromPlayer_12)); }
	inline float get_SpawnDistanceFromPlayer_12() const { return ___SpawnDistanceFromPlayer_12; }
	inline float* get_address_of_SpawnDistanceFromPlayer_12() { return &___SpawnDistanceFromPlayer_12; }
	inline void set_SpawnDistanceFromPlayer_12(float value)
	{
		___SpawnDistanceFromPlayer_12 = value;
	}

	inline static int32_t get_offset_of__lastLinkedSpawnedObject_13() { return static_cast<int32_t>(offsetof(LinkedSpawner_t1772930446, ____lastLinkedSpawnedObject_13)); }
	inline LinkedSpawnedObject_t3952174992 * get__lastLinkedSpawnedObject_13() const { return ____lastLinkedSpawnedObject_13; }
	inline LinkedSpawnedObject_t3952174992 ** get_address_of__lastLinkedSpawnedObject_13() { return &____lastLinkedSpawnedObject_13; }
	inline void set__lastLinkedSpawnedObject_13(LinkedSpawnedObject_t3952174992 * value)
	{
		____lastLinkedSpawnedObject_13 = value;
		Il2CppCodeGenWriteBarrier((&____lastLinkedSpawnedObject_13), value);
	}

	inline static int32_t get_offset_of__lastSpawnedTransform_14() { return static_cast<int32_t>(offsetof(LinkedSpawner_t1772930446, ____lastSpawnedTransform_14)); }
	inline Transform_t3600365921 * get__lastSpawnedTransform_14() const { return ____lastSpawnedTransform_14; }
	inline Transform_t3600365921 ** get_address_of__lastSpawnedTransform_14() { return &____lastSpawnedTransform_14; }
	inline void set__lastSpawnedTransform_14(Transform_t3600365921 * value)
	{
		____lastSpawnedTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&____lastSpawnedTransform_14), value);
	}

	inline static int32_t get_offset_of__nextSpawnDistance_15() { return static_cast<int32_t>(offsetof(LinkedSpawner_t1772930446, ____nextSpawnDistance_15)); }
	inline float get__nextSpawnDistance_15() const { return ____nextSpawnDistance_15; }
	inline float* get_address_of__nextSpawnDistance_15() { return &____nextSpawnDistance_15; }
	inline void set__nextSpawnDistance_15(float value)
	{
		____nextSpawnDistance_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDSPAWNER_T1772930446_H
#ifndef ROCKET_T400269931_H
#define ROCKET_T400269931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.Rocket
struct  Rocket_t400269931  : public PlayableCharacter_t2738907193
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.Rocket::FlyForce
	float ___FlyForce_16;
	// System.Single MoreMountains.InfiniteRunnerEngine.Rocket::MaximumVelocity
	float ___MaximumVelocity_17;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.Rocket::_boosting
	bool ____boosting_18;

public:
	inline static int32_t get_offset_of_FlyForce_16() { return static_cast<int32_t>(offsetof(Rocket_t400269931, ___FlyForce_16)); }
	inline float get_FlyForce_16() const { return ___FlyForce_16; }
	inline float* get_address_of_FlyForce_16() { return &___FlyForce_16; }
	inline void set_FlyForce_16(float value)
	{
		___FlyForce_16 = value;
	}

	inline static int32_t get_offset_of_MaximumVelocity_17() { return static_cast<int32_t>(offsetof(Rocket_t400269931, ___MaximumVelocity_17)); }
	inline float get_MaximumVelocity_17() const { return ___MaximumVelocity_17; }
	inline float* get_address_of_MaximumVelocity_17() { return &___MaximumVelocity_17; }
	inline void set_MaximumVelocity_17(float value)
	{
		___MaximumVelocity_17 = value;
	}

	inline static int32_t get_offset_of__boosting_18() { return static_cast<int32_t>(offsetof(Rocket_t400269931, ____boosting_18)); }
	inline bool get__boosting_18() const { return ____boosting_18; }
	inline bool* get_address_of__boosting_18() { return &____boosting_18; }
	inline void set__boosting_18(bool value)
	{
		____boosting_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROCKET_T400269931_H
#ifndef SPEEDBONUS_T4085138956_H
#define SPEEDBONUS_T4085138956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.SpeedBonus
struct  SpeedBonus_t4085138956  : public PickableObject_t2542163765
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.SpeedBonus::SpeedFactor
	float ___SpeedFactor_4;
	// System.Single MoreMountains.InfiniteRunnerEngine.SpeedBonus::EffectDuration
	float ___EffectDuration_5;

public:
	inline static int32_t get_offset_of_SpeedFactor_4() { return static_cast<int32_t>(offsetof(SpeedBonus_t4085138956, ___SpeedFactor_4)); }
	inline float get_SpeedFactor_4() const { return ___SpeedFactor_4; }
	inline float* get_address_of_SpeedFactor_4() { return &___SpeedFactor_4; }
	inline void set_SpeedFactor_4(float value)
	{
		___SpeedFactor_4 = value;
	}

	inline static int32_t get_offset_of_EffectDuration_5() { return static_cast<int32_t>(offsetof(SpeedBonus_t4085138956, ___EffectDuration_5)); }
	inline float get_EffectDuration_5() const { return ___EffectDuration_5; }
	inline float* get_address_of_EffectDuration_5() { return &___EffectDuration_5; }
	inline void set_EffectDuration_5(float value)
	{
		___EffectDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEEDBONUS_T4085138956_H
#ifndef COIN_T1024513743_H
#define COIN_T1024513743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.Coin
struct  Coin_t1024513743  : public PickableObject_t2542163765
{
public:
	// System.Int32 MoreMountains.InfiniteRunnerEngine.Coin::PointsToAdd
	int32_t ___PointsToAdd_4;

public:
	inline static int32_t get_offset_of_PointsToAdd_4() { return static_cast<int32_t>(offsetof(Coin_t1024513743, ___PointsToAdd_4)); }
	inline int32_t get_PointsToAdd_4() const { return ___PointsToAdd_4; }
	inline int32_t* get_address_of_PointsToAdd_4() { return &___PointsToAdd_4; }
	inline void set_PointsToAdd_4(int32_t value)
	{
		___PointsToAdd_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COIN_T1024513743_H
#ifndef FREELEFTTORIGHTMOVEMENT_T333621482_H
#define FREELEFTTORIGHTMOVEMENT_T333621482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement
struct  FreeLeftToRightMovement_t333621482  : public PlayableCharacter_t2738907193
{
public:
	// MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement/PossibleMovementAxis MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::MovementAxis
	int32_t ___MovementAxis_16;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::MoveSpeed
	float ___MoveSpeed_17;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::MovementInertia
	float ___MovementInertia_18;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::ConstrainMovementToDeathBounds
	bool ___ConstrainMovementToDeathBounds_19;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_movementLeft
	float ____movementLeft_20;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_movementRight
	float ____movementRight_21;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_movement
	float ____movement_22;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_currentMovement
	float ____currentMovement_23;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_newPosition
	Vector3_t3722313464  ____newPosition_24;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_minBound
	float ____minBound_25;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_maxBound
	float ____maxBound_26;
	// System.Single MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_boundsSecurity
	float ____boundsSecurity_27;
	// UnityEngine.Vector3 MoreMountains.InfiniteRunnerEngine.FreeLeftToRightMovement::_movementAxis
	Vector3_t3722313464  ____movementAxis_28;

public:
	inline static int32_t get_offset_of_MovementAxis_16() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ___MovementAxis_16)); }
	inline int32_t get_MovementAxis_16() const { return ___MovementAxis_16; }
	inline int32_t* get_address_of_MovementAxis_16() { return &___MovementAxis_16; }
	inline void set_MovementAxis_16(int32_t value)
	{
		___MovementAxis_16 = value;
	}

	inline static int32_t get_offset_of_MoveSpeed_17() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ___MoveSpeed_17)); }
	inline float get_MoveSpeed_17() const { return ___MoveSpeed_17; }
	inline float* get_address_of_MoveSpeed_17() { return &___MoveSpeed_17; }
	inline void set_MoveSpeed_17(float value)
	{
		___MoveSpeed_17 = value;
	}

	inline static int32_t get_offset_of_MovementInertia_18() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ___MovementInertia_18)); }
	inline float get_MovementInertia_18() const { return ___MovementInertia_18; }
	inline float* get_address_of_MovementInertia_18() { return &___MovementInertia_18; }
	inline void set_MovementInertia_18(float value)
	{
		___MovementInertia_18 = value;
	}

	inline static int32_t get_offset_of_ConstrainMovementToDeathBounds_19() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ___ConstrainMovementToDeathBounds_19)); }
	inline bool get_ConstrainMovementToDeathBounds_19() const { return ___ConstrainMovementToDeathBounds_19; }
	inline bool* get_address_of_ConstrainMovementToDeathBounds_19() { return &___ConstrainMovementToDeathBounds_19; }
	inline void set_ConstrainMovementToDeathBounds_19(bool value)
	{
		___ConstrainMovementToDeathBounds_19 = value;
	}

	inline static int32_t get_offset_of__movementLeft_20() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____movementLeft_20)); }
	inline float get__movementLeft_20() const { return ____movementLeft_20; }
	inline float* get_address_of__movementLeft_20() { return &____movementLeft_20; }
	inline void set__movementLeft_20(float value)
	{
		____movementLeft_20 = value;
	}

	inline static int32_t get_offset_of__movementRight_21() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____movementRight_21)); }
	inline float get__movementRight_21() const { return ____movementRight_21; }
	inline float* get_address_of__movementRight_21() { return &____movementRight_21; }
	inline void set__movementRight_21(float value)
	{
		____movementRight_21 = value;
	}

	inline static int32_t get_offset_of__movement_22() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____movement_22)); }
	inline float get__movement_22() const { return ____movement_22; }
	inline float* get_address_of__movement_22() { return &____movement_22; }
	inline void set__movement_22(float value)
	{
		____movement_22 = value;
	}

	inline static int32_t get_offset_of__currentMovement_23() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____currentMovement_23)); }
	inline float get__currentMovement_23() const { return ____currentMovement_23; }
	inline float* get_address_of__currentMovement_23() { return &____currentMovement_23; }
	inline void set__currentMovement_23(float value)
	{
		____currentMovement_23 = value;
	}

	inline static int32_t get_offset_of__newPosition_24() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____newPosition_24)); }
	inline Vector3_t3722313464  get__newPosition_24() const { return ____newPosition_24; }
	inline Vector3_t3722313464 * get_address_of__newPosition_24() { return &____newPosition_24; }
	inline void set__newPosition_24(Vector3_t3722313464  value)
	{
		____newPosition_24 = value;
	}

	inline static int32_t get_offset_of__minBound_25() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____minBound_25)); }
	inline float get__minBound_25() const { return ____minBound_25; }
	inline float* get_address_of__minBound_25() { return &____minBound_25; }
	inline void set__minBound_25(float value)
	{
		____minBound_25 = value;
	}

	inline static int32_t get_offset_of__maxBound_26() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____maxBound_26)); }
	inline float get__maxBound_26() const { return ____maxBound_26; }
	inline float* get_address_of__maxBound_26() { return &____maxBound_26; }
	inline void set__maxBound_26(float value)
	{
		____maxBound_26 = value;
	}

	inline static int32_t get_offset_of__boundsSecurity_27() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____boundsSecurity_27)); }
	inline float get__boundsSecurity_27() const { return ____boundsSecurity_27; }
	inline float* get_address_of__boundsSecurity_27() { return &____boundsSecurity_27; }
	inline void set__boundsSecurity_27(float value)
	{
		____boundsSecurity_27 = value;
	}

	inline static int32_t get_offset_of__movementAxis_28() { return static_cast<int32_t>(offsetof(FreeLeftToRightMovement_t333621482, ____movementAxis_28)); }
	inline Vector3_t3722313464  get__movementAxis_28() const { return ____movementAxis_28; }
	inline Vector3_t3722313464 * get_address_of__movementAxis_28() { return &____movementAxis_28; }
	inline void set__movementAxis_28(Vector3_t3722313464  value)
	{
		____movementAxis_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FREELEFTTORIGHTMOVEMENT_T333621482_H
#ifndef LEFTRIGHTMOVE_T2546799954_H
#define LEFTRIGHTMOVE_T2546799954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.LeftRightMove
struct  LeftRightMove_t2546799954  : public PlayableCharacter_t2738907193
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.LeftRightMove::MoveSpeed
	float ___MoveSpeed_16;

public:
	inline static int32_t get_offset_of_MoveSpeed_16() { return static_cast<int32_t>(offsetof(LeftRightMove_t2546799954, ___MoveSpeed_16)); }
	inline float get_MoveSpeed_16() const { return ___MoveSpeed_16; }
	inline float* get_address_of_MoveSpeed_16() { return &___MoveSpeed_16; }
	inline void set_MoveSpeed_16(float value)
	{
		___MoveSpeed_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEFTRIGHTMOVE_T2546799954_H
#ifndef JUMPER_T898605922_H
#define JUMPER_T898605922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.InfiniteRunnerEngine.Jumper
struct  Jumper_t898605922  : public PlayableCharacter_t2738907193
{
public:
	// System.Single MoreMountains.InfiniteRunnerEngine.Jumper::JumpForce
	float ___JumpForce_16;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.Jumper::NumberOfJumpsAllowed
	int32_t ___NumberOfJumpsAllowed_17;
	// System.Single MoreMountains.InfiniteRunnerEngine.Jumper::CooldownBetweenJumps
	float ___CooldownBetweenJumps_18;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.Jumper::JumpsAllowedWhenGroundedOnly
	bool ___JumpsAllowedWhenGroundedOnly_19;
	// System.Single MoreMountains.InfiniteRunnerEngine.Jumper::JumpReleaseSpeed
	float ___JumpReleaseSpeed_20;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.Jumper::JumpProportionalToPress
	bool ___JumpProportionalToPress_21;
	// System.Int32 MoreMountains.InfiniteRunnerEngine.Jumper::_numberOfJumpsLeft
	int32_t ____numberOfJumpsLeft_22;
	// System.Boolean MoreMountains.InfiniteRunnerEngine.Jumper::_jumping
	bool ____jumping_23;
	// System.Single MoreMountains.InfiniteRunnerEngine.Jumper::_lastJumpTime
	float ____lastJumpTime_24;

public:
	inline static int32_t get_offset_of_JumpForce_16() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ___JumpForce_16)); }
	inline float get_JumpForce_16() const { return ___JumpForce_16; }
	inline float* get_address_of_JumpForce_16() { return &___JumpForce_16; }
	inline void set_JumpForce_16(float value)
	{
		___JumpForce_16 = value;
	}

	inline static int32_t get_offset_of_NumberOfJumpsAllowed_17() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ___NumberOfJumpsAllowed_17)); }
	inline int32_t get_NumberOfJumpsAllowed_17() const { return ___NumberOfJumpsAllowed_17; }
	inline int32_t* get_address_of_NumberOfJumpsAllowed_17() { return &___NumberOfJumpsAllowed_17; }
	inline void set_NumberOfJumpsAllowed_17(int32_t value)
	{
		___NumberOfJumpsAllowed_17 = value;
	}

	inline static int32_t get_offset_of_CooldownBetweenJumps_18() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ___CooldownBetweenJumps_18)); }
	inline float get_CooldownBetweenJumps_18() const { return ___CooldownBetweenJumps_18; }
	inline float* get_address_of_CooldownBetweenJumps_18() { return &___CooldownBetweenJumps_18; }
	inline void set_CooldownBetweenJumps_18(float value)
	{
		___CooldownBetweenJumps_18 = value;
	}

	inline static int32_t get_offset_of_JumpsAllowedWhenGroundedOnly_19() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ___JumpsAllowedWhenGroundedOnly_19)); }
	inline bool get_JumpsAllowedWhenGroundedOnly_19() const { return ___JumpsAllowedWhenGroundedOnly_19; }
	inline bool* get_address_of_JumpsAllowedWhenGroundedOnly_19() { return &___JumpsAllowedWhenGroundedOnly_19; }
	inline void set_JumpsAllowedWhenGroundedOnly_19(bool value)
	{
		___JumpsAllowedWhenGroundedOnly_19 = value;
	}

	inline static int32_t get_offset_of_JumpReleaseSpeed_20() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ___JumpReleaseSpeed_20)); }
	inline float get_JumpReleaseSpeed_20() const { return ___JumpReleaseSpeed_20; }
	inline float* get_address_of_JumpReleaseSpeed_20() { return &___JumpReleaseSpeed_20; }
	inline void set_JumpReleaseSpeed_20(float value)
	{
		___JumpReleaseSpeed_20 = value;
	}

	inline static int32_t get_offset_of_JumpProportionalToPress_21() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ___JumpProportionalToPress_21)); }
	inline bool get_JumpProportionalToPress_21() const { return ___JumpProportionalToPress_21; }
	inline bool* get_address_of_JumpProportionalToPress_21() { return &___JumpProportionalToPress_21; }
	inline void set_JumpProportionalToPress_21(bool value)
	{
		___JumpProportionalToPress_21 = value;
	}

	inline static int32_t get_offset_of__numberOfJumpsLeft_22() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ____numberOfJumpsLeft_22)); }
	inline int32_t get__numberOfJumpsLeft_22() const { return ____numberOfJumpsLeft_22; }
	inline int32_t* get_address_of__numberOfJumpsLeft_22() { return &____numberOfJumpsLeft_22; }
	inline void set__numberOfJumpsLeft_22(int32_t value)
	{
		____numberOfJumpsLeft_22 = value;
	}

	inline static int32_t get_offset_of__jumping_23() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ____jumping_23)); }
	inline bool get__jumping_23() const { return ____jumping_23; }
	inline bool* get_address_of__jumping_23() { return &____jumping_23; }
	inline void set__jumping_23(bool value)
	{
		____jumping_23 = value;
	}

	inline static int32_t get_offset_of__lastJumpTime_24() { return static_cast<int32_t>(offsetof(Jumper_t898605922, ____lastJumpTime_24)); }
	inline float get__lastJumpTime_24() const { return ____lastJumpTime_24; }
	inline float* get_address_of__lastJumpTime_24() { return &____lastJumpTime_24; }
	inline void set__lastJumpTime_24(float value)
	{
		____lastJumpTime_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMPER_T898605922_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (GameManagerInspectorRedraw_t3388984653), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (U3CIncrementScoreU3Ec__Iterator0_t3972351830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[4] = 
{
	U3CIncrementScoreU3Ec__Iterator0_t3972351830::get_offset_of_U24this_0(),
	U3CIncrementScoreU3Ec__Iterator0_t3972351830::get_offset_of_U24current_1(),
	U3CIncrementScoreU3Ec__Iterator0_t3972351830::get_offset_of_U24disposing_2(),
	U3CIncrementScoreU3Ec__Iterator0_t3972351830::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (GUIManager_t1233035445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[8] = 
{
	GUIManager_t1233035445::get_offset_of_PauseScreen_3(),
	GUIManager_t1233035445::get_offset_of_GameOverScreen_4(),
	GUIManager_t1233035445::get_offset_of_HeartsContainer_5(),
	GUIManager_t1233035445::get_offset_of_PointsText_6(),
	GUIManager_t1233035445::get_offset_of_LevelText_7(),
	GUIManager_t1233035445::get_offset_of_CountdownText_8(),
	GUIManager_t1233035445::get_offset_of_Fader_9(),
	GUIManager_t1233035445::get_offset_of_Timer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (InputManager_t3769144452), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (LevelManager_t1115010890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[30] = 
{
	LevelManager_t1115010890::get_offset_of_U3CSpeedU3Ek__BackingField_3(),
	LevelManager_t1115010890::get_offset_of_U3CDistanceTraveledU3Ek__BackingField_4(),
	LevelManager_t1115010890::get_offset_of_StartingPosition_5(),
	LevelManager_t1115010890::get_offset_of_PlayableCharacters_6(),
	LevelManager_t1115010890::get_offset_of_U3CCurrentPlayableCharactersU3Ek__BackingField_7(),
	LevelManager_t1115010890::get_offset_of_DistanceBetweenCharacters_8(),
	LevelManager_t1115010890::get_offset_of_U3CRunningTimeU3Ek__BackingField_9(),
	LevelManager_t1115010890::get_offset_of_PointsPerSecond_10(),
	LevelManager_t1115010890::get_offset_of_levelTime_11(),
	LevelManager_t1115010890::get_offset_of_InstructionsText_12(),
	LevelManager_t1115010890::get_offset_of_RecycleBounds_13(),
	LevelManager_t1115010890::get_offset_of_DeathBounds_14(),
	LevelManager_t1115010890::get_offset_of_InitialSpeed_15(),
	LevelManager_t1115010890::get_offset_of_MaximumSpeed_16(),
	LevelManager_t1115010890::get_offset_of_SpeedAcceleration_17(),
	LevelManager_t1115010890::get_offset_of_IntroFadeDuration_18(),
	LevelManager_t1115010890::get_offset_of_OutroFadeDuration_19(),
	LevelManager_t1115010890::get_offset_of_StartCountdown_20(),
	LevelManager_t1115010890::get_offset_of_StartText_21(),
	LevelManager_t1115010890::get_offset_of_ControlScheme_22(),
	LevelManager_t1115010890::get_offset_of_LifeLostExplosion_23(),
	LevelManager_t1115010890::get_offset_of__started_24(),
	LevelManager_t1115010890::get_offset_of__savedPoints_25(),
	LevelManager_t1115010890::get_offset_of__recycleX_26(),
	LevelManager_t1115010890::get_offset_of__tmpRecycleBounds_27(),
	LevelManager_t1115010890::get_offset_of__temporarySpeedFactorActive_28(),
	LevelManager_t1115010890::get_offset_of__temporarySpeedFactor_29(),
	LevelManager_t1115010890::get_offset_of__temporarySpeedFactorRemainingTime_30(),
	LevelManager_t1115010890::get_offset_of__temporarySavedSpeed_31(),
	LevelManager_t1115010890::get_offset_of__timeCounter_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (Controls_t3636651702)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1905[4] = 
{
	Controls_t3636651702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[5] = 
{
	U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311::get_offset_of_U3CcountdownU3E__0_0(),
	U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311::get_offset_of_U24this_1(),
	U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311::get_offset_of_U24current_2(),
	U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311::get_offset_of_U24disposing_3(),
	U3CPrepareStartCountdownU3Ec__Iterator0_t3347106311::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (U3CGotoLevelCoU3Ec__Iterator1_t1210678941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[5] = 
{
	U3CGotoLevelCoU3Ec__Iterator1_t1210678941::get_offset_of_levelName_0(),
	U3CGotoLevelCoU3Ec__Iterator1_t1210678941::get_offset_of_U24this_1(),
	U3CGotoLevelCoU3Ec__Iterator1_t1210678941::get_offset_of_U24current_2(),
	U3CGotoLevelCoU3Ec__Iterator1_t1210678941::get_offset_of_U24disposing_3(),
	U3CGotoLevelCoU3Ec__Iterator1_t1210678941::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (U3CKillCharacterCoU3Ec__Iterator2_t374224669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[5] = 
{
	U3CKillCharacterCoU3Ec__Iterator2_t374224669::get_offset_of_player_0(),
	U3CKillCharacterCoU3Ec__Iterator2_t374224669::get_offset_of_U24this_1(),
	U3CKillCharacterCoU3Ec__Iterator2_t374224669::get_offset_of_U24current_2(),
	U3CKillCharacterCoU3Ec__Iterator2_t374224669::get_offset_of_U24disposing_3(),
	U3CKillCharacterCoU3Ec__Iterator2_t374224669::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (LoadingSceneManager_t1484615109), -1, sizeof(LoadingSceneManager_t1484615109_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1909[14] = 
{
	LoadingSceneManager_t1484615109_StaticFields::get_offset_of_LoadingScreenSceneName_2(),
	LoadingSceneManager_t1484615109::get_offset_of_LoadingText_3(),
	LoadingSceneManager_t1484615109::get_offset_of_LoadingProgressBar_4(),
	LoadingSceneManager_t1484615109::get_offset_of_LoadingAnimation_5(),
	LoadingSceneManager_t1484615109::get_offset_of_LoadingCompleteAnimation_6(),
	LoadingSceneManager_t1484615109::get_offset_of_StartFadeDuration_7(),
	LoadingSceneManager_t1484615109::get_offset_of_ProgressBarSpeed_8(),
	LoadingSceneManager_t1484615109::get_offset_of_ExitFadeDuration_9(),
	LoadingSceneManager_t1484615109::get_offset_of_LoadCompleteDelay_10(),
	LoadingSceneManager_t1484615109::get_offset_of__asyncOperation_11(),
	LoadingSceneManager_t1484615109_StaticFields::get_offset_of__sceneToLoad_12(),
	LoadingSceneManager_t1484615109::get_offset_of__fadeDuration_13(),
	LoadingSceneManager_t1484615109::get_offset_of__fillTarget_14(),
	LoadingSceneManager_t1484615109::get_offset_of__loadingTextValue_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[4] = 
{
	U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867::get_offset_of_U24this_0(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867::get_offset_of_U24current_1(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867::get_offset_of_U24disposing_2(),
	U3CLoadAsynchronouslyU3Ec__Iterator0_t3995192867::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (SingleHighScoreManager_t860903255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (SoundManager_t1622142048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[5] = 
{
	SoundManager_t1622142048::get_offset_of_MusicOn_4(),
	SoundManager_t1622142048::get_offset_of_SfxOn_5(),
	SoundManager_t1622142048::get_offset_of_MusicVolume_6(),
	SoundManager_t1622142048::get_offset_of_SfxVolume_7(),
	SoundManager_t1622142048::get_offset_of__backgroundMusic_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (MovingObject_t2316572705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[6] = 
{
	MovingObject_t2316572705::get_offset_of_Speed_2(),
	MovingObject_t2316572705::get_offset_of_Acceleration_3(),
	MovingObject_t2316572705::get_offset_of_Direction_4(),
	MovingObject_t2316572705::get_offset_of_DirectionCanBeChangedBySpawner_5(),
	MovingObject_t2316572705::get_offset_of__movement_6(),
	MovingObject_t2316572705::get_offset_of__initialSpeed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (ReactivateOnSpawn_t2603330824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[1] = 
{
	ReactivateOnSpawn_t2603330824::get_offset_of_ShouldReactivate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (RepositionOnStart2D_t4190716172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[2] = 
{
	RepositionOnStart2D_t4190716172::get_offset_of_PositionOffset_2(),
	RepositionOnStart2D_t4190716172::get_offset_of_RaycastLength_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (Parallax_t3067417749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[8] = 
{
	Parallax_t3067417749::get_offset_of_ParallaxSpeed_4(),
	Parallax_t3067417749::get_offset_of_ParallaxDirection_5(),
	Parallax_t3067417749::get_offset_of__clone_6(),
	Parallax_t3067417749::get_offset_of__movement_7(),
	Parallax_t3067417749::get_offset_of__initialPosition_8(),
	Parallax_t3067417749::get_offset_of__newPosition_9(),
	Parallax_t3067417749::get_offset_of__direction_10(),
	Parallax_t3067417749::get_offset_of__width_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (PossibleDirections_t3381919105)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1917[7] = 
{
	PossibleDirections_t3381919105::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (ParallaxOffset_t133287567), -1, sizeof(ParallaxOffset_t133287567_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1918[7] = 
{
	ParallaxOffset_t133287567::get_offset_of_Speed_2(),
	ParallaxOffset_t133287567_StaticFields::get_offset_of_CurrentParallaxOffset_3(),
	ParallaxOffset_t133287567::get_offset_of__rawImage_4(),
	ParallaxOffset_t133287567::get_offset_of__renderer_5(),
	ParallaxOffset_t133287567::get_offset_of__newOffset_6(),
	ParallaxOffset_t133287567::get_offset_of__position_7(),
	ParallaxOffset_t133287567::get_offset_of_yOffset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (Coin_t1024513743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[1] = 
{
	Coin_t1024513743::get_offset_of_PointsToAdd_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (PickableObject_t2542163765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[2] = 
{
	PickableObject_t2542163765::get_offset_of_PickEffect_2(),
	PickableObject_t2542163765::get_offset_of_PickSoundFx_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (SpeedBonus_t4085138956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[2] = 
{
	SpeedBonus_t4085138956::get_offset_of_SpeedFactor_4(),
	SpeedBonus_t4085138956::get_offset_of_EffectDuration_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (FreeLeftToRightMovement_t333621482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[13] = 
{
	FreeLeftToRightMovement_t333621482::get_offset_of_MovementAxis_16(),
	FreeLeftToRightMovement_t333621482::get_offset_of_MoveSpeed_17(),
	FreeLeftToRightMovement_t333621482::get_offset_of_MovementInertia_18(),
	FreeLeftToRightMovement_t333621482::get_offset_of_ConstrainMovementToDeathBounds_19(),
	FreeLeftToRightMovement_t333621482::get_offset_of__movementLeft_20(),
	FreeLeftToRightMovement_t333621482::get_offset_of__movementRight_21(),
	FreeLeftToRightMovement_t333621482::get_offset_of__movement_22(),
	FreeLeftToRightMovement_t333621482::get_offset_of__currentMovement_23(),
	FreeLeftToRightMovement_t333621482::get_offset_of__newPosition_24(),
	FreeLeftToRightMovement_t333621482::get_offset_of__minBound_25(),
	FreeLeftToRightMovement_t333621482::get_offset_of__maxBound_26(),
	FreeLeftToRightMovement_t333621482::get_offset_of__boundsSecurity_27(),
	FreeLeftToRightMovement_t333621482::get_offset_of__movementAxis_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (PossibleMovementAxis_t588859528)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[4] = 
{
	PossibleMovementAxis_t588859528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (Jumper_t898605922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[9] = 
{
	Jumper_t898605922::get_offset_of_JumpForce_16(),
	Jumper_t898605922::get_offset_of_NumberOfJumpsAllowed_17(),
	Jumper_t898605922::get_offset_of_CooldownBetweenJumps_18(),
	Jumper_t898605922::get_offset_of_JumpsAllowedWhenGroundedOnly_19(),
	Jumper_t898605922::get_offset_of_JumpReleaseSpeed_20(),
	Jumper_t898605922::get_offset_of_JumpProportionalToPress_21(),
	Jumper_t898605922::get_offset_of__numberOfJumpsLeft_22(),
	Jumper_t898605922::get_offset_of__jumping_23(),
	Jumper_t898605922::get_offset_of__lastJumpTime_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (U3CJumpSlowU3Ec__Iterator0_t4153544754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[5] = 
{
	U3CJumpSlowU3Ec__Iterator0_t4153544754::get_offset_of_U3CnewGravityU3E__1_0(),
	U3CJumpSlowU3Ec__Iterator0_t4153544754::get_offset_of_U24this_1(),
	U3CJumpSlowU3Ec__Iterator0_t4153544754::get_offset_of_U24current_2(),
	U3CJumpSlowU3Ec__Iterator0_t4153544754::get_offset_of_U24disposing_3(),
	U3CJumpSlowU3Ec__Iterator0_t4153544754::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (LeftRightMove_t2546799954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[1] = 
{
	LeftRightMove_t2546799954::get_offset_of_MoveSpeed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (PlayableCharacter_t2738907193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[14] = 
{
	PlayableCharacter_t2738907193::get_offset_of_UseDefaultMecanim_2(),
	PlayableCharacter_t2738907193::get_offset_of_ShouldResetPosition_3(),
	PlayableCharacter_t2738907193::get_offset_of_ResetPositionSpeed_4(),
	PlayableCharacter_t2738907193::get_offset_of_U3CDistanceToTheGroundU3Ek__BackingField_5(),
	PlayableCharacter_t2738907193::get_offset_of__initialPosition_6(),
	PlayableCharacter_t2738907193::get_offset_of__grounded_7(),
	PlayableCharacter_t2738907193::get_offset_of__rigidbodyInterface_8(),
	PlayableCharacter_t2738907193::get_offset_of__animator_9(),
	PlayableCharacter_t2738907193::get_offset_of__distanceToTheGroundRaycastLength_10(),
	PlayableCharacter_t2738907193::get_offset_of__ground_11(),
	PlayableCharacter_t2738907193::get_offset_of__groundDistanceTolerance_12(),
	PlayableCharacter_t2738907193::get_offset_of__collisionMaskSave_13(),
	PlayableCharacter_t2738907193::get_offset_of__raycastLeftOrigin_14(),
	PlayableCharacter_t2738907193::get_offset_of__raycastRightOrigin_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (Rocket_t400269931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[3] = 
{
	Rocket_t400269931::get_offset_of_FlyForce_16(),
	Rocket_t400269931::get_offset_of_MaximumVelocity_17(),
	Rocket_t400269931::get_offset_of__boosting_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (ExampleScenario_t2931033563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (ScenarioEvent_t207753204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[6] = 
{
	ScenarioEvent_t207753204::get_offset_of_ScenarioEventType_0(),
	ScenarioEvent_t207753204::get_offset_of_StartTime_1(),
	ScenarioEvent_t207753204::get_offset_of_StartScore_2(),
	ScenarioEvent_t207753204::get_offset_of_Status_3(),
	ScenarioEvent_t207753204::get_offset_of_MMEventName_4(),
	ScenarioEvent_t207753204::get_offset_of_ScenarioEventAction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (ScenarioEventTypes_t2859657042)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[3] = 
{
	ScenarioEventTypes_t2859657042::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (ScenarioManager_t2117133370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[3] = 
{
	ScenarioManager_t2117133370::get_offset_of_EvaluationFrequency_2(),
	ScenarioManager_t2117133370::get_offset_of_UseEventManager_3(),
	ScenarioManager_t2117133370::get_offset_of__scenario_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (DistanceSpawner_t2744940552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[11] = 
{
	DistanceSpawner_t2744940552::get_offset_of_GapOrigin_12(),
	DistanceSpawner_t2744940552::get_offset_of_MinimumGap_13(),
	DistanceSpawner_t2744940552::get_offset_of_MaximumGap_14(),
	DistanceSpawner_t2744940552::get_offset_of_MinimumYClamp_15(),
	DistanceSpawner_t2744940552::get_offset_of_MaximumYClamp_16(),
	DistanceSpawner_t2744940552::get_offset_of_MinimumZClamp_17(),
	DistanceSpawner_t2744940552::get_offset_of_MaximumZClamp_18(),
	DistanceSpawner_t2744940552::get_offset_of_SpawnRotatedToDirection_19(),
	DistanceSpawner_t2744940552::get_offset_of__lastSpawnedTransform_20(),
	DistanceSpawner_t2744940552::get_offset_of__nextSpawnDistance_21(),
	DistanceSpawner_t2744940552::get_offset_of__gap_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (GapOrigins_t138053510)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1934[3] = 
{
	GapOrigins_t138053510::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (LinkedSpawnedObject_t3952174992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[3] = 
{
	LinkedSpawnedObject_t3952174992::get_offset_of_In_2(),
	LinkedSpawnedObject_t3952174992::get_offset_of_Out_3(),
	LinkedSpawnedObject_t3952174992::get_offset_of_U3CInOutSetupU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (LinkedSpawner_t1772930446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[4] = 
{
	LinkedSpawner_t1772930446::get_offset_of_SpawnDistanceFromPlayer_12(),
	LinkedSpawner_t1772930446::get_offset_of__lastLinkedSpawnedObject_13(),
	LinkedSpawner_t1772930446::get_offset_of__lastSpawnedTransform_14(),
	LinkedSpawner_t1772930446::get_offset_of__nextSpawnDistance_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (OutOfBoundsRecycle_t3009250907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[1] = 
{
	OutOfBoundsRecycle_t3009250907::get_offset_of_DestroyDistanceBehindBounds_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (Spawner_t3426627155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[10] = 
{
	Spawner_t3426627155::get_offset_of_MinimumSize_2(),
	Spawner_t3426627155::get_offset_of_MaximumSize_3(),
	Spawner_t3426627155::get_offset_of_PreserveRatio_4(),
	Spawner_t3426627155::get_offset_of_MinimumRotation_5(),
	Spawner_t3426627155::get_offset_of_MaximumRotation_6(),
	Spawner_t3426627155::get_offset_of_Spawning_7(),
	Spawner_t3426627155::get_offset_of_OnlySpawnWhileGameInProgress_8(),
	Spawner_t3426627155::get_offset_of_InitialDelay_9(),
	Spawner_t3426627155::get_offset_of__objectPooler_10(),
	Spawner_t3426627155::get_offset_of__startTime_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (TimedSpawner_t239769417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[4] = 
{
	TimedSpawner_t239769417::get_offset_of_MinSpawnTime_12(),
	TimedSpawner_t239769417::get_offset_of_MaxSpawnTime_13(),
	TimedSpawner_t239769417::get_offset_of_MinPosition_14(),
	TimedSpawner_t239769417::get_offset_of_MaxPosition_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (LaneRunner_t2456357099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[6] = 
{
	LaneRunner_t2456357099::get_offset_of_LaneWidth_16(),
	LaneRunner_t2456357099::get_offset_of_NumberOfLanes_17(),
	LaneRunner_t2456357099::get_offset_of_ChangingLaneSpeed_18(),
	LaneRunner_t2456357099::get_offset_of_Explosion_19(),
	LaneRunner_t2456357099::get_offset_of__currentLane_20(),
	LaneRunner_t2456357099::get_offset_of__isMoving_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (U3CMoveToU3Ec__Iterator0_t4290519188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[9] = 
{
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_U3CinitialPositionU3E__0_1(),
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_destination_2(),
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_U3CsqrRemainingDistanceU3E__0_3(),
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_movementDuration_4(),
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_U24this_5(),
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_U24current_6(),
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_U24disposing_7(),
	U3CMoveToU3Ec__Iterator0_t4290519188::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (AchievementTypes_t2802848312)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1942[3] = 
{
	AchievementTypes_t2802848312::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (MMAchievement_t2522936773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[13] = 
{
	MMAchievement_t2522936773::get_offset_of_AchievementID_0(),
	MMAchievement_t2522936773::get_offset_of_AchievementType_1(),
	MMAchievement_t2522936773::get_offset_of_HiddenAchievement_2(),
	MMAchievement_t2522936773::get_offset_of_UnlockedStatus_3(),
	MMAchievement_t2522936773::get_offset_of_Title_4(),
	MMAchievement_t2522936773::get_offset_of_Description_5(),
	MMAchievement_t2522936773::get_offset_of_Points_6(),
	MMAchievement_t2522936773::get_offset_of_LockedImage_7(),
	MMAchievement_t2522936773::get_offset_of_UnlockedImage_8(),
	MMAchievement_t2522936773::get_offset_of_UnlockedSound_9(),
	MMAchievement_t2522936773::get_offset_of_ProgressTarget_10(),
	MMAchievement_t2522936773::get_offset_of_ProgressCurrent_11(),
	MMAchievement_t2522936773::get_offset_of__achievementDisplayItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (MMAchievementDisplayer_t237347715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[4] = 
{
	MMAchievementDisplayer_t237347715::get_offset_of_AchievementDisplayPrefab_2(),
	MMAchievementDisplayer_t237347715::get_offset_of_AchievementDisplayDuration_3(),
	MMAchievementDisplayer_t237347715::get_offset_of_AchievementFadeDuration_4(),
	MMAchievementDisplayer_t237347715::get_offset_of__achievementFadeOutWFS_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (U3CDisplayAchievementU3Ec__Iterator0_t2970070073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[8] = 
{
	U3CDisplayAchievementU3Ec__Iterator0_t2970070073::get_offset_of_U3CinstanceU3E__0_0(),
	U3CDisplayAchievementU3Ec__Iterator0_t2970070073::get_offset_of_U3CachievementDisplayU3E__0_1(),
	U3CDisplayAchievementU3Ec__Iterator0_t2970070073::get_offset_of_achievement_2(),
	U3CDisplayAchievementU3Ec__Iterator0_t2970070073::get_offset_of_U3CachievementCanvasGroupU3E__0_3(),
	U3CDisplayAchievementU3Ec__Iterator0_t2970070073::get_offset_of_U24this_4(),
	U3CDisplayAchievementU3Ec__Iterator0_t2970070073::get_offset_of_U24current_5(),
	U3CDisplayAchievementU3Ec__Iterator0_t2970070073::get_offset_of_U24disposing_6(),
	U3CDisplayAchievementU3Ec__Iterator0_t2970070073::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (MMAchievementDisplayItem_t1206168000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[6] = 
{
	MMAchievementDisplayItem_t1206168000::get_offset_of_BackgroundLocked_2(),
	MMAchievementDisplayItem_t1206168000::get_offset_of_BackgroundUnlocked_3(),
	MMAchievementDisplayItem_t1206168000::get_offset_of_Icon_4(),
	MMAchievementDisplayItem_t1206168000::get_offset_of_Title_5(),
	MMAchievementDisplayItem_t1206168000::get_offset_of_Description_6(),
	MMAchievementDisplayItem_t1206168000::get_offset_of_ProgressBarDisplay_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (MMAchievementUnlockedEvent_t4238862435)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[1] = 
{
	MMAchievementUnlockedEvent_t4238862435::get_offset_of_Achievement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (MMAchievementList_t2729535630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[2] = 
{
	MMAchievementList_t2729535630::get_offset_of_AchievementsListID_2(),
	MMAchievementList_t2729535630::get_offset_of_Achievements_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (MMAchievementManager_t3982398824), -1, sizeof(MMAchievementManager_t3982398824_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1949[7] = 
{
	MMAchievementManager_t3982398824_StaticFields::get_offset_of_Achievements_0(),
	MMAchievementManager_t3982398824_StaticFields::get_offset_of__achievement_1(),
	0,
	0,
	MMAchievementManager_t3982398824_StaticFields::get_offset_of__savePath_4(),
	MMAchievementManager_t3982398824_StaticFields::get_offset_of__saveFileName_5(),
	MMAchievementManager_t3982398824_StaticFields::get_offset_of__listID_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (MMAchievementRules_t226002105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (SerializedMMAchievement_t770899448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[3] = 
{
	SerializedMMAchievement_t770899448::get_offset_of_AchievementID_0(),
	SerializedMMAchievement_t770899448::get_offset_of_UnlockedStatus_1(),
	SerializedMMAchievement_t770899448::get_offset_of_ProgressCurrent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (SerializedMMAchievementManager_t2451628177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[1] = 
{
	SerializedMMAchievementManager_t2451628177::get_offset_of_Achievements_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (HiddenAttribute_t1779691839), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (InformationAttribute_t4007568514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (InformationType_t2282927356)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1955[5] = 
{
	InformationType_t2282927356::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (InspectorButtonAttribute_t3716718695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[1] = 
{
	InspectorButtonAttribute_t3716718695::get_offset_of_MethodName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (ReadOnlyAttribute_t657102582), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (TimedAutoDestroy_t483462573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[2] = 
{
	TimedAutoDestroy_t483462573::get_offset_of_TimeBeforeDestruction_2(),
	TimedAutoDestroy_t483462573::get_offset_of__timeBeforeDestructionWFS_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (U3CDestructionU3Ec__Iterator0_t2359794738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[4] = 
{
	U3CDestructionU3Ec__Iterator0_t2359794738::get_offset_of_U24this_0(),
	U3CDestructionU3Ec__Iterator0_t2359794738::get_offset_of_U24current_1(),
	U3CDestructionU3Ec__Iterator0_t2359794738::get_offset_of_U24disposing_2(),
	U3CDestructionU3Ec__Iterator0_t2359794738::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (AutoOrderInLayer_t2678432318), -1, sizeof(AutoOrderInLayer_t2678432318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1960[7] = 
{
	AutoOrderInLayer_t2678432318_StaticFields::get_offset_of_CurrentMaxCharacterOrderInLayer_2(),
	AutoOrderInLayer_t2678432318::get_offset_of_GlobalCounterIncrement_3(),
	AutoOrderInLayer_t2678432318::get_offset_of_BasedOnParentOrder_4(),
	AutoOrderInLayer_t2678432318::get_offset_of_ParentIncrement_5(),
	AutoOrderInLayer_t2678432318::get_offset_of_ApplyNewOrderToChildren_6(),
	AutoOrderInLayer_t2678432318::get_offset_of_ChildrenIncrement_7(),
	AutoOrderInLayer_t2678432318::get_offset_of__spriteRenderer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (AutoRotate_t2431756326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[2] = 
{
	AutoRotate_t2431756326::get_offset_of_RotationSpace_2(),
	AutoRotate_t2431756326::get_offset_of_RotationSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (PathMovementElement_t2178100112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[2] = 
{
	PathMovementElement_t2178100112::get_offset_of_PathElementPosition_0(),
	PathMovementElement_t2178100112::get_offset_of_Delay_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (PathMovement_t1036546472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[20] = 
{
	PathMovement_t1036546472::get_offset_of_CycleOption_2(),
	PathMovement_t1036546472::get_offset_of_LoopInitialMovementDirection_3(),
	PathMovement_t1036546472::get_offset_of_PathElements_4(),
	PathMovement_t1036546472::get_offset_of_MovementSpeed_5(),
	PathMovement_t1036546472::get_offset_of_U3CCurrentSpeedU3Ek__BackingField_6(),
	PathMovement_t1036546472::get_offset_of_AccelerationType_7(),
	PathMovement_t1036546472::get_offset_of_Acceleration_8(),
	PathMovement_t1036546472::get_offset_of_MinDistanceToGoal_9(),
	PathMovement_t1036546472::get_offset_of__originalTransformPosition_10(),
	PathMovement_t1036546472::get_offset_of__originalTransformPositionStatus_11(),
	PathMovement_t1036546472::get_offset_of__active_12(),
	PathMovement_t1036546472::get_offset_of__currentPoint_13(),
	PathMovement_t1036546472::get_offset_of__direction_14(),
	PathMovement_t1036546472::get_offset_of__initialPosition_15(),
	PathMovement_t1036546472::get_offset_of__finalPosition_16(),
	PathMovement_t1036546472::get_offset_of__previousPoint_17(),
	PathMovement_t1036546472::get_offset_of__waiting_18(),
	PathMovement_t1036546472::get_offset_of__currentIndex_19(),
	PathMovement_t1036546472::get_offset_of__distanceToNextPoint_20(),
	PathMovement_t1036546472::get_offset_of__endReached_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (PossibleAccelerationType_t1228029181)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1964[4] = 
{
	PossibleAccelerationType_t1228029181::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (CycleOptions_t470408444)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1965[4] = 
{
	CycleOptions_t470408444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (MovementDirection_t3398153413)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1966[3] = 
{
	MovementDirection_t3398153413::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[5] = 
{
	U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918::get_offset_of_U3CindexU3E__0_0(),
	U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918::get_offset_of_U24this_1(),
	U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918::get_offset_of_U24current_2(),
	U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918::get_offset_of_U24disposing_3(),
	U3CGetPathEnumeratorU3Ec__Iterator0_t4098586918::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (Wiggle_t1237850810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[30] = 
{
	Wiggle_t1237850810::get_offset_of_PositionFrequencyMin_2(),
	Wiggle_t1237850810::get_offset_of_PositionFrequencyMax_3(),
	Wiggle_t1237850810::get_offset_of_PositionAmplitudeMin_4(),
	Wiggle_t1237850810::get_offset_of_PositionAmplitudeMax_5(),
	Wiggle_t1237850810::get_offset_of_RotationFrequencyMin_6(),
	Wiggle_t1237850810::get_offset_of_RotationFrequencyMax_7(),
	Wiggle_t1237850810::get_offset_of_RotationAmplitudeMin_8(),
	Wiggle_t1237850810::get_offset_of_RotationAmplitudeMax_9(),
	Wiggle_t1237850810::get_offset_of_ScaleFrequencyMin_10(),
	Wiggle_t1237850810::get_offset_of_ScaleFrequencyMax_11(),
	Wiggle_t1237850810::get_offset_of_ScaleAmplitudeMin_12(),
	Wiggle_t1237850810::get_offset_of_ScaleAmplitudeMax_13(),
	Wiggle_t1237850810::get_offset_of__startPosition_14(),
	Wiggle_t1237850810::get_offset_of__startRotation_15(),
	Wiggle_t1237850810::get_offset_of__startScale_16(),
	Wiggle_t1237850810::get_offset_of__randomPositionFrequency_17(),
	Wiggle_t1237850810::get_offset_of__randomPositionAmplitude_18(),
	Wiggle_t1237850810::get_offset_of__randomRotationFrequency_19(),
	Wiggle_t1237850810::get_offset_of__randomRotationAmplitude_20(),
	Wiggle_t1237850810::get_offset_of__randomScaleFrequency_21(),
	Wiggle_t1237850810::get_offset_of__randomScaleAmplitude_22(),
	Wiggle_t1237850810::get_offset_of__positionTimer_23(),
	Wiggle_t1237850810::get_offset_of__rotationTimer_24(),
	Wiggle_t1237850810::get_offset_of__scaleTimer_25(),
	Wiggle_t1237850810::get_offset_of__positionT_26(),
	Wiggle_t1237850810::get_offset_of__rotationT_27(),
	Wiggle_t1237850810::get_offset_of__scaleT_28(),
	Wiggle_t1237850810::get_offset_of__newPosition_29(),
	Wiggle_t1237850810::get_offset_of__newRotation_30(),
	Wiggle_t1237850810::get_offset_of__newScale_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (U3CComputePositionU3Ec__Iterator0_t3591811775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[4] = 
{
	U3CComputePositionU3Ec__Iterator0_t3591811775::get_offset_of_U24this_0(),
	U3CComputePositionU3Ec__Iterator0_t3591811775::get_offset_of_U24current_1(),
	U3CComputePositionU3Ec__Iterator0_t3591811775::get_offset_of_U24disposing_2(),
	U3CComputePositionU3Ec__Iterator0_t3591811775::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (U3CComputeRotationU3Ec__Iterator1_t2080689703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[4] = 
{
	U3CComputeRotationU3Ec__Iterator1_t2080689703::get_offset_of_U24this_0(),
	U3CComputeRotationU3Ec__Iterator1_t2080689703::get_offset_of_U24current_1(),
	U3CComputeRotationU3Ec__Iterator1_t2080689703::get_offset_of_U24disposing_2(),
	U3CComputeRotationU3Ec__Iterator1_t2080689703::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (U3CComputeScaleU3Ec__Iterator2_t1730042883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	U3CComputeScaleU3Ec__Iterator2_t1730042883::get_offset_of_U24this_0(),
	U3CComputeScaleU3Ec__Iterator2_t1730042883::get_offset_of_U24current_1(),
	U3CComputeScaleU3Ec__Iterator2_t1730042883::get_offset_of_U24disposing_2(),
	U3CComputeScaleU3Ec__Iterator2_t1730042883::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (CameraAspectRatio_t3762038886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[2] = 
{
	CameraAspectRatio_t3762038886::get_offset_of_AspectRatio_2(),
	CameraAspectRatio_t3762038886::get_offset_of__camera_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (MMGameEvent_t4039509544)+ sizeof (RuntimeObject), sizeof(MMGameEvent_t4039509544_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1973[1] = 
{
	MMGameEvent_t4039509544::get_offset_of_EventName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (MMSfxEvent_t4198710224)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[1] = 
{
	MMSfxEvent_t4198710224::get_offset_of_ClipToPlay_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (MMEventManager_t383372512), -1, sizeof(MMEventManager_t383372512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1975[1] = 
{
	MMEventManager_t383372512_StaticFields::get_offset_of__subscribersList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (EventRegister_t3454741100), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (ExtensionMethods_t3763603995), -1, sizeof(ExtensionMethods_t3763603995_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1980[1] = 
{
	ExtensionMethods_t3763603995_StaticFields::get_offset_of_m_ComponentCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (FPSCounter_t4258725281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[5] = 
{
	FPSCounter_t4258725281::get_offset_of_UpdateInterval_2(),
	FPSCounter_t4258725281::get_offset_of__framesAccumulated_3(),
	FPSCounter_t4258725281::get_offset_of__framesDrawnInTheInterval_4(),
	FPSCounter_t4258725281::get_offset_of__timeLeft_5(),
	FPSCounter_t4258725281::get_offset_of__text_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (GetFocusOnEnable_t3625362509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (ProgressBar_t1074804577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[4] = 
{
	ProgressBar_t1074804577::get_offset_of_ForegroundBar_2(),
	ProgressBar_t1074804577::get_offset_of_PlayerID_3(),
	ProgressBar_t1074804577::get_offset_of__newLocalScale_4(),
	ProgressBar_t1074804577::get_offset_of__newPercent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (SceneViewIcon_t2894348385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (MMControlsTestInputManager_t8107686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (MMPossibleSwipeDirections_t661446340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1986[5] = 
{
	MMPossibleSwipeDirections_t661446340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (SwipeEvent_t1828135924), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (MMSwipeEvent_t3243755981)+ sizeof (RuntimeObject), sizeof(MMSwipeEvent_t3243755981 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1988[5] = 
{
	MMSwipeEvent_t3243755981::get_offset_of_SwipeDirection_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MMSwipeEvent_t3243755981::get_offset_of_SwipeAngle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MMSwipeEvent_t3243755981::get_offset_of_SwipeLength_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MMSwipeEvent_t3243755981::get_offset_of_SwipeOrigin_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MMSwipeEvent_t3243755981::get_offset_of_SwipeDestination_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (MMSwipeZone_t2755251325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[11] = 
{
	MMSwipeZone_t2755251325::get_offset_of_MinimalSwipeLength_2(),
	MMSwipeZone_t2755251325::get_offset_of_MaximumPressLength_3(),
	MMSwipeZone_t2755251325::get_offset_of_ZoneSwiped_4(),
	MMSwipeZone_t2755251325::get_offset_of_ZonePressed_5(),
	MMSwipeZone_t2755251325::get_offset_of_MouseMode_6(),
	MMSwipeZone_t2755251325::get_offset_of__firstTouchPosition_7(),
	MMSwipeZone_t2755251325::get_offset_of__angle_8(),
	MMSwipeZone_t2755251325::get_offset_of__length_9(),
	MMSwipeZone_t2755251325::get_offset_of__destination_10(),
	MMSwipeZone_t2755251325::get_offset_of__deltaSwipe_11(),
	MMSwipeZone_t2755251325::get_offset_of__swipeDirection_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (AxisEvent_t2672331441), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (MMTouchAxis_t2941413966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[9] = 
{
	MMTouchAxis_t2941413966::get_offset_of_AxisPressedFirstTime_2(),
	MMTouchAxis_t2941413966::get_offset_of_AxisReleased_3(),
	MMTouchAxis_t2941413966::get_offset_of_AxisPressed_4(),
	MMTouchAxis_t2941413966::get_offset_of_PressedOpacity_5(),
	MMTouchAxis_t2941413966::get_offset_of_AxisValue_6(),
	MMTouchAxis_t2941413966::get_offset_of_MouseMode_7(),
	MMTouchAxis_t2941413966::get_offset_of_U3CCurrentStateU3Ek__BackingField_8(),
	MMTouchAxis_t2941413966::get_offset_of__canvasGroup_9(),
	MMTouchAxis_t2941413966::get_offset_of__initialOpacity_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (ButtonStates_t2679489820)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1992[5] = 
{
	ButtonStates_t2679489820::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (MMTouchButton_t1318972791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[9] = 
{
	MMTouchButton_t1318972791::get_offset_of_ButtonPressedFirstTime_2(),
	MMTouchButton_t1318972791::get_offset_of_ButtonReleased_3(),
	MMTouchButton_t1318972791::get_offset_of_ButtonPressed_4(),
	MMTouchButton_t1318972791::get_offset_of_PressedOpacity_5(),
	MMTouchButton_t1318972791::get_offset_of_MouseMode_6(),
	MMTouchButton_t1318972791::get_offset_of_U3CCurrentStateU3Ek__BackingField_7(),
	MMTouchButton_t1318972791::get_offset_of__zonePressed_8(),
	MMTouchButton_t1318972791::get_offset_of__canvasGroup_9(),
	MMTouchButton_t1318972791::get_offset_of__initialOpacity_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (ButtonStates_t3760686147)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1994[5] = 
{
	ButtonStates_t3760686147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (MMTouchControls_t1924831068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[5] = 
{
	MMTouchControls_t1924831068::get_offset_of_AutoMobileDetection_2(),
	MMTouchControls_t1924831068::get_offset_of_ForcedMode_3(),
	MMTouchControls_t1924831068::get_offset_of_U3CIsMobileU3Ek__BackingField_4(),
	MMTouchControls_t1924831068::get_offset_of__canvasGroup_5(),
	MMTouchControls_t1924831068::get_offset_of__initialMobileControlsAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (InputForcedMode_t1115639122)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1996[4] = 
{
	InputForcedMode_t1115639122::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (MMTouchDynamicJoystick_t1438601146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[5] = 
{
	MMTouchDynamicJoystick_t1438601146::get_offset_of_JoystickKnobImage_17(),
	MMTouchDynamicJoystick_t1438601146::get_offset_of_RestorePosition_18(),
	MMTouchDynamicJoystick_t1438601146::get_offset_of__initialPosition_19(),
	MMTouchDynamicJoystick_t1438601146::get_offset_of__newPosition_20(),
	MMTouchDynamicJoystick_t1438601146::get_offset_of__knobCanvasGroup_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (JoystickEvent_t500143420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (MMTouchJoystick_t2762864105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[15] = 
{
	MMTouchJoystick_t2762864105::get_offset_of_TargetCamera_2(),
	MMTouchJoystick_t2762864105::get_offset_of_PressedOpacity_3(),
	MMTouchJoystick_t2762864105::get_offset_of_HorizontalAxisEnabled_4(),
	MMTouchJoystick_t2762864105::get_offset_of_VerticalAxisEnabled_5(),
	MMTouchJoystick_t2762864105::get_offset_of_MaxRange_6(),
	MMTouchJoystick_t2762864105::get_offset_of_JoystickValue_7(),
	MMTouchJoystick_t2762864105::get_offset_of_U3CParentCanvasRenderModeU3Ek__BackingField_8(),
	MMTouchJoystick_t2762864105::get_offset_of__neutralPosition_9(),
	MMTouchJoystick_t2762864105::get_offset_of__joystickValue_10(),
	MMTouchJoystick_t2762864105::get_offset_of__canvasRectTransform_11(),
	MMTouchJoystick_t2762864105::get_offset_of__newTargetPosition_12(),
	MMTouchJoystick_t2762864105::get_offset_of__newJoystickPosition_13(),
	MMTouchJoystick_t2762864105::get_offset_of__initialZPosition_14(),
	MMTouchJoystick_t2762864105::get_offset_of__canvasGroup_15(),
	MMTouchJoystick_t2762864105::get_offset_of__initialOpacity_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
