﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// UnityEngine.PostProcessing.MotionBlurModel
struct MotionBlurModel_t3080286123;
// UnityEngine.PostProcessing.PostProcessingModel
struct PostProcessingModel_t540111976;
// UnityEngine.PostProcessing.PostProcessingBehaviour
struct PostProcessingBehaviour_t3229946336;
// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>
struct List_1_t4203178569;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct Dictionary_2_t1572824908;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct Dictionary_2_t1349653261;
// UnityEngine.PostProcessing.MaterialFactory
struct MaterialFactory_t2445948724;
// UnityEngine.PostProcessing.RenderTextureFactory
struct RenderTextureFactory_t1946967824;
// UnityEngine.PostProcessing.PostProcessingContext
struct PostProcessingContext_t2014408948;
// UnityEngine.PostProcessing.BuiltinDebugViewsComponent
struct BuiltinDebugViewsComponent_t2123147871;
// UnityEngine.PostProcessing.AmbientOcclusionComponent
struct AmbientOcclusionComponent_t4130625043;
// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent
struct ScreenSpaceReflectionComponent_t856094247;
// UnityEngine.PostProcessing.FogComponent
struct FogComponent_t3400726830;
// UnityEngine.PostProcessing.MotionBlurComponent
struct MotionBlurComponent_t3686516877;
// UnityEngine.PostProcessing.TaaComponent
struct TaaComponent_t3791749658;
// UnityEngine.PostProcessing.EyeAdaptationComponent
struct EyeAdaptationComponent_t3394805121;
// UnityEngine.PostProcessing.DepthOfFieldComponent
struct DepthOfFieldComponent_t554756766;
// UnityEngine.PostProcessing.BloomComponent
struct BloomComponent_t3791419130;
// UnityEngine.PostProcessing.ChromaticAberrationComponent
struct ChromaticAberrationComponent_t1647263118;
// UnityEngine.PostProcessing.ColorGradingComponent
struct ColorGradingComponent_t1715259467;
// UnityEngine.PostProcessing.UserLutComponent
struct UserLutComponent_t2843161776;
// UnityEngine.PostProcessing.GrainComponent
struct GrainComponent_t866324317;
// UnityEngine.PostProcessing.VignetteComponent
struct VignetteComponent_t3243642943;
// UnityEngine.PostProcessing.DitheringComponent
struct DitheringComponent_t277621267;
// UnityEngine.PostProcessing.FxaaComponent
struct FxaaComponent_t1312385771;
// System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>
struct Dictionary_2_t3095696878;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1444694249;
// UnityEngine.PostProcessing.PostProcessingComponentBase
struct PostProcessingComponentBase_t2731103827;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct BuiltinDebugViewsModel_t1462618840;
// System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>
struct Func_2_t4093140010;
// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct PostProcessingComponentCommandBuffer_1_t2737920729;
// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<System.Object>
struct PostProcessingComponentCommandBuffer_1_t60440757;
// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct PostProcessingComponentCommandBuffer_1_t1664772955;
// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>
struct PostProcessingComponentCommandBuffer_1_t6679325;
// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.FogModel>
struct PostProcessingComponentCommandBuffer_1_t601023342;
// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.MotionBlurModel>
struct PostProcessingComponentCommandBuffer_1_t60620716;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Material
struct Material_t340375123;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct PostProcessingComponentRenderTexture_1_t1236717598;
// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<System.Object>
struct PostProcessingComponentRenderTexture_1_t353423909;
// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ColorGradingModel>
struct PostProcessingComponentRenderTexture_1_t3016333222;
// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.VignetteModel>
struct PostProcessingComponentRenderTexture_1_t118834922;
// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.UserLutModel>
struct PostProcessingComponentRenderTexture_1_t3238393121;
// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.GrainModel>
struct PostProcessingComponentRenderTexture_1_t2721167529;
// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DitheringModel>
struct PostProcessingComponentRenderTexture_1_t3997290437;
// UnityEngine.Event
struct Event_t2956885303;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct ValueCollection_t3288869226;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct ValueCollection_t3065697579;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.PostProcessing.PostProcessingProfile
struct PostProcessingProfile_t724195375;
// UnityEngine.PostProcessing.FogModel
struct FogModel_t3620688749;
// UnityEngine.PostProcessing.AntialiasingModel
struct AntialiasingModel_t1521139388;
// UnityEngine.PostProcessing.AmbientOcclusionModel
struct AmbientOcclusionModel_t389471066;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct ScreenSpaceReflectionModel_t3026344732;
// UnityEngine.PostProcessing.DepthOfFieldModel
struct DepthOfFieldModel_t514067330;
// UnityEngine.PostProcessing.EyeAdaptationModel
struct EyeAdaptationModel_t242823912;
// UnityEngine.PostProcessing.BloomModel
struct BloomModel_t2099727860;
// UnityEngine.PostProcessing.ColorGradingModel
struct ColorGradingModel_t1448048181;
// UnityEngine.PostProcessing.UserLutModel
struct UserLutModel_t1670108080;
// UnityEngine.PostProcessing.ChromaticAberrationModel
struct ChromaticAberrationModel_t3963399853;
// UnityEngine.PostProcessing.GrainModel
struct GrainModel_t1152882488;
// UnityEngine.PostProcessing.VignetteModel
struct VignetteModel_t2845517177;
// UnityEngine.PostProcessing.DitheringModel
struct DitheringModel_t2429005396;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>
struct HashSet_1_t673836907;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1645055638;
// System.ArgumentException
struct ArgumentException_t132251570;
// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>
struct PostProcessingComponent_1_t2319520934;
// UnityEngine.PostProcessing.PostProcessingComponent`1<System.Object>
struct PostProcessingComponent_1_t2373282366;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct ReflectionSettings_t282755190;
// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.AntialiasingModel>
struct PostProcessingComponentRenderTexture_1_t3089424429;
// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AntialiasingModel>
struct PostProcessingComponent_1_t814315590;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1615831949;
// UnityEngine.PostProcessing.TrackballAttribute
struct TrackballAttribute_t219960417;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t3677895545;
// UnityEngine.PostProcessing.TrackballGroupAttribute
struct TrackballGroupAttribute_t624107828;
// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.UserLutModel>
struct PostProcessingComponent_1_t963284282;
// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.VignetteModel>
struct PostProcessingComponent_1_t2138693379;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// UnityEngine.PostProcessing.PostProcessingComponentBase[]
struct PostProcessingComponentBaseU5BU5D_t199974658;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.PostProcessing.PostProcessingComponentBase>
struct IEqualityComparer_1_t543468549;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean,System.Collections.DictionaryEntry>
struct Transform_1_t4041962198;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_t125631422;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>[]
struct KeyValuePair_2U5BU5D_t802031261;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t296309482;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>,System.Collections.DictionaryEntry>
struct Transform_1_t1082526272;
// System.Collections.Generic.HashSet`1/Link<UnityEngine.RenderTexture>[]
struct LinkU5BU5D_t2504560647;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.RenderTexture>
struct IEqualityComparer_1_t4216219451;
// UnityEngine.PostProcessing.ColorGradingCurve
struct ColorGradingCurve_t2000571184;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter
struct ReconstructionFilter_t705677647;
// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter
struct FrameBlendingFilter_t2699796096;
// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray
struct ArrowArray_t303178545;
// UnityEngine.Rendering.RenderTargetIdentifier[]
struct RenderTargetIdentifierU5BU5D_t2742279485;
// UnityEngine.ComputeShader
struct ComputeShader_t317220254;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Type
struct Type_t;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;

extern RuntimeClass* List_1_t4203178569_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m4170144800_RuntimeMethod_var;
extern const uint32_t PostProcessingBehaviour__ctor_m3910102566_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1572824908_il2cpp_TypeInfo_var;
extern RuntimeClass* MaterialFactory_t2445948724_il2cpp_TypeInfo_var;
extern RuntimeClass* RenderTextureFactory_t1946967824_il2cpp_TypeInfo_var;
extern RuntimeClass* PostProcessingContext_t2014408948_il2cpp_TypeInfo_var;
extern RuntimeClass* BuiltinDebugViewsComponent_t2123147871_il2cpp_TypeInfo_var;
extern RuntimeClass* AmbientOcclusionComponent_t4130625043_il2cpp_TypeInfo_var;
extern RuntimeClass* ScreenSpaceReflectionComponent_t856094247_il2cpp_TypeInfo_var;
extern RuntimeClass* FogComponent_t3400726830_il2cpp_TypeInfo_var;
extern RuntimeClass* MotionBlurComponent_t3686516877_il2cpp_TypeInfo_var;
extern RuntimeClass* TaaComponent_t3791749658_il2cpp_TypeInfo_var;
extern RuntimeClass* EyeAdaptationComponent_t3394805121_il2cpp_TypeInfo_var;
extern RuntimeClass* DepthOfFieldComponent_t554756766_il2cpp_TypeInfo_var;
extern RuntimeClass* BloomComponent_t3791419130_il2cpp_TypeInfo_var;
extern RuntimeClass* ChromaticAberrationComponent_t1647263118_il2cpp_TypeInfo_var;
extern RuntimeClass* ColorGradingComponent_t1715259467_il2cpp_TypeInfo_var;
extern RuntimeClass* UserLutComponent_t2843161776_il2cpp_TypeInfo_var;
extern RuntimeClass* GrainComponent_t866324317_il2cpp_TypeInfo_var;
extern RuntimeClass* VignetteComponent_t3243642943_il2cpp_TypeInfo_var;
extern RuntimeClass* DitheringComponent_t277621267_il2cpp_TypeInfo_var;
extern RuntimeClass* FxaaComponent_t1312385771_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t3095696878_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2678073514_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisBuiltinDebugViewsComponent_t2123147871_m1960338779_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisAmbientOcclusionComponent_t4130625043_m2502213982_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisScreenSpaceReflectionComponent_t856094247_m3898891935_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisFogComponent_t3400726830_m2397394497_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisMotionBlurComponent_t3686516877_m2944290405_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisTaaComponent_t3791749658_m1885470620_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisEyeAdaptationComponent_t3394805121_m1462221255_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisDepthOfFieldComponent_t554756766_m2473682395_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisBloomComponent_t3791419130_m3367245233_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisChromaticAberrationComponent_t1647263118_m2769190930_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisColorGradingComponent_t1715259467_m47539266_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisUserLutComponent_t2843161776_m3715666184_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisGrainComponent_t866324317_m3375556804_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisVignetteComponent_t3243642943_m2941218025_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisDitheringComponent_t277621267_m1758707692_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_AddComponent_TisFxaaComponent_t1312385771_m1843850521_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m896571293_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m659292003_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m747737324_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m1451613760_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m425563657_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2340450683_RuntimeMethod_var;
extern const uint32_t PostProcessingBehaviour_OnEnable_m1469190174_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var;
extern const uint32_t PostProcessingBehaviour_OnPreCull_m382834868_MetadataUsageId;
extern const RuntimeMethod* PostProcessingBehaviour_TryExecuteCommandBuffer_TisBuiltinDebugViewsModel_t1462618840_m3493222038_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryExecuteCommandBuffer_TisAmbientOcclusionModel_t389471066_m619521879_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryExecuteCommandBuffer_TisScreenSpaceReflectionModel_t3026344732_m311471412_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryExecuteCommandBuffer_TisFogModel_t3620688749_m3894248309_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryExecuteCommandBuffer_TisMotionBlurModel_t3080286123_m3591009761_RuntimeMethod_var;
extern const uint32_t PostProcessingBehaviour_OnPreRender_m1753639883_MetadataUsageId;
extern const uint32_t PostProcessingBehaviour_OnPostRender_m2861720768_MetadataUsageId;
extern RuntimeClass* Graphics_t783367614_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryPrepareUberImageEffect_TisChromaticAberrationModel_t3963399853_m1586502784_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryPrepareUberImageEffect_TisColorGradingModel_t1448048181_m2160422038_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryPrepareUberImageEffect_TisVignetteModel_t2845517177_m1716742003_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryPrepareUberImageEffect_TisUserLutModel_t1670108080_m2338498891_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryPrepareUberImageEffect_TisGrainModel_t1152882488_m3807764775_RuntimeMethod_var;
extern const RuntimeMethod* PostProcessingBehaviour_TryPrepareUberImageEffect_TisDitheringModel_t2429005396_m570759740_RuntimeMethod_var;
extern String_t* _stringLiteral1580053373;
extern String_t* _stringLiteral3422572598;
extern String_t* _stringLiteral1699430022;
extern String_t* _stringLiteral2853687220;
extern const uint32_t PostProcessingBehaviour_OnRenderImage_m561762982_MetadataUsageId;
extern const uint32_t PostProcessingBehaviour_OnGUI_m2377759815_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_get_Values_m907585786_RuntimeMethod_var;
extern const RuntimeMethod* ValueCollection_GetEnumerator_m141340968_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1441963613_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m648688154_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m1482434896_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1047154078_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3868019798_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m3961351021_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m1773456631_RuntimeMethod_var;
extern const uint32_t PostProcessingBehaviour_OnDisable_m3454750292_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m2066271542_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3820558532_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m1176910999_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m3146329666_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1572460420_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3116365360_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2110019143_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1448434680_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m1186219162_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m728013831_RuntimeMethod_var;
extern const uint32_t PostProcessingBehaviour_CheckObservers_m3285018848_MetadataUsageId;
extern const uint32_t PostProcessingBehaviour_DisableComponents_m1054770722_MetadataUsageId;
extern RuntimeClass* BuiltinDebugViewsModel_t1462618840_il2cpp_TypeInfo_var;
extern RuntimeClass* FogModel_t3620688749_il2cpp_TypeInfo_var;
extern RuntimeClass* AntialiasingModel_t1521139388_il2cpp_TypeInfo_var;
extern RuntimeClass* AmbientOcclusionModel_t389471066_il2cpp_TypeInfo_var;
extern RuntimeClass* ScreenSpaceReflectionModel_t3026344732_il2cpp_TypeInfo_var;
extern RuntimeClass* DepthOfFieldModel_t514067330_il2cpp_TypeInfo_var;
extern RuntimeClass* MotionBlurModel_t3080286123_il2cpp_TypeInfo_var;
extern RuntimeClass* EyeAdaptationModel_t242823912_il2cpp_TypeInfo_var;
extern RuntimeClass* BloomModel_t2099727860_il2cpp_TypeInfo_var;
extern RuntimeClass* ColorGradingModel_t1448048181_il2cpp_TypeInfo_var;
extern RuntimeClass* UserLutModel_t1670108080_il2cpp_TypeInfo_var;
extern RuntimeClass* ChromaticAberrationModel_t3963399853_il2cpp_TypeInfo_var;
extern RuntimeClass* GrainModel_t1152882488_il2cpp_TypeInfo_var;
extern RuntimeClass* VignetteModel_t2845517177_il2cpp_TypeInfo_var;
extern RuntimeClass* DitheringModel_t2429005396_il2cpp_TypeInfo_var;
extern const uint32_t PostProcessingProfile__ctor_m2477575403_MetadataUsageId;
extern RuntimeClass* HashSet_1_t673836907_il2cpp_TypeInfo_var;
extern const RuntimeMethod* HashSet_1__ctor_m1993572700_RuntimeMethod_var;
extern const uint32_t RenderTextureFactory__ctor_m1345809438_MetadataUsageId;
extern String_t* _stringLiteral3857694337;
extern const uint32_t RenderTextureFactory_Get_m169036867_MetadataUsageId;
extern const RuntimeMethod* HashSet_1_Add_m2910627594_RuntimeMethod_var;
extern const uint32_t RenderTextureFactory_Get_m1772850884_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern const RuntimeMethod* HashSet_1_Contains_m3753409409_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Remove_m467255894_RuntimeMethod_var;
extern String_t* _stringLiteral1950092709;
extern const uint32_t RenderTextureFactory_Release_m717800481_MetadataUsageId;
extern const RuntimeMethod* HashSet_1_GetEnumerator_m4157320476_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3525666044_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m519760291_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Clear_m552370813_RuntimeMethod_var;
extern const uint32_t RenderTextureFactory_ReleaseAll_m3329667721_MetadataUsageId;
extern RuntimeClass* Int32U5BU5D_t385246372_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PostProcessingComponentCommandBuffer_1__ctor_m1811253078_RuntimeMethod_var;
extern const uint32_t ScreenSpaceReflectionComponent__ctor_m2879296341_MetadataUsageId;
extern const RuntimeMethod* PostProcessingComponent_1_get_model_m3701723778_RuntimeMethod_var;
extern const uint32_t ScreenSpaceReflectionComponent_get_active_m243829711_MetadataUsageId;
extern String_t* _stringLiteral3996654635;
extern String_t* _stringLiteral1267771280;
extern String_t* _stringLiteral864486753;
extern String_t* _stringLiteral2430570694;
extern String_t* _stringLiteral2027286167;
extern const uint32_t ScreenSpaceReflectionComponent_OnEnable_m346974116_MetadataUsageId;
extern String_t* _stringLiteral2578318749;
extern const uint32_t ScreenSpaceReflectionComponent_GetName_m2112635201_MetadataUsageId;
extern RuntimeClass* Uniforms_t2970573890_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector4_t3319028937_il2cpp_TypeInfo_var;
extern RuntimeClass* Matrix4x4_t1817901843_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3225738970;
extern const uint32_t ScreenSpaceReflectionComponent_PopulateCommandBuffer_m3120254331_MetadataUsageId;
extern String_t* _stringLiteral1292081225;
extern String_t* _stringLiteral3590979936;
extern String_t* _stringLiteral3812922687;
extern String_t* _stringLiteral3220650816;
extern String_t* _stringLiteral550721294;
extern String_t* _stringLiteral591083681;
extern String_t* _stringLiteral4226617077;
extern String_t* _stringLiteral2380614554;
extern String_t* _stringLiteral2030135646;
extern String_t* _stringLiteral2271957017;
extern String_t* _stringLiteral2534592956;
extern String_t* _stringLiteral3503710369;
extern String_t* _stringLiteral4081118877;
extern String_t* _stringLiteral3535566692;
extern String_t* _stringLiteral1464815389;
extern String_t* _stringLiteral3054324692;
extern String_t* _stringLiteral4044860599;
extern String_t* _stringLiteral2857999720;
extern String_t* _stringLiteral2381300568;
extern String_t* _stringLiteral1469005573;
extern String_t* _stringLiteral2262278099;
extern String_t* _stringLiteral601799897;
extern String_t* _stringLiteral3142184566;
extern String_t* _stringLiteral1562959127;
extern String_t* _stringLiteral2920707233;
extern String_t* _stringLiteral2060163631;
extern String_t* _stringLiteral3346007495;
extern String_t* _stringLiteral4055059096;
extern String_t* _stringLiteral1923191447;
extern String_t* _stringLiteral735047140;
extern String_t* _stringLiteral4037140854;
extern String_t* _stringLiteral2224716414;
extern String_t* _stringLiteral782944872;
extern String_t* _stringLiteral3700046144;
extern String_t* _stringLiteral1660455728;
extern const uint32_t Uniforms__cctor_m2613778490_MetadataUsageId;
struct ReflectionSettings_t282755190_marshaled_pinvoke;
struct ReflectionSettings_t282755190;;
struct ReflectionSettings_t282755190_marshaled_pinvoke;;
struct ReflectionSettings_t282755190_marshaled_com;
struct ReflectionSettings_t282755190_marshaled_com;;
extern RuntimeClass* RenderBufferU5BU5D_t1615831949_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PostProcessingComponentRenderTexture_1__ctor_m1485528837_RuntimeMethod_var;
extern const uint32_t TaaComponent__ctor_m675959233_MetadataUsageId;
extern const RuntimeMethod* PostProcessingComponent_1_get_model_m3744888901_RuntimeMethod_var;
extern const uint32_t TaaComponent_get_active_m3051132930_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Uniforms_t3024963833_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Func_2_Invoke_m886748628_RuntimeMethod_var;
extern String_t* _stringLiteral1950142242;
extern const uint32_t TaaComponent_SetProjectionMatrix_m2316589171_MetadataUsageId;
extern String_t* _stringLiteral504576554;
extern const uint32_t TaaComponent_Render_m2638556758_MetadataUsageId;
extern const uint32_t TaaComponent_GetPerspectiveProjectionMatrix_m2335334281_MetadataUsageId;
extern const uint32_t TaaComponent_GetOrthographicProjectionMatrix_m3494165154_MetadataUsageId;
extern const uint32_t TaaComponent_OnDisable_m1137752543_MetadataUsageId;
extern String_t* _stringLiteral108671656;
extern String_t* _stringLiteral1815697245;
extern String_t* _stringLiteral2390542409;
extern String_t* _stringLiteral3181956072;
extern String_t* _stringLiteral3184621405;
extern const uint32_t Uniforms__cctor_m1642638623_MetadataUsageId;
extern const RuntimeMethod* PostProcessingComponentRenderTexture_1__ctor_m3643001101_RuntimeMethod_var;
extern const uint32_t UserLutComponent__ctor_m3557887131_MetadataUsageId;
extern const RuntimeMethod* PostProcessingComponent_1_get_model_m2956985483_RuntimeMethod_var;
extern const uint32_t UserLutComponent_get_active_m1788341588_MetadataUsageId;
extern RuntimeClass* Uniforms_t1046717683_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3293862872;
extern const uint32_t UserLutComponent_Prepare_m1158977240_MetadataUsageId;
extern RuntimeClass* GUI_t1624858472_il2cpp_TypeInfo_var;
extern const uint32_t UserLutComponent_OnGUI_m2614562252_MetadataUsageId;
extern String_t* _stringLiteral3922522743;
extern String_t* _stringLiteral2045738677;
extern const uint32_t Uniforms__cctor_m521797190_MetadataUsageId;
extern const RuntimeMethod* PostProcessingComponentRenderTexture_1__ctor_m2651217025_RuntimeMethod_var;
extern const uint32_t VignetteComponent__ctor_m3640704423_MetadataUsageId;
extern const RuntimeMethod* PostProcessingComponent_1_get_model_m2875105431_RuntimeMethod_var;
extern const uint32_t VignetteComponent_get_active_m4124729355_MetadataUsageId;
extern RuntimeClass* Uniforms_t2205824134_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3727145709;
extern String_t* _stringLiteral3652629709;
extern const uint32_t VignetteComponent_Prepare_m3452838302_MetadataUsageId;
extern String_t* _stringLiteral1569778134;
extern String_t* _stringLiteral1253245767;
extern String_t* _stringLiteral1478126650;
extern String_t* _stringLiteral4174543017;
extern String_t* _stringLiteral629754547;
extern const uint32_t Uniforms__cctor_m4093978548_MetadataUsageId;

struct StringU5BU5D_t1281789340;
struct Int32U5BU5D_t385246372;
struct RenderBufferU5BU5D_t1615831949;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef RENDERTEXTUREFACTORY_T1946967824_H
#define RENDERTEXTUREFACTORY_T1946967824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.RenderTextureFactory
struct  RenderTextureFactory_t1946967824  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture> UnityEngine.PostProcessing.RenderTextureFactory::m_TemporaryRTs
	HashSet_1_t673836907 * ___m_TemporaryRTs_0;

public:
	inline static int32_t get_offset_of_m_TemporaryRTs_0() { return static_cast<int32_t>(offsetof(RenderTextureFactory_t1946967824, ___m_TemporaryRTs_0)); }
	inline HashSet_1_t673836907 * get_m_TemporaryRTs_0() const { return ___m_TemporaryRTs_0; }
	inline HashSet_1_t673836907 ** get_address_of_m_TemporaryRTs_0() { return &___m_TemporaryRTs_0; }
	inline void set_m_TemporaryRTs_0(HashSet_1_t673836907 * value)
	{
		___m_TemporaryRTs_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TemporaryRTs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFACTORY_T1946967824_H
#ifndef POSTPROCESSINGCONTEXT_T2014408948_H
#define POSTPROCESSINGCONTEXT_T2014408948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingContext
struct  PostProcessingContext_t2014408948  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingContext::profile
	PostProcessingProfile_t724195375 * ___profile_0;
	// UnityEngine.Camera UnityEngine.PostProcessing.PostProcessingContext::camera
	Camera_t4157153871 * ___camera_1;
	// UnityEngine.PostProcessing.MaterialFactory UnityEngine.PostProcessing.PostProcessingContext::materialFactory
	MaterialFactory_t2445948724 * ___materialFactory_2;
	// UnityEngine.PostProcessing.RenderTextureFactory UnityEngine.PostProcessing.PostProcessingContext::renderTextureFactory
	RenderTextureFactory_t1946967824 * ___renderTextureFactory_3;
	// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::<interrupted>k__BackingField
	bool ___U3CinterruptedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_profile_0() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___profile_0)); }
	inline PostProcessingProfile_t724195375 * get_profile_0() const { return ___profile_0; }
	inline PostProcessingProfile_t724195375 ** get_address_of_profile_0() { return &___profile_0; }
	inline void set_profile_0(PostProcessingProfile_t724195375 * value)
	{
		___profile_0 = value;
		Il2CppCodeGenWriteBarrier((&___profile_0), value);
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___camera_1)); }
	inline Camera_t4157153871 * get_camera_1() const { return ___camera_1; }
	inline Camera_t4157153871 ** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(Camera_t4157153871 * value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}

	inline static int32_t get_offset_of_materialFactory_2() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___materialFactory_2)); }
	inline MaterialFactory_t2445948724 * get_materialFactory_2() const { return ___materialFactory_2; }
	inline MaterialFactory_t2445948724 ** get_address_of_materialFactory_2() { return &___materialFactory_2; }
	inline void set_materialFactory_2(MaterialFactory_t2445948724 * value)
	{
		___materialFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&___materialFactory_2), value);
	}

	inline static int32_t get_offset_of_renderTextureFactory_3() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___renderTextureFactory_3)); }
	inline RenderTextureFactory_t1946967824 * get_renderTextureFactory_3() const { return ___renderTextureFactory_3; }
	inline RenderTextureFactory_t1946967824 ** get_address_of_renderTextureFactory_3() { return &___renderTextureFactory_3; }
	inline void set_renderTextureFactory_3(RenderTextureFactory_t1946967824 * value)
	{
		___renderTextureFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&___renderTextureFactory_3), value);
	}

	inline static int32_t get_offset_of_U3CinterruptedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___U3CinterruptedU3Ek__BackingField_4)); }
	inline bool get_U3CinterruptedU3Ek__BackingField_4() const { return ___U3CinterruptedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CinterruptedU3Ek__BackingField_4() { return &___U3CinterruptedU3Ek__BackingField_4; }
	inline void set_U3CinterruptedU3Ek__BackingField_4(bool value)
	{
		___U3CinterruptedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCONTEXT_T2014408948_H
#ifndef UNIFORMS_T2205824134_H
#define UNIFORMS_T2205824134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteComponent/Uniforms
struct  Uniforms_t2205824134  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2205824134_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Color
	int32_t ____Vignette_Color_0;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Center
	int32_t ____Vignette_Center_1;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Settings
	int32_t ____Vignette_Settings_2;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Mask
	int32_t ____Vignette_Mask_3;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Opacity
	int32_t ____Vignette_Opacity_4;

public:
	inline static int32_t get_offset_of__Vignette_Color_0() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Color_0)); }
	inline int32_t get__Vignette_Color_0() const { return ____Vignette_Color_0; }
	inline int32_t* get_address_of__Vignette_Color_0() { return &____Vignette_Color_0; }
	inline void set__Vignette_Color_0(int32_t value)
	{
		____Vignette_Color_0 = value;
	}

	inline static int32_t get_offset_of__Vignette_Center_1() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Center_1)); }
	inline int32_t get__Vignette_Center_1() const { return ____Vignette_Center_1; }
	inline int32_t* get_address_of__Vignette_Center_1() { return &____Vignette_Center_1; }
	inline void set__Vignette_Center_1(int32_t value)
	{
		____Vignette_Center_1 = value;
	}

	inline static int32_t get_offset_of__Vignette_Settings_2() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Settings_2)); }
	inline int32_t get__Vignette_Settings_2() const { return ____Vignette_Settings_2; }
	inline int32_t* get_address_of__Vignette_Settings_2() { return &____Vignette_Settings_2; }
	inline void set__Vignette_Settings_2(int32_t value)
	{
		____Vignette_Settings_2 = value;
	}

	inline static int32_t get_offset_of__Vignette_Mask_3() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Mask_3)); }
	inline int32_t get__Vignette_Mask_3() const { return ____Vignette_Mask_3; }
	inline int32_t* get_address_of__Vignette_Mask_3() { return &____Vignette_Mask_3; }
	inline void set__Vignette_Mask_3(int32_t value)
	{
		____Vignette_Mask_3 = value;
	}

	inline static int32_t get_offset_of__Vignette_Opacity_4() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Opacity_4)); }
	inline int32_t get__Vignette_Opacity_4() const { return ____Vignette_Opacity_4; }
	inline int32_t* get_address_of__Vignette_Opacity_4() { return &____Vignette_Opacity_4; }
	inline void set__Vignette_Opacity_4(int32_t value)
	{
		____Vignette_Opacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2205824134_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef UNIFORMS_T1046717683_H
#define UNIFORMS_T1046717683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutComponent/Uniforms
struct  Uniforms_t1046717683  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1046717683_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.UserLutComponent/Uniforms::_UserLut
	int32_t ____UserLut_0;
	// System.Int32 UnityEngine.PostProcessing.UserLutComponent/Uniforms::_UserLut_Params
	int32_t ____UserLut_Params_1;

public:
	inline static int32_t get_offset_of__UserLut_0() { return static_cast<int32_t>(offsetof(Uniforms_t1046717683_StaticFields, ____UserLut_0)); }
	inline int32_t get__UserLut_0() const { return ____UserLut_0; }
	inline int32_t* get_address_of__UserLut_0() { return &____UserLut_0; }
	inline void set__UserLut_0(int32_t value)
	{
		____UserLut_0 = value;
	}

	inline static int32_t get_offset_of__UserLut_Params_1() { return static_cast<int32_t>(offsetof(Uniforms_t1046717683_StaticFields, ____UserLut_Params_1)); }
	inline int32_t get__UserLut_Params_1() const { return ____UserLut_Params_1; }
	inline int32_t* get_address_of__UserLut_Params_1() { return &____UserLut_Params_1; }
	inline void set__UserLut_Params_1(int32_t value)
	{
		____UserLut_Params_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1046717683_H
#ifndef DICTIONARY_2_T3095696878_H
#define DICTIONARY_2_T3095696878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>
struct  Dictionary_2_t3095696878  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	PostProcessingComponentBaseU5BU5D_t199974658* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	BooleanU5BU5D_t2897418192* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___keySlots_6)); }
	inline PostProcessingComponentBaseU5BU5D_t199974658* get_keySlots_6() const { return ___keySlots_6; }
	inline PostProcessingComponentBaseU5BU5D_t199974658** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(PostProcessingComponentBaseU5BU5D_t199974658* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___valueSlots_7)); }
	inline BooleanU5BU5D_t2897418192* get_valueSlots_7() const { return ___valueSlots_7; }
	inline BooleanU5BU5D_t2897418192** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(BooleanU5BU5D_t2897418192* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3095696878_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4041962198 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3095696878_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4041962198 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4041962198 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4041962198 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3095696878_H
#ifndef UNIFORMS_T3024963833_H
#define UNIFORMS_T3024963833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TaaComponent/Uniforms
struct  Uniforms_t3024963833  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3024963833_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_Jitter
	int32_t ____Jitter_0;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_SharpenParameters
	int32_t ____SharpenParameters_1;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_FinalBlendParameters
	int32_t ____FinalBlendParameters_2;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_HistoryTex
	int32_t ____HistoryTex_3;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_MainTex
	int32_t ____MainTex_4;

public:
	inline static int32_t get_offset_of__Jitter_0() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____Jitter_0)); }
	inline int32_t get__Jitter_0() const { return ____Jitter_0; }
	inline int32_t* get_address_of__Jitter_0() { return &____Jitter_0; }
	inline void set__Jitter_0(int32_t value)
	{
		____Jitter_0 = value;
	}

	inline static int32_t get_offset_of__SharpenParameters_1() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____SharpenParameters_1)); }
	inline int32_t get__SharpenParameters_1() const { return ____SharpenParameters_1; }
	inline int32_t* get_address_of__SharpenParameters_1() { return &____SharpenParameters_1; }
	inline void set__SharpenParameters_1(int32_t value)
	{
		____SharpenParameters_1 = value;
	}

	inline static int32_t get_offset_of__FinalBlendParameters_2() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____FinalBlendParameters_2)); }
	inline int32_t get__FinalBlendParameters_2() const { return ____FinalBlendParameters_2; }
	inline int32_t* get_address_of__FinalBlendParameters_2() { return &____FinalBlendParameters_2; }
	inline void set__FinalBlendParameters_2(int32_t value)
	{
		____FinalBlendParameters_2 = value;
	}

	inline static int32_t get_offset_of__HistoryTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____HistoryTex_3)); }
	inline int32_t get__HistoryTex_3() const { return ____HistoryTex_3; }
	inline int32_t* get_address_of__HistoryTex_3() { return &____HistoryTex_3; }
	inline void set__HistoryTex_3(int32_t value)
	{
		____HistoryTex_3 = value;
	}

	inline static int32_t get_offset_of__MainTex_4() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____MainTex_4)); }
	inline int32_t get__MainTex_4() const { return ____MainTex_4; }
	inline int32_t* get_address_of__MainTex_4() { return &____MainTex_4; }
	inline void set__MainTex_4(int32_t value)
	{
		____MainTex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3024963833_H
#ifndef UNIFORMS_T2970573890_H
#define UNIFORMS_T2970573890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms
struct  Uniforms_t2970573890  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2970573890_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_RayStepSize
	int32_t ____RayStepSize_0;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_AdditiveReflection
	int32_t ____AdditiveReflection_1;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_BilateralUpsampling
	int32_t ____BilateralUpsampling_2;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TreatBackfaceHitAsMiss
	int32_t ____TreatBackfaceHitAsMiss_3;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_AllowBackwardsRays
	int32_t ____AllowBackwardsRays_4;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TraceBehindObjects
	int32_t ____TraceBehindObjects_5;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_MaxSteps
	int32_t ____MaxSteps_6;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FullResolutionFiltering
	int32_t ____FullResolutionFiltering_7;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HalfResolution
	int32_t ____HalfResolution_8;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HighlightSuppression
	int32_t ____HighlightSuppression_9;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_PixelsPerMeterAtOneMeter
	int32_t ____PixelsPerMeterAtOneMeter_10;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ScreenEdgeFading
	int32_t ____ScreenEdgeFading_11;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ReflectionBlur
	int32_t ____ReflectionBlur_12;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_MaxRayTraceDistance
	int32_t ____MaxRayTraceDistance_13;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FadeDistance
	int32_t ____FadeDistance_14;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_LayerThickness
	int32_t ____LayerThickness_15;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_SSRMultiplier
	int32_t ____SSRMultiplier_16;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FresnelFade
	int32_t ____FresnelFade_17;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FresnelFadePower
	int32_t ____FresnelFadePower_18;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ReflectionBufferSize
	int32_t ____ReflectionBufferSize_19;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ScreenSize
	int32_t ____ScreenSize_20;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_InvScreenSize
	int32_t ____InvScreenSize_21;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ProjInfo
	int32_t ____ProjInfo_22;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CameraClipInfo
	int32_t ____CameraClipInfo_23;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ProjectToPixelMatrix
	int32_t ____ProjectToPixelMatrix_24;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_WorldToCameraMatrix
	int32_t ____WorldToCameraMatrix_25;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CameraToWorldMatrix
	int32_t ____CameraToWorldMatrix_26;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_Axis
	int32_t ____Axis_27;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CurrentMipLevel
	int32_t ____CurrentMipLevel_28;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_NormalAndRoughnessTexture
	int32_t ____NormalAndRoughnessTexture_29;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HitPointTexture
	int32_t ____HitPointTexture_30;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_BlurTexture
	int32_t ____BlurTexture_31;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FilteredReflections
	int32_t ____FilteredReflections_32;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FinalReflectionTexture
	int32_t ____FinalReflectionTexture_33;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TempTexture
	int32_t ____TempTexture_34;

public:
	inline static int32_t get_offset_of__RayStepSize_0() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____RayStepSize_0)); }
	inline int32_t get__RayStepSize_0() const { return ____RayStepSize_0; }
	inline int32_t* get_address_of__RayStepSize_0() { return &____RayStepSize_0; }
	inline void set__RayStepSize_0(int32_t value)
	{
		____RayStepSize_0 = value;
	}

	inline static int32_t get_offset_of__AdditiveReflection_1() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____AdditiveReflection_1)); }
	inline int32_t get__AdditiveReflection_1() const { return ____AdditiveReflection_1; }
	inline int32_t* get_address_of__AdditiveReflection_1() { return &____AdditiveReflection_1; }
	inline void set__AdditiveReflection_1(int32_t value)
	{
		____AdditiveReflection_1 = value;
	}

	inline static int32_t get_offset_of__BilateralUpsampling_2() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____BilateralUpsampling_2)); }
	inline int32_t get__BilateralUpsampling_2() const { return ____BilateralUpsampling_2; }
	inline int32_t* get_address_of__BilateralUpsampling_2() { return &____BilateralUpsampling_2; }
	inline void set__BilateralUpsampling_2(int32_t value)
	{
		____BilateralUpsampling_2 = value;
	}

	inline static int32_t get_offset_of__TreatBackfaceHitAsMiss_3() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TreatBackfaceHitAsMiss_3)); }
	inline int32_t get__TreatBackfaceHitAsMiss_3() const { return ____TreatBackfaceHitAsMiss_3; }
	inline int32_t* get_address_of__TreatBackfaceHitAsMiss_3() { return &____TreatBackfaceHitAsMiss_3; }
	inline void set__TreatBackfaceHitAsMiss_3(int32_t value)
	{
		____TreatBackfaceHitAsMiss_3 = value;
	}

	inline static int32_t get_offset_of__AllowBackwardsRays_4() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____AllowBackwardsRays_4)); }
	inline int32_t get__AllowBackwardsRays_4() const { return ____AllowBackwardsRays_4; }
	inline int32_t* get_address_of__AllowBackwardsRays_4() { return &____AllowBackwardsRays_4; }
	inline void set__AllowBackwardsRays_4(int32_t value)
	{
		____AllowBackwardsRays_4 = value;
	}

	inline static int32_t get_offset_of__TraceBehindObjects_5() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TraceBehindObjects_5)); }
	inline int32_t get__TraceBehindObjects_5() const { return ____TraceBehindObjects_5; }
	inline int32_t* get_address_of__TraceBehindObjects_5() { return &____TraceBehindObjects_5; }
	inline void set__TraceBehindObjects_5(int32_t value)
	{
		____TraceBehindObjects_5 = value;
	}

	inline static int32_t get_offset_of__MaxSteps_6() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____MaxSteps_6)); }
	inline int32_t get__MaxSteps_6() const { return ____MaxSteps_6; }
	inline int32_t* get_address_of__MaxSteps_6() { return &____MaxSteps_6; }
	inline void set__MaxSteps_6(int32_t value)
	{
		____MaxSteps_6 = value;
	}

	inline static int32_t get_offset_of__FullResolutionFiltering_7() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FullResolutionFiltering_7)); }
	inline int32_t get__FullResolutionFiltering_7() const { return ____FullResolutionFiltering_7; }
	inline int32_t* get_address_of__FullResolutionFiltering_7() { return &____FullResolutionFiltering_7; }
	inline void set__FullResolutionFiltering_7(int32_t value)
	{
		____FullResolutionFiltering_7 = value;
	}

	inline static int32_t get_offset_of__HalfResolution_8() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HalfResolution_8)); }
	inline int32_t get__HalfResolution_8() const { return ____HalfResolution_8; }
	inline int32_t* get_address_of__HalfResolution_8() { return &____HalfResolution_8; }
	inline void set__HalfResolution_8(int32_t value)
	{
		____HalfResolution_8 = value;
	}

	inline static int32_t get_offset_of__HighlightSuppression_9() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HighlightSuppression_9)); }
	inline int32_t get__HighlightSuppression_9() const { return ____HighlightSuppression_9; }
	inline int32_t* get_address_of__HighlightSuppression_9() { return &____HighlightSuppression_9; }
	inline void set__HighlightSuppression_9(int32_t value)
	{
		____HighlightSuppression_9 = value;
	}

	inline static int32_t get_offset_of__PixelsPerMeterAtOneMeter_10() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____PixelsPerMeterAtOneMeter_10)); }
	inline int32_t get__PixelsPerMeterAtOneMeter_10() const { return ____PixelsPerMeterAtOneMeter_10; }
	inline int32_t* get_address_of__PixelsPerMeterAtOneMeter_10() { return &____PixelsPerMeterAtOneMeter_10; }
	inline void set__PixelsPerMeterAtOneMeter_10(int32_t value)
	{
		____PixelsPerMeterAtOneMeter_10 = value;
	}

	inline static int32_t get_offset_of__ScreenEdgeFading_11() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ScreenEdgeFading_11)); }
	inline int32_t get__ScreenEdgeFading_11() const { return ____ScreenEdgeFading_11; }
	inline int32_t* get_address_of__ScreenEdgeFading_11() { return &____ScreenEdgeFading_11; }
	inline void set__ScreenEdgeFading_11(int32_t value)
	{
		____ScreenEdgeFading_11 = value;
	}

	inline static int32_t get_offset_of__ReflectionBlur_12() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ReflectionBlur_12)); }
	inline int32_t get__ReflectionBlur_12() const { return ____ReflectionBlur_12; }
	inline int32_t* get_address_of__ReflectionBlur_12() { return &____ReflectionBlur_12; }
	inline void set__ReflectionBlur_12(int32_t value)
	{
		____ReflectionBlur_12 = value;
	}

	inline static int32_t get_offset_of__MaxRayTraceDistance_13() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____MaxRayTraceDistance_13)); }
	inline int32_t get__MaxRayTraceDistance_13() const { return ____MaxRayTraceDistance_13; }
	inline int32_t* get_address_of__MaxRayTraceDistance_13() { return &____MaxRayTraceDistance_13; }
	inline void set__MaxRayTraceDistance_13(int32_t value)
	{
		____MaxRayTraceDistance_13 = value;
	}

	inline static int32_t get_offset_of__FadeDistance_14() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FadeDistance_14)); }
	inline int32_t get__FadeDistance_14() const { return ____FadeDistance_14; }
	inline int32_t* get_address_of__FadeDistance_14() { return &____FadeDistance_14; }
	inline void set__FadeDistance_14(int32_t value)
	{
		____FadeDistance_14 = value;
	}

	inline static int32_t get_offset_of__LayerThickness_15() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____LayerThickness_15)); }
	inline int32_t get__LayerThickness_15() const { return ____LayerThickness_15; }
	inline int32_t* get_address_of__LayerThickness_15() { return &____LayerThickness_15; }
	inline void set__LayerThickness_15(int32_t value)
	{
		____LayerThickness_15 = value;
	}

	inline static int32_t get_offset_of__SSRMultiplier_16() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____SSRMultiplier_16)); }
	inline int32_t get__SSRMultiplier_16() const { return ____SSRMultiplier_16; }
	inline int32_t* get_address_of__SSRMultiplier_16() { return &____SSRMultiplier_16; }
	inline void set__SSRMultiplier_16(int32_t value)
	{
		____SSRMultiplier_16 = value;
	}

	inline static int32_t get_offset_of__FresnelFade_17() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FresnelFade_17)); }
	inline int32_t get__FresnelFade_17() const { return ____FresnelFade_17; }
	inline int32_t* get_address_of__FresnelFade_17() { return &____FresnelFade_17; }
	inline void set__FresnelFade_17(int32_t value)
	{
		____FresnelFade_17 = value;
	}

	inline static int32_t get_offset_of__FresnelFadePower_18() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FresnelFadePower_18)); }
	inline int32_t get__FresnelFadePower_18() const { return ____FresnelFadePower_18; }
	inline int32_t* get_address_of__FresnelFadePower_18() { return &____FresnelFadePower_18; }
	inline void set__FresnelFadePower_18(int32_t value)
	{
		____FresnelFadePower_18 = value;
	}

	inline static int32_t get_offset_of__ReflectionBufferSize_19() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ReflectionBufferSize_19)); }
	inline int32_t get__ReflectionBufferSize_19() const { return ____ReflectionBufferSize_19; }
	inline int32_t* get_address_of__ReflectionBufferSize_19() { return &____ReflectionBufferSize_19; }
	inline void set__ReflectionBufferSize_19(int32_t value)
	{
		____ReflectionBufferSize_19 = value;
	}

	inline static int32_t get_offset_of__ScreenSize_20() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ScreenSize_20)); }
	inline int32_t get__ScreenSize_20() const { return ____ScreenSize_20; }
	inline int32_t* get_address_of__ScreenSize_20() { return &____ScreenSize_20; }
	inline void set__ScreenSize_20(int32_t value)
	{
		____ScreenSize_20 = value;
	}

	inline static int32_t get_offset_of__InvScreenSize_21() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____InvScreenSize_21)); }
	inline int32_t get__InvScreenSize_21() const { return ____InvScreenSize_21; }
	inline int32_t* get_address_of__InvScreenSize_21() { return &____InvScreenSize_21; }
	inline void set__InvScreenSize_21(int32_t value)
	{
		____InvScreenSize_21 = value;
	}

	inline static int32_t get_offset_of__ProjInfo_22() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ProjInfo_22)); }
	inline int32_t get__ProjInfo_22() const { return ____ProjInfo_22; }
	inline int32_t* get_address_of__ProjInfo_22() { return &____ProjInfo_22; }
	inline void set__ProjInfo_22(int32_t value)
	{
		____ProjInfo_22 = value;
	}

	inline static int32_t get_offset_of__CameraClipInfo_23() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CameraClipInfo_23)); }
	inline int32_t get__CameraClipInfo_23() const { return ____CameraClipInfo_23; }
	inline int32_t* get_address_of__CameraClipInfo_23() { return &____CameraClipInfo_23; }
	inline void set__CameraClipInfo_23(int32_t value)
	{
		____CameraClipInfo_23 = value;
	}

	inline static int32_t get_offset_of__ProjectToPixelMatrix_24() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ProjectToPixelMatrix_24)); }
	inline int32_t get__ProjectToPixelMatrix_24() const { return ____ProjectToPixelMatrix_24; }
	inline int32_t* get_address_of__ProjectToPixelMatrix_24() { return &____ProjectToPixelMatrix_24; }
	inline void set__ProjectToPixelMatrix_24(int32_t value)
	{
		____ProjectToPixelMatrix_24 = value;
	}

	inline static int32_t get_offset_of__WorldToCameraMatrix_25() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____WorldToCameraMatrix_25)); }
	inline int32_t get__WorldToCameraMatrix_25() const { return ____WorldToCameraMatrix_25; }
	inline int32_t* get_address_of__WorldToCameraMatrix_25() { return &____WorldToCameraMatrix_25; }
	inline void set__WorldToCameraMatrix_25(int32_t value)
	{
		____WorldToCameraMatrix_25 = value;
	}

	inline static int32_t get_offset_of__CameraToWorldMatrix_26() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CameraToWorldMatrix_26)); }
	inline int32_t get__CameraToWorldMatrix_26() const { return ____CameraToWorldMatrix_26; }
	inline int32_t* get_address_of__CameraToWorldMatrix_26() { return &____CameraToWorldMatrix_26; }
	inline void set__CameraToWorldMatrix_26(int32_t value)
	{
		____CameraToWorldMatrix_26 = value;
	}

	inline static int32_t get_offset_of__Axis_27() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____Axis_27)); }
	inline int32_t get__Axis_27() const { return ____Axis_27; }
	inline int32_t* get_address_of__Axis_27() { return &____Axis_27; }
	inline void set__Axis_27(int32_t value)
	{
		____Axis_27 = value;
	}

	inline static int32_t get_offset_of__CurrentMipLevel_28() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CurrentMipLevel_28)); }
	inline int32_t get__CurrentMipLevel_28() const { return ____CurrentMipLevel_28; }
	inline int32_t* get_address_of__CurrentMipLevel_28() { return &____CurrentMipLevel_28; }
	inline void set__CurrentMipLevel_28(int32_t value)
	{
		____CurrentMipLevel_28 = value;
	}

	inline static int32_t get_offset_of__NormalAndRoughnessTexture_29() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____NormalAndRoughnessTexture_29)); }
	inline int32_t get__NormalAndRoughnessTexture_29() const { return ____NormalAndRoughnessTexture_29; }
	inline int32_t* get_address_of__NormalAndRoughnessTexture_29() { return &____NormalAndRoughnessTexture_29; }
	inline void set__NormalAndRoughnessTexture_29(int32_t value)
	{
		____NormalAndRoughnessTexture_29 = value;
	}

	inline static int32_t get_offset_of__HitPointTexture_30() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HitPointTexture_30)); }
	inline int32_t get__HitPointTexture_30() const { return ____HitPointTexture_30; }
	inline int32_t* get_address_of__HitPointTexture_30() { return &____HitPointTexture_30; }
	inline void set__HitPointTexture_30(int32_t value)
	{
		____HitPointTexture_30 = value;
	}

	inline static int32_t get_offset_of__BlurTexture_31() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____BlurTexture_31)); }
	inline int32_t get__BlurTexture_31() const { return ____BlurTexture_31; }
	inline int32_t* get_address_of__BlurTexture_31() { return &____BlurTexture_31; }
	inline void set__BlurTexture_31(int32_t value)
	{
		____BlurTexture_31 = value;
	}

	inline static int32_t get_offset_of__FilteredReflections_32() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FilteredReflections_32)); }
	inline int32_t get__FilteredReflections_32() const { return ____FilteredReflections_32; }
	inline int32_t* get_address_of__FilteredReflections_32() { return &____FilteredReflections_32; }
	inline void set__FilteredReflections_32(int32_t value)
	{
		____FilteredReflections_32 = value;
	}

	inline static int32_t get_offset_of__FinalReflectionTexture_33() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FinalReflectionTexture_33)); }
	inline int32_t get__FinalReflectionTexture_33() const { return ____FinalReflectionTexture_33; }
	inline int32_t* get_address_of__FinalReflectionTexture_33() { return &____FinalReflectionTexture_33; }
	inline void set__FinalReflectionTexture_33(int32_t value)
	{
		____FinalReflectionTexture_33 = value;
	}

	inline static int32_t get_offset_of__TempTexture_34() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TempTexture_34)); }
	inline int32_t get__TempTexture_34() const { return ____TempTexture_34; }
	inline int32_t* get_address_of__TempTexture_34() { return &____TempTexture_34; }
	inline void set__TempTexture_34(int32_t value)
	{
		____TempTexture_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2970573890_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef MATERIALFACTORY_T2445948724_H
#define MATERIALFACTORY_T2445948724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MaterialFactory
struct  MaterialFactory_t2445948724  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> UnityEngine.PostProcessing.MaterialFactory::m_Materials
	Dictionary_2_t125631422 * ___m_Materials_0;

public:
	inline static int32_t get_offset_of_m_Materials_0() { return static_cast<int32_t>(offsetof(MaterialFactory_t2445948724, ___m_Materials_0)); }
	inline Dictionary_2_t125631422 * get_m_Materials_0() const { return ___m_Materials_0; }
	inline Dictionary_2_t125631422 ** get_address_of_m_Materials_0() { return &___m_Materials_0; }
	inline void set_m_Materials_0(Dictionary_2_t125631422 * value)
	{
		___m_Materials_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Materials_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALFACTORY_T2445948724_H
#ifndef VALUECOLLECTION_T3288869226_H
#define VALUECOLLECTION_T3288869226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct  ValueCollection_t3288869226  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t1572824908 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t3288869226, ___dictionary_0)); }
	inline Dictionary_2_t1572824908 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1572824908 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1572824908 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T3288869226_H
#ifndef POSTPROCESSINGMODEL_T540111976_H
#define POSTPROCESSINGMODEL_T540111976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingModel
struct  PostProcessingModel_t540111976  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.PostProcessing.PostProcessingModel::m_Enabled
	bool ___m_Enabled_0;

public:
	inline static int32_t get_offset_of_m_Enabled_0() { return static_cast<int32_t>(offsetof(PostProcessingModel_t540111976, ___m_Enabled_0)); }
	inline bool get_m_Enabled_0() const { return ___m_Enabled_0; }
	inline bool* get_address_of_m_Enabled_0() { return &___m_Enabled_0; }
	inline void set_m_Enabled_0(bool value)
	{
		___m_Enabled_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGMODEL_T540111976_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#define POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentBase
struct  PostProcessingComponentBase_t2731103827  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingComponentBase::context
	PostProcessingContext_t2014408948 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(PostProcessingComponentBase_t2731103827, ___context_0)); }
	inline PostProcessingContext_t2014408948 * get_context_0() const { return ___context_0; }
	inline PostProcessingContext_t2014408948 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(PostProcessingContext_t2014408948 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifndef DICTIONARY_2_T1572824908_H
#define DICTIONARY_2_T1572824908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct  Dictionary_2_t1572824908  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	TypeU5BU5D_t3940880105* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	KeyValuePair_2U5BU5D_t802031261* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___keySlots_6)); }
	inline TypeU5BU5D_t3940880105* get_keySlots_6() const { return ___keySlots_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(TypeU5BU5D_t3940880105* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___valueSlots_7)); }
	inline KeyValuePair_2U5BU5D_t802031261* get_valueSlots_7() const { return ___valueSlots_7; }
	inline KeyValuePair_2U5BU5D_t802031261** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(KeyValuePair_2U5BU5D_t802031261* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1572824908_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1082526272 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1572824908_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1082526272 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1082526272 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1082526272 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1572824908_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef LIST_1_T4203178569_H
#define LIST_1_T4203178569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>
struct  List_1_t4203178569  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PostProcessingComponentBaseU5BU5D_t199974658* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4203178569, ____items_1)); }
	inline PostProcessingComponentBaseU5BU5D_t199974658* get__items_1() const { return ____items_1; }
	inline PostProcessingComponentBaseU5BU5D_t199974658** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PostProcessingComponentBaseU5BU5D_t199974658* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4203178569, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4203178569, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4203178569_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PostProcessingComponentBaseU5BU5D_t199974658* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4203178569_StaticFields, ___EmptyArray_4)); }
	inline PostProcessingComponentBaseU5BU5D_t199974658* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PostProcessingComponentBaseU5BU5D_t199974658** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PostProcessingComponentBaseU5BU5D_t199974658* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4203178569_H
#ifndef HASHSET_1_T673836907_H
#define HASHSET_1_T673836907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>
struct  HashSet_1_t673836907  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.HashSet`1/Link<T>[] System.Collections.Generic.HashSet`1::links
	LinkU5BU5D_t2504560647* ___links_5;
	// T[] System.Collections.Generic.HashSet`1::slots
	RenderTextureU5BU5D_t4111643188* ___slots_6;
	// System.Int32 System.Collections.Generic.HashSet`1::touched
	int32_t ___touched_7;
	// System.Int32 System.Collections.Generic.HashSet`1::empty_slot
	int32_t ___empty_slot_8;
	// System.Int32 System.Collections.Generic.HashSet`1::count
	int32_t ___count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::threshold
	int32_t ___threshold_10;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::comparer
	RuntimeObject* ___comparer_11;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::si
	SerializationInfo_t950877179 * ___si_12;
	// System.Int32 System.Collections.Generic.HashSet`1::generation
	int32_t ___generation_13;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_links_5() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___links_5)); }
	inline LinkU5BU5D_t2504560647* get_links_5() const { return ___links_5; }
	inline LinkU5BU5D_t2504560647** get_address_of_links_5() { return &___links_5; }
	inline void set_links_5(LinkU5BU5D_t2504560647* value)
	{
		___links_5 = value;
		Il2CppCodeGenWriteBarrier((&___links_5), value);
	}

	inline static int32_t get_offset_of_slots_6() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___slots_6)); }
	inline RenderTextureU5BU5D_t4111643188* get_slots_6() const { return ___slots_6; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_slots_6() { return &___slots_6; }
	inline void set_slots_6(RenderTextureU5BU5D_t4111643188* value)
	{
		___slots_6 = value;
		Il2CppCodeGenWriteBarrier((&___slots_6), value);
	}

	inline static int32_t get_offset_of_touched_7() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___touched_7)); }
	inline int32_t get_touched_7() const { return ___touched_7; }
	inline int32_t* get_address_of_touched_7() { return &___touched_7; }
	inline void set_touched_7(int32_t value)
	{
		___touched_7 = value;
	}

	inline static int32_t get_offset_of_empty_slot_8() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___empty_slot_8)); }
	inline int32_t get_empty_slot_8() const { return ___empty_slot_8; }
	inline int32_t* get_address_of_empty_slot_8() { return &___empty_slot_8; }
	inline void set_empty_slot_8(int32_t value)
	{
		___empty_slot_8 = value;
	}

	inline static int32_t get_offset_of_count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___count_9)); }
	inline int32_t get_count_9() const { return ___count_9; }
	inline int32_t* get_address_of_count_9() { return &___count_9; }
	inline void set_count_9(int32_t value)
	{
		___count_9 = value;
	}

	inline static int32_t get_offset_of_threshold_10() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___threshold_10)); }
	inline int32_t get_threshold_10() const { return ___threshold_10; }
	inline int32_t* get_address_of_threshold_10() { return &___threshold_10; }
	inline void set_threshold_10(int32_t value)
	{
		___threshold_10 = value;
	}

	inline static int32_t get_offset_of_comparer_11() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___comparer_11)); }
	inline RuntimeObject* get_comparer_11() const { return ___comparer_11; }
	inline RuntimeObject** get_address_of_comparer_11() { return &___comparer_11; }
	inline void set_comparer_11(RuntimeObject* value)
	{
		___comparer_11 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_11), value);
	}

	inline static int32_t get_offset_of_si_12() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___si_12)); }
	inline SerializationInfo_t950877179 * get_si_12() const { return ___si_12; }
	inline SerializationInfo_t950877179 ** get_address_of_si_12() { return &___si_12; }
	inline void set_si_12(SerializationInfo_t950877179 * value)
	{
		___si_12 = value;
		Il2CppCodeGenWriteBarrier((&___si_12), value);
	}

	inline static int32_t get_offset_of_generation_13() { return static_cast<int32_t>(offsetof(HashSet_1_t673836907, ___generation_13)); }
	inline int32_t get_generation_13() const { return ___generation_13; }
	inline int32_t* get_address_of_generation_13() { return &___generation_13; }
	inline void set_generation_13(int32_t value)
	{
		___generation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSET_1_T673836907_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef POSTPROCESSINGCOMPONENT_1_T741224383_H
#define POSTPROCESSINGCOMPONENT_1_T741224383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ColorGradingModel>
struct  PostProcessingComponent_1_t741224383  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ColorGradingModel_t1448048181 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t741224383, ___U3CmodelU3Ek__BackingField_1)); }
	inline ColorGradingModel_t1448048181 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ColorGradingModel_t1448048181 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ColorGradingModel_t1448048181 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T741224383_H
#ifndef POSTPROCESSINGCOMPONENT_1_T814315590_H
#define POSTPROCESSINGCOMPONENT_1_T814315590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AntialiasingModel>
struct  PostProcessingComponent_1_t814315590  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AntialiasingModel_t1521139388 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t814315590, ___U3CmodelU3Ek__BackingField_1)); }
	inline AntialiasingModel_t1521139388 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AntialiasingModel_t1521139388 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AntialiasingModel_t1521139388 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T814315590_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3830967410_H
#define POSTPROCESSINGCOMPONENT_1_T3830967410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.EyeAdaptationModel>
struct  PostProcessingComponent_1_t3830967410  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	EyeAdaptationModel_t242823912 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3830967410, ___U3CmodelU3Ek__BackingField_1)); }
	inline EyeAdaptationModel_t242823912 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline EyeAdaptationModel_t242823912 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(EyeAdaptationModel_t242823912 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3830967410_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2913864951_H
#define POSTPROCESSINGCOMPONENT_1_T2913864951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.FogModel>
struct  PostProcessingComponent_1_t2913864951  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	FogModel_t3620688749 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2913864951, ___U3CmodelU3Ek__BackingField_1)); }
	inline FogModel_t3620688749 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline FogModel_t3620688749 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(FogModel_t3620688749 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2913864951_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2373462325_H
#define POSTPROCESSINGCOMPONENT_1_T2373462325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.MotionBlurModel>
struct  PostProcessingComponent_1_t2373462325  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	MotionBlurModel_t3080286123 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2373462325, ___U3CmodelU3Ek__BackingField_1)); }
	inline MotionBlurModel_t3080286123 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline MotionBlurModel_t3080286123 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(MotionBlurModel_t3080286123 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2373462325_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3256576055_H
#define POSTPROCESSINGCOMPONENT_1_T3256576055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct  PostProcessingComponent_1_t3256576055  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ChromaticAberrationModel_t3963399853 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3256576055, ___U3CmodelU3Ek__BackingField_1)); }
	inline ChromaticAberrationModel_t3963399853 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ChromaticAberrationModel_t3963399853 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ChromaticAberrationModel_t3963399853 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3256576055_H
#ifndef POSTPROCESSINGCOMPONENT_1_T4102210828_H
#define POSTPROCESSINGCOMPONENT_1_T4102210828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DepthOfFieldModel>
struct  PostProcessingComponent_1_t4102210828  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	DepthOfFieldModel_t514067330 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t4102210828, ___U3CmodelU3Ek__BackingField_1)); }
	inline DepthOfFieldModel_t514067330 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline DepthOfFieldModel_t514067330 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(DepthOfFieldModel_t514067330 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T4102210828_H
#ifndef POSTPROCESSINGCOMPONENT_1_T1392904062_H
#define POSTPROCESSINGCOMPONENT_1_T1392904062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponent_1_t1392904062  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BloomModel_t2099727860 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t1392904062, ___U3CmodelU3Ek__BackingField_1)); }
	inline BloomModel_t2099727860 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BloomModel_t2099727860 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BloomModel_t2099727860 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T1392904062_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUMERATOR_T3350232909_H
#define ENUMERATOR_T3350232909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
struct  Enumerator_t3350232909 
{
public:
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator::hashset
	HashSet_1_t1645055638 * ___hashset_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::stamp
	int32_t ___stamp_2;
	// T System.Collections.Generic.HashSet`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_hashset_0() { return static_cast<int32_t>(offsetof(Enumerator_t3350232909, ___hashset_0)); }
	inline HashSet_1_t1645055638 * get_hashset_0() const { return ___hashset_0; }
	inline HashSet_1_t1645055638 ** get_address_of_hashset_0() { return &___hashset_0; }
	inline void set_hashset_0(HashSet_1_t1645055638 * value)
	{
		___hashset_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashset_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3350232909, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3350232909, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3350232909, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3350232909_H
#ifndef KEYVALUEPAIR_2_T3842366416_H
#define KEYVALUEPAIR_2_T3842366416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct  KeyValuePair_2_t3842366416 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3842366416, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3842366416, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3842366416_H
#ifndef KEYVALUEPAIR_2_T1198401749_H
#define KEYVALUEPAIR_2_T1198401749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>
struct  KeyValuePair_2_t1198401749 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	PostProcessingComponentBase_t2731103827 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1198401749, ___key_0)); }
	inline PostProcessingComponentBase_t2731103827 * get_key_0() const { return ___key_0; }
	inline PostProcessingComponentBase_t2731103827 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(PostProcessingComponentBase_t2731103827 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1198401749, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1198401749_H
#ifndef ENUMERATOR_T2379014178_H
#define ENUMERATOR_T2379014178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.RenderTexture>
struct  Enumerator_t2379014178 
{
public:
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator::hashset
	HashSet_1_t673836907 * ___hashset_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::stamp
	int32_t ___stamp_2;
	// T System.Collections.Generic.HashSet`1/Enumerator::current
	RenderTexture_t2108887433 * ___current_3;

public:
	inline static int32_t get_offset_of_hashset_0() { return static_cast<int32_t>(offsetof(Enumerator_t2379014178, ___hashset_0)); }
	inline HashSet_1_t673836907 * get_hashset_0() const { return ___hashset_0; }
	inline HashSet_1_t673836907 ** get_address_of_hashset_0() { return &___hashset_0; }
	inline void set_hashset_0(HashSet_1_t673836907 * value)
	{
		___hashset_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashset_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2379014178, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2379014178, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2379014178, ___current_3)); }
	inline RenderTexture_t2108887433 * get_current_3() const { return ___current_3; }
	inline RenderTexture_t2108887433 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RenderTexture_t2108887433 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2379014178_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef POSTPROCESSINGCOMPONENT_1_T446058690_H
#define POSTPROCESSINGCOMPONENT_1_T446058690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.GrainModel>
struct  PostProcessingComponent_1_t446058690  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	GrainModel_t1152882488 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t446058690, ___U3CmodelU3Ek__BackingField_1)); }
	inline GrainModel_t1152882488 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline GrainModel_t1152882488 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(GrainModel_t1152882488 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T446058690_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef SETTINGS_T3006579223_H
#define SETTINGS_T3006579223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutModel/Settings
struct  Settings_t3006579223 
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.UserLutModel/Settings::lut
	Texture2D_t3840446185 * ___lut_0;
	// System.Single UnityEngine.PostProcessing.UserLutModel/Settings::contribution
	float ___contribution_1;

public:
	inline static int32_t get_offset_of_lut_0() { return static_cast<int32_t>(offsetof(Settings_t3006579223, ___lut_0)); }
	inline Texture2D_t3840446185 * get_lut_0() const { return ___lut_0; }
	inline Texture2D_t3840446185 ** get_address_of_lut_0() { return &___lut_0; }
	inline void set_lut_0(Texture2D_t3840446185 * value)
	{
		___lut_0 = value;
		Il2CppCodeGenWriteBarrier((&___lut_0), value);
	}

	inline static int32_t get_offset_of_contribution_1() { return static_cast<int32_t>(offsetof(Settings_t3006579223, ___contribution_1)); }
	inline float get_contribution_1() const { return ___contribution_1; }
	inline float* get_address_of_contribution_1() { return &___contribution_1; }
	inline void set_contribution_1(float value)
	{
		___contribution_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.UserLutModel/Settings
struct Settings_t3006579223_marshaled_pinvoke
{
	Texture2D_t3840446185 * ___lut_0;
	float ___contribution_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.UserLutModel/Settings
struct Settings_t3006579223_marshaled_com
{
	Texture2D_t3840446185 * ___lut_0;
	float ___contribution_1;
};
#endif // SETTINGS_T3006579223_H
#ifndef POSTPROCESSINGCOMPONENT_1_T963284282_H
#define POSTPROCESSINGCOMPONENT_1_T963284282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.UserLutModel>
struct  PostProcessingComponent_1_t963284282  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	UserLutModel_t1670108080 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t963284282, ___U3CmodelU3Ek__BackingField_1)); }
	inline UserLutModel_t1670108080 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline UserLutModel_t1670108080 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(UserLutModel_t1670108080 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T963284282_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2138693379_H
#define POSTPROCESSINGCOMPONENT_1_T2138693379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.VignetteModel>
struct  PostProcessingComponent_1_t2138693379  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	VignetteModel_t2845517177 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2138693379, ___U3CmodelU3Ek__BackingField_1)); }
	inline VignetteModel_t2845517177 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline VignetteModel_t2845517177 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(VignetteModel_t2845517177 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2138693379_H
#ifndef INTENSITYSETTINGS_T1721872184_H
#define INTENSITYSETTINGS_T1721872184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings
struct  IntensitySettings_t1721872184 
{
public:
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::reflectionMultiplier
	float ___reflectionMultiplier_0;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fadeDistance
	float ___fadeDistance_1;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fresnelFade
	float ___fresnelFade_2;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fresnelFadePower
	float ___fresnelFadePower_3;

public:
	inline static int32_t get_offset_of_reflectionMultiplier_0() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___reflectionMultiplier_0)); }
	inline float get_reflectionMultiplier_0() const { return ___reflectionMultiplier_0; }
	inline float* get_address_of_reflectionMultiplier_0() { return &___reflectionMultiplier_0; }
	inline void set_reflectionMultiplier_0(float value)
	{
		___reflectionMultiplier_0 = value;
	}

	inline static int32_t get_offset_of_fadeDistance_1() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fadeDistance_1)); }
	inline float get_fadeDistance_1() const { return ___fadeDistance_1; }
	inline float* get_address_of_fadeDistance_1() { return &___fadeDistance_1; }
	inline void set_fadeDistance_1(float value)
	{
		___fadeDistance_1 = value;
	}

	inline static int32_t get_offset_of_fresnelFade_2() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fresnelFade_2)); }
	inline float get_fresnelFade_2() const { return ___fresnelFade_2; }
	inline float* get_address_of_fresnelFade_2() { return &___fresnelFade_2; }
	inline void set_fresnelFade_2(float value)
	{
		___fresnelFade_2 = value;
	}

	inline static int32_t get_offset_of_fresnelFadePower_3() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fresnelFadePower_3)); }
	inline float get_fresnelFadePower_3() const { return ___fresnelFadePower_3; }
	inline float* get_address_of_fresnelFadePower_3() { return &___fresnelFadePower_3; }
	inline void set_fresnelFadePower_3(float value)
	{
		___fresnelFadePower_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENSITYSETTINGS_T1721872184_H
#ifndef SCREENEDGEMASK_T4063288584_H
#define SCREENEDGEMASK_T4063288584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask
struct  ScreenEdgeMask_t4063288584 
{
public:
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask::intensity
	float ___intensity_0;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(ScreenEdgeMask_t4063288584, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENEDGEMASK_T4063288584_H
#ifndef POSTPROCESSINGCOMPONENT_1_T1722181598_H
#define POSTPROCESSINGCOMPONENT_1_T1722181598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DitheringModel>
struct  PostProcessingComponent_1_t1722181598  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	DitheringModel_t2429005396 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t1722181598, ___U3CmodelU3Ek__BackingField_1)); }
	inline DitheringModel_t2429005396 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline DitheringModel_t2429005396 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(DitheringModel_t2429005396 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T1722181598_H
#ifndef TAASETTINGS_T2709374970_H
#define TAASETTINGS_T2709374970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/TaaSettings
struct  TaaSettings_t2709374970 
{
public:
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::jitterSpread
	float ___jitterSpread_0;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::sharpen
	float ___sharpen_1;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::stationaryBlending
	float ___stationaryBlending_2;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::motionBlending
	float ___motionBlending_3;

public:
	inline static int32_t get_offset_of_jitterSpread_0() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___jitterSpread_0)); }
	inline float get_jitterSpread_0() const { return ___jitterSpread_0; }
	inline float* get_address_of_jitterSpread_0() { return &___jitterSpread_0; }
	inline void set_jitterSpread_0(float value)
	{
		___jitterSpread_0 = value;
	}

	inline static int32_t get_offset_of_sharpen_1() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___sharpen_1)); }
	inline float get_sharpen_1() const { return ___sharpen_1; }
	inline float* get_address_of_sharpen_1() { return &___sharpen_1; }
	inline void set_sharpen_1(float value)
	{
		___sharpen_1 = value;
	}

	inline static int32_t get_offset_of_stationaryBlending_2() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___stationaryBlending_2)); }
	inline float get_stationaryBlending_2() const { return ___stationaryBlending_2; }
	inline float* get_address_of_stationaryBlending_2() { return &___stationaryBlending_2; }
	inline void set_stationaryBlending_2(float value)
	{
		___stationaryBlending_2 = value;
	}

	inline static int32_t get_offset_of_motionBlending_3() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___motionBlending_3)); }
	inline float get_motionBlending_3() const { return ___motionBlending_3; }
	inline float* get_address_of_motionBlending_3() { return &___motionBlending_3; }
	inline void set_motionBlending_3(float value)
	{
		___motionBlending_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAASETTINGS_T2709374970_H
#ifndef ENUMERATOR_T1797455150_H
#define ENUMERATOR_T1797455150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.PostProcessing.PostProcessingComponentBase>
struct  Enumerator_t1797455150 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t4203178569 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PostProcessingComponentBase_t2731103827 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1797455150, ___l_0)); }
	inline List_1_t4203178569 * get_l_0() const { return ___l_0; }
	inline List_1_t4203178569 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t4203178569 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1797455150, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1797455150, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1797455150, ___current_3)); }
	inline PostProcessingComponentBase_t2731103827 * get_current_3() const { return ___current_3; }
	inline PostProcessingComponentBase_t2731103827 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PostProcessingComponentBase_t2731103827 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1797455150_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef CURVESSETTINGS_T2830270037_H
#define CURVESSETTINGS_T2830270037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct  CurvesSettings_t2830270037 
{
public:
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::master
	ColorGradingCurve_t2000571184 * ___master_0;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::red
	ColorGradingCurve_t2000571184 * ___red_1;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::green
	ColorGradingCurve_t2000571184 * ___green_2;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::blue
	ColorGradingCurve_t2000571184 * ___blue_3;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::hueVShue
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::hueVSsat
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::satVSsat
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::lumVSsat
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurrentEditingCurve
	int32_t ___e_CurrentEditingCurve_8;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveY
	bool ___e_CurveY_9;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveR
	bool ___e_CurveR_10;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveG
	bool ___e_CurveG_11;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveB
	bool ___e_CurveB_12;

public:
	inline static int32_t get_offset_of_master_0() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___master_0)); }
	inline ColorGradingCurve_t2000571184 * get_master_0() const { return ___master_0; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_master_0() { return &___master_0; }
	inline void set_master_0(ColorGradingCurve_t2000571184 * value)
	{
		___master_0 = value;
		Il2CppCodeGenWriteBarrier((&___master_0), value);
	}

	inline static int32_t get_offset_of_red_1() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___red_1)); }
	inline ColorGradingCurve_t2000571184 * get_red_1() const { return ___red_1; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_red_1() { return &___red_1; }
	inline void set_red_1(ColorGradingCurve_t2000571184 * value)
	{
		___red_1 = value;
		Il2CppCodeGenWriteBarrier((&___red_1), value);
	}

	inline static int32_t get_offset_of_green_2() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___green_2)); }
	inline ColorGradingCurve_t2000571184 * get_green_2() const { return ___green_2; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_green_2() { return &___green_2; }
	inline void set_green_2(ColorGradingCurve_t2000571184 * value)
	{
		___green_2 = value;
		Il2CppCodeGenWriteBarrier((&___green_2), value);
	}

	inline static int32_t get_offset_of_blue_3() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___blue_3)); }
	inline ColorGradingCurve_t2000571184 * get_blue_3() const { return ___blue_3; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_blue_3() { return &___blue_3; }
	inline void set_blue_3(ColorGradingCurve_t2000571184 * value)
	{
		___blue_3 = value;
		Il2CppCodeGenWriteBarrier((&___blue_3), value);
	}

	inline static int32_t get_offset_of_hueVShue_4() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___hueVShue_4)); }
	inline ColorGradingCurve_t2000571184 * get_hueVShue_4() const { return ___hueVShue_4; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_hueVShue_4() { return &___hueVShue_4; }
	inline void set_hueVShue_4(ColorGradingCurve_t2000571184 * value)
	{
		___hueVShue_4 = value;
		Il2CppCodeGenWriteBarrier((&___hueVShue_4), value);
	}

	inline static int32_t get_offset_of_hueVSsat_5() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___hueVSsat_5)); }
	inline ColorGradingCurve_t2000571184 * get_hueVSsat_5() const { return ___hueVSsat_5; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_hueVSsat_5() { return &___hueVSsat_5; }
	inline void set_hueVSsat_5(ColorGradingCurve_t2000571184 * value)
	{
		___hueVSsat_5 = value;
		Il2CppCodeGenWriteBarrier((&___hueVSsat_5), value);
	}

	inline static int32_t get_offset_of_satVSsat_6() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___satVSsat_6)); }
	inline ColorGradingCurve_t2000571184 * get_satVSsat_6() const { return ___satVSsat_6; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_satVSsat_6() { return &___satVSsat_6; }
	inline void set_satVSsat_6(ColorGradingCurve_t2000571184 * value)
	{
		___satVSsat_6 = value;
		Il2CppCodeGenWriteBarrier((&___satVSsat_6), value);
	}

	inline static int32_t get_offset_of_lumVSsat_7() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___lumVSsat_7)); }
	inline ColorGradingCurve_t2000571184 * get_lumVSsat_7() const { return ___lumVSsat_7; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_lumVSsat_7() { return &___lumVSsat_7; }
	inline void set_lumVSsat_7(ColorGradingCurve_t2000571184 * value)
	{
		___lumVSsat_7 = value;
		Il2CppCodeGenWriteBarrier((&___lumVSsat_7), value);
	}

	inline static int32_t get_offset_of_e_CurrentEditingCurve_8() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurrentEditingCurve_8)); }
	inline int32_t get_e_CurrentEditingCurve_8() const { return ___e_CurrentEditingCurve_8; }
	inline int32_t* get_address_of_e_CurrentEditingCurve_8() { return &___e_CurrentEditingCurve_8; }
	inline void set_e_CurrentEditingCurve_8(int32_t value)
	{
		___e_CurrentEditingCurve_8 = value;
	}

	inline static int32_t get_offset_of_e_CurveY_9() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveY_9)); }
	inline bool get_e_CurveY_9() const { return ___e_CurveY_9; }
	inline bool* get_address_of_e_CurveY_9() { return &___e_CurveY_9; }
	inline void set_e_CurveY_9(bool value)
	{
		___e_CurveY_9 = value;
	}

	inline static int32_t get_offset_of_e_CurveR_10() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveR_10)); }
	inline bool get_e_CurveR_10() const { return ___e_CurveR_10; }
	inline bool* get_address_of_e_CurveR_10() { return &___e_CurveR_10; }
	inline void set_e_CurveR_10(bool value)
	{
		___e_CurveR_10 = value;
	}

	inline static int32_t get_offset_of_e_CurveG_11() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveG_11)); }
	inline bool get_e_CurveG_11() const { return ___e_CurveG_11; }
	inline bool* get_address_of_e_CurveG_11() { return &___e_CurveG_11; }
	inline void set_e_CurveG_11(bool value)
	{
		___e_CurveG_11 = value;
	}

	inline static int32_t get_offset_of_e_CurveB_12() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveB_12)); }
	inline bool get_e_CurveB_12() const { return ___e_CurveB_12; }
	inline bool* get_address_of_e_CurveB_12() { return &___e_CurveB_12; }
	inline void set_e_CurveB_12(bool value)
	{
		___e_CurveB_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct CurvesSettings_t2830270037_marshaled_pinvoke
{
	ColorGradingCurve_t2000571184 * ___master_0;
	ColorGradingCurve_t2000571184 * ___red_1;
	ColorGradingCurve_t2000571184 * ___green_2;
	ColorGradingCurve_t2000571184 * ___blue_3;
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	int32_t ___e_CurrentEditingCurve_8;
	int32_t ___e_CurveY_9;
	int32_t ___e_CurveR_10;
	int32_t ___e_CurveG_11;
	int32_t ___e_CurveB_12;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct CurvesSettings_t2830270037_marshaled_com
{
	ColorGradingCurve_t2000571184 * ___master_0;
	ColorGradingCurve_t2000571184 * ___red_1;
	ColorGradingCurve_t2000571184 * ___green_2;
	ColorGradingCurve_t2000571184 * ___blue_3;
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	int32_t ___e_CurrentEditingCurve_8;
	int32_t ___e_CurveY_9;
	int32_t ___e_CurveR_10;
	int32_t ___e_CurveG_11;
	int32_t ___e_CurveB_12;
};
#endif // CURVESSETTINGS_T2830270037_H
#ifndef SETTINGS_T4123292438_H
#define SETTINGS_T4123292438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainModel/Settings
struct  Settings_t4123292438 
{
public:
	// System.Boolean UnityEngine.PostProcessing.GrainModel/Settings::colored
	bool ___colored_0;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::intensity
	float ___intensity_1;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::size
	float ___size_2;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::luminanceContribution
	float ___luminanceContribution_3;

public:
	inline static int32_t get_offset_of_colored_0() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___colored_0)); }
	inline bool get_colored_0() const { return ___colored_0; }
	inline bool* get_address_of_colored_0() { return &___colored_0; }
	inline void set_colored_0(bool value)
	{
		___colored_0 = value;
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___intensity_1)); }
	inline float get_intensity_1() const { return ___intensity_1; }
	inline float* get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(float value)
	{
		___intensity_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___size_2)); }
	inline float get_size_2() const { return ___size_2; }
	inline float* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(float value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_luminanceContribution_3() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___luminanceContribution_3)); }
	inline float get_luminanceContribution_3() const { return ___luminanceContribution_3; }
	inline float* get_address_of_luminanceContribution_3() { return &___luminanceContribution_3; }
	inline void set_luminanceContribution_3(float value)
	{
		___luminanceContribution_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.GrainModel/Settings
struct Settings_t4123292438_marshaled_pinvoke
{
	int32_t ___colored_0;
	float ___intensity_1;
	float ___size_2;
	float ___luminanceContribution_3;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.GrainModel/Settings
struct Settings_t4123292438_marshaled_com
{
	int32_t ___colored_0;
	float ___intensity_1;
	float ___size_2;
	float ___luminanceContribution_3;
};
#endif // SETTINGS_T4123292438_H
#ifndef SETTINGS_T2313396630_H
#define SETTINGS_T2313396630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringModel/Settings
struct  Settings_t2313396630 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Settings_t2313396630__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T2313396630_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef LENSDIRTSETTINGS_T3693422705_H
#define LENSDIRTSETTINGS_T3693422705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomModel/LensDirtSettings
struct  LensDirtSettings_t3693422705 
{
public:
	// UnityEngine.Texture UnityEngine.PostProcessing.BloomModel/LensDirtSettings::texture
	Texture_t3661962703 * ___texture_0;
	// System.Single UnityEngine.PostProcessing.BloomModel/LensDirtSettings::intensity
	float ___intensity_1;

public:
	inline static int32_t get_offset_of_texture_0() { return static_cast<int32_t>(offsetof(LensDirtSettings_t3693422705, ___texture_0)); }
	inline Texture_t3661962703 * get_texture_0() const { return ___texture_0; }
	inline Texture_t3661962703 ** get_address_of_texture_0() { return &___texture_0; }
	inline void set_texture_0(Texture_t3661962703 * value)
	{
		___texture_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture_0), value);
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(LensDirtSettings_t3693422705, ___intensity_1)); }
	inline float get_intensity_1() const { return ___intensity_1; }
	inline float* get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(float value)
	{
		___intensity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.BloomModel/LensDirtSettings
struct LensDirtSettings_t3693422705_marshaled_pinvoke
{
	Texture_t3661962703 * ___texture_0;
	float ___intensity_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.BloomModel/LensDirtSettings
struct LensDirtSettings_t3693422705_marshaled_com
{
	Texture_t3661962703 * ___texture_0;
	float ___intensity_1;
};
#endif // LENSDIRTSETTINGS_T3693422705_H
#ifndef BLOOMSETTINGS_T2599855122_H
#define BLOOMSETTINGS_T2599855122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomModel/BloomSettings
struct  BloomSettings_t2599855122 
{
public:
	// System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::intensity
	float ___intensity_0;
	// System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::threshold
	float ___threshold_1;
	// System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::softKnee
	float ___softKnee_2;
	// System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::radius
	float ___radius_3;
	// System.Boolean UnityEngine.PostProcessing.BloomModel/BloomSettings::antiFlicker
	bool ___antiFlicker_4;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}

	inline static int32_t get_offset_of_threshold_1() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___threshold_1)); }
	inline float get_threshold_1() const { return ___threshold_1; }
	inline float* get_address_of_threshold_1() { return &___threshold_1; }
	inline void set_threshold_1(float value)
	{
		___threshold_1 = value;
	}

	inline static int32_t get_offset_of_softKnee_2() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___softKnee_2)); }
	inline float get_softKnee_2() const { return ___softKnee_2; }
	inline float* get_address_of_softKnee_2() { return &___softKnee_2; }
	inline void set_softKnee_2(float value)
	{
		___softKnee_2 = value;
	}

	inline static int32_t get_offset_of_radius_3() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___radius_3)); }
	inline float get_radius_3() const { return ___radius_3; }
	inline float* get_address_of_radius_3() { return &___radius_3; }
	inline void set_radius_3(float value)
	{
		___radius_3 = value;
	}

	inline static int32_t get_offset_of_antiFlicker_4() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___antiFlicker_4)); }
	inline bool get_antiFlicker_4() const { return ___antiFlicker_4; }
	inline bool* get_address_of_antiFlicker_4() { return &___antiFlicker_4; }
	inline void set_antiFlicker_4(bool value)
	{
		___antiFlicker_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.BloomModel/BloomSettings
struct BloomSettings_t2599855122_marshaled_pinvoke
{
	float ___intensity_0;
	float ___threshold_1;
	float ___softKnee_2;
	float ___radius_3;
	int32_t ___antiFlicker_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.BloomModel/BloomSettings
struct BloomSettings_t2599855122_marshaled_com
{
	float ___intensity_0;
	float ___threshold_1;
	float ___softKnee_2;
	float ___radius_3;
	int32_t ___antiFlicker_4;
};
#endif // BLOOMSETTINGS_T2599855122_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef DEPTHSETTINGS_T1820272864_H
#define DEPTHSETTINGS_T1820272864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel/DepthSettings
struct  DepthSettings_t1820272864 
{
public:
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/DepthSettings::scale
	float ___scale_0;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(DepthSettings_t1820272864, ___scale_0)); }
	inline float get_scale_0() const { return ___scale_0; }
	inline float* get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(float value)
	{
		___scale_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHSETTINGS_T1820272864_H
#ifndef MOTIONVECTORSSETTINGS_T3857813598_H
#define MOTIONVECTORSSETTINGS_T3857813598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings
struct  MotionVectorsSettings_t3857813598 
{
public:
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::sourceOpacity
	float ___sourceOpacity_0;
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionImageOpacity
	float ___motionImageOpacity_1;
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionImageAmplitude
	float ___motionImageAmplitude_2;
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionVectorsOpacity
	float ___motionVectorsOpacity_3;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionVectorsResolution
	int32_t ___motionVectorsResolution_4;
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionVectorsAmplitude
	float ___motionVectorsAmplitude_5;

public:
	inline static int32_t get_offset_of_sourceOpacity_0() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___sourceOpacity_0)); }
	inline float get_sourceOpacity_0() const { return ___sourceOpacity_0; }
	inline float* get_address_of_sourceOpacity_0() { return &___sourceOpacity_0; }
	inline void set_sourceOpacity_0(float value)
	{
		___sourceOpacity_0 = value;
	}

	inline static int32_t get_offset_of_motionImageOpacity_1() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionImageOpacity_1)); }
	inline float get_motionImageOpacity_1() const { return ___motionImageOpacity_1; }
	inline float* get_address_of_motionImageOpacity_1() { return &___motionImageOpacity_1; }
	inline void set_motionImageOpacity_1(float value)
	{
		___motionImageOpacity_1 = value;
	}

	inline static int32_t get_offset_of_motionImageAmplitude_2() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionImageAmplitude_2)); }
	inline float get_motionImageAmplitude_2() const { return ___motionImageAmplitude_2; }
	inline float* get_address_of_motionImageAmplitude_2() { return &___motionImageAmplitude_2; }
	inline void set_motionImageAmplitude_2(float value)
	{
		___motionImageAmplitude_2 = value;
	}

	inline static int32_t get_offset_of_motionVectorsOpacity_3() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionVectorsOpacity_3)); }
	inline float get_motionVectorsOpacity_3() const { return ___motionVectorsOpacity_3; }
	inline float* get_address_of_motionVectorsOpacity_3() { return &___motionVectorsOpacity_3; }
	inline void set_motionVectorsOpacity_3(float value)
	{
		___motionVectorsOpacity_3 = value;
	}

	inline static int32_t get_offset_of_motionVectorsResolution_4() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionVectorsResolution_4)); }
	inline int32_t get_motionVectorsResolution_4() const { return ___motionVectorsResolution_4; }
	inline int32_t* get_address_of_motionVectorsResolution_4() { return &___motionVectorsResolution_4; }
	inline void set_motionVectorsResolution_4(int32_t value)
	{
		___motionVectorsResolution_4 = value;
	}

	inline static int32_t get_offset_of_motionVectorsAmplitude_5() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionVectorsAmplitude_5)); }
	inline float get_motionVectorsAmplitude_5() const { return ___motionVectorsAmplitude_5; }
	inline float* get_address_of_motionVectorsAmplitude_5() { return &___motionVectorsAmplitude_5; }
	inline void set_motionVectorsAmplitude_5(float value)
	{
		___motionVectorsAmplitude_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONVECTORSSETTINGS_T3857813598_H
#ifndef BASICSETTINGS_T838098426_H
#define BASICSETTINGS_T838098426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/BasicSettings
struct  BasicSettings_t838098426 
{
public:
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::postExposure
	float ___postExposure_0;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::temperature
	float ___temperature_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::tint
	float ___tint_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::hueShift
	float ___hueShift_3;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::saturation
	float ___saturation_4;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::contrast
	float ___contrast_5;

public:
	inline static int32_t get_offset_of_postExposure_0() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___postExposure_0)); }
	inline float get_postExposure_0() const { return ___postExposure_0; }
	inline float* get_address_of_postExposure_0() { return &___postExposure_0; }
	inline void set_postExposure_0(float value)
	{
		___postExposure_0 = value;
	}

	inline static int32_t get_offset_of_temperature_1() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___temperature_1)); }
	inline float get_temperature_1() const { return ___temperature_1; }
	inline float* get_address_of_temperature_1() { return &___temperature_1; }
	inline void set_temperature_1(float value)
	{
		___temperature_1 = value;
	}

	inline static int32_t get_offset_of_tint_2() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___tint_2)); }
	inline float get_tint_2() const { return ___tint_2; }
	inline float* get_address_of_tint_2() { return &___tint_2; }
	inline void set_tint_2(float value)
	{
		___tint_2 = value;
	}

	inline static int32_t get_offset_of_hueShift_3() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___hueShift_3)); }
	inline float get_hueShift_3() const { return ___hueShift_3; }
	inline float* get_address_of_hueShift_3() { return &___hueShift_3; }
	inline void set_hueShift_3(float value)
	{
		___hueShift_3 = value;
	}

	inline static int32_t get_offset_of_saturation_4() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___saturation_4)); }
	inline float get_saturation_4() const { return ___saturation_4; }
	inline float* get_address_of_saturation_4() { return &___saturation_4; }
	inline void set_saturation_4(float value)
	{
		___saturation_4 = value;
	}

	inline static int32_t get_offset_of_contrast_5() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___contrast_5)); }
	inline float get_contrast_5() const { return ___contrast_5; }
	inline float* get_address_of_contrast_5() { return &___contrast_5; }
	inline void set_contrast_5(float value)
	{
		___contrast_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICSETTINGS_T838098426_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3977614564_H
#define POSTPROCESSINGCOMPONENT_1_T3977614564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponent_1_t3977614564  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AmbientOcclusionModel_t389471066 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3977614564, ___U3CmodelU3Ek__BackingField_1)); }
	inline AmbientOcclusionModel_t389471066 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AmbientOcclusionModel_t389471066 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AmbientOcclusionModel_t389471066 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3977614564_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2319520934_H
#define POSTPROCESSINGCOMPONENT_1_T2319520934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>
struct  PostProcessingComponent_1_t2319520934  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ScreenSpaceReflectionModel_t3026344732 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2319520934, ___U3CmodelU3Ek__BackingField_1)); }
	inline ScreenSpaceReflectionModel_t3026344732 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ScreenSpaceReflectionModel_t3026344732 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ScreenSpaceReflectionModel_t3026344732 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2319520934_H
#ifndef POSTPROCESSINGCOMPONENT_1_T755795042_H
#define POSTPROCESSINGCOMPONENT_1_T755795042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct  PostProcessingComponent_1_t755795042  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BuiltinDebugViewsModel_t1462618840 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t755795042, ___U3CmodelU3Ek__BackingField_1)); }
	inline BuiltinDebugViewsModel_t1462618840 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BuiltinDebugViewsModel_t1462618840 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BuiltinDebugViewsModel_t1462618840 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T755795042_H
#ifndef SETTINGS_T2111398455_H
#define SETTINGS_T2111398455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationModel/Settings
struct  Settings_t2111398455 
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ChromaticAberrationModel/Settings::spectralTexture
	Texture2D_t3840446185 * ___spectralTexture_0;
	// System.Single UnityEngine.PostProcessing.ChromaticAberrationModel/Settings::intensity
	float ___intensity_1;

public:
	inline static int32_t get_offset_of_spectralTexture_0() { return static_cast<int32_t>(offsetof(Settings_t2111398455, ___spectralTexture_0)); }
	inline Texture2D_t3840446185 * get_spectralTexture_0() const { return ___spectralTexture_0; }
	inline Texture2D_t3840446185 ** get_address_of_spectralTexture_0() { return &___spectralTexture_0; }
	inline void set_spectralTexture_0(Texture2D_t3840446185 * value)
	{
		___spectralTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___spectralTexture_0), value);
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(Settings_t2111398455, ___intensity_1)); }
	inline float get_intensity_1() const { return ___intensity_1; }
	inline float* get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(float value)
	{
		___intensity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ChromaticAberrationModel/Settings
struct Settings_t2111398455_marshaled_pinvoke
{
	Texture2D_t3840446185 * ___spectralTexture_0;
	float ___intensity_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ChromaticAberrationModel/Settings
struct Settings_t2111398455_marshaled_com
{
	Texture2D_t3840446185 * ___spectralTexture_0;
	float ___intensity_1;
};
#endif // SETTINGS_T2111398455_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SETTINGS_T4282162361_H
#define SETTINGS_T4282162361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurModel/Settings
struct  Settings_t4282162361 
{
public:
	// System.Single UnityEngine.PostProcessing.MotionBlurModel/Settings::shutterAngle
	float ___shutterAngle_0;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurModel/Settings::sampleCount
	int32_t ___sampleCount_1;
	// System.Single UnityEngine.PostProcessing.MotionBlurModel/Settings::frameBlending
	float ___frameBlending_2;

public:
	inline static int32_t get_offset_of_shutterAngle_0() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___shutterAngle_0)); }
	inline float get_shutterAngle_0() const { return ___shutterAngle_0; }
	inline float* get_address_of_shutterAngle_0() { return &___shutterAngle_0; }
	inline void set_shutterAngle_0(float value)
	{
		___shutterAngle_0 = value;
	}

	inline static int32_t get_offset_of_sampleCount_1() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___sampleCount_1)); }
	inline int32_t get_sampleCount_1() const { return ___sampleCount_1; }
	inline int32_t* get_address_of_sampleCount_1() { return &___sampleCount_1; }
	inline void set_sampleCount_1(int32_t value)
	{
		___sampleCount_1 = value;
	}

	inline static int32_t get_offset_of_frameBlending_2() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___frameBlending_2)); }
	inline float get_frameBlending_2() const { return ___frameBlending_2; }
	inline float* get_address_of_frameBlending_2() { return &___frameBlending_2; }
	inline void set_frameBlending_2(float value)
	{
		___frameBlending_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T4282162361_H
#ifndef SETTINGS_T224798599_H
#define SETTINGS_T224798599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogModel/Settings
struct  Settings_t224798599 
{
public:
	// System.Boolean UnityEngine.PostProcessing.FogModel/Settings::excludeSkybox
	bool ___excludeSkybox_0;

public:
	inline static int32_t get_offset_of_excludeSkybox_0() { return static_cast<int32_t>(offsetof(Settings_t224798599, ___excludeSkybox_0)); }
	inline bool get_excludeSkybox_0() const { return ___excludeSkybox_0; }
	inline bool* get_address_of_excludeSkybox_0() { return &___excludeSkybox_0; }
	inline void set_excludeSkybox_0(bool value)
	{
		___excludeSkybox_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.FogModel/Settings
struct Settings_t224798599_marshaled_pinvoke
{
	int32_t ___excludeSkybox_0;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.FogModel/Settings
struct Settings_t224798599_marshaled_com
{
	int32_t ___excludeSkybox_0;
};
#endif // SETTINGS_T224798599_H
#ifndef TONEMAPPER_T1404353651_H
#define TONEMAPPER_T1404353651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/Tonemapper
struct  Tonemapper_t1404353651 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/Tonemapper::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Tonemapper_t1404353651, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPER_T1404353651_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef BUILTINRENDERTEXTURETYPE_T2399837169_H
#define BUILTINRENDERTEXTURETYPE_T2399837169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t2399837169 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t2399837169, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T2399837169_H
#ifndef FXAAPRESET_T2149486832_H
#define FXAAPRESET_T2149486832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset
struct  FxaaPreset_t2149486832 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FxaaPreset_t2149486832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAAPRESET_T2149486832_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef FILTERMODE_T3761284007_H
#define FILTERMODE_T3761284007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t3761284007 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t3761284007, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T3761284007_H
#ifndef RENDERINGPATH_T883966888_H
#define RENDERINGPATH_T883966888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderingPath
struct  RenderingPath_t883966888 
{
public:
	// System.Int32 UnityEngine.RenderingPath::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderingPath_t883966888, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERINGPATH_T883966888_H
#ifndef LOGWHEELSSETTINGS_T1545220311_H
#define LOGWHEELSSETTINGS_T1545220311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings
struct  LogWheelsSettings_t1545220311 
{
public:
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::slope
	Color_t2555686324  ___slope_0;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::power
	Color_t2555686324  ___power_1;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::offset
	Color_t2555686324  ___offset_2;

public:
	inline static int32_t get_offset_of_slope_0() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___slope_0)); }
	inline Color_t2555686324  get_slope_0() const { return ___slope_0; }
	inline Color_t2555686324 * get_address_of_slope_0() { return &___slope_0; }
	inline void set_slope_0(Color_t2555686324  value)
	{
		___slope_0 = value;
	}

	inline static int32_t get_offset_of_power_1() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___power_1)); }
	inline Color_t2555686324  get_power_1() const { return ___power_1; }
	inline Color_t2555686324 * get_address_of_power_1() { return &___power_1; }
	inline void set_power_1(Color_t2555686324  value)
	{
		___power_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___offset_2)); }
	inline Color_t2555686324  get_offset_2() const { return ___offset_2; }
	inline Color_t2555686324 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Color_t2555686324  value)
	{
		___offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGWHEELSSETTINGS_T1545220311_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef COLORWHEELMODE_T1939415375_H
#define COLORWHEELMODE_T1939415375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode
struct  ColorWheelMode_t1939415375 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorWheelMode_t1939415375, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELMODE_T1939415375_H
#ifndef TEXTUREWRAPMODE_T584250749_H
#define TEXTUREWRAPMODE_T584250749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t584250749 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t584250749, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T584250749_H
#ifndef RENDERTEXTUREREADWRITE_T1793271918_H
#define RENDERTEXTUREREADWRITE_T1793271918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureReadWrite
struct  RenderTextureReadWrite_t1793271918 
{
public:
	// System.Int32 UnityEngine.RenderTextureReadWrite::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureReadWrite_t1793271918, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREREADWRITE_T1793271918_H
#ifndef CUBEMAPFACE_T1358225318_H
#define CUBEMAPFACE_T1358225318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t1358225318 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubemapFace_t1358225318, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T1358225318_H
#ifndef TRACKBALLGROUPATTRIBUTE_T624107828_H
#define TRACKBALLGROUPATTRIBUTE_T624107828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballGroupAttribute
struct  TrackballGroupAttribute_t624107828  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLGROUPATTRIBUTE_T624107828_H
#ifndef SAMPLECOUNT_T1158000259_H
#define SAMPLECOUNT_T1158000259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount
struct  SampleCount_t1158000259 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleCount_t1158000259, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECOUNT_T1158000259_H
#ifndef TRACKBALLATTRIBUTE_T219960417_H
#define TRACKBALLATTRIBUTE_T219960417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballAttribute
struct  TrackballAttribute_t219960417  : public PropertyAttribute_t3677895545
{
public:
	// System.String UnityEngine.PostProcessing.TrackballAttribute::method
	String_t* ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(TrackballAttribute_t219960417, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLATTRIBUTE_T219960417_H
#ifndef SETTINGS_T181254429_H
#define SETTINGS_T181254429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomModel/Settings
struct  Settings_t181254429 
{
public:
	// UnityEngine.PostProcessing.BloomModel/BloomSettings UnityEngine.PostProcessing.BloomModel/Settings::bloom
	BloomSettings_t2599855122  ___bloom_0;
	// UnityEngine.PostProcessing.BloomModel/LensDirtSettings UnityEngine.PostProcessing.BloomModel/Settings::lensDirt
	LensDirtSettings_t3693422705  ___lensDirt_1;

public:
	inline static int32_t get_offset_of_bloom_0() { return static_cast<int32_t>(offsetof(Settings_t181254429, ___bloom_0)); }
	inline BloomSettings_t2599855122  get_bloom_0() const { return ___bloom_0; }
	inline BloomSettings_t2599855122 * get_address_of_bloom_0() { return &___bloom_0; }
	inline void set_bloom_0(BloomSettings_t2599855122  value)
	{
		___bloom_0 = value;
	}

	inline static int32_t get_offset_of_lensDirt_1() { return static_cast<int32_t>(offsetof(Settings_t181254429, ___lensDirt_1)); }
	inline LensDirtSettings_t3693422705  get_lensDirt_1() const { return ___lensDirt_1; }
	inline LensDirtSettings_t3693422705 * get_address_of_lensDirt_1() { return &___lensDirt_1; }
	inline void set_lensDirt_1(LensDirtSettings_t3693422705  value)
	{
		___lensDirt_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.BloomModel/Settings
struct Settings_t181254429_marshaled_pinvoke
{
	BloomSettings_t2599855122_marshaled_pinvoke ___bloom_0;
	LensDirtSettings_t3693422705_marshaled_pinvoke ___lensDirt_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.BloomModel/Settings
struct Settings_t181254429_marshaled_com
{
	BloomSettings_t2599855122_marshaled_com ___bloom_0;
	LensDirtSettings_t3693422705_marshaled_com ___lensDirt_1;
};
#endif // SETTINGS_T181254429_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponentRenderTexture_1_t3668012901  : public PostProcessingComponent_1_t1392904062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DepthOfFieldModel>
struct  PostProcessingComponentRenderTexture_1_t2082352371  : public PostProcessingComponent_1_t4102210828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#ifndef MODE_T3936508933_H
#define MODE_T3936508933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel/Mode
struct  Mode_t3936508933 
{
public:
	// System.Int32 UnityEngine.PostProcessing.VignetteModel/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3936508933, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3936508933_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.EyeAdaptationModel>
struct  PostProcessingComponentRenderTexture_1_t1811108953  : public PostProcessingComponent_1_t3830967410
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#ifndef SSRREFLECTIONBLENDTYPE_T3026770880_H
#define SSRREFLECTIONBLENDTYPE_T3026770880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType
struct  SSRReflectionBlendType_t3026770880 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SSRReflectionBlendType_t3026770880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSRREFLECTIONBLENDTYPE_T3026770880_H
#ifndef PASSINDEX_T1642913883_H
#define PASSINDEX_T1642913883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/PassIndex
struct  PassIndex_t1642913883 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/PassIndex::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PassIndex_t1642913883, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSINDEX_T1642913883_H
#ifndef SSRRESOLUTION_T161222554_H
#define SSRRESOLUTION_T161222554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution
struct  SSRResolution_t161222554 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SSRResolution_t161222554, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSRRESOLUTION_T161222554_H
#ifndef CHANNELMIXERSETTINGS_T898701698_H
#define CHANNELMIXERSETTINGS_T898701698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings
struct  ChannelMixerSettings_t898701698 
{
public:
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::red
	Vector3_t3722313464  ___red_0;
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::green
	Vector3_t3722313464  ___green_1;
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::blue
	Vector3_t3722313464  ___blue_2;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::currentEditingChannel
	int32_t ___currentEditingChannel_3;

public:
	inline static int32_t get_offset_of_red_0() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___red_0)); }
	inline Vector3_t3722313464  get_red_0() const { return ___red_0; }
	inline Vector3_t3722313464 * get_address_of_red_0() { return &___red_0; }
	inline void set_red_0(Vector3_t3722313464  value)
	{
		___red_0 = value;
	}

	inline static int32_t get_offset_of_green_1() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___green_1)); }
	inline Vector3_t3722313464  get_green_1() const { return ___green_1; }
	inline Vector3_t3722313464 * get_address_of_green_1() { return &___green_1; }
	inline void set_green_1(Vector3_t3722313464  value)
	{
		___green_1 = value;
	}

	inline static int32_t get_offset_of_blue_2() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___blue_2)); }
	inline Vector3_t3722313464  get_blue_2() const { return ___blue_2; }
	inline Vector3_t3722313464 * get_address_of_blue_2() { return &___blue_2; }
	inline void set_blue_2(Vector3_t3722313464  value)
	{
		___blue_2 = value;
	}

	inline static int32_t get_offset_of_currentEditingChannel_3() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___currentEditingChannel_3)); }
	inline int32_t get_currentEditingChannel_3() const { return ___currentEditingChannel_3; }
	inline int32_t* get_address_of_currentEditingChannel_3() { return &___currentEditingChannel_3; }
	inline void set_currentEditingChannel_3(int32_t value)
	{
		___currentEditingChannel_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELMIXERSETTINGS_T898701698_H
#ifndef RENDERBUFFER_T586150500_H
#define RENDERBUFFER_T586150500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderBuffer
struct  RenderBuffer_t586150500 
{
public:
	// System.Int32 UnityEngine.RenderBuffer::m_RenderTextureInstanceID
	int32_t ___m_RenderTextureInstanceID_0;
	// System.IntPtr UnityEngine.RenderBuffer::m_BufferPtr
	intptr_t ___m_BufferPtr_1;

public:
	inline static int32_t get_offset_of_m_RenderTextureInstanceID_0() { return static_cast<int32_t>(offsetof(RenderBuffer_t586150500, ___m_RenderTextureInstanceID_0)); }
	inline int32_t get_m_RenderTextureInstanceID_0() const { return ___m_RenderTextureInstanceID_0; }
	inline int32_t* get_address_of_m_RenderTextureInstanceID_0() { return &___m_RenderTextureInstanceID_0; }
	inline void set_m_RenderTextureInstanceID_0(int32_t value)
	{
		___m_RenderTextureInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_m_BufferPtr_1() { return static_cast<int32_t>(offsetof(RenderBuffer_t586150500, ___m_BufferPtr_1)); }
	inline intptr_t get_m_BufferPtr_1() const { return ___m_BufferPtr_1; }
	inline intptr_t* get_address_of_m_BufferPtr_1() { return &___m_BufferPtr_1; }
	inline void set_m_BufferPtr_1(intptr_t value)
	{
		___m_BufferPtr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERBUFFER_T586150500_H
#ifndef KERNELSIZE_T2406218613_H
#define KERNELSIZE_T2406218613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize
struct  KernelSize_t2406218613 
{
public:
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KernelSize_t2406218613, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNELSIZE_T2406218613_H
#ifndef EYEADAPTATIONTYPE_T3053468307_H
#define EYEADAPTATIONTYPE_T3053468307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType
struct  EyeAdaptationType_t3053468307 
{
public:
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EyeAdaptationType_t3053468307, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONTYPE_T3053468307_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.AntialiasingModel>
struct  PostProcessingComponentRenderTexture_1_t3089424429  : public PostProcessingComponent_1_t814315590
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#ifndef METHOD_T287042102_H
#define METHOD_T287042102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/Method
struct  Method_t287042102 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AntialiasingModel/Method::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Method_t287042102, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T287042102_H
#ifndef FOGMODEL_T3620688749_H
#define FOGMODEL_T3620688749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogModel
struct  FogModel_t3620688749  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.FogModel/Settings UnityEngine.PostProcessing.FogModel::m_Settings
	Settings_t224798599  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(FogModel_t3620688749, ___m_Settings_1)); }
	inline Settings_t224798599  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t224798599 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t224798599  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGMODEL_T3620688749_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct  PostProcessingComponentRenderTexture_1_t1236717598  : public PostProcessingComponent_1_t3256576055
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.MotionBlurModel>
struct  PostProcessingComponentCommandBuffer_1_t60620716  : public PostProcessingComponent_1_t2373462325
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ColorGradingModel>
struct  PostProcessingComponentRenderTexture_1_t3016333222  : public PostProcessingComponent_1_t741224383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.UserLutModel>
struct  PostProcessingComponentRenderTexture_1_t3238393121  : public PostProcessingComponent_1_t963284282
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.VignetteModel>
struct  PostProcessingComponentRenderTexture_1_t118834922  : public PostProcessingComponent_1_t2138693379
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.FogModel>
struct  PostProcessingComponentCommandBuffer_1_t601023342  : public PostProcessingComponent_1_t2913864951
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#ifndef DITHERINGMODEL_T2429005396_H
#define DITHERINGMODEL_T2429005396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringModel
struct  DitheringModel_t2429005396  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.DitheringModel/Settings UnityEngine.PostProcessing.DitheringModel::m_Settings
	Settings_t2313396630  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(DitheringModel_t2429005396, ___m_Settings_1)); }
	inline Settings_t2313396630  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2313396630 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2313396630  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERINGMODEL_T2429005396_H
#ifndef GRAINMODEL_T1152882488_H
#define GRAINMODEL_T1152882488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainModel
struct  GrainModel_t1152882488  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.GrainModel/Settings UnityEngine.PostProcessing.GrainModel::m_Settings
	Settings_t4123292438  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(GrainModel_t1152882488, ___m_Settings_1)); }
	inline Settings_t4123292438  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4123292438 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4123292438  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINMODEL_T1152882488_H
#ifndef USERLUTMODEL_T1670108080_H
#define USERLUTMODEL_T1670108080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutModel
struct  UserLutModel_t1670108080  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel::m_Settings
	Settings_t3006579223  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(UserLutModel_t1670108080, ___m_Settings_1)); }
	inline Settings_t3006579223  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t3006579223 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t3006579223  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERLUTMODEL_T1670108080_H
#ifndef CHROMATICABERRATIONMODEL_T3963399853_H
#define CHROMATICABERRATIONMODEL_T3963399853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationModel
struct  ChromaticAberrationModel_t3963399853  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.ChromaticAberrationModel/Settings UnityEngine.PostProcessing.ChromaticAberrationModel::m_Settings
	Settings_t2111398455  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(ChromaticAberrationModel_t3963399853, ___m_Settings_1)); }
	inline Settings_t2111398455  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2111398455 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2111398455  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATIONMODEL_T3963399853_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>
struct  PostProcessingComponentCommandBuffer_1_t6679325  : public PostProcessingComponent_1_t2319520934
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponentCommandBuffer_1_t1664772955  : public PostProcessingComponent_1_t3977614564
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct  PostProcessingComponentCommandBuffer_1_t2737920729  : public PostProcessingComponent_1_t755795042
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#ifndef CAMERAEVENT_T2033959522_H
#define CAMERAEVENT_T2033959522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t2033959522 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t2033959522, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T2033959522_H
#ifndef LINEARWHEELSSETTINGS_T3897781309_H
#define LINEARWHEELSSETTINGS_T3897781309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings
struct  LinearWheelsSettings_t3897781309 
{
public:
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::lift
	Color_t2555686324  ___lift_0;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::gamma
	Color_t2555686324  ___gamma_1;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::gain
	Color_t2555686324  ___gain_2;

public:
	inline static int32_t get_offset_of_lift_0() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___lift_0)); }
	inline Color_t2555686324  get_lift_0() const { return ___lift_0; }
	inline Color_t2555686324 * get_address_of_lift_0() { return &___lift_0; }
	inline void set_lift_0(Color_t2555686324  value)
	{
		___lift_0 = value;
	}

	inline static int32_t get_offset_of_gamma_1() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___gamma_1)); }
	inline Color_t2555686324  get_gamma_1() const { return ___gamma_1; }
	inline Color_t2555686324 * get_address_of_gamma_1() { return &___gamma_1; }
	inline void set_gamma_1(Color_t2555686324  value)
	{
		___gamma_1 = value;
	}

	inline static int32_t get_offset_of_gain_2() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___gain_2)); }
	inline Color_t2555686324  get_gain_2() const { return ___gain_2; }
	inline Color_t2555686324 * get_address_of_gain_2() { return &___gain_2; }
	inline void set_gain_2(Color_t2555686324  value)
	{
		___gain_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARWHEELSSETTINGS_T3897781309_H
#ifndef ENUMERATOR_T3398877024_H
#define ENUMERATOR_T3398877024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
struct  Enumerator_t3398877024 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1444694249 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3842366416  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3398877024, ___dictionary_0)); }
	inline Dictionary_2_t1444694249 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1444694249 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1444694249 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3398877024, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3398877024, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3398877024, ___current_3)); }
	inline KeyValuePair_2_t3842366416  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3842366416 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3842366416  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3398877024_H
#ifndef ENUMERATOR_T754912357_H
#define ENUMERATOR_T754912357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>
struct  Enumerator_t754912357 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3095696878 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1198401749  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t754912357, ___dictionary_0)); }
	inline Dictionary_2_t3095696878 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3095696878 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3095696878 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t754912357, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t754912357, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t754912357, ___current_3)); }
	inline KeyValuePair_2_t1198401749  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1198401749 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1198401749  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T754912357_H
#ifndef COMMANDBUFFER_T2206337031_H
#define COMMANDBUFFER_T2206337031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t2206337031  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t2206337031, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T2206337031_H
#ifndef MOTIONBLURMODEL_T3080286123_H
#define MOTIONBLURMODEL_T3080286123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurModel
struct  MotionBlurModel_t3080286123  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.MotionBlurModel/Settings UnityEngine.PostProcessing.MotionBlurModel::m_Settings
	Settings_t4282162361  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(MotionBlurModel_t3080286123, ___m_Settings_1)); }
	inline Settings_t4282162361  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4282162361 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4282162361  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURMODEL_T3080286123_H
#ifndef EVENT_T2956885303_H
#define EVENT_T2956885303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Event
struct  Event_t2956885303  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Event::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Event_t2956885303, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

struct Event_t2956885303_StaticFields
{
public:
	// UnityEngine.Event UnityEngine.Event::s_Current
	Event_t2956885303 * ___s_Current_1;
	// UnityEngine.Event UnityEngine.Event::s_MasterEvent
	Event_t2956885303 * ___s_MasterEvent_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UnityEngine.Event::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_3;

public:
	inline static int32_t get_offset_of_s_Current_1() { return static_cast<int32_t>(offsetof(Event_t2956885303_StaticFields, ___s_Current_1)); }
	inline Event_t2956885303 * get_s_Current_1() const { return ___s_Current_1; }
	inline Event_t2956885303 ** get_address_of_s_Current_1() { return &___s_Current_1; }
	inline void set_s_Current_1(Event_t2956885303 * value)
	{
		___s_Current_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Current_1), value);
	}

	inline static int32_t get_offset_of_s_MasterEvent_2() { return static_cast<int32_t>(offsetof(Event_t2956885303_StaticFields, ___s_MasterEvent_2)); }
	inline Event_t2956885303 * get_s_MasterEvent_2() const { return ___s_MasterEvent_2; }
	inline Event_t2956885303 ** get_address_of_s_MasterEvent_2() { return &___s_MasterEvent_2; }
	inline void set_s_MasterEvent_2(Event_t2956885303 * value)
	{
		___s_MasterEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_MasterEvent_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_3() { return static_cast<int32_t>(offsetof(Event_t2956885303_StaticFields, ___U3CU3Ef__switchU24map0_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_3() const { return ___U3CU3Ef__switchU24map0_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_3() { return &___U3CU3Ef__switchU24map0_3; }
	inline void set_U3CU3Ef__switchU24map0_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Event
struct Event_t2956885303_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Event
struct Event_t2956885303_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // EVENT_T2956885303_H
#ifndef DEPTHTEXTUREMODE_T4161834719_H
#define DEPTHTEXTUREMODE_T4161834719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DepthTextureMode
struct  DepthTextureMode_t4161834719 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DepthTextureMode_t4161834719, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHTEXTUREMODE_T4161834719_H
#ifndef MODE_T2695902415_H
#define MODE_T2695902415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel/Mode
struct  Mode_t2695902415 
{
public:
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsModel/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t2695902415, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2695902415_H
#ifndef EVENTTYPE_T3528516131_H
#define EVENTTYPE_T3528516131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventType
struct  EventType_t3528516131 
{
public:
	// System.Int32 UnityEngine.EventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventType_t3528516131, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T3528516131_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DitheringModel>
struct  PostProcessingComponentRenderTexture_1_t3997290437  : public PostProcessingComponent_1_t1722181598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.GrainModel>
struct  PostProcessingComponentRenderTexture_1_t2721167529  : public PostProcessingComponent_1_t446058690
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GRAINCOMPONENT_T866324317_H
#define GRAINCOMPONENT_T866324317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainComponent
struct  GrainComponent_t866324317  : public PostProcessingComponentRenderTexture_1_t2721167529
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.GrainComponent::m_GrainLookupRT
	RenderTexture_t2108887433 * ___m_GrainLookupRT_2;

public:
	inline static int32_t get_offset_of_m_GrainLookupRT_2() { return static_cast<int32_t>(offsetof(GrainComponent_t866324317, ___m_GrainLookupRT_2)); }
	inline RenderTexture_t2108887433 * get_m_GrainLookupRT_2() const { return ___m_GrainLookupRT_2; }
	inline RenderTexture_t2108887433 ** get_address_of_m_GrainLookupRT_2() { return &___m_GrainLookupRT_2; }
	inline void set_m_GrainLookupRT_2(RenderTexture_t2108887433 * value)
	{
		___m_GrainLookupRT_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GrainLookupRT_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINCOMPONENT_T866324317_H
#ifndef SETTINGS_T2874244444_H
#define SETTINGS_T2874244444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct  Settings_t2874244444 
{
public:
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::lowPercent
	float ___lowPercent_0;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::highPercent
	float ___highPercent_1;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::minLuminance
	float ___minLuminance_2;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::maxLuminance
	float ___maxLuminance_3;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::keyValue
	float ___keyValue_4;
	// System.Boolean UnityEngine.PostProcessing.EyeAdaptationModel/Settings::dynamicKeyValue
	bool ___dynamicKeyValue_5;
	// UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType UnityEngine.PostProcessing.EyeAdaptationModel/Settings::adaptationType
	int32_t ___adaptationType_6;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::speedUp
	float ___speedUp_7;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::speedDown
	float ___speedDown_8;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/Settings::logMin
	int32_t ___logMin_9;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/Settings::logMax
	int32_t ___logMax_10;

public:
	inline static int32_t get_offset_of_lowPercent_0() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___lowPercent_0)); }
	inline float get_lowPercent_0() const { return ___lowPercent_0; }
	inline float* get_address_of_lowPercent_0() { return &___lowPercent_0; }
	inline void set_lowPercent_0(float value)
	{
		___lowPercent_0 = value;
	}

	inline static int32_t get_offset_of_highPercent_1() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___highPercent_1)); }
	inline float get_highPercent_1() const { return ___highPercent_1; }
	inline float* get_address_of_highPercent_1() { return &___highPercent_1; }
	inline void set_highPercent_1(float value)
	{
		___highPercent_1 = value;
	}

	inline static int32_t get_offset_of_minLuminance_2() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___minLuminance_2)); }
	inline float get_minLuminance_2() const { return ___minLuminance_2; }
	inline float* get_address_of_minLuminance_2() { return &___minLuminance_2; }
	inline void set_minLuminance_2(float value)
	{
		___minLuminance_2 = value;
	}

	inline static int32_t get_offset_of_maxLuminance_3() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___maxLuminance_3)); }
	inline float get_maxLuminance_3() const { return ___maxLuminance_3; }
	inline float* get_address_of_maxLuminance_3() { return &___maxLuminance_3; }
	inline void set_maxLuminance_3(float value)
	{
		___maxLuminance_3 = value;
	}

	inline static int32_t get_offset_of_keyValue_4() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___keyValue_4)); }
	inline float get_keyValue_4() const { return ___keyValue_4; }
	inline float* get_address_of_keyValue_4() { return &___keyValue_4; }
	inline void set_keyValue_4(float value)
	{
		___keyValue_4 = value;
	}

	inline static int32_t get_offset_of_dynamicKeyValue_5() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___dynamicKeyValue_5)); }
	inline bool get_dynamicKeyValue_5() const { return ___dynamicKeyValue_5; }
	inline bool* get_address_of_dynamicKeyValue_5() { return &___dynamicKeyValue_5; }
	inline void set_dynamicKeyValue_5(bool value)
	{
		___dynamicKeyValue_5 = value;
	}

	inline static int32_t get_offset_of_adaptationType_6() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___adaptationType_6)); }
	inline int32_t get_adaptationType_6() const { return ___adaptationType_6; }
	inline int32_t* get_address_of_adaptationType_6() { return &___adaptationType_6; }
	inline void set_adaptationType_6(int32_t value)
	{
		___adaptationType_6 = value;
	}

	inline static int32_t get_offset_of_speedUp_7() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___speedUp_7)); }
	inline float get_speedUp_7() const { return ___speedUp_7; }
	inline float* get_address_of_speedUp_7() { return &___speedUp_7; }
	inline void set_speedUp_7(float value)
	{
		___speedUp_7 = value;
	}

	inline static int32_t get_offset_of_speedDown_8() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___speedDown_8)); }
	inline float get_speedDown_8() const { return ___speedDown_8; }
	inline float* get_address_of_speedDown_8() { return &___speedDown_8; }
	inline void set_speedDown_8(float value)
	{
		___speedDown_8 = value;
	}

	inline static int32_t get_offset_of_logMin_9() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___logMin_9)); }
	inline int32_t get_logMin_9() const { return ___logMin_9; }
	inline int32_t* get_address_of_logMin_9() { return &___logMin_9; }
	inline void set_logMin_9(int32_t value)
	{
		___logMin_9 = value;
	}

	inline static int32_t get_offset_of_logMax_10() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___logMax_10)); }
	inline int32_t get_logMax_10() const { return ___logMax_10; }
	inline int32_t* get_address_of_logMax_10() { return &___logMax_10; }
	inline void set_logMax_10(int32_t value)
	{
		___logMax_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct Settings_t2874244444_marshaled_pinvoke
{
	float ___lowPercent_0;
	float ___highPercent_1;
	float ___minLuminance_2;
	float ___maxLuminance_3;
	float ___keyValue_4;
	int32_t ___dynamicKeyValue_5;
	int32_t ___adaptationType_6;
	float ___speedUp_7;
	float ___speedDown_8;
	int32_t ___logMin_9;
	int32_t ___logMax_10;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct Settings_t2874244444_marshaled_com
{
	float ___lowPercent_0;
	float ___highPercent_1;
	float ___minLuminance_2;
	float ___maxLuminance_3;
	float ___keyValue_4;
	int32_t ___dynamicKeyValue_5;
	int32_t ___adaptationType_6;
	float ___speedUp_7;
	float ___speedDown_8;
	int32_t ___logMin_9;
	int32_t ___logMax_10;
};
#endif // SETTINGS_T2874244444_H
#ifndef DITHERINGCOMPONENT_T277621267_H
#define DITHERINGCOMPONENT_T277621267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringComponent
struct  DitheringComponent_t277621267  : public PostProcessingComponentRenderTexture_1_t3997290437
{
public:
	// UnityEngine.Texture2D[] UnityEngine.PostProcessing.DitheringComponent::noiseTextures
	Texture2DU5BU5D_t149664596* ___noiseTextures_2;
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent::textureIndex
	int32_t ___textureIndex_3;

public:
	inline static int32_t get_offset_of_noiseTextures_2() { return static_cast<int32_t>(offsetof(DitheringComponent_t277621267, ___noiseTextures_2)); }
	inline Texture2DU5BU5D_t149664596* get_noiseTextures_2() const { return ___noiseTextures_2; }
	inline Texture2DU5BU5D_t149664596** get_address_of_noiseTextures_2() { return &___noiseTextures_2; }
	inline void set_noiseTextures_2(Texture2DU5BU5D_t149664596* value)
	{
		___noiseTextures_2 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTextures_2), value);
	}

	inline static int32_t get_offset_of_textureIndex_3() { return static_cast<int32_t>(offsetof(DitheringComponent_t277621267, ___textureIndex_3)); }
	inline int32_t get_textureIndex_3() const { return ___textureIndex_3; }
	inline int32_t* get_address_of_textureIndex_3() { return &___textureIndex_3; }
	inline void set_textureIndex_3(int32_t value)
	{
		___textureIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERINGCOMPONENT_T277621267_H
#ifndef FXAACOMPONENT_T1312385771_H
#define FXAACOMPONENT_T1312385771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FxaaComponent
struct  FxaaComponent_t1312385771  : public PostProcessingComponentRenderTexture_1_t3089424429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAACOMPONENT_T1312385771_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef VIGNETTECOMPONENT_T3243642943_H
#define VIGNETTECOMPONENT_T3243642943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteComponent
struct  VignetteComponent_t3243642943  : public PostProcessingComponentRenderTexture_1_t118834922
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTECOMPONENT_T3243642943_H
#ifndef SETTINGS_T2195468135_H
#define SETTINGS_T2195468135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct  Settings_t2195468135 
{
public:
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::focusDistance
	float ___focusDistance_0;
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::aperture
	float ___aperture_1;
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::focalLength
	float ___focalLength_2;
	// System.Boolean UnityEngine.PostProcessing.DepthOfFieldModel/Settings::useCameraFov
	bool ___useCameraFov_3;
	// UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize UnityEngine.PostProcessing.DepthOfFieldModel/Settings::kernelSize
	int32_t ___kernelSize_4;

public:
	inline static int32_t get_offset_of_focusDistance_0() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___focusDistance_0)); }
	inline float get_focusDistance_0() const { return ___focusDistance_0; }
	inline float* get_address_of_focusDistance_0() { return &___focusDistance_0; }
	inline void set_focusDistance_0(float value)
	{
		___focusDistance_0 = value;
	}

	inline static int32_t get_offset_of_aperture_1() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___aperture_1)); }
	inline float get_aperture_1() const { return ___aperture_1; }
	inline float* get_address_of_aperture_1() { return &___aperture_1; }
	inline void set_aperture_1(float value)
	{
		___aperture_1 = value;
	}

	inline static int32_t get_offset_of_focalLength_2() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___focalLength_2)); }
	inline float get_focalLength_2() const { return ___focalLength_2; }
	inline float* get_address_of_focalLength_2() { return &___focalLength_2; }
	inline void set_focalLength_2(float value)
	{
		___focalLength_2 = value;
	}

	inline static int32_t get_offset_of_useCameraFov_3() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___useCameraFov_3)); }
	inline bool get_useCameraFov_3() const { return ___useCameraFov_3; }
	inline bool* get_address_of_useCameraFov_3() { return &___useCameraFov_3; }
	inline void set_useCameraFov_3(bool value)
	{
		___useCameraFov_3 = value;
	}

	inline static int32_t get_offset_of_kernelSize_4() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___kernelSize_4)); }
	inline int32_t get_kernelSize_4() const { return ___kernelSize_4; }
	inline int32_t* get_address_of_kernelSize_4() { return &___kernelSize_4; }
	inline void set_kernelSize_4(int32_t value)
	{
		___kernelSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct Settings_t2195468135_marshaled_pinvoke
{
	float ___focusDistance_0;
	float ___aperture_1;
	float ___focalLength_2;
	int32_t ___useCameraFov_3;
	int32_t ___kernelSize_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct Settings_t2195468135_marshaled_com
{
	float ___focusDistance_0;
	float ___aperture_1;
	float ___focalLength_2;
	int32_t ___useCameraFov_3;
	int32_t ___kernelSize_4;
};
#endif // SETTINGS_T2195468135_H
#ifndef FXAASETTINGS_T1280675075_H
#define FXAASETTINGS_T1280675075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings
struct  FxaaSettings_t1280675075 
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings::preset
	int32_t ___preset_0;

public:
	inline static int32_t get_offset_of_preset_0() { return static_cast<int32_t>(offsetof(FxaaSettings_t1280675075, ___preset_0)); }
	inline int32_t get_preset_0() const { return ___preset_0; }
	inline int32_t* get_address_of_preset_0() { return &___preset_0; }
	inline void set_preset_0(int32_t value)
	{
		___preset_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAASETTINGS_T1280675075_H
#ifndef USERLUTCOMPONENT_T2843161776_H
#define USERLUTCOMPONENT_T2843161776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutComponent
struct  UserLutComponent_t2843161776  : public PostProcessingComponentRenderTexture_1_t3238393121
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERLUTCOMPONENT_T2843161776_H
#ifndef FOGCOMPONENT_T3400726830_H
#define FOGCOMPONENT_T3400726830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogComponent
struct  FogComponent_t3400726830  : public PostProcessingComponentCommandBuffer_1_t601023342
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGCOMPONENT_T3400726830_H
#ifndef COLORWHEELSSETTINGS_T3120867866_H
#define COLORWHEELSSETTINGS_T3120867866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings
struct  ColorWheelsSettings_t3120867866 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::mode
	int32_t ___mode_0;
	// UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::log
	LogWheelsSettings_t1545220311  ___log_1;
	// UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::linear
	LinearWheelsSettings_t3897781309  ___linear_2;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_log_1() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___log_1)); }
	inline LogWheelsSettings_t1545220311  get_log_1() const { return ___log_1; }
	inline LogWheelsSettings_t1545220311 * get_address_of_log_1() { return &___log_1; }
	inline void set_log_1(LogWheelsSettings_t1545220311  value)
	{
		___log_1 = value;
	}

	inline static int32_t get_offset_of_linear_2() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___linear_2)); }
	inline LinearWheelsSettings_t3897781309  get_linear_2() const { return ___linear_2; }
	inline LinearWheelsSettings_t3897781309 * get_address_of_linear_2() { return &___linear_2; }
	inline void set_linear_2(LinearWheelsSettings_t3897781309  value)
	{
		___linear_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELSSETTINGS_T3120867866_H
#ifndef MOTIONBLURCOMPONENT_T3686516877_H
#define MOTIONBLURCOMPONENT_T3686516877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent
struct  MotionBlurComponent_t3686516877  : public PostProcessingComponentCommandBuffer_1_t60620716
{
public:
	// UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter UnityEngine.PostProcessing.MotionBlurComponent::m_ReconstructionFilter
	ReconstructionFilter_t705677647 * ___m_ReconstructionFilter_2;
	// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter UnityEngine.PostProcessing.MotionBlurComponent::m_FrameBlendingFilter
	FrameBlendingFilter_t2699796096 * ___m_FrameBlendingFilter_3;
	// System.Boolean UnityEngine.PostProcessing.MotionBlurComponent::m_FirstFrame
	bool ___m_FirstFrame_4;

public:
	inline static int32_t get_offset_of_m_ReconstructionFilter_2() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_ReconstructionFilter_2)); }
	inline ReconstructionFilter_t705677647 * get_m_ReconstructionFilter_2() const { return ___m_ReconstructionFilter_2; }
	inline ReconstructionFilter_t705677647 ** get_address_of_m_ReconstructionFilter_2() { return &___m_ReconstructionFilter_2; }
	inline void set_m_ReconstructionFilter_2(ReconstructionFilter_t705677647 * value)
	{
		___m_ReconstructionFilter_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReconstructionFilter_2), value);
	}

	inline static int32_t get_offset_of_m_FrameBlendingFilter_3() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_FrameBlendingFilter_3)); }
	inline FrameBlendingFilter_t2699796096 * get_m_FrameBlendingFilter_3() const { return ___m_FrameBlendingFilter_3; }
	inline FrameBlendingFilter_t2699796096 ** get_address_of_m_FrameBlendingFilter_3() { return &___m_FrameBlendingFilter_3; }
	inline void set_m_FrameBlendingFilter_3(FrameBlendingFilter_t2699796096 * value)
	{
		___m_FrameBlendingFilter_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_FrameBlendingFilter_3), value);
	}

	inline static int32_t get_offset_of_m_FirstFrame_4() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_FirstFrame_4)); }
	inline bool get_m_FirstFrame_4() const { return ___m_FirstFrame_4; }
	inline bool* get_address_of_m_FirstFrame_4() { return &___m_FirstFrame_4; }
	inline void set_m_FirstFrame_4(bool value)
	{
		___m_FirstFrame_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURCOMPONENT_T3686516877_H
#ifndef BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#define BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent
struct  BuiltinDebugViewsComponent_t2123147871  : public PostProcessingComponentCommandBuffer_1_t2737920729
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray UnityEngine.PostProcessing.BuiltinDebugViewsComponent::m_Arrows
	ArrowArray_t303178545 * ___m_Arrows_3;

public:
	inline static int32_t get_offset_of_m_Arrows_3() { return static_cast<int32_t>(offsetof(BuiltinDebugViewsComponent_t2123147871, ___m_Arrows_3)); }
	inline ArrowArray_t303178545 * get_m_Arrows_3() const { return ___m_Arrows_3; }
	inline ArrowArray_t303178545 ** get_address_of_m_Arrows_3() { return &___m_Arrows_3; }
	inline void set_m_Arrows_3(ArrowArray_t303178545 * value)
	{
		___m_Arrows_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arrows_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#ifndef AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#define AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent
struct  AmbientOcclusionComponent_t4130625043  : public PostProcessingComponentCommandBuffer_1_t1664772955
{
public:
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.PostProcessing.AmbientOcclusionComponent::m_MRT
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_MRT_4;

public:
	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(AmbientOcclusionComponent_t4130625043, ___m_MRT_4)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#ifndef SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#define SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent
struct  ScreenSpaceReflectionComponent_t856094247  : public PostProcessingComponentCommandBuffer_1_t6679325
{
public:
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_HighlightSuppression
	bool ___k_HighlightSuppression_2;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_TraceBehindObjects
	bool ___k_TraceBehindObjects_3;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_TreatBackfaceHitAsMiss
	bool ___k_TreatBackfaceHitAsMiss_4;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_BilateralUpsample
	bool ___k_BilateralUpsample_5;
	// System.Int32[] UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::m_ReflectionTextures
	Int32U5BU5D_t385246372* ___m_ReflectionTextures_6;

public:
	inline static int32_t get_offset_of_k_HighlightSuppression_2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_HighlightSuppression_2)); }
	inline bool get_k_HighlightSuppression_2() const { return ___k_HighlightSuppression_2; }
	inline bool* get_address_of_k_HighlightSuppression_2() { return &___k_HighlightSuppression_2; }
	inline void set_k_HighlightSuppression_2(bool value)
	{
		___k_HighlightSuppression_2 = value;
	}

	inline static int32_t get_offset_of_k_TraceBehindObjects_3() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_TraceBehindObjects_3)); }
	inline bool get_k_TraceBehindObjects_3() const { return ___k_TraceBehindObjects_3; }
	inline bool* get_address_of_k_TraceBehindObjects_3() { return &___k_TraceBehindObjects_3; }
	inline void set_k_TraceBehindObjects_3(bool value)
	{
		___k_TraceBehindObjects_3 = value;
	}

	inline static int32_t get_offset_of_k_TreatBackfaceHitAsMiss_4() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_TreatBackfaceHitAsMiss_4)); }
	inline bool get_k_TreatBackfaceHitAsMiss_4() const { return ___k_TreatBackfaceHitAsMiss_4; }
	inline bool* get_address_of_k_TreatBackfaceHitAsMiss_4() { return &___k_TreatBackfaceHitAsMiss_4; }
	inline void set_k_TreatBackfaceHitAsMiss_4(bool value)
	{
		___k_TreatBackfaceHitAsMiss_4 = value;
	}

	inline static int32_t get_offset_of_k_BilateralUpsample_5() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_BilateralUpsample_5)); }
	inline bool get_k_BilateralUpsample_5() const { return ___k_BilateralUpsample_5; }
	inline bool* get_address_of_k_BilateralUpsample_5() { return &___k_BilateralUpsample_5; }
	inline void set_k_BilateralUpsample_5(bool value)
	{
		___k_BilateralUpsample_5 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionTextures_6() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___m_ReflectionTextures_6)); }
	inline Int32U5BU5D_t385246372* get_m_ReflectionTextures_6() const { return ___m_ReflectionTextures_6; }
	inline Int32U5BU5D_t385246372** get_address_of_m_ReflectionTextures_6() { return &___m_ReflectionTextures_6; }
	inline void set_m_ReflectionTextures_6(Int32U5BU5D_t385246372* value)
	{
		___m_ReflectionTextures_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTextures_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#ifndef TAACOMPONENT_T3791749658_H
#define TAACOMPONENT_T3791749658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TaaComponent
struct  TaaComponent_t3791749658  : public PostProcessingComponentRenderTexture_1_t3089424429
{
public:
	// UnityEngine.RenderBuffer[] UnityEngine.PostProcessing.TaaComponent::m_MRT
	RenderBufferU5BU5D_t1615831949* ___m_MRT_4;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent::m_SampleIndex
	int32_t ___m_SampleIndex_5;
	// System.Boolean UnityEngine.PostProcessing.TaaComponent::m_ResetHistory
	bool ___m_ResetHistory_6;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.TaaComponent::m_HistoryTexture
	RenderTexture_t2108887433 * ___m_HistoryTexture_7;

public:
	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_MRT_4)); }
	inline RenderBufferU5BU5D_t1615831949* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderBufferU5BU5D_t1615831949** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderBufferU5BU5D_t1615831949* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}

	inline static int32_t get_offset_of_m_SampleIndex_5() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_SampleIndex_5)); }
	inline int32_t get_m_SampleIndex_5() const { return ___m_SampleIndex_5; }
	inline int32_t* get_address_of_m_SampleIndex_5() { return &___m_SampleIndex_5; }
	inline void set_m_SampleIndex_5(int32_t value)
	{
		___m_SampleIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_ResetHistory_6() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_ResetHistory_6)); }
	inline bool get_m_ResetHistory_6() const { return ___m_ResetHistory_6; }
	inline bool* get_address_of_m_ResetHistory_6() { return &___m_ResetHistory_6; }
	inline void set_m_ResetHistory_6(bool value)
	{
		___m_ResetHistory_6 = value;
	}

	inline static int32_t get_offset_of_m_HistoryTexture_7() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_HistoryTexture_7)); }
	inline RenderTexture_t2108887433 * get_m_HistoryTexture_7() const { return ___m_HistoryTexture_7; }
	inline RenderTexture_t2108887433 ** get_address_of_m_HistoryTexture_7() { return &___m_HistoryTexture_7; }
	inline void set_m_HistoryTexture_7(RenderTexture_t2108887433 * value)
	{
		___m_HistoryTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryTexture_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAACOMPONENT_T3791749658_H
#ifndef BLOOMCOMPONENT_T3791419130_H
#define BLOOMCOMPONENT_T3791419130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent
struct  BloomComponent_t3791419130  : public PostProcessingComponentRenderTexture_1_t3668012901
{
public:
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer1
	RenderTextureU5BU5D_t4111643188* ___m_BlurBuffer1_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer2
	RenderTextureU5BU5D_t4111643188* ___m_BlurBuffer2_4;

public:
	inline static int32_t get_offset_of_m_BlurBuffer1_3() { return static_cast<int32_t>(offsetof(BloomComponent_t3791419130, ___m_BlurBuffer1_3)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_BlurBuffer1_3() const { return ___m_BlurBuffer1_3; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_BlurBuffer1_3() { return &___m_BlurBuffer1_3; }
	inline void set_m_BlurBuffer1_3(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_BlurBuffer1_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer1_3), value);
	}

	inline static int32_t get_offset_of_m_BlurBuffer2_4() { return static_cast<int32_t>(offsetof(BloomComponent_t3791419130, ___m_BlurBuffer2_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_BlurBuffer2_4() const { return ___m_BlurBuffer2_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_BlurBuffer2_4() { return &___m_BlurBuffer2_4; }
	inline void set_m_BlurBuffer2_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_BlurBuffer2_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMCOMPONENT_T3791419130_H
#ifndef CHROMATICABERRATIONCOMPONENT_T1647263118_H
#define CHROMATICABERRATIONCOMPONENT_T1647263118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationComponent
struct  ChromaticAberrationComponent_t1647263118  : public PostProcessingComponentRenderTexture_1_t1236717598
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ChromaticAberrationComponent::m_SpectrumLut
	Texture2D_t3840446185 * ___m_SpectrumLut_2;

public:
	inline static int32_t get_offset_of_m_SpectrumLut_2() { return static_cast<int32_t>(offsetof(ChromaticAberrationComponent_t1647263118, ___m_SpectrumLut_2)); }
	inline Texture2D_t3840446185 * get_m_SpectrumLut_2() const { return ___m_SpectrumLut_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_SpectrumLut_2() { return &___m_SpectrumLut_2; }
	inline void set_m_SpectrumLut_2(Texture2D_t3840446185 * value)
	{
		___m_SpectrumLut_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpectrumLut_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATIONCOMPONENT_T1647263118_H
#ifndef COLORGRADINGCOMPONENT_T1715259467_H
#define COLORGRADINGCOMPONENT_T1715259467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingComponent
struct  ColorGradingComponent_t1715259467  : public PostProcessingComponentRenderTexture_1_t3016333222
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ColorGradingComponent::m_GradingCurves
	Texture2D_t3840446185 * ___m_GradingCurves_5;

public:
	inline static int32_t get_offset_of_m_GradingCurves_5() { return static_cast<int32_t>(offsetof(ColorGradingComponent_t1715259467, ___m_GradingCurves_5)); }
	inline Texture2D_t3840446185 * get_m_GradingCurves_5() const { return ___m_GradingCurves_5; }
	inline Texture2D_t3840446185 ** get_address_of_m_GradingCurves_5() { return &___m_GradingCurves_5; }
	inline void set_m_GradingCurves_5(Texture2D_t3840446185 * value)
	{
		___m_GradingCurves_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradingCurves_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGCOMPONENT_T1715259467_H
#ifndef TONEMAPPINGSETTINGS_T4154044775_H
#define TONEMAPPINGSETTINGS_T4154044775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings
struct  TonemappingSettings_t4154044775 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/Tonemapper UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::tonemapper
	int32_t ___tonemapper_0;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralBlackIn
	float ___neutralBlackIn_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteIn
	float ___neutralWhiteIn_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralBlackOut
	float ___neutralBlackOut_3;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteOut
	float ___neutralWhiteOut_4;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteLevel
	float ___neutralWhiteLevel_5;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteClip
	float ___neutralWhiteClip_6;

public:
	inline static int32_t get_offset_of_tonemapper_0() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___tonemapper_0)); }
	inline int32_t get_tonemapper_0() const { return ___tonemapper_0; }
	inline int32_t* get_address_of_tonemapper_0() { return &___tonemapper_0; }
	inline void set_tonemapper_0(int32_t value)
	{
		___tonemapper_0 = value;
	}

	inline static int32_t get_offset_of_neutralBlackIn_1() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralBlackIn_1)); }
	inline float get_neutralBlackIn_1() const { return ___neutralBlackIn_1; }
	inline float* get_address_of_neutralBlackIn_1() { return &___neutralBlackIn_1; }
	inline void set_neutralBlackIn_1(float value)
	{
		___neutralBlackIn_1 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteIn_2() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteIn_2)); }
	inline float get_neutralWhiteIn_2() const { return ___neutralWhiteIn_2; }
	inline float* get_address_of_neutralWhiteIn_2() { return &___neutralWhiteIn_2; }
	inline void set_neutralWhiteIn_2(float value)
	{
		___neutralWhiteIn_2 = value;
	}

	inline static int32_t get_offset_of_neutralBlackOut_3() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralBlackOut_3)); }
	inline float get_neutralBlackOut_3() const { return ___neutralBlackOut_3; }
	inline float* get_address_of_neutralBlackOut_3() { return &___neutralBlackOut_3; }
	inline void set_neutralBlackOut_3(float value)
	{
		___neutralBlackOut_3 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteOut_4() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteOut_4)); }
	inline float get_neutralWhiteOut_4() const { return ___neutralWhiteOut_4; }
	inline float* get_address_of_neutralWhiteOut_4() { return &___neutralWhiteOut_4; }
	inline void set_neutralWhiteOut_4(float value)
	{
		___neutralWhiteOut_4 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteLevel_5() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteLevel_5)); }
	inline float get_neutralWhiteLevel_5() const { return ___neutralWhiteLevel_5; }
	inline float* get_address_of_neutralWhiteLevel_5() { return &___neutralWhiteLevel_5; }
	inline void set_neutralWhiteLevel_5(float value)
	{
		___neutralWhiteLevel_5 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteClip_6() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteClip_6)); }
	inline float get_neutralWhiteClip_6() const { return ___neutralWhiteClip_6; }
	inline float* get_address_of_neutralWhiteClip_6() { return &___neutralWhiteClip_6; }
	inline void set_neutralWhiteClip_6(float value)
	{
		___neutralWhiteClip_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPINGSETTINGS_T4154044775_H
#ifndef EYEADAPTATIONCOMPONENT_T3394805121_H
#define EYEADAPTATIONCOMPONENT_T3394805121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationComponent
struct  EyeAdaptationComponent_t3394805121  : public PostProcessingComponentRenderTexture_1_t1811108953
{
public:
	// UnityEngine.ComputeShader UnityEngine.PostProcessing.EyeAdaptationComponent::m_EyeCompute
	ComputeShader_t317220254 * ___m_EyeCompute_2;
	// UnityEngine.ComputeBuffer UnityEngine.PostProcessing.EyeAdaptationComponent::m_HistogramBuffer
	ComputeBuffer_t1033194329 * ___m_HistogramBuffer_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.EyeAdaptationComponent::m_AutoExposurePool
	RenderTextureU5BU5D_t4111643188* ___m_AutoExposurePool_4;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent::m_AutoExposurePingPing
	int32_t ___m_AutoExposurePingPing_5;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.EyeAdaptationComponent::m_CurrentAutoExposure
	RenderTexture_t2108887433 * ___m_CurrentAutoExposure_6;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.EyeAdaptationComponent::m_DebugHistogram
	RenderTexture_t2108887433 * ___m_DebugHistogram_7;
	// System.Boolean UnityEngine.PostProcessing.EyeAdaptationComponent::m_FirstFrame
	bool ___m_FirstFrame_9;

public:
	inline static int32_t get_offset_of_m_EyeCompute_2() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_EyeCompute_2)); }
	inline ComputeShader_t317220254 * get_m_EyeCompute_2() const { return ___m_EyeCompute_2; }
	inline ComputeShader_t317220254 ** get_address_of_m_EyeCompute_2() { return &___m_EyeCompute_2; }
	inline void set_m_EyeCompute_2(ComputeShader_t317220254 * value)
	{
		___m_EyeCompute_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EyeCompute_2), value);
	}

	inline static int32_t get_offset_of_m_HistogramBuffer_3() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_HistogramBuffer_3)); }
	inline ComputeBuffer_t1033194329 * get_m_HistogramBuffer_3() const { return ___m_HistogramBuffer_3; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_HistogramBuffer_3() { return &___m_HistogramBuffer_3; }
	inline void set_m_HistogramBuffer_3(ComputeBuffer_t1033194329 * value)
	{
		___m_HistogramBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistogramBuffer_3), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePool_4() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_AutoExposurePool_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_AutoExposurePool_4() const { return ___m_AutoExposurePool_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_AutoExposurePool_4() { return &___m_AutoExposurePool_4; }
	inline void set_m_AutoExposurePool_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_AutoExposurePool_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AutoExposurePool_4), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePingPing_5() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_AutoExposurePingPing_5)); }
	inline int32_t get_m_AutoExposurePingPing_5() const { return ___m_AutoExposurePingPing_5; }
	inline int32_t* get_address_of_m_AutoExposurePingPing_5() { return &___m_AutoExposurePingPing_5; }
	inline void set_m_AutoExposurePingPing_5(int32_t value)
	{
		___m_AutoExposurePingPing_5 = value;
	}

	inline static int32_t get_offset_of_m_CurrentAutoExposure_6() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_CurrentAutoExposure_6)); }
	inline RenderTexture_t2108887433 * get_m_CurrentAutoExposure_6() const { return ___m_CurrentAutoExposure_6; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CurrentAutoExposure_6() { return &___m_CurrentAutoExposure_6; }
	inline void set_m_CurrentAutoExposure_6(RenderTexture_t2108887433 * value)
	{
		___m_CurrentAutoExposure_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentAutoExposure_6), value);
	}

	inline static int32_t get_offset_of_m_DebugHistogram_7() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_DebugHistogram_7)); }
	inline RenderTexture_t2108887433 * get_m_DebugHistogram_7() const { return ___m_DebugHistogram_7; }
	inline RenderTexture_t2108887433 ** get_address_of_m_DebugHistogram_7() { return &___m_DebugHistogram_7; }
	inline void set_m_DebugHistogram_7(RenderTexture_t2108887433 * value)
	{
		___m_DebugHistogram_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugHistogram_7), value);
	}

	inline static int32_t get_offset_of_m_FirstFrame_9() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_FirstFrame_9)); }
	inline bool get_m_FirstFrame_9() const { return ___m_FirstFrame_9; }
	inline bool* get_address_of_m_FirstFrame_9() { return &___m_FirstFrame_9; }
	inline void set_m_FirstFrame_9(bool value)
	{
		___m_FirstFrame_9 = value;
	}
};

struct EyeAdaptationComponent_t3394805121_StaticFields
{
public:
	// System.UInt32[] UnityEngine.PostProcessing.EyeAdaptationComponent::s_EmptyHistogramBuffer
	UInt32U5BU5D_t2770800703* ___s_EmptyHistogramBuffer_8;

public:
	inline static int32_t get_offset_of_s_EmptyHistogramBuffer_8() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121_StaticFields, ___s_EmptyHistogramBuffer_8)); }
	inline UInt32U5BU5D_t2770800703* get_s_EmptyHistogramBuffer_8() const { return ___s_EmptyHistogramBuffer_8; }
	inline UInt32U5BU5D_t2770800703** get_address_of_s_EmptyHistogramBuffer_8() { return &___s_EmptyHistogramBuffer_8; }
	inline void set_s_EmptyHistogramBuffer_8(UInt32U5BU5D_t2770800703* value)
	{
		___s_EmptyHistogramBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyHistogramBuffer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONCOMPONENT_T3394805121_H
#ifndef DEPTHOFFIELDCOMPONENT_T554756766_H
#define DEPTHOFFIELDCOMPONENT_T554756766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldComponent
struct  DepthOfFieldComponent_t554756766  : public PostProcessingComponentRenderTexture_1_t2082352371
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.DepthOfFieldComponent::m_CoCHistory
	RenderTexture_t2108887433 * ___m_CoCHistory_3;
	// UnityEngine.RenderBuffer[] UnityEngine.PostProcessing.DepthOfFieldComponent::m_MRT
	RenderBufferU5BU5D_t1615831949* ___m_MRT_4;

public:
	inline static int32_t get_offset_of_m_CoCHistory_3() { return static_cast<int32_t>(offsetof(DepthOfFieldComponent_t554756766, ___m_CoCHistory_3)); }
	inline RenderTexture_t2108887433 * get_m_CoCHistory_3() const { return ___m_CoCHistory_3; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CoCHistory_3() { return &___m_CoCHistory_3; }
	inline void set_m_CoCHistory_3(RenderTexture_t2108887433 * value)
	{
		___m_CoCHistory_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoCHistory_3), value);
	}

	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(DepthOfFieldComponent_t554756766, ___m_MRT_4)); }
	inline RenderBufferU5BU5D_t1615831949* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderBufferU5BU5D_t1615831949** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderBufferU5BU5D_t1615831949* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDCOMPONENT_T554756766_H
#ifndef BLOOMMODEL_T2099727860_H
#define BLOOMMODEL_T2099727860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomModel
struct  BloomModel_t2099727860  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.BloomModel/Settings UnityEngine.PostProcessing.BloomModel::m_Settings
	Settings_t181254429  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(BloomModel_t2099727860, ___m_Settings_1)); }
	inline Settings_t181254429  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t181254429 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t181254429  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMMODEL_T2099727860_H
#ifndef SETTINGS_T1354494600_H
#define SETTINGS_T1354494600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel/Settings
struct  Settings_t1354494600 
{
public:
	// UnityEngine.PostProcessing.VignetteModel/Mode UnityEngine.PostProcessing.VignetteModel/Settings::mode
	int32_t ___mode_0;
	// UnityEngine.Color UnityEngine.PostProcessing.VignetteModel/Settings::color
	Color_t2555686324  ___color_1;
	// UnityEngine.Vector2 UnityEngine.PostProcessing.VignetteModel/Settings::center
	Vector2_t2156229523  ___center_2;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::intensity
	float ___intensity_3;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::smoothness
	float ___smoothness_4;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::roundness
	float ___roundness_5;
	// UnityEngine.Texture UnityEngine.PostProcessing.VignetteModel/Settings::mask
	Texture_t3661962703 * ___mask_6;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::opacity
	float ___opacity_7;
	// System.Boolean UnityEngine.PostProcessing.VignetteModel/Settings::rounded
	bool ___rounded_8;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___color_1)); }
	inline Color_t2555686324  get_color_1() const { return ___color_1; }
	inline Color_t2555686324 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2555686324  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_center_2() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___center_2)); }
	inline Vector2_t2156229523  get_center_2() const { return ___center_2; }
	inline Vector2_t2156229523 * get_address_of_center_2() { return &___center_2; }
	inline void set_center_2(Vector2_t2156229523  value)
	{
		___center_2 = value;
	}

	inline static int32_t get_offset_of_intensity_3() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___intensity_3)); }
	inline float get_intensity_3() const { return ___intensity_3; }
	inline float* get_address_of_intensity_3() { return &___intensity_3; }
	inline void set_intensity_3(float value)
	{
		___intensity_3 = value;
	}

	inline static int32_t get_offset_of_smoothness_4() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___smoothness_4)); }
	inline float get_smoothness_4() const { return ___smoothness_4; }
	inline float* get_address_of_smoothness_4() { return &___smoothness_4; }
	inline void set_smoothness_4(float value)
	{
		___smoothness_4 = value;
	}

	inline static int32_t get_offset_of_roundness_5() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___roundness_5)); }
	inline float get_roundness_5() const { return ___roundness_5; }
	inline float* get_address_of_roundness_5() { return &___roundness_5; }
	inline void set_roundness_5(float value)
	{
		___roundness_5 = value;
	}

	inline static int32_t get_offset_of_mask_6() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___mask_6)); }
	inline Texture_t3661962703 * get_mask_6() const { return ___mask_6; }
	inline Texture_t3661962703 ** get_address_of_mask_6() { return &___mask_6; }
	inline void set_mask_6(Texture_t3661962703 * value)
	{
		___mask_6 = value;
		Il2CppCodeGenWriteBarrier((&___mask_6), value);
	}

	inline static int32_t get_offset_of_opacity_7() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___opacity_7)); }
	inline float get_opacity_7() const { return ___opacity_7; }
	inline float* get_address_of_opacity_7() { return &___opacity_7; }
	inline void set_opacity_7(float value)
	{
		___opacity_7 = value;
	}

	inline static int32_t get_offset_of_rounded_8() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___rounded_8)); }
	inline bool get_rounded_8() const { return ___rounded_8; }
	inline bool* get_address_of_rounded_8() { return &___rounded_8; }
	inline void set_rounded_8(bool value)
	{
		___rounded_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.VignetteModel/Settings
struct Settings_t1354494600_marshaled_pinvoke
{
	int32_t ___mode_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	Texture_t3661962703 * ___mask_6;
	float ___opacity_7;
	int32_t ___rounded_8;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.VignetteModel/Settings
struct Settings_t1354494600_marshaled_com
{
	int32_t ___mode_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	Texture_t3661962703 * ___mask_6;
	float ___opacity_7;
	int32_t ___rounded_8;
};
#endif // SETTINGS_T1354494600_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef KEYVALUEPAIR_2_T3423445140_H
#define KEYVALUEPAIR_2_T3423445140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>
struct  KeyValuePair_2_t3423445140 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	CommandBuffer_t2206337031 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3423445140, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3423445140, ___value_1)); }
	inline CommandBuffer_t2206337031 * get_value_1() const { return ___value_1; }
	inline CommandBuffer_t2206337031 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(CommandBuffer_t2206337031 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3423445140_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef REFLECTIONSETTINGS_T282755190_H
#define REFLECTIONSETTINGS_T282755190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct  ReflectionSettings_t282755190 
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::blendType
	int32_t ___blendType_0;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectionQuality
	int32_t ___reflectionQuality_1;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::maxDistance
	float ___maxDistance_2;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::iterationCount
	int32_t ___iterationCount_3;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::stepSize
	int32_t ___stepSize_4;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::widthModifier
	float ___widthModifier_5;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectionBlur
	float ___reflectionBlur_6;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectBackfaces
	bool ___reflectBackfaces_7;

public:
	inline static int32_t get_offset_of_blendType_0() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___blendType_0)); }
	inline int32_t get_blendType_0() const { return ___blendType_0; }
	inline int32_t* get_address_of_blendType_0() { return &___blendType_0; }
	inline void set_blendType_0(int32_t value)
	{
		___blendType_0 = value;
	}

	inline static int32_t get_offset_of_reflectionQuality_1() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectionQuality_1)); }
	inline int32_t get_reflectionQuality_1() const { return ___reflectionQuality_1; }
	inline int32_t* get_address_of_reflectionQuality_1() { return &___reflectionQuality_1; }
	inline void set_reflectionQuality_1(int32_t value)
	{
		___reflectionQuality_1 = value;
	}

	inline static int32_t get_offset_of_maxDistance_2() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___maxDistance_2)); }
	inline float get_maxDistance_2() const { return ___maxDistance_2; }
	inline float* get_address_of_maxDistance_2() { return &___maxDistance_2; }
	inline void set_maxDistance_2(float value)
	{
		___maxDistance_2 = value;
	}

	inline static int32_t get_offset_of_iterationCount_3() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___iterationCount_3)); }
	inline int32_t get_iterationCount_3() const { return ___iterationCount_3; }
	inline int32_t* get_address_of_iterationCount_3() { return &___iterationCount_3; }
	inline void set_iterationCount_3(int32_t value)
	{
		___iterationCount_3 = value;
	}

	inline static int32_t get_offset_of_stepSize_4() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___stepSize_4)); }
	inline int32_t get_stepSize_4() const { return ___stepSize_4; }
	inline int32_t* get_address_of_stepSize_4() { return &___stepSize_4; }
	inline void set_stepSize_4(int32_t value)
	{
		___stepSize_4 = value;
	}

	inline static int32_t get_offset_of_widthModifier_5() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___widthModifier_5)); }
	inline float get_widthModifier_5() const { return ___widthModifier_5; }
	inline float* get_address_of_widthModifier_5() { return &___widthModifier_5; }
	inline void set_widthModifier_5(float value)
	{
		___widthModifier_5 = value;
	}

	inline static int32_t get_offset_of_reflectionBlur_6() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectionBlur_6)); }
	inline float get_reflectionBlur_6() const { return ___reflectionBlur_6; }
	inline float* get_address_of_reflectionBlur_6() { return &___reflectionBlur_6; }
	inline void set_reflectionBlur_6(float value)
	{
		___reflectionBlur_6 = value;
	}

	inline static int32_t get_offset_of_reflectBackfaces_7() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectBackfaces_7)); }
	inline bool get_reflectBackfaces_7() const { return ___reflectBackfaces_7; }
	inline bool* get_address_of_reflectBackfaces_7() { return &___reflectBackfaces_7; }
	inline void set_reflectBackfaces_7(bool value)
	{
		___reflectBackfaces_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct ReflectionSettings_t282755190_marshaled_pinvoke
{
	int32_t ___blendType_0;
	int32_t ___reflectionQuality_1;
	float ___maxDistance_2;
	int32_t ___iterationCount_3;
	int32_t ___stepSize_4;
	float ___widthModifier_5;
	float ___reflectionBlur_6;
	int32_t ___reflectBackfaces_7;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct ReflectionSettings_t282755190_marshaled_com
{
	int32_t ___blendType_0;
	int32_t ___reflectionQuality_1;
	float ___maxDistance_2;
	int32_t ___iterationCount_3;
	int32_t ___stepSize_4;
	float ___widthModifier_5;
	float ___reflectionBlur_6;
	int32_t ___reflectBackfaces_7;
};
#endif // REFLECTIONSETTINGS_T282755190_H
#ifndef SETTINGS_T3016786575_H
#define SETTINGS_T3016786575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct  Settings_t3016786575 
{
public:
	// System.Single UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::intensity
	float ___intensity_0;
	// System.Single UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::radius
	float ___radius_1;
	// UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::sampleCount
	int32_t ___sampleCount_2;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::downsampling
	bool ___downsampling_3;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::forceForwardCompatibility
	bool ___forceForwardCompatibility_4;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::ambientOnly
	bool ___ambientOnly_5;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::highPrecision
	bool ___highPrecision_6;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}

	inline static int32_t get_offset_of_radius_1() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___radius_1)); }
	inline float get_radius_1() const { return ___radius_1; }
	inline float* get_address_of_radius_1() { return &___radius_1; }
	inline void set_radius_1(float value)
	{
		___radius_1 = value;
	}

	inline static int32_t get_offset_of_sampleCount_2() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___sampleCount_2)); }
	inline int32_t get_sampleCount_2() const { return ___sampleCount_2; }
	inline int32_t* get_address_of_sampleCount_2() { return &___sampleCount_2; }
	inline void set_sampleCount_2(int32_t value)
	{
		___sampleCount_2 = value;
	}

	inline static int32_t get_offset_of_downsampling_3() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___downsampling_3)); }
	inline bool get_downsampling_3() const { return ___downsampling_3; }
	inline bool* get_address_of_downsampling_3() { return &___downsampling_3; }
	inline void set_downsampling_3(bool value)
	{
		___downsampling_3 = value;
	}

	inline static int32_t get_offset_of_forceForwardCompatibility_4() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___forceForwardCompatibility_4)); }
	inline bool get_forceForwardCompatibility_4() const { return ___forceForwardCompatibility_4; }
	inline bool* get_address_of_forceForwardCompatibility_4() { return &___forceForwardCompatibility_4; }
	inline void set_forceForwardCompatibility_4(bool value)
	{
		___forceForwardCompatibility_4 = value;
	}

	inline static int32_t get_offset_of_ambientOnly_5() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___ambientOnly_5)); }
	inline bool get_ambientOnly_5() const { return ___ambientOnly_5; }
	inline bool* get_address_of_ambientOnly_5() { return &___ambientOnly_5; }
	inline void set_ambientOnly_5(bool value)
	{
		___ambientOnly_5 = value;
	}

	inline static int32_t get_offset_of_highPrecision_6() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___highPrecision_6)); }
	inline bool get_highPrecision_6() const { return ___highPrecision_6; }
	inline bool* get_address_of_highPrecision_6() { return &___highPrecision_6; }
	inline void set_highPrecision_6(bool value)
	{
		___highPrecision_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct Settings_t3016786575_marshaled_pinvoke
{
	float ___intensity_0;
	float ___radius_1;
	int32_t ___sampleCount_2;
	int32_t ___downsampling_3;
	int32_t ___forceForwardCompatibility_4;
	int32_t ___ambientOnly_5;
	int32_t ___highPrecision_6;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct Settings_t3016786575_marshaled_com
{
	float ___intensity_0;
	float ___radius_1;
	int32_t ___sampleCount_2;
	int32_t ___downsampling_3;
	int32_t ___forceForwardCompatibility_4;
	int32_t ___ambientOnly_5;
	int32_t ___highPrecision_6;
};
#endif // SETTINGS_T3016786575_H
#ifndef RENDERTARGETIDENTIFIER_T2079184500_H
#define RENDERTARGETIDENTIFIER_T2079184500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_t2079184500 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_BufferPointer_3() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_BufferPointer_3)); }
	inline intptr_t get_m_BufferPointer_3() const { return ___m_BufferPointer_3; }
	inline intptr_t* get_address_of_m_BufferPointer_3() { return &___m_BufferPointer_3; }
	inline void set_m_BufferPointer_3(intptr_t value)
	{
		___m_BufferPointer_3 = value;
	}

	inline static int32_t get_offset_of_m_MipLevel_4() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_MipLevel_4)); }
	inline int32_t get_m_MipLevel_4() const { return ___m_MipLevel_4; }
	inline int32_t* get_address_of_m_MipLevel_4() { return &___m_MipLevel_4; }
	inline void set_m_MipLevel_4(int32_t value)
	{
		___m_MipLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_CubeFace_5() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_CubeFace_5)); }
	inline int32_t get_m_CubeFace_5() const { return ___m_CubeFace_5; }
	inline int32_t* get_address_of_m_CubeFace_5() { return &___m_CubeFace_5; }
	inline void set_m_CubeFace_5(int32_t value)
	{
		___m_CubeFace_5 = value;
	}

	inline static int32_t get_offset_of_m_DepthSlice_6() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t2079184500, ___m_DepthSlice_6)); }
	inline int32_t get_m_DepthSlice_6() const { return ___m_DepthSlice_6; }
	inline int32_t* get_address_of_m_DepthSlice_6() { return &___m_DepthSlice_6; }
	inline void set_m_DepthSlice_6(int32_t value)
	{
		___m_DepthSlice_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_T2079184500_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef SETTINGS_T3984509665_H
#define SETTINGS_T3984509665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings
struct  Settings_t3984509665 
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel/Mode UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings::mode
	int32_t ___mode_0;
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel/DepthSettings UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings::depth
	DepthSettings_t1820272864  ___depth_1;
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings::motionVectors
	MotionVectorsSettings_t3857813598  ___motionVectors_2;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(Settings_t3984509665, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_depth_1() { return static_cast<int32_t>(offsetof(Settings_t3984509665, ___depth_1)); }
	inline DepthSettings_t1820272864  get_depth_1() const { return ___depth_1; }
	inline DepthSettings_t1820272864 * get_address_of_depth_1() { return &___depth_1; }
	inline void set_depth_1(DepthSettings_t1820272864  value)
	{
		___depth_1 = value;
	}

	inline static int32_t get_offset_of_motionVectors_2() { return static_cast<int32_t>(offsetof(Settings_t3984509665, ___motionVectors_2)); }
	inline MotionVectorsSettings_t3857813598  get_motionVectors_2() const { return ___motionVectors_2; }
	inline MotionVectorsSettings_t3857813598 * get_address_of_motionVectors_2() { return &___motionVectors_2; }
	inline void set_motionVectors_2(MotionVectorsSettings_t3857813598  value)
	{
		___motionVectors_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T3984509665_H
#ifndef KEYVALUEPAIR_2_T2246977_H
#define KEYVALUEPAIR_2_T2246977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>
struct  KeyValuePair_2_t2246977 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2246977, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2246977, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2246977_H
#ifndef RENDERTEXTURE_T2108887433_H
#define RENDERTEXTURE_T2108887433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2108887433  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2108887433_H
#ifndef SETTINGS_T4292431647_H
#define SETTINGS_T4292431647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/Settings
struct  Settings_t4292431647 
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/Method UnityEngine.PostProcessing.AntialiasingModel/Settings::method
	int32_t ___method_0;
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings UnityEngine.PostProcessing.AntialiasingModel/Settings::fxaaSettings
	FxaaSettings_t1280675075  ___fxaaSettings_1;
	// UnityEngine.PostProcessing.AntialiasingModel/TaaSettings UnityEngine.PostProcessing.AntialiasingModel/Settings::taaSettings
	TaaSettings_t2709374970  ___taaSettings_2;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___method_0)); }
	inline int32_t get_method_0() const { return ___method_0; }
	inline int32_t* get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(int32_t value)
	{
		___method_0 = value;
	}

	inline static int32_t get_offset_of_fxaaSettings_1() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___fxaaSettings_1)); }
	inline FxaaSettings_t1280675075  get_fxaaSettings_1() const { return ___fxaaSettings_1; }
	inline FxaaSettings_t1280675075 * get_address_of_fxaaSettings_1() { return &___fxaaSettings_1; }
	inline void set_fxaaSettings_1(FxaaSettings_t1280675075  value)
	{
		___fxaaSettings_1 = value;
	}

	inline static int32_t get_offset_of_taaSettings_2() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___taaSettings_2)); }
	inline TaaSettings_t2709374970  get_taaSettings_2() const { return ___taaSettings_2; }
	inline TaaSettings_t2709374970 * get_address_of_taaSettings_2() { return &___taaSettings_2; }
	inline void set_taaSettings_2(TaaSettings_t2709374970  value)
	{
		___taaSettings_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T4292431647_H
#ifndef KEYVALUEPAIR_2_T3970497075_H
#define KEYVALUEPAIR_2_T3970497075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct  KeyValuePair_2_t3970497075 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Type_t * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	KeyValuePair_2_t3423445140  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3970497075, ___key_0)); }
	inline Type_t * get_key_0() const { return ___key_0; }
	inline Type_t ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Type_t * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3970497075, ___value_1)); }
	inline KeyValuePair_2_t3423445140  get_value_1() const { return ___value_1; }
	inline KeyValuePair_2_t3423445140 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(KeyValuePair_2_t3423445140  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3970497075_H
#ifndef KEYVALUEPAIR_2_T3747325428_H
#define KEYVALUEPAIR_2_T3747325428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct  KeyValuePair_2_t3747325428 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	KeyValuePair_2_t2246977  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3747325428, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3747325428, ___value_1)); }
	inline KeyValuePair_2_t2246977  get_value_1() const { return ___value_1; }
	inline KeyValuePair_2_t2246977 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(KeyValuePair_2_t2246977  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3747325428_H
#ifndef SETTINGS_T1995791524_H
#define SETTINGS_T1995791524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct  Settings_t1995791524 
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::reflection
	ReflectionSettings_t282755190  ___reflection_0;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::intensity
	IntensitySettings_t1721872184  ___intensity_1;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::screenEdgeMask
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;

public:
	inline static int32_t get_offset_of_reflection_0() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___reflection_0)); }
	inline ReflectionSettings_t282755190  get_reflection_0() const { return ___reflection_0; }
	inline ReflectionSettings_t282755190 * get_address_of_reflection_0() { return &___reflection_0; }
	inline void set_reflection_0(ReflectionSettings_t282755190  value)
	{
		___reflection_0 = value;
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___intensity_1)); }
	inline IntensitySettings_t1721872184  get_intensity_1() const { return ___intensity_1; }
	inline IntensitySettings_t1721872184 * get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(IntensitySettings_t1721872184  value)
	{
		___intensity_1 = value;
	}

	inline static int32_t get_offset_of_screenEdgeMask_2() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___screenEdgeMask_2)); }
	inline ScreenEdgeMask_t4063288584  get_screenEdgeMask_2() const { return ___screenEdgeMask_2; }
	inline ScreenEdgeMask_t4063288584 * get_address_of_screenEdgeMask_2() { return &___screenEdgeMask_2; }
	inline void set_screenEdgeMask_2(ScreenEdgeMask_t4063288584  value)
	{
		___screenEdgeMask_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct Settings_t1995791524_marshaled_pinvoke
{
	ReflectionSettings_t282755190_marshaled_pinvoke ___reflection_0;
	IntensitySettings_t1721872184  ___intensity_1;
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct Settings_t1995791524_marshaled_com
{
	ReflectionSettings_t282755190_marshaled_com ___reflection_0;
	IntensitySettings_t1721872184  ___intensity_1;
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;
};
#endif // SETTINGS_T1995791524_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef FUNC_2_T4093140010_H
#define FUNC_2_T4093140010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>
struct  Func_2_t4093140010  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T4093140010_H
#ifndef AMBIENTOCCLUSIONMODEL_T389471066_H
#define AMBIENTOCCLUSIONMODEL_T389471066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel
struct  AmbientOcclusionModel_t389471066  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.AmbientOcclusionModel/Settings UnityEngine.PostProcessing.AmbientOcclusionModel::m_Settings
	Settings_t3016786575  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(AmbientOcclusionModel_t389471066, ___m_Settings_1)); }
	inline Settings_t3016786575  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t3016786575 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t3016786575  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONMODEL_T389471066_H
#ifndef POSTPROCESSINGPROFILE_T724195375_H
#define POSTPROCESSINGPROFILE_T724195375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingProfile
struct  PostProcessingProfile_t724195375  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel UnityEngine.PostProcessing.PostProcessingProfile::debugViews
	BuiltinDebugViewsModel_t1462618840 * ___debugViews_2;
	// UnityEngine.PostProcessing.FogModel UnityEngine.PostProcessing.PostProcessingProfile::fog
	FogModel_t3620688749 * ___fog_3;
	// UnityEngine.PostProcessing.AntialiasingModel UnityEngine.PostProcessing.PostProcessingProfile::antialiasing
	AntialiasingModel_t1521139388 * ___antialiasing_4;
	// UnityEngine.PostProcessing.AmbientOcclusionModel UnityEngine.PostProcessing.PostProcessingProfile::ambientOcclusion
	AmbientOcclusionModel_t389471066 * ___ambientOcclusion_5;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel UnityEngine.PostProcessing.PostProcessingProfile::screenSpaceReflection
	ScreenSpaceReflectionModel_t3026344732 * ___screenSpaceReflection_6;
	// UnityEngine.PostProcessing.DepthOfFieldModel UnityEngine.PostProcessing.PostProcessingProfile::depthOfField
	DepthOfFieldModel_t514067330 * ___depthOfField_7;
	// UnityEngine.PostProcessing.MotionBlurModel UnityEngine.PostProcessing.PostProcessingProfile::motionBlur
	MotionBlurModel_t3080286123 * ___motionBlur_8;
	// UnityEngine.PostProcessing.EyeAdaptationModel UnityEngine.PostProcessing.PostProcessingProfile::eyeAdaptation
	EyeAdaptationModel_t242823912 * ___eyeAdaptation_9;
	// UnityEngine.PostProcessing.BloomModel UnityEngine.PostProcessing.PostProcessingProfile::bloom
	BloomModel_t2099727860 * ___bloom_10;
	// UnityEngine.PostProcessing.ColorGradingModel UnityEngine.PostProcessing.PostProcessingProfile::colorGrading
	ColorGradingModel_t1448048181 * ___colorGrading_11;
	// UnityEngine.PostProcessing.UserLutModel UnityEngine.PostProcessing.PostProcessingProfile::userLut
	UserLutModel_t1670108080 * ___userLut_12;
	// UnityEngine.PostProcessing.ChromaticAberrationModel UnityEngine.PostProcessing.PostProcessingProfile::chromaticAberration
	ChromaticAberrationModel_t3963399853 * ___chromaticAberration_13;
	// UnityEngine.PostProcessing.GrainModel UnityEngine.PostProcessing.PostProcessingProfile::grain
	GrainModel_t1152882488 * ___grain_14;
	// UnityEngine.PostProcessing.VignetteModel UnityEngine.PostProcessing.PostProcessingProfile::vignette
	VignetteModel_t2845517177 * ___vignette_15;
	// UnityEngine.PostProcessing.DitheringModel UnityEngine.PostProcessing.PostProcessingProfile::dithering
	DitheringModel_t2429005396 * ___dithering_16;

public:
	inline static int32_t get_offset_of_debugViews_2() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___debugViews_2)); }
	inline BuiltinDebugViewsModel_t1462618840 * get_debugViews_2() const { return ___debugViews_2; }
	inline BuiltinDebugViewsModel_t1462618840 ** get_address_of_debugViews_2() { return &___debugViews_2; }
	inline void set_debugViews_2(BuiltinDebugViewsModel_t1462618840 * value)
	{
		___debugViews_2 = value;
		Il2CppCodeGenWriteBarrier((&___debugViews_2), value);
	}

	inline static int32_t get_offset_of_fog_3() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___fog_3)); }
	inline FogModel_t3620688749 * get_fog_3() const { return ___fog_3; }
	inline FogModel_t3620688749 ** get_address_of_fog_3() { return &___fog_3; }
	inline void set_fog_3(FogModel_t3620688749 * value)
	{
		___fog_3 = value;
		Il2CppCodeGenWriteBarrier((&___fog_3), value);
	}

	inline static int32_t get_offset_of_antialiasing_4() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___antialiasing_4)); }
	inline AntialiasingModel_t1521139388 * get_antialiasing_4() const { return ___antialiasing_4; }
	inline AntialiasingModel_t1521139388 ** get_address_of_antialiasing_4() { return &___antialiasing_4; }
	inline void set_antialiasing_4(AntialiasingModel_t1521139388 * value)
	{
		___antialiasing_4 = value;
		Il2CppCodeGenWriteBarrier((&___antialiasing_4), value);
	}

	inline static int32_t get_offset_of_ambientOcclusion_5() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___ambientOcclusion_5)); }
	inline AmbientOcclusionModel_t389471066 * get_ambientOcclusion_5() const { return ___ambientOcclusion_5; }
	inline AmbientOcclusionModel_t389471066 ** get_address_of_ambientOcclusion_5() { return &___ambientOcclusion_5; }
	inline void set_ambientOcclusion_5(AmbientOcclusionModel_t389471066 * value)
	{
		___ambientOcclusion_5 = value;
		Il2CppCodeGenWriteBarrier((&___ambientOcclusion_5), value);
	}

	inline static int32_t get_offset_of_screenSpaceReflection_6() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___screenSpaceReflection_6)); }
	inline ScreenSpaceReflectionModel_t3026344732 * get_screenSpaceReflection_6() const { return ___screenSpaceReflection_6; }
	inline ScreenSpaceReflectionModel_t3026344732 ** get_address_of_screenSpaceReflection_6() { return &___screenSpaceReflection_6; }
	inline void set_screenSpaceReflection_6(ScreenSpaceReflectionModel_t3026344732 * value)
	{
		___screenSpaceReflection_6 = value;
		Il2CppCodeGenWriteBarrier((&___screenSpaceReflection_6), value);
	}

	inline static int32_t get_offset_of_depthOfField_7() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___depthOfField_7)); }
	inline DepthOfFieldModel_t514067330 * get_depthOfField_7() const { return ___depthOfField_7; }
	inline DepthOfFieldModel_t514067330 ** get_address_of_depthOfField_7() { return &___depthOfField_7; }
	inline void set_depthOfField_7(DepthOfFieldModel_t514067330 * value)
	{
		___depthOfField_7 = value;
		Il2CppCodeGenWriteBarrier((&___depthOfField_7), value);
	}

	inline static int32_t get_offset_of_motionBlur_8() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___motionBlur_8)); }
	inline MotionBlurModel_t3080286123 * get_motionBlur_8() const { return ___motionBlur_8; }
	inline MotionBlurModel_t3080286123 ** get_address_of_motionBlur_8() { return &___motionBlur_8; }
	inline void set_motionBlur_8(MotionBlurModel_t3080286123 * value)
	{
		___motionBlur_8 = value;
		Il2CppCodeGenWriteBarrier((&___motionBlur_8), value);
	}

	inline static int32_t get_offset_of_eyeAdaptation_9() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___eyeAdaptation_9)); }
	inline EyeAdaptationModel_t242823912 * get_eyeAdaptation_9() const { return ___eyeAdaptation_9; }
	inline EyeAdaptationModel_t242823912 ** get_address_of_eyeAdaptation_9() { return &___eyeAdaptation_9; }
	inline void set_eyeAdaptation_9(EyeAdaptationModel_t242823912 * value)
	{
		___eyeAdaptation_9 = value;
		Il2CppCodeGenWriteBarrier((&___eyeAdaptation_9), value);
	}

	inline static int32_t get_offset_of_bloom_10() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___bloom_10)); }
	inline BloomModel_t2099727860 * get_bloom_10() const { return ___bloom_10; }
	inline BloomModel_t2099727860 ** get_address_of_bloom_10() { return &___bloom_10; }
	inline void set_bloom_10(BloomModel_t2099727860 * value)
	{
		___bloom_10 = value;
		Il2CppCodeGenWriteBarrier((&___bloom_10), value);
	}

	inline static int32_t get_offset_of_colorGrading_11() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___colorGrading_11)); }
	inline ColorGradingModel_t1448048181 * get_colorGrading_11() const { return ___colorGrading_11; }
	inline ColorGradingModel_t1448048181 ** get_address_of_colorGrading_11() { return &___colorGrading_11; }
	inline void set_colorGrading_11(ColorGradingModel_t1448048181 * value)
	{
		___colorGrading_11 = value;
		Il2CppCodeGenWriteBarrier((&___colorGrading_11), value);
	}

	inline static int32_t get_offset_of_userLut_12() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___userLut_12)); }
	inline UserLutModel_t1670108080 * get_userLut_12() const { return ___userLut_12; }
	inline UserLutModel_t1670108080 ** get_address_of_userLut_12() { return &___userLut_12; }
	inline void set_userLut_12(UserLutModel_t1670108080 * value)
	{
		___userLut_12 = value;
		Il2CppCodeGenWriteBarrier((&___userLut_12), value);
	}

	inline static int32_t get_offset_of_chromaticAberration_13() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___chromaticAberration_13)); }
	inline ChromaticAberrationModel_t3963399853 * get_chromaticAberration_13() const { return ___chromaticAberration_13; }
	inline ChromaticAberrationModel_t3963399853 ** get_address_of_chromaticAberration_13() { return &___chromaticAberration_13; }
	inline void set_chromaticAberration_13(ChromaticAberrationModel_t3963399853 * value)
	{
		___chromaticAberration_13 = value;
		Il2CppCodeGenWriteBarrier((&___chromaticAberration_13), value);
	}

	inline static int32_t get_offset_of_grain_14() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___grain_14)); }
	inline GrainModel_t1152882488 * get_grain_14() const { return ___grain_14; }
	inline GrainModel_t1152882488 ** get_address_of_grain_14() { return &___grain_14; }
	inline void set_grain_14(GrainModel_t1152882488 * value)
	{
		___grain_14 = value;
		Il2CppCodeGenWriteBarrier((&___grain_14), value);
	}

	inline static int32_t get_offset_of_vignette_15() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___vignette_15)); }
	inline VignetteModel_t2845517177 * get_vignette_15() const { return ___vignette_15; }
	inline VignetteModel_t2845517177 ** get_address_of_vignette_15() { return &___vignette_15; }
	inline void set_vignette_15(VignetteModel_t2845517177 * value)
	{
		___vignette_15 = value;
		Il2CppCodeGenWriteBarrier((&___vignette_15), value);
	}

	inline static int32_t get_offset_of_dithering_16() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___dithering_16)); }
	inline DitheringModel_t2429005396 * get_dithering_16() const { return ___dithering_16; }
	inline DitheringModel_t2429005396 ** get_address_of_dithering_16() { return &___dithering_16; }
	inline void set_dithering_16(DitheringModel_t2429005396 * value)
	{
		___dithering_16 = value;
		Il2CppCodeGenWriteBarrier((&___dithering_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGPROFILE_T724195375_H
#ifndef BUILTINDEBUGVIEWSMODEL_T1462618840_H
#define BUILTINDEBUGVIEWSMODEL_T1462618840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct  BuiltinDebugViewsModel_t1462618840  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings UnityEngine.PostProcessing.BuiltinDebugViewsModel::m_Settings
	Settings_t3984509665  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(BuiltinDebugViewsModel_t1462618840, ___m_Settings_1)); }
	inline Settings_t3984509665  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t3984509665 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t3984509665  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINDEBUGVIEWSMODEL_T1462618840_H
#ifndef SETTINGS_T451872061_H
#define SETTINGS_T451872061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/Settings
struct  Settings_t451872061 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::tonemapping
	TonemappingSettings_t4154044775  ___tonemapping_0;
	// UnityEngine.PostProcessing.ColorGradingModel/BasicSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::basic
	BasicSettings_t838098426  ___basic_1;
	// UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::channelMixer
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::colorWheels
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	// UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::curves
	CurvesSettings_t2830270037  ___curves_4;

public:
	inline static int32_t get_offset_of_tonemapping_0() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___tonemapping_0)); }
	inline TonemappingSettings_t4154044775  get_tonemapping_0() const { return ___tonemapping_0; }
	inline TonemappingSettings_t4154044775 * get_address_of_tonemapping_0() { return &___tonemapping_0; }
	inline void set_tonemapping_0(TonemappingSettings_t4154044775  value)
	{
		___tonemapping_0 = value;
	}

	inline static int32_t get_offset_of_basic_1() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___basic_1)); }
	inline BasicSettings_t838098426  get_basic_1() const { return ___basic_1; }
	inline BasicSettings_t838098426 * get_address_of_basic_1() { return &___basic_1; }
	inline void set_basic_1(BasicSettings_t838098426  value)
	{
		___basic_1 = value;
	}

	inline static int32_t get_offset_of_channelMixer_2() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___channelMixer_2)); }
	inline ChannelMixerSettings_t898701698  get_channelMixer_2() const { return ___channelMixer_2; }
	inline ChannelMixerSettings_t898701698 * get_address_of_channelMixer_2() { return &___channelMixer_2; }
	inline void set_channelMixer_2(ChannelMixerSettings_t898701698  value)
	{
		___channelMixer_2 = value;
	}

	inline static int32_t get_offset_of_colorWheels_3() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___colorWheels_3)); }
	inline ColorWheelsSettings_t3120867866  get_colorWheels_3() const { return ___colorWheels_3; }
	inline ColorWheelsSettings_t3120867866 * get_address_of_colorWheels_3() { return &___colorWheels_3; }
	inline void set_colorWheels_3(ColorWheelsSettings_t3120867866  value)
	{
		___colorWheels_3 = value;
	}

	inline static int32_t get_offset_of_curves_4() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___curves_4)); }
	inline CurvesSettings_t2830270037  get_curves_4() const { return ___curves_4; }
	inline CurvesSettings_t2830270037 * get_address_of_curves_4() { return &___curves_4; }
	inline void set_curves_4(CurvesSettings_t2830270037  value)
	{
		___curves_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ColorGradingModel/Settings
struct Settings_t451872061_marshaled_pinvoke
{
	TonemappingSettings_t4154044775  ___tonemapping_0;
	BasicSettings_t838098426  ___basic_1;
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	CurvesSettings_t2830270037_marshaled_pinvoke ___curves_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ColorGradingModel/Settings
struct Settings_t451872061_marshaled_com
{
	TonemappingSettings_t4154044775  ___tonemapping_0;
	BasicSettings_t838098426  ___basic_1;
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	CurvesSettings_t2830270037_marshaled_com ___curves_4;
};
#endif // SETTINGS_T451872061_H
#ifndef DEPTHOFFIELDMODEL_T514067330_H
#define DEPTHOFFIELDMODEL_T514067330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel
struct  DepthOfFieldModel_t514067330  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.DepthOfFieldModel/Settings UnityEngine.PostProcessing.DepthOfFieldModel::m_Settings
	Settings_t2195468135  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(DepthOfFieldModel_t514067330, ___m_Settings_1)); }
	inline Settings_t2195468135  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2195468135 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2195468135  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDMODEL_T514067330_H
#ifndef EYEADAPTATIONMODEL_T242823912_H
#define EYEADAPTATIONMODEL_T242823912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel
struct  EyeAdaptationModel_t242823912  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.EyeAdaptationModel/Settings UnityEngine.PostProcessing.EyeAdaptationModel::m_Settings
	Settings_t2874244444  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(EyeAdaptationModel_t242823912, ___m_Settings_1)); }
	inline Settings_t2874244444  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2874244444 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2874244444  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONMODEL_T242823912_H
#ifndef VIGNETTEMODEL_T2845517177_H
#define VIGNETTEMODEL_T2845517177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel
struct  VignetteModel_t2845517177  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel::m_Settings
	Settings_t1354494600  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(VignetteModel_t2845517177, ___m_Settings_1)); }
	inline Settings_t1354494600  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t1354494600 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t1354494600  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEMODEL_T2845517177_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef COLORGRADINGMODEL_T1448048181_H
#define COLORGRADINGMODEL_T1448048181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel
struct  ColorGradingModel_t1448048181  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/Settings UnityEngine.PostProcessing.ColorGradingModel::m_Settings
	Settings_t451872061  ___m_Settings_1;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel::<isDirty>k__BackingField
	bool ___U3CisDirtyU3Ek__BackingField_2;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.ColorGradingModel::<bakedLut>k__BackingField
	RenderTexture_t2108887433 * ___U3CbakedLutU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(ColorGradingModel_t1448048181, ___m_Settings_1)); }
	inline Settings_t451872061  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t451872061 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t451872061  value)
	{
		___m_Settings_1 = value;
	}

	inline static int32_t get_offset_of_U3CisDirtyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ColorGradingModel_t1448048181, ___U3CisDirtyU3Ek__BackingField_2)); }
	inline bool get_U3CisDirtyU3Ek__BackingField_2() const { return ___U3CisDirtyU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CisDirtyU3Ek__BackingField_2() { return &___U3CisDirtyU3Ek__BackingField_2; }
	inline void set_U3CisDirtyU3Ek__BackingField_2(bool value)
	{
		___U3CisDirtyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CbakedLutU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ColorGradingModel_t1448048181, ___U3CbakedLutU3Ek__BackingField_3)); }
	inline RenderTexture_t2108887433 * get_U3CbakedLutU3Ek__BackingField_3() const { return ___U3CbakedLutU3Ek__BackingField_3; }
	inline RenderTexture_t2108887433 ** get_address_of_U3CbakedLutU3Ek__BackingField_3() { return &___U3CbakedLutU3Ek__BackingField_3; }
	inline void set_U3CbakedLutU3Ek__BackingField_3(RenderTexture_t2108887433 * value)
	{
		___U3CbakedLutU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbakedLutU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGMODEL_T1448048181_H
#ifndef ENUMERATOR_T3527007683_H
#define ENUMERATOR_T3527007683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct  Enumerator_t3527007683 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1572824908 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3970497075  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3527007683, ___dictionary_0)); }
	inline Dictionary_2_t1572824908 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1572824908 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1572824908 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3527007683, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3527007683, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3527007683, ___current_3)); }
	inline KeyValuePair_2_t3970497075  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3970497075 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3970497075  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3527007683_H
#ifndef ENUMERATOR_T3303836036_H
#define ENUMERATOR_T3303836036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct  Enumerator_t3303836036 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1349653261 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3747325428  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3303836036, ___dictionary_0)); }
	inline Dictionary_2_t1349653261 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1349653261 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1349653261 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3303836036, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3303836036, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3303836036, ___current_3)); }
	inline KeyValuePair_2_t3747325428  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3747325428 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3747325428  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3303836036_H
#ifndef ANTIALIASINGMODEL_T1521139388_H
#define ANTIALIASINGMODEL_T1521139388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel
struct  AntialiasingModel_t1521139388  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/Settings UnityEngine.PostProcessing.AntialiasingModel::m_Settings
	Settings_t4292431647  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(AntialiasingModel_t1521139388, ___m_Settings_1)); }
	inline Settings_t4292431647  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4292431647 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4292431647  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIALIASINGMODEL_T1521139388_H
#ifndef SCREENSPACEREFLECTIONMODEL_T3026344732_H
#define SCREENSPACEREFLECTIONMODEL_T3026344732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct  ScreenSpaceReflectionModel_t3026344732  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel::m_Settings
	Settings_t1995791524  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionModel_t3026344732, ___m_Settings_1)); }
	inline Settings_t1995791524  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t1995791524 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t1995791524  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONMODEL_T3026344732_H
#ifndef ENUMERATOR_T2141718565_H
#define ENUMERATOR_T2141718565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct  Enumerator_t2141718565 
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t3527007683  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(Enumerator_t2141718565, ___host_enumerator_0)); }
	inline Enumerator_t3527007683  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t3527007683 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t3527007683  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2141718565_H
#ifndef ENUMERATOR_T1918546918_H
#define ENUMERATOR_T1918546918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>
struct  Enumerator_t1918546918 
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t3303836036  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(Enumerator_t1918546918, ___host_enumerator_0)); }
	inline Enumerator_t3303836036  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t3303836036 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t3303836036  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1918546918_H
#ifndef POSTPROCESSINGBEHAVIOUR_T3229946336_H
#define POSTPROCESSINGBEHAVIOUR_T3229946336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingBehaviour
struct  PostProcessingBehaviour_t3229946336  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingBehaviour::profile
	PostProcessingProfile_t724195375 * ___profile_2;
	// System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4> UnityEngine.PostProcessing.PostProcessingBehaviour::jitteredMatrixFunc
	Func_2_t4093140010 * ___jitteredMatrixFunc_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>> UnityEngine.PostProcessing.PostProcessingBehaviour::m_CommandBuffers
	Dictionary_2_t1572824908 * ___m_CommandBuffers_4;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_Components
	List_1_t4203178569 * ___m_Components_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentStates
	Dictionary_2_t3095696878 * ___m_ComponentStates_6;
	// UnityEngine.PostProcessing.MaterialFactory UnityEngine.PostProcessing.PostProcessingBehaviour::m_MaterialFactory
	MaterialFactory_t2445948724 * ___m_MaterialFactory_7;
	// UnityEngine.PostProcessing.RenderTextureFactory UnityEngine.PostProcessing.PostProcessingBehaviour::m_RenderTextureFactory
	RenderTextureFactory_t1946967824 * ___m_RenderTextureFactory_8;
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingBehaviour::m_Context
	PostProcessingContext_t2014408948 * ___m_Context_9;
	// UnityEngine.Camera UnityEngine.PostProcessing.PostProcessingBehaviour::m_Camera
	Camera_t4157153871 * ___m_Camera_10;
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingBehaviour::m_PreviousProfile
	PostProcessingProfile_t724195375 * ___m_PreviousProfile_11;
	// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::m_RenderingInSceneView
	bool ___m_RenderingInSceneView_12;
	// UnityEngine.PostProcessing.BuiltinDebugViewsComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_DebugViews
	BuiltinDebugViewsComponent_t2123147871 * ___m_DebugViews_13;
	// UnityEngine.PostProcessing.AmbientOcclusionComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_AmbientOcclusion
	AmbientOcclusionComponent_t4130625043 * ___m_AmbientOcclusion_14;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ScreenSpaceReflection
	ScreenSpaceReflectionComponent_t856094247 * ___m_ScreenSpaceReflection_15;
	// UnityEngine.PostProcessing.FogComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_FogComponent
	FogComponent_t3400726830 * ___m_FogComponent_16;
	// UnityEngine.PostProcessing.MotionBlurComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_MotionBlur
	MotionBlurComponent_t3686516877 * ___m_MotionBlur_17;
	// UnityEngine.PostProcessing.TaaComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Taa
	TaaComponent_t3791749658 * ___m_Taa_18;
	// UnityEngine.PostProcessing.EyeAdaptationComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_EyeAdaptation
	EyeAdaptationComponent_t3394805121 * ___m_EyeAdaptation_19;
	// UnityEngine.PostProcessing.DepthOfFieldComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_DepthOfField
	DepthOfFieldComponent_t554756766 * ___m_DepthOfField_20;
	// UnityEngine.PostProcessing.BloomComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Bloom
	BloomComponent_t3791419130 * ___m_Bloom_21;
	// UnityEngine.PostProcessing.ChromaticAberrationComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ChromaticAberration
	ChromaticAberrationComponent_t1647263118 * ___m_ChromaticAberration_22;
	// UnityEngine.PostProcessing.ColorGradingComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ColorGrading
	ColorGradingComponent_t1715259467 * ___m_ColorGrading_23;
	// UnityEngine.PostProcessing.UserLutComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_UserLut
	UserLutComponent_t2843161776 * ___m_UserLut_24;
	// UnityEngine.PostProcessing.GrainComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Grain
	GrainComponent_t866324317 * ___m_Grain_25;
	// UnityEngine.PostProcessing.VignetteComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Vignette
	VignetteComponent_t3243642943 * ___m_Vignette_26;
	// UnityEngine.PostProcessing.DitheringComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Dithering
	DitheringComponent_t277621267 * ___m_Dithering_27;
	// UnityEngine.PostProcessing.FxaaComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Fxaa
	FxaaComponent_t1312385771 * ___m_Fxaa_28;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentsToEnable
	List_1_t4203178569 * ___m_ComponentsToEnable_29;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentsToDisable
	List_1_t4203178569 * ___m_ComponentsToDisable_30;

public:
	inline static int32_t get_offset_of_profile_2() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___profile_2)); }
	inline PostProcessingProfile_t724195375 * get_profile_2() const { return ___profile_2; }
	inline PostProcessingProfile_t724195375 ** get_address_of_profile_2() { return &___profile_2; }
	inline void set_profile_2(PostProcessingProfile_t724195375 * value)
	{
		___profile_2 = value;
		Il2CppCodeGenWriteBarrier((&___profile_2), value);
	}

	inline static int32_t get_offset_of_jitteredMatrixFunc_3() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___jitteredMatrixFunc_3)); }
	inline Func_2_t4093140010 * get_jitteredMatrixFunc_3() const { return ___jitteredMatrixFunc_3; }
	inline Func_2_t4093140010 ** get_address_of_jitteredMatrixFunc_3() { return &___jitteredMatrixFunc_3; }
	inline void set_jitteredMatrixFunc_3(Func_2_t4093140010 * value)
	{
		___jitteredMatrixFunc_3 = value;
		Il2CppCodeGenWriteBarrier((&___jitteredMatrixFunc_3), value);
	}

	inline static int32_t get_offset_of_m_CommandBuffers_4() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_CommandBuffers_4)); }
	inline Dictionary_2_t1572824908 * get_m_CommandBuffers_4() const { return ___m_CommandBuffers_4; }
	inline Dictionary_2_t1572824908 ** get_address_of_m_CommandBuffers_4() { return &___m_CommandBuffers_4; }
	inline void set_m_CommandBuffers_4(Dictionary_2_t1572824908 * value)
	{
		___m_CommandBuffers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommandBuffers_4), value);
	}

	inline static int32_t get_offset_of_m_Components_5() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Components_5)); }
	inline List_1_t4203178569 * get_m_Components_5() const { return ___m_Components_5; }
	inline List_1_t4203178569 ** get_address_of_m_Components_5() { return &___m_Components_5; }
	inline void set_m_Components_5(List_1_t4203178569 * value)
	{
		___m_Components_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Components_5), value);
	}

	inline static int32_t get_offset_of_m_ComponentStates_6() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentStates_6)); }
	inline Dictionary_2_t3095696878 * get_m_ComponentStates_6() const { return ___m_ComponentStates_6; }
	inline Dictionary_2_t3095696878 ** get_address_of_m_ComponentStates_6() { return &___m_ComponentStates_6; }
	inline void set_m_ComponentStates_6(Dictionary_2_t3095696878 * value)
	{
		___m_ComponentStates_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentStates_6), value);
	}

	inline static int32_t get_offset_of_m_MaterialFactory_7() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_MaterialFactory_7)); }
	inline MaterialFactory_t2445948724 * get_m_MaterialFactory_7() const { return ___m_MaterialFactory_7; }
	inline MaterialFactory_t2445948724 ** get_address_of_m_MaterialFactory_7() { return &___m_MaterialFactory_7; }
	inline void set_m_MaterialFactory_7(MaterialFactory_t2445948724 * value)
	{
		___m_MaterialFactory_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaterialFactory_7), value);
	}

	inline static int32_t get_offset_of_m_RenderTextureFactory_8() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_RenderTextureFactory_8)); }
	inline RenderTextureFactory_t1946967824 * get_m_RenderTextureFactory_8() const { return ___m_RenderTextureFactory_8; }
	inline RenderTextureFactory_t1946967824 ** get_address_of_m_RenderTextureFactory_8() { return &___m_RenderTextureFactory_8; }
	inline void set_m_RenderTextureFactory_8(RenderTextureFactory_t1946967824 * value)
	{
		___m_RenderTextureFactory_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_RenderTextureFactory_8), value);
	}

	inline static int32_t get_offset_of_m_Context_9() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Context_9)); }
	inline PostProcessingContext_t2014408948 * get_m_Context_9() const { return ___m_Context_9; }
	inline PostProcessingContext_t2014408948 ** get_address_of_m_Context_9() { return &___m_Context_9; }
	inline void set_m_Context_9(PostProcessingContext_t2014408948 * value)
	{
		___m_Context_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Context_9), value);
	}

	inline static int32_t get_offset_of_m_Camera_10() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Camera_10)); }
	inline Camera_t4157153871 * get_m_Camera_10() const { return ___m_Camera_10; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_10() { return &___m_Camera_10; }
	inline void set_m_Camera_10(Camera_t4157153871 * value)
	{
		___m_Camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_10), value);
	}

	inline static int32_t get_offset_of_m_PreviousProfile_11() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_PreviousProfile_11)); }
	inline PostProcessingProfile_t724195375 * get_m_PreviousProfile_11() const { return ___m_PreviousProfile_11; }
	inline PostProcessingProfile_t724195375 ** get_address_of_m_PreviousProfile_11() { return &___m_PreviousProfile_11; }
	inline void set_m_PreviousProfile_11(PostProcessingProfile_t724195375 * value)
	{
		___m_PreviousProfile_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_PreviousProfile_11), value);
	}

	inline static int32_t get_offset_of_m_RenderingInSceneView_12() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_RenderingInSceneView_12)); }
	inline bool get_m_RenderingInSceneView_12() const { return ___m_RenderingInSceneView_12; }
	inline bool* get_address_of_m_RenderingInSceneView_12() { return &___m_RenderingInSceneView_12; }
	inline void set_m_RenderingInSceneView_12(bool value)
	{
		___m_RenderingInSceneView_12 = value;
	}

	inline static int32_t get_offset_of_m_DebugViews_13() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_DebugViews_13)); }
	inline BuiltinDebugViewsComponent_t2123147871 * get_m_DebugViews_13() const { return ___m_DebugViews_13; }
	inline BuiltinDebugViewsComponent_t2123147871 ** get_address_of_m_DebugViews_13() { return &___m_DebugViews_13; }
	inline void set_m_DebugViews_13(BuiltinDebugViewsComponent_t2123147871 * value)
	{
		___m_DebugViews_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugViews_13), value);
	}

	inline static int32_t get_offset_of_m_AmbientOcclusion_14() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_AmbientOcclusion_14)); }
	inline AmbientOcclusionComponent_t4130625043 * get_m_AmbientOcclusion_14() const { return ___m_AmbientOcclusion_14; }
	inline AmbientOcclusionComponent_t4130625043 ** get_address_of_m_AmbientOcclusion_14() { return &___m_AmbientOcclusion_14; }
	inline void set_m_AmbientOcclusion_14(AmbientOcclusionComponent_t4130625043 * value)
	{
		___m_AmbientOcclusion_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_AmbientOcclusion_14), value);
	}

	inline static int32_t get_offset_of_m_ScreenSpaceReflection_15() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ScreenSpaceReflection_15)); }
	inline ScreenSpaceReflectionComponent_t856094247 * get_m_ScreenSpaceReflection_15() const { return ___m_ScreenSpaceReflection_15; }
	inline ScreenSpaceReflectionComponent_t856094247 ** get_address_of_m_ScreenSpaceReflection_15() { return &___m_ScreenSpaceReflection_15; }
	inline void set_m_ScreenSpaceReflection_15(ScreenSpaceReflectionComponent_t856094247 * value)
	{
		___m_ScreenSpaceReflection_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScreenSpaceReflection_15), value);
	}

	inline static int32_t get_offset_of_m_FogComponent_16() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_FogComponent_16)); }
	inline FogComponent_t3400726830 * get_m_FogComponent_16() const { return ___m_FogComponent_16; }
	inline FogComponent_t3400726830 ** get_address_of_m_FogComponent_16() { return &___m_FogComponent_16; }
	inline void set_m_FogComponent_16(FogComponent_t3400726830 * value)
	{
		___m_FogComponent_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_FogComponent_16), value);
	}

	inline static int32_t get_offset_of_m_MotionBlur_17() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_MotionBlur_17)); }
	inline MotionBlurComponent_t3686516877 * get_m_MotionBlur_17() const { return ___m_MotionBlur_17; }
	inline MotionBlurComponent_t3686516877 ** get_address_of_m_MotionBlur_17() { return &___m_MotionBlur_17; }
	inline void set_m_MotionBlur_17(MotionBlurComponent_t3686516877 * value)
	{
		___m_MotionBlur_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_MotionBlur_17), value);
	}

	inline static int32_t get_offset_of_m_Taa_18() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Taa_18)); }
	inline TaaComponent_t3791749658 * get_m_Taa_18() const { return ___m_Taa_18; }
	inline TaaComponent_t3791749658 ** get_address_of_m_Taa_18() { return &___m_Taa_18; }
	inline void set_m_Taa_18(TaaComponent_t3791749658 * value)
	{
		___m_Taa_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Taa_18), value);
	}

	inline static int32_t get_offset_of_m_EyeAdaptation_19() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_EyeAdaptation_19)); }
	inline EyeAdaptationComponent_t3394805121 * get_m_EyeAdaptation_19() const { return ___m_EyeAdaptation_19; }
	inline EyeAdaptationComponent_t3394805121 ** get_address_of_m_EyeAdaptation_19() { return &___m_EyeAdaptation_19; }
	inline void set_m_EyeAdaptation_19(EyeAdaptationComponent_t3394805121 * value)
	{
		___m_EyeAdaptation_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_EyeAdaptation_19), value);
	}

	inline static int32_t get_offset_of_m_DepthOfField_20() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_DepthOfField_20)); }
	inline DepthOfFieldComponent_t554756766 * get_m_DepthOfField_20() const { return ___m_DepthOfField_20; }
	inline DepthOfFieldComponent_t554756766 ** get_address_of_m_DepthOfField_20() { return &___m_DepthOfField_20; }
	inline void set_m_DepthOfField_20(DepthOfFieldComponent_t554756766 * value)
	{
		___m_DepthOfField_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_DepthOfField_20), value);
	}

	inline static int32_t get_offset_of_m_Bloom_21() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Bloom_21)); }
	inline BloomComponent_t3791419130 * get_m_Bloom_21() const { return ___m_Bloom_21; }
	inline BloomComponent_t3791419130 ** get_address_of_m_Bloom_21() { return &___m_Bloom_21; }
	inline void set_m_Bloom_21(BloomComponent_t3791419130 * value)
	{
		___m_Bloom_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bloom_21), value);
	}

	inline static int32_t get_offset_of_m_ChromaticAberration_22() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ChromaticAberration_22)); }
	inline ChromaticAberrationComponent_t1647263118 * get_m_ChromaticAberration_22() const { return ___m_ChromaticAberration_22; }
	inline ChromaticAberrationComponent_t1647263118 ** get_address_of_m_ChromaticAberration_22() { return &___m_ChromaticAberration_22; }
	inline void set_m_ChromaticAberration_22(ChromaticAberrationComponent_t1647263118 * value)
	{
		___m_ChromaticAberration_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChromaticAberration_22), value);
	}

	inline static int32_t get_offset_of_m_ColorGrading_23() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ColorGrading_23)); }
	inline ColorGradingComponent_t1715259467 * get_m_ColorGrading_23() const { return ___m_ColorGrading_23; }
	inline ColorGradingComponent_t1715259467 ** get_address_of_m_ColorGrading_23() { return &___m_ColorGrading_23; }
	inline void set_m_ColorGrading_23(ColorGradingComponent_t1715259467 * value)
	{
		___m_ColorGrading_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorGrading_23), value);
	}

	inline static int32_t get_offset_of_m_UserLut_24() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_UserLut_24)); }
	inline UserLutComponent_t2843161776 * get_m_UserLut_24() const { return ___m_UserLut_24; }
	inline UserLutComponent_t2843161776 ** get_address_of_m_UserLut_24() { return &___m_UserLut_24; }
	inline void set_m_UserLut_24(UserLutComponent_t2843161776 * value)
	{
		___m_UserLut_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_UserLut_24), value);
	}

	inline static int32_t get_offset_of_m_Grain_25() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Grain_25)); }
	inline GrainComponent_t866324317 * get_m_Grain_25() const { return ___m_Grain_25; }
	inline GrainComponent_t866324317 ** get_address_of_m_Grain_25() { return &___m_Grain_25; }
	inline void set_m_Grain_25(GrainComponent_t866324317 * value)
	{
		___m_Grain_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Grain_25), value);
	}

	inline static int32_t get_offset_of_m_Vignette_26() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Vignette_26)); }
	inline VignetteComponent_t3243642943 * get_m_Vignette_26() const { return ___m_Vignette_26; }
	inline VignetteComponent_t3243642943 ** get_address_of_m_Vignette_26() { return &___m_Vignette_26; }
	inline void set_m_Vignette_26(VignetteComponent_t3243642943 * value)
	{
		___m_Vignette_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Vignette_26), value);
	}

	inline static int32_t get_offset_of_m_Dithering_27() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Dithering_27)); }
	inline DitheringComponent_t277621267 * get_m_Dithering_27() const { return ___m_Dithering_27; }
	inline DitheringComponent_t277621267 ** get_address_of_m_Dithering_27() { return &___m_Dithering_27; }
	inline void set_m_Dithering_27(DitheringComponent_t277621267 * value)
	{
		___m_Dithering_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dithering_27), value);
	}

	inline static int32_t get_offset_of_m_Fxaa_28() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Fxaa_28)); }
	inline FxaaComponent_t1312385771 * get_m_Fxaa_28() const { return ___m_Fxaa_28; }
	inline FxaaComponent_t1312385771 ** get_address_of_m_Fxaa_28() { return &___m_Fxaa_28; }
	inline void set_m_Fxaa_28(FxaaComponent_t1312385771 * value)
	{
		___m_Fxaa_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fxaa_28), value);
	}

	inline static int32_t get_offset_of_m_ComponentsToEnable_29() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentsToEnable_29)); }
	inline List_1_t4203178569 * get_m_ComponentsToEnable_29() const { return ___m_ComponentsToEnable_29; }
	inline List_1_t4203178569 ** get_address_of_m_ComponentsToEnable_29() { return &___m_ComponentsToEnable_29; }
	inline void set_m_ComponentsToEnable_29(List_1_t4203178569 * value)
	{
		___m_ComponentsToEnable_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentsToEnable_29), value);
	}

	inline static int32_t get_offset_of_m_ComponentsToDisable_30() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentsToDisable_30)); }
	inline List_1_t4203178569 * get_m_ComponentsToDisable_30() const { return ___m_ComponentsToDisable_30; }
	inline List_1_t4203178569 ** get_address_of_m_ComponentsToDisable_30() { return &___m_ComponentsToDisable_30; }
	inline void set_m_ComponentsToDisable_30(List_1_t4203178569 * value)
	{
		___m_ComponentsToDisable_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentsToDisable_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGBEHAVIOUR_T3229946336_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1615831949  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RenderBuffer_t586150500  m_Items[1];

public:
	inline RenderBuffer_t586150500  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RenderBuffer_t586150500 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RenderBuffer_t586150500  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RenderBuffer_t586150500  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RenderBuffer_t586150500 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RenderBuffer_t586150500  value)
	{
		m_Items[index] = value;
	}
};

extern "C" void ReflectionSettings_t282755190_marshal_pinvoke(const ReflectionSettings_t282755190& unmarshaled, ReflectionSettings_t282755190_marshaled_pinvoke& marshaled);
extern "C" void ReflectionSettings_t282755190_marshal_pinvoke_back(const ReflectionSettings_t282755190_marshaled_pinvoke& marshaled, ReflectionSettings_t282755190& unmarshaled);
extern "C" void ReflectionSettings_t282755190_marshal_pinvoke_cleanup(ReflectionSettings_t282755190_marshaled_pinvoke& marshaled);
extern "C" void ReflectionSettings_t282755190_marshal_com(const ReflectionSettings_t282755190& unmarshaled, ReflectionSettings_t282755190_marshaled_com& marshaled);
extern "C" void ReflectionSettings_t282755190_marshal_com_back(const ReflectionSettings_t282755190_marshaled_com& marshaled, ReflectionSettings_t282755190& unmarshaled);
extern "C" void ReflectionSettings_t282755190_marshal_com_cleanup(ReflectionSettings_t282755190_marshaled_com& marshaled);

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::.ctor()
extern "C"  void Dictionary_2__ctor_m3471890303_gshared (Dictionary_2_t1349653261 * __this, const RuntimeMethod* method);
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<System.Object>(T)
extern "C"  RuntimeObject * PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared (PostProcessingBehaviour_t3229946336 * __this, RuntimeObject * ___component0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C"  void Dictionary_2__ctor_m236774955_gshared (Dictionary_2_t1444694249 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m4262304220_gshared (Dictionary_2_t1444694249 * __this, RuntimeObject * p0, bool p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::TryExecuteCommandBuffer<System.Object>(UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<T>)
extern "C"  void PostProcessingBehaviour_TryExecuteCommandBuffer_TisRuntimeObject_m4209704038_gshared (PostProcessingBehaviour_t3229946336 * __this, PostProcessingComponentCommandBuffer_1_t60440757 * ___component0, const RuntimeMethod* method);
// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::TryPrepareUberImageEffect<System.Object>(UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<T>,UnityEngine.Material)
extern "C"  bool PostProcessingBehaviour_TryPrepareUberImageEffect_TisRuntimeObject_m969063268_gshared (PostProcessingBehaviour_t3229946336 * __this, PostProcessingComponentRenderTexture_1_t353423909 * ___component0, Material_t340375123 * ___material1, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::get_Values()
extern "C"  ValueCollection_t3065697579 * Dictionary_2_get_Values_m897995681_gshared (Dictionary_2_t1349653261 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t1918546918  ValueCollection_GetEnumerator_m3831944692_gshared (ValueCollection_t3065697579 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2246977  Enumerator_get_Current_m2779034449_gshared (Enumerator_t1918546918 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m798624497_gshared (KeyValuePair_2_t2246977 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1172331095_gshared (KeyValuePair_2_t2246977 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3836715879_gshared (Enumerator_t1918546918 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m1063065379_gshared (Enumerator_t1918546918 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,System.Object>>::Clear()
extern "C"  void Dictionary_2_Clear_m676003809_gshared (Dictionary_2_t1349653261 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C"  Enumerator_t3398877024  Dictionary_2_GetEnumerator_m2978988825_gshared (Dictionary_2_t1444694249 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  KeyValuePair_2_t3842366416  Enumerator_get_Current_m2190540846_gshared (Enumerator_t3398877024 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3347890282_gshared (KeyValuePair_2_t3842366416 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m3881342223_gshared (KeyValuePair_2_t3842366416 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m481679286_gshared (Enumerator_t3398877024 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m3834169052_gshared (Enumerator_t3398877024 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m351745166_gshared (Dictionary_2_t1444694249 * __this, RuntimeObject * p0, bool p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C"  void HashSet_1__ctor_m4231804131_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
extern "C"  bool HashSet_1_Add_m1971460364_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(!0)
extern "C"  bool HashSet_1_Contains_m3173358704_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(!0)
extern "C"  bool HashSet_1_Remove_m709044238_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Collections.Generic.HashSet`1/Enumerator<!0> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t3350232909  HashSet_1_GetEnumerator_m3346268098_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m4213278602_gshared (Enumerator_t3350232909 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3714175425_gshared (Enumerator_t3350232909 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C"  void HashSet_1_Clear_m507835370_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<System.Object>::.ctor()
extern "C"  void PostProcessingComponentCommandBuffer_1__ctor_m120899638_gshared (PostProcessingComponentCommandBuffer_1_t60440757 * __this, const RuntimeMethod* method);
// T UnityEngine.PostProcessing.PostProcessingComponent`1<System.Object>::get_model()
extern "C"  RuntimeObject * PostProcessingComponent_1_get_model_m1119990137_gshared (PostProcessingComponent_1_t2373282366 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<System.Object>::.ctor()
extern "C"  void PostProcessingComponentRenderTexture_1__ctor_m2869702566_gshared (PostProcessingComponentRenderTexture_1_t353423909 * __this, const RuntimeMethod* method);
// !1 System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>::Invoke(!0)
extern "C"  Matrix4x4_t1817901843  Func_2_Invoke_m886748628_gshared (Func_2_t4093140010 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);

// UnityEngine.PostProcessing.MotionBlurModel/Settings UnityEngine.PostProcessing.MotionBlurModel/Settings::get_defaultSettings()
extern "C"  Settings_t4282162361  Settings_get_defaultSettings_m3507581962 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingModel::.ctor()
extern "C"  void PostProcessingModel__ctor_m4158388095 (PostProcessingModel_t540111976 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>::.ctor()
#define List_1__ctor_m4170144800(__this, method) ((  void (*) (List_1_t4203178569 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>::.ctor()
#define Dictionary_2__ctor_m2678073514(__this, method) ((  void (*) (Dictionary_2_t1572824908 *, const RuntimeMethod*))Dictionary_2__ctor_m3471890303_gshared)(__this, method)
// System.Void UnityEngine.PostProcessing.MaterialFactory::.ctor()
extern "C"  void MaterialFactory__ctor_m231704672 (MaterialFactory_t2445948724 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.RenderTextureFactory::.ctor()
extern "C"  void RenderTextureFactory__ctor_m1345809438 (RenderTextureFactory_t1946967824 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingContext::.ctor()
extern "C"  void PostProcessingContext__ctor_m1285766435 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent::.ctor()
extern "C"  void BuiltinDebugViewsComponent__ctor_m2740954434 (BuiltinDebugViewsComponent_t2123147871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.BuiltinDebugViewsComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisBuiltinDebugViewsComponent_t2123147871_m1960338779(__this, ___component0, method) ((  BuiltinDebugViewsComponent_t2123147871 * (*) (PostProcessingBehaviour_t3229946336 *, BuiltinDebugViewsComponent_t2123147871 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.AmbientOcclusionComponent::.ctor()
extern "C"  void AmbientOcclusionComponent__ctor_m2762306729 (AmbientOcclusionComponent_t4130625043 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.AmbientOcclusionComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisAmbientOcclusionComponent_t4130625043_m2502213982(__this, ___component0, method) ((  AmbientOcclusionComponent_t4130625043 * (*) (PostProcessingBehaviour_t3229946336 *, AmbientOcclusionComponent_t4130625043 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::.ctor()
extern "C"  void ScreenSpaceReflectionComponent__ctor_m2879296341 (ScreenSpaceReflectionComponent_t856094247 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.ScreenSpaceReflectionComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisScreenSpaceReflectionComponent_t856094247_m3898891935(__this, ___component0, method) ((  ScreenSpaceReflectionComponent_t856094247 * (*) (PostProcessingBehaviour_t3229946336 *, ScreenSpaceReflectionComponent_t856094247 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.FogComponent::.ctor()
extern "C"  void FogComponent__ctor_m4028577741 (FogComponent_t3400726830 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.FogComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisFogComponent_t3400726830_m2397394497(__this, ___component0, method) ((  FogComponent_t3400726830 * (*) (PostProcessingBehaviour_t3229946336 *, FogComponent_t3400726830 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.MotionBlurComponent::.ctor()
extern "C"  void MotionBlurComponent__ctor_m2500919077 (MotionBlurComponent_t3686516877 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.MotionBlurComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisMotionBlurComponent_t3686516877_m2944290405(__this, ___component0, method) ((  MotionBlurComponent_t3686516877 * (*) (PostProcessingBehaviour_t3229946336 *, MotionBlurComponent_t3686516877 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.TaaComponent::.ctor()
extern "C"  void TaaComponent__ctor_m675959233 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.TaaComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisTaaComponent_t3791749658_m1885470620(__this, ___component0, method) ((  TaaComponent_t3791749658 * (*) (PostProcessingBehaviour_t3229946336 *, TaaComponent_t3791749658 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.EyeAdaptationComponent::.ctor()
extern "C"  void EyeAdaptationComponent__ctor_m137598109 (EyeAdaptationComponent_t3394805121 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.EyeAdaptationComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisEyeAdaptationComponent_t3394805121_m1462221255(__this, ___component0, method) ((  EyeAdaptationComponent_t3394805121 * (*) (PostProcessingBehaviour_t3229946336 *, EyeAdaptationComponent_t3394805121 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.DepthOfFieldComponent::.ctor()
extern "C"  void DepthOfFieldComponent__ctor_m954390370 (DepthOfFieldComponent_t554756766 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.DepthOfFieldComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisDepthOfFieldComponent_t554756766_m2473682395(__this, ___component0, method) ((  DepthOfFieldComponent_t554756766 * (*) (PostProcessingBehaviour_t3229946336 *, DepthOfFieldComponent_t554756766 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.BloomComponent::.ctor()
extern "C"  void BloomComponent__ctor_m3106370286 (BloomComponent_t3791419130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.BloomComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisBloomComponent_t3791419130_m3367245233(__this, ___component0, method) ((  BloomComponent_t3791419130 * (*) (PostProcessingBehaviour_t3229946336 *, BloomComponent_t3791419130 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.ChromaticAberrationComponent::.ctor()
extern "C"  void ChromaticAberrationComponent__ctor_m3851952747 (ChromaticAberrationComponent_t1647263118 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.ChromaticAberrationComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisChromaticAberrationComponent_t1647263118_m2769190930(__this, ___component0, method) ((  ChromaticAberrationComponent_t1647263118 * (*) (PostProcessingBehaviour_t3229946336 *, ChromaticAberrationComponent_t1647263118 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.ColorGradingComponent::.ctor()
extern "C"  void ColorGradingComponent__ctor_m1390733314 (ColorGradingComponent_t1715259467 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.ColorGradingComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisColorGradingComponent_t1715259467_m47539266(__this, ___component0, method) ((  ColorGradingComponent_t1715259467 * (*) (PostProcessingBehaviour_t3229946336 *, ColorGradingComponent_t1715259467 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.UserLutComponent::.ctor()
extern "C"  void UserLutComponent__ctor_m3557887131 (UserLutComponent_t2843161776 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.UserLutComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisUserLutComponent_t2843161776_m3715666184(__this, ___component0, method) ((  UserLutComponent_t2843161776 * (*) (PostProcessingBehaviour_t3229946336 *, UserLutComponent_t2843161776 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.GrainComponent::.ctor()
extern "C"  void GrainComponent__ctor_m497184467 (GrainComponent_t866324317 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.GrainComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisGrainComponent_t866324317_m3375556804(__this, ___component0, method) ((  GrainComponent_t866324317 * (*) (PostProcessingBehaviour_t3229946336 *, GrainComponent_t866324317 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.VignetteComponent::.ctor()
extern "C"  void VignetteComponent__ctor_m3640704423 (VignetteComponent_t3243642943 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.VignetteComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisVignetteComponent_t3243642943_m2941218025(__this, ___component0, method) ((  VignetteComponent_t3243642943 * (*) (PostProcessingBehaviour_t3229946336 *, VignetteComponent_t3243642943 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.DitheringComponent::.ctor()
extern "C"  void DitheringComponent__ctor_m11042692 (DitheringComponent_t277621267 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.DitheringComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisDitheringComponent_t277621267_m1758707692(__this, ___component0, method) ((  DitheringComponent_t277621267 * (*) (PostProcessingBehaviour_t3229946336 *, DitheringComponent_t277621267 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.FxaaComponent::.ctor()
extern "C"  void FxaaComponent__ctor_m3929535888 (FxaaComponent_t1312385771 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent<UnityEngine.PostProcessing.FxaaComponent>(T)
#define PostProcessingBehaviour_AddComponent_TisFxaaComponent_t1312385771_m1843850521(__this, ___component0, method) ((  FxaaComponent_t1312385771 * (*) (PostProcessingBehaviour_t3229946336 *, FxaaComponent_t1312385771 *, const RuntimeMethod*))PostProcessingBehaviour_AddComponent_TisRuntimeObject_m4220113493_gshared)(__this, ___component0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::.ctor()
#define Dictionary_2__ctor_m896571293(__this, method) ((  void (*) (Dictionary_2_t3095696878 *, const RuntimeMethod*))Dictionary_2__ctor_m236774955_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>::GetEnumerator()
#define List_1_GetEnumerator_m659292003(__this, method) ((  Enumerator_t1797455150  (*) (List_1_t4203178569 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.PostProcessing.PostProcessingComponentBase>::get_Current()
#define Enumerator_get_Current_m747737324(__this, method) ((  PostProcessingComponentBase_t2731103827 * (*) (Enumerator_t1797455150 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::Add(!0,!1)
#define Dictionary_2_Add_m1451613760(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3095696878 *, PostProcessingComponentBase_t2731103827 *, bool, const RuntimeMethod*))Dictionary_2_Add_m4262304220_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.PostProcessing.PostProcessingComponentBase>::MoveNext()
#define Enumerator_MoveNext_m425563657(__this, method) ((  bool (*) (Enumerator_t1797455150 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.PostProcessing.PostProcessingComponentBase>::Dispose()
#define Enumerator_Dispose_m2340450683(__this, method) ((  void (*) (Enumerator_t1797455150 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m3492031340 (MonoBehaviour_t3962482529 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, method) ((  Camera_t4157153871 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingContext::Reset()
extern "C"  PostProcessingContext_t2014408948 * PostProcessingContext_Reset_m4133896387 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::DisableComponents()
extern "C"  void PostProcessingBehaviour_DisableComponents_m1054770722 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::CheckObservers()
extern "C"  void PostProcessingBehaviour_CheckObservers_m3285018848 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern "C"  void Camera_set_depthTextureMode_m754977860 (Camera_t4157153871 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PostProcessing.BuiltinDebugViewsModel::get_willInterrupt()
extern "C"  bool BuiltinDebugViewsModel_get_willInterrupt_m2596798096 (BuiltinDebugViewsModel_t1462618840 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.TaaComponent::SetProjectionMatrix(System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>)
extern "C"  void TaaComponent_SetProjectionMatrix_m2316589171 (TaaComponent_t3791749658 * __this, Func_2_t4093140010 * ___jitteredFunc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::TryExecuteCommandBuffer<UnityEngine.PostProcessing.BuiltinDebugViewsModel>(UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<T>)
#define PostProcessingBehaviour_TryExecuteCommandBuffer_TisBuiltinDebugViewsModel_t1462618840_m3493222038(__this, ___component0, method) ((  void (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentCommandBuffer_1_t2737920729 *, const RuntimeMethod*))PostProcessingBehaviour_TryExecuteCommandBuffer_TisRuntimeObject_m4209704038_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::TryExecuteCommandBuffer<UnityEngine.PostProcessing.AmbientOcclusionModel>(UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<T>)
#define PostProcessingBehaviour_TryExecuteCommandBuffer_TisAmbientOcclusionModel_t389471066_m619521879(__this, ___component0, method) ((  void (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentCommandBuffer_1_t1664772955 *, const RuntimeMethod*))PostProcessingBehaviour_TryExecuteCommandBuffer_TisRuntimeObject_m4209704038_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::TryExecuteCommandBuffer<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>(UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<T>)
#define PostProcessingBehaviour_TryExecuteCommandBuffer_TisScreenSpaceReflectionModel_t3026344732_m311471412(__this, ___component0, method) ((  void (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentCommandBuffer_1_t6679325 *, const RuntimeMethod*))PostProcessingBehaviour_TryExecuteCommandBuffer_TisRuntimeObject_m4209704038_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::TryExecuteCommandBuffer<UnityEngine.PostProcessing.FogModel>(UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<T>)
#define PostProcessingBehaviour_TryExecuteCommandBuffer_TisFogModel_t3620688749_m3894248309(__this, ___component0, method) ((  void (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentCommandBuffer_1_t601023342 *, const RuntimeMethod*))PostProcessingBehaviour_TryExecuteCommandBuffer_TisRuntimeObject_m4209704038_gshared)(__this, ___component0, method)
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::TryExecuteCommandBuffer<UnityEngine.PostProcessing.MotionBlurModel>(UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<T>)
#define PostProcessingBehaviour_TryExecuteCommandBuffer_TisMotionBlurModel_t3080286123_m3591009761(__this, ___component0, method) ((  void (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentCommandBuffer_1_t60620716 *, const RuntimeMethod*))PostProcessingBehaviour_TryExecuteCommandBuffer_TisRuntimeObject_m4209704038_gshared)(__this, ___component0, method)
// System.Void UnityEngine.Camera::ResetProjectionMatrix()
extern "C"  void Camera_ResetProjectionMatrix_m1910759531 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern "C"  void Graphics_Blit_m1336850842 (RuntimeObject * __this /* static, unused */, Texture_t3661962703 * p0, RenderTexture_t2108887433 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.PostProcessing.MaterialFactory::Get(System.String)
extern "C"  Material_t340375123 * MaterialFactory_Get_m4113232693 (MaterialFactory_t2445948724 * __this, String_t* ___shaderName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_shaderKeywords(System.String[])
extern "C"  void Material_set_shaderKeywords_m4017377042 (Material_t340375123 * __this, StringU5BU5D_t1281789340* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.PostProcessing.RenderTextureFactory::Get(UnityEngine.RenderTexture)
extern "C"  RenderTexture_t2108887433 * RenderTextureFactory_Get_m169036867 (RenderTextureFactory_t1946967824 * __this, RenderTexture_t2108887433 * ___baseRenderTexture0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.TaaComponent::Render(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void TaaComponent_Render_m2638556758 (TaaComponent_t3791749658 * __this, RenderTexture_t2108887433 * ___source0, RenderTexture_t2108887433 * ___destination1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.PostProcessing.GraphicsUtils::get_whiteTexture()
extern "C"  Texture2D_t3840446185 * GraphicsUtils_get_whiteTexture_m2270091967 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.PostProcessing.EyeAdaptationComponent::Prepare(UnityEngine.RenderTexture,UnityEngine.Material)
extern "C"  Texture_t3661962703 * EyeAdaptationComponent_Prepare_m3960566531 (EyeAdaptationComponent_t3394805121 * __this, RenderTexture_t2108887433 * ___source0, Material_t340375123 * ___uberMaterial1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1829349465 (Material_t340375123 * __this, String_t* p0, Texture_t3661962703 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.DepthOfFieldComponent::Prepare(UnityEngine.RenderTexture,UnityEngine.Material,System.Boolean)
extern "C"  void DepthOfFieldComponent_Prepare_m1717418444 (DepthOfFieldComponent_t554756766 * __this, RenderTexture_t2108887433 * ___source0, Material_t340375123 * ___uberMaterial1, bool ___antialiasCoC2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.BloomComponent::Prepare(UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture)
extern "C"  void BloomComponent_Prepare_m571914554 (BloomComponent_t3791419130 * __this, RenderTexture_t2108887433 * ___source0, Material_t340375123 * ___uberMaterial1, Texture_t3661962703 * ___autoExposure2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::TryPrepareUberImageEffect<UnityEngine.PostProcessing.ChromaticAberrationModel>(UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<T>,UnityEngine.Material)
#define PostProcessingBehaviour_TryPrepareUberImageEffect_TisChromaticAberrationModel_t3963399853_m1586502784(__this, ___component0, ___material1, method) ((  bool (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentRenderTexture_1_t1236717598 *, Material_t340375123 *, const RuntimeMethod*))PostProcessingBehaviour_TryPrepareUberImageEffect_TisRuntimeObject_m969063268_gshared)(__this, ___component0, ___material1, method)
// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::TryPrepareUberImageEffect<UnityEngine.PostProcessing.ColorGradingModel>(UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<T>,UnityEngine.Material)
#define PostProcessingBehaviour_TryPrepareUberImageEffect_TisColorGradingModel_t1448048181_m2160422038(__this, ___component0, ___material1, method) ((  bool (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentRenderTexture_1_t3016333222 *, Material_t340375123 *, const RuntimeMethod*))PostProcessingBehaviour_TryPrepareUberImageEffect_TisRuntimeObject_m969063268_gshared)(__this, ___component0, ___material1, method)
// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::TryPrepareUberImageEffect<UnityEngine.PostProcessing.VignetteModel>(UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<T>,UnityEngine.Material)
#define PostProcessingBehaviour_TryPrepareUberImageEffect_TisVignetteModel_t2845517177_m1716742003(__this, ___component0, ___material1, method) ((  bool (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentRenderTexture_1_t118834922 *, Material_t340375123 *, const RuntimeMethod*))PostProcessingBehaviour_TryPrepareUberImageEffect_TisRuntimeObject_m969063268_gshared)(__this, ___component0, ___material1, method)
// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::TryPrepareUberImageEffect<UnityEngine.PostProcessing.UserLutModel>(UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<T>,UnityEngine.Material)
#define PostProcessingBehaviour_TryPrepareUberImageEffect_TisUserLutModel_t1670108080_m2338498891(__this, ___component0, ___material1, method) ((  bool (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentRenderTexture_1_t3238393121 *, Material_t340375123 *, const RuntimeMethod*))PostProcessingBehaviour_TryPrepareUberImageEffect_TisRuntimeObject_m969063268_gshared)(__this, ___component0, ___material1, method)
// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::TryPrepareUberImageEffect<UnityEngine.PostProcessing.GrainModel>(UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<T>,UnityEngine.Material)
#define PostProcessingBehaviour_TryPrepareUberImageEffect_TisGrainModel_t1152882488_m3807764775(__this, ___component0, ___material1, method) ((  bool (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentRenderTexture_1_t2721167529 *, Material_t340375123 *, const RuntimeMethod*))PostProcessingBehaviour_TryPrepareUberImageEffect_TisRuntimeObject_m969063268_gshared)(__this, ___component0, ___material1, method)
// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::TryPrepareUberImageEffect<UnityEngine.PostProcessing.DitheringModel>(UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<T>,UnityEngine.Material)
#define PostProcessingBehaviour_TryPrepareUberImageEffect_TisDitheringModel_t2429005396_m570759740(__this, ___component0, ___material1, method) ((  bool (*) (PostProcessingBehaviour_t3229946336 *, PostProcessingComponentRenderTexture_1_t3997290437 *, Material_t340375123 *, const RuntimeMethod*))PostProcessingBehaviour_TryPrepareUberImageEffect_TisRuntimeObject_m969063268_gshared)(__this, ___component0, ___material1, method)
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern "C"  void Graphics_Blit_m4126770912 (RuntimeObject * __this /* static, unused */, Texture_t3661962703 * p0, RenderTexture_t2108887433 * p1, Material_t340375123 * p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.FxaaComponent::Render(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void FxaaComponent_Render_m1006683150 (FxaaComponent_t1312385771 * __this, RenderTexture_t2108887433 * ___source0, RenderTexture_t2108887433 * ___destination1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PostProcessing.GraphicsUtils::get_isLinearColorSpace()
extern "C"  bool GraphicsUtils_get_isLinearColorSpace_m2783177889 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::EnableKeyword(System.String)
extern "C"  void Material_EnableKeyword_m329692301 (Material_t340375123 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.RenderTextureFactory::ReleaseAll()
extern "C"  void RenderTextureFactory_ReleaseAll_m3329667721 (RenderTextureFactory_t1946967824 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Event UnityEngine.Event::get_current()
extern "C"  Event_t2956885303 * Event_get_current_m2393892120 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C"  int32_t Event_get_type_m1370041809 (Event_t2956885303 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PostProcessing.BuiltinDebugViewsModel::IsModeActive(UnityEngine.PostProcessing.BuiltinDebugViewsModel/Mode)
extern "C"  bool BuiltinDebugViewsModel_IsModeActive_m1972979357 (BuiltinDebugViewsModel_t1462618840 * __this, int32_t ___mode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.EyeAdaptationComponent::OnGUI()
extern "C"  void EyeAdaptationComponent_OnGUI_m1617202736 (EyeAdaptationComponent_t3394805121 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.ColorGradingComponent::OnGUI()
extern "C"  void ColorGradingComponent_OnGUI_m4238095531 (ColorGradingComponent_t1715259467 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.UserLutComponent::OnGUI()
extern "C"  void UserLutComponent_OnGUI_m2614562252 (UserLutComponent_t2843161776 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>::get_Values()
#define Dictionary_2_get_Values_m907585786(__this, method) ((  ValueCollection_t3288869226 * (*) (Dictionary_2_t1572824908 *, const RuntimeMethod*))Dictionary_2_get_Values_m897995681_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m141340968(__this, method) ((  Enumerator_t2141718565  (*) (ValueCollection_t3288869226 *, const RuntimeMethod*))ValueCollection_GetEnumerator_m3831944692_gshared)(__this, method)
// !1 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>::get_Current()
#define Enumerator_get_Current_m1441963613(__this, method) ((  KeyValuePair_2_t3423445140  (*) (Enumerator_t2141718565 *, const RuntimeMethod*))Enumerator_get_Current_m2779034449_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>::get_Key()
#define KeyValuePair_2_get_Key_m648688154(__this, method) ((  int32_t (*) (KeyValuePair_2_t3423445140 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m798624497_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>::get_Value()
#define KeyValuePair_2_get_Value_m1482434896(__this, method) ((  CommandBuffer_t2206337031 * (*) (KeyValuePair_2_t3423445140 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1172331095_gshared)(__this, method)
// System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_RemoveCommandBuffer_m773243127 (Camera_t4157153871 * __this, int32_t p0, CommandBuffer_t2206337031 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose()
extern "C"  void CommandBuffer_Dispose_m146760806 (CommandBuffer_t2206337031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>::MoveNext()
#define Enumerator_MoveNext_m1047154078(__this, method) ((  bool (*) (Enumerator_t2141718565 *, const RuntimeMethod*))Enumerator_MoveNext_m3836715879_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>::Dispose()
#define Enumerator_Dispose_m3868019798(__this, method) ((  void (*) (Enumerator_t2141718565 *, const RuntimeMethod*))Enumerator_Dispose_m1063065379_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>::Clear()
#define Dictionary_2_Clear_m3961351021(__this, method) ((  void (*) (Dictionary_2_t1572824908 *, const RuntimeMethod*))Dictionary_2_Clear_m676003809_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>::Clear()
#define List_1_Clear_m1773456631(__this, method) ((  void (*) (List_1_t4203178569 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void UnityEngine.PostProcessing.MaterialFactory::Dispose()
extern "C"  void MaterialFactory_Dispose_m620032304 (MaterialFactory_t2445948724 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.RenderTextureFactory::Dispose()
extern "C"  void RenderTextureFactory_Dispose_m2714960054 (RenderTextureFactory_t1946967824 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.GraphicsUtils::Dispose()
extern "C"  void GraphicsUtils_Dispose_m791740987 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.TaaComponent::ResetHistory()
extern "C"  void TaaComponent_ResetHistory_m3846253241 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.MotionBlurComponent::ResetHistory()
extern "C"  void MotionBlurComponent_ResetHistory_m1016490533 (MotionBlurComponent_t3686516877 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.EyeAdaptationComponent::ResetHistory()
extern "C"  void EyeAdaptationComponent_ResetHistory_m3423621609 (EyeAdaptationComponent_t3394805121 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2066271542(__this, method) ((  Enumerator_t754912357  (*) (Dictionary_2_t3095696878 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m2978988825_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::get_Current()
#define Enumerator_get_Current_m3820558532(__this, method) ((  KeyValuePair_2_t1198401749  (*) (Enumerator_t754912357 *, const RuntimeMethod*))Enumerator_get_Current_m2190540846_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m1176910999(__this, method) ((  PostProcessingComponentBase_t2731103827 * (*) (KeyValuePair_2_t1198401749 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3347890282_gshared)(__this, method)
// System.Boolean UnityEngine.PostProcessing.PostProcessingModel::get_enabled()
extern "C"  bool PostProcessingModel_get_enabled_m2892084724 (PostProcessingModel_t540111976 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !1 System.Collections.Generic.KeyValuePair`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m3146329666(__this, method) ((  bool (*) (KeyValuePair_2_t1198401749 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3881342223_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>::Add(!0)
#define List_1_Add_m1572460420(__this, p0, method) ((  void (*) (List_1_t4203178569 *, PostProcessingComponentBase_t2731103827 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m3116365360(__this, method) ((  bool (*) (Enumerator_t754912357 *, const RuntimeMethod*))Enumerator_MoveNext_m481679286_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::Dispose()
#define Enumerator_Dispose_m2110019143(__this, method) ((  void (*) (Enumerator_t754912357 *, const RuntimeMethod*))Enumerator_Dispose_m3834169052_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>::get_Item(System.Int32)
#define List_1_get_Item_m1448434680(__this, p0, method) ((  PostProcessingComponentBase_t2731103827 * (*) (List_1_t4203178569 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m1186219162(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3095696878 *, PostProcessingComponentBase_t2731103827 *, bool, const RuntimeMethod*))Dictionary_2_set_Item_m351745166_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>::get_Count()
#define List_1_get_Count_m728013831(__this, method) ((  int32_t (*) (List_1_t4203178569 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingContext::set_interrupted(System.Boolean)
extern "C"  void PostProcessingContext_set_interrupted_m4192460866 (PostProcessingContext_t2014408948 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderingPath UnityEngine.Camera::get_actualRenderingPath()
extern "C"  int32_t Camera_get_actualRenderingPath_m423069678 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Camera::get_allowHDR()
extern "C"  bool Camera_get_allowHDR_m2615180899 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C"  int32_t Camera_get_pixelWidth_m1110053668 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C"  int32_t Camera_get_pixelHeight_m722276884 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Camera::get_rect()
extern "C"  Rect_t2360479859  Camera_get_rect_m2458154151 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.BuiltinDebugViewsModel::.ctor()
extern "C"  void BuiltinDebugViewsModel__ctor_m2849271374 (BuiltinDebugViewsModel_t1462618840 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.FogModel::.ctor()
extern "C"  void FogModel__ctor_m849674861 (FogModel_t3620688749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.AntialiasingModel::.ctor()
extern "C"  void AntialiasingModel__ctor_m407834341 (AntialiasingModel_t1521139388 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.AmbientOcclusionModel::.ctor()
extern "C"  void AmbientOcclusionModel__ctor_m1631144053 (AmbientOcclusionModel_t389471066 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionModel::.ctor()
extern "C"  void ScreenSpaceReflectionModel__ctor_m855607741 (ScreenSpaceReflectionModel_t3026344732 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.DepthOfFieldModel::.ctor()
extern "C"  void DepthOfFieldModel__ctor_m2227976965 (DepthOfFieldModel_t514067330 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.MotionBlurModel::.ctor()
extern "C"  void MotionBlurModel__ctor_m428522222 (MotionBlurModel_t3080286123 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.EyeAdaptationModel::.ctor()
extern "C"  void EyeAdaptationModel__ctor_m1238644491 (EyeAdaptationModel_t242823912 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.BloomModel::.ctor()
extern "C"  void BloomModel__ctor_m2312170588 (BloomModel_t2099727860 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.ColorGradingModel::.ctor()
extern "C"  void ColorGradingModel__ctor_m845539817 (ColorGradingModel_t1448048181 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.UserLutModel::.ctor()
extern "C"  void UserLutModel__ctor_m1936047466 (UserLutModel_t1670108080 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.ChromaticAberrationModel::.ctor()
extern "C"  void ChromaticAberrationModel__ctor_m2337246990 (ChromaticAberrationModel_t3963399853 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.GrainModel::.ctor()
extern "C"  void GrainModel__ctor_m542869878 (GrainModel_t1152882488 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.VignetteModel::.ctor()
extern "C"  void VignetteModel__ctor_m512852279 (VignetteModel_t2845517177 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.DitheringModel::.ctor()
extern "C"  void DitheringModel__ctor_m4012316787 (DitheringModel_t2429005396 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>::.ctor()
#define HashSet_1__ctor_m1993572700(__this, method) ((  void (*) (HashSet_1_t673836907 *, const RuntimeMethod*))HashSet_1__ctor_m4231804131_gshared)(__this, method)
// System.Int32 UnityEngine.RenderTexture::get_depth()
extern "C"  int32_t RenderTexture_get_depth_m3825994142 (RenderTexture_t2108887433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
extern "C"  int32_t RenderTexture_get_format_m3846871418 (RenderTexture_t2108887433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RenderTexture::get_sRGB()
extern "C"  bool RenderTexture_get_sRGB_m300498885 (RenderTexture_t2108887433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.FilterMode UnityEngine.Texture::get_filterMode()
extern "C"  int32_t Texture_get_filterMode_m3474837873 (Texture_t3661962703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
extern "C"  int32_t Texture_get_wrapMode_m2187367613 (Texture_t3661962703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.PostProcessing.RenderTextureFactory::Get(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,UnityEngine.FilterMode,UnityEngine.TextureWrapMode,System.String)
extern "C"  RenderTexture_t2108887433 * RenderTextureFactory_Get_m1772850884 (RenderTextureFactory_t1946967824 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___rw4, int32_t ___filterMode5, int32_t ___wrapMode6, String_t* ___name7, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  RenderTexture_t2108887433 * RenderTexture_GetTemporary_m3378328322 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m3078068058 (Texture_t3661962703 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m587872754 (Texture_t3661962703 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m291480324 (Object_t631007953 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>::Add(!0)
#define HashSet_1_Add_m2910627594(__this, p0, method) ((  bool (*) (HashSet_1_t673836907 *, RenderTexture_t2108887433 *, const RuntimeMethod*))HashSet_1_Add_m1971460364_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>::Contains(!0)
#define HashSet_1_Contains_m3753409409(__this, p0, method) ((  bool (*) (HashSet_1_t673836907 *, RenderTexture_t2108887433 *, const RuntimeMethod*))HashSet_1_Contains_m3173358704_gshared)(__this, p0, method)
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>::Remove(!0)
#define HashSet_1_Remove_m467255894(__this, p0, method) ((  bool (*) (HashSet_1_t673836907 *, RenderTexture_t2108887433 *, const RuntimeMethod*))HashSet_1_Remove_m709044238_gshared)(__this, p0, method)
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_ReleaseTemporary_m2400081536 (RuntimeObject * __this /* static, unused */, RenderTexture_t2108887433 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.HashSet`1/Enumerator<!0> System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>::GetEnumerator()
#define HashSet_1_GetEnumerator_m4157320476(__this, method) ((  Enumerator_t2379014178  (*) (HashSet_1_t673836907 *, const RuntimeMethod*))HashSet_1_GetEnumerator_m3346268098_gshared)(__this, method)
// !0 System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.RenderTexture>::get_Current()
#define Enumerator_get_Current_m3525666044(__this, method) ((  RenderTexture_t2108887433 * (*) (Enumerator_t2379014178 *, const RuntimeMethod*))Enumerator_get_Current_m4213278602_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.RenderTexture>::MoveNext()
#define Enumerator_MoveNext_m519760291(__this, method) ((  bool (*) (Enumerator_t2379014178 *, const RuntimeMethod*))Enumerator_MoveNext_m3714175425_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>::Clear()
#define HashSet_1_Clear_m552370813(__this, method) ((  void (*) (HashSet_1_t673836907 *, const RuntimeMethod*))HashSet_1_Clear_m507835370_gshared)(__this, method)
// System.Void UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>::.ctor()
#define PostProcessingComponentCommandBuffer_1__ctor_m1811253078(__this, method) ((  void (*) (PostProcessingComponentCommandBuffer_1_t6679325 *, const RuntimeMethod*))PostProcessingComponentCommandBuffer_1__ctor_m120899638_gshared)(__this, method)
// T UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>::get_model()
#define PostProcessingComponent_1_get_model_m3701723778(__this, method) ((  ScreenSpaceReflectionModel_t3026344732 * (*) (PostProcessingComponent_1_t2319520934 *, const RuntimeMethod*))PostProcessingComponent_1_get_model_m1119990137_gshared)(__this, method)
// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_isGBufferAvailable()
extern "C"  bool PostProcessingContext_get_isGBufferAvailable_m949646721 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_interrupted()
extern "C"  bool PostProcessingContext_get_interrupted_m1809095682 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m1030499873 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel::get_settings()
extern "C"  Settings_t1995791524  ScreenSpaceReflectionModel_get_settings_m3165114047 (ScreenSpaceReflectionModel_t3026344732 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PostProcessing.PostProcessingContext::get_width()
extern "C"  int32_t PostProcessingContext_get_width_m2658937703 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PostProcessing.PostProcessingContext::get_height()
extern "C"  int32_t PostProcessingContext_get_height_m4218042885 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern "C"  void Material_SetInt_m475299667 (Material_t340375123 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m1018585504 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern "C"  void Material_SetFloat_m1688718093 (Material_t340375123 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_projectionMatrix_m667780853 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m567451091 (Matrix4x4_t1817901843 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m538536689 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::IsPositiveInfinity(System.Single)
extern "C"  bool Single_IsPositiveInfinity_m1411272350 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m837839537 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector4_t3319028937  Vector4_op_Implicit_m237151757 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetVector(System.Int32,UnityEngine.Vector4)
extern "C"  void Material_SetVector_m2633010038 (Material_t340375123 * __this, int32_t p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t3319028937  Vector4_op_Implicit_m2966035112 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::SetRow(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetRow_m2327530647 (Matrix4x4_t1817901843 * __this, int32_t p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  Matrix4x4_t1817901843  Matrix4x4_op_Multiply_m1876492807 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m751249077 (Material_t340375123 * __this, int32_t p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_worldToCameraMatrix_m22661425 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C"  Matrix4x4_t1817901843  Matrix4x4_get_inverse_m1870592360 (Matrix4x4_t1817901843 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_isHdr()
extern "C"  bool PostProcessingContext_get_isHdr_m3057655858 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern "C"  void CommandBuffer_GetTemporaryRT_m2948653747 (CommandBuffer_t2206337031 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat)
extern "C"  void CommandBuffer_GetTemporaryRT_m2252457381 (CommandBuffer_t2206337031 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  RenderTargetIdentifier_t2079184500  RenderTargetIdentifier_op_Implicit_m2644497587 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(System.Int32)
extern "C"  RenderTargetIdentifier_t2079184500  RenderTargetIdentifier_op_Implicit_m1310414951 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material,System.Int32)
extern "C"  void CommandBuffer_Blit_m1867893672 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500  p0, RenderTargetIdentifier_t2079184500  p1, Material_t340375123 * p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalVector(System.Int32,UnityEngine.Vector4)
extern "C"  void CommandBuffer_SetGlobalVector_m2474181847 (CommandBuffer_t2206337031 * __this, int32_t p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalFloat(System.Int32,System.Single)
extern "C"  void CommandBuffer_SetGlobalFloat_m4256468291 (CommandBuffer_t2206337031 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseTemporaryRT(System.Int32)
extern "C"  void CommandBuffer_ReleaseTemporaryRT_m2627662573 (CommandBuffer_t2206337031 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier)
extern "C"  void CommandBuffer_Blit_m1393847922 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500  p0, RenderTargetIdentifier_t2079184500  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::get_defaultSettings()
extern "C"  Settings_t1995791524  Settings_get_defaultSettings_m3330699527 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.AntialiasingModel>::.ctor()
#define PostProcessingComponentRenderTexture_1__ctor_m1485528837(__this, method) ((  void (*) (PostProcessingComponentRenderTexture_1_t3089424429 *, const RuntimeMethod*))PostProcessingComponentRenderTexture_1__ctor_m2869702566_gshared)(__this, method)
// T UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AntialiasingModel>::get_model()
#define PostProcessingComponent_1_get_model_m3744888901(__this, method) ((  AntialiasingModel_t1521139388 * (*) (PostProcessingComponent_1_t814315590 *, const RuntimeMethod*))PostProcessingComponent_1_get_model_m1119990137_gshared)(__this, method)
// UnityEngine.PostProcessing.AntialiasingModel/Settings UnityEngine.PostProcessing.AntialiasingModel::get_settings()
extern "C"  Settings_t4292431647  AntialiasingModel_get_settings_m675444796 (AntialiasingModel_t1521139388 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
extern "C"  bool SystemInfo_SupportsRenderTextureFormat_m1663449629 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsMotionVectors()
extern "C"  bool SystemInfo_get_supportsMotionVectors_m46965105 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.PostProcessing.TaaComponent::GenerateRandomOffset()
extern "C"  Vector2_t2156229523  TaaComponent_GenerateRandomOffset_m1416894136 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_nonJitteredProjectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_nonJitteredProjectionMatrix_m3492270478 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !1 System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>::Invoke(!0)
#define Func_2_Invoke_m886748628(__this, p0, method) ((  Matrix4x4_t1817901843  (*) (Func_2_t4093140010 *, Vector2_t2156229523 , const RuntimeMethod*))Func_2_Invoke_m886748628_gshared)(__this, p0, method)
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m3293177686 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Camera::get_orthographic()
extern "C"  bool Camera_get_orthographic_m2831464531 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.PostProcessing.TaaComponent::GetOrthographicProjectionMatrix(UnityEngine.Vector2)
extern "C"  Matrix4x4_t1817901843  TaaComponent_GetOrthographicProjectionMatrix_m3494165154 (TaaComponent_t3791749658 * __this, Vector2_t2156229523  ___offset0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.PostProcessing.TaaComponent::GetPerspectiveProjectionMatrix(UnityEngine.Vector2)
extern "C"  Matrix4x4_t1817901843  TaaComponent_GetPerspectiveProjectionMatrix_m2335334281 (TaaComponent_t3791749658 * __this, Vector2_t2156229523  ___offset0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_useJitteredProjectionMatrixForTransparentRendering(System.Boolean)
extern "C"  void Camera_set_useJitteredProjectionMatrixForTransparentRendering_m1059913304 (Camera_t4157153871 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m3009528825 (Material_t340375123 * __this, int32_t p0, Texture_t3661962703 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_colorBuffer()
extern "C"  RenderBuffer_t586150500  RenderTexture_get_colorBuffer_m2062927451 (RenderTexture_t2108887433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_depthBuffer()
extern "C"  RenderBuffer_t586150500  RenderTexture_get_depthBuffer_m1409937006 (RenderTexture_t2108887433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer)
extern "C"  void Graphics_SetRenderTarget_m191444692 (RuntimeObject * __this /* static, unused */, RenderBufferU5BU5D_t1615831949* p0, RenderBuffer_t586150500  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.GraphicsUtils::Blit(UnityEngine.Material,System.Int32)
extern "C"  void GraphicsUtils_Blit_m1958513870 (RuntimeObject * __this /* static, unused */, Material_t340375123 * ___material0, int32_t ___pass1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.PostProcessing.TaaComponent::GetHaltonValue(System.Int32,System.Int32)
extern "C"  float TaaComponent_GetHaltonValue_m3411042843 (TaaComponent_t3791749658 * __this, int32_t ___index0, int32_t ___radix1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_aspect()
extern "C"  float Camera_get_aspect_m862507514 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m4102745984 (Matrix4x4_t1817901843 * __this, int32_t p0, int32_t p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m3903216845 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  Matrix4x4_t1817901843  Matrix4x4_Ortho_m1994183957 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, float p3, float p4, float p5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m1017741868 (PropertyAttribute_t3677895545 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.UserLutModel>::.ctor()
#define PostProcessingComponentRenderTexture_1__ctor_m3643001101(__this, method) ((  void (*) (PostProcessingComponentRenderTexture_1_t3238393121 *, const RuntimeMethod*))PostProcessingComponentRenderTexture_1__ctor_m2869702566_gshared)(__this, method)
// T UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.UserLutModel>::get_model()
#define PostProcessingComponent_1_get_model_m2956985483(__this, method) ((  UserLutModel_t1670108080 * (*) (PostProcessingComponent_1_t963284282 *, const RuntimeMethod*))PostProcessingComponent_1_get_model_m1119990137_gshared)(__this, method)
// UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel::get_settings()
extern "C"  Settings_t3006579223  UserLutModel_get_settings_m1799133964 (UserLutModel_t1670108080 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.PostProcessing.PostProcessingContext::get_viewport()
extern "C"  Rect_t2360479859  PostProcessingContext_get_viewport_m2647794360 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m3839990490 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
extern "C"  void GUI_DrawTexture_m3124770796 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, Texture_t3661962703 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel/Settings::get_defaultSettings()
extern "C"  Settings_t3006579223  Settings_get_defaultSettings_m1988709150 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.VignetteModel>::.ctor()
#define PostProcessingComponentRenderTexture_1__ctor_m2651217025(__this, method) ((  void (*) (PostProcessingComponentRenderTexture_1_t118834922 *, const RuntimeMethod*))PostProcessingComponentRenderTexture_1__ctor_m2869702566_gshared)(__this, method)
// T UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.VignetteModel>::get_model()
#define PostProcessingComponent_1_get_model_m2875105431(__this, method) ((  VignetteModel_t2845517177 * (*) (PostProcessingComponent_1_t2138693379 *, const RuntimeMethod*))PostProcessingComponent_1_get_model_m1119990137_gshared)(__this, method)
// UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel::get_settings()
extern "C"  Settings_t1354494600  VignetteModel_get_settings_m3633002454 (VignetteModel_t2845517177 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColor_m355160541 (Material_t340375123 * __this, int32_t p0, Color_t2555686324  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel/Settings::get_defaultSettings()
extern "C"  Settings_t1354494600  Settings_get_defaultSettings_m3086602550 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.MotionBlurModel::.ctor()
extern "C"  void MotionBlurModel__ctor_m428522222 (MotionBlurModel_t3080286123 * __this, const RuntimeMethod* method)
{
	{
		// Settings m_Settings = Settings.defaultSettings;
		Settings_t4282162361  L_0 = Settings_get_defaultSettings_m3507581962(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Settings_1(L_0);
		PostProcessingModel__ctor_m4158388095(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.PostProcessing.MotionBlurModel/Settings UnityEngine.PostProcessing.MotionBlurModel::get_settings()
extern "C"  Settings_t4282162361  MotionBlurModel_get_settings_m2229150726 (MotionBlurModel_t3080286123 * __this, const RuntimeMethod* method)
{
	Settings_t4282162361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// get { return m_Settings; }
		Settings_t4282162361  L_0 = __this->get_m_Settings_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_Settings; }
		Settings_t4282162361  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PostProcessing.MotionBlurModel::set_settings(UnityEngine.PostProcessing.MotionBlurModel/Settings)
extern "C"  void MotionBlurModel_set_settings_m4142385848 (MotionBlurModel_t3080286123 * __this, Settings_t4282162361  ___value0, const RuntimeMethod* method)
{
	{
		// set { m_Settings = value; }
		Settings_t4282162361  L_0 = ___value0;
		__this->set_m_Settings_1(L_0);
		// set { m_Settings = value; }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.MotionBlurModel::Reset()
extern "C"  void MotionBlurModel_Reset_m3507530397 (MotionBlurModel_t3080286123 * __this, const RuntimeMethod* method)
{
	{
		// m_Settings = Settings.defaultSettings;
		// m_Settings = Settings.defaultSettings;
		Settings_t4282162361  L_0 = Settings_get_defaultSettings_m3507581962(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Settings_1(L_0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.PostProcessing.MotionBlurModel/Settings UnityEngine.PostProcessing.MotionBlurModel/Settings::get_defaultSettings()
extern "C"  Settings_t4282162361  Settings_get_defaultSettings_m3507581962 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Settings_t4282162361  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Settings_t4282162361  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// return new Settings
		il2cpp_codegen_initobj((&V_0), sizeof(Settings_t4282162361 ));
		// shutterAngle = 270f,
		(&V_0)->set_shutterAngle_0((270.0f));
		// sampleCount = 10,
		(&V_0)->set_sampleCount_1(((int32_t)10));
		// frameBlending = 0f
		(&V_0)->set_frameBlending_2((0.0f));
		Settings_t4282162361  L_0 = V_0;
		V_1 = L_0;
		goto IL_0031;
	}

IL_0031:
	{
		// }
		Settings_t4282162361  L_1 = V_1;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::.ctor()
extern "C"  void PostProcessingBehaviour__ctor_m3910102566 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour__ctor_m3910102566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bool m_RenderingInSceneView = false;
		__this->set_m_RenderingInSceneView_12((bool)0);
		// List<PostProcessingComponentBase> m_ComponentsToEnable = new List<PostProcessingComponentBase>();
		List_1_t4203178569 * L_0 = (List_1_t4203178569 *)il2cpp_codegen_object_new(List_1_t4203178569_il2cpp_TypeInfo_var);
		List_1__ctor_m4170144800(L_0, /*hidden argument*/List_1__ctor_m4170144800_RuntimeMethod_var);
		__this->set_m_ComponentsToEnable_29(L_0);
		// List<PostProcessingComponentBase> m_ComponentsToDisable = new List<PostProcessingComponentBase>();
		List_1_t4203178569 * L_1 = (List_1_t4203178569 *)il2cpp_codegen_object_new(List_1_t4203178569_il2cpp_TypeInfo_var);
		List_1__ctor_m4170144800(L_1, /*hidden argument*/List_1__ctor_m4170144800_RuntimeMethod_var);
		__this->set_m_ComponentsToDisable_30(L_1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnEnable()
extern "C"  void PostProcessingBehaviour_OnEnable_m1469190174 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_OnEnable_m1469190174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PostProcessingComponentBase_t2731103827 * V_0 = NULL;
	Enumerator_t1797455150  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// m_CommandBuffers = new Dictionary<Type, KeyValuePair<CameraEvent, CommandBuffer>>();
		// m_CommandBuffers = new Dictionary<Type, KeyValuePair<CameraEvent, CommandBuffer>>();
		Dictionary_2_t1572824908 * L_0 = (Dictionary_2_t1572824908 *)il2cpp_codegen_object_new(Dictionary_2_t1572824908_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2678073514(L_0, /*hidden argument*/Dictionary_2__ctor_m2678073514_RuntimeMethod_var);
		__this->set_m_CommandBuffers_4(L_0);
		// m_MaterialFactory = new MaterialFactory();
		// m_MaterialFactory = new MaterialFactory();
		MaterialFactory_t2445948724 * L_1 = (MaterialFactory_t2445948724 *)il2cpp_codegen_object_new(MaterialFactory_t2445948724_il2cpp_TypeInfo_var);
		MaterialFactory__ctor_m231704672(L_1, /*hidden argument*/NULL);
		__this->set_m_MaterialFactory_7(L_1);
		// m_RenderTextureFactory = new RenderTextureFactory();
		// m_RenderTextureFactory = new RenderTextureFactory();
		RenderTextureFactory_t1946967824 * L_2 = (RenderTextureFactory_t1946967824 *)il2cpp_codegen_object_new(RenderTextureFactory_t1946967824_il2cpp_TypeInfo_var);
		RenderTextureFactory__ctor_m1345809438(L_2, /*hidden argument*/NULL);
		__this->set_m_RenderTextureFactory_8(L_2);
		// m_Context = new PostProcessingContext();
		// m_Context = new PostProcessingContext();
		PostProcessingContext_t2014408948 * L_3 = (PostProcessingContext_t2014408948 *)il2cpp_codegen_object_new(PostProcessingContext_t2014408948_il2cpp_TypeInfo_var);
		PostProcessingContext__ctor_m1285766435(L_3, /*hidden argument*/NULL);
		__this->set_m_Context_9(L_3);
		// m_Components = new List<PostProcessingComponentBase>();
		// m_Components = new List<PostProcessingComponentBase>();
		List_1_t4203178569 * L_4 = (List_1_t4203178569 *)il2cpp_codegen_object_new(List_1_t4203178569_il2cpp_TypeInfo_var);
		List_1__ctor_m4170144800(L_4, /*hidden argument*/List_1__ctor_m4170144800_RuntimeMethod_var);
		__this->set_m_Components_5(L_4);
		// m_DebugViews = AddComponent(new BuiltinDebugViewsComponent());
		// m_DebugViews = AddComponent(new BuiltinDebugViewsComponent());
		BuiltinDebugViewsComponent_t2123147871 * L_5 = (BuiltinDebugViewsComponent_t2123147871 *)il2cpp_codegen_object_new(BuiltinDebugViewsComponent_t2123147871_il2cpp_TypeInfo_var);
		BuiltinDebugViewsComponent__ctor_m2740954434(L_5, /*hidden argument*/NULL);
		// m_DebugViews = AddComponent(new BuiltinDebugViewsComponent());
		BuiltinDebugViewsComponent_t2123147871 * L_6 = PostProcessingBehaviour_AddComponent_TisBuiltinDebugViewsComponent_t2123147871_m1960338779(__this, L_5, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisBuiltinDebugViewsComponent_t2123147871_m1960338779_RuntimeMethod_var);
		__this->set_m_DebugViews_13(L_6);
		// m_AmbientOcclusion = AddComponent(new AmbientOcclusionComponent());
		// m_AmbientOcclusion = AddComponent(new AmbientOcclusionComponent());
		AmbientOcclusionComponent_t4130625043 * L_7 = (AmbientOcclusionComponent_t4130625043 *)il2cpp_codegen_object_new(AmbientOcclusionComponent_t4130625043_il2cpp_TypeInfo_var);
		AmbientOcclusionComponent__ctor_m2762306729(L_7, /*hidden argument*/NULL);
		// m_AmbientOcclusion = AddComponent(new AmbientOcclusionComponent());
		AmbientOcclusionComponent_t4130625043 * L_8 = PostProcessingBehaviour_AddComponent_TisAmbientOcclusionComponent_t4130625043_m2502213982(__this, L_7, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisAmbientOcclusionComponent_t4130625043_m2502213982_RuntimeMethod_var);
		__this->set_m_AmbientOcclusion_14(L_8);
		// m_ScreenSpaceReflection = AddComponent(new ScreenSpaceReflectionComponent());
		// m_ScreenSpaceReflection = AddComponent(new ScreenSpaceReflectionComponent());
		ScreenSpaceReflectionComponent_t856094247 * L_9 = (ScreenSpaceReflectionComponent_t856094247 *)il2cpp_codegen_object_new(ScreenSpaceReflectionComponent_t856094247_il2cpp_TypeInfo_var);
		ScreenSpaceReflectionComponent__ctor_m2879296341(L_9, /*hidden argument*/NULL);
		// m_ScreenSpaceReflection = AddComponent(new ScreenSpaceReflectionComponent());
		ScreenSpaceReflectionComponent_t856094247 * L_10 = PostProcessingBehaviour_AddComponent_TisScreenSpaceReflectionComponent_t856094247_m3898891935(__this, L_9, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisScreenSpaceReflectionComponent_t856094247_m3898891935_RuntimeMethod_var);
		__this->set_m_ScreenSpaceReflection_15(L_10);
		// m_FogComponent = AddComponent(new FogComponent());
		// m_FogComponent = AddComponent(new FogComponent());
		FogComponent_t3400726830 * L_11 = (FogComponent_t3400726830 *)il2cpp_codegen_object_new(FogComponent_t3400726830_il2cpp_TypeInfo_var);
		FogComponent__ctor_m4028577741(L_11, /*hidden argument*/NULL);
		// m_FogComponent = AddComponent(new FogComponent());
		FogComponent_t3400726830 * L_12 = PostProcessingBehaviour_AddComponent_TisFogComponent_t3400726830_m2397394497(__this, L_11, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisFogComponent_t3400726830_m2397394497_RuntimeMethod_var);
		__this->set_m_FogComponent_16(L_12);
		// m_MotionBlur = AddComponent(new MotionBlurComponent());
		// m_MotionBlur = AddComponent(new MotionBlurComponent());
		MotionBlurComponent_t3686516877 * L_13 = (MotionBlurComponent_t3686516877 *)il2cpp_codegen_object_new(MotionBlurComponent_t3686516877_il2cpp_TypeInfo_var);
		MotionBlurComponent__ctor_m2500919077(L_13, /*hidden argument*/NULL);
		// m_MotionBlur = AddComponent(new MotionBlurComponent());
		MotionBlurComponent_t3686516877 * L_14 = PostProcessingBehaviour_AddComponent_TisMotionBlurComponent_t3686516877_m2944290405(__this, L_13, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisMotionBlurComponent_t3686516877_m2944290405_RuntimeMethod_var);
		__this->set_m_MotionBlur_17(L_14);
		// m_Taa = AddComponent(new TaaComponent());
		// m_Taa = AddComponent(new TaaComponent());
		TaaComponent_t3791749658 * L_15 = (TaaComponent_t3791749658 *)il2cpp_codegen_object_new(TaaComponent_t3791749658_il2cpp_TypeInfo_var);
		TaaComponent__ctor_m675959233(L_15, /*hidden argument*/NULL);
		// m_Taa = AddComponent(new TaaComponent());
		TaaComponent_t3791749658 * L_16 = PostProcessingBehaviour_AddComponent_TisTaaComponent_t3791749658_m1885470620(__this, L_15, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisTaaComponent_t3791749658_m1885470620_RuntimeMethod_var);
		__this->set_m_Taa_18(L_16);
		// m_EyeAdaptation = AddComponent(new EyeAdaptationComponent());
		// m_EyeAdaptation = AddComponent(new EyeAdaptationComponent());
		EyeAdaptationComponent_t3394805121 * L_17 = (EyeAdaptationComponent_t3394805121 *)il2cpp_codegen_object_new(EyeAdaptationComponent_t3394805121_il2cpp_TypeInfo_var);
		EyeAdaptationComponent__ctor_m137598109(L_17, /*hidden argument*/NULL);
		// m_EyeAdaptation = AddComponent(new EyeAdaptationComponent());
		EyeAdaptationComponent_t3394805121 * L_18 = PostProcessingBehaviour_AddComponent_TisEyeAdaptationComponent_t3394805121_m1462221255(__this, L_17, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisEyeAdaptationComponent_t3394805121_m1462221255_RuntimeMethod_var);
		__this->set_m_EyeAdaptation_19(L_18);
		// m_DepthOfField = AddComponent(new DepthOfFieldComponent());
		// m_DepthOfField = AddComponent(new DepthOfFieldComponent());
		DepthOfFieldComponent_t554756766 * L_19 = (DepthOfFieldComponent_t554756766 *)il2cpp_codegen_object_new(DepthOfFieldComponent_t554756766_il2cpp_TypeInfo_var);
		DepthOfFieldComponent__ctor_m954390370(L_19, /*hidden argument*/NULL);
		// m_DepthOfField = AddComponent(new DepthOfFieldComponent());
		DepthOfFieldComponent_t554756766 * L_20 = PostProcessingBehaviour_AddComponent_TisDepthOfFieldComponent_t554756766_m2473682395(__this, L_19, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisDepthOfFieldComponent_t554756766_m2473682395_RuntimeMethod_var);
		__this->set_m_DepthOfField_20(L_20);
		// m_Bloom = AddComponent(new BloomComponent());
		// m_Bloom = AddComponent(new BloomComponent());
		BloomComponent_t3791419130 * L_21 = (BloomComponent_t3791419130 *)il2cpp_codegen_object_new(BloomComponent_t3791419130_il2cpp_TypeInfo_var);
		BloomComponent__ctor_m3106370286(L_21, /*hidden argument*/NULL);
		// m_Bloom = AddComponent(new BloomComponent());
		BloomComponent_t3791419130 * L_22 = PostProcessingBehaviour_AddComponent_TisBloomComponent_t3791419130_m3367245233(__this, L_21, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisBloomComponent_t3791419130_m3367245233_RuntimeMethod_var);
		__this->set_m_Bloom_21(L_22);
		// m_ChromaticAberration = AddComponent(new ChromaticAberrationComponent());
		// m_ChromaticAberration = AddComponent(new ChromaticAberrationComponent());
		ChromaticAberrationComponent_t1647263118 * L_23 = (ChromaticAberrationComponent_t1647263118 *)il2cpp_codegen_object_new(ChromaticAberrationComponent_t1647263118_il2cpp_TypeInfo_var);
		ChromaticAberrationComponent__ctor_m3851952747(L_23, /*hidden argument*/NULL);
		// m_ChromaticAberration = AddComponent(new ChromaticAberrationComponent());
		ChromaticAberrationComponent_t1647263118 * L_24 = PostProcessingBehaviour_AddComponent_TisChromaticAberrationComponent_t1647263118_m2769190930(__this, L_23, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisChromaticAberrationComponent_t1647263118_m2769190930_RuntimeMethod_var);
		__this->set_m_ChromaticAberration_22(L_24);
		// m_ColorGrading = AddComponent(new ColorGradingComponent());
		// m_ColorGrading = AddComponent(new ColorGradingComponent());
		ColorGradingComponent_t1715259467 * L_25 = (ColorGradingComponent_t1715259467 *)il2cpp_codegen_object_new(ColorGradingComponent_t1715259467_il2cpp_TypeInfo_var);
		ColorGradingComponent__ctor_m1390733314(L_25, /*hidden argument*/NULL);
		// m_ColorGrading = AddComponent(new ColorGradingComponent());
		ColorGradingComponent_t1715259467 * L_26 = PostProcessingBehaviour_AddComponent_TisColorGradingComponent_t1715259467_m47539266(__this, L_25, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisColorGradingComponent_t1715259467_m47539266_RuntimeMethod_var);
		__this->set_m_ColorGrading_23(L_26);
		// m_UserLut = AddComponent(new UserLutComponent());
		// m_UserLut = AddComponent(new UserLutComponent());
		UserLutComponent_t2843161776 * L_27 = (UserLutComponent_t2843161776 *)il2cpp_codegen_object_new(UserLutComponent_t2843161776_il2cpp_TypeInfo_var);
		UserLutComponent__ctor_m3557887131(L_27, /*hidden argument*/NULL);
		// m_UserLut = AddComponent(new UserLutComponent());
		UserLutComponent_t2843161776 * L_28 = PostProcessingBehaviour_AddComponent_TisUserLutComponent_t2843161776_m3715666184(__this, L_27, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisUserLutComponent_t2843161776_m3715666184_RuntimeMethod_var);
		__this->set_m_UserLut_24(L_28);
		// m_Grain = AddComponent(new GrainComponent());
		// m_Grain = AddComponent(new GrainComponent());
		GrainComponent_t866324317 * L_29 = (GrainComponent_t866324317 *)il2cpp_codegen_object_new(GrainComponent_t866324317_il2cpp_TypeInfo_var);
		GrainComponent__ctor_m497184467(L_29, /*hidden argument*/NULL);
		// m_Grain = AddComponent(new GrainComponent());
		GrainComponent_t866324317 * L_30 = PostProcessingBehaviour_AddComponent_TisGrainComponent_t866324317_m3375556804(__this, L_29, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisGrainComponent_t866324317_m3375556804_RuntimeMethod_var);
		__this->set_m_Grain_25(L_30);
		// m_Vignette = AddComponent(new VignetteComponent());
		// m_Vignette = AddComponent(new VignetteComponent());
		VignetteComponent_t3243642943 * L_31 = (VignetteComponent_t3243642943 *)il2cpp_codegen_object_new(VignetteComponent_t3243642943_il2cpp_TypeInfo_var);
		VignetteComponent__ctor_m3640704423(L_31, /*hidden argument*/NULL);
		// m_Vignette = AddComponent(new VignetteComponent());
		VignetteComponent_t3243642943 * L_32 = PostProcessingBehaviour_AddComponent_TisVignetteComponent_t3243642943_m2941218025(__this, L_31, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisVignetteComponent_t3243642943_m2941218025_RuntimeMethod_var);
		__this->set_m_Vignette_26(L_32);
		// m_Dithering = AddComponent(new DitheringComponent());
		// m_Dithering = AddComponent(new DitheringComponent());
		DitheringComponent_t277621267 * L_33 = (DitheringComponent_t277621267 *)il2cpp_codegen_object_new(DitheringComponent_t277621267_il2cpp_TypeInfo_var);
		DitheringComponent__ctor_m11042692(L_33, /*hidden argument*/NULL);
		// m_Dithering = AddComponent(new DitheringComponent());
		DitheringComponent_t277621267 * L_34 = PostProcessingBehaviour_AddComponent_TisDitheringComponent_t277621267_m1758707692(__this, L_33, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisDitheringComponent_t277621267_m1758707692_RuntimeMethod_var);
		__this->set_m_Dithering_27(L_34);
		// m_Fxaa = AddComponent(new FxaaComponent());
		// m_Fxaa = AddComponent(new FxaaComponent());
		FxaaComponent_t1312385771 * L_35 = (FxaaComponent_t1312385771 *)il2cpp_codegen_object_new(FxaaComponent_t1312385771_il2cpp_TypeInfo_var);
		FxaaComponent__ctor_m3929535888(L_35, /*hidden argument*/NULL);
		// m_Fxaa = AddComponent(new FxaaComponent());
		FxaaComponent_t1312385771 * L_36 = PostProcessingBehaviour_AddComponent_TisFxaaComponent_t1312385771_m1843850521(__this, L_35, /*hidden argument*/PostProcessingBehaviour_AddComponent_TisFxaaComponent_t1312385771_m1843850521_RuntimeMethod_var);
		__this->set_m_Fxaa_28(L_36);
		// m_ComponentStates = new Dictionary<PostProcessingComponentBase, bool>();
		// m_ComponentStates = new Dictionary<PostProcessingComponentBase, bool>();
		Dictionary_2_t3095696878 * L_37 = (Dictionary_2_t3095696878 *)il2cpp_codegen_object_new(Dictionary_2_t3095696878_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m896571293(L_37, /*hidden argument*/Dictionary_2__ctor_m896571293_RuntimeMethod_var);
		__this->set_m_ComponentStates_6(L_37);
		// foreach (var component in m_Components)
		List_1_t4203178569 * L_38 = __this->get_m_Components_5();
		// foreach (var component in m_Components)
		NullCheck(L_38);
		Enumerator_t1797455150  L_39 = List_1_GetEnumerator_m659292003(L_38, /*hidden argument*/List_1_GetEnumerator_m659292003_RuntimeMethod_var);
		V_1 = L_39;
	}

IL_0160:
	try
	{ // begin try (depth: 1)
		{
			goto IL_017a;
		}

IL_0165:
		{
			// foreach (var component in m_Components)
			// foreach (var component in m_Components)
			PostProcessingComponentBase_t2731103827 * L_40 = Enumerator_get_Current_m747737324((&V_1), /*hidden argument*/Enumerator_get_Current_m747737324_RuntimeMethod_var);
			V_0 = L_40;
			// m_ComponentStates.Add(component, false);
			Dictionary_2_t3095696878 * L_41 = __this->get_m_ComponentStates_6();
			PostProcessingComponentBase_t2731103827 * L_42 = V_0;
			// m_ComponentStates.Add(component, false);
			NullCheck(L_41);
			Dictionary_2_Add_m1451613760(L_41, L_42, (bool)0, /*hidden argument*/Dictionary_2_Add_m1451613760_RuntimeMethod_var);
		}

IL_017a:
		{
			// foreach (var component in m_Components)
			bool L_43 = Enumerator_MoveNext_m425563657((&V_1), /*hidden argument*/Enumerator_MoveNext_m425563657_RuntimeMethod_var);
			if (L_43)
			{
				goto IL_0165;
			}
		}

IL_0186:
		{
			IL2CPP_LEAVE(0x199, FINALLY_018b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_018b;
	}

FINALLY_018b:
	{ // begin finally (depth: 1)
		// foreach (var component in m_Components)
		Enumerator_Dispose_m2340450683((&V_1), /*hidden argument*/Enumerator_Dispose_m2340450683_RuntimeMethod_var);
		IL2CPP_END_FINALLY(395)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(395)
	{
		IL2CPP_JUMP_TBL(0x199, IL_0199)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0199:
	{
		// useGUILayout = false;
		// useGUILayout = false;
		MonoBehaviour_set_useGUILayout_m3492031340(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnPreCull()
extern "C"  void PostProcessingBehaviour_OnPreCull_m382834868 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_OnPreCull_m382834868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PostProcessingContext_t2014408948 * V_0 = NULL;
	int32_t V_1 = 0;
	PostProcessingComponentBase_t2731103827 * V_2 = NULL;
	Enumerator_t1797455150  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// m_Camera = GetComponent<Camera>();
		// m_Camera = GetComponent<Camera>();
		Camera_t4157153871 * L_0 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		__this->set_m_Camera_10(L_0);
		// if (profile == null || m_Camera == null)
		PostProcessingProfile_t724195375 * L_1 = __this->get_profile_2();
		// if (profile == null || m_Camera == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002f;
		}
	}
	{
		Camera_t4157153871 * L_3 = __this->get_m_Camera_10();
		// if (profile == null || m_Camera == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}

IL_002f:
	{
		// return;
		goto IL_02ae;
	}

IL_0034:
	{
		// var context = m_Context.Reset();
		PostProcessingContext_t2014408948 * L_5 = __this->get_m_Context_9();
		// var context = m_Context.Reset();
		NullCheck(L_5);
		PostProcessingContext_t2014408948 * L_6 = PostProcessingContext_Reset_m4133896387(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// context.profile = profile;
		PostProcessingContext_t2014408948 * L_7 = V_0;
		PostProcessingProfile_t724195375 * L_8 = __this->get_profile_2();
		NullCheck(L_7);
		L_7->set_profile_0(L_8);
		// context.renderTextureFactory = m_RenderTextureFactory;
		PostProcessingContext_t2014408948 * L_9 = V_0;
		RenderTextureFactory_t1946967824 * L_10 = __this->get_m_RenderTextureFactory_8();
		NullCheck(L_9);
		L_9->set_renderTextureFactory_3(L_10);
		// context.materialFactory = m_MaterialFactory;
		PostProcessingContext_t2014408948 * L_11 = V_0;
		MaterialFactory_t2445948724 * L_12 = __this->get_m_MaterialFactory_7();
		NullCheck(L_11);
		L_11->set_materialFactory_2(L_12);
		// context.camera = m_Camera;
		PostProcessingContext_t2014408948 * L_13 = V_0;
		Camera_t4157153871 * L_14 = __this->get_m_Camera_10();
		NullCheck(L_13);
		L_13->set_camera_1(L_14);
		// m_DebugViews.Init(context, profile.debugViews);
		BuiltinDebugViewsComponent_t2123147871 * L_15 = __this->get_m_DebugViews_13();
		PostProcessingContext_t2014408948 * L_16 = V_0;
		PostProcessingProfile_t724195375 * L_17 = __this->get_profile_2();
		NullCheck(L_17);
		BuiltinDebugViewsModel_t1462618840 * L_18 = L_17->get_debugViews_2();
		// m_DebugViews.Init(context, profile.debugViews);
		NullCheck(L_15);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, BuiltinDebugViewsModel_t1462618840 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_15, L_16, L_18);
		// m_AmbientOcclusion.Init(context, profile.ambientOcclusion);
		AmbientOcclusionComponent_t4130625043 * L_19 = __this->get_m_AmbientOcclusion_14();
		PostProcessingContext_t2014408948 * L_20 = V_0;
		PostProcessingProfile_t724195375 * L_21 = __this->get_profile_2();
		NullCheck(L_21);
		AmbientOcclusionModel_t389471066 * L_22 = L_21->get_ambientOcclusion_5();
		// m_AmbientOcclusion.Init(context, profile.ambientOcclusion);
		NullCheck(L_19);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, AmbientOcclusionModel_t389471066 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AmbientOcclusionModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_19, L_20, L_22);
		// m_ScreenSpaceReflection.Init(context, profile.screenSpaceReflection);
		ScreenSpaceReflectionComponent_t856094247 * L_23 = __this->get_m_ScreenSpaceReflection_15();
		PostProcessingContext_t2014408948 * L_24 = V_0;
		PostProcessingProfile_t724195375 * L_25 = __this->get_profile_2();
		NullCheck(L_25);
		ScreenSpaceReflectionModel_t3026344732 * L_26 = L_25->get_screenSpaceReflection_6();
		// m_ScreenSpaceReflection.Init(context, profile.screenSpaceReflection);
		NullCheck(L_23);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, ScreenSpaceReflectionModel_t3026344732 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_23, L_24, L_26);
		// m_FogComponent.Init(context, profile.fog);
		FogComponent_t3400726830 * L_27 = __this->get_m_FogComponent_16();
		PostProcessingContext_t2014408948 * L_28 = V_0;
		PostProcessingProfile_t724195375 * L_29 = __this->get_profile_2();
		NullCheck(L_29);
		FogModel_t3620688749 * L_30 = L_29->get_fog_3();
		// m_FogComponent.Init(context, profile.fog);
		NullCheck(L_27);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, FogModel_t3620688749 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.FogModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_27, L_28, L_30);
		// m_MotionBlur.Init(context, profile.motionBlur);
		MotionBlurComponent_t3686516877 * L_31 = __this->get_m_MotionBlur_17();
		PostProcessingContext_t2014408948 * L_32 = V_0;
		PostProcessingProfile_t724195375 * L_33 = __this->get_profile_2();
		NullCheck(L_33);
		MotionBlurModel_t3080286123 * L_34 = L_33->get_motionBlur_8();
		// m_MotionBlur.Init(context, profile.motionBlur);
		NullCheck(L_31);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, MotionBlurModel_t3080286123 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.MotionBlurModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_31, L_32, L_34);
		// m_Taa.Init(context, profile.antialiasing);
		TaaComponent_t3791749658 * L_35 = __this->get_m_Taa_18();
		PostProcessingContext_t2014408948 * L_36 = V_0;
		PostProcessingProfile_t724195375 * L_37 = __this->get_profile_2();
		NullCheck(L_37);
		AntialiasingModel_t1521139388 * L_38 = L_37->get_antialiasing_4();
		// m_Taa.Init(context, profile.antialiasing);
		NullCheck(L_35);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, AntialiasingModel_t1521139388 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AntialiasingModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_35, L_36, L_38);
		// m_EyeAdaptation.Init(context, profile.eyeAdaptation);
		EyeAdaptationComponent_t3394805121 * L_39 = __this->get_m_EyeAdaptation_19();
		PostProcessingContext_t2014408948 * L_40 = V_0;
		PostProcessingProfile_t724195375 * L_41 = __this->get_profile_2();
		NullCheck(L_41);
		EyeAdaptationModel_t242823912 * L_42 = L_41->get_eyeAdaptation_9();
		// m_EyeAdaptation.Init(context, profile.eyeAdaptation);
		NullCheck(L_39);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, EyeAdaptationModel_t242823912 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.EyeAdaptationModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_39, L_40, L_42);
		// m_DepthOfField.Init(context, profile.depthOfField);
		DepthOfFieldComponent_t554756766 * L_43 = __this->get_m_DepthOfField_20();
		PostProcessingContext_t2014408948 * L_44 = V_0;
		PostProcessingProfile_t724195375 * L_45 = __this->get_profile_2();
		NullCheck(L_45);
		DepthOfFieldModel_t514067330 * L_46 = L_45->get_depthOfField_7();
		// m_DepthOfField.Init(context, profile.depthOfField);
		NullCheck(L_43);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, DepthOfFieldModel_t514067330 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DepthOfFieldModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_43, L_44, L_46);
		// m_Bloom.Init(context, profile.bloom);
		BloomComponent_t3791419130 * L_47 = __this->get_m_Bloom_21();
		PostProcessingContext_t2014408948 * L_48 = V_0;
		PostProcessingProfile_t724195375 * L_49 = __this->get_profile_2();
		NullCheck(L_49);
		BloomModel_t2099727860 * L_50 = L_49->get_bloom_10();
		// m_Bloom.Init(context, profile.bloom);
		NullCheck(L_47);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, BloomModel_t2099727860 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BloomModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_47, L_48, L_50);
		// m_ChromaticAberration.Init(context, profile.chromaticAberration);
		ChromaticAberrationComponent_t1647263118 * L_51 = __this->get_m_ChromaticAberration_22();
		PostProcessingContext_t2014408948 * L_52 = V_0;
		PostProcessingProfile_t724195375 * L_53 = __this->get_profile_2();
		NullCheck(L_53);
		ChromaticAberrationModel_t3963399853 * L_54 = L_53->get_chromaticAberration_13();
		// m_ChromaticAberration.Init(context, profile.chromaticAberration);
		NullCheck(L_51);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, ChromaticAberrationModel_t3963399853 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ChromaticAberrationModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_51, L_52, L_54);
		// m_ColorGrading.Init(context, profile.colorGrading);
		ColorGradingComponent_t1715259467 * L_55 = __this->get_m_ColorGrading_23();
		PostProcessingContext_t2014408948 * L_56 = V_0;
		PostProcessingProfile_t724195375 * L_57 = __this->get_profile_2();
		NullCheck(L_57);
		ColorGradingModel_t1448048181 * L_58 = L_57->get_colorGrading_11();
		// m_ColorGrading.Init(context, profile.colorGrading);
		NullCheck(L_55);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, ColorGradingModel_t1448048181 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ColorGradingModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_55, L_56, L_58);
		// m_UserLut.Init(context, profile.userLut);
		UserLutComponent_t2843161776 * L_59 = __this->get_m_UserLut_24();
		PostProcessingContext_t2014408948 * L_60 = V_0;
		PostProcessingProfile_t724195375 * L_61 = __this->get_profile_2();
		NullCheck(L_61);
		UserLutModel_t1670108080 * L_62 = L_61->get_userLut_12();
		// m_UserLut.Init(context, profile.userLut);
		NullCheck(L_59);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, UserLutModel_t1670108080 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.UserLutModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_59, L_60, L_62);
		// m_Grain.Init(context, profile.grain);
		GrainComponent_t866324317 * L_63 = __this->get_m_Grain_25();
		PostProcessingContext_t2014408948 * L_64 = V_0;
		PostProcessingProfile_t724195375 * L_65 = __this->get_profile_2();
		NullCheck(L_65);
		GrainModel_t1152882488 * L_66 = L_65->get_grain_14();
		// m_Grain.Init(context, profile.grain);
		NullCheck(L_63);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, GrainModel_t1152882488 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.GrainModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_63, L_64, L_66);
		// m_Vignette.Init(context, profile.vignette);
		VignetteComponent_t3243642943 * L_67 = __this->get_m_Vignette_26();
		PostProcessingContext_t2014408948 * L_68 = V_0;
		PostProcessingProfile_t724195375 * L_69 = __this->get_profile_2();
		NullCheck(L_69);
		VignetteModel_t2845517177 * L_70 = L_69->get_vignette_15();
		// m_Vignette.Init(context, profile.vignette);
		NullCheck(L_67);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, VignetteModel_t2845517177 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.VignetteModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_67, L_68, L_70);
		// m_Dithering.Init(context, profile.dithering);
		DitheringComponent_t277621267 * L_71 = __this->get_m_Dithering_27();
		PostProcessingContext_t2014408948 * L_72 = V_0;
		PostProcessingProfile_t724195375 * L_73 = __this->get_profile_2();
		NullCheck(L_73);
		DitheringModel_t2429005396 * L_74 = L_73->get_dithering_16();
		// m_Dithering.Init(context, profile.dithering);
		NullCheck(L_71);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, DitheringModel_t2429005396 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DitheringModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_71, L_72, L_74);
		// m_Fxaa.Init(context, profile.antialiasing);
		FxaaComponent_t1312385771 * L_75 = __this->get_m_Fxaa_28();
		PostProcessingContext_t2014408948 * L_76 = V_0;
		PostProcessingProfile_t724195375 * L_77 = __this->get_profile_2();
		NullCheck(L_77);
		AntialiasingModel_t1521139388 * L_78 = L_77->get_antialiasing_4();
		// m_Fxaa.Init(context, profile.antialiasing);
		NullCheck(L_75);
		VirtActionInvoker2< PostProcessingContext_t2014408948 *, AntialiasingModel_t1521139388 * >::Invoke(9 /* System.Void UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AntialiasingModel>::Init(UnityEngine.PostProcessing.PostProcessingContext,T) */, L_75, L_76, L_78);
		// if (m_PreviousProfile != profile)
		PostProcessingProfile_t724195375 * L_79 = __this->get_m_PreviousProfile_11();
		PostProcessingProfile_t724195375 * L_80 = __this->get_profile_2();
		// if (m_PreviousProfile != profile)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_81 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_79, L_80, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_020a;
		}
	}
	{
		// DisableComponents();
		// DisableComponents();
		PostProcessingBehaviour_DisableComponents_m1054770722(__this, /*hidden argument*/NULL);
		// m_PreviousProfile = profile;
		PostProcessingProfile_t724195375 * L_82 = __this->get_profile_2();
		__this->set_m_PreviousProfile_11(L_82);
	}

IL_020a:
	{
		// CheckObservers();
		// CheckObservers();
		PostProcessingBehaviour_CheckObservers_m3285018848(__this, /*hidden argument*/NULL);
		// var flags = DepthTextureMode.None;
		V_1 = 0;
		// foreach (var component in m_Components)
		List_1_t4203178569 * L_83 = __this->get_m_Components_5();
		// foreach (var component in m_Components)
		NullCheck(L_83);
		Enumerator_t1797455150  L_84 = List_1_GetEnumerator_m659292003(L_83, /*hidden argument*/List_1_GetEnumerator_m659292003_RuntimeMethod_var);
		V_3 = L_84;
	}

IL_021f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0242;
		}

IL_0224:
		{
			// foreach (var component in m_Components)
			// foreach (var component in m_Components)
			PostProcessingComponentBase_t2731103827 * L_85 = Enumerator_get_Current_m747737324((&V_3), /*hidden argument*/Enumerator_get_Current_m747737324_RuntimeMethod_var);
			V_2 = L_85;
			// if (component.active)
			PostProcessingComponentBase_t2731103827 * L_86 = V_2;
			// if (component.active)
			NullCheck(L_86);
			bool L_87 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_86);
			if (!L_87)
			{
				goto IL_0241;
			}
		}

IL_0238:
		{
			// flags |= component.GetCameraFlags();
			int32_t L_88 = V_1;
			PostProcessingComponentBase_t2731103827 * L_89 = V_2;
			// flags |= component.GetCameraFlags();
			NullCheck(L_89);
			int32_t L_90 = VirtFuncInvoker0< int32_t >::Invoke(4 /* UnityEngine.DepthTextureMode UnityEngine.PostProcessing.PostProcessingComponentBase::GetCameraFlags() */, L_89);
			V_1 = ((int32_t)((int32_t)L_88|(int32_t)L_90));
		}

IL_0241:
		{
		}

IL_0242:
		{
			// foreach (var component in m_Components)
			bool L_91 = Enumerator_MoveNext_m425563657((&V_3), /*hidden argument*/Enumerator_MoveNext_m425563657_RuntimeMethod_var);
			if (L_91)
			{
				goto IL_0224;
			}
		}

IL_024e:
		{
			IL2CPP_LEAVE(0x261, FINALLY_0253);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0253;
	}

FINALLY_0253:
	{ // begin finally (depth: 1)
		// foreach (var component in m_Components)
		Enumerator_Dispose_m2340450683((&V_3), /*hidden argument*/Enumerator_Dispose_m2340450683_RuntimeMethod_var);
		IL2CPP_END_FINALLY(595)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(595)
	{
		IL2CPP_JUMP_TBL(0x261, IL_0261)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0261:
	{
		// context.camera.depthTextureMode = flags;
		PostProcessingContext_t2014408948 * L_92 = V_0;
		NullCheck(L_92);
		Camera_t4157153871 * L_93 = L_92->get_camera_1();
		int32_t L_94 = V_1;
		// context.camera.depthTextureMode = flags;
		NullCheck(L_93);
		Camera_set_depthTextureMode_m754977860(L_93, L_94, /*hidden argument*/NULL);
		// if (!m_RenderingInSceneView && m_Taa.active && !profile.debugViews.willInterrupt)
		bool L_95 = __this->get_m_RenderingInSceneView_12();
		if (L_95)
		{
			goto IL_02ae;
		}
	}
	{
		TaaComponent_t3791749658 * L_96 = __this->get_m_Taa_18();
		// if (!m_RenderingInSceneView && m_Taa.active && !profile.debugViews.willInterrupt)
		NullCheck(L_96);
		bool L_97 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_96);
		if (!L_97)
		{
			goto IL_02ae;
		}
	}
	{
		PostProcessingProfile_t724195375 * L_98 = __this->get_profile_2();
		NullCheck(L_98);
		BuiltinDebugViewsModel_t1462618840 * L_99 = L_98->get_debugViews_2();
		// if (!m_RenderingInSceneView && m_Taa.active && !profile.debugViews.willInterrupt)
		NullCheck(L_99);
		bool L_100 = BuiltinDebugViewsModel_get_willInterrupt_m2596798096(L_99, /*hidden argument*/NULL);
		if (L_100)
		{
			goto IL_02ae;
		}
	}
	{
		// m_Taa.SetProjectionMatrix(jitteredMatrixFunc);
		TaaComponent_t3791749658 * L_101 = __this->get_m_Taa_18();
		Func_2_t4093140010 * L_102 = __this->get_jitteredMatrixFunc_3();
		// m_Taa.SetProjectionMatrix(jitteredMatrixFunc);
		NullCheck(L_101);
		TaaComponent_SetProjectionMatrix_m2316589171(L_101, L_102, /*hidden argument*/NULL);
	}

IL_02ae:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnPreRender()
extern "C"  void PostProcessingBehaviour_OnPreRender_m1753639883 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_OnPreRender_m1753639883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (profile == null)
		PostProcessingProfile_t724195375 * L_0 = __this->get_profile_2();
		// if (profile == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		// return;
		goto IL_005e;
	}

IL_0017:
	{
		// TryExecuteCommandBuffer(m_DebugViews);
		BuiltinDebugViewsComponent_t2123147871 * L_2 = __this->get_m_DebugViews_13();
		// TryExecuteCommandBuffer(m_DebugViews);
		PostProcessingBehaviour_TryExecuteCommandBuffer_TisBuiltinDebugViewsModel_t1462618840_m3493222038(__this, L_2, /*hidden argument*/PostProcessingBehaviour_TryExecuteCommandBuffer_TisBuiltinDebugViewsModel_t1462618840_m3493222038_RuntimeMethod_var);
		// TryExecuteCommandBuffer(m_AmbientOcclusion);
		AmbientOcclusionComponent_t4130625043 * L_3 = __this->get_m_AmbientOcclusion_14();
		// TryExecuteCommandBuffer(m_AmbientOcclusion);
		PostProcessingBehaviour_TryExecuteCommandBuffer_TisAmbientOcclusionModel_t389471066_m619521879(__this, L_3, /*hidden argument*/PostProcessingBehaviour_TryExecuteCommandBuffer_TisAmbientOcclusionModel_t389471066_m619521879_RuntimeMethod_var);
		// TryExecuteCommandBuffer(m_ScreenSpaceReflection);
		ScreenSpaceReflectionComponent_t856094247 * L_4 = __this->get_m_ScreenSpaceReflection_15();
		// TryExecuteCommandBuffer(m_ScreenSpaceReflection);
		PostProcessingBehaviour_TryExecuteCommandBuffer_TisScreenSpaceReflectionModel_t3026344732_m311471412(__this, L_4, /*hidden argument*/PostProcessingBehaviour_TryExecuteCommandBuffer_TisScreenSpaceReflectionModel_t3026344732_m311471412_RuntimeMethod_var);
		// TryExecuteCommandBuffer(m_FogComponent);
		FogComponent_t3400726830 * L_5 = __this->get_m_FogComponent_16();
		// TryExecuteCommandBuffer(m_FogComponent);
		PostProcessingBehaviour_TryExecuteCommandBuffer_TisFogModel_t3620688749_m3894248309(__this, L_5, /*hidden argument*/PostProcessingBehaviour_TryExecuteCommandBuffer_TisFogModel_t3620688749_m3894248309_RuntimeMethod_var);
		// if (!m_RenderingInSceneView)
		bool L_6 = __this->get_m_RenderingInSceneView_12();
		if (L_6)
		{
			goto IL_005e;
		}
	}
	{
		// TryExecuteCommandBuffer(m_MotionBlur);
		MotionBlurComponent_t3686516877 * L_7 = __this->get_m_MotionBlur_17();
		// TryExecuteCommandBuffer(m_MotionBlur);
		PostProcessingBehaviour_TryExecuteCommandBuffer_TisMotionBlurModel_t3080286123_m3591009761(__this, L_7, /*hidden argument*/PostProcessingBehaviour_TryExecuteCommandBuffer_TisMotionBlurModel_t3080286123_m3591009761_RuntimeMethod_var);
	}

IL_005e:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnPostRender()
extern "C"  void PostProcessingBehaviour_OnPostRender_m2861720768 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_OnPostRender_m2861720768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (profile == null || m_Camera == null)
		PostProcessingProfile_t724195375 * L_0 = __this->get_profile_2();
		// if (profile == null || m_Camera == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		Camera_t4157153871 * L_2 = __this->get_m_Camera_10();
		// if (profile == null || m_Camera == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}

IL_0023:
	{
		// return;
		goto IL_0068;
	}

IL_0028:
	{
		// if (!m_RenderingInSceneView && m_Taa.active && !profile.debugViews.willInterrupt)
		bool L_4 = __this->get_m_RenderingInSceneView_12();
		if (L_4)
		{
			goto IL_0068;
		}
	}
	{
		TaaComponent_t3791749658 * L_5 = __this->get_m_Taa_18();
		// if (!m_RenderingInSceneView && m_Taa.active && !profile.debugViews.willInterrupt)
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_5);
		if (!L_6)
		{
			goto IL_0068;
		}
	}
	{
		PostProcessingProfile_t724195375 * L_7 = __this->get_profile_2();
		NullCheck(L_7);
		BuiltinDebugViewsModel_t1462618840 * L_8 = L_7->get_debugViews_2();
		// if (!m_RenderingInSceneView && m_Taa.active && !profile.debugViews.willInterrupt)
		NullCheck(L_8);
		bool L_9 = BuiltinDebugViewsModel_get_willInterrupt_m2596798096(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0068;
		}
	}
	{
		// m_Context.camera.ResetProjectionMatrix();
		PostProcessingContext_t2014408948 * L_10 = __this->get_m_Context_9();
		NullCheck(L_10);
		Camera_t4157153871 * L_11 = L_10->get_camera_1();
		// m_Context.camera.ResetProjectionMatrix();
		NullCheck(L_11);
		Camera_ResetProjectionMatrix_m1910759531(L_11, /*hidden argument*/NULL);
	}

IL_0068:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void PostProcessingBehaviour_OnRenderImage_m561762982 (PostProcessingBehaviour_t3229946336 * __this, RenderTexture_t2108887433 * ___source0, RenderTexture_t2108887433 * ___destination1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_OnRenderImage_m561762982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	Material_t340375123 * V_4 = NULL;
	RenderTexture_t2108887433 * V_5 = NULL;
	RenderTexture_t2108887433 * V_6 = NULL;
	RenderTexture_t2108887433 * V_7 = NULL;
	Texture_t3661962703 * V_8 = NULL;
	Material_t340375123 * V_9 = NULL;
	RenderTexture_t2108887433 * V_10 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B9_0 = 0;
	Material_t340375123 * G_B20_0 = NULL;
	{
		// if (profile == null || m_Camera == null)
		PostProcessingProfile_t724195375 * L_0 = __this->get_profile_2();
		// if (profile == null || m_Camera == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		Camera_t4157153871 * L_2 = __this->get_m_Camera_10();
		// if (profile == null || m_Camera == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}

IL_0023:
	{
		// Graphics.Blit(source, destination);
		RenderTexture_t2108887433 * L_4 = ___source0;
		RenderTexture_t2108887433 * L_5 = ___destination1;
		// Graphics.Blit(source, destination);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m1336850842(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		// return;
		goto IL_0274;
	}

IL_0030:
	{
		// bool uberActive = false;
		V_0 = (bool)0;
		// bool fxaaActive = m_Fxaa.active;
		FxaaComponent_t1312385771 * L_6 = __this->get_m_Fxaa_28();
		// bool fxaaActive = m_Fxaa.active;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_6);
		V_1 = L_7;
		// bool taaActive = m_Taa.active && !m_RenderingInSceneView;
		TaaComponent_t3791749658 * L_8 = __this->get_m_Taa_18();
		// bool taaActive = m_Taa.active && !m_RenderingInSceneView;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_8);
		if (!L_9)
		{
			goto IL_0059;
		}
	}
	{
		bool L_10 = __this->get_m_RenderingInSceneView_12();
		G_B6_0 = ((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		goto IL_005a;
	}

IL_0059:
	{
		G_B6_0 = 0;
	}

IL_005a:
	{
		V_2 = (bool)G_B6_0;
		// bool dofActive = m_DepthOfField.active && !m_RenderingInSceneView;
		DepthOfFieldComponent_t554756766 * L_11 = __this->get_m_DepthOfField_20();
		// bool dofActive = m_DepthOfField.active && !m_RenderingInSceneView;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_11);
		if (!L_12)
		{
			goto IL_0076;
		}
	}
	{
		bool L_13 = __this->get_m_RenderingInSceneView_12();
		G_B9_0 = ((((int32_t)L_13) == ((int32_t)0))? 1 : 0);
		goto IL_0077;
	}

IL_0076:
	{
		G_B9_0 = 0;
	}

IL_0077:
	{
		V_3 = (bool)G_B9_0;
		// var uberMaterial = m_MaterialFactory.Get("Hidden/Post FX/Uber Shader");
		MaterialFactory_t2445948724 * L_14 = __this->get_m_MaterialFactory_7();
		// var uberMaterial = m_MaterialFactory.Get("Hidden/Post FX/Uber Shader");
		NullCheck(L_14);
		Material_t340375123 * L_15 = MaterialFactory_Get_m4113232693(L_14, _stringLiteral1580053373, /*hidden argument*/NULL);
		V_4 = L_15;
		// uberMaterial.shaderKeywords = null;
		Material_t340375123 * L_16 = V_4;
		// uberMaterial.shaderKeywords = null;
		NullCheck(L_16);
		Material_set_shaderKeywords_m4017377042(L_16, (StringU5BU5D_t1281789340*)(StringU5BU5D_t1281789340*)NULL, /*hidden argument*/NULL);
		// var src = source;
		RenderTexture_t2108887433 * L_17 = ___source0;
		V_5 = L_17;
		// var dst = destination;
		RenderTexture_t2108887433 * L_18 = ___destination1;
		V_6 = L_18;
		// if (taaActive)
		bool L_19 = V_2;
		if (!L_19)
		{
			goto IL_00c2;
		}
	}
	{
		// var tempRT = m_RenderTextureFactory.Get(src);
		RenderTextureFactory_t1946967824 * L_20 = __this->get_m_RenderTextureFactory_8();
		RenderTexture_t2108887433 * L_21 = V_5;
		// var tempRT = m_RenderTextureFactory.Get(src);
		NullCheck(L_20);
		RenderTexture_t2108887433 * L_22 = RenderTextureFactory_Get_m169036867(L_20, L_21, /*hidden argument*/NULL);
		V_7 = L_22;
		// m_Taa.Render(src, tempRT);
		TaaComponent_t3791749658 * L_23 = __this->get_m_Taa_18();
		RenderTexture_t2108887433 * L_24 = V_5;
		RenderTexture_t2108887433 * L_25 = V_7;
		// m_Taa.Render(src, tempRT);
		NullCheck(L_23);
		TaaComponent_Render_m2638556758(L_23, L_24, L_25, /*hidden argument*/NULL);
		// src = tempRT;
		RenderTexture_t2108887433 * L_26 = V_7;
		V_5 = L_26;
	}

IL_00c2:
	{
		// Texture autoExposure = GraphicsUtils.whiteTexture;
		Texture2D_t3840446185 * L_27 = GraphicsUtils_get_whiteTexture_m2270091967(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = L_27;
		// if (m_EyeAdaptation.active)
		EyeAdaptationComponent_t3394805121 * L_28 = __this->get_m_EyeAdaptation_19();
		// if (m_EyeAdaptation.active)
		NullCheck(L_28);
		bool L_29 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_28);
		if (!L_29)
		{
			goto IL_00ee;
		}
	}
	{
		// uberActive = true;
		V_0 = (bool)1;
		// autoExposure = m_EyeAdaptation.Prepare(src, uberMaterial);
		EyeAdaptationComponent_t3394805121 * L_30 = __this->get_m_EyeAdaptation_19();
		RenderTexture_t2108887433 * L_31 = V_5;
		Material_t340375123 * L_32 = V_4;
		// autoExposure = m_EyeAdaptation.Prepare(src, uberMaterial);
		NullCheck(L_30);
		Texture_t3661962703 * L_33 = EyeAdaptationComponent_Prepare_m3960566531(L_30, L_31, L_32, /*hidden argument*/NULL);
		V_8 = L_33;
	}

IL_00ee:
	{
		// uberMaterial.SetTexture("_AutoExposure", autoExposure);
		Material_t340375123 * L_34 = V_4;
		Texture_t3661962703 * L_35 = V_8;
		// uberMaterial.SetTexture("_AutoExposure", autoExposure);
		NullCheck(L_34);
		Material_SetTexture_m1829349465(L_34, _stringLiteral3422572598, L_35, /*hidden argument*/NULL);
		// if (dofActive)
		bool L_36 = V_3;
		if (!L_36)
		{
			goto IL_0116;
		}
	}
	{
		// uberActive = true;
		V_0 = (bool)1;
		// m_DepthOfField.Prepare(src, uberMaterial, taaActive);
		DepthOfFieldComponent_t554756766 * L_37 = __this->get_m_DepthOfField_20();
		RenderTexture_t2108887433 * L_38 = V_5;
		Material_t340375123 * L_39 = V_4;
		bool L_40 = V_2;
		// m_DepthOfField.Prepare(src, uberMaterial, taaActive);
		NullCheck(L_37);
		DepthOfFieldComponent_Prepare_m1717418444(L_37, L_38, L_39, L_40, /*hidden argument*/NULL);
	}

IL_0116:
	{
		// if (m_Bloom.active)
		BloomComponent_t3791419130 * L_41 = __this->get_m_Bloom_21();
		// if (m_Bloom.active)
		NullCheck(L_41);
		bool L_42 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_41);
		if (!L_42)
		{
			goto IL_013b;
		}
	}
	{
		// uberActive = true;
		V_0 = (bool)1;
		// m_Bloom.Prepare(src, uberMaterial, autoExposure);
		BloomComponent_t3791419130 * L_43 = __this->get_m_Bloom_21();
		RenderTexture_t2108887433 * L_44 = V_5;
		Material_t340375123 * L_45 = V_4;
		Texture_t3661962703 * L_46 = V_8;
		// m_Bloom.Prepare(src, uberMaterial, autoExposure);
		NullCheck(L_43);
		BloomComponent_Prepare_m571914554(L_43, L_44, L_45, L_46, /*hidden argument*/NULL);
	}

IL_013b:
	{
		// uberActive |= TryPrepareUberImageEffect(m_ChromaticAberration, uberMaterial);
		bool L_47 = V_0;
		ChromaticAberrationComponent_t1647263118 * L_48 = __this->get_m_ChromaticAberration_22();
		Material_t340375123 * L_49 = V_4;
		// uberActive |= TryPrepareUberImageEffect(m_ChromaticAberration, uberMaterial);
		bool L_50 = PostProcessingBehaviour_TryPrepareUberImageEffect_TisChromaticAberrationModel_t3963399853_m1586502784(__this, L_48, L_49, /*hidden argument*/PostProcessingBehaviour_TryPrepareUberImageEffect_TisChromaticAberrationModel_t3963399853_m1586502784_RuntimeMethod_var);
		V_0 = (bool)((int32_t)((int32_t)L_47|(int32_t)L_50));
		// uberActive |= TryPrepareUberImageEffect(m_ColorGrading, uberMaterial);
		bool L_51 = V_0;
		ColorGradingComponent_t1715259467 * L_52 = __this->get_m_ColorGrading_23();
		Material_t340375123 * L_53 = V_4;
		// uberActive |= TryPrepareUberImageEffect(m_ColorGrading, uberMaterial);
		bool L_54 = PostProcessingBehaviour_TryPrepareUberImageEffect_TisColorGradingModel_t1448048181_m2160422038(__this, L_52, L_53, /*hidden argument*/PostProcessingBehaviour_TryPrepareUberImageEffect_TisColorGradingModel_t1448048181_m2160422038_RuntimeMethod_var);
		V_0 = (bool)((int32_t)((int32_t)L_51|(int32_t)L_54));
		// uberActive |= TryPrepareUberImageEffect(m_Vignette, uberMaterial);
		bool L_55 = V_0;
		VignetteComponent_t3243642943 * L_56 = __this->get_m_Vignette_26();
		Material_t340375123 * L_57 = V_4;
		// uberActive |= TryPrepareUberImageEffect(m_Vignette, uberMaterial);
		bool L_58 = PostProcessingBehaviour_TryPrepareUberImageEffect_TisVignetteModel_t2845517177_m1716742003(__this, L_56, L_57, /*hidden argument*/PostProcessingBehaviour_TryPrepareUberImageEffect_TisVignetteModel_t2845517177_m1716742003_RuntimeMethod_var);
		V_0 = (bool)((int32_t)((int32_t)L_55|(int32_t)L_58));
		// uberActive |= TryPrepareUberImageEffect(m_UserLut, uberMaterial);
		bool L_59 = V_0;
		UserLutComponent_t2843161776 * L_60 = __this->get_m_UserLut_24();
		Material_t340375123 * L_61 = V_4;
		// uberActive |= TryPrepareUberImageEffect(m_UserLut, uberMaterial);
		bool L_62 = PostProcessingBehaviour_TryPrepareUberImageEffect_TisUserLutModel_t1670108080_m2338498891(__this, L_60, L_61, /*hidden argument*/PostProcessingBehaviour_TryPrepareUberImageEffect_TisUserLutModel_t1670108080_m2338498891_RuntimeMethod_var);
		V_0 = (bool)((int32_t)((int32_t)L_59|(int32_t)L_62));
		// var fxaaMaterial = fxaaActive
		bool L_63 = V_1;
		if (!L_63)
		{
			goto IL_019a;
		}
	}
	{
		MaterialFactory_t2445948724 * L_64 = __this->get_m_MaterialFactory_7();
		// ? m_MaterialFactory.Get("Hidden/Post FX/FXAA")
		NullCheck(L_64);
		Material_t340375123 * L_65 = MaterialFactory_Get_m4113232693(L_64, _stringLiteral1699430022, /*hidden argument*/NULL);
		G_B20_0 = L_65;
		goto IL_019b;
	}

IL_019a:
	{
		G_B20_0 = ((Material_t340375123 *)(NULL));
	}

IL_019b:
	{
		V_9 = G_B20_0;
		// if (fxaaActive)
		bool L_66 = V_1;
		if (!L_66)
		{
			goto IL_0206;
		}
	}
	{
		// fxaaMaterial.shaderKeywords = null;
		Material_t340375123 * L_67 = V_9;
		// fxaaMaterial.shaderKeywords = null;
		NullCheck(L_67);
		Material_set_shaderKeywords_m4017377042(L_67, (StringU5BU5D_t1281789340*)(StringU5BU5D_t1281789340*)NULL, /*hidden argument*/NULL);
		// TryPrepareUberImageEffect(m_Grain, fxaaMaterial);
		GrainComponent_t866324317 * L_68 = __this->get_m_Grain_25();
		Material_t340375123 * L_69 = V_9;
		// TryPrepareUberImageEffect(m_Grain, fxaaMaterial);
		PostProcessingBehaviour_TryPrepareUberImageEffect_TisGrainModel_t1152882488_m3807764775(__this, L_68, L_69, /*hidden argument*/PostProcessingBehaviour_TryPrepareUberImageEffect_TisGrainModel_t1152882488_m3807764775_RuntimeMethod_var);
		// TryPrepareUberImageEffect(m_Dithering, fxaaMaterial);
		DitheringComponent_t277621267 * L_70 = __this->get_m_Dithering_27();
		Material_t340375123 * L_71 = V_9;
		// TryPrepareUberImageEffect(m_Dithering, fxaaMaterial);
		PostProcessingBehaviour_TryPrepareUberImageEffect_TisDitheringModel_t2429005396_m570759740(__this, L_70, L_71, /*hidden argument*/PostProcessingBehaviour_TryPrepareUberImageEffect_TisDitheringModel_t2429005396_m570759740_RuntimeMethod_var);
		// if (uberActive)
		bool L_72 = V_0;
		if (!L_72)
		{
			goto IL_01f1;
		}
	}
	{
		// var output = m_RenderTextureFactory.Get(src);
		RenderTextureFactory_t1946967824 * L_73 = __this->get_m_RenderTextureFactory_8();
		RenderTexture_t2108887433 * L_74 = V_5;
		// var output = m_RenderTextureFactory.Get(src);
		NullCheck(L_73);
		RenderTexture_t2108887433 * L_75 = RenderTextureFactory_Get_m169036867(L_73, L_74, /*hidden argument*/NULL);
		V_10 = L_75;
		// Graphics.Blit(src, output, uberMaterial, 0);
		RenderTexture_t2108887433 * L_76 = V_5;
		RenderTexture_t2108887433 * L_77 = V_10;
		Material_t340375123 * L_78 = V_4;
		// Graphics.Blit(src, output, uberMaterial, 0);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m4126770912(NULL /*static, unused*/, L_76, L_77, L_78, 0, /*hidden argument*/NULL);
		// src = output;
		RenderTexture_t2108887433 * L_79 = V_10;
		V_5 = L_79;
	}

IL_01f1:
	{
		// m_Fxaa.Render(src, dst);
		FxaaComponent_t1312385771 * L_80 = __this->get_m_Fxaa_28();
		RenderTexture_t2108887433 * L_81 = V_5;
		RenderTexture_t2108887433 * L_82 = V_6;
		// m_Fxaa.Render(src, dst);
		NullCheck(L_80);
		FxaaComponent_Render_m1006683150(L_80, L_81, L_82, /*hidden argument*/NULL);
		goto IL_0254;
	}

IL_0206:
	{
		// uberActive |= TryPrepareUberImageEffect(m_Grain, uberMaterial);
		bool L_83 = V_0;
		GrainComponent_t866324317 * L_84 = __this->get_m_Grain_25();
		Material_t340375123 * L_85 = V_4;
		// uberActive |= TryPrepareUberImageEffect(m_Grain, uberMaterial);
		bool L_86 = PostProcessingBehaviour_TryPrepareUberImageEffect_TisGrainModel_t1152882488_m3807764775(__this, L_84, L_85, /*hidden argument*/PostProcessingBehaviour_TryPrepareUberImageEffect_TisGrainModel_t1152882488_m3807764775_RuntimeMethod_var);
		V_0 = (bool)((int32_t)((int32_t)L_83|(int32_t)L_86));
		// uberActive |= TryPrepareUberImageEffect(m_Dithering, uberMaterial);
		bool L_87 = V_0;
		DitheringComponent_t277621267 * L_88 = __this->get_m_Dithering_27();
		Material_t340375123 * L_89 = V_4;
		// uberActive |= TryPrepareUberImageEffect(m_Dithering, uberMaterial);
		bool L_90 = PostProcessingBehaviour_TryPrepareUberImageEffect_TisDitheringModel_t2429005396_m570759740(__this, L_88, L_89, /*hidden argument*/PostProcessingBehaviour_TryPrepareUberImageEffect_TisDitheringModel_t2429005396_m570759740_RuntimeMethod_var);
		V_0 = (bool)((int32_t)((int32_t)L_87|(int32_t)L_90));
		// if (uberActive)
		bool L_91 = V_0;
		if (!L_91)
		{
			goto IL_0253;
		}
	}
	{
		// if (!GraphicsUtils.isLinearColorSpace)
		bool L_92 = GraphicsUtils_get_isLinearColorSpace_m2783177889(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_92)
		{
			goto IL_0246;
		}
	}
	{
		// uberMaterial.EnableKeyword("UNITY_COLORSPACE_GAMMA");
		Material_t340375123 * L_93 = V_4;
		// uberMaterial.EnableKeyword("UNITY_COLORSPACE_GAMMA");
		NullCheck(L_93);
		Material_EnableKeyword_m329692301(L_93, _stringLiteral2853687220, /*hidden argument*/NULL);
	}

IL_0246:
	{
		// Graphics.Blit(src, dst, uberMaterial, 0);
		RenderTexture_t2108887433 * L_94 = V_5;
		RenderTexture_t2108887433 * L_95 = V_6;
		Material_t340375123 * L_96 = V_4;
		// Graphics.Blit(src, dst, uberMaterial, 0);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m4126770912(NULL /*static, unused*/, L_94, L_95, L_96, 0, /*hidden argument*/NULL);
	}

IL_0253:
	{
	}

IL_0254:
	{
		// if (!uberActive && !fxaaActive)
		bool L_97 = V_0;
		if (L_97)
		{
			goto IL_0269;
		}
	}
	{
		bool L_98 = V_1;
		if (L_98)
		{
			goto IL_0269;
		}
	}
	{
		// Graphics.Blit(src, dst);
		RenderTexture_t2108887433 * L_99 = V_5;
		RenderTexture_t2108887433 * L_100 = V_6;
		// Graphics.Blit(src, dst);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m1336850842(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
	}

IL_0269:
	{
		// m_RenderTextureFactory.ReleaseAll();
		RenderTextureFactory_t1946967824 * L_101 = __this->get_m_RenderTextureFactory_8();
		// m_RenderTextureFactory.ReleaseAll();
		NullCheck(L_101);
		RenderTextureFactory_ReleaseAll_m3329667721(L_101, /*hidden argument*/NULL);
	}

IL_0274:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnGUI()
extern "C"  void PostProcessingBehaviour_OnGUI_m2377759815 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_OnGUI_m2377759815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Event.current.type != EventType.Repaint)
		Event_t2956885303 * L_0 = Event_get_current_m2393892120(NULL /*static, unused*/, /*hidden argument*/NULL);
		// if (Event.current.type != EventType.Repaint)
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m1370041809(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_0016;
		}
	}
	{
		// return;
		goto IL_00db;
	}

IL_0016:
	{
		// if (profile == null || m_Camera == null)
		PostProcessingProfile_t724195375 * L_2 = __this->get_profile_2();
		// if (profile == null || m_Camera == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		Camera_t4157153871 * L_4 = __this->get_m_Camera_10();
		// if (profile == null || m_Camera == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003d;
		}
	}

IL_0038:
	{
		// return;
		goto IL_00db;
	}

IL_003d:
	{
		// if (m_EyeAdaptation.active && profile.debugViews.IsModeActive(DebugMode.EyeAdaptation))
		EyeAdaptationComponent_t3394805121 * L_6 = __this->get_m_EyeAdaptation_19();
		// if (m_EyeAdaptation.active && profile.debugViews.IsModeActive(DebugMode.EyeAdaptation))
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_6);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		PostProcessingProfile_t724195375 * L_8 = __this->get_profile_2();
		NullCheck(L_8);
		BuiltinDebugViewsModel_t1462618840 * L_9 = L_8->get_debugViews_2();
		// if (m_EyeAdaptation.active && profile.debugViews.IsModeActive(DebugMode.EyeAdaptation))
		NullCheck(L_9);
		bool L_10 = BuiltinDebugViewsModel_IsModeActive_m1972979357(L_9, 5, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		// m_EyeAdaptation.OnGUI();
		EyeAdaptationComponent_t3394805121 * L_11 = __this->get_m_EyeAdaptation_19();
		// m_EyeAdaptation.OnGUI();
		NullCheck(L_11);
		EyeAdaptationComponent_OnGUI_m1617202736(L_11, /*hidden argument*/NULL);
		goto IL_00db;
	}

IL_0073:
	{
		// else if (m_ColorGrading.active && profile.debugViews.IsModeActive(DebugMode.LogLut))
		ColorGradingComponent_t1715259467 * L_12 = __this->get_m_ColorGrading_23();
		// else if (m_ColorGrading.active && profile.debugViews.IsModeActive(DebugMode.LogLut))
		NullCheck(L_12);
		bool L_13 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_12);
		if (!L_13)
		{
			goto IL_00a9;
		}
	}
	{
		PostProcessingProfile_t724195375 * L_14 = __this->get_profile_2();
		NullCheck(L_14);
		BuiltinDebugViewsModel_t1462618840 * L_15 = L_14->get_debugViews_2();
		// else if (m_ColorGrading.active && profile.debugViews.IsModeActive(DebugMode.LogLut))
		NullCheck(L_15);
		bool L_16 = BuiltinDebugViewsModel_IsModeActive_m1972979357(L_15, 8, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a9;
		}
	}
	{
		// m_ColorGrading.OnGUI();
		ColorGradingComponent_t1715259467 * L_17 = __this->get_m_ColorGrading_23();
		// m_ColorGrading.OnGUI();
		NullCheck(L_17);
		ColorGradingComponent_OnGUI_m4238095531(L_17, /*hidden argument*/NULL);
		goto IL_00db;
	}

IL_00a9:
	{
		// else if (m_UserLut.active && profile.debugViews.IsModeActive(DebugMode.UserLut))
		UserLutComponent_t2843161776 * L_18 = __this->get_m_UserLut_24();
		// else if (m_UserLut.active && profile.debugViews.IsModeActive(DebugMode.UserLut))
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active() */, L_18);
		if (!L_19)
		{
			goto IL_00db;
		}
	}
	{
		PostProcessingProfile_t724195375 * L_20 = __this->get_profile_2();
		NullCheck(L_20);
		BuiltinDebugViewsModel_t1462618840 * L_21 = L_20->get_debugViews_2();
		// else if (m_UserLut.active && profile.debugViews.IsModeActive(DebugMode.UserLut))
		NullCheck(L_21);
		bool L_22 = BuiltinDebugViewsModel_IsModeActive_m1972979357(L_21, ((int32_t)9), /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00db;
		}
	}
	{
		// m_UserLut.OnGUI();
		UserLutComponent_t2843161776 * L_23 = __this->get_m_UserLut_24();
		// m_UserLut.OnGUI();
		NullCheck(L_23);
		UserLutComponent_OnGUI_m2614562252(L_23, /*hidden argument*/NULL);
	}

IL_00db:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnDisable()
extern "C"  void PostProcessingBehaviour_OnDisable_m3454750292 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_OnDisable_m3454750292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t3423445140  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t2141718565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// foreach (var cb in m_CommandBuffers.Values)
		Dictionary_2_t1572824908 * L_0 = __this->get_m_CommandBuffers_4();
		// foreach (var cb in m_CommandBuffers.Values)
		NullCheck(L_0);
		ValueCollection_t3288869226 * L_1 = Dictionary_2_get_Values_m907585786(L_0, /*hidden argument*/Dictionary_2_get_Values_m907585786_RuntimeMethod_var);
		// foreach (var cb in m_CommandBuffers.Values)
		NullCheck(L_1);
		Enumerator_t2141718565  L_2 = ValueCollection_GetEnumerator_m141340968(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m141340968_RuntimeMethod_var);
		V_1 = L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0047;
		}

IL_0018:
		{
			// foreach (var cb in m_CommandBuffers.Values)
			// foreach (var cb in m_CommandBuffers.Values)
			KeyValuePair_2_t3423445140  L_3 = Enumerator_get_Current_m1441963613((&V_1), /*hidden argument*/Enumerator_get_Current_m1441963613_RuntimeMethod_var);
			V_0 = L_3;
			// m_Camera.RemoveCommandBuffer(cb.Key, cb.Value);
			Camera_t4157153871 * L_4 = __this->get_m_Camera_10();
			// m_Camera.RemoveCommandBuffer(cb.Key, cb.Value);
			int32_t L_5 = KeyValuePair_2_get_Key_m648688154((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m648688154_RuntimeMethod_var);
			// m_Camera.RemoveCommandBuffer(cb.Key, cb.Value);
			CommandBuffer_t2206337031 * L_6 = KeyValuePair_2_get_Value_m1482434896((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1482434896_RuntimeMethod_var);
			// m_Camera.RemoveCommandBuffer(cb.Key, cb.Value);
			NullCheck(L_4);
			Camera_RemoveCommandBuffer_m773243127(L_4, L_5, L_6, /*hidden argument*/NULL);
			// cb.Value.Dispose();
			// cb.Value.Dispose();
			CommandBuffer_t2206337031 * L_7 = KeyValuePair_2_get_Value_m1482434896((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1482434896_RuntimeMethod_var);
			// cb.Value.Dispose();
			NullCheck(L_7);
			CommandBuffer_Dispose_m146760806(L_7, /*hidden argument*/NULL);
		}

IL_0047:
		{
			// foreach (var cb in m_CommandBuffers.Values)
			bool L_8 = Enumerator_MoveNext_m1047154078((&V_1), /*hidden argument*/Enumerator_MoveNext_m1047154078_RuntimeMethod_var);
			if (L_8)
			{
				goto IL_0018;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x66, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		// foreach (var cb in m_CommandBuffers.Values)
		Enumerator_Dispose_m3868019798((&V_1), /*hidden argument*/Enumerator_Dispose_m3868019798_RuntimeMethod_var);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0066:
	{
		// m_CommandBuffers.Clear();
		Dictionary_2_t1572824908 * L_9 = __this->get_m_CommandBuffers_4();
		// m_CommandBuffers.Clear();
		NullCheck(L_9);
		Dictionary_2_Clear_m3961351021(L_9, /*hidden argument*/Dictionary_2_Clear_m3961351021_RuntimeMethod_var);
		// if (profile != null)
		PostProcessingProfile_t724195375 * L_10 = __this->get_profile_2();
		// if (profile != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_10, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0088;
		}
	}
	{
		// DisableComponents();
		// DisableComponents();
		PostProcessingBehaviour_DisableComponents_m1054770722(__this, /*hidden argument*/NULL);
	}

IL_0088:
	{
		// m_Components.Clear();
		List_1_t4203178569 * L_12 = __this->get_m_Components_5();
		// m_Components.Clear();
		NullCheck(L_12);
		List_1_Clear_m1773456631(L_12, /*hidden argument*/List_1_Clear_m1773456631_RuntimeMethod_var);
		// if (m_Camera != null)
		Camera_t4157153871 * L_13 = __this->get_m_Camera_10();
		// if (m_Camera != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_13, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b0;
		}
	}
	{
		// m_Camera.depthTextureMode = DepthTextureMode.None;
		Camera_t4157153871 * L_15 = __this->get_m_Camera_10();
		// m_Camera.depthTextureMode = DepthTextureMode.None;
		NullCheck(L_15);
		Camera_set_depthTextureMode_m754977860(L_15, 0, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		// m_MaterialFactory.Dispose();
		MaterialFactory_t2445948724 * L_16 = __this->get_m_MaterialFactory_7();
		// m_MaterialFactory.Dispose();
		NullCheck(L_16);
		MaterialFactory_Dispose_m620032304(L_16, /*hidden argument*/NULL);
		// m_RenderTextureFactory.Dispose();
		RenderTextureFactory_t1946967824 * L_17 = __this->get_m_RenderTextureFactory_8();
		// m_RenderTextureFactory.Dispose();
		NullCheck(L_17);
		RenderTextureFactory_Dispose_m2714960054(L_17, /*hidden argument*/NULL);
		// GraphicsUtils.Dispose();
		GraphicsUtils_Dispose_m791740987(NULL /*static, unused*/, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::ResetTemporalEffects()
extern "C"  void PostProcessingBehaviour_ResetTemporalEffects_m1478453056 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	{
		// m_Taa.ResetHistory();
		TaaComponent_t3791749658 * L_0 = __this->get_m_Taa_18();
		// m_Taa.ResetHistory();
		NullCheck(L_0);
		TaaComponent_ResetHistory_m3846253241(L_0, /*hidden argument*/NULL);
		// m_MotionBlur.ResetHistory();
		MotionBlurComponent_t3686516877 * L_1 = __this->get_m_MotionBlur_17();
		// m_MotionBlur.ResetHistory();
		NullCheck(L_1);
		MotionBlurComponent_ResetHistory_m1016490533(L_1, /*hidden argument*/NULL);
		// m_EyeAdaptation.ResetHistory();
		EyeAdaptationComponent_t3394805121 * L_2 = __this->get_m_EyeAdaptation_19();
		// m_EyeAdaptation.ResetHistory();
		NullCheck(L_2);
		EyeAdaptationComponent_ResetHistory_m3423621609(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::CheckObservers()
extern "C"  void PostProcessingBehaviour_CheckObservers_m3285018848 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_CheckObservers_m3285018848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1198401749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t754912357  V_1;
	memset(&V_1, 0, sizeof(V_1));
	PostProcessingComponentBase_t2731103827 * V_2 = NULL;
	bool V_3 = false;
	int32_t V_4 = 0;
	PostProcessingComponentBase_t2731103827 * V_5 = NULL;
	int32_t V_6 = 0;
	PostProcessingComponentBase_t2731103827 * V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// foreach (var cs in m_ComponentStates)
		Dictionary_2_t3095696878 * L_0 = __this->get_m_ComponentStates_6();
		// foreach (var cs in m_ComponentStates)
		NullCheck(L_0);
		Enumerator_t754912357  L_1 = Dictionary_2_GetEnumerator_m2066271542(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2066271542_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0063;
		}

IL_0013:
		{
			// foreach (var cs in m_ComponentStates)
			// foreach (var cs in m_ComponentStates)
			KeyValuePair_2_t1198401749  L_2 = Enumerator_get_Current_m3820558532((&V_1), /*hidden argument*/Enumerator_get_Current_m3820558532_RuntimeMethod_var);
			V_0 = L_2;
			// var component = cs.Key;
			// var component = cs.Key;
			PostProcessingComponentBase_t2731103827 * L_3 = KeyValuePair_2_get_Key_m1176910999((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1176910999_RuntimeMethod_var);
			V_2 = L_3;
			// var state = component.GetModel().enabled;
			PostProcessingComponentBase_t2731103827 * L_4 = V_2;
			// var state = component.GetModel().enabled;
			NullCheck(L_4);
			PostProcessingModel_t540111976 * L_5 = VirtFuncInvoker0< PostProcessingModel_t540111976 * >::Invoke(8 /* UnityEngine.PostProcessing.PostProcessingModel UnityEngine.PostProcessing.PostProcessingComponentBase::GetModel() */, L_4);
			// var state = component.GetModel().enabled;
			NullCheck(L_5);
			bool L_6 = PostProcessingModel_get_enabled_m2892084724(L_5, /*hidden argument*/NULL);
			V_3 = L_6;
			// if (state != cs.Value)
			bool L_7 = V_3;
			// if (state != cs.Value)
			bool L_8 = KeyValuePair_2_get_Value_m3146329666((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3146329666_RuntimeMethod_var);
			if ((((int32_t)L_7) == ((int32_t)L_8)))
			{
				goto IL_0062;
			}
		}

IL_003d:
		{
			// if (state) m_ComponentsToEnable.Add(component);
			bool L_9 = V_3;
			if (!L_9)
			{
				goto IL_0055;
			}
		}

IL_0044:
		{
			// if (state) m_ComponentsToEnable.Add(component);
			List_1_t4203178569 * L_10 = __this->get_m_ComponentsToEnable_29();
			PostProcessingComponentBase_t2731103827 * L_11 = V_2;
			// if (state) m_ComponentsToEnable.Add(component);
			NullCheck(L_10);
			List_1_Add_m1572460420(L_10, L_11, /*hidden argument*/List_1_Add_m1572460420_RuntimeMethod_var);
			goto IL_0061;
		}

IL_0055:
		{
			// else m_ComponentsToDisable.Add(component);
			List_1_t4203178569 * L_12 = __this->get_m_ComponentsToDisable_30();
			PostProcessingComponentBase_t2731103827 * L_13 = V_2;
			// else m_ComponentsToDisable.Add(component);
			NullCheck(L_12);
			List_1_Add_m1572460420(L_12, L_13, /*hidden argument*/List_1_Add_m1572460420_RuntimeMethod_var);
		}

IL_0061:
		{
		}

IL_0062:
		{
		}

IL_0063:
		{
			// foreach (var cs in m_ComponentStates)
			bool L_14 = Enumerator_MoveNext_m3116365360((&V_1), /*hidden argument*/Enumerator_MoveNext_m3116365360_RuntimeMethod_var);
			if (L_14)
			{
				goto IL_0013;
			}
		}

IL_006f:
		{
			IL2CPP_LEAVE(0x82, FINALLY_0074);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0074;
	}

FINALLY_0074:
	{ // begin finally (depth: 1)
		// foreach (var cs in m_ComponentStates)
		Enumerator_Dispose_m2110019143((&V_1), /*hidden argument*/Enumerator_Dispose_m2110019143_RuntimeMethod_var);
		IL2CPP_END_FINALLY(116)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(116)
	{
		IL2CPP_JUMP_TBL(0x82, IL_0082)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0082:
	{
		// for (int i = 0; i < m_ComponentsToDisable.Count; i++)
		V_4 = 0;
		goto IL_00b6;
	}

IL_008a:
	{
		// var c = m_ComponentsToDisable[i];
		List_1_t4203178569 * L_15 = __this->get_m_ComponentsToDisable_30();
		int32_t L_16 = V_4;
		// var c = m_ComponentsToDisable[i];
		NullCheck(L_15);
		PostProcessingComponentBase_t2731103827 * L_17 = List_1_get_Item_m1448434680(L_15, L_16, /*hidden argument*/List_1_get_Item_m1448434680_RuntimeMethod_var);
		V_5 = L_17;
		// m_ComponentStates[c] = false;
		Dictionary_2_t3095696878 * L_18 = __this->get_m_ComponentStates_6();
		PostProcessingComponentBase_t2731103827 * L_19 = V_5;
		// m_ComponentStates[c] = false;
		NullCheck(L_18);
		Dictionary_2_set_Item_m1186219162(L_18, L_19, (bool)0, /*hidden argument*/Dictionary_2_set_Item_m1186219162_RuntimeMethod_var);
		// c.OnDisable();
		PostProcessingComponentBase_t2731103827 * L_20 = V_5;
		// c.OnDisable();
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(7 /* System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::OnDisable() */, L_20);
		// for (int i = 0; i < m_ComponentsToDisable.Count; i++)
		int32_t L_21 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_00b6:
	{
		// for (int i = 0; i < m_ComponentsToDisable.Count; i++)
		int32_t L_22 = V_4;
		List_1_t4203178569 * L_23 = __this->get_m_ComponentsToDisable_30();
		// for (int i = 0; i < m_ComponentsToDisable.Count; i++)
		NullCheck(L_23);
		int32_t L_24 = List_1_get_Count_m728013831(L_23, /*hidden argument*/List_1_get_Count_m728013831_RuntimeMethod_var);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_008a;
		}
	}
	{
		// for (int i = 0; i < m_ComponentsToEnable.Count; i++)
		V_6 = 0;
		goto IL_00fc;
	}

IL_00d0:
	{
		// var c = m_ComponentsToEnable[i];
		List_1_t4203178569 * L_25 = __this->get_m_ComponentsToEnable_29();
		int32_t L_26 = V_6;
		// var c = m_ComponentsToEnable[i];
		NullCheck(L_25);
		PostProcessingComponentBase_t2731103827 * L_27 = List_1_get_Item_m1448434680(L_25, L_26, /*hidden argument*/List_1_get_Item_m1448434680_RuntimeMethod_var);
		V_7 = L_27;
		// m_ComponentStates[c] = true;
		Dictionary_2_t3095696878 * L_28 = __this->get_m_ComponentStates_6();
		PostProcessingComponentBase_t2731103827 * L_29 = V_7;
		// m_ComponentStates[c] = true;
		NullCheck(L_28);
		Dictionary_2_set_Item_m1186219162(L_28, L_29, (bool)1, /*hidden argument*/Dictionary_2_set_Item_m1186219162_RuntimeMethod_var);
		// c.OnEnable();
		PostProcessingComponentBase_t2731103827 * L_30 = V_7;
		// c.OnEnable();
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::OnEnable() */, L_30);
		// for (int i = 0; i < m_ComponentsToEnable.Count; i++)
		int32_t L_31 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_00fc:
	{
		// for (int i = 0; i < m_ComponentsToEnable.Count; i++)
		int32_t L_32 = V_6;
		List_1_t4203178569 * L_33 = __this->get_m_ComponentsToEnable_29();
		// for (int i = 0; i < m_ComponentsToEnable.Count; i++)
		NullCheck(L_33);
		int32_t L_34 = List_1_get_Count_m728013831(L_33, /*hidden argument*/List_1_get_Count_m728013831_RuntimeMethod_var);
		if ((((int32_t)L_32) < ((int32_t)L_34)))
		{
			goto IL_00d0;
		}
	}
	{
		// m_ComponentsToDisable.Clear();
		List_1_t4203178569 * L_35 = __this->get_m_ComponentsToDisable_30();
		// m_ComponentsToDisable.Clear();
		NullCheck(L_35);
		List_1_Clear_m1773456631(L_35, /*hidden argument*/List_1_Clear_m1773456631_RuntimeMethod_var);
		// m_ComponentsToEnable.Clear();
		List_1_t4203178569 * L_36 = __this->get_m_ComponentsToEnable_29();
		// m_ComponentsToEnable.Clear();
		NullCheck(L_36);
		List_1_Clear_m1773456631(L_36, /*hidden argument*/List_1_Clear_m1773456631_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::DisableComponents()
extern "C"  void PostProcessingBehaviour_DisableComponents_m1054770722 (PostProcessingBehaviour_t3229946336 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingBehaviour_DisableComponents_m1054770722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PostProcessingComponentBase_t2731103827 * V_0 = NULL;
	Enumerator_t1797455150  V_1;
	memset(&V_1, 0, sizeof(V_1));
	PostProcessingModel_t540111976 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// foreach (var component in m_Components)
		List_1_t4203178569 * L_0 = __this->get_m_Components_5();
		// foreach (var component in m_Components)
		NullCheck(L_0);
		Enumerator_t1797455150  L_1 = List_1_GetEnumerator_m659292003(L_0, /*hidden argument*/List_1_GetEnumerator_m659292003_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003b;
		}

IL_0013:
		{
			// foreach (var component in m_Components)
			// foreach (var component in m_Components)
			PostProcessingComponentBase_t2731103827 * L_2 = Enumerator_get_Current_m747737324((&V_1), /*hidden argument*/Enumerator_get_Current_m747737324_RuntimeMethod_var);
			V_0 = L_2;
			// var model = component.GetModel();
			PostProcessingComponentBase_t2731103827 * L_3 = V_0;
			// var model = component.GetModel();
			NullCheck(L_3);
			PostProcessingModel_t540111976 * L_4 = VirtFuncInvoker0< PostProcessingModel_t540111976 * >::Invoke(8 /* UnityEngine.PostProcessing.PostProcessingModel UnityEngine.PostProcessing.PostProcessingComponentBase::GetModel() */, L_3);
			V_2 = L_4;
			// if (model != null && model.enabled)
			PostProcessingModel_t540111976 * L_5 = V_2;
			if (!L_5)
			{
				goto IL_003a;
			}
		}

IL_0029:
		{
			PostProcessingModel_t540111976 * L_6 = V_2;
			// if (model != null && model.enabled)
			NullCheck(L_6);
			bool L_7 = PostProcessingModel_get_enabled_m2892084724(L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003a;
			}
		}

IL_0034:
		{
			// component.OnDisable();
			PostProcessingComponentBase_t2731103827 * L_8 = V_0;
			// component.OnDisable();
			NullCheck(L_8);
			VirtActionInvoker0::Invoke(7 /* System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::OnDisable() */, L_8);
		}

IL_003a:
		{
		}

IL_003b:
		{
			// foreach (var component in m_Components)
			bool L_9 = Enumerator_MoveNext_m425563657((&V_1), /*hidden argument*/Enumerator_MoveNext_m425563657_RuntimeMethod_var);
			if (L_9)
			{
				goto IL_0013;
			}
		}

IL_0047:
		{
			IL2CPP_LEAVE(0x5A, FINALLY_004c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004c;
	}

FINALLY_004c:
	{ // begin finally (depth: 1)
		// foreach (var component in m_Components)
		Enumerator_Dispose_m2340450683((&V_1), /*hidden argument*/Enumerator_Dispose_m2340450683_RuntimeMethod_var);
		IL2CPP_END_FINALLY(76)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(76)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005a:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::.ctor()
extern "C"  void PostProcessingComponentBase__ctor_m3239609128 (PostProcessingComponentBase_t2731103827 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.DepthTextureMode UnityEngine.PostProcessing.PostProcessingComponentBase::GetCameraFlags()
extern "C"  int32_t PostProcessingComponentBase_GetCameraFlags_m3128948865 (PostProcessingComponentBase_t2731103827 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return DepthTextureMode.None;
		V_0 = 0;
		goto IL_0008;
	}

IL_0008:
	{
		// }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::OnEnable()
extern "C"  void PostProcessingComponentBase_OnEnable_m2417868870 (PostProcessingComponentBase_t2731103827 * __this, const RuntimeMethod* method)
{
	{
		// {}
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::OnDisable()
extern "C"  void PostProcessingComponentBase_OnDisable_m3565405166 (PostProcessingComponentBase_t2731103827 * __this, const RuntimeMethod* method)
{
	{
		// {}
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.PostProcessingContext::.ctor()
extern "C"  void PostProcessingContext__ctor_m1285766435 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_interrupted()
extern "C"  bool PostProcessingContext_get_interrupted_m1809095682 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// public bool interrupted { get; private set; }
		bool L_0 = __this->get_U3CinterruptedU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingContext::set_interrupted(System.Boolean)
extern "C"  void PostProcessingContext_set_interrupted_m4192460866 (PostProcessingContext_t2014408948 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool interrupted { get; private set; }
		bool L_0 = ___value0;
		__this->set_U3CinterruptedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingContext::Interrupt()
extern "C"  void PostProcessingContext_Interrupt_m4183521937 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	{
		// interrupted = true;
		// interrupted = true;
		PostProcessingContext_set_interrupted_m4192460866(__this, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingContext::Reset()
extern "C"  PostProcessingContext_t2014408948 * PostProcessingContext_Reset_m4133896387 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	PostProcessingContext_t2014408948 * V_0 = NULL;
	{
		// profile = null;
		__this->set_profile_0((PostProcessingProfile_t724195375 *)NULL);
		// camera = null;
		__this->set_camera_1((Camera_t4157153871 *)NULL);
		// materialFactory = null;
		__this->set_materialFactory_2((MaterialFactory_t2445948724 *)NULL);
		// renderTextureFactory = null;
		__this->set_renderTextureFactory_3((RenderTextureFactory_t1946967824 *)NULL);
		// interrupted = false;
		// interrupted = false;
		PostProcessingContext_set_interrupted_m4192460866(__this, (bool)0, /*hidden argument*/NULL);
		// return this;
		V_0 = __this;
		goto IL_002b;
	}

IL_002b:
	{
		// }
		PostProcessingContext_t2014408948 * L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_isGBufferAvailable()
extern "C"  bool PostProcessingContext_get_isGBufferAvailable_m949646721 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return camera.actualRenderingPath == RenderingPath.DeferredShading; }
		Camera_t4157153871 * L_0 = __this->get_camera_1();
		// get { return camera.actualRenderingPath == RenderingPath.DeferredShading; }
		NullCheck(L_0);
		int32_t L_1 = Camera_get_actualRenderingPath_m423069678(L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)3))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		// get { return camera.actualRenderingPath == RenderingPath.DeferredShading; }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_isHdr()
extern "C"  bool PostProcessingContext_get_isHdr_m3057655858 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return camera.allowHDR; }
		Camera_t4157153871 * L_0 = __this->get_camera_1();
		// get { return camera.allowHDR; }
		NullCheck(L_0);
		bool L_1 = Camera_get_allowHDR_m2615180899(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// get { return camera.allowHDR; }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.PostProcessing.PostProcessingContext::get_width()
extern "C"  int32_t PostProcessingContext_get_width_m2658937703 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// get { return camera.pixelWidth; }
		Camera_t4157153871 * L_0 = __this->get_camera_1();
		// get { return camera.pixelWidth; }
		NullCheck(L_0);
		int32_t L_1 = Camera_get_pixelWidth_m1110053668(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// get { return camera.pixelWidth; }
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.PostProcessing.PostProcessingContext::get_height()
extern "C"  int32_t PostProcessingContext_get_height_m4218042885 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// get { return camera.pixelHeight; }
		Camera_t4157153871 * L_0 = __this->get_camera_1();
		// get { return camera.pixelHeight; }
		NullCheck(L_0);
		int32_t L_1 = Camera_get_pixelHeight_m722276884(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// get { return camera.pixelHeight; }
		int32_t L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Rect UnityEngine.PostProcessing.PostProcessingContext::get_viewport()
extern "C"  Rect_t2360479859  PostProcessingContext_get_viewport_m2647794360 (PostProcessingContext_t2014408948 * __this, const RuntimeMethod* method)
{
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// get { return camera.rect; } // Normalized coordinates
		Camera_t4157153871 * L_0 = __this->get_camera_1();
		// get { return camera.rect; } // Normalized coordinates
		NullCheck(L_0);
		Rect_t2360479859  L_1 = Camera_get_rect_m2458154151(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		// get { return camera.rect; } // Normalized coordinates
		Rect_t2360479859  L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.PostProcessingModel::.ctor()
extern "C"  void PostProcessingModel__ctor_m4158388095 (PostProcessingModel_t540111976 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PostProcessing.PostProcessingModel::get_enabled()
extern "C"  bool PostProcessingModel_get_enabled_m2892084724 (PostProcessingModel_t540111976 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return m_Enabled; }
		bool L_0 = __this->get_m_Enabled_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_Enabled; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingModel::set_enabled(System.Boolean)
extern "C"  void PostProcessingModel_set_enabled_m3195575587 (PostProcessingModel_t540111976 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// m_Enabled = value;
		bool L_0 = ___value0;
		__this->set_m_Enabled_0(L_0);
		// if (value)
		bool L_1 = ___value0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// OnValidate();
		// OnValidate();
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.PostProcessing.PostProcessingModel::OnValidate() */, __this);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.PostProcessingModel::OnValidate()
extern "C"  void PostProcessingModel_OnValidate_m3345216803 (PostProcessingModel_t540111976 * __this, const RuntimeMethod* method)
{
	{
		// {}
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.PostProcessingProfile::.ctor()
extern "C"  void PostProcessingProfile__ctor_m2477575403 (PostProcessingProfile_t724195375 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostProcessingProfile__ctor_m2477575403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public BuiltinDebugViewsModel debugViews = new BuiltinDebugViewsModel();
		BuiltinDebugViewsModel_t1462618840 * L_0 = (BuiltinDebugViewsModel_t1462618840 *)il2cpp_codegen_object_new(BuiltinDebugViewsModel_t1462618840_il2cpp_TypeInfo_var);
		BuiltinDebugViewsModel__ctor_m2849271374(L_0, /*hidden argument*/NULL);
		__this->set_debugViews_2(L_0);
		// public FogModel fog = new FogModel();
		FogModel_t3620688749 * L_1 = (FogModel_t3620688749 *)il2cpp_codegen_object_new(FogModel_t3620688749_il2cpp_TypeInfo_var);
		FogModel__ctor_m849674861(L_1, /*hidden argument*/NULL);
		__this->set_fog_3(L_1);
		// public AntialiasingModel antialiasing = new AntialiasingModel();
		AntialiasingModel_t1521139388 * L_2 = (AntialiasingModel_t1521139388 *)il2cpp_codegen_object_new(AntialiasingModel_t1521139388_il2cpp_TypeInfo_var);
		AntialiasingModel__ctor_m407834341(L_2, /*hidden argument*/NULL);
		__this->set_antialiasing_4(L_2);
		// public AmbientOcclusionModel ambientOcclusion = new AmbientOcclusionModel();
		AmbientOcclusionModel_t389471066 * L_3 = (AmbientOcclusionModel_t389471066 *)il2cpp_codegen_object_new(AmbientOcclusionModel_t389471066_il2cpp_TypeInfo_var);
		AmbientOcclusionModel__ctor_m1631144053(L_3, /*hidden argument*/NULL);
		__this->set_ambientOcclusion_5(L_3);
		// public ScreenSpaceReflectionModel screenSpaceReflection = new ScreenSpaceReflectionModel();
		ScreenSpaceReflectionModel_t3026344732 * L_4 = (ScreenSpaceReflectionModel_t3026344732 *)il2cpp_codegen_object_new(ScreenSpaceReflectionModel_t3026344732_il2cpp_TypeInfo_var);
		ScreenSpaceReflectionModel__ctor_m855607741(L_4, /*hidden argument*/NULL);
		__this->set_screenSpaceReflection_6(L_4);
		// public DepthOfFieldModel depthOfField = new DepthOfFieldModel();
		DepthOfFieldModel_t514067330 * L_5 = (DepthOfFieldModel_t514067330 *)il2cpp_codegen_object_new(DepthOfFieldModel_t514067330_il2cpp_TypeInfo_var);
		DepthOfFieldModel__ctor_m2227976965(L_5, /*hidden argument*/NULL);
		__this->set_depthOfField_7(L_5);
		// public MotionBlurModel motionBlur = new MotionBlurModel();
		MotionBlurModel_t3080286123 * L_6 = (MotionBlurModel_t3080286123 *)il2cpp_codegen_object_new(MotionBlurModel_t3080286123_il2cpp_TypeInfo_var);
		MotionBlurModel__ctor_m428522222(L_6, /*hidden argument*/NULL);
		__this->set_motionBlur_8(L_6);
		// public EyeAdaptationModel eyeAdaptation = new EyeAdaptationModel();
		EyeAdaptationModel_t242823912 * L_7 = (EyeAdaptationModel_t242823912 *)il2cpp_codegen_object_new(EyeAdaptationModel_t242823912_il2cpp_TypeInfo_var);
		EyeAdaptationModel__ctor_m1238644491(L_7, /*hidden argument*/NULL);
		__this->set_eyeAdaptation_9(L_7);
		// public BloomModel bloom = new BloomModel();
		BloomModel_t2099727860 * L_8 = (BloomModel_t2099727860 *)il2cpp_codegen_object_new(BloomModel_t2099727860_il2cpp_TypeInfo_var);
		BloomModel__ctor_m2312170588(L_8, /*hidden argument*/NULL);
		__this->set_bloom_10(L_8);
		// public ColorGradingModel colorGrading = new ColorGradingModel();
		ColorGradingModel_t1448048181 * L_9 = (ColorGradingModel_t1448048181 *)il2cpp_codegen_object_new(ColorGradingModel_t1448048181_il2cpp_TypeInfo_var);
		ColorGradingModel__ctor_m845539817(L_9, /*hidden argument*/NULL);
		__this->set_colorGrading_11(L_9);
		// public UserLutModel userLut = new UserLutModel();
		UserLutModel_t1670108080 * L_10 = (UserLutModel_t1670108080 *)il2cpp_codegen_object_new(UserLutModel_t1670108080_il2cpp_TypeInfo_var);
		UserLutModel__ctor_m1936047466(L_10, /*hidden argument*/NULL);
		__this->set_userLut_12(L_10);
		// public ChromaticAberrationModel chromaticAberration = new ChromaticAberrationModel();
		ChromaticAberrationModel_t3963399853 * L_11 = (ChromaticAberrationModel_t3963399853 *)il2cpp_codegen_object_new(ChromaticAberrationModel_t3963399853_il2cpp_TypeInfo_var);
		ChromaticAberrationModel__ctor_m2337246990(L_11, /*hidden argument*/NULL);
		__this->set_chromaticAberration_13(L_11);
		// public GrainModel grain = new GrainModel();
		GrainModel_t1152882488 * L_12 = (GrainModel_t1152882488 *)il2cpp_codegen_object_new(GrainModel_t1152882488_il2cpp_TypeInfo_var);
		GrainModel__ctor_m542869878(L_12, /*hidden argument*/NULL);
		__this->set_grain_14(L_12);
		// public VignetteModel vignette = new VignetteModel();
		VignetteModel_t2845517177 * L_13 = (VignetteModel_t2845517177 *)il2cpp_codegen_object_new(VignetteModel_t2845517177_il2cpp_TypeInfo_var);
		VignetteModel__ctor_m512852279(L_13, /*hidden argument*/NULL);
		__this->set_vignette_15(L_13);
		// public DitheringModel dithering = new DitheringModel();
		DitheringModel_t2429005396 * L_14 = (DitheringModel_t2429005396 *)il2cpp_codegen_object_new(DitheringModel_t2429005396_il2cpp_TypeInfo_var);
		DitheringModel__ctor_m4012316787(L_14, /*hidden argument*/NULL);
		__this->set_dithering_16(L_14);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.RenderTextureFactory::.ctor()
extern "C"  void RenderTextureFactory__ctor_m1345809438 (RenderTextureFactory_t1946967824 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTextureFactory__ctor_m1345809438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public RenderTextureFactory()
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// m_TemporaryRTs = new HashSet<RenderTexture>();
		// m_TemporaryRTs = new HashSet<RenderTexture>();
		HashSet_1_t673836907 * L_0 = (HashSet_1_t673836907 *)il2cpp_codegen_object_new(HashSet_1_t673836907_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m1993572700(L_0, /*hidden argument*/HashSet_1__ctor_m1993572700_RuntimeMethod_var);
		__this->set_m_TemporaryRTs_0(L_0);
		// }
		return;
	}
}
// UnityEngine.RenderTexture UnityEngine.PostProcessing.RenderTextureFactory::Get(UnityEngine.RenderTexture)
extern "C"  RenderTexture_t2108887433 * RenderTextureFactory_Get_m169036867 (RenderTextureFactory_t1946967824 * __this, RenderTexture_t2108887433 * ___baseRenderTexture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTextureFactory_Get_m169036867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_t2108887433 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	int32_t G_B2_2 = 0;
	int32_t G_B2_3 = 0;
	RenderTextureFactory_t1946967824 * G_B2_4 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	int32_t G_B1_2 = 0;
	int32_t G_B1_3 = 0;
	RenderTextureFactory_t1946967824 * G_B1_4 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	int32_t G_B3_2 = 0;
	int32_t G_B3_3 = 0;
	int32_t G_B3_4 = 0;
	RenderTextureFactory_t1946967824 * G_B3_5 = NULL;
	{
		// return Get(
		RenderTexture_t2108887433 * L_0 = ___baseRenderTexture0;
		// baseRenderTexture.width,
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		RenderTexture_t2108887433 * L_2 = ___baseRenderTexture0;
		// baseRenderTexture.height,
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_2);
		RenderTexture_t2108887433 * L_4 = ___baseRenderTexture0;
		// baseRenderTexture.depth,
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_depth_m3825994142(L_4, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_6 = ___baseRenderTexture0;
		// baseRenderTexture.format,
		NullCheck(L_6);
		int32_t L_7 = RenderTexture_get_format_m3846871418(L_6, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_8 = ___baseRenderTexture0;
		// baseRenderTexture.sRGB ? RenderTextureReadWrite.sRGB : RenderTextureReadWrite.Linear,
		NullCheck(L_8);
		bool L_9 = RenderTexture_get_sRGB_m300498885(L_8, /*hidden argument*/NULL);
		G_B1_0 = L_7;
		G_B1_1 = L_5;
		G_B1_2 = L_3;
		G_B1_3 = L_1;
		G_B1_4 = __this;
		if (!L_9)
		{
			G_B2_0 = L_7;
			G_B2_1 = L_5;
			G_B2_2 = L_3;
			G_B2_3 = L_1;
			G_B2_4 = __this;
			goto IL_002b;
		}
	}
	{
		G_B3_0 = 2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_002c:
	{
		RenderTexture_t2108887433 * L_10 = ___baseRenderTexture0;
		// baseRenderTexture.filterMode,
		NullCheck(L_10);
		int32_t L_11 = Texture_get_filterMode_m3474837873(L_10, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_12 = ___baseRenderTexture0;
		// baseRenderTexture.wrapMode
		NullCheck(L_12);
		int32_t L_13 = Texture_get_wrapMode_m2187367613(L_12, /*hidden argument*/NULL);
		// return Get(
		NullCheck(G_B3_5);
		RenderTexture_t2108887433 * L_14 = RenderTextureFactory_Get_m1772850884(G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_11, L_13, _stringLiteral3857694337, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_0048;
	}

IL_0048:
	{
		// }
		RenderTexture_t2108887433 * L_15 = V_0;
		return L_15;
	}
}
// UnityEngine.RenderTexture UnityEngine.PostProcessing.RenderTextureFactory::Get(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,UnityEngine.FilterMode,UnityEngine.TextureWrapMode,System.String)
extern "C"  RenderTexture_t2108887433 * RenderTextureFactory_Get_m1772850884 (RenderTextureFactory_t1946967824 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___rw4, int32_t ___filterMode5, int32_t ___wrapMode6, String_t* ___name7, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTextureFactory_Get_m1772850884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_t2108887433 * V_0 = NULL;
	RenderTexture_t2108887433 * V_1 = NULL;
	{
		// var rt = RenderTexture.GetTemporary(width, height, depthBuffer, format);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = ___format3;
		// var rt = RenderTexture.GetTemporary(width, height, depthBuffer, format);
		RenderTexture_t2108887433 * L_4 = RenderTexture_GetTemporary_m3378328322(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// rt.filterMode = filterMode;
		RenderTexture_t2108887433 * L_5 = V_0;
		int32_t L_6 = ___filterMode5;
		// rt.filterMode = filterMode;
		NullCheck(L_5);
		Texture_set_filterMode_m3078068058(L_5, L_6, /*hidden argument*/NULL);
		// rt.wrapMode = wrapMode;
		RenderTexture_t2108887433 * L_7 = V_0;
		int32_t L_8 = ___wrapMode6;
		// rt.wrapMode = wrapMode;
		NullCheck(L_7);
		Texture_set_wrapMode_m587872754(L_7, L_8, /*hidden argument*/NULL);
		// rt.name = name;
		RenderTexture_t2108887433 * L_9 = V_0;
		String_t* L_10 = ___name7;
		// rt.name = name;
		NullCheck(L_9);
		Object_set_name_m291480324(L_9, L_10, /*hidden argument*/NULL);
		// m_TemporaryRTs.Add(rt);
		HashSet_1_t673836907 * L_11 = __this->get_m_TemporaryRTs_0();
		RenderTexture_t2108887433 * L_12 = V_0;
		// m_TemporaryRTs.Add(rt);
		NullCheck(L_11);
		HashSet_1_Add_m2910627594(L_11, L_12, /*hidden argument*/HashSet_1_Add_m2910627594_RuntimeMethod_var);
		// return rt;
		RenderTexture_t2108887433 * L_13 = V_0;
		V_1 = L_13;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		RenderTexture_t2108887433 * L_14 = V_1;
		return L_14;
	}
}
// System.Void UnityEngine.PostProcessing.RenderTextureFactory::Release(UnityEngine.RenderTexture)
extern "C"  void RenderTextureFactory_Release_m717800481 (RenderTextureFactory_t1946967824 * __this, RenderTexture_t2108887433 * ___rt0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTextureFactory_Release_m717800481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (rt == null)
		RenderTexture_t2108887433 * L_0 = ___rt0;
		// if (rt == null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		// return;
		goto IL_0047;
	}

IL_0012:
	{
		// if (!m_TemporaryRTs.Contains(rt))
		HashSet_1_t673836907 * L_2 = __this->get_m_TemporaryRTs_0();
		RenderTexture_t2108887433 * L_3 = ___rt0;
		// if (!m_TemporaryRTs.Contains(rt))
		NullCheck(L_2);
		bool L_4 = HashSet_1_Contains_m3753409409(L_2, L_3, /*hidden argument*/HashSet_1_Contains_m3753409409_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		// throw new ArgumentException(string.Format("Attempting to remove a RenderTexture that was not allocated: {0}", rt));
		RenderTexture_t2108887433 * L_5 = ___rt0;
		// throw new ArgumentException(string.Format("Attempting to remove a RenderTexture that was not allocated: {0}", rt));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral1950092709, L_5, /*hidden argument*/NULL);
		// throw new ArgumentException(string.Format("Attempting to remove a RenderTexture that was not allocated: {0}", rt));
		ArgumentException_t132251570 * L_7 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0034:
	{
		// m_TemporaryRTs.Remove(rt);
		HashSet_1_t673836907 * L_8 = __this->get_m_TemporaryRTs_0();
		RenderTexture_t2108887433 * L_9 = ___rt0;
		// m_TemporaryRTs.Remove(rt);
		NullCheck(L_8);
		HashSet_1_Remove_m467255894(L_8, L_9, /*hidden argument*/HashSet_1_Remove_m467255894_RuntimeMethod_var);
		// RenderTexture.ReleaseTemporary(rt);
		RenderTexture_t2108887433 * L_10 = ___rt0;
		// RenderTexture.ReleaseTemporary(rt);
		RenderTexture_ReleaseTemporary_m2400081536(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0047:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.RenderTextureFactory::ReleaseAll()
extern "C"  void RenderTextureFactory_ReleaseAll_m3329667721 (RenderTextureFactory_t1946967824 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTextureFactory_ReleaseAll_m3329667721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t2379014178  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// var enumerator = m_TemporaryRTs.GetEnumerator();
		HashSet_1_t673836907 * L_0 = __this->get_m_TemporaryRTs_0();
		// var enumerator = m_TemporaryRTs.GetEnumerator();
		NullCheck(L_0);
		Enumerator_t2379014178  L_1 = HashSet_1_GetEnumerator_m4157320476(L_0, /*hidden argument*/HashSet_1_GetEnumerator_m4157320476_RuntimeMethod_var);
		V_0 = L_1;
		// while (enumerator.MoveNext())
		goto IL_001e;
	}

IL_0012:
	{
		// RenderTexture.ReleaseTemporary(enumerator.Current);
		// RenderTexture.ReleaseTemporary(enumerator.Current);
		RenderTexture_t2108887433 * L_2 = Enumerator_get_Current_m3525666044((&V_0), /*hidden argument*/Enumerator_get_Current_m3525666044_RuntimeMethod_var);
		// RenderTexture.ReleaseTemporary(enumerator.Current);
		RenderTexture_ReleaseTemporary_m2400081536(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		// while (enumerator.MoveNext())
		// while (enumerator.MoveNext())
		bool L_3 = Enumerator_MoveNext_m519760291((&V_0), /*hidden argument*/Enumerator_MoveNext_m519760291_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		// m_TemporaryRTs.Clear();
		HashSet_1_t673836907 * L_4 = __this->get_m_TemporaryRTs_0();
		// m_TemporaryRTs.Clear();
		NullCheck(L_4);
		HashSet_1_Clear_m552370813(L_4, /*hidden argument*/HashSet_1_Clear_m552370813_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.RenderTextureFactory::Dispose()
extern "C"  void RenderTextureFactory_Dispose_m2714960054 (RenderTextureFactory_t1946967824 * __this, const RuntimeMethod* method)
{
	{
		// ReleaseAll();
		// ReleaseAll();
		RenderTextureFactory_ReleaseAll_m3329667721(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::.ctor()
extern "C"  void ScreenSpaceReflectionComponent__ctor_m2879296341 (ScreenSpaceReflectionComponent_t856094247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenSpaceReflectionComponent__ctor_m2879296341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bool k_HighlightSuppression = false;
		__this->set_k_HighlightSuppression_2((bool)0);
		// bool k_TraceBehindObjects = true;
		__this->set_k_TraceBehindObjects_3((bool)1);
		// bool k_TreatBackfaceHitAsMiss = false;
		__this->set_k_TreatBackfaceHitAsMiss_4((bool)0);
		// bool k_BilateralUpsample = true;
		__this->set_k_BilateralUpsample_5((bool)1);
		// readonly int[] m_ReflectionTextures = new int[5];
		__this->set_m_ReflectionTextures_6(((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)5)));
		PostProcessingComponentCommandBuffer_1__ctor_m1811253078(__this, /*hidden argument*/PostProcessingComponentCommandBuffer_1__ctor_m1811253078_RuntimeMethod_var);
		return;
	}
}
// UnityEngine.DepthTextureMode UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::GetCameraFlags()
extern "C"  int32_t ScreenSpaceReflectionComponent_GetCameraFlags_m3820416721 (ScreenSpaceReflectionComponent_t856094247 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return DepthTextureMode.Depth;
		V_0 = 1;
		goto IL_0008;
	}

IL_0008:
	{
		// }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::get_active()
extern "C"  bool ScreenSpaceReflectionComponent_get_active_m243829711 (ScreenSpaceReflectionComponent_t856094247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenSpaceReflectionComponent_get_active_m243829711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B4_0 = 0;
	{
		// return model.enabled
		// return model.enabled
		ScreenSpaceReflectionModel_t3026344732 * L_0 = PostProcessingComponent_1_get_model_m3701723778(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m3701723778_RuntimeMethod_var);
		// return model.enabled
		NullCheck(L_0);
		bool L_1 = PostProcessingModel_get_enabled_m2892084724(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		PostProcessingContext_t2014408948 * L_2 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// && context.isGBufferAvailable
		NullCheck(L_2);
		bool L_3 = PostProcessingContext_get_isGBufferAvailable_m949646721(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		PostProcessingContext_t2014408948 * L_4 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// && !context.interrupted;
		NullCheck(L_4);
		bool L_5 = PostProcessingContext_get_interrupted_m1809095682(L_4, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		goto IL_0032;
	}

IL_0031:
	{
		G_B4_0 = 0;
	}

IL_0032:
	{
		V_0 = (bool)G_B4_0;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::OnEnable()
extern "C"  void ScreenSpaceReflectionComponent_OnEnable_m346974116 (ScreenSpaceReflectionComponent_t856094247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenSpaceReflectionComponent_OnEnable_m346974116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_ReflectionTextures[0] = Shader.PropertyToID("_ReflectionTexture0");
		Int32U5BU5D_t385246372* L_0 = __this->get_m_ReflectionTextures_6();
		// m_ReflectionTextures[0] = Shader.PropertyToID("_ReflectionTexture0");
		int32_t L_1 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3996654635, /*hidden argument*/NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_1);
		// m_ReflectionTextures[1] = Shader.PropertyToID("_ReflectionTexture1");
		Int32U5BU5D_t385246372* L_2 = __this->get_m_ReflectionTextures_6();
		// m_ReflectionTextures[1] = Shader.PropertyToID("_ReflectionTexture1");
		int32_t L_3 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1267771280, /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_3);
		// m_ReflectionTextures[2] = Shader.PropertyToID("_ReflectionTexture2");
		Int32U5BU5D_t385246372* L_4 = __this->get_m_ReflectionTextures_6();
		// m_ReflectionTextures[2] = Shader.PropertyToID("_ReflectionTexture2");
		int32_t L_5 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral864486753, /*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)L_5);
		// m_ReflectionTextures[3] = Shader.PropertyToID("_ReflectionTexture3");
		Int32U5BU5D_t385246372* L_6 = __this->get_m_ReflectionTextures_6();
		// m_ReflectionTextures[3] = Shader.PropertyToID("_ReflectionTexture3");
		int32_t L_7 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2430570694, /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)L_7);
		// m_ReflectionTextures[4] = Shader.PropertyToID("_ReflectionTexture4");
		Int32U5BU5D_t385246372* L_8 = __this->get_m_ReflectionTextures_6();
		// m_ReflectionTextures[4] = Shader.PropertyToID("_ReflectionTexture4");
		int32_t L_9 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2027286167, /*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (int32_t)L_9);
		// }
		return;
	}
}
// System.String UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::GetName()
extern "C"  String_t* ScreenSpaceReflectionComponent_GetName_m2112635201 (ScreenSpaceReflectionComponent_t856094247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenSpaceReflectionComponent_GetName_m2112635201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return "Screen Space Reflection";
		V_0 = _stringLiteral2578318749;
		goto IL_000c;
	}

IL_000c:
	{
		// }
		String_t* L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Rendering.CameraEvent UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::GetCameraEvent()
extern "C"  int32_t ScreenSpaceReflectionComponent_GetCameraEvent_m1332987900 (ScreenSpaceReflectionComponent_t856094247 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return CameraEvent.AfterFinalPass;
		V_0 = ((int32_t)9);
		goto IL_0009;
	}

IL_0009:
	{
		// }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::PopulateCommandBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  void ScreenSpaceReflectionComponent_PopulateCommandBuffer_m3120254331 (ScreenSpaceReflectionComponent_t856094247 * __this, CommandBuffer_t2206337031 * ___cb0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenSpaceReflectionComponent_PopulateCommandBuffer_m3120254331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Settings_t1995791524  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Camera_t4157153871 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	Material_t340375123 * V_9 = NULL;
	float V_10 = 0.0f;
	Matrix4x4_t1817901843  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector4_t3319028937  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Matrix4x4_t1817901843  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Matrix4x4_t1817901843  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Matrix4x4_t1817901843  V_16;
	memset(&V_16, 0, sizeof(V_16));
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	int32_t V_24 = 0;
	int32_t V_25 = 0;
	int32_t V_26 = 0;
	int32_t V_27 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B5_0 = 0;
	Material_t340375123 * G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	Material_t340375123 * G_B4_1 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	Material_t340375123 * G_B6_2 = NULL;
	int32_t G_B8_0 = 0;
	Material_t340375123 * G_B8_1 = NULL;
	int32_t G_B7_0 = 0;
	Material_t340375123 * G_B7_1 = NULL;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	Material_t340375123 * G_B9_2 = NULL;
	int32_t G_B11_0 = 0;
	Material_t340375123 * G_B11_1 = NULL;
	int32_t G_B10_0 = 0;
	Material_t340375123 * G_B10_1 = NULL;
	int32_t G_B12_0 = 0;
	int32_t G_B12_1 = 0;
	Material_t340375123 * G_B12_2 = NULL;
	int32_t G_B14_0 = 0;
	Material_t340375123 * G_B14_1 = NULL;
	int32_t G_B13_0 = 0;
	Material_t340375123 * G_B13_1 = NULL;
	int32_t G_B15_0 = 0;
	int32_t G_B15_1 = 0;
	Material_t340375123 * G_B15_2 = NULL;
	int32_t G_B17_0 = 0;
	Material_t340375123 * G_B17_1 = NULL;
	int32_t G_B16_0 = 0;
	Material_t340375123 * G_B16_1 = NULL;
	int32_t G_B18_0 = 0;
	int32_t G_B18_1 = 0;
	Material_t340375123 * G_B18_2 = NULL;
	int32_t G_B20_0 = 0;
	Material_t340375123 * G_B20_1 = NULL;
	int32_t G_B19_0 = 0;
	Material_t340375123 * G_B19_1 = NULL;
	int32_t G_B21_0 = 0;
	int32_t G_B21_1 = 0;
	Material_t340375123 * G_B21_2 = NULL;
	int32_t G_B23_0 = 0;
	Material_t340375123 * G_B23_1 = NULL;
	int32_t G_B22_0 = 0;
	Material_t340375123 * G_B22_1 = NULL;
	int32_t G_B24_0 = 0;
	int32_t G_B24_1 = 0;
	Material_t340375123 * G_B24_2 = NULL;
	Vector3_t3722313464  G_B27_0;
	memset(&G_B27_0, 0, sizeof(G_B27_0));
	int32_t G_B30_0 = 0;
	int32_t G_B35_0 = 0;
	int32_t G_B35_1 = 0;
	int32_t G_B35_2 = 0;
	int32_t G_B35_3 = 0;
	CommandBuffer_t2206337031 * G_B35_4 = NULL;
	int32_t G_B34_0 = 0;
	int32_t G_B34_1 = 0;
	int32_t G_B34_2 = 0;
	int32_t G_B34_3 = 0;
	CommandBuffer_t2206337031 * G_B34_4 = NULL;
	int32_t G_B36_0 = 0;
	int32_t G_B36_1 = 0;
	int32_t G_B36_2 = 0;
	int32_t G_B36_3 = 0;
	int32_t G_B36_4 = 0;
	CommandBuffer_t2206337031 * G_B36_5 = NULL;
	{
		// var settings = model.settings;
		// var settings = model.settings;
		ScreenSpaceReflectionModel_t3026344732 * L_0 = PostProcessingComponent_1_get_model_m3701723778(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m3701723778_RuntimeMethod_var);
		// var settings = model.settings;
		NullCheck(L_0);
		Settings_t1995791524  L_1 = ScreenSpaceReflectionModel_get_settings_m3165114047(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var camera = context.camera;
		PostProcessingContext_t2014408948 * L_2 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_2);
		Camera_t4157153871 * L_3 = L_2->get_camera_1();
		V_1 = L_3;
		// int downsampleAmount = (settings.reflection.reflectionQuality == SSRResolution.High) ? 1 : 2;
		ReflectionSettings_t282755190 * L_4 = (&V_0)->get_address_of_reflection_0();
		int32_t L_5 = L_4->get_reflectionQuality_1();
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0031;
	}

IL_0030:
	{
		G_B3_0 = 2;
	}

IL_0031:
	{
		V_2 = G_B3_0;
		// var rtW = context.width / downsampleAmount;
		PostProcessingContext_t2014408948 * L_6 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// var rtW = context.width / downsampleAmount;
		NullCheck(L_6);
		int32_t L_7 = PostProcessingContext_get_width_m2658937703(L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_2;
		V_3 = ((int32_t)((int32_t)L_7/(int32_t)L_8));
		// var rtH = context.height / downsampleAmount;
		PostProcessingContext_t2014408948 * L_9 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// var rtH = context.height / downsampleAmount;
		NullCheck(L_9);
		int32_t L_10 = PostProcessingContext_get_height_m4218042885(L_9, /*hidden argument*/NULL);
		int32_t L_11 = V_2;
		V_4 = ((int32_t)((int32_t)L_10/(int32_t)L_11));
		// float sWidth = context.width;
		PostProcessingContext_t2014408948 * L_12 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// float sWidth = context.width;
		NullCheck(L_12);
		int32_t L_13 = PostProcessingContext_get_width_m2658937703(L_12, /*hidden argument*/NULL);
		V_5 = (((float)((float)L_13)));
		// float sHeight = context.height;
		PostProcessingContext_t2014408948 * L_14 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// float sHeight = context.height;
		NullCheck(L_14);
		int32_t L_15 = PostProcessingContext_get_height_m4218042885(L_14, /*hidden argument*/NULL);
		V_6 = (((float)((float)L_15)));
		// float sx = sWidth / 2f;
		float L_16 = V_5;
		V_7 = ((float)((float)L_16/(float)(2.0f)));
		// float sy = sHeight / 2f;
		float L_17 = V_6;
		V_8 = ((float)((float)L_17/(float)(2.0f)));
		// var material = context.materialFactory.Get("Hidden/Post FX/Screen Space Reflection");
		PostProcessingContext_t2014408948 * L_18 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_18);
		MaterialFactory_t2445948724 * L_19 = L_18->get_materialFactory_2();
		// var material = context.materialFactory.Get("Hidden/Post FX/Screen Space Reflection");
		NullCheck(L_19);
		Material_t340375123 * L_20 = MaterialFactory_Get_m4113232693(L_19, _stringLiteral3225738970, /*hidden argument*/NULL);
		V_9 = L_20;
		// material.SetInt(Uniforms._RayStepSize, settings.reflection.stepSize);
		Material_t340375123 * L_21 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_22 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__RayStepSize_0();
		ReflectionSettings_t282755190 * L_23 = (&V_0)->get_address_of_reflection_0();
		int32_t L_24 = L_23->get_stepSize_4();
		// material.SetInt(Uniforms._RayStepSize, settings.reflection.stepSize);
		NullCheck(L_21);
		Material_SetInt_m475299667(L_21, L_22, L_24, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._AdditiveReflection, settings.reflection.blendType == SSRReflectionBlendType.Additive ? 1 : 0);
		Material_t340375123 * L_25 = V_9;
		int32_t L_26 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__AdditiveReflection_1();
		ReflectionSettings_t282755190 * L_27 = (&V_0)->get_address_of_reflection_0();
		int32_t L_28 = L_27->get_blendType_0();
		G_B4_0 = L_26;
		G_B4_1 = L_25;
		if ((!(((uint32_t)L_28) == ((uint32_t)1))))
		{
			G_B5_0 = L_26;
			G_B5_1 = L_25;
			goto IL_00cd;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_00ce;
	}

IL_00cd:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_00ce:
	{
		// material.SetInt(Uniforms._AdditiveReflection, settings.reflection.blendType == SSRReflectionBlendType.Additive ? 1 : 0);
		NullCheck(G_B6_2);
		Material_SetInt_m475299667(G_B6_2, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._BilateralUpsampling, k_BilateralUpsample ? 1 : 0);
		Material_t340375123 * L_29 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_30 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__BilateralUpsampling_2();
		bool L_31 = __this->get_k_BilateralUpsample_5();
		G_B7_0 = L_30;
		G_B7_1 = L_29;
		if (!L_31)
		{
			G_B8_0 = L_30;
			G_B8_1 = L_29;
			goto IL_00eb;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00ec;
	}

IL_00eb:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00ec:
	{
		// material.SetInt(Uniforms._BilateralUpsampling, k_BilateralUpsample ? 1 : 0);
		NullCheck(G_B9_2);
		Material_SetInt_m475299667(G_B9_2, G_B9_1, G_B9_0, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._TreatBackfaceHitAsMiss, k_TreatBackfaceHitAsMiss ? 1 : 0);
		Material_t340375123 * L_32 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_33 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__TreatBackfaceHitAsMiss_3();
		bool L_34 = __this->get_k_TreatBackfaceHitAsMiss_4();
		G_B10_0 = L_33;
		G_B10_1 = L_32;
		if (!L_34)
		{
			G_B11_0 = L_33;
			G_B11_1 = L_32;
			goto IL_0109;
		}
	}
	{
		G_B12_0 = 1;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_010a;
	}

IL_0109:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_010a:
	{
		// material.SetInt(Uniforms._TreatBackfaceHitAsMiss, k_TreatBackfaceHitAsMiss ? 1 : 0);
		NullCheck(G_B12_2);
		Material_SetInt_m475299667(G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._AllowBackwardsRays, settings.reflection.reflectBackfaces ? 1 : 0);
		Material_t340375123 * L_35 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_36 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__AllowBackwardsRays_4();
		ReflectionSettings_t282755190 * L_37 = (&V_0)->get_address_of_reflection_0();
		bool L_38 = L_37->get_reflectBackfaces_7();
		G_B13_0 = L_36;
		G_B13_1 = L_35;
		if (!L_38)
		{
			G_B14_0 = L_36;
			G_B14_1 = L_35;
			goto IL_012d;
		}
	}
	{
		G_B15_0 = 1;
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		goto IL_012e;
	}

IL_012d:
	{
		G_B15_0 = 0;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
	}

IL_012e:
	{
		// material.SetInt(Uniforms._AllowBackwardsRays, settings.reflection.reflectBackfaces ? 1 : 0);
		NullCheck(G_B15_2);
		Material_SetInt_m475299667(G_B15_2, G_B15_1, G_B15_0, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._TraceBehindObjects, k_TraceBehindObjects ? 1 : 0);
		Material_t340375123 * L_39 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_40 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__TraceBehindObjects_5();
		bool L_41 = __this->get_k_TraceBehindObjects_3();
		G_B16_0 = L_40;
		G_B16_1 = L_39;
		if (!L_41)
		{
			G_B17_0 = L_40;
			G_B17_1 = L_39;
			goto IL_014b;
		}
	}
	{
		G_B18_0 = 1;
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		goto IL_014c;
	}

IL_014b:
	{
		G_B18_0 = 0;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
	}

IL_014c:
	{
		// material.SetInt(Uniforms._TraceBehindObjects, k_TraceBehindObjects ? 1 : 0);
		NullCheck(G_B18_2);
		Material_SetInt_m475299667(G_B18_2, G_B18_1, G_B18_0, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._MaxSteps, settings.reflection.iterationCount);
		Material_t340375123 * L_42 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_43 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__MaxSteps_6();
		ReflectionSettings_t282755190 * L_44 = (&V_0)->get_address_of_reflection_0();
		int32_t L_45 = L_44->get_iterationCount_3();
		// material.SetInt(Uniforms._MaxSteps, settings.reflection.iterationCount);
		NullCheck(L_42);
		Material_SetInt_m475299667(L_42, L_43, L_45, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._FullResolutionFiltering, 0);
		Material_t340375123 * L_46 = V_9;
		int32_t L_47 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__FullResolutionFiltering_7();
		// material.SetInt(Uniforms._FullResolutionFiltering, 0);
		NullCheck(L_46);
		Material_SetInt_m475299667(L_46, L_47, 0, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._HalfResolution, (settings.reflection.reflectionQuality != SSRResolution.High) ? 1 : 0);
		Material_t340375123 * L_48 = V_9;
		int32_t L_49 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__HalfResolution_8();
		ReflectionSettings_t282755190 * L_50 = (&V_0)->get_address_of_reflection_0();
		int32_t L_51 = L_50->get_reflectionQuality_1();
		G_B19_0 = L_49;
		G_B19_1 = L_48;
		if (!L_51)
		{
			G_B20_0 = L_49;
			G_B20_1 = L_48;
			goto IL_0194;
		}
	}
	{
		G_B21_0 = 1;
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		goto IL_0195;
	}

IL_0194:
	{
		G_B21_0 = 0;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
	}

IL_0195:
	{
		// material.SetInt(Uniforms._HalfResolution, (settings.reflection.reflectionQuality != SSRResolution.High) ? 1 : 0);
		NullCheck(G_B21_2);
		Material_SetInt_m475299667(G_B21_2, G_B21_1, G_B21_0, /*hidden argument*/NULL);
		// material.SetInt(Uniforms._HighlightSuppression, k_HighlightSuppression ? 1 : 0);
		Material_t340375123 * L_52 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_53 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__HighlightSuppression_9();
		bool L_54 = __this->get_k_HighlightSuppression_2();
		G_B22_0 = L_53;
		G_B22_1 = L_52;
		if (!L_54)
		{
			G_B23_0 = L_53;
			G_B23_1 = L_52;
			goto IL_01b2;
		}
	}
	{
		G_B24_0 = 1;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		goto IL_01b3;
	}

IL_01b2:
	{
		G_B24_0 = 0;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
	}

IL_01b3:
	{
		// material.SetInt(Uniforms._HighlightSuppression, k_HighlightSuppression ? 1 : 0);
		NullCheck(G_B24_2);
		Material_SetInt_m475299667(G_B24_2, G_B24_1, G_B24_0, /*hidden argument*/NULL);
		// float pixelsPerMeterAtOneMeter = sWidth / (-2f * Mathf.Tan(camera.fieldOfView / 180f * Mathf.PI * 0.5f));
		float L_55 = V_5;
		Camera_t4157153871 * L_56 = V_1;
		// float pixelsPerMeterAtOneMeter = sWidth / (-2f * Mathf.Tan(camera.fieldOfView / 180f * Mathf.PI * 0.5f));
		NullCheck(L_56);
		float L_57 = Camera_get_fieldOfView_m1018585504(L_56, /*hidden argument*/NULL);
		// float pixelsPerMeterAtOneMeter = sWidth / (-2f * Mathf.Tan(camera.fieldOfView / 180f * Mathf.PI * 0.5f));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_58 = tanf(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)((float)L_57/(float)(180.0f))), (float)(3.14159274f))), (float)(0.5f))));
		V_10 = ((float)((float)L_55/(float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_58))));
		// material.SetFloat(Uniforms._PixelsPerMeterAtOneMeter, pixelsPerMeterAtOneMeter);
		Material_t340375123 * L_59 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_60 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__PixelsPerMeterAtOneMeter_10();
		float L_61 = V_10;
		// material.SetFloat(Uniforms._PixelsPerMeterAtOneMeter, pixelsPerMeterAtOneMeter);
		NullCheck(L_59);
		Material_SetFloat_m1688718093(L_59, L_60, L_61, /*hidden argument*/NULL);
		// material.SetFloat(Uniforms._ScreenEdgeFading, settings.screenEdgeMask.intensity);
		Material_t340375123 * L_62 = V_9;
		int32_t L_63 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__ScreenEdgeFading_11();
		ScreenEdgeMask_t4063288584 * L_64 = (&V_0)->get_address_of_screenEdgeMask_2();
		float L_65 = L_64->get_intensity_0();
		// material.SetFloat(Uniforms._ScreenEdgeFading, settings.screenEdgeMask.intensity);
		NullCheck(L_62);
		Material_SetFloat_m1688718093(L_62, L_63, L_65, /*hidden argument*/NULL);
		// material.SetFloat(Uniforms._ReflectionBlur, settings.reflection.reflectionBlur);
		Material_t340375123 * L_66 = V_9;
		int32_t L_67 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__ReflectionBlur_12();
		ReflectionSettings_t282755190 * L_68 = (&V_0)->get_address_of_reflection_0();
		float L_69 = L_68->get_reflectionBlur_6();
		// material.SetFloat(Uniforms._ReflectionBlur, settings.reflection.reflectionBlur);
		NullCheck(L_66);
		Material_SetFloat_m1688718093(L_66, L_67, L_69, /*hidden argument*/NULL);
		// material.SetFloat(Uniforms._MaxRayTraceDistance, settings.reflection.maxDistance);
		Material_t340375123 * L_70 = V_9;
		int32_t L_71 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__MaxRayTraceDistance_13();
		ReflectionSettings_t282755190 * L_72 = (&V_0)->get_address_of_reflection_0();
		float L_73 = L_72->get_maxDistance_2();
		// material.SetFloat(Uniforms._MaxRayTraceDistance, settings.reflection.maxDistance);
		NullCheck(L_70);
		Material_SetFloat_m1688718093(L_70, L_71, L_73, /*hidden argument*/NULL);
		// material.SetFloat(Uniforms._FadeDistance, settings.intensity.fadeDistance);
		Material_t340375123 * L_74 = V_9;
		int32_t L_75 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__FadeDistance_14();
		IntensitySettings_t1721872184 * L_76 = (&V_0)->get_address_of_intensity_1();
		float L_77 = L_76->get_fadeDistance_1();
		// material.SetFloat(Uniforms._FadeDistance, settings.intensity.fadeDistance);
		NullCheck(L_74);
		Material_SetFloat_m1688718093(L_74, L_75, L_77, /*hidden argument*/NULL);
		// material.SetFloat(Uniforms._LayerThickness, settings.reflection.widthModifier);
		Material_t340375123 * L_78 = V_9;
		int32_t L_79 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__LayerThickness_15();
		ReflectionSettings_t282755190 * L_80 = (&V_0)->get_address_of_reflection_0();
		float L_81 = L_80->get_widthModifier_5();
		// material.SetFloat(Uniforms._LayerThickness, settings.reflection.widthModifier);
		NullCheck(L_78);
		Material_SetFloat_m1688718093(L_78, L_79, L_81, /*hidden argument*/NULL);
		// material.SetFloat(Uniforms._SSRMultiplier, settings.intensity.reflectionMultiplier);
		Material_t340375123 * L_82 = V_9;
		int32_t L_83 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__SSRMultiplier_16();
		IntensitySettings_t1721872184 * L_84 = (&V_0)->get_address_of_intensity_1();
		float L_85 = L_84->get_reflectionMultiplier_0();
		// material.SetFloat(Uniforms._SSRMultiplier, settings.intensity.reflectionMultiplier);
		NullCheck(L_82);
		Material_SetFloat_m1688718093(L_82, L_83, L_85, /*hidden argument*/NULL);
		// material.SetFloat(Uniforms._FresnelFade, settings.intensity.fresnelFade);
		Material_t340375123 * L_86 = V_9;
		int32_t L_87 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__FresnelFade_17();
		IntensitySettings_t1721872184 * L_88 = (&V_0)->get_address_of_intensity_1();
		float L_89 = L_88->get_fresnelFade_2();
		// material.SetFloat(Uniforms._FresnelFade, settings.intensity.fresnelFade);
		NullCheck(L_86);
		Material_SetFloat_m1688718093(L_86, L_87, L_89, /*hidden argument*/NULL);
		// material.SetFloat(Uniforms._FresnelFadePower, settings.intensity.fresnelFadePower);
		Material_t340375123 * L_90 = V_9;
		int32_t L_91 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__FresnelFadePower_18();
		IntensitySettings_t1721872184 * L_92 = (&V_0)->get_address_of_intensity_1();
		float L_93 = L_92->get_fresnelFadePower_3();
		// material.SetFloat(Uniforms._FresnelFadePower, settings.intensity.fresnelFadePower);
		NullCheck(L_90);
		Material_SetFloat_m1688718093(L_90, L_91, L_93, /*hidden argument*/NULL);
		// var P = camera.projectionMatrix;
		Camera_t4157153871 * L_94 = V_1;
		// var P = camera.projectionMatrix;
		NullCheck(L_94);
		Matrix4x4_t1817901843  L_95 = Camera_get_projectionMatrix_m667780853(L_94, /*hidden argument*/NULL);
		V_11 = L_95;
		// var projInfo = new Vector4(
		float L_96 = V_5;
		// -2f / (sWidth * P[0]),
		float L_97 = Matrix4x4_get_Item_m567451091((&V_11), 0, /*hidden argument*/NULL);
		float L_98 = V_6;
		// -2f / (sHeight * P[5]),
		float L_99 = Matrix4x4_get_Item_m567451091((&V_11), 5, /*hidden argument*/NULL);
		// (1f - P[2]) / P[0],
		float L_100 = Matrix4x4_get_Item_m567451091((&V_11), 2, /*hidden argument*/NULL);
		// (1f - P[2]) / P[0],
		float L_101 = Matrix4x4_get_Item_m567451091((&V_11), 0, /*hidden argument*/NULL);
		// (1f + P[6]) / P[5]
		float L_102 = Matrix4x4_get_Item_m567451091((&V_11), 6, /*hidden argument*/NULL);
		// (1f + P[6]) / P[5]
		float L_103 = Matrix4x4_get_Item_m567451091((&V_11), 5, /*hidden argument*/NULL);
		// var projInfo = new Vector4(
		Vector4__ctor_m2498754347((&V_12), ((float)((float)(-2.0f)/(float)((float)il2cpp_codegen_multiply((float)L_96, (float)L_97)))), ((float)((float)(-2.0f)/(float)((float)il2cpp_codegen_multiply((float)L_98, (float)L_99)))), ((float)((float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_100))/(float)L_101)), ((float)((float)((float)il2cpp_codegen_add((float)(1.0f), (float)L_102))/(float)L_103)), /*hidden argument*/NULL);
		// var cameraClipInfo = float.IsPositiveInfinity(camera.farClipPlane) ?
		Camera_t4157153871 * L_104 = V_1;
		// var cameraClipInfo = float.IsPositiveInfinity(camera.farClipPlane) ?
		NullCheck(L_104);
		float L_105 = Camera_get_farClipPlane_m538536689(L_104, /*hidden argument*/NULL);
		// var cameraClipInfo = float.IsPositiveInfinity(camera.farClipPlane) ?
		bool L_106 = Single_IsPositiveInfinity_m1411272350(NULL /*static, unused*/, L_105, /*hidden argument*/NULL);
		if (!L_106)
		{
			goto IL_0337;
		}
	}
	{
		Camera_t4157153871 * L_107 = V_1;
		// new Vector3(camera.nearClipPlane, -1f, 1f) :
		NullCheck(L_107);
		float L_108 = Camera_get_nearClipPlane_m837839537(L_107, /*hidden argument*/NULL);
		// new Vector3(camera.nearClipPlane, -1f, 1f) :
		Vector3_t3722313464  L_109;
		memset(&L_109, 0, sizeof(L_109));
		Vector3__ctor_m3353183577((&L_109), L_108, (-1.0f), (1.0f), /*hidden argument*/NULL);
		G_B27_0 = L_109;
		goto IL_035c;
	}

IL_0337:
	{
		Camera_t4157153871 * L_110 = V_1;
		// new Vector3(camera.nearClipPlane * camera.farClipPlane, camera.nearClipPlane - camera.farClipPlane, camera.farClipPlane);
		NullCheck(L_110);
		float L_111 = Camera_get_nearClipPlane_m837839537(L_110, /*hidden argument*/NULL);
		Camera_t4157153871 * L_112 = V_1;
		// new Vector3(camera.nearClipPlane * camera.farClipPlane, camera.nearClipPlane - camera.farClipPlane, camera.farClipPlane);
		NullCheck(L_112);
		float L_113 = Camera_get_farClipPlane_m538536689(L_112, /*hidden argument*/NULL);
		Camera_t4157153871 * L_114 = V_1;
		// new Vector3(camera.nearClipPlane * camera.farClipPlane, camera.nearClipPlane - camera.farClipPlane, camera.farClipPlane);
		NullCheck(L_114);
		float L_115 = Camera_get_nearClipPlane_m837839537(L_114, /*hidden argument*/NULL);
		Camera_t4157153871 * L_116 = V_1;
		// new Vector3(camera.nearClipPlane * camera.farClipPlane, camera.nearClipPlane - camera.farClipPlane, camera.farClipPlane);
		NullCheck(L_116);
		float L_117 = Camera_get_farClipPlane_m538536689(L_116, /*hidden argument*/NULL);
		Camera_t4157153871 * L_118 = V_1;
		// new Vector3(camera.nearClipPlane * camera.farClipPlane, camera.nearClipPlane - camera.farClipPlane, camera.farClipPlane);
		NullCheck(L_118);
		float L_119 = Camera_get_farClipPlane_m538536689(L_118, /*hidden argument*/NULL);
		// new Vector3(camera.nearClipPlane * camera.farClipPlane, camera.nearClipPlane - camera.farClipPlane, camera.farClipPlane);
		Vector3_t3722313464  L_120;
		memset(&L_120, 0, sizeof(L_120));
		Vector3__ctor_m3353183577((&L_120), ((float)il2cpp_codegen_multiply((float)L_111, (float)L_113)), ((float)il2cpp_codegen_subtract((float)L_115, (float)L_117)), L_119, /*hidden argument*/NULL);
		G_B27_0 = L_120;
	}

IL_035c:
	{
		V_13 = G_B27_0;
		// material.SetVector(Uniforms._ReflectionBufferSize, new Vector2(rtW, rtH));
		Material_t340375123 * L_121 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_122 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__ReflectionBufferSize_19();
		int32_t L_123 = V_3;
		int32_t L_124 = V_4;
		// material.SetVector(Uniforms._ReflectionBufferSize, new Vector2(rtW, rtH));
		Vector2_t2156229523  L_125;
		memset(&L_125, 0, sizeof(L_125));
		Vector2__ctor_m3970636864((&L_125), (((float)((float)L_123))), (((float)((float)L_124))), /*hidden argument*/NULL);
		// material.SetVector(Uniforms._ReflectionBufferSize, new Vector2(rtW, rtH));
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_126 = Vector4_op_Implicit_m237151757(NULL /*static, unused*/, L_125, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._ReflectionBufferSize, new Vector2(rtW, rtH));
		NullCheck(L_121);
		Material_SetVector_m2633010038(L_121, L_122, L_126, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._ScreenSize, new Vector2(sWidth, sHeight));
		Material_t340375123 * L_127 = V_9;
		int32_t L_128 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__ScreenSize_20();
		float L_129 = V_5;
		float L_130 = V_6;
		// material.SetVector(Uniforms._ScreenSize, new Vector2(sWidth, sHeight));
		Vector2_t2156229523  L_131;
		memset(&L_131, 0, sizeof(L_131));
		Vector2__ctor_m3970636864((&L_131), L_129, L_130, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._ScreenSize, new Vector2(sWidth, sHeight));
		Vector4_t3319028937  L_132 = Vector4_op_Implicit_m237151757(NULL /*static, unused*/, L_131, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._ScreenSize, new Vector2(sWidth, sHeight));
		NullCheck(L_127);
		Material_SetVector_m2633010038(L_127, L_128, L_132, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._InvScreenSize, new Vector2(1f / sWidth, 1f / sHeight));
		Material_t340375123 * L_133 = V_9;
		int32_t L_134 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__InvScreenSize_21();
		float L_135 = V_5;
		float L_136 = V_6;
		// material.SetVector(Uniforms._InvScreenSize, new Vector2(1f / sWidth, 1f / sHeight));
		Vector2_t2156229523  L_137;
		memset(&L_137, 0, sizeof(L_137));
		Vector2__ctor_m3970636864((&L_137), ((float)((float)(1.0f)/(float)L_135)), ((float)((float)(1.0f)/(float)L_136)), /*hidden argument*/NULL);
		// material.SetVector(Uniforms._InvScreenSize, new Vector2(1f / sWidth, 1f / sHeight));
		Vector4_t3319028937  L_138 = Vector4_op_Implicit_m237151757(NULL /*static, unused*/, L_137, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._InvScreenSize, new Vector2(1f / sWidth, 1f / sHeight));
		NullCheck(L_133);
		Material_SetVector_m2633010038(L_133, L_134, L_138, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._ProjInfo, projInfo); // used for unprojection
		Material_t340375123 * L_139 = V_9;
		int32_t L_140 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__ProjInfo_22();
		Vector4_t3319028937  L_141 = V_12;
		// material.SetVector(Uniforms._ProjInfo, projInfo); // used for unprojection
		NullCheck(L_139);
		Material_SetVector_m2633010038(L_139, L_140, L_141, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._CameraClipInfo, cameraClipInfo);
		Material_t340375123 * L_142 = V_9;
		int32_t L_143 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__CameraClipInfo_23();
		Vector3_t3722313464  L_144 = V_13;
		// material.SetVector(Uniforms._CameraClipInfo, cameraClipInfo);
		Vector4_t3319028937  L_145 = Vector4_op_Implicit_m2966035112(NULL /*static, unused*/, L_144, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._CameraClipInfo, cameraClipInfo);
		NullCheck(L_142);
		Material_SetVector_m2633010038(L_142, L_143, L_145, /*hidden argument*/NULL);
		// var warpToScreenSpaceMatrix = new Matrix4x4();
		il2cpp_codegen_initobj((&V_14), sizeof(Matrix4x4_t1817901843 ));
		// warpToScreenSpaceMatrix.SetRow(0, new Vector4(sx, 0f, 0f, sx));
		float L_146 = V_7;
		float L_147 = V_7;
		// warpToScreenSpaceMatrix.SetRow(0, new Vector4(sx, 0f, 0f, sx));
		Vector4_t3319028937  L_148;
		memset(&L_148, 0, sizeof(L_148));
		Vector4__ctor_m2498754347((&L_148), L_146, (0.0f), (0.0f), L_147, /*hidden argument*/NULL);
		// warpToScreenSpaceMatrix.SetRow(0, new Vector4(sx, 0f, 0f, sx));
		Matrix4x4_SetRow_m2327530647((&V_14), 0, L_148, /*hidden argument*/NULL);
		// warpToScreenSpaceMatrix.SetRow(1, new Vector4(0f, sy, 0f, sy));
		float L_149 = V_8;
		float L_150 = V_8;
		// warpToScreenSpaceMatrix.SetRow(1, new Vector4(0f, sy, 0f, sy));
		Vector4_t3319028937  L_151;
		memset(&L_151, 0, sizeof(L_151));
		Vector4__ctor_m2498754347((&L_151), (0.0f), L_149, (0.0f), L_150, /*hidden argument*/NULL);
		// warpToScreenSpaceMatrix.SetRow(1, new Vector4(0f, sy, 0f, sy));
		Matrix4x4_SetRow_m2327530647((&V_14), 1, L_151, /*hidden argument*/NULL);
		// warpToScreenSpaceMatrix.SetRow(2, new Vector4(0f, 0f, 1f, 0f));
		// warpToScreenSpaceMatrix.SetRow(2, new Vector4(0f, 0f, 1f, 0f));
		Vector4_t3319028937  L_152;
		memset(&L_152, 0, sizeof(L_152));
		Vector4__ctor_m2498754347((&L_152), (0.0f), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		// warpToScreenSpaceMatrix.SetRow(2, new Vector4(0f, 0f, 1f, 0f));
		Matrix4x4_SetRow_m2327530647((&V_14), 2, L_152, /*hidden argument*/NULL);
		// warpToScreenSpaceMatrix.SetRow(3, new Vector4(0f, 0f, 0f, 1f));
		// warpToScreenSpaceMatrix.SetRow(3, new Vector4(0f, 0f, 0f, 1f));
		Vector4_t3319028937  L_153;
		memset(&L_153, 0, sizeof(L_153));
		Vector4__ctor_m2498754347((&L_153), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		// warpToScreenSpaceMatrix.SetRow(3, new Vector4(0f, 0f, 0f, 1f));
		Matrix4x4_SetRow_m2327530647((&V_14), 3, L_153, /*hidden argument*/NULL);
		// var projectToPixelMatrix = warpToScreenSpaceMatrix * P;
		Matrix4x4_t1817901843  L_154 = V_14;
		Matrix4x4_t1817901843  L_155 = V_11;
		// var projectToPixelMatrix = warpToScreenSpaceMatrix * P;
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_156 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_154, L_155, /*hidden argument*/NULL);
		V_15 = L_156;
		// material.SetMatrix(Uniforms._ProjectToPixelMatrix, projectToPixelMatrix);
		Material_t340375123 * L_157 = V_9;
		int32_t L_158 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__ProjectToPixelMatrix_24();
		Matrix4x4_t1817901843  L_159 = V_15;
		// material.SetMatrix(Uniforms._ProjectToPixelMatrix, projectToPixelMatrix);
		NullCheck(L_157);
		Material_SetMatrix_m751249077(L_157, L_158, L_159, /*hidden argument*/NULL);
		// material.SetMatrix(Uniforms._WorldToCameraMatrix, camera.worldToCameraMatrix);
		Material_t340375123 * L_160 = V_9;
		int32_t L_161 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__WorldToCameraMatrix_25();
		Camera_t4157153871 * L_162 = V_1;
		// material.SetMatrix(Uniforms._WorldToCameraMatrix, camera.worldToCameraMatrix);
		NullCheck(L_162);
		Matrix4x4_t1817901843  L_163 = Camera_get_worldToCameraMatrix_m22661425(L_162, /*hidden argument*/NULL);
		// material.SetMatrix(Uniforms._WorldToCameraMatrix, camera.worldToCameraMatrix);
		NullCheck(L_160);
		Material_SetMatrix_m751249077(L_160, L_161, L_163, /*hidden argument*/NULL);
		// material.SetMatrix(Uniforms._CameraToWorldMatrix, camera.worldToCameraMatrix.inverse);
		Material_t340375123 * L_164 = V_9;
		int32_t L_165 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__CameraToWorldMatrix_26();
		Camera_t4157153871 * L_166 = V_1;
		// material.SetMatrix(Uniforms._CameraToWorldMatrix, camera.worldToCameraMatrix.inverse);
		NullCheck(L_166);
		Matrix4x4_t1817901843  L_167 = Camera_get_worldToCameraMatrix_m22661425(L_166, /*hidden argument*/NULL);
		V_16 = L_167;
		// material.SetMatrix(Uniforms._CameraToWorldMatrix, camera.worldToCameraMatrix.inverse);
		Matrix4x4_t1817901843  L_168 = Matrix4x4_get_inverse_m1870592360((&V_16), /*hidden argument*/NULL);
		// material.SetMatrix(Uniforms._CameraToWorldMatrix, camera.worldToCameraMatrix.inverse);
		NullCheck(L_164);
		Material_SetMatrix_m751249077(L_164, L_165, L_168, /*hidden argument*/NULL);
		// var intermediateFormat = context.isHdr ? RenderTextureFormat.ARGBHalf : RenderTextureFormat.ARGB32;
		PostProcessingContext_t2014408948 * L_169 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// var intermediateFormat = context.isHdr ? RenderTextureFormat.ARGBHalf : RenderTextureFormat.ARGB32;
		NullCheck(L_169);
		bool L_170 = PostProcessingContext_get_isHdr_m3057655858(L_169, /*hidden argument*/NULL);
		if (!L_170)
		{
			goto IL_04b6;
		}
	}
	{
		G_B30_0 = 2;
		goto IL_04b7;
	}

IL_04b6:
	{
		G_B30_0 = 0;
	}

IL_04b7:
	{
		V_17 = G_B30_0;
		// var kNormalAndRoughnessTexture = Uniforms._NormalAndRoughnessTexture;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_171 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__NormalAndRoughnessTexture_29();
		V_18 = L_171;
		// var kHitPointTexture = Uniforms._HitPointTexture;
		int32_t L_172 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__HitPointTexture_30();
		V_19 = L_172;
		// var kBlurTexture = Uniforms._BlurTexture;
		int32_t L_173 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__BlurTexture_31();
		V_20 = L_173;
		// var kFilteredReflections = Uniforms._FilteredReflections;
		int32_t L_174 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__FilteredReflections_32();
		V_21 = L_174;
		// var kFinalReflectionTexture = Uniforms._FinalReflectionTexture;
		int32_t L_175 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__FinalReflectionTexture_33();
		V_22 = L_175;
		// var kTempTexture = Uniforms._TempTexture;
		int32_t L_176 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__TempTexture_34();
		V_23 = L_176;
		// cb.GetTemporaryRT(kNormalAndRoughnessTexture, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
		CommandBuffer_t2206337031 * L_177 = ___cb0;
		int32_t L_178 = V_18;
		// cb.GetTemporaryRT(kNormalAndRoughnessTexture, -1, -1, 0, FilterMode.Point, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
		NullCheck(L_177);
		CommandBuffer_GetTemporaryRT_m2948653747(L_177, L_178, (-1), (-1), 0, 0, 0, 1, /*hidden argument*/NULL);
		// cb.GetTemporaryRT(kHitPointTexture, rtW, rtH, 0, FilterMode.Bilinear, RenderTextureFormat.ARGBHalf, RenderTextureReadWrite.Linear);
		CommandBuffer_t2206337031 * L_179 = ___cb0;
		int32_t L_180 = V_19;
		int32_t L_181 = V_3;
		int32_t L_182 = V_4;
		// cb.GetTemporaryRT(kHitPointTexture, rtW, rtH, 0, FilterMode.Bilinear, RenderTextureFormat.ARGBHalf, RenderTextureReadWrite.Linear);
		NullCheck(L_179);
		CommandBuffer_GetTemporaryRT_m2948653747(L_179, L_180, L_181, L_182, 0, 1, 2, 1, /*hidden argument*/NULL);
		// for (int i = 0; i < maxMip; ++i)
		V_24 = 0;
		goto IL_0532;
	}

IL_0508:
	{
		// cb.GetTemporaryRT(m_ReflectionTextures[i], rtW >> i, rtH >> i, 0, FilterMode.Bilinear, intermediateFormat);
		CommandBuffer_t2206337031 * L_183 = ___cb0;
		Int32U5BU5D_t385246372* L_184 = __this->get_m_ReflectionTextures_6();
		int32_t L_185 = V_24;
		NullCheck(L_184);
		int32_t L_186 = L_185;
		int32_t L_187 = (L_184)->GetAt(static_cast<il2cpp_array_size_t>(L_186));
		int32_t L_188 = V_3;
		int32_t L_189 = V_24;
		int32_t L_190 = V_4;
		int32_t L_191 = V_24;
		int32_t L_192 = V_17;
		// cb.GetTemporaryRT(m_ReflectionTextures[i], rtW >> i, rtH >> i, 0, FilterMode.Bilinear, intermediateFormat);
		NullCheck(L_183);
		CommandBuffer_GetTemporaryRT_m2252457381(L_183, L_187, ((int32_t)((int32_t)L_188>>(int32_t)((int32_t)((int32_t)L_189&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_190>>(int32_t)((int32_t)((int32_t)L_191&(int32_t)((int32_t)31))))), 0, 1, L_192, /*hidden argument*/NULL);
		// for (int i = 0; i < maxMip; ++i)
		int32_t L_193 = V_24;
		V_24 = ((int32_t)il2cpp_codegen_add((int32_t)L_193, (int32_t)1));
	}

IL_0532:
	{
		// for (int i = 0; i < maxMip; ++i)
		int32_t L_194 = V_24;
		if ((((int32_t)L_194) < ((int32_t)5)))
		{
			goto IL_0508;
		}
	}
	{
		// cb.GetTemporaryRT(kFilteredReflections, rtW, rtH, 0, k_BilateralUpsample ? FilterMode.Point : FilterMode.Bilinear, intermediateFormat);
		CommandBuffer_t2206337031 * L_195 = ___cb0;
		int32_t L_196 = V_21;
		int32_t L_197 = V_3;
		int32_t L_198 = V_4;
		bool L_199 = __this->get_k_BilateralUpsample_5();
		G_B34_0 = 0;
		G_B34_1 = L_198;
		G_B34_2 = L_197;
		G_B34_3 = L_196;
		G_B34_4 = L_195;
		if (!L_199)
		{
			G_B35_0 = 0;
			G_B35_1 = L_198;
			G_B35_2 = L_197;
			G_B35_3 = L_196;
			G_B35_4 = L_195;
			goto IL_0552;
		}
	}
	{
		G_B36_0 = 0;
		G_B36_1 = G_B34_0;
		G_B36_2 = G_B34_1;
		G_B36_3 = G_B34_2;
		G_B36_4 = G_B34_3;
		G_B36_5 = G_B34_4;
		goto IL_0553;
	}

IL_0552:
	{
		G_B36_0 = 1;
		G_B36_1 = G_B35_0;
		G_B36_2 = G_B35_1;
		G_B36_3 = G_B35_2;
		G_B36_4 = G_B35_3;
		G_B36_5 = G_B35_4;
	}

IL_0553:
	{
		int32_t L_200 = V_17;
		// cb.GetTemporaryRT(kFilteredReflections, rtW, rtH, 0, k_BilateralUpsample ? FilterMode.Point : FilterMode.Bilinear, intermediateFormat);
		NullCheck(G_B36_5);
		CommandBuffer_GetTemporaryRT_m2252457381(G_B36_5, G_B36_4, G_B36_3, G_B36_2, G_B36_1, G_B36_0, L_200, /*hidden argument*/NULL);
		// cb.GetTemporaryRT(kFinalReflectionTexture, rtW, rtH, 0, FilterMode.Point, intermediateFormat);
		CommandBuffer_t2206337031 * L_201 = ___cb0;
		int32_t L_202 = V_22;
		int32_t L_203 = V_3;
		int32_t L_204 = V_4;
		int32_t L_205 = V_17;
		// cb.GetTemporaryRT(kFinalReflectionTexture, rtW, rtH, 0, FilterMode.Point, intermediateFormat);
		NullCheck(L_201);
		CommandBuffer_GetTemporaryRT_m2252457381(L_201, L_202, L_203, L_204, 0, 0, L_205, /*hidden argument*/NULL);
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kNormalAndRoughnessTexture, material, (int)PassIndex.BilateralKeyPack);
		CommandBuffer_t2206337031 * L_206 = ___cb0;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kNormalAndRoughnessTexture, material, (int)PassIndex.BilateralKeyPack);
		RenderTargetIdentifier_t2079184500  L_207 = RenderTargetIdentifier_op_Implicit_m2644497587(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		int32_t L_208 = V_18;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kNormalAndRoughnessTexture, material, (int)PassIndex.BilateralKeyPack);
		RenderTargetIdentifier_t2079184500  L_209 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_208, /*hidden argument*/NULL);
		Material_t340375123 * L_210 = V_9;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kNormalAndRoughnessTexture, material, (int)PassIndex.BilateralKeyPack);
		NullCheck(L_206);
		CommandBuffer_Blit_m1867893672(L_206, L_207, L_209, L_210, 6, /*hidden argument*/NULL);
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kHitPointTexture, material, (int)PassIndex.RayTraceStep);
		CommandBuffer_t2206337031 * L_211 = ___cb0;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kHitPointTexture, material, (int)PassIndex.RayTraceStep);
		RenderTargetIdentifier_t2079184500  L_212 = RenderTargetIdentifier_op_Implicit_m2644497587(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		int32_t L_213 = V_19;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kHitPointTexture, material, (int)PassIndex.RayTraceStep);
		RenderTargetIdentifier_t2079184500  L_214 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_213, /*hidden argument*/NULL);
		Material_t340375123 * L_215 = V_9;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kHitPointTexture, material, (int)PassIndex.RayTraceStep);
		NullCheck(L_211);
		CommandBuffer_Blit_m1867893672(L_211, L_212, L_214, L_215, 0, /*hidden argument*/NULL);
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kFilteredReflections, material, (int)PassIndex.HitPointToReflections);
		CommandBuffer_t2206337031 * L_216 = ___cb0;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kFilteredReflections, material, (int)PassIndex.HitPointToReflections);
		RenderTargetIdentifier_t2079184500  L_217 = RenderTargetIdentifier_op_Implicit_m2644497587(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		int32_t L_218 = V_21;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kFilteredReflections, material, (int)PassIndex.HitPointToReflections);
		RenderTargetIdentifier_t2079184500  L_219 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_218, /*hidden argument*/NULL);
		Material_t340375123 * L_220 = V_9;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kFilteredReflections, material, (int)PassIndex.HitPointToReflections);
		NullCheck(L_216);
		CommandBuffer_Blit_m1867893672(L_216, L_217, L_219, L_220, 5, /*hidden argument*/NULL);
		// cb.Blit(kFilteredReflections, m_ReflectionTextures[0], material, (int)PassIndex.PoissonBlur);
		CommandBuffer_t2206337031 * L_221 = ___cb0;
		int32_t L_222 = V_21;
		// cb.Blit(kFilteredReflections, m_ReflectionTextures[0], material, (int)PassIndex.PoissonBlur);
		RenderTargetIdentifier_t2079184500  L_223 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_222, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_224 = __this->get_m_ReflectionTextures_6();
		NullCheck(L_224);
		int32_t L_225 = 0;
		int32_t L_226 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_225));
		// cb.Blit(kFilteredReflections, m_ReflectionTextures[0], material, (int)PassIndex.PoissonBlur);
		RenderTargetIdentifier_t2079184500  L_227 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_226, /*hidden argument*/NULL);
		Material_t340375123 * L_228 = V_9;
		// cb.Blit(kFilteredReflections, m_ReflectionTextures[0], material, (int)PassIndex.PoissonBlur);
		NullCheck(L_221);
		CommandBuffer_Blit_m1867893672(L_221, L_223, L_227, L_228, 8, /*hidden argument*/NULL);
		// for (int i = 1; i < maxMip; ++i)
		V_25 = 1;
		goto IL_06a1;
	}

IL_05d0:
	{
		// int inputTex = m_ReflectionTextures[i - 1];
		Int32U5BU5D_t385246372* L_229 = __this->get_m_ReflectionTextures_6();
		int32_t L_230 = V_25;
		NullCheck(L_229);
		int32_t L_231 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_230, (int32_t)1));
		int32_t L_232 = (L_229)->GetAt(static_cast<il2cpp_array_size_t>(L_231));
		V_26 = L_232;
		// int lowMip = i;
		int32_t L_233 = V_25;
		V_27 = L_233;
		// cb.GetTemporaryRT(kBlurTexture, rtW >> lowMip, rtH >> lowMip, 0, FilterMode.Bilinear, intermediateFormat);
		CommandBuffer_t2206337031 * L_234 = ___cb0;
		int32_t L_235 = V_20;
		int32_t L_236 = V_3;
		int32_t L_237 = V_27;
		int32_t L_238 = V_4;
		int32_t L_239 = V_27;
		int32_t L_240 = V_17;
		// cb.GetTemporaryRT(kBlurTexture, rtW >> lowMip, rtH >> lowMip, 0, FilterMode.Bilinear, intermediateFormat);
		NullCheck(L_234);
		CommandBuffer_GetTemporaryRT_m2252457381(L_234, L_235, ((int32_t)((int32_t)L_236>>(int32_t)((int32_t)((int32_t)L_237&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_238>>(int32_t)((int32_t)((int32_t)L_239&(int32_t)((int32_t)31))))), 0, 1, L_240, /*hidden argument*/NULL);
		// cb.SetGlobalVector(Uniforms._Axis, new Vector4(1.0f, 0.0f, 0.0f, 0.0f));
		CommandBuffer_t2206337031 * L_241 = ___cb0;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2970573890_il2cpp_TypeInfo_var);
		int32_t L_242 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__Axis_27();
		// cb.SetGlobalVector(Uniforms._Axis, new Vector4(1.0f, 0.0f, 0.0f, 0.0f));
		Vector4_t3319028937  L_243;
		memset(&L_243, 0, sizeof(L_243));
		Vector4__ctor_m2498754347((&L_243), (1.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// cb.SetGlobalVector(Uniforms._Axis, new Vector4(1.0f, 0.0f, 0.0f, 0.0f));
		NullCheck(L_241);
		CommandBuffer_SetGlobalVector_m2474181847(L_241, L_242, L_243, /*hidden argument*/NULL);
		// cb.SetGlobalFloat(Uniforms._CurrentMipLevel, i - 1.0f);
		CommandBuffer_t2206337031 * L_244 = ___cb0;
		int32_t L_245 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__CurrentMipLevel_28();
		int32_t L_246 = V_25;
		// cb.SetGlobalFloat(Uniforms._CurrentMipLevel, i - 1.0f);
		NullCheck(L_244);
		CommandBuffer_SetGlobalFloat_m4256468291(L_244, L_245, ((float)il2cpp_codegen_subtract((float)(((float)((float)L_246))), (float)(1.0f))), /*hidden argument*/NULL);
		// cb.Blit(inputTex, kBlurTexture, material, (int)PassIndex.Blur);
		CommandBuffer_t2206337031 * L_247 = ___cb0;
		int32_t L_248 = V_26;
		// cb.Blit(inputTex, kBlurTexture, material, (int)PassIndex.Blur);
		RenderTargetIdentifier_t2079184500  L_249 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_248, /*hidden argument*/NULL);
		int32_t L_250 = V_20;
		// cb.Blit(inputTex, kBlurTexture, material, (int)PassIndex.Blur);
		RenderTargetIdentifier_t2079184500  L_251 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_250, /*hidden argument*/NULL);
		Material_t340375123 * L_252 = V_9;
		// cb.Blit(inputTex, kBlurTexture, material, (int)PassIndex.Blur);
		NullCheck(L_247);
		CommandBuffer_Blit_m1867893672(L_247, L_249, L_251, L_252, 2, /*hidden argument*/NULL);
		// cb.SetGlobalVector(Uniforms._Axis, new Vector4(0.0f, 1.0f, 0.0f, 0.0f));
		CommandBuffer_t2206337031 * L_253 = ___cb0;
		int32_t L_254 = ((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->get__Axis_27();
		// cb.SetGlobalVector(Uniforms._Axis, new Vector4(0.0f, 1.0f, 0.0f, 0.0f));
		Vector4_t3319028937  L_255;
		memset(&L_255, 0, sizeof(L_255));
		Vector4__ctor_m2498754347((&L_255), (0.0f), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// cb.SetGlobalVector(Uniforms._Axis, new Vector4(0.0f, 1.0f, 0.0f, 0.0f));
		NullCheck(L_253);
		CommandBuffer_SetGlobalVector_m2474181847(L_253, L_254, L_255, /*hidden argument*/NULL);
		// inputTex = m_ReflectionTextures[i];
		Int32U5BU5D_t385246372* L_256 = __this->get_m_ReflectionTextures_6();
		int32_t L_257 = V_25;
		NullCheck(L_256);
		int32_t L_258 = L_257;
		int32_t L_259 = (L_256)->GetAt(static_cast<il2cpp_array_size_t>(L_258));
		V_26 = L_259;
		// cb.Blit(kBlurTexture, inputTex, material, (int)PassIndex.Blur);
		CommandBuffer_t2206337031 * L_260 = ___cb0;
		int32_t L_261 = V_20;
		// cb.Blit(kBlurTexture, inputTex, material, (int)PassIndex.Blur);
		RenderTargetIdentifier_t2079184500  L_262 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_261, /*hidden argument*/NULL);
		int32_t L_263 = V_26;
		// cb.Blit(kBlurTexture, inputTex, material, (int)PassIndex.Blur);
		RenderTargetIdentifier_t2079184500  L_264 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_263, /*hidden argument*/NULL);
		Material_t340375123 * L_265 = V_9;
		// cb.Blit(kBlurTexture, inputTex, material, (int)PassIndex.Blur);
		NullCheck(L_260);
		CommandBuffer_Blit_m1867893672(L_260, L_262, L_264, L_265, 2, /*hidden argument*/NULL);
		// cb.ReleaseTemporaryRT(kBlurTexture);
		CommandBuffer_t2206337031 * L_266 = ___cb0;
		int32_t L_267 = V_20;
		// cb.ReleaseTemporaryRT(kBlurTexture);
		NullCheck(L_266);
		CommandBuffer_ReleaseTemporaryRT_m2627662573(L_266, L_267, /*hidden argument*/NULL);
		// for (int i = 1; i < maxMip; ++i)
		int32_t L_268 = V_25;
		V_25 = ((int32_t)il2cpp_codegen_add((int32_t)L_268, (int32_t)1));
	}

IL_06a1:
	{
		// for (int i = 1; i < maxMip; ++i)
		int32_t L_269 = V_25;
		if ((((int32_t)L_269) < ((int32_t)5)))
		{
			goto IL_05d0;
		}
	}
	{
		// cb.Blit(m_ReflectionTextures[0], kFinalReflectionTexture, material, (int)PassIndex.CompositeSSR);
		CommandBuffer_t2206337031 * L_270 = ___cb0;
		Int32U5BU5D_t385246372* L_271 = __this->get_m_ReflectionTextures_6();
		NullCheck(L_271);
		int32_t L_272 = 0;
		int32_t L_273 = (L_271)->GetAt(static_cast<il2cpp_array_size_t>(L_272));
		// cb.Blit(m_ReflectionTextures[0], kFinalReflectionTexture, material, (int)PassIndex.CompositeSSR);
		RenderTargetIdentifier_t2079184500  L_274 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_273, /*hidden argument*/NULL);
		int32_t L_275 = V_22;
		// cb.Blit(m_ReflectionTextures[0], kFinalReflectionTexture, material, (int)PassIndex.CompositeSSR);
		RenderTargetIdentifier_t2079184500  L_276 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_275, /*hidden argument*/NULL);
		Material_t340375123 * L_277 = V_9;
		// cb.Blit(m_ReflectionTextures[0], kFinalReflectionTexture, material, (int)PassIndex.CompositeSSR);
		NullCheck(L_270);
		CommandBuffer_Blit_m1867893672(L_270, L_274, L_276, L_277, 3, /*hidden argument*/NULL);
		// cb.GetTemporaryRT(kTempTexture, camera.pixelWidth, camera.pixelHeight, 0, FilterMode.Bilinear, intermediateFormat);
		CommandBuffer_t2206337031 * L_278 = ___cb0;
		int32_t L_279 = V_23;
		Camera_t4157153871 * L_280 = V_1;
		// cb.GetTemporaryRT(kTempTexture, camera.pixelWidth, camera.pixelHeight, 0, FilterMode.Bilinear, intermediateFormat);
		NullCheck(L_280);
		int32_t L_281 = Camera_get_pixelWidth_m1110053668(L_280, /*hidden argument*/NULL);
		Camera_t4157153871 * L_282 = V_1;
		// cb.GetTemporaryRT(kTempTexture, camera.pixelWidth, camera.pixelHeight, 0, FilterMode.Bilinear, intermediateFormat);
		NullCheck(L_282);
		int32_t L_283 = Camera_get_pixelHeight_m722276884(L_282, /*hidden argument*/NULL);
		int32_t L_284 = V_17;
		// cb.GetTemporaryRT(kTempTexture, camera.pixelWidth, camera.pixelHeight, 0, FilterMode.Bilinear, intermediateFormat);
		NullCheck(L_278);
		CommandBuffer_GetTemporaryRT_m2252457381(L_278, L_279, L_281, L_283, 0, 1, L_284, /*hidden argument*/NULL);
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kTempTexture, material, (int)PassIndex.CompositeFinal);
		CommandBuffer_t2206337031 * L_285 = ___cb0;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kTempTexture, material, (int)PassIndex.CompositeFinal);
		RenderTargetIdentifier_t2079184500  L_286 = RenderTargetIdentifier_op_Implicit_m2644497587(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		int32_t L_287 = V_23;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kTempTexture, material, (int)PassIndex.CompositeFinal);
		RenderTargetIdentifier_t2079184500  L_288 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_287, /*hidden argument*/NULL);
		Material_t340375123 * L_289 = V_9;
		// cb.Blit(BuiltinRenderTextureType.CameraTarget, kTempTexture, material, (int)PassIndex.CompositeFinal);
		NullCheck(L_285);
		CommandBuffer_Blit_m1867893672(L_285, L_286, L_288, L_289, 1, /*hidden argument*/NULL);
		// cb.Blit(kTempTexture, BuiltinRenderTextureType.CameraTarget);
		CommandBuffer_t2206337031 * L_290 = ___cb0;
		int32_t L_291 = V_23;
		// cb.Blit(kTempTexture, BuiltinRenderTextureType.CameraTarget);
		RenderTargetIdentifier_t2079184500  L_292 = RenderTargetIdentifier_op_Implicit_m1310414951(NULL /*static, unused*/, L_291, /*hidden argument*/NULL);
		// cb.Blit(kTempTexture, BuiltinRenderTextureType.CameraTarget);
		RenderTargetIdentifier_t2079184500  L_293 = RenderTargetIdentifier_op_Implicit_m2644497587(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		// cb.Blit(kTempTexture, BuiltinRenderTextureType.CameraTarget);
		NullCheck(L_290);
		CommandBuffer_Blit_m1393847922(L_290, L_292, L_293, /*hidden argument*/NULL);
		// cb.ReleaseTemporaryRT(kTempTexture);
		CommandBuffer_t2206337031 * L_294 = ___cb0;
		int32_t L_295 = V_23;
		// cb.ReleaseTemporaryRT(kTempTexture);
		NullCheck(L_294);
		CommandBuffer_ReleaseTemporaryRT_m2627662573(L_294, L_295, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::.cctor()
extern "C"  void Uniforms__cctor_m2613778490 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Uniforms__cctor_m2613778490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static readonly int _RayStepSize                = Shader.PropertyToID("_RayStepSize");
		int32_t L_0 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1292081225, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__RayStepSize_0(L_0);
		// internal static readonly int _AdditiveReflection         = Shader.PropertyToID("_AdditiveReflection");
		int32_t L_1 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3590979936, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__AdditiveReflection_1(L_1);
		// internal static readonly int _BilateralUpsampling        = Shader.PropertyToID("_BilateralUpsampling");
		int32_t L_2 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3812922687, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__BilateralUpsampling_2(L_2);
		// internal static readonly int _TreatBackfaceHitAsMiss     = Shader.PropertyToID("_TreatBackfaceHitAsMiss");
		int32_t L_3 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3220650816, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__TreatBackfaceHitAsMiss_3(L_3);
		// internal static readonly int _AllowBackwardsRays         = Shader.PropertyToID("_AllowBackwardsRays");
		int32_t L_4 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral550721294, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__AllowBackwardsRays_4(L_4);
		// internal static readonly int _TraceBehindObjects         = Shader.PropertyToID("_TraceBehindObjects");
		int32_t L_5 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral591083681, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__TraceBehindObjects_5(L_5);
		// internal static readonly int _MaxSteps                   = Shader.PropertyToID("_MaxSteps");
		int32_t L_6 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral4226617077, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__MaxSteps_6(L_6);
		// internal static readonly int _FullResolutionFiltering    = Shader.PropertyToID("_FullResolutionFiltering");
		int32_t L_7 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2380614554, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__FullResolutionFiltering_7(L_7);
		// internal static readonly int _HalfResolution             = Shader.PropertyToID("_HalfResolution");
		int32_t L_8 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2030135646, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__HalfResolution_8(L_8);
		// internal static readonly int _HighlightSuppression       = Shader.PropertyToID("_HighlightSuppression");
		int32_t L_9 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2271957017, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__HighlightSuppression_9(L_9);
		// internal static readonly int _PixelsPerMeterAtOneMeter   = Shader.PropertyToID("_PixelsPerMeterAtOneMeter");
		int32_t L_10 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2534592956, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__PixelsPerMeterAtOneMeter_10(L_10);
		// internal static readonly int _ScreenEdgeFading           = Shader.PropertyToID("_ScreenEdgeFading");
		int32_t L_11 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3503710369, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__ScreenEdgeFading_11(L_11);
		// internal static readonly int _ReflectionBlur             = Shader.PropertyToID("_ReflectionBlur");
		int32_t L_12 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral4081118877, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__ReflectionBlur_12(L_12);
		// internal static readonly int _MaxRayTraceDistance        = Shader.PropertyToID("_MaxRayTraceDistance");
		int32_t L_13 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3535566692, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__MaxRayTraceDistance_13(L_13);
		// internal static readonly int _FadeDistance               = Shader.PropertyToID("_FadeDistance");
		int32_t L_14 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1464815389, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__FadeDistance_14(L_14);
		// internal static readonly int _LayerThickness             = Shader.PropertyToID("_LayerThickness");
		int32_t L_15 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3054324692, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__LayerThickness_15(L_15);
		// internal static readonly int _SSRMultiplier              = Shader.PropertyToID("_SSRMultiplier");
		int32_t L_16 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral4044860599, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__SSRMultiplier_16(L_16);
		// internal static readonly int _FresnelFade                = Shader.PropertyToID("_FresnelFade");
		int32_t L_17 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2857999720, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__FresnelFade_17(L_17);
		// internal static readonly int _FresnelFadePower           = Shader.PropertyToID("_FresnelFadePower");
		int32_t L_18 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2381300568, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__FresnelFadePower_18(L_18);
		// internal static readonly int _ReflectionBufferSize       = Shader.PropertyToID("_ReflectionBufferSize");
		int32_t L_19 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1469005573, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__ReflectionBufferSize_19(L_19);
		// internal static readonly int _ScreenSize                 = Shader.PropertyToID("_ScreenSize");
		int32_t L_20 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2262278099, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__ScreenSize_20(L_20);
		// internal static readonly int _InvScreenSize              = Shader.PropertyToID("_InvScreenSize");
		int32_t L_21 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral601799897, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__InvScreenSize_21(L_21);
		// internal static readonly int _ProjInfo                   = Shader.PropertyToID("_ProjInfo");
		int32_t L_22 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3142184566, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__ProjInfo_22(L_22);
		// internal static readonly int _CameraClipInfo             = Shader.PropertyToID("_CameraClipInfo");
		int32_t L_23 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1562959127, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__CameraClipInfo_23(L_23);
		// internal static readonly int _ProjectToPixelMatrix       = Shader.PropertyToID("_ProjectToPixelMatrix");
		int32_t L_24 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2920707233, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__ProjectToPixelMatrix_24(L_24);
		// internal static readonly int _WorldToCameraMatrix        = Shader.PropertyToID("_WorldToCameraMatrix");
		int32_t L_25 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2060163631, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__WorldToCameraMatrix_25(L_25);
		// internal static readonly int _CameraToWorldMatrix        = Shader.PropertyToID("_CameraToWorldMatrix");
		int32_t L_26 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3346007495, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__CameraToWorldMatrix_26(L_26);
		// internal static readonly int _Axis                       = Shader.PropertyToID("_Axis");
		int32_t L_27 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral4055059096, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__Axis_27(L_27);
		// internal static readonly int _CurrentMipLevel            = Shader.PropertyToID("_CurrentMipLevel");
		int32_t L_28 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1923191447, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__CurrentMipLevel_28(L_28);
		// internal static readonly int _NormalAndRoughnessTexture  = Shader.PropertyToID("_NormalAndRoughnessTexture");
		int32_t L_29 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral735047140, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__NormalAndRoughnessTexture_29(L_29);
		// internal static readonly int _HitPointTexture            = Shader.PropertyToID("_HitPointTexture");
		int32_t L_30 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral4037140854, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__HitPointTexture_30(L_30);
		// internal static readonly int _BlurTexture                = Shader.PropertyToID("_BlurTexture");
		int32_t L_31 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2224716414, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__BlurTexture_31(L_31);
		// internal static readonly int _FilteredReflections        = Shader.PropertyToID("_FilteredReflections");
		int32_t L_32 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral782944872, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__FilteredReflections_32(L_32);
		// internal static readonly int _FinalReflectionTexture     = Shader.PropertyToID("_FinalReflectionTexture");
		int32_t L_33 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3700046144, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__FinalReflectionTexture_33(L_33);
		// internal static readonly int _TempTexture                = Shader.PropertyToID("_TempTexture");
		int32_t L_34 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1660455728, /*hidden argument*/NULL);
		((Uniforms_t2970573890_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2970573890_il2cpp_TypeInfo_var))->set__TempTexture_34(L_34);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionModel::.ctor()
extern "C"  void ScreenSpaceReflectionModel__ctor_m855607741 (ScreenSpaceReflectionModel_t3026344732 * __this, const RuntimeMethod* method)
{
	{
		// Settings m_Settings = Settings.defaultSettings;
		Settings_t1995791524  L_0 = Settings_get_defaultSettings_m3330699527(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Settings_1(L_0);
		PostProcessingModel__ctor_m4158388095(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel::get_settings()
extern "C"  Settings_t1995791524  ScreenSpaceReflectionModel_get_settings_m3165114047 (ScreenSpaceReflectionModel_t3026344732 * __this, const RuntimeMethod* method)
{
	Settings_t1995791524  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// get { return m_Settings; }
		Settings_t1995791524  L_0 = __this->get_m_Settings_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_Settings; }
		Settings_t1995791524  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionModel::set_settings(UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings)
extern "C"  void ScreenSpaceReflectionModel_set_settings_m3883690039 (ScreenSpaceReflectionModel_t3026344732 * __this, Settings_t1995791524  ___value0, const RuntimeMethod* method)
{
	{
		// set { m_Settings = value; }
		Settings_t1995791524  L_0 = ___value0;
		__this->set_m_Settings_1(L_0);
		// set { m_Settings = value; }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionModel::Reset()
extern "C"  void ScreenSpaceReflectionModel_Reset_m1531575281 (ScreenSpaceReflectionModel_t3026344732 * __this, const RuntimeMethod* method)
{
	{
		// m_Settings = Settings.defaultSettings;
		// m_Settings = Settings.defaultSettings;
		Settings_t1995791524  L_0 = Settings_get_defaultSettings_m3330699527(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Settings_1(L_0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
extern "C" void ReflectionSettings_t282755190_marshal_pinvoke(const ReflectionSettings_t282755190& unmarshaled, ReflectionSettings_t282755190_marshaled_pinvoke& marshaled)
{
	marshaled.___blendType_0 = unmarshaled.get_blendType_0();
	marshaled.___reflectionQuality_1 = unmarshaled.get_reflectionQuality_1();
	marshaled.___maxDistance_2 = unmarshaled.get_maxDistance_2();
	marshaled.___iterationCount_3 = unmarshaled.get_iterationCount_3();
	marshaled.___stepSize_4 = unmarshaled.get_stepSize_4();
	marshaled.___widthModifier_5 = unmarshaled.get_widthModifier_5();
	marshaled.___reflectionBlur_6 = unmarshaled.get_reflectionBlur_6();
	marshaled.___reflectBackfaces_7 = static_cast<int32_t>(unmarshaled.get_reflectBackfaces_7());
}
extern "C" void ReflectionSettings_t282755190_marshal_pinvoke_back(const ReflectionSettings_t282755190_marshaled_pinvoke& marshaled, ReflectionSettings_t282755190& unmarshaled)
{
	int32_t unmarshaled_blendType_temp_0 = 0;
	unmarshaled_blendType_temp_0 = marshaled.___blendType_0;
	unmarshaled.set_blendType_0(unmarshaled_blendType_temp_0);
	int32_t unmarshaled_reflectionQuality_temp_1 = 0;
	unmarshaled_reflectionQuality_temp_1 = marshaled.___reflectionQuality_1;
	unmarshaled.set_reflectionQuality_1(unmarshaled_reflectionQuality_temp_1);
	float unmarshaled_maxDistance_temp_2 = 0.0f;
	unmarshaled_maxDistance_temp_2 = marshaled.___maxDistance_2;
	unmarshaled.set_maxDistance_2(unmarshaled_maxDistance_temp_2);
	int32_t unmarshaled_iterationCount_temp_3 = 0;
	unmarshaled_iterationCount_temp_3 = marshaled.___iterationCount_3;
	unmarshaled.set_iterationCount_3(unmarshaled_iterationCount_temp_3);
	int32_t unmarshaled_stepSize_temp_4 = 0;
	unmarshaled_stepSize_temp_4 = marshaled.___stepSize_4;
	unmarshaled.set_stepSize_4(unmarshaled_stepSize_temp_4);
	float unmarshaled_widthModifier_temp_5 = 0.0f;
	unmarshaled_widthModifier_temp_5 = marshaled.___widthModifier_5;
	unmarshaled.set_widthModifier_5(unmarshaled_widthModifier_temp_5);
	float unmarshaled_reflectionBlur_temp_6 = 0.0f;
	unmarshaled_reflectionBlur_temp_6 = marshaled.___reflectionBlur_6;
	unmarshaled.set_reflectionBlur_6(unmarshaled_reflectionBlur_temp_6);
	bool unmarshaled_reflectBackfaces_temp_7 = false;
	unmarshaled_reflectBackfaces_temp_7 = static_cast<bool>(marshaled.___reflectBackfaces_7);
	unmarshaled.set_reflectBackfaces_7(unmarshaled_reflectBackfaces_temp_7);
}
// Conversion method for clean up from marshalling of: UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
extern "C" void ReflectionSettings_t282755190_marshal_pinvoke_cleanup(ReflectionSettings_t282755190_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
extern "C" void ReflectionSettings_t282755190_marshal_com(const ReflectionSettings_t282755190& unmarshaled, ReflectionSettings_t282755190_marshaled_com& marshaled)
{
	marshaled.___blendType_0 = unmarshaled.get_blendType_0();
	marshaled.___reflectionQuality_1 = unmarshaled.get_reflectionQuality_1();
	marshaled.___maxDistance_2 = unmarshaled.get_maxDistance_2();
	marshaled.___iterationCount_3 = unmarshaled.get_iterationCount_3();
	marshaled.___stepSize_4 = unmarshaled.get_stepSize_4();
	marshaled.___widthModifier_5 = unmarshaled.get_widthModifier_5();
	marshaled.___reflectionBlur_6 = unmarshaled.get_reflectionBlur_6();
	marshaled.___reflectBackfaces_7 = static_cast<int32_t>(unmarshaled.get_reflectBackfaces_7());
}
extern "C" void ReflectionSettings_t282755190_marshal_com_back(const ReflectionSettings_t282755190_marshaled_com& marshaled, ReflectionSettings_t282755190& unmarshaled)
{
	int32_t unmarshaled_blendType_temp_0 = 0;
	unmarshaled_blendType_temp_0 = marshaled.___blendType_0;
	unmarshaled.set_blendType_0(unmarshaled_blendType_temp_0);
	int32_t unmarshaled_reflectionQuality_temp_1 = 0;
	unmarshaled_reflectionQuality_temp_1 = marshaled.___reflectionQuality_1;
	unmarshaled.set_reflectionQuality_1(unmarshaled_reflectionQuality_temp_1);
	float unmarshaled_maxDistance_temp_2 = 0.0f;
	unmarshaled_maxDistance_temp_2 = marshaled.___maxDistance_2;
	unmarshaled.set_maxDistance_2(unmarshaled_maxDistance_temp_2);
	int32_t unmarshaled_iterationCount_temp_3 = 0;
	unmarshaled_iterationCount_temp_3 = marshaled.___iterationCount_3;
	unmarshaled.set_iterationCount_3(unmarshaled_iterationCount_temp_3);
	int32_t unmarshaled_stepSize_temp_4 = 0;
	unmarshaled_stepSize_temp_4 = marshaled.___stepSize_4;
	unmarshaled.set_stepSize_4(unmarshaled_stepSize_temp_4);
	float unmarshaled_widthModifier_temp_5 = 0.0f;
	unmarshaled_widthModifier_temp_5 = marshaled.___widthModifier_5;
	unmarshaled.set_widthModifier_5(unmarshaled_widthModifier_temp_5);
	float unmarshaled_reflectionBlur_temp_6 = 0.0f;
	unmarshaled_reflectionBlur_temp_6 = marshaled.___reflectionBlur_6;
	unmarshaled.set_reflectionBlur_6(unmarshaled_reflectionBlur_temp_6);
	bool unmarshaled_reflectBackfaces_temp_7 = false;
	unmarshaled_reflectBackfaces_temp_7 = static_cast<bool>(marshaled.___reflectBackfaces_7);
	unmarshaled.set_reflectBackfaces_7(unmarshaled_reflectBackfaces_temp_7);
}
// Conversion method for clean up from marshalling of: UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
extern "C" void ReflectionSettings_t282755190_marshal_com_cleanup(ReflectionSettings_t282755190_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif


// Conversion methods for marshalling of: UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
extern "C" void Settings_t1995791524_marshal_pinvoke(const Settings_t1995791524& unmarshaled, Settings_t1995791524_marshaled_pinvoke& marshaled)
{
	ReflectionSettings_t282755190_marshal_pinvoke(unmarshaled.get_reflection_0(), marshaled.___reflection_0);
	marshaled.___intensity_1 = unmarshaled.get_intensity_1();
	marshaled.___screenEdgeMask_2 = unmarshaled.get_screenEdgeMask_2();
}
extern "C" void Settings_t1995791524_marshal_pinvoke_back(const Settings_t1995791524_marshaled_pinvoke& marshaled, Settings_t1995791524& unmarshaled)
{
	ReflectionSettings_t282755190  unmarshaled_reflection_temp_0;
	memset(&unmarshaled_reflection_temp_0, 0, sizeof(unmarshaled_reflection_temp_0));
	ReflectionSettings_t282755190_marshal_pinvoke_back(marshaled.___reflection_0, unmarshaled_reflection_temp_0);
	unmarshaled.set_reflection_0(unmarshaled_reflection_temp_0);
	IntensitySettings_t1721872184  unmarshaled_intensity_temp_1;
	memset(&unmarshaled_intensity_temp_1, 0, sizeof(unmarshaled_intensity_temp_1));
	unmarshaled_intensity_temp_1 = marshaled.___intensity_1;
	unmarshaled.set_intensity_1(unmarshaled_intensity_temp_1);
	ScreenEdgeMask_t4063288584  unmarshaled_screenEdgeMask_temp_2;
	memset(&unmarshaled_screenEdgeMask_temp_2, 0, sizeof(unmarshaled_screenEdgeMask_temp_2));
	unmarshaled_screenEdgeMask_temp_2 = marshaled.___screenEdgeMask_2;
	unmarshaled.set_screenEdgeMask_2(unmarshaled_screenEdgeMask_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
extern "C" void Settings_t1995791524_marshal_pinvoke_cleanup(Settings_t1995791524_marshaled_pinvoke& marshaled)
{
	ReflectionSettings_t282755190_marshal_pinvoke_cleanup(marshaled.___reflection_0);
}


// Conversion methods for marshalling of: UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
extern "C" void Settings_t1995791524_marshal_com(const Settings_t1995791524& unmarshaled, Settings_t1995791524_marshaled_com& marshaled)
{
	ReflectionSettings_t282755190_marshal_com(unmarshaled.get_reflection_0(), marshaled.___reflection_0);
	marshaled.___intensity_1 = unmarshaled.get_intensity_1();
	marshaled.___screenEdgeMask_2 = unmarshaled.get_screenEdgeMask_2();
}
extern "C" void Settings_t1995791524_marshal_com_back(const Settings_t1995791524_marshaled_com& marshaled, Settings_t1995791524& unmarshaled)
{
	ReflectionSettings_t282755190  unmarshaled_reflection_temp_0;
	memset(&unmarshaled_reflection_temp_0, 0, sizeof(unmarshaled_reflection_temp_0));
	ReflectionSettings_t282755190_marshal_com_back(marshaled.___reflection_0, unmarshaled_reflection_temp_0);
	unmarshaled.set_reflection_0(unmarshaled_reflection_temp_0);
	IntensitySettings_t1721872184  unmarshaled_intensity_temp_1;
	memset(&unmarshaled_intensity_temp_1, 0, sizeof(unmarshaled_intensity_temp_1));
	unmarshaled_intensity_temp_1 = marshaled.___intensity_1;
	unmarshaled.set_intensity_1(unmarshaled_intensity_temp_1);
	ScreenEdgeMask_t4063288584  unmarshaled_screenEdgeMask_temp_2;
	memset(&unmarshaled_screenEdgeMask_temp_2, 0, sizeof(unmarshaled_screenEdgeMask_temp_2));
	unmarshaled_screenEdgeMask_temp_2 = marshaled.___screenEdgeMask_2;
	unmarshaled.set_screenEdgeMask_2(unmarshaled_screenEdgeMask_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
extern "C" void Settings_t1995791524_marshal_com_cleanup(Settings_t1995791524_marshaled_com& marshaled)
{
	ReflectionSettings_t282755190_marshal_com_cleanup(marshaled.___reflection_0);
}
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::get_defaultSettings()
extern "C"  Settings_t1995791524  Settings_get_defaultSettings_m3330699527 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Settings_t1995791524  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ReflectionSettings_t282755190  V_1;
	memset(&V_1, 0, sizeof(V_1));
	IntensitySettings_t1721872184  V_2;
	memset(&V_2, 0, sizeof(V_2));
	ScreenEdgeMask_t4063288584  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Settings_t1995791524  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		// return new Settings
		il2cpp_codegen_initobj((&V_0), sizeof(Settings_t1995791524 ));
		// reflection = new ReflectionSettings
		il2cpp_codegen_initobj((&V_1), sizeof(ReflectionSettings_t282755190 ));
		// blendType = SSRReflectionBlendType.PhysicallyBased,
		(&V_1)->set_blendType_0(0);
		// reflectionQuality = SSRResolution.Low,
		(&V_1)->set_reflectionQuality_1(2);
		// maxDistance = 100f,
		(&V_1)->set_maxDistance_2((100.0f));
		// iterationCount = 256,
		(&V_1)->set_iterationCount_3(((int32_t)256));
		// stepSize = 3,
		(&V_1)->set_stepSize_4(3);
		// widthModifier = 0.5f,
		(&V_1)->set_widthModifier_5((0.5f));
		// reflectionBlur = 1f,
		(&V_1)->set_reflectionBlur_6((1.0f));
		// reflectBackfaces = false
		(&V_1)->set_reflectBackfaces_7((bool)0);
		ReflectionSettings_t282755190  L_0 = V_1;
		(&V_0)->set_reflection_0(L_0);
		// intensity = new IntensitySettings
		il2cpp_codegen_initobj((&V_2), sizeof(IntensitySettings_t1721872184 ));
		// reflectionMultiplier = 1f,
		(&V_2)->set_reflectionMultiplier_0((1.0f));
		// fadeDistance = 100f,
		(&V_2)->set_fadeDistance_1((100.0f));
		// fresnelFade = 1f,
		(&V_2)->set_fresnelFade_2((1.0f));
		// fresnelFadePower = 1f,
		(&V_2)->set_fresnelFadePower_3((1.0f));
		IntensitySettings_t1721872184  L_1 = V_2;
		(&V_0)->set_intensity_1(L_1);
		// screenEdgeMask = new ScreenEdgeMask
		il2cpp_codegen_initobj((&V_3), sizeof(ScreenEdgeMask_t4063288584 ));
		// intensity = 0.03f
		(&V_3)->set_intensity_0((0.03f));
		ScreenEdgeMask_t4063288584  L_2 = V_3;
		(&V_0)->set_screenEdgeMask_2(L_2);
		Settings_t1995791524  L_3 = V_0;
		V_4 = L_3;
		goto IL_00cd;
	}

IL_00cd:
	{
		// }
		Settings_t1995791524  L_4 = V_4;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.TaaComponent::.ctor()
extern "C"  void TaaComponent__ctor_m675959233 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaaComponent__ctor_m675959233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly RenderBuffer[] m_MRT = new RenderBuffer[2];
		__this->set_m_MRT_4(((RenderBufferU5BU5D_t1615831949*)SZArrayNew(RenderBufferU5BU5D_t1615831949_il2cpp_TypeInfo_var, (uint32_t)2)));
		// int m_SampleIndex = 0;
		__this->set_m_SampleIndex_5(0);
		// bool m_ResetHistory = true;
		__this->set_m_ResetHistory_6((bool)1);
		PostProcessingComponentRenderTexture_1__ctor_m1485528837(__this, /*hidden argument*/PostProcessingComponentRenderTexture_1__ctor_m1485528837_RuntimeMethod_var);
		return;
	}
}
// System.Boolean UnityEngine.PostProcessing.TaaComponent::get_active()
extern "C"  bool TaaComponent_get_active_m3051132930 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaaComponent_get_active_m3051132930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Settings_t4292431647  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	int32_t G_B6_0 = 0;
	{
		// return model.enabled
		// return model.enabled
		AntialiasingModel_t1521139388 * L_0 = PostProcessingComponent_1_get_model_m3744888901(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m3744888901_RuntimeMethod_var);
		// return model.enabled
		NullCheck(L_0);
		bool L_1 = PostProcessingModel_get_enabled_m2892084724(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		// && model.settings.method == AntialiasingModel.Method.Taa
		AntialiasingModel_t1521139388 * L_2 = PostProcessingComponent_1_get_model_m3744888901(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m3744888901_RuntimeMethod_var);
		// && model.settings.method == AntialiasingModel.Method.Taa
		NullCheck(L_2);
		Settings_t4292431647  L_3 = AntialiasingModel_get_settings_m675444796(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = (&V_0)->get_method_0();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_004f;
		}
	}
	{
		// && SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf)
		bool L_5 = SystemInfo_SupportsRenderTextureFormat_m1663449629(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004f;
		}
	}
	{
		// && SystemInfo.supportsMotionVectors
		bool L_6 = SystemInfo_get_supportsMotionVectors_m46965105(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		PostProcessingContext_t2014408948 * L_7 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// && !context.interrupted;
		NullCheck(L_7);
		bool L_8 = PostProcessingContext_get_interrupted_m1809095682(L_7, /*hidden argument*/NULL);
		G_B6_0 = ((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		goto IL_0050;
	}

IL_004f:
	{
		G_B6_0 = 0;
	}

IL_0050:
	{
		V_1 = (bool)G_B6_0;
		goto IL_0056;
	}

IL_0056:
	{
		// }
		bool L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.DepthTextureMode UnityEngine.PostProcessing.TaaComponent::GetCameraFlags()
extern "C"  int32_t TaaComponent_GetCameraFlags_m2323189770 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return DepthTextureMode.Depth | DepthTextureMode.MotionVectors;
		V_0 = 5;
		goto IL_0008;
	}

IL_0008:
	{
		// }
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.PostProcessing.TaaComponent::ResetHistory()
extern "C"  void TaaComponent_ResetHistory_m3846253241 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method)
{
	{
		// m_ResetHistory = true;
		__this->set_m_ResetHistory_6((bool)1);
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.TaaComponent::SetProjectionMatrix(System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>)
extern "C"  void TaaComponent_SetProjectionMatrix_m2316589171 (TaaComponent_t3791749658 * __this, Func_2_t4093140010 * ___jitteredFunc0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaaComponent_SetProjectionMatrix_m2316589171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TaaSettings_t2709374970  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Settings_t4292431647  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Material_t340375123 * V_3 = NULL;
	Camera_t4157153871 * G_B4_0 = NULL;
	Camera_t4157153871 * G_B3_0 = NULL;
	Matrix4x4_t1817901843  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	Camera_t4157153871 * G_B5_1 = NULL;
	{
		// var settings = model.settings.taaSettings;
		// var settings = model.settings.taaSettings;
		AntialiasingModel_t1521139388 * L_0 = PostProcessingComponent_1_get_model_m3744888901(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m3744888901_RuntimeMethod_var);
		// var settings = model.settings.taaSettings;
		NullCheck(L_0);
		Settings_t4292431647  L_1 = AntialiasingModel_get_settings_m675444796(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		TaaSettings_t2709374970  L_2 = (&V_1)->get_taaSettings_2();
		V_0 = L_2;
		// var jitter = GenerateRandomOffset();
		// var jitter = GenerateRandomOffset();
		Vector2_t2156229523  L_3 = TaaComponent_GenerateRandomOffset_m1416894136(__this, /*hidden argument*/NULL);
		V_2 = L_3;
		// jitter *= settings.jitterSpread;
		Vector2_t2156229523  L_4 = V_2;
		float L_5 = (&V_0)->get_jitterSpread_0();
		// jitter *= settings.jitterSpread;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_6 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		// context.camera.nonJitteredProjectionMatrix = context.camera.projectionMatrix;
		PostProcessingContext_t2014408948 * L_7 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_7);
		Camera_t4157153871 * L_8 = L_7->get_camera_1();
		PostProcessingContext_t2014408948 * L_9 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_9);
		Camera_t4157153871 * L_10 = L_9->get_camera_1();
		// context.camera.nonJitteredProjectionMatrix = context.camera.projectionMatrix;
		NullCheck(L_10);
		Matrix4x4_t1817901843  L_11 = Camera_get_projectionMatrix_m667780853(L_10, /*hidden argument*/NULL);
		// context.camera.nonJitteredProjectionMatrix = context.camera.projectionMatrix;
		NullCheck(L_8);
		Camera_set_nonJitteredProjectionMatrix_m3492270478(L_8, L_11, /*hidden argument*/NULL);
		// if (jitteredFunc != null)
		Func_2_t4093140010 * L_12 = ___jitteredFunc0;
		if (!L_12)
		{
			goto IL_006e;
		}
	}
	{
		// context.camera.projectionMatrix = jitteredFunc(jitter);
		PostProcessingContext_t2014408948 * L_13 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_13);
		Camera_t4157153871 * L_14 = L_13->get_camera_1();
		Func_2_t4093140010 * L_15 = ___jitteredFunc0;
		Vector2_t2156229523  L_16 = V_2;
		// context.camera.projectionMatrix = jitteredFunc(jitter);
		NullCheck(L_15);
		Matrix4x4_t1817901843  L_17 = Func_2_Invoke_m886748628(L_15, L_16, /*hidden argument*/Func_2_Invoke_m886748628_RuntimeMethod_var);
		// context.camera.projectionMatrix = jitteredFunc(jitter);
		NullCheck(L_14);
		Camera_set_projectionMatrix_m3293177686(L_14, L_17, /*hidden argument*/NULL);
		goto IL_00a8;
	}

IL_006e:
	{
		// context.camera.projectionMatrix = context.camera.orthographic
		PostProcessingContext_t2014408948 * L_18 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_18);
		Camera_t4157153871 * L_19 = L_18->get_camera_1();
		PostProcessingContext_t2014408948 * L_20 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_20);
		Camera_t4157153871 * L_21 = L_20->get_camera_1();
		// context.camera.projectionMatrix = context.camera.orthographic
		NullCheck(L_21);
		bool L_22 = Camera_get_orthographic_m2831464531(L_21, /*hidden argument*/NULL);
		G_B3_0 = L_19;
		if (!L_22)
		{
			G_B4_0 = L_19;
			goto IL_009b;
		}
	}
	{
		Vector2_t2156229523  L_23 = V_2;
		// ? GetOrthographicProjectionMatrix(jitter)
		Matrix4x4_t1817901843  L_24 = TaaComponent_GetOrthographicProjectionMatrix_m3494165154(__this, L_23, /*hidden argument*/NULL);
		G_B5_0 = L_24;
		G_B5_1 = G_B3_0;
		goto IL_00a2;
	}

IL_009b:
	{
		Vector2_t2156229523  L_25 = V_2;
		// : GetPerspectiveProjectionMatrix(jitter);
		Matrix4x4_t1817901843  L_26 = TaaComponent_GetPerspectiveProjectionMatrix_m2335334281(__this, L_25, /*hidden argument*/NULL);
		G_B5_0 = L_26;
		G_B5_1 = G_B4_0;
	}

IL_00a2:
	{
		// context.camera.projectionMatrix = context.camera.orthographic
		NullCheck(G_B5_1);
		Camera_set_projectionMatrix_m3293177686(G_B5_1, G_B5_0, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		// context.camera.useJitteredProjectionMatrixForTransparentRendering = false;
		PostProcessingContext_t2014408948 * L_27 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_27);
		Camera_t4157153871 * L_28 = L_27->get_camera_1();
		// context.camera.useJitteredProjectionMatrixForTransparentRendering = false;
		NullCheck(L_28);
		Camera_set_useJitteredProjectionMatrixForTransparentRendering_m1059913304(L_28, (bool)0, /*hidden argument*/NULL);
		// jitter.x /= context.width;
		Vector2_t2156229523 * L_29 = (&V_2);
		float L_30 = L_29->get_x_0();
		PostProcessingContext_t2014408948 * L_31 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// jitter.x /= context.width;
		NullCheck(L_31);
		int32_t L_32 = PostProcessingContext_get_width_m2658937703(L_31, /*hidden argument*/NULL);
		L_29->set_x_0(((float)((float)L_30/(float)(((float)((float)L_32))))));
		// jitter.y /= context.height;
		Vector2_t2156229523 * L_33 = (&V_2);
		float L_34 = L_33->get_y_1();
		PostProcessingContext_t2014408948 * L_35 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// jitter.y /= context.height;
		NullCheck(L_35);
		int32_t L_36 = PostProcessingContext_get_height_m4218042885(L_35, /*hidden argument*/NULL);
		L_33->set_y_1(((float)((float)L_34/(float)(((float)((float)L_36))))));
		// var material = context.materialFactory.Get(k_ShaderString);
		PostProcessingContext_t2014408948 * L_37 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_37);
		MaterialFactory_t2445948724 * L_38 = L_37->get_materialFactory_2();
		// var material = context.materialFactory.Get(k_ShaderString);
		NullCheck(L_38);
		Material_t340375123 * L_39 = MaterialFactory_Get_m4113232693(L_38, _stringLiteral1950142242, /*hidden argument*/NULL);
		V_3 = L_39;
		// material.SetVector(Uniforms._Jitter, jitter);
		Material_t340375123 * L_40 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t3024963833_il2cpp_TypeInfo_var);
		int32_t L_41 = ((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->get__Jitter_0();
		Vector2_t2156229523  L_42 = V_2;
		// material.SetVector(Uniforms._Jitter, jitter);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_43 = Vector4_op_Implicit_m237151757(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._Jitter, jitter);
		NullCheck(L_40);
		Material_SetVector_m2633010038(L_40, L_41, L_43, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.TaaComponent::Render(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void TaaComponent_Render_m2638556758 (TaaComponent_t3791749658 * __this, RenderTexture_t2108887433 * ___source0, RenderTexture_t2108887433 * ___destination1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaaComponent_Render_m2638556758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	TaaSettings_t2709374970  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Settings_t4292431647  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RenderTexture_t2108887433 * V_3 = NULL;
	Material_t340375123 * G_B9_0 = NULL;
	Material_t340375123 * G_B8_0 = NULL;
	int32_t G_B10_0 = 0;
	Material_t340375123 * G_B10_1 = NULL;
	{
		// var material = context.materialFactory.Get(k_ShaderString);
		PostProcessingContext_t2014408948 * L_0 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_0);
		MaterialFactory_t2445948724 * L_1 = L_0->get_materialFactory_2();
		// var material = context.materialFactory.Get(k_ShaderString);
		NullCheck(L_1);
		Material_t340375123 * L_2 = MaterialFactory_Get_m4113232693(L_1, _stringLiteral1950142242, /*hidden argument*/NULL);
		V_0 = L_2;
		// material.shaderKeywords = null;
		Material_t340375123 * L_3 = V_0;
		// material.shaderKeywords = null;
		NullCheck(L_3);
		Material_set_shaderKeywords_m4017377042(L_3, (StringU5BU5D_t1281789340*)(StringU5BU5D_t1281789340*)NULL, /*hidden argument*/NULL);
		// var settings = model.settings.taaSettings;
		// var settings = model.settings.taaSettings;
		AntialiasingModel_t1521139388 * L_4 = PostProcessingComponent_1_get_model_m3744888901(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m3744888901_RuntimeMethod_var);
		// var settings = model.settings.taaSettings;
		NullCheck(L_4);
		Settings_t4292431647  L_5 = AntialiasingModel_get_settings_m675444796(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		TaaSettings_t2709374970  L_6 = (&V_2)->get_taaSettings_2();
		V_1 = L_6;
		// if (m_ResetHistory || m_HistoryTexture == null || m_HistoryTexture.width != source.width || m_HistoryTexture.height != source.height)
		bool L_7 = __this->get_m_ResetHistory_6();
		if (L_7)
		{
			goto IL_007a;
		}
	}
	{
		RenderTexture_t2108887433 * L_8 = __this->get_m_HistoryTexture_7();
		// if (m_ResetHistory || m_HistoryTexture == null || m_HistoryTexture.width != source.width || m_HistoryTexture.height != source.height)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_007a;
		}
	}
	{
		RenderTexture_t2108887433 * L_10 = __this->get_m_HistoryTexture_7();
		// if (m_ResetHistory || m_HistoryTexture == null || m_HistoryTexture.width != source.width || m_HistoryTexture.height != source.height)
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_10);
		RenderTexture_t2108887433 * L_12 = ___source0;
		// if (m_ResetHistory || m_HistoryTexture == null || m_HistoryTexture.width != source.width || m_HistoryTexture.height != source.height)
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_12);
		if ((!(((uint32_t)L_11) == ((uint32_t)L_13))))
		{
			goto IL_007a;
		}
	}
	{
		RenderTexture_t2108887433 * L_14 = __this->get_m_HistoryTexture_7();
		// if (m_ResetHistory || m_HistoryTexture == null || m_HistoryTexture.width != source.width || m_HistoryTexture.height != source.height)
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_14);
		RenderTexture_t2108887433 * L_16 = ___source0;
		// if (m_ResetHistory || m_HistoryTexture == null || m_HistoryTexture.width != source.width || m_HistoryTexture.height != source.height)
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_16);
		if ((((int32_t)L_15) == ((int32_t)L_17)))
		{
			goto IL_00d3;
		}
	}

IL_007a:
	{
		// if (m_HistoryTexture)
		RenderTexture_t2108887433 * L_18 = __this->get_m_HistoryTexture_7();
		// if (m_HistoryTexture)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0096;
		}
	}
	{
		// RenderTexture.ReleaseTemporary(m_HistoryTexture);
		RenderTexture_t2108887433 * L_20 = __this->get_m_HistoryTexture_7();
		// RenderTexture.ReleaseTemporary(m_HistoryTexture);
		RenderTexture_ReleaseTemporary_m2400081536(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
	}

IL_0096:
	{
		// m_HistoryTexture = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		RenderTexture_t2108887433 * L_21 = ___source0;
		// m_HistoryTexture = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_21);
		RenderTexture_t2108887433 * L_23 = ___source0;
		// m_HistoryTexture = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		NullCheck(L_23);
		int32_t L_24 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_23);
		RenderTexture_t2108887433 * L_25 = ___source0;
		// m_HistoryTexture = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		NullCheck(L_25);
		int32_t L_26 = RenderTexture_get_format_m3846871418(L_25, /*hidden argument*/NULL);
		// m_HistoryTexture = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		RenderTexture_t2108887433 * L_27 = RenderTexture_GetTemporary_m3378328322(NULL /*static, unused*/, L_22, L_24, 0, L_26, /*hidden argument*/NULL);
		__this->set_m_HistoryTexture_7(L_27);
		// m_HistoryTexture.name = "TAA History";
		RenderTexture_t2108887433 * L_28 = __this->get_m_HistoryTexture_7();
		// m_HistoryTexture.name = "TAA History";
		NullCheck(L_28);
		Object_set_name_m291480324(L_28, _stringLiteral504576554, /*hidden argument*/NULL);
		// Graphics.Blit(source, m_HistoryTexture, material, 2);
		RenderTexture_t2108887433 * L_29 = ___source0;
		RenderTexture_t2108887433 * L_30 = __this->get_m_HistoryTexture_7();
		Material_t340375123 * L_31 = V_0;
		// Graphics.Blit(source, m_HistoryTexture, material, 2);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m4126770912(NULL /*static, unused*/, L_29, L_30, L_31, 2, /*hidden argument*/NULL);
	}

IL_00d3:
	{
		// material.SetVector(Uniforms._SharpenParameters, new Vector4(settings.sharpen, 0f, 0f, 0f));
		Material_t340375123 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t3024963833_il2cpp_TypeInfo_var);
		int32_t L_33 = ((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->get__SharpenParameters_1();
		float L_34 = (&V_1)->get_sharpen_1();
		// material.SetVector(Uniforms._SharpenParameters, new Vector4(settings.sharpen, 0f, 0f, 0f));
		Vector4_t3319028937  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector4__ctor_m2498754347((&L_35), L_34, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// material.SetVector(Uniforms._SharpenParameters, new Vector4(settings.sharpen, 0f, 0f, 0f));
		NullCheck(L_32);
		Material_SetVector_m2633010038(L_32, L_33, L_35, /*hidden argument*/NULL);
		// material.SetVector(Uniforms._FinalBlendParameters, new Vector4(settings.stationaryBlending, settings.motionBlending, kMotionAmplification, 0f));
		Material_t340375123 * L_36 = V_0;
		int32_t L_37 = ((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->get__FinalBlendParameters_2();
		float L_38 = (&V_1)->get_stationaryBlending_2();
		float L_39 = (&V_1)->get_motionBlending_3();
		// material.SetVector(Uniforms._FinalBlendParameters, new Vector4(settings.stationaryBlending, settings.motionBlending, kMotionAmplification, 0f));
		Vector4_t3319028937  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector4__ctor_m2498754347((&L_40), L_38, L_39, (6000.0f), (0.0f), /*hidden argument*/NULL);
		// material.SetVector(Uniforms._FinalBlendParameters, new Vector4(settings.stationaryBlending, settings.motionBlending, kMotionAmplification, 0f));
		NullCheck(L_36);
		Material_SetVector_m2633010038(L_36, L_37, L_40, /*hidden argument*/NULL);
		// material.SetTexture(Uniforms._MainTex, source);
		Material_t340375123 * L_41 = V_0;
		int32_t L_42 = ((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->get__MainTex_4();
		RenderTexture_t2108887433 * L_43 = ___source0;
		// material.SetTexture(Uniforms._MainTex, source);
		NullCheck(L_41);
		Material_SetTexture_m3009528825(L_41, L_42, L_43, /*hidden argument*/NULL);
		// material.SetTexture(Uniforms._HistoryTex, m_HistoryTexture);
		Material_t340375123 * L_44 = V_0;
		int32_t L_45 = ((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->get__HistoryTex_3();
		RenderTexture_t2108887433 * L_46 = __this->get_m_HistoryTexture_7();
		// material.SetTexture(Uniforms._HistoryTex, m_HistoryTexture);
		NullCheck(L_44);
		Material_SetTexture_m3009528825(L_44, L_45, L_46, /*hidden argument*/NULL);
		// var tempHistory = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		RenderTexture_t2108887433 * L_47 = ___source0;
		// var tempHistory = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		NullCheck(L_47);
		int32_t L_48 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_47);
		RenderTexture_t2108887433 * L_49 = ___source0;
		// var tempHistory = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		NullCheck(L_49);
		int32_t L_50 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_49);
		RenderTexture_t2108887433 * L_51 = ___source0;
		// var tempHistory = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		NullCheck(L_51);
		int32_t L_52 = RenderTexture_get_format_m3846871418(L_51, /*hidden argument*/NULL);
		// var tempHistory = RenderTexture.GetTemporary(source.width, source.height, 0, source.format);
		RenderTexture_t2108887433 * L_53 = RenderTexture_GetTemporary_m3378328322(NULL /*static, unused*/, L_48, L_50, 0, L_52, /*hidden argument*/NULL);
		V_3 = L_53;
		// tempHistory.name = "TAA History";
		RenderTexture_t2108887433 * L_54 = V_3;
		// tempHistory.name = "TAA History";
		NullCheck(L_54);
		Object_set_name_m291480324(L_54, _stringLiteral504576554, /*hidden argument*/NULL);
		// m_MRT[0] = destination.colorBuffer;
		RenderBufferU5BU5D_t1615831949* L_55 = __this->get_m_MRT_4();
		NullCheck(L_55);
		RenderTexture_t2108887433 * L_56 = ___destination1;
		// m_MRT[0] = destination.colorBuffer;
		NullCheck(L_56);
		RenderBuffer_t586150500  L_57 = RenderTexture_get_colorBuffer_m2062927451(L_56, /*hidden argument*/NULL);
		*(RenderBuffer_t586150500 *)((L_55)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_57;
		// m_MRT[1] = tempHistory.colorBuffer;
		RenderBufferU5BU5D_t1615831949* L_58 = __this->get_m_MRT_4();
		NullCheck(L_58);
		RenderTexture_t2108887433 * L_59 = V_3;
		// m_MRT[1] = tempHistory.colorBuffer;
		NullCheck(L_59);
		RenderBuffer_t586150500  L_60 = RenderTexture_get_colorBuffer_m2062927451(L_59, /*hidden argument*/NULL);
		*(RenderBuffer_t586150500 *)((L_58)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_60;
		// Graphics.SetRenderTarget(m_MRT, source.depthBuffer);
		RenderBufferU5BU5D_t1615831949* L_61 = __this->get_m_MRT_4();
		RenderTexture_t2108887433 * L_62 = ___source0;
		// Graphics.SetRenderTarget(m_MRT, source.depthBuffer);
		NullCheck(L_62);
		RenderBuffer_t586150500  L_63 = RenderTexture_get_depthBuffer_m1409937006(L_62, /*hidden argument*/NULL);
		// Graphics.SetRenderTarget(m_MRT, source.depthBuffer);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_SetRenderTarget_m191444692(NULL /*static, unused*/, L_61, L_63, /*hidden argument*/NULL);
		// GraphicsUtils.Blit(material, context.camera.orthographic ? 1 : 0);
		Material_t340375123 * L_64 = V_0;
		PostProcessingContext_t2014408948 * L_65 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_65);
		Camera_t4157153871 * L_66 = L_65->get_camera_1();
		// GraphicsUtils.Blit(material, context.camera.orthographic ? 1 : 0);
		NullCheck(L_66);
		bool L_67 = Camera_get_orthographic_m2831464531(L_66, /*hidden argument*/NULL);
		G_B8_0 = L_64;
		if (!L_67)
		{
			G_B9_0 = L_64;
			goto IL_01bd;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		goto IL_01be;
	}

IL_01bd:
	{
		G_B10_0 = 0;
		G_B10_1 = G_B9_0;
	}

IL_01be:
	{
		// GraphicsUtils.Blit(material, context.camera.orthographic ? 1 : 0);
		GraphicsUtils_Blit_m1958513870(NULL /*static, unused*/, G_B10_1, G_B10_0, /*hidden argument*/NULL);
		// RenderTexture.ReleaseTemporary(m_HistoryTexture);
		RenderTexture_t2108887433 * L_68 = __this->get_m_HistoryTexture_7();
		// RenderTexture.ReleaseTemporary(m_HistoryTexture);
		RenderTexture_ReleaseTemporary_m2400081536(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		// m_HistoryTexture = tempHistory;
		RenderTexture_t2108887433 * L_69 = V_3;
		__this->set_m_HistoryTexture_7(L_69);
		// m_ResetHistory = false;
		__this->set_m_ResetHistory_6((bool)0);
		// }
		return;
	}
}
// System.Single UnityEngine.PostProcessing.TaaComponent::GetHaltonValue(System.Int32,System.Int32)
extern "C"  float TaaComponent_GetHaltonValue_m3411042843 (TaaComponent_t3791749658 * __this, int32_t ___index0, int32_t ___radix1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// float result = 0f;
		V_0 = (0.0f);
		// float fraction = 1f / (float)radix;
		int32_t L_0 = ___radix1;
		V_1 = ((float)((float)(1.0f)/(float)(((float)((float)L_0)))));
		// while (index > 0)
		goto IL_002a;
	}

IL_0015:
	{
		// result += (float)(index % radix) * fraction;
		float L_1 = V_0;
		int32_t L_2 = ___index0;
		int32_t L_3 = ___radix1;
		float L_4 = V_1;
		V_0 = ((float)il2cpp_codegen_add((float)L_1, (float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)((int32_t)L_2%(int32_t)L_3))))), (float)L_4))));
		// index /= radix;
		int32_t L_5 = ___index0;
		int32_t L_6 = ___radix1;
		___index0 = ((int32_t)((int32_t)L_5/(int32_t)L_6));
		// fraction /= (float)radix;
		float L_7 = V_1;
		int32_t L_8 = ___radix1;
		V_1 = ((float)((float)L_7/(float)(((float)((float)L_8)))));
	}

IL_002a:
	{
		// while (index > 0)
		int32_t L_9 = ___index0;
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		// return result;
		float L_10 = V_0;
		V_2 = L_10;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		float L_11 = V_2;
		return L_11;
	}
}
// UnityEngine.Vector2 UnityEngine.PostProcessing.TaaComponent::GenerateRandomOffset()
extern "C"  Vector2_t2156229523  TaaComponent_GenerateRandomOffset_m1416894136 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		// var offset = new Vector2(
		int32_t L_0 = __this->get_m_SampleIndex_5();
		// GetHaltonValue(m_SampleIndex & 1023, 2),
		float L_1 = TaaComponent_GetHaltonValue_m3411042843(__this, ((int32_t)((int32_t)L_0&(int32_t)((int32_t)1023))), 2, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_SampleIndex_5();
		// GetHaltonValue(m_SampleIndex & 1023, 3));
		float L_3 = TaaComponent_GetHaltonValue_m3411042843(__this, ((int32_t)((int32_t)L_2&(int32_t)((int32_t)1023))), 3, /*hidden argument*/NULL);
		// var offset = new Vector2(
		Vector2__ctor_m3970636864((&V_0), L_1, L_3, /*hidden argument*/NULL);
		// if (++m_SampleIndex >= k_SampleCount)
		int32_t L_4 = __this->get_m_SampleIndex_5();
		int32_t L_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
		V_1 = L_5;
		__this->set_m_SampleIndex_5(L_5);
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) < ((int32_t)8)))
		{
			goto IL_004c;
		}
	}
	{
		// m_SampleIndex = 0;
		__this->set_m_SampleIndex_5(0);
	}

IL_004c:
	{
		// return offset;
		Vector2_t2156229523  L_7 = V_0;
		V_2 = L_7;
		goto IL_0053;
	}

IL_0053:
	{
		// }
		Vector2_t2156229523  L_8 = V_2;
		return L_8;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.PostProcessing.TaaComponent::GetPerspectiveProjectionMatrix(UnityEngine.Vector2)
extern "C"  Matrix4x4_t1817901843  TaaComponent_GetPerspectiveProjectionMatrix_m2335334281 (TaaComponent_t3791749658 * __this, Vector2_t2156229523  ___offset0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaaComponent_GetPerspectiveProjectionMatrix_m2335334281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Matrix4x4_t1817901843  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Matrix4x4_t1817901843  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		// float vertical = Mathf.Tan(0.5f * Mathf.Deg2Rad * context.camera.fieldOfView);
		PostProcessingContext_t2014408948 * L_0 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_0);
		Camera_t4157153871 * L_1 = L_0->get_camera_1();
		// float vertical = Mathf.Tan(0.5f * Mathf.Deg2Rad * context.camera.fieldOfView);
		NullCheck(L_1);
		float L_2 = Camera_get_fieldOfView_m1018585504(L_1, /*hidden argument*/NULL);
		// float vertical = Mathf.Tan(0.5f * Mathf.Deg2Rad * context.camera.fieldOfView);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = tanf(((float)il2cpp_codegen_multiply((float)(0.008726646f), (float)L_2)));
		V_0 = L_3;
		// float horizontal = vertical * context.camera.aspect;
		float L_4 = V_0;
		PostProcessingContext_t2014408948 * L_5 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_5);
		Camera_t4157153871 * L_6 = L_5->get_camera_1();
		// float horizontal = vertical * context.camera.aspect;
		NullCheck(L_6);
		float L_7 = Camera_get_aspect_m862507514(L_6, /*hidden argument*/NULL);
		V_1 = ((float)il2cpp_codegen_multiply((float)L_4, (float)L_7));
		// offset.x *= horizontal / (0.5f * context.width);
		Vector2_t2156229523 * L_8 = (&___offset0);
		float L_9 = L_8->get_x_0();
		float L_10 = V_1;
		PostProcessingContext_t2014408948 * L_11 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// offset.x *= horizontal / (0.5f * context.width);
		NullCheck(L_11);
		int32_t L_12 = PostProcessingContext_get_width_m2658937703(L_11, /*hidden argument*/NULL);
		L_8->set_x_0(((float)il2cpp_codegen_multiply((float)L_9, (float)((float)((float)L_10/(float)((float)il2cpp_codegen_multiply((float)(0.5f), (float)(((float)((float)L_12))))))))));
		// offset.y *= vertical / (0.5f * context.height);
		Vector2_t2156229523 * L_13 = (&___offset0);
		float L_14 = L_13->get_y_1();
		float L_15 = V_0;
		PostProcessingContext_t2014408948 * L_16 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// offset.y *= vertical / (0.5f * context.height);
		NullCheck(L_16);
		int32_t L_17 = PostProcessingContext_get_height_m4218042885(L_16, /*hidden argument*/NULL);
		L_13->set_y_1(((float)il2cpp_codegen_multiply((float)L_14, (float)((float)((float)L_15/(float)((float)il2cpp_codegen_multiply((float)(0.5f), (float)(((float)((float)L_17))))))))));
		// float left = (offset.x - horizontal) * context.camera.nearClipPlane;
		float L_18 = (&___offset0)->get_x_0();
		float L_19 = V_1;
		PostProcessingContext_t2014408948 * L_20 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_20);
		Camera_t4157153871 * L_21 = L_20->get_camera_1();
		// float left = (offset.x - horizontal) * context.camera.nearClipPlane;
		NullCheck(L_21);
		float L_22 = Camera_get_nearClipPlane_m837839537(L_21, /*hidden argument*/NULL);
		V_2 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_18, (float)L_19)), (float)L_22));
		// float right = (offset.x + horizontal) * context.camera.nearClipPlane;
		float L_23 = (&___offset0)->get_x_0();
		float L_24 = V_1;
		PostProcessingContext_t2014408948 * L_25 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_25);
		Camera_t4157153871 * L_26 = L_25->get_camera_1();
		// float right = (offset.x + horizontal) * context.camera.nearClipPlane;
		NullCheck(L_26);
		float L_27 = Camera_get_nearClipPlane_m837839537(L_26, /*hidden argument*/NULL);
		V_3 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_add((float)L_23, (float)L_24)), (float)L_27));
		// float top = (offset.y + vertical) * context.camera.nearClipPlane;
		float L_28 = (&___offset0)->get_y_1();
		float L_29 = V_0;
		PostProcessingContext_t2014408948 * L_30 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_30);
		Camera_t4157153871 * L_31 = L_30->get_camera_1();
		// float top = (offset.y + vertical) * context.camera.nearClipPlane;
		NullCheck(L_31);
		float L_32 = Camera_get_nearClipPlane_m837839537(L_31, /*hidden argument*/NULL);
		V_4 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_add((float)L_28, (float)L_29)), (float)L_32));
		// float bottom = (offset.y - vertical) * context.camera.nearClipPlane;
		float L_33 = (&___offset0)->get_y_1();
		float L_34 = V_0;
		PostProcessingContext_t2014408948 * L_35 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_35);
		Camera_t4157153871 * L_36 = L_35->get_camera_1();
		// float bottom = (offset.y - vertical) * context.camera.nearClipPlane;
		NullCheck(L_36);
		float L_37 = Camera_get_nearClipPlane_m837839537(L_36, /*hidden argument*/NULL);
		V_5 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_33, (float)L_34)), (float)L_37));
		// var matrix = new Matrix4x4();
		il2cpp_codegen_initobj((&V_6), sizeof(Matrix4x4_t1817901843 ));
		// matrix[0, 0] = (2f * context.camera.nearClipPlane) / (right - left);
		PostProcessingContext_t2014408948 * L_38 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_38);
		Camera_t4157153871 * L_39 = L_38->get_camera_1();
		// matrix[0, 0] = (2f * context.camera.nearClipPlane) / (right - left);
		NullCheck(L_39);
		float L_40 = Camera_get_nearClipPlane_m837839537(L_39, /*hidden argument*/NULL);
		float L_41 = V_3;
		float L_42 = V_2;
		// matrix[0, 0] = (2f * context.camera.nearClipPlane) / (right - left);
		Matrix4x4_set_Item_m4102745984((&V_6), 0, 0, ((float)((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_40))/(float)((float)il2cpp_codegen_subtract((float)L_41, (float)L_42)))), /*hidden argument*/NULL);
		// matrix[0, 1] = 0f;
		// matrix[0, 1] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 0, 1, (0.0f), /*hidden argument*/NULL);
		// matrix[0, 2] = (right + left) / (right - left);
		float L_43 = V_3;
		float L_44 = V_2;
		float L_45 = V_3;
		float L_46 = V_2;
		// matrix[0, 2] = (right + left) / (right - left);
		Matrix4x4_set_Item_m4102745984((&V_6), 0, 2, ((float)((float)((float)il2cpp_codegen_add((float)L_43, (float)L_44))/(float)((float)il2cpp_codegen_subtract((float)L_45, (float)L_46)))), /*hidden argument*/NULL);
		// matrix[0, 3] = 0f;
		// matrix[0, 3] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 0, 3, (0.0f), /*hidden argument*/NULL);
		// matrix[1, 0] = 0f;
		// matrix[1, 0] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 1, 0, (0.0f), /*hidden argument*/NULL);
		// matrix[1, 1] = (2f * context.camera.nearClipPlane) / (top - bottom);
		PostProcessingContext_t2014408948 * L_47 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_47);
		Camera_t4157153871 * L_48 = L_47->get_camera_1();
		// matrix[1, 1] = (2f * context.camera.nearClipPlane) / (top - bottom);
		NullCheck(L_48);
		float L_49 = Camera_get_nearClipPlane_m837839537(L_48, /*hidden argument*/NULL);
		float L_50 = V_4;
		float L_51 = V_5;
		// matrix[1, 1] = (2f * context.camera.nearClipPlane) / (top - bottom);
		Matrix4x4_set_Item_m4102745984((&V_6), 1, 1, ((float)((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_49))/(float)((float)il2cpp_codegen_subtract((float)L_50, (float)L_51)))), /*hidden argument*/NULL);
		// matrix[1, 2] = (top + bottom) / (top - bottom);
		float L_52 = V_4;
		float L_53 = V_5;
		float L_54 = V_4;
		float L_55 = V_5;
		// matrix[1, 2] = (top + bottom) / (top - bottom);
		Matrix4x4_set_Item_m4102745984((&V_6), 1, 2, ((float)((float)((float)il2cpp_codegen_add((float)L_52, (float)L_53))/(float)((float)il2cpp_codegen_subtract((float)L_54, (float)L_55)))), /*hidden argument*/NULL);
		// matrix[1, 3] = 0f;
		// matrix[1, 3] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 1, 3, (0.0f), /*hidden argument*/NULL);
		// matrix[2, 0] = 0f;
		// matrix[2, 0] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 2, 0, (0.0f), /*hidden argument*/NULL);
		// matrix[2, 1] = 0f;
		// matrix[2, 1] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 2, 1, (0.0f), /*hidden argument*/NULL);
		// matrix[2, 2] = -(context.camera.farClipPlane + context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		PostProcessingContext_t2014408948 * L_56 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_56);
		Camera_t4157153871 * L_57 = L_56->get_camera_1();
		// matrix[2, 2] = -(context.camera.farClipPlane + context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		NullCheck(L_57);
		float L_58 = Camera_get_farClipPlane_m538536689(L_57, /*hidden argument*/NULL);
		PostProcessingContext_t2014408948 * L_59 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_59);
		Camera_t4157153871 * L_60 = L_59->get_camera_1();
		// matrix[2, 2] = -(context.camera.farClipPlane + context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		NullCheck(L_60);
		float L_61 = Camera_get_nearClipPlane_m837839537(L_60, /*hidden argument*/NULL);
		PostProcessingContext_t2014408948 * L_62 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_62);
		Camera_t4157153871 * L_63 = L_62->get_camera_1();
		// matrix[2, 2] = -(context.camera.farClipPlane + context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		NullCheck(L_63);
		float L_64 = Camera_get_farClipPlane_m538536689(L_63, /*hidden argument*/NULL);
		PostProcessingContext_t2014408948 * L_65 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_65);
		Camera_t4157153871 * L_66 = L_65->get_camera_1();
		// matrix[2, 2] = -(context.camera.farClipPlane + context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		NullCheck(L_66);
		float L_67 = Camera_get_nearClipPlane_m837839537(L_66, /*hidden argument*/NULL);
		// matrix[2, 2] = -(context.camera.farClipPlane + context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		Matrix4x4_set_Item_m4102745984((&V_6), 2, 2, ((float)((float)((-((float)il2cpp_codegen_add((float)L_58, (float)L_61))))/(float)((float)il2cpp_codegen_subtract((float)L_64, (float)L_67)))), /*hidden argument*/NULL);
		// matrix[2, 3] = -(2f * context.camera.farClipPlane * context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		PostProcessingContext_t2014408948 * L_68 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_68);
		Camera_t4157153871 * L_69 = L_68->get_camera_1();
		// matrix[2, 3] = -(2f * context.camera.farClipPlane * context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		NullCheck(L_69);
		float L_70 = Camera_get_farClipPlane_m538536689(L_69, /*hidden argument*/NULL);
		PostProcessingContext_t2014408948 * L_71 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_71);
		Camera_t4157153871 * L_72 = L_71->get_camera_1();
		// matrix[2, 3] = -(2f * context.camera.farClipPlane * context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		NullCheck(L_72);
		float L_73 = Camera_get_nearClipPlane_m837839537(L_72, /*hidden argument*/NULL);
		PostProcessingContext_t2014408948 * L_74 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_74);
		Camera_t4157153871 * L_75 = L_74->get_camera_1();
		// matrix[2, 3] = -(2f * context.camera.farClipPlane * context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		NullCheck(L_75);
		float L_76 = Camera_get_farClipPlane_m538536689(L_75, /*hidden argument*/NULL);
		PostProcessingContext_t2014408948 * L_77 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_77);
		Camera_t4157153871 * L_78 = L_77->get_camera_1();
		// matrix[2, 3] = -(2f * context.camera.farClipPlane * context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		NullCheck(L_78);
		float L_79 = Camera_get_nearClipPlane_m837839537(L_78, /*hidden argument*/NULL);
		// matrix[2, 3] = -(2f * context.camera.farClipPlane * context.camera.nearClipPlane) / (context.camera.farClipPlane - context.camera.nearClipPlane);
		Matrix4x4_set_Item_m4102745984((&V_6), 2, 3, ((float)((float)((-((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_70)), (float)L_73))))/(float)((float)il2cpp_codegen_subtract((float)L_76, (float)L_79)))), /*hidden argument*/NULL);
		// matrix[3, 0] = 0f;
		// matrix[3, 0] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 3, 0, (0.0f), /*hidden argument*/NULL);
		// matrix[3, 1] = 0f;
		// matrix[3, 1] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 3, 1, (0.0f), /*hidden argument*/NULL);
		// matrix[3, 2] = -1f;
		// matrix[3, 2] = -1f;
		Matrix4x4_set_Item_m4102745984((&V_6), 3, 2, (-1.0f), /*hidden argument*/NULL);
		// matrix[3, 3] = 0f;
		// matrix[3, 3] = 0f;
		Matrix4x4_set_Item_m4102745984((&V_6), 3, 3, (0.0f), /*hidden argument*/NULL);
		// return matrix;
		Matrix4x4_t1817901843  L_80 = V_6;
		V_7 = L_80;
		goto IL_028b;
	}

IL_028b:
	{
		// }
		Matrix4x4_t1817901843  L_81 = V_7;
		return L_81;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.PostProcessing.TaaComponent::GetOrthographicProjectionMatrix(UnityEngine.Vector2)
extern "C"  Matrix4x4_t1817901843  TaaComponent_GetOrthographicProjectionMatrix_m3494165154 (TaaComponent_t3791749658 * __this, Vector2_t2156229523  ___offset0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaaComponent_GetOrthographicProjectionMatrix_m3494165154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Matrix4x4_t1817901843  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		// float vertical = context.camera.orthographicSize;
		PostProcessingContext_t2014408948 * L_0 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_0);
		Camera_t4157153871 * L_1 = L_0->get_camera_1();
		// float vertical = context.camera.orthographicSize;
		NullCheck(L_1);
		float L_2 = Camera_get_orthographicSize_m3903216845(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// float horizontal = vertical * context.camera.aspect;
		float L_3 = V_0;
		PostProcessingContext_t2014408948 * L_4 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_4);
		Camera_t4157153871 * L_5 = L_4->get_camera_1();
		// float horizontal = vertical * context.camera.aspect;
		NullCheck(L_5);
		float L_6 = Camera_get_aspect_m862507514(L_5, /*hidden argument*/NULL);
		V_1 = ((float)il2cpp_codegen_multiply((float)L_3, (float)L_6));
		// offset.x *= horizontal / (0.5f * context.width);
		Vector2_t2156229523 * L_7 = (&___offset0);
		float L_8 = L_7->get_x_0();
		float L_9 = V_1;
		PostProcessingContext_t2014408948 * L_10 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// offset.x *= horizontal / (0.5f * context.width);
		NullCheck(L_10);
		int32_t L_11 = PostProcessingContext_get_width_m2658937703(L_10, /*hidden argument*/NULL);
		L_7->set_x_0(((float)il2cpp_codegen_multiply((float)L_8, (float)((float)((float)L_9/(float)((float)il2cpp_codegen_multiply((float)(0.5f), (float)(((float)((float)L_11))))))))));
		// offset.y *= vertical / (0.5f * context.height);
		Vector2_t2156229523 * L_12 = (&___offset0);
		float L_13 = L_12->get_y_1();
		float L_14 = V_0;
		PostProcessingContext_t2014408948 * L_15 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// offset.y *= vertical / (0.5f * context.height);
		NullCheck(L_15);
		int32_t L_16 = PostProcessingContext_get_height_m4218042885(L_15, /*hidden argument*/NULL);
		L_12->set_y_1(((float)il2cpp_codegen_multiply((float)L_13, (float)((float)((float)L_14/(float)((float)il2cpp_codegen_multiply((float)(0.5f), (float)(((float)((float)L_16))))))))));
		// float left = offset.x - horizontal;
		float L_17 = (&___offset0)->get_x_0();
		float L_18 = V_1;
		V_2 = ((float)il2cpp_codegen_subtract((float)L_17, (float)L_18));
		// float right = offset.x + horizontal;
		float L_19 = (&___offset0)->get_x_0();
		float L_20 = V_1;
		V_3 = ((float)il2cpp_codegen_add((float)L_19, (float)L_20));
		// float top = offset.y + vertical;
		float L_21 = (&___offset0)->get_y_1();
		float L_22 = V_0;
		V_4 = ((float)il2cpp_codegen_add((float)L_21, (float)L_22));
		// float bottom = offset.y - vertical;
		float L_23 = (&___offset0)->get_y_1();
		float L_24 = V_0;
		V_5 = ((float)il2cpp_codegen_subtract((float)L_23, (float)L_24));
		// return Matrix4x4.Ortho(left, right, bottom, top, context.camera.nearClipPlane, context.camera.farClipPlane);
		float L_25 = V_2;
		float L_26 = V_3;
		float L_27 = V_5;
		float L_28 = V_4;
		PostProcessingContext_t2014408948 * L_29 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_29);
		Camera_t4157153871 * L_30 = L_29->get_camera_1();
		// return Matrix4x4.Ortho(left, right, bottom, top, context.camera.nearClipPlane, context.camera.farClipPlane);
		NullCheck(L_30);
		float L_31 = Camera_get_nearClipPlane_m837839537(L_30, /*hidden argument*/NULL);
		PostProcessingContext_t2014408948 * L_32 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		NullCheck(L_32);
		Camera_t4157153871 * L_33 = L_32->get_camera_1();
		// return Matrix4x4.Ortho(left, right, bottom, top, context.camera.nearClipPlane, context.camera.farClipPlane);
		NullCheck(L_33);
		float L_34 = Camera_get_farClipPlane_m538536689(L_33, /*hidden argument*/NULL);
		// return Matrix4x4.Ortho(left, right, bottom, top, context.camera.nearClipPlane, context.camera.farClipPlane);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_35 = Matrix4x4_Ortho_m1994183957(NULL /*static, unused*/, L_25, L_26, L_27, L_28, L_31, L_34, /*hidden argument*/NULL);
		V_6 = L_35;
		goto IL_00c5;
	}

IL_00c5:
	{
		// }
		Matrix4x4_t1817901843  L_36 = V_6;
		return L_36;
	}
}
// System.Void UnityEngine.PostProcessing.TaaComponent::OnDisable()
extern "C"  void TaaComponent_OnDisable_m1137752543 (TaaComponent_t3791749658 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaaComponent_OnDisable_m1137752543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_HistoryTexture != null)
		RenderTexture_t2108887433 * L_0 = __this->get_m_HistoryTexture_7();
		// if (m_HistoryTexture != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		// RenderTexture.ReleaseTemporary(m_HistoryTexture);
		RenderTexture_t2108887433 * L_2 = __this->get_m_HistoryTexture_7();
		// RenderTexture.ReleaseTemporary(m_HistoryTexture);
		RenderTexture_ReleaseTemporary_m2400081536(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		// m_HistoryTexture = null;
		__this->set_m_HistoryTexture_7((RenderTexture_t2108887433 *)NULL);
		// m_SampleIndex = 0;
		__this->set_m_SampleIndex_5(0);
		// ResetHistory();
		// ResetHistory();
		TaaComponent_ResetHistory_m3846253241(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.TaaComponent/Uniforms::.cctor()
extern "C"  void Uniforms__cctor_m1642638623 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Uniforms__cctor_m1642638623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static int _Jitter               = Shader.PropertyToID("_Jitter");
		int32_t L_0 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral108671656, /*hidden argument*/NULL);
		((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->set__Jitter_0(L_0);
		// internal static int _SharpenParameters    = Shader.PropertyToID("_SharpenParameters");
		int32_t L_1 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1815697245, /*hidden argument*/NULL);
		((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->set__SharpenParameters_1(L_1);
		// internal static int _FinalBlendParameters = Shader.PropertyToID("_FinalBlendParameters");
		int32_t L_2 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2390542409, /*hidden argument*/NULL);
		((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->set__FinalBlendParameters_2(L_2);
		// internal static int _HistoryTex           = Shader.PropertyToID("_HistoryTex");
		int32_t L_3 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3181956072, /*hidden argument*/NULL);
		((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->set__HistoryTex_3(L_3);
		// internal static int _MainTex              = Shader.PropertyToID("_MainTex");
		int32_t L_4 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3184621405, /*hidden argument*/NULL);
		((Uniforms_t3024963833_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t3024963833_il2cpp_TypeInfo_var))->set__MainTex_4(L_4);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.TrackballAttribute::.ctor(System.String)
extern "C"  void TrackballAttribute__ctor_m774296646 (TrackballAttribute_t219960417 * __this, String_t* ___method0, const RuntimeMethod* method)
{
	{
		// public TrackballAttribute(string method)
		PropertyAttribute__ctor_m1017741868(__this, /*hidden argument*/NULL);
		// this.method = method;
		String_t* L_0 = ___method0;
		__this->set_method_0(L_0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.TrackballGroupAttribute::.ctor()
extern "C"  void TrackballGroupAttribute__ctor_m2574378639 (TrackballGroupAttribute_t624107828 * __this, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m1017741868(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.UserLutComponent::.ctor()
extern "C"  void UserLutComponent__ctor_m3557887131 (UserLutComponent_t2843161776 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserLutComponent__ctor_m3557887131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PostProcessingComponentRenderTexture_1__ctor_m3643001101(__this, /*hidden argument*/PostProcessingComponentRenderTexture_1__ctor_m3643001101_RuntimeMethod_var);
		return;
	}
}
// System.Boolean UnityEngine.PostProcessing.UserLutComponent::get_active()
extern "C"  bool UserLutComponent_get_active_m1788341588 (UserLutComponent_t2843161776 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserLutComponent_get_active_m1788341588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Settings_t3006579223  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	int32_t G_B6_0 = 0;
	{
		// var settings = model.settings;
		// var settings = model.settings;
		UserLutModel_t1670108080 * L_0 = PostProcessingComponent_1_get_model_m2956985483(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m2956985483_RuntimeMethod_var);
		// var settings = model.settings;
		NullCheck(L_0);
		Settings_t3006579223  L_1 = UserLutModel_get_settings_m1799133964(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// return model.enabled
		// return model.enabled
		UserLutModel_t1670108080 * L_2 = PostProcessingComponent_1_get_model_m2956985483(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m2956985483_RuntimeMethod_var);
		// return model.enabled
		NullCheck(L_2);
		bool L_3 = PostProcessingModel_get_enabled_m2892084724(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0074;
		}
	}
	{
		Texture2D_t3840446185 * L_4 = (&V_0)->get_lut_0();
		// && settings.lut != null
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0074;
		}
	}
	{
		float L_6 = (&V_0)->get_contribution_1();
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_0074;
		}
	}
	{
		Texture2D_t3840446185 * L_7 = (&V_0)->get_lut_0();
		// && settings.lut.height == (int)Mathf.Sqrt(settings.lut.width)
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_7);
		Texture2D_t3840446185 * L_9 = (&V_0)->get_lut_0();
		// && settings.lut.height == (int)Mathf.Sqrt(settings.lut.width)
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_9);
		// && settings.lut.height == (int)Mathf.Sqrt(settings.lut.width)
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_11 = sqrtf((((float)((float)L_10))));
		if ((!(((uint32_t)L_8) == ((uint32_t)(((int32_t)((int32_t)L_11)))))))
		{
			goto IL_0074;
		}
	}
	{
		PostProcessingContext_t2014408948 * L_12 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// && !context.interrupted;
		NullCheck(L_12);
		bool L_13 = PostProcessingContext_get_interrupted_m1809095682(L_12, /*hidden argument*/NULL);
		G_B6_0 = ((((int32_t)L_13) == ((int32_t)0))? 1 : 0);
		goto IL_0075;
	}

IL_0074:
	{
		G_B6_0 = 0;
	}

IL_0075:
	{
		V_1 = (bool)G_B6_0;
		goto IL_007b;
	}

IL_007b:
	{
		// }
		bool L_14 = V_1;
		return L_14;
	}
}
// System.Void UnityEngine.PostProcessing.UserLutComponent::Prepare(UnityEngine.Material)
extern "C"  void UserLutComponent_Prepare_m1158977240 (UserLutComponent_t2843161776 * __this, Material_t340375123 * ___uberMaterial0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserLutComponent_Prepare_m1158977240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Settings_t3006579223  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// var settings = model.settings;
		// var settings = model.settings;
		UserLutModel_t1670108080 * L_0 = PostProcessingComponent_1_get_model_m2956985483(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m2956985483_RuntimeMethod_var);
		// var settings = model.settings;
		NullCheck(L_0);
		Settings_t3006579223  L_1 = UserLutModel_get_settings_m1799133964(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// uberMaterial.EnableKeyword("USER_LUT");
		Material_t340375123 * L_2 = ___uberMaterial0;
		// uberMaterial.EnableKeyword("USER_LUT");
		NullCheck(L_2);
		Material_EnableKeyword_m329692301(L_2, _stringLiteral3293862872, /*hidden argument*/NULL);
		// uberMaterial.SetTexture(Uniforms._UserLut, settings.lut);
		Material_t340375123 * L_3 = ___uberMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t1046717683_il2cpp_TypeInfo_var);
		int32_t L_4 = ((Uniforms_t1046717683_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t1046717683_il2cpp_TypeInfo_var))->get__UserLut_0();
		Texture2D_t3840446185 * L_5 = (&V_0)->get_lut_0();
		// uberMaterial.SetTexture(Uniforms._UserLut, settings.lut);
		NullCheck(L_3);
		Material_SetTexture_m3009528825(L_3, L_4, L_5, /*hidden argument*/NULL);
		// uberMaterial.SetVector(Uniforms._UserLut_Params, new Vector4(1f / settings.lut.width, 1f / settings.lut.height, settings.lut.height - 1f, settings.contribution));
		Material_t340375123 * L_6 = ___uberMaterial0;
		int32_t L_7 = ((Uniforms_t1046717683_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t1046717683_il2cpp_TypeInfo_var))->get__UserLut_Params_1();
		Texture2D_t3840446185 * L_8 = (&V_0)->get_lut_0();
		// uberMaterial.SetVector(Uniforms._UserLut_Params, new Vector4(1f / settings.lut.width, 1f / settings.lut.height, settings.lut.height - 1f, settings.contribution));
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		Texture2D_t3840446185 * L_10 = (&V_0)->get_lut_0();
		// uberMaterial.SetVector(Uniforms._UserLut_Params, new Vector4(1f / settings.lut.width, 1f / settings.lut.height, settings.lut.height - 1f, settings.contribution));
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_10);
		Texture2D_t3840446185 * L_12 = (&V_0)->get_lut_0();
		// uberMaterial.SetVector(Uniforms._UserLut_Params, new Vector4(1f / settings.lut.width, 1f / settings.lut.height, settings.lut.height - 1f, settings.contribution));
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_12);
		float L_14 = (&V_0)->get_contribution_1();
		// uberMaterial.SetVector(Uniforms._UserLut_Params, new Vector4(1f / settings.lut.width, 1f / settings.lut.height, settings.lut.height - 1f, settings.contribution));
		Vector4_t3319028937  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector4__ctor_m2498754347((&L_15), ((float)((float)(1.0f)/(float)(((float)((float)L_9))))), ((float)((float)(1.0f)/(float)(((float)((float)L_11))))), ((float)il2cpp_codegen_subtract((float)(((float)((float)L_13))), (float)(1.0f))), L_14, /*hidden argument*/NULL);
		// uberMaterial.SetVector(Uniforms._UserLut_Params, new Vector4(1f / settings.lut.width, 1f / settings.lut.height, settings.lut.height - 1f, settings.contribution));
		NullCheck(L_6);
		Material_SetVector_m2633010038(L_6, L_7, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.UserLutComponent::OnGUI()
extern "C"  void UserLutComponent_OnGUI_m2614562252 (UserLutComponent_t2843161776 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserLutComponent_OnGUI_m2614562252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Settings_t3006579223  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		// var settings = model.settings;
		// var settings = model.settings;
		UserLutModel_t1670108080 * L_0 = PostProcessingComponent_1_get_model_m2956985483(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m2956985483_RuntimeMethod_var);
		// var settings = model.settings;
		NullCheck(L_0);
		Settings_t3006579223  L_1 = UserLutModel_get_settings_m1799133964(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var rect = new Rect(context.viewport.x * Screen.width + 8f, 8f, settings.lut.width, settings.lut.height);
		PostProcessingContext_t2014408948 * L_2 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// var rect = new Rect(context.viewport.x * Screen.width + 8f, 8f, settings.lut.width, settings.lut.height);
		NullCheck(L_2);
		Rect_t2360479859  L_3 = PostProcessingContext_get_viewport_m2647794360(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		// var rect = new Rect(context.viewport.x * Screen.width + 8f, 8f, settings.lut.width, settings.lut.height);
		float L_4 = Rect_get_x_m3839990490((&V_2), /*hidden argument*/NULL);
		// var rect = new Rect(context.viewport.x * Screen.width + 8f, 8f, settings.lut.width, settings.lut.height);
		int32_t L_5 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_6 = (&V_0)->get_lut_0();
		// var rect = new Rect(context.viewport.x * Screen.width + 8f, 8f, settings.lut.width, settings.lut.height);
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		Texture2D_t3840446185 * L_8 = (&V_0)->get_lut_0();
		// var rect = new Rect(context.viewport.x * Screen.width + 8f, 8f, settings.lut.width, settings.lut.height);
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_8);
		// var rect = new Rect(context.viewport.x * Screen.width + 8f, 8f, settings.lut.width, settings.lut.height);
		Rect__ctor_m2614021312((&V_1), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_4, (float)(((float)((float)L_5))))), (float)(8.0f))), (8.0f), (((float)((float)L_7))), (((float)((float)L_9))), /*hidden argument*/NULL);
		// GUI.DrawTexture(rect, settings.lut);
		Rect_t2360479859  L_10 = V_1;
		Texture2D_t3840446185 * L_11 = (&V_0)->get_lut_0();
		// GUI.DrawTexture(rect, settings.lut);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.UserLutComponent/Uniforms::.cctor()
extern "C"  void Uniforms__cctor_m521797190 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Uniforms__cctor_m521797190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static readonly int _UserLut        = Shader.PropertyToID("_UserLut");
		int32_t L_0 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral3922522743, /*hidden argument*/NULL);
		((Uniforms_t1046717683_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t1046717683_il2cpp_TypeInfo_var))->set__UserLut_0(L_0);
		// internal static readonly int _UserLut_Params = Shader.PropertyToID("_UserLut_Params");
		int32_t L_1 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral2045738677, /*hidden argument*/NULL);
		((Uniforms_t1046717683_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t1046717683_il2cpp_TypeInfo_var))->set__UserLut_Params_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.UserLutModel::.ctor()
extern "C"  void UserLutModel__ctor_m1936047466 (UserLutModel_t1670108080 * __this, const RuntimeMethod* method)
{
	{
		// Settings m_Settings = Settings.defaultSettings;
		Settings_t3006579223  L_0 = Settings_get_defaultSettings_m1988709150(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Settings_1(L_0);
		PostProcessingModel__ctor_m4158388095(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel::get_settings()
extern "C"  Settings_t3006579223  UserLutModel_get_settings_m1799133964 (UserLutModel_t1670108080 * __this, const RuntimeMethod* method)
{
	Settings_t3006579223  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// get { return m_Settings; }
		Settings_t3006579223  L_0 = __this->get_m_Settings_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_Settings; }
		Settings_t3006579223  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PostProcessing.UserLutModel::set_settings(UnityEngine.PostProcessing.UserLutModel/Settings)
extern "C"  void UserLutModel_set_settings_m1299940782 (UserLutModel_t1670108080 * __this, Settings_t3006579223  ___value0, const RuntimeMethod* method)
{
	{
		// set { m_Settings = value; }
		Settings_t3006579223  L_0 = ___value0;
		__this->set_m_Settings_1(L_0);
		// set { m_Settings = value; }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.UserLutModel::Reset()
extern "C"  void UserLutModel_Reset_m2837745026 (UserLutModel_t1670108080 * __this, const RuntimeMethod* method)
{
	{
		// m_Settings = Settings.defaultSettings;
		// m_Settings = Settings.defaultSettings;
		Settings_t3006579223  L_0 = Settings_get_defaultSettings_m1988709150(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Settings_1(L_0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.PostProcessing.UserLutModel/Settings
extern "C" void Settings_t3006579223_marshal_pinvoke(const Settings_t3006579223& unmarshaled, Settings_t3006579223_marshaled_pinvoke& marshaled)
{
	Exception_t* ___lut_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'lut' of type 'Settings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___lut_0Exception);
}
extern "C" void Settings_t3006579223_marshal_pinvoke_back(const Settings_t3006579223_marshaled_pinvoke& marshaled, Settings_t3006579223& unmarshaled)
{
	Exception_t* ___lut_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'lut' of type 'Settings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___lut_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.PostProcessing.UserLutModel/Settings
extern "C" void Settings_t3006579223_marshal_pinvoke_cleanup(Settings_t3006579223_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.PostProcessing.UserLutModel/Settings
extern "C" void Settings_t3006579223_marshal_com(const Settings_t3006579223& unmarshaled, Settings_t3006579223_marshaled_com& marshaled)
{
	Exception_t* ___lut_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'lut' of type 'Settings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___lut_0Exception);
}
extern "C" void Settings_t3006579223_marshal_com_back(const Settings_t3006579223_marshaled_com& marshaled, Settings_t3006579223& unmarshaled)
{
	Exception_t* ___lut_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'lut' of type 'Settings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___lut_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.PostProcessing.UserLutModel/Settings
extern "C" void Settings_t3006579223_marshal_com_cleanup(Settings_t3006579223_marshaled_com& marshaled)
{
}
// UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel/Settings::get_defaultSettings()
extern "C"  Settings_t3006579223  Settings_get_defaultSettings_m1988709150 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Settings_t3006579223  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Settings_t3006579223  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// return new Settings
		il2cpp_codegen_initobj((&V_0), sizeof(Settings_t3006579223 ));
		// lut = null,
		(&V_0)->set_lut_0((Texture2D_t3840446185 *)NULL);
		// contribution = 1f
		(&V_0)->set_contribution_1((1.0f));
		Settings_t3006579223  L_0 = V_0;
		V_1 = L_0;
		goto IL_0024;
	}

IL_0024:
	{
		// }
		Settings_t3006579223  L_1 = V_1;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.VignetteComponent::.ctor()
extern "C"  void VignetteComponent__ctor_m3640704423 (VignetteComponent_t3243642943 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VignetteComponent__ctor_m3640704423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PostProcessingComponentRenderTexture_1__ctor_m2651217025(__this, /*hidden argument*/PostProcessingComponentRenderTexture_1__ctor_m2651217025_RuntimeMethod_var);
		return;
	}
}
// System.Boolean UnityEngine.PostProcessing.VignetteComponent::get_active()
extern "C"  bool VignetteComponent_get_active_m4124729355 (VignetteComponent_t3243642943 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VignetteComponent_get_active_m4124729355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		// return model.enabled
		// return model.enabled
		VignetteModel_t2845517177 * L_0 = PostProcessingComponent_1_get_model_m2875105431(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m2875105431_RuntimeMethod_var);
		// return model.enabled
		NullCheck(L_0);
		bool L_1 = PostProcessingModel_get_enabled_m2892084724(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		PostProcessingContext_t2014408948 * L_2 = ((PostProcessingComponentBase_t2731103827 *)__this)->get_context_0();
		// && !context.interrupted;
		NullCheck(L_2);
		bool L_3 = PostProcessingContext_get_interrupted_m1809095682(L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.PostProcessing.VignetteComponent::Prepare(UnityEngine.Material)
extern "C"  void VignetteComponent_Prepare_m3452838302 (VignetteComponent_t3243642943 * __this, Material_t340375123 * ___uberMaterial0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VignetteComponent_Prepare_m3452838302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Settings_t1354494600  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	float G_B3_2 = 0.0f;
	int32_t G_B3_3 = 0;
	Material_t340375123 * G_B3_4 = NULL;
	float G_B2_0 = 0.0f;
	float G_B2_1 = 0.0f;
	float G_B2_2 = 0.0f;
	int32_t G_B2_3 = 0;
	Material_t340375123 * G_B2_4 = NULL;
	float G_B4_0 = 0.0f;
	float G_B4_1 = 0.0f;
	float G_B4_2 = 0.0f;
	float G_B4_3 = 0.0f;
	int32_t G_B4_4 = 0;
	Material_t340375123 * G_B4_5 = NULL;
	{
		// var settings = model.settings;
		// var settings = model.settings;
		VignetteModel_t2845517177 * L_0 = PostProcessingComponent_1_get_model_m2875105431(__this, /*hidden argument*/PostProcessingComponent_1_get_model_m2875105431_RuntimeMethod_var);
		// var settings = model.settings;
		NullCheck(L_0);
		Settings_t1354494600  L_1 = VignetteModel_get_settings_m3633002454(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// uberMaterial.SetColor(Uniforms._Vignette_Color, settings.color);
		Material_t340375123 * L_2 = ___uberMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2205824134_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->get__Vignette_Color_0();
		Color_t2555686324  L_4 = (&V_0)->get_color_1();
		// uberMaterial.SetColor(Uniforms._Vignette_Color, settings.color);
		NullCheck(L_2);
		Material_SetColor_m355160541(L_2, L_3, L_4, /*hidden argument*/NULL);
		// if (settings.mode == VignetteModel.Mode.Classic)
		int32_t L_5 = (&V_0)->get_mode_0();
		if (L_5)
		{
			goto IL_00b6;
		}
	}
	{
		// uberMaterial.SetVector(Uniforms._Vignette_Center, settings.center);
		Material_t340375123 * L_6 = ___uberMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2205824134_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->get__Vignette_Center_1();
		Vector2_t2156229523  L_8 = (&V_0)->get_center_2();
		// uberMaterial.SetVector(Uniforms._Vignette_Center, settings.center);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_9 = Vector4_op_Implicit_m237151757(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		// uberMaterial.SetVector(Uniforms._Vignette_Center, settings.center);
		NullCheck(L_6);
		Material_SetVector_m2633010038(L_6, L_7, L_9, /*hidden argument*/NULL);
		// uberMaterial.EnableKeyword("VIGNETTE_CLASSIC");
		Material_t340375123 * L_10 = ___uberMaterial0;
		// uberMaterial.EnableKeyword("VIGNETTE_CLASSIC");
		NullCheck(L_10);
		Material_EnableKeyword_m329692301(L_10, _stringLiteral3727145709, /*hidden argument*/NULL);
		// float roundness = (1f - settings.roundness) * 6f + settings.roundness;
		float L_11 = (&V_0)->get_roundness_5();
		float L_12 = (&V_0)->get_roundness_5();
		V_1 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_11)), (float)(6.0f))), (float)L_12));
		// uberMaterial.SetVector(Uniforms._Vignette_Settings, new Vector4(settings.intensity * 3f, settings.smoothness * 5f, roundness, settings.rounded ? 1f : 0f));
		Material_t340375123 * L_13 = ___uberMaterial0;
		int32_t L_14 = ((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->get__Vignette_Settings_2();
		float L_15 = (&V_0)->get_intensity_3();
		float L_16 = (&V_0)->get_smoothness_4();
		float L_17 = V_1;
		bool L_18 = (&V_0)->get_rounded_8();
		G_B2_0 = L_17;
		G_B2_1 = ((float)il2cpp_codegen_multiply((float)L_16, (float)(5.0f)));
		G_B2_2 = ((float)il2cpp_codegen_multiply((float)L_15, (float)(3.0f)));
		G_B2_3 = L_14;
		G_B2_4 = L_13;
		if (!L_18)
		{
			G_B3_0 = L_17;
			G_B3_1 = ((float)il2cpp_codegen_multiply((float)L_16, (float)(5.0f)));
			G_B3_2 = ((float)il2cpp_codegen_multiply((float)L_15, (float)(3.0f)));
			G_B3_3 = L_14;
			G_B3_4 = L_13;
			goto IL_00a1;
		}
	}
	{
		G_B4_0 = (1.0f);
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		G_B4_3 = G_B2_2;
		G_B4_4 = G_B2_3;
		G_B4_5 = G_B2_4;
		goto IL_00a6;
	}

IL_00a1:
	{
		G_B4_0 = (0.0f);
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
		G_B4_3 = G_B3_2;
		G_B4_4 = G_B3_3;
		G_B4_5 = G_B3_4;
	}

IL_00a6:
	{
		// uberMaterial.SetVector(Uniforms._Vignette_Settings, new Vector4(settings.intensity * 3f, settings.smoothness * 5f, roundness, settings.rounded ? 1f : 0f));
		Vector4_t3319028937  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector4__ctor_m2498754347((&L_19), G_B4_3, G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/NULL);
		// uberMaterial.SetVector(Uniforms._Vignette_Settings, new Vector4(settings.intensity * 3f, settings.smoothness * 5f, roundness, settings.rounded ? 1f : 0f));
		NullCheck(G_B4_5);
		Material_SetVector_m2633010038(G_B4_5, G_B4_4, L_19, /*hidden argument*/NULL);
		goto IL_0119;
	}

IL_00b6:
	{
		// else if (settings.mode == VignetteModel.Mode.Masked)
		int32_t L_20 = (&V_0)->get_mode_0();
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_0119;
		}
	}
	{
		// if (settings.mask != null && settings.opacity > 0f)
		Texture_t3661962703 * L_21 = (&V_0)->get_mask_6();
		// if (settings.mask != null && settings.opacity > 0f)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_21, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0118;
		}
	}
	{
		float L_23 = (&V_0)->get_opacity_7();
		if ((!(((float)L_23) > ((float)(0.0f)))))
		{
			goto IL_0118;
		}
	}
	{
		// uberMaterial.EnableKeyword("VIGNETTE_MASKED");
		Material_t340375123 * L_24 = ___uberMaterial0;
		// uberMaterial.EnableKeyword("VIGNETTE_MASKED");
		NullCheck(L_24);
		Material_EnableKeyword_m329692301(L_24, _stringLiteral3652629709, /*hidden argument*/NULL);
		// uberMaterial.SetTexture(Uniforms._Vignette_Mask, settings.mask);
		Material_t340375123 * L_25 = ___uberMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t2205824134_il2cpp_TypeInfo_var);
		int32_t L_26 = ((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->get__Vignette_Mask_3();
		Texture_t3661962703 * L_27 = (&V_0)->get_mask_6();
		// uberMaterial.SetTexture(Uniforms._Vignette_Mask, settings.mask);
		NullCheck(L_25);
		Material_SetTexture_m3009528825(L_25, L_26, L_27, /*hidden argument*/NULL);
		// uberMaterial.SetFloat(Uniforms._Vignette_Opacity, settings.opacity);
		Material_t340375123 * L_28 = ___uberMaterial0;
		int32_t L_29 = ((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->get__Vignette_Opacity_4();
		float L_30 = (&V_0)->get_opacity_7();
		// uberMaterial.SetFloat(Uniforms._Vignette_Opacity, settings.opacity);
		NullCheck(L_28);
		Material_SetFloat_m1688718093(L_28, L_29, L_30, /*hidden argument*/NULL);
	}

IL_0118:
	{
	}

IL_0119:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.VignetteComponent/Uniforms::.cctor()
extern "C"  void Uniforms__cctor_m4093978548 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Uniforms__cctor_m4093978548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static readonly int _Vignette_Color    = Shader.PropertyToID("_Vignette_Color");
		int32_t L_0 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1569778134, /*hidden argument*/NULL);
		((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->set__Vignette_Color_0(L_0);
		// internal static readonly int _Vignette_Center   = Shader.PropertyToID("_Vignette_Center");
		int32_t L_1 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1253245767, /*hidden argument*/NULL);
		((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->set__Vignette_Center_1(L_1);
		// internal static readonly int _Vignette_Settings = Shader.PropertyToID("_Vignette_Settings");
		int32_t L_2 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral1478126650, /*hidden argument*/NULL);
		((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->set__Vignette_Settings_2(L_2);
		// internal static readonly int _Vignette_Mask     = Shader.PropertyToID("_Vignette_Mask");
		int32_t L_3 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral4174543017, /*hidden argument*/NULL);
		((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->set__Vignette_Mask_3(L_3);
		// internal static readonly int _Vignette_Opacity  = Shader.PropertyToID("_Vignette_Opacity");
		int32_t L_4 = Shader_PropertyToID_m1030499873(NULL /*static, unused*/, _stringLiteral629754547, /*hidden argument*/NULL);
		((Uniforms_t2205824134_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t2205824134_il2cpp_TypeInfo_var))->set__Vignette_Opacity_4(L_4);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PostProcessing.VignetteModel::.ctor()
extern "C"  void VignetteModel__ctor_m512852279 (VignetteModel_t2845517177 * __this, const RuntimeMethod* method)
{
	{
		// Settings m_Settings = Settings.defaultSettings;
		Settings_t1354494600  L_0 = Settings_get_defaultSettings_m3086602550(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Settings_1(L_0);
		PostProcessingModel__ctor_m4158388095(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel::get_settings()
extern "C"  Settings_t1354494600  VignetteModel_get_settings_m3633002454 (VignetteModel_t2845517177 * __this, const RuntimeMethod* method)
{
	Settings_t1354494600  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// get { return m_Settings; }
		Settings_t1354494600  L_0 = __this->get_m_Settings_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		// get { return m_Settings; }
		Settings_t1354494600  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PostProcessing.VignetteModel::set_settings(UnityEngine.PostProcessing.VignetteModel/Settings)
extern "C"  void VignetteModel_set_settings_m1724115687 (VignetteModel_t2845517177 * __this, Settings_t1354494600  ___value0, const RuntimeMethod* method)
{
	{
		// set { m_Settings = value; }
		Settings_t1354494600  L_0 = ___value0;
		__this->set_m_Settings_1(L_0);
		// set { m_Settings = value; }
		return;
	}
}
// System.Void UnityEngine.PostProcessing.VignetteModel::Reset()
extern "C"  void VignetteModel_Reset_m1857303294 (VignetteModel_t2845517177 * __this, const RuntimeMethod* method)
{
	{
		// m_Settings = Settings.defaultSettings;
		// m_Settings = Settings.defaultSettings;
		Settings_t1354494600  L_0 = Settings_get_defaultSettings_m3086602550(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Settings_1(L_0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.PostProcessing.VignetteModel/Settings
extern "C" void Settings_t1354494600_marshal_pinvoke(const Settings_t1354494600& unmarshaled, Settings_t1354494600_marshaled_pinvoke& marshaled)
{
	Exception_t* ___mask_6Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mask' of type 'Settings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mask_6Exception);
}
extern "C" void Settings_t1354494600_marshal_pinvoke_back(const Settings_t1354494600_marshaled_pinvoke& marshaled, Settings_t1354494600& unmarshaled)
{
	Exception_t* ___mask_6Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mask' of type 'Settings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mask_6Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.PostProcessing.VignetteModel/Settings
extern "C" void Settings_t1354494600_marshal_pinvoke_cleanup(Settings_t1354494600_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.PostProcessing.VignetteModel/Settings
extern "C" void Settings_t1354494600_marshal_com(const Settings_t1354494600& unmarshaled, Settings_t1354494600_marshaled_com& marshaled)
{
	Exception_t* ___mask_6Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mask' of type 'Settings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mask_6Exception);
}
extern "C" void Settings_t1354494600_marshal_com_back(const Settings_t1354494600_marshaled_com& marshaled, Settings_t1354494600& unmarshaled)
{
	Exception_t* ___mask_6Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mask' of type 'Settings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mask_6Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.PostProcessing.VignetteModel/Settings
extern "C" void Settings_t1354494600_marshal_com_cleanup(Settings_t1354494600_marshaled_com& marshaled)
{
}
// UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel/Settings::get_defaultSettings()
extern "C"  Settings_t1354494600  Settings_get_defaultSettings_m3086602550 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Settings_t1354494600  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Settings_t1354494600  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// return new Settings
		il2cpp_codegen_initobj((&V_0), sizeof(Settings_t1354494600 ));
		// mode = Mode.Classic,
		(&V_0)->set_mode_0(0);
		// color = new Color(0f, 0f, 0f, 1f),
		// color = new Color(0f, 0f, 0f, 1f),
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2943235014((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		(&V_0)->set_color_1(L_0);
		// center = new Vector2(0.5f, 0.5f),
		// center = new Vector2(0.5f, 0.5f),
		Vector2_t2156229523  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m3970636864((&L_1), (0.5f), (0.5f), /*hidden argument*/NULL);
		(&V_0)->set_center_2(L_1);
		// intensity = 0.45f,
		(&V_0)->set_intensity_3((0.45f));
		// smoothness = 0.2f,
		(&V_0)->set_smoothness_4((0.2f));
		// roundness = 1f,
		(&V_0)->set_roundness_5((1.0f));
		// mask = null,
		(&V_0)->set_mask_6((Texture_t3661962703 *)NULL);
		// opacity = 1f,
		(&V_0)->set_opacity_7((1.0f));
		// rounded = false
		(&V_0)->set_rounded_8((bool)0);
		Settings_t1354494600  L_2 = V_0;
		V_1 = L_2;
		goto IL_008e;
	}

IL_008e:
	{
		// }
		Settings_t1354494600  L_3 = V_1;
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
