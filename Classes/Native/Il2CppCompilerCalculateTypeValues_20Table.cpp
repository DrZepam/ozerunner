﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// MoreMountains.Tools.MMConsole
struct MMConsole_t3000489708;
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>
struct MMStateMachine_1_t2171884985;
// MoreMountains.Tools.MMInput/IMButton/ButtonDownMethodDelegate
struct ButtonDownMethodDelegate_t3308383149;
// MoreMountains.Tools.MMInput/IMButton/ButtonPressedMethodDelegate
struct ButtonPressedMethodDelegate_t2485678181;
// MoreMountains.Tools.MMInput/IMButton/ButtonUpMethodDelegate
struct ButtonUpMethodDelegate_t939453058;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.PostProcessing.PostProcessingContext
struct PostProcessingContext_t2014408948;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Rendering.RenderTargetIdentifier[]
struct RenderTargetIdentifierU5BU5D_t2742279485;
// UnityEngine.PostProcessing.GrainModel
struct GrainModel_t1152882488;
// UnityEngine.PostProcessing.MotionBlurModel
struct MotionBlurModel_t3080286123;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct ScreenSpaceReflectionModel_t3026344732;
// UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings[]
struct FxaaQualitySettingsU5BU5D_t3129287688;
// System.Void
struct Void_t1185182177;
// UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings[]
struct FxaaConsoleSettingsU5BU5D_t1126991587;
// UnityEngine.PostProcessing.UserLutModel
struct UserLutModel_t1670108080;
// UnityEngine.PostProcessing.VignetteModel
struct VignetteModel_t2845517177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.PostProcessing.AntialiasingModel
struct AntialiasingModel_t1521139388;
// UnityEngine.PostProcessing.EyeAdaptationModel
struct EyeAdaptationModel_t242823912;
// UnityEngine.PostProcessing.DitheringModel
struct DitheringModel_t2429005396;
// UnityEngine.PostProcessing.FogModel
struct FogModel_t3620688749;
// UnityEngine.PostProcessing.AmbientOcclusionModel
struct AmbientOcclusionModel_t389471066;
// UnityEngine.PostProcessing.BloomModel
struct BloomModel_t2099727860;
// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct BuiltinDebugViewsModel_t1462618840;
// UnityEngine.PostProcessing.ChromaticAberrationModel
struct ChromaticAberrationModel_t3963399853;
// UnityEngine.PostProcessing.DepthOfFieldModel
struct DepthOfFieldModel_t514067330;
// UnityEngine.PostProcessing.ColorGradingModel
struct ColorGradingModel_t1448048181;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray
struct ArrowArray_t303178545;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1615831949;
// UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter
struct ReconstructionFilter_t705677647;
// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter
struct FrameBlendingFilter_t2699796096;
// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame[]
struct FrameU5BU5D_t1363420656;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// UnityEngine.ComputeShader
struct ComputeShader_t317220254;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Collider
struct Collider_t1773347010;
// MoreMountains.Tools.PoolableObject/Events
struct Events_t2152452284;
// System.Collections.Generic.List`1<MoreMountains.Tools.MultipleObjectPoolerObject>
struct List_1_t3539521104;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;

struct RenderTargetIdentifier_t2079184500 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef UNIFORMS_T4164805197_H
#define UNIFORMS_T4164805197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent/Uniforms
struct  Uniforms_t4164805197  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t4164805197_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_AutoExposure
	int32_t ____AutoExposure_0;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Threshold
	int32_t ____Threshold_1;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Curve
	int32_t ____Curve_2;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_PrefilterOffs
	int32_t ____PrefilterOffs_3;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_SampleScale
	int32_t ____SampleScale_4;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_BaseTex
	int32_t ____BaseTex_5;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_BloomTex
	int32_t ____BloomTex_6;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_Settings
	int32_t ____Bloom_Settings_7;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_DirtTex
	int32_t ____Bloom_DirtTex_8;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_DirtIntensity
	int32_t ____Bloom_DirtIntensity_9;

public:
	inline static int32_t get_offset_of__AutoExposure_0() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____AutoExposure_0)); }
	inline int32_t get__AutoExposure_0() const { return ____AutoExposure_0; }
	inline int32_t* get_address_of__AutoExposure_0() { return &____AutoExposure_0; }
	inline void set__AutoExposure_0(int32_t value)
	{
		____AutoExposure_0 = value;
	}

	inline static int32_t get_offset_of__Threshold_1() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Threshold_1)); }
	inline int32_t get__Threshold_1() const { return ____Threshold_1; }
	inline int32_t* get_address_of__Threshold_1() { return &____Threshold_1; }
	inline void set__Threshold_1(int32_t value)
	{
		____Threshold_1 = value;
	}

	inline static int32_t get_offset_of__Curve_2() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Curve_2)); }
	inline int32_t get__Curve_2() const { return ____Curve_2; }
	inline int32_t* get_address_of__Curve_2() { return &____Curve_2; }
	inline void set__Curve_2(int32_t value)
	{
		____Curve_2 = value;
	}

	inline static int32_t get_offset_of__PrefilterOffs_3() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____PrefilterOffs_3)); }
	inline int32_t get__PrefilterOffs_3() const { return ____PrefilterOffs_3; }
	inline int32_t* get_address_of__PrefilterOffs_3() { return &____PrefilterOffs_3; }
	inline void set__PrefilterOffs_3(int32_t value)
	{
		____PrefilterOffs_3 = value;
	}

	inline static int32_t get_offset_of__SampleScale_4() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____SampleScale_4)); }
	inline int32_t get__SampleScale_4() const { return ____SampleScale_4; }
	inline int32_t* get_address_of__SampleScale_4() { return &____SampleScale_4; }
	inline void set__SampleScale_4(int32_t value)
	{
		____SampleScale_4 = value;
	}

	inline static int32_t get_offset_of__BaseTex_5() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____BaseTex_5)); }
	inline int32_t get__BaseTex_5() const { return ____BaseTex_5; }
	inline int32_t* get_address_of__BaseTex_5() { return &____BaseTex_5; }
	inline void set__BaseTex_5(int32_t value)
	{
		____BaseTex_5 = value;
	}

	inline static int32_t get_offset_of__BloomTex_6() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____BloomTex_6)); }
	inline int32_t get__BloomTex_6() const { return ____BloomTex_6; }
	inline int32_t* get_address_of__BloomTex_6() { return &____BloomTex_6; }
	inline void set__BloomTex_6(int32_t value)
	{
		____BloomTex_6 = value;
	}

	inline static int32_t get_offset_of__Bloom_Settings_7() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_Settings_7)); }
	inline int32_t get__Bloom_Settings_7() const { return ____Bloom_Settings_7; }
	inline int32_t* get_address_of__Bloom_Settings_7() { return &____Bloom_Settings_7; }
	inline void set__Bloom_Settings_7(int32_t value)
	{
		____Bloom_Settings_7 = value;
	}

	inline static int32_t get_offset_of__Bloom_DirtTex_8() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_DirtTex_8)); }
	inline int32_t get__Bloom_DirtTex_8() const { return ____Bloom_DirtTex_8; }
	inline int32_t* get_address_of__Bloom_DirtTex_8() { return &____Bloom_DirtTex_8; }
	inline void set__Bloom_DirtTex_8(int32_t value)
	{
		____Bloom_DirtTex_8 = value;
	}

	inline static int32_t get_offset_of__Bloom_DirtIntensity_9() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_DirtIntensity_9)); }
	inline int32_t get__Bloom_DirtIntensity_9() const { return ____Bloom_DirtIntensity_9; }
	inline int32_t* get_address_of__Bloom_DirtIntensity_9() { return &____Bloom_DirtIntensity_9; }
	inline void set__Bloom_DirtIntensity_9(int32_t value)
	{
		____Bloom_DirtIntensity_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T4164805197_H
#ifndef UNIFORMS_T3797733410_H
#define UNIFORMS_T3797733410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms
struct  Uniforms_t3797733410  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3797733410_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Intensity
	int32_t ____Intensity_0;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Radius
	int32_t ____Radius_1;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Downsample
	int32_t ____Downsample_2;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_SampleCount
	int32_t ____SampleCount_3;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture1
	int32_t ____OcclusionTexture1_4;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture2
	int32_t ____OcclusionTexture2_5;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture
	int32_t ____OcclusionTexture_6;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_MainTex
	int32_t ____MainTex_7;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_TempRT
	int32_t ____TempRT_8;

public:
	inline static int32_t get_offset_of__Intensity_0() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Intensity_0)); }
	inline int32_t get__Intensity_0() const { return ____Intensity_0; }
	inline int32_t* get_address_of__Intensity_0() { return &____Intensity_0; }
	inline void set__Intensity_0(int32_t value)
	{
		____Intensity_0 = value;
	}

	inline static int32_t get_offset_of__Radius_1() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Radius_1)); }
	inline int32_t get__Radius_1() const { return ____Radius_1; }
	inline int32_t* get_address_of__Radius_1() { return &____Radius_1; }
	inline void set__Radius_1(int32_t value)
	{
		____Radius_1 = value;
	}

	inline static int32_t get_offset_of__Downsample_2() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Downsample_2)); }
	inline int32_t get__Downsample_2() const { return ____Downsample_2; }
	inline int32_t* get_address_of__Downsample_2() { return &____Downsample_2; }
	inline void set__Downsample_2(int32_t value)
	{
		____Downsample_2 = value;
	}

	inline static int32_t get_offset_of__SampleCount_3() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____SampleCount_3)); }
	inline int32_t get__SampleCount_3() const { return ____SampleCount_3; }
	inline int32_t* get_address_of__SampleCount_3() { return &____SampleCount_3; }
	inline void set__SampleCount_3(int32_t value)
	{
		____SampleCount_3 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture1_4() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture1_4)); }
	inline int32_t get__OcclusionTexture1_4() const { return ____OcclusionTexture1_4; }
	inline int32_t* get_address_of__OcclusionTexture1_4() { return &____OcclusionTexture1_4; }
	inline void set__OcclusionTexture1_4(int32_t value)
	{
		____OcclusionTexture1_4 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture2_5() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture2_5)); }
	inline int32_t get__OcclusionTexture2_5() const { return ____OcclusionTexture2_5; }
	inline int32_t* get_address_of__OcclusionTexture2_5() { return &____OcclusionTexture2_5; }
	inline void set__OcclusionTexture2_5(int32_t value)
	{
		____OcclusionTexture2_5 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture_6() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture_6)); }
	inline int32_t get__OcclusionTexture_6() const { return ____OcclusionTexture_6; }
	inline int32_t* get_address_of__OcclusionTexture_6() { return &____OcclusionTexture_6; }
	inline void set__OcclusionTexture_6(int32_t value)
	{
		____OcclusionTexture_6 = value;
	}

	inline static int32_t get_offset_of__MainTex_7() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____MainTex_7)); }
	inline int32_t get__MainTex_7() const { return ____MainTex_7; }
	inline int32_t* get_address_of__MainTex_7() { return &____MainTex_7; }
	inline void set__MainTex_7(int32_t value)
	{
		____MainTex_7 = value;
	}

	inline static int32_t get_offset_of__TempRT_8() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____TempRT_8)); }
	inline int32_t get__TempRT_8() const { return ____TempRT_8; }
	inline int32_t* get_address_of__TempRT_8() { return &____TempRT_8; }
	inline void set__TempRT_8(int32_t value)
	{
		____TempRT_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3797733410_H
#ifndef UNIFORMS_T424361460_H
#define UNIFORMS_T424361460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms
struct  Uniforms_t424361460  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t424361460_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms::_ChromaticAberration_Amount
	int32_t ____ChromaticAberration_Amount_0;
	// System.Int32 UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms::_ChromaticAberration_Spectrum
	int32_t ____ChromaticAberration_Spectrum_1;

public:
	inline static int32_t get_offset_of__ChromaticAberration_Amount_0() { return static_cast<int32_t>(offsetof(Uniforms_t424361460_StaticFields, ____ChromaticAberration_Amount_0)); }
	inline int32_t get__ChromaticAberration_Amount_0() const { return ____ChromaticAberration_Amount_0; }
	inline int32_t* get_address_of__ChromaticAberration_Amount_0() { return &____ChromaticAberration_Amount_0; }
	inline void set__ChromaticAberration_Amount_0(int32_t value)
	{
		____ChromaticAberration_Amount_0 = value;
	}

	inline static int32_t get_offset_of__ChromaticAberration_Spectrum_1() { return static_cast<int32_t>(offsetof(Uniforms_t424361460_StaticFields, ____ChromaticAberration_Spectrum_1)); }
	inline int32_t get__ChromaticAberration_Spectrum_1() const { return ____ChromaticAberration_Spectrum_1; }
	inline int32_t* get_address_of__ChromaticAberration_Spectrum_1() { return &____ChromaticAberration_Spectrum_1; }
	inline void set__ChromaticAberration_Spectrum_1(int32_t value)
	{
		____ChromaticAberration_Spectrum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T424361460_H
#ifndef ARROWARRAY_T303178545_H
#define ARROWARRAY_T303178545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray
struct  ArrowArray_t303178545  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<mesh>k__BackingField
	Mesh_t3648964284 * ___U3CmeshU3Ek__BackingField_0;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<columnCount>k__BackingField
	int32_t ___U3CcolumnCountU3Ek__BackingField_1;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<rowCount>k__BackingField
	int32_t ___U3CrowCountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmeshU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CmeshU3Ek__BackingField_0)); }
	inline Mesh_t3648964284 * get_U3CmeshU3Ek__BackingField_0() const { return ___U3CmeshU3Ek__BackingField_0; }
	inline Mesh_t3648964284 ** get_address_of_U3CmeshU3Ek__BackingField_0() { return &___U3CmeshU3Ek__BackingField_0; }
	inline void set_U3CmeshU3Ek__BackingField_0(Mesh_t3648964284 * value)
	{
		___U3CmeshU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcolumnCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CcolumnCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CcolumnCountU3Ek__BackingField_1() const { return ___U3CcolumnCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CcolumnCountU3Ek__BackingField_1() { return &___U3CcolumnCountU3Ek__BackingField_1; }
	inline void set_U3CcolumnCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CcolumnCountU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CrowCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CrowCountU3Ek__BackingField_2)); }
	inline int32_t get_U3CrowCountU3Ek__BackingField_2() const { return ___U3CrowCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CrowCountU3Ek__BackingField_2() { return &___U3CrowCountU3Ek__BackingField_2; }
	inline void set_U3CrowCountU3Ek__BackingField_2(int32_t value)
	{
		___U3CrowCountU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWARRAY_T303178545_H
#ifndef UNIFORMS_T2158582951_H
#define UNIFORMS_T2158582951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms
struct  Uniforms_t2158582951  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2158582951_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_DepthScale
	int32_t ____DepthScale_0;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_TempRT
	int32_t ____TempRT_1;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Opacity
	int32_t ____Opacity_2;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_MainTex
	int32_t ____MainTex_3;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_TempRT2
	int32_t ____TempRT2_4;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Amplitude
	int32_t ____Amplitude_5;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Scale
	int32_t ____Scale_6;

public:
	inline static int32_t get_offset_of__DepthScale_0() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____DepthScale_0)); }
	inline int32_t get__DepthScale_0() const { return ____DepthScale_0; }
	inline int32_t* get_address_of__DepthScale_0() { return &____DepthScale_0; }
	inline void set__DepthScale_0(int32_t value)
	{
		____DepthScale_0 = value;
	}

	inline static int32_t get_offset_of__TempRT_1() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____TempRT_1)); }
	inline int32_t get__TempRT_1() const { return ____TempRT_1; }
	inline int32_t* get_address_of__TempRT_1() { return &____TempRT_1; }
	inline void set__TempRT_1(int32_t value)
	{
		____TempRT_1 = value;
	}

	inline static int32_t get_offset_of__Opacity_2() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Opacity_2)); }
	inline int32_t get__Opacity_2() const { return ____Opacity_2; }
	inline int32_t* get_address_of__Opacity_2() { return &____Opacity_2; }
	inline void set__Opacity_2(int32_t value)
	{
		____Opacity_2 = value;
	}

	inline static int32_t get_offset_of__MainTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____MainTex_3)); }
	inline int32_t get__MainTex_3() const { return ____MainTex_3; }
	inline int32_t* get_address_of__MainTex_3() { return &____MainTex_3; }
	inline void set__MainTex_3(int32_t value)
	{
		____MainTex_3 = value;
	}

	inline static int32_t get_offset_of__TempRT2_4() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____TempRT2_4)); }
	inline int32_t get__TempRT2_4() const { return ____TempRT2_4; }
	inline int32_t* get_address_of__TempRT2_4() { return &____TempRT2_4; }
	inline void set__TempRT2_4(int32_t value)
	{
		____TempRT2_4 = value;
	}

	inline static int32_t get_offset_of__Amplitude_5() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Amplitude_5)); }
	inline int32_t get__Amplitude_5() const { return ____Amplitude_5; }
	inline int32_t* get_address_of__Amplitude_5() { return &____Amplitude_5; }
	inline void set__Amplitude_5(int32_t value)
	{
		____Amplitude_5 = value;
	}

	inline static int32_t get_offset_of__Scale_6() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Scale_6)); }
	inline int32_t get__Scale_6() const { return ____Scale_6; }
	inline int32_t* get_address_of__Scale_6() { return &____Scale_6; }
	inline void set__Scale_6(int32_t value)
	{
		____Scale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2158582951_H
#ifndef UNIFORMS_T1046717683_H
#define UNIFORMS_T1046717683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutComponent/Uniforms
struct  Uniforms_t1046717683  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1046717683_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.UserLutComponent/Uniforms::_UserLut
	int32_t ____UserLut_0;
	// System.Int32 UnityEngine.PostProcessing.UserLutComponent/Uniforms::_UserLut_Params
	int32_t ____UserLut_Params_1;

public:
	inline static int32_t get_offset_of__UserLut_0() { return static_cast<int32_t>(offsetof(Uniforms_t1046717683_StaticFields, ____UserLut_0)); }
	inline int32_t get__UserLut_0() const { return ____UserLut_0; }
	inline int32_t* get_address_of__UserLut_0() { return &____UserLut_0; }
	inline void set__UserLut_0(int32_t value)
	{
		____UserLut_0 = value;
	}

	inline static int32_t get_offset_of__UserLut_Params_1() { return static_cast<int32_t>(offsetof(Uniforms_t1046717683_StaticFields, ____UserLut_Params_1)); }
	inline int32_t get__UserLut_Params_1() const { return ____UserLut_Params_1; }
	inline int32_t* get_address_of__UserLut_Params_1() { return &____UserLut_Params_1; }
	inline void set__UserLut_Params_1(int32_t value)
	{
		____UserLut_Params_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1046717683_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef MULTIPLEOBJECTPOOLEROBJECT_T2067446362_H
#define MULTIPLEOBJECTPOOLEROBJECT_T2067446362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MultipleObjectPoolerObject
struct  MultipleObjectPoolerObject_t2067446362  : public RuntimeObject
{
public:
	// UnityEngine.GameObject MoreMountains.Tools.MultipleObjectPoolerObject::GameObjectToPool
	GameObject_t1113636619 * ___GameObjectToPool_0;
	// System.Int32 MoreMountains.Tools.MultipleObjectPoolerObject::PoolSize
	int32_t ___PoolSize_1;
	// System.Boolean MoreMountains.Tools.MultipleObjectPoolerObject::PoolCanExpand
	bool ___PoolCanExpand_2;
	// System.Boolean MoreMountains.Tools.MultipleObjectPoolerObject::Enabled
	bool ___Enabled_3;

public:
	inline static int32_t get_offset_of_GameObjectToPool_0() { return static_cast<int32_t>(offsetof(MultipleObjectPoolerObject_t2067446362, ___GameObjectToPool_0)); }
	inline GameObject_t1113636619 * get_GameObjectToPool_0() const { return ___GameObjectToPool_0; }
	inline GameObject_t1113636619 ** get_address_of_GameObjectToPool_0() { return &___GameObjectToPool_0; }
	inline void set_GameObjectToPool_0(GameObject_t1113636619 * value)
	{
		___GameObjectToPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___GameObjectToPool_0), value);
	}

	inline static int32_t get_offset_of_PoolSize_1() { return static_cast<int32_t>(offsetof(MultipleObjectPoolerObject_t2067446362, ___PoolSize_1)); }
	inline int32_t get_PoolSize_1() const { return ___PoolSize_1; }
	inline int32_t* get_address_of_PoolSize_1() { return &___PoolSize_1; }
	inline void set_PoolSize_1(int32_t value)
	{
		___PoolSize_1 = value;
	}

	inline static int32_t get_offset_of_PoolCanExpand_2() { return static_cast<int32_t>(offsetof(MultipleObjectPoolerObject_t2067446362, ___PoolCanExpand_2)); }
	inline bool get_PoolCanExpand_2() const { return ___PoolCanExpand_2; }
	inline bool* get_address_of_PoolCanExpand_2() { return &___PoolCanExpand_2; }
	inline void set_PoolCanExpand_2(bool value)
	{
		___PoolCanExpand_2 = value;
	}

	inline static int32_t get_offset_of_Enabled_3() { return static_cast<int32_t>(offsetof(MultipleObjectPoolerObject_t2067446362, ___Enabled_3)); }
	inline bool get_Enabled_3() const { return ___Enabled_3; }
	inline bool* get_address_of_Enabled_3() { return &___Enabled_3; }
	inline void set_Enabled_3(bool value)
	{
		___Enabled_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPLEOBJECTPOOLEROBJECT_T2067446362_H
#ifndef POSTPROCESSINGMODEL_T540111976_H
#define POSTPROCESSINGMODEL_T540111976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingModel
struct  PostProcessingModel_t540111976  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.PostProcessing.PostProcessingModel::m_Enabled
	bool ___m_Enabled_0;

public:
	inline static int32_t get_offset_of_m_Enabled_0() { return static_cast<int32_t>(offsetof(PostProcessingModel_t540111976, ___m_Enabled_0)); }
	inline bool get_m_Enabled_0() const { return ___m_Enabled_0; }
	inline bool* get_address_of_m_Enabled_0() { return &___m_Enabled_0; }
	inline void set_m_Enabled_0(bool value)
	{
		___m_Enabled_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGMODEL_T540111976_H
#ifndef SAVEMANAGER_T380343877_H
#define SAVEMANAGER_T380343877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.SaveManager
struct  SaveManager_t380343877  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEMANAGER_T380343877_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNIFORMS_T589754008_H
#define UNIFORMS_T589754008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/Uniforms
struct  Uniforms_t589754008  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t589754008_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_VelocityScale
	int32_t ____VelocityScale_0;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_MaxBlurRadius
	int32_t ____MaxBlurRadius_1;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_RcpMaxBlurRadius
	int32_t ____RcpMaxBlurRadius_2;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_VelocityTex
	int32_t ____VelocityTex_3;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_MainTex
	int32_t ____MainTex_4;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_Tile2RT
	int32_t ____Tile2RT_5;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_Tile4RT
	int32_t ____Tile4RT_6;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_Tile8RT
	int32_t ____Tile8RT_7;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_TileMaxOffs
	int32_t ____TileMaxOffs_8;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_TileMaxLoop
	int32_t ____TileMaxLoop_9;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_TileVRT
	int32_t ____TileVRT_10;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_NeighborMaxTex
	int32_t ____NeighborMaxTex_11;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_LoopCount
	int32_t ____LoopCount_12;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_TempRT
	int32_t ____TempRT_13;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History1LumaTex
	int32_t ____History1LumaTex_14;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History2LumaTex
	int32_t ____History2LumaTex_15;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History3LumaTex
	int32_t ____History3LumaTex_16;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History4LumaTex
	int32_t ____History4LumaTex_17;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History1ChromaTex
	int32_t ____History1ChromaTex_18;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History2ChromaTex
	int32_t ____History2ChromaTex_19;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History3ChromaTex
	int32_t ____History3ChromaTex_20;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History4ChromaTex
	int32_t ____History4ChromaTex_21;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History1Weight
	int32_t ____History1Weight_22;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History2Weight
	int32_t ____History2Weight_23;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History3Weight
	int32_t ____History3Weight_24;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History4Weight
	int32_t ____History4Weight_25;

public:
	inline static int32_t get_offset_of__VelocityScale_0() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____VelocityScale_0)); }
	inline int32_t get__VelocityScale_0() const { return ____VelocityScale_0; }
	inline int32_t* get_address_of__VelocityScale_0() { return &____VelocityScale_0; }
	inline void set__VelocityScale_0(int32_t value)
	{
		____VelocityScale_0 = value;
	}

	inline static int32_t get_offset_of__MaxBlurRadius_1() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____MaxBlurRadius_1)); }
	inline int32_t get__MaxBlurRadius_1() const { return ____MaxBlurRadius_1; }
	inline int32_t* get_address_of__MaxBlurRadius_1() { return &____MaxBlurRadius_1; }
	inline void set__MaxBlurRadius_1(int32_t value)
	{
		____MaxBlurRadius_1 = value;
	}

	inline static int32_t get_offset_of__RcpMaxBlurRadius_2() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____RcpMaxBlurRadius_2)); }
	inline int32_t get__RcpMaxBlurRadius_2() const { return ____RcpMaxBlurRadius_2; }
	inline int32_t* get_address_of__RcpMaxBlurRadius_2() { return &____RcpMaxBlurRadius_2; }
	inline void set__RcpMaxBlurRadius_2(int32_t value)
	{
		____RcpMaxBlurRadius_2 = value;
	}

	inline static int32_t get_offset_of__VelocityTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____VelocityTex_3)); }
	inline int32_t get__VelocityTex_3() const { return ____VelocityTex_3; }
	inline int32_t* get_address_of__VelocityTex_3() { return &____VelocityTex_3; }
	inline void set__VelocityTex_3(int32_t value)
	{
		____VelocityTex_3 = value;
	}

	inline static int32_t get_offset_of__MainTex_4() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____MainTex_4)); }
	inline int32_t get__MainTex_4() const { return ____MainTex_4; }
	inline int32_t* get_address_of__MainTex_4() { return &____MainTex_4; }
	inline void set__MainTex_4(int32_t value)
	{
		____MainTex_4 = value;
	}

	inline static int32_t get_offset_of__Tile2RT_5() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____Tile2RT_5)); }
	inline int32_t get__Tile2RT_5() const { return ____Tile2RT_5; }
	inline int32_t* get_address_of__Tile2RT_5() { return &____Tile2RT_5; }
	inline void set__Tile2RT_5(int32_t value)
	{
		____Tile2RT_5 = value;
	}

	inline static int32_t get_offset_of__Tile4RT_6() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____Tile4RT_6)); }
	inline int32_t get__Tile4RT_6() const { return ____Tile4RT_6; }
	inline int32_t* get_address_of__Tile4RT_6() { return &____Tile4RT_6; }
	inline void set__Tile4RT_6(int32_t value)
	{
		____Tile4RT_6 = value;
	}

	inline static int32_t get_offset_of__Tile8RT_7() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____Tile8RT_7)); }
	inline int32_t get__Tile8RT_7() const { return ____Tile8RT_7; }
	inline int32_t* get_address_of__Tile8RT_7() { return &____Tile8RT_7; }
	inline void set__Tile8RT_7(int32_t value)
	{
		____Tile8RT_7 = value;
	}

	inline static int32_t get_offset_of__TileMaxOffs_8() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____TileMaxOffs_8)); }
	inline int32_t get__TileMaxOffs_8() const { return ____TileMaxOffs_8; }
	inline int32_t* get_address_of__TileMaxOffs_8() { return &____TileMaxOffs_8; }
	inline void set__TileMaxOffs_8(int32_t value)
	{
		____TileMaxOffs_8 = value;
	}

	inline static int32_t get_offset_of__TileMaxLoop_9() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____TileMaxLoop_9)); }
	inline int32_t get__TileMaxLoop_9() const { return ____TileMaxLoop_9; }
	inline int32_t* get_address_of__TileMaxLoop_9() { return &____TileMaxLoop_9; }
	inline void set__TileMaxLoop_9(int32_t value)
	{
		____TileMaxLoop_9 = value;
	}

	inline static int32_t get_offset_of__TileVRT_10() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____TileVRT_10)); }
	inline int32_t get__TileVRT_10() const { return ____TileVRT_10; }
	inline int32_t* get_address_of__TileVRT_10() { return &____TileVRT_10; }
	inline void set__TileVRT_10(int32_t value)
	{
		____TileVRT_10 = value;
	}

	inline static int32_t get_offset_of__NeighborMaxTex_11() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____NeighborMaxTex_11)); }
	inline int32_t get__NeighborMaxTex_11() const { return ____NeighborMaxTex_11; }
	inline int32_t* get_address_of__NeighborMaxTex_11() { return &____NeighborMaxTex_11; }
	inline void set__NeighborMaxTex_11(int32_t value)
	{
		____NeighborMaxTex_11 = value;
	}

	inline static int32_t get_offset_of__LoopCount_12() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____LoopCount_12)); }
	inline int32_t get__LoopCount_12() const { return ____LoopCount_12; }
	inline int32_t* get_address_of__LoopCount_12() { return &____LoopCount_12; }
	inline void set__LoopCount_12(int32_t value)
	{
		____LoopCount_12 = value;
	}

	inline static int32_t get_offset_of__TempRT_13() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____TempRT_13)); }
	inline int32_t get__TempRT_13() const { return ____TempRT_13; }
	inline int32_t* get_address_of__TempRT_13() { return &____TempRT_13; }
	inline void set__TempRT_13(int32_t value)
	{
		____TempRT_13 = value;
	}

	inline static int32_t get_offset_of__History1LumaTex_14() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History1LumaTex_14)); }
	inline int32_t get__History1LumaTex_14() const { return ____History1LumaTex_14; }
	inline int32_t* get_address_of__History1LumaTex_14() { return &____History1LumaTex_14; }
	inline void set__History1LumaTex_14(int32_t value)
	{
		____History1LumaTex_14 = value;
	}

	inline static int32_t get_offset_of__History2LumaTex_15() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History2LumaTex_15)); }
	inline int32_t get__History2LumaTex_15() const { return ____History2LumaTex_15; }
	inline int32_t* get_address_of__History2LumaTex_15() { return &____History2LumaTex_15; }
	inline void set__History2LumaTex_15(int32_t value)
	{
		____History2LumaTex_15 = value;
	}

	inline static int32_t get_offset_of__History3LumaTex_16() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History3LumaTex_16)); }
	inline int32_t get__History3LumaTex_16() const { return ____History3LumaTex_16; }
	inline int32_t* get_address_of__History3LumaTex_16() { return &____History3LumaTex_16; }
	inline void set__History3LumaTex_16(int32_t value)
	{
		____History3LumaTex_16 = value;
	}

	inline static int32_t get_offset_of__History4LumaTex_17() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History4LumaTex_17)); }
	inline int32_t get__History4LumaTex_17() const { return ____History4LumaTex_17; }
	inline int32_t* get_address_of__History4LumaTex_17() { return &____History4LumaTex_17; }
	inline void set__History4LumaTex_17(int32_t value)
	{
		____History4LumaTex_17 = value;
	}

	inline static int32_t get_offset_of__History1ChromaTex_18() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History1ChromaTex_18)); }
	inline int32_t get__History1ChromaTex_18() const { return ____History1ChromaTex_18; }
	inline int32_t* get_address_of__History1ChromaTex_18() { return &____History1ChromaTex_18; }
	inline void set__History1ChromaTex_18(int32_t value)
	{
		____History1ChromaTex_18 = value;
	}

	inline static int32_t get_offset_of__History2ChromaTex_19() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History2ChromaTex_19)); }
	inline int32_t get__History2ChromaTex_19() const { return ____History2ChromaTex_19; }
	inline int32_t* get_address_of__History2ChromaTex_19() { return &____History2ChromaTex_19; }
	inline void set__History2ChromaTex_19(int32_t value)
	{
		____History2ChromaTex_19 = value;
	}

	inline static int32_t get_offset_of__History3ChromaTex_20() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History3ChromaTex_20)); }
	inline int32_t get__History3ChromaTex_20() const { return ____History3ChromaTex_20; }
	inline int32_t* get_address_of__History3ChromaTex_20() { return &____History3ChromaTex_20; }
	inline void set__History3ChromaTex_20(int32_t value)
	{
		____History3ChromaTex_20 = value;
	}

	inline static int32_t get_offset_of__History4ChromaTex_21() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History4ChromaTex_21)); }
	inline int32_t get__History4ChromaTex_21() const { return ____History4ChromaTex_21; }
	inline int32_t* get_address_of__History4ChromaTex_21() { return &____History4ChromaTex_21; }
	inline void set__History4ChromaTex_21(int32_t value)
	{
		____History4ChromaTex_21 = value;
	}

	inline static int32_t get_offset_of__History1Weight_22() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History1Weight_22)); }
	inline int32_t get__History1Weight_22() const { return ____History1Weight_22; }
	inline int32_t* get_address_of__History1Weight_22() { return &____History1Weight_22; }
	inline void set__History1Weight_22(int32_t value)
	{
		____History1Weight_22 = value;
	}

	inline static int32_t get_offset_of__History2Weight_23() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History2Weight_23)); }
	inline int32_t get__History2Weight_23() const { return ____History2Weight_23; }
	inline int32_t* get_address_of__History2Weight_23() { return &____History2Weight_23; }
	inline void set__History2Weight_23(int32_t value)
	{
		____History2Weight_23 = value;
	}

	inline static int32_t get_offset_of__History3Weight_24() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History3Weight_24)); }
	inline int32_t get__History3Weight_24() const { return ____History3Weight_24; }
	inline int32_t* get_address_of__History3Weight_24() { return &____History3Weight_24; }
	inline void set__History3Weight_24(int32_t value)
	{
		____History3Weight_24 = value;
	}

	inline static int32_t get_offset_of__History4Weight_25() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History4Weight_25)); }
	inline int32_t get__History4Weight_25() const { return ____History4Weight_25; }
	inline int32_t* get_address_of__History4Weight_25() { return &____History4Weight_25; }
	inline void set__History4Weight_25(int32_t value)
	{
		____History4Weight_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T589754008_H
#ifndef UNIFORMS_T1442519687_H
#define UNIFORMS_T1442519687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainComponent/Uniforms
struct  Uniforms_t1442519687  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1442519687_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Grain_Params1
	int32_t ____Grain_Params1_0;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Grain_Params2
	int32_t ____Grain_Params2_1;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_GrainTex
	int32_t ____GrainTex_2;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Phase
	int32_t ____Phase_3;

public:
	inline static int32_t get_offset_of__Grain_Params1_0() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Grain_Params1_0)); }
	inline int32_t get__Grain_Params1_0() const { return ____Grain_Params1_0; }
	inline int32_t* get_address_of__Grain_Params1_0() { return &____Grain_Params1_0; }
	inline void set__Grain_Params1_0(int32_t value)
	{
		____Grain_Params1_0 = value;
	}

	inline static int32_t get_offset_of__Grain_Params2_1() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Grain_Params2_1)); }
	inline int32_t get__Grain_Params2_1() const { return ____Grain_Params2_1; }
	inline int32_t* get_address_of__Grain_Params2_1() { return &____Grain_Params2_1; }
	inline void set__Grain_Params2_1(int32_t value)
	{
		____Grain_Params2_1 = value;
	}

	inline static int32_t get_offset_of__GrainTex_2() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____GrainTex_2)); }
	inline int32_t get__GrainTex_2() const { return ____GrainTex_2; }
	inline int32_t* get_address_of__GrainTex_2() { return &____GrainTex_2; }
	inline void set__GrainTex_2(int32_t value)
	{
		____GrainTex_2 = value;
	}

	inline static int32_t get_offset_of__Phase_3() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Phase_3)); }
	inline int32_t get__Phase_3() const { return ____Phase_3; }
	inline int32_t* get_address_of__Phase_3() { return &____Phase_3; }
	inline void set__Phase_3(int32_t value)
	{
		____Phase_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1442519687_H
#ifndef UNIFORMS_T2970573890_H
#define UNIFORMS_T2970573890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms
struct  Uniforms_t2970573890  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2970573890_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_RayStepSize
	int32_t ____RayStepSize_0;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_AdditiveReflection
	int32_t ____AdditiveReflection_1;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_BilateralUpsampling
	int32_t ____BilateralUpsampling_2;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TreatBackfaceHitAsMiss
	int32_t ____TreatBackfaceHitAsMiss_3;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_AllowBackwardsRays
	int32_t ____AllowBackwardsRays_4;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TraceBehindObjects
	int32_t ____TraceBehindObjects_5;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_MaxSteps
	int32_t ____MaxSteps_6;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FullResolutionFiltering
	int32_t ____FullResolutionFiltering_7;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HalfResolution
	int32_t ____HalfResolution_8;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HighlightSuppression
	int32_t ____HighlightSuppression_9;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_PixelsPerMeterAtOneMeter
	int32_t ____PixelsPerMeterAtOneMeter_10;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ScreenEdgeFading
	int32_t ____ScreenEdgeFading_11;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ReflectionBlur
	int32_t ____ReflectionBlur_12;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_MaxRayTraceDistance
	int32_t ____MaxRayTraceDistance_13;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FadeDistance
	int32_t ____FadeDistance_14;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_LayerThickness
	int32_t ____LayerThickness_15;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_SSRMultiplier
	int32_t ____SSRMultiplier_16;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FresnelFade
	int32_t ____FresnelFade_17;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FresnelFadePower
	int32_t ____FresnelFadePower_18;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ReflectionBufferSize
	int32_t ____ReflectionBufferSize_19;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ScreenSize
	int32_t ____ScreenSize_20;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_InvScreenSize
	int32_t ____InvScreenSize_21;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ProjInfo
	int32_t ____ProjInfo_22;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CameraClipInfo
	int32_t ____CameraClipInfo_23;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ProjectToPixelMatrix
	int32_t ____ProjectToPixelMatrix_24;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_WorldToCameraMatrix
	int32_t ____WorldToCameraMatrix_25;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CameraToWorldMatrix
	int32_t ____CameraToWorldMatrix_26;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_Axis
	int32_t ____Axis_27;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CurrentMipLevel
	int32_t ____CurrentMipLevel_28;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_NormalAndRoughnessTexture
	int32_t ____NormalAndRoughnessTexture_29;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HitPointTexture
	int32_t ____HitPointTexture_30;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_BlurTexture
	int32_t ____BlurTexture_31;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FilteredReflections
	int32_t ____FilteredReflections_32;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FinalReflectionTexture
	int32_t ____FinalReflectionTexture_33;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TempTexture
	int32_t ____TempTexture_34;

public:
	inline static int32_t get_offset_of__RayStepSize_0() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____RayStepSize_0)); }
	inline int32_t get__RayStepSize_0() const { return ____RayStepSize_0; }
	inline int32_t* get_address_of__RayStepSize_0() { return &____RayStepSize_0; }
	inline void set__RayStepSize_0(int32_t value)
	{
		____RayStepSize_0 = value;
	}

	inline static int32_t get_offset_of__AdditiveReflection_1() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____AdditiveReflection_1)); }
	inline int32_t get__AdditiveReflection_1() const { return ____AdditiveReflection_1; }
	inline int32_t* get_address_of__AdditiveReflection_1() { return &____AdditiveReflection_1; }
	inline void set__AdditiveReflection_1(int32_t value)
	{
		____AdditiveReflection_1 = value;
	}

	inline static int32_t get_offset_of__BilateralUpsampling_2() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____BilateralUpsampling_2)); }
	inline int32_t get__BilateralUpsampling_2() const { return ____BilateralUpsampling_2; }
	inline int32_t* get_address_of__BilateralUpsampling_2() { return &____BilateralUpsampling_2; }
	inline void set__BilateralUpsampling_2(int32_t value)
	{
		____BilateralUpsampling_2 = value;
	}

	inline static int32_t get_offset_of__TreatBackfaceHitAsMiss_3() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TreatBackfaceHitAsMiss_3)); }
	inline int32_t get__TreatBackfaceHitAsMiss_3() const { return ____TreatBackfaceHitAsMiss_3; }
	inline int32_t* get_address_of__TreatBackfaceHitAsMiss_3() { return &____TreatBackfaceHitAsMiss_3; }
	inline void set__TreatBackfaceHitAsMiss_3(int32_t value)
	{
		____TreatBackfaceHitAsMiss_3 = value;
	}

	inline static int32_t get_offset_of__AllowBackwardsRays_4() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____AllowBackwardsRays_4)); }
	inline int32_t get__AllowBackwardsRays_4() const { return ____AllowBackwardsRays_4; }
	inline int32_t* get_address_of__AllowBackwardsRays_4() { return &____AllowBackwardsRays_4; }
	inline void set__AllowBackwardsRays_4(int32_t value)
	{
		____AllowBackwardsRays_4 = value;
	}

	inline static int32_t get_offset_of__TraceBehindObjects_5() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TraceBehindObjects_5)); }
	inline int32_t get__TraceBehindObjects_5() const { return ____TraceBehindObjects_5; }
	inline int32_t* get_address_of__TraceBehindObjects_5() { return &____TraceBehindObjects_5; }
	inline void set__TraceBehindObjects_5(int32_t value)
	{
		____TraceBehindObjects_5 = value;
	}

	inline static int32_t get_offset_of__MaxSteps_6() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____MaxSteps_6)); }
	inline int32_t get__MaxSteps_6() const { return ____MaxSteps_6; }
	inline int32_t* get_address_of__MaxSteps_6() { return &____MaxSteps_6; }
	inline void set__MaxSteps_6(int32_t value)
	{
		____MaxSteps_6 = value;
	}

	inline static int32_t get_offset_of__FullResolutionFiltering_7() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FullResolutionFiltering_7)); }
	inline int32_t get__FullResolutionFiltering_7() const { return ____FullResolutionFiltering_7; }
	inline int32_t* get_address_of__FullResolutionFiltering_7() { return &____FullResolutionFiltering_7; }
	inline void set__FullResolutionFiltering_7(int32_t value)
	{
		____FullResolutionFiltering_7 = value;
	}

	inline static int32_t get_offset_of__HalfResolution_8() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HalfResolution_8)); }
	inline int32_t get__HalfResolution_8() const { return ____HalfResolution_8; }
	inline int32_t* get_address_of__HalfResolution_8() { return &____HalfResolution_8; }
	inline void set__HalfResolution_8(int32_t value)
	{
		____HalfResolution_8 = value;
	}

	inline static int32_t get_offset_of__HighlightSuppression_9() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HighlightSuppression_9)); }
	inline int32_t get__HighlightSuppression_9() const { return ____HighlightSuppression_9; }
	inline int32_t* get_address_of__HighlightSuppression_9() { return &____HighlightSuppression_9; }
	inline void set__HighlightSuppression_9(int32_t value)
	{
		____HighlightSuppression_9 = value;
	}

	inline static int32_t get_offset_of__PixelsPerMeterAtOneMeter_10() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____PixelsPerMeterAtOneMeter_10)); }
	inline int32_t get__PixelsPerMeterAtOneMeter_10() const { return ____PixelsPerMeterAtOneMeter_10; }
	inline int32_t* get_address_of__PixelsPerMeterAtOneMeter_10() { return &____PixelsPerMeterAtOneMeter_10; }
	inline void set__PixelsPerMeterAtOneMeter_10(int32_t value)
	{
		____PixelsPerMeterAtOneMeter_10 = value;
	}

	inline static int32_t get_offset_of__ScreenEdgeFading_11() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ScreenEdgeFading_11)); }
	inline int32_t get__ScreenEdgeFading_11() const { return ____ScreenEdgeFading_11; }
	inline int32_t* get_address_of__ScreenEdgeFading_11() { return &____ScreenEdgeFading_11; }
	inline void set__ScreenEdgeFading_11(int32_t value)
	{
		____ScreenEdgeFading_11 = value;
	}

	inline static int32_t get_offset_of__ReflectionBlur_12() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ReflectionBlur_12)); }
	inline int32_t get__ReflectionBlur_12() const { return ____ReflectionBlur_12; }
	inline int32_t* get_address_of__ReflectionBlur_12() { return &____ReflectionBlur_12; }
	inline void set__ReflectionBlur_12(int32_t value)
	{
		____ReflectionBlur_12 = value;
	}

	inline static int32_t get_offset_of__MaxRayTraceDistance_13() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____MaxRayTraceDistance_13)); }
	inline int32_t get__MaxRayTraceDistance_13() const { return ____MaxRayTraceDistance_13; }
	inline int32_t* get_address_of__MaxRayTraceDistance_13() { return &____MaxRayTraceDistance_13; }
	inline void set__MaxRayTraceDistance_13(int32_t value)
	{
		____MaxRayTraceDistance_13 = value;
	}

	inline static int32_t get_offset_of__FadeDistance_14() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FadeDistance_14)); }
	inline int32_t get__FadeDistance_14() const { return ____FadeDistance_14; }
	inline int32_t* get_address_of__FadeDistance_14() { return &____FadeDistance_14; }
	inline void set__FadeDistance_14(int32_t value)
	{
		____FadeDistance_14 = value;
	}

	inline static int32_t get_offset_of__LayerThickness_15() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____LayerThickness_15)); }
	inline int32_t get__LayerThickness_15() const { return ____LayerThickness_15; }
	inline int32_t* get_address_of__LayerThickness_15() { return &____LayerThickness_15; }
	inline void set__LayerThickness_15(int32_t value)
	{
		____LayerThickness_15 = value;
	}

	inline static int32_t get_offset_of__SSRMultiplier_16() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____SSRMultiplier_16)); }
	inline int32_t get__SSRMultiplier_16() const { return ____SSRMultiplier_16; }
	inline int32_t* get_address_of__SSRMultiplier_16() { return &____SSRMultiplier_16; }
	inline void set__SSRMultiplier_16(int32_t value)
	{
		____SSRMultiplier_16 = value;
	}

	inline static int32_t get_offset_of__FresnelFade_17() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FresnelFade_17)); }
	inline int32_t get__FresnelFade_17() const { return ____FresnelFade_17; }
	inline int32_t* get_address_of__FresnelFade_17() { return &____FresnelFade_17; }
	inline void set__FresnelFade_17(int32_t value)
	{
		____FresnelFade_17 = value;
	}

	inline static int32_t get_offset_of__FresnelFadePower_18() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FresnelFadePower_18)); }
	inline int32_t get__FresnelFadePower_18() const { return ____FresnelFadePower_18; }
	inline int32_t* get_address_of__FresnelFadePower_18() { return &____FresnelFadePower_18; }
	inline void set__FresnelFadePower_18(int32_t value)
	{
		____FresnelFadePower_18 = value;
	}

	inline static int32_t get_offset_of__ReflectionBufferSize_19() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ReflectionBufferSize_19)); }
	inline int32_t get__ReflectionBufferSize_19() const { return ____ReflectionBufferSize_19; }
	inline int32_t* get_address_of__ReflectionBufferSize_19() { return &____ReflectionBufferSize_19; }
	inline void set__ReflectionBufferSize_19(int32_t value)
	{
		____ReflectionBufferSize_19 = value;
	}

	inline static int32_t get_offset_of__ScreenSize_20() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ScreenSize_20)); }
	inline int32_t get__ScreenSize_20() const { return ____ScreenSize_20; }
	inline int32_t* get_address_of__ScreenSize_20() { return &____ScreenSize_20; }
	inline void set__ScreenSize_20(int32_t value)
	{
		____ScreenSize_20 = value;
	}

	inline static int32_t get_offset_of__InvScreenSize_21() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____InvScreenSize_21)); }
	inline int32_t get__InvScreenSize_21() const { return ____InvScreenSize_21; }
	inline int32_t* get_address_of__InvScreenSize_21() { return &____InvScreenSize_21; }
	inline void set__InvScreenSize_21(int32_t value)
	{
		____InvScreenSize_21 = value;
	}

	inline static int32_t get_offset_of__ProjInfo_22() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ProjInfo_22)); }
	inline int32_t get__ProjInfo_22() const { return ____ProjInfo_22; }
	inline int32_t* get_address_of__ProjInfo_22() { return &____ProjInfo_22; }
	inline void set__ProjInfo_22(int32_t value)
	{
		____ProjInfo_22 = value;
	}

	inline static int32_t get_offset_of__CameraClipInfo_23() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CameraClipInfo_23)); }
	inline int32_t get__CameraClipInfo_23() const { return ____CameraClipInfo_23; }
	inline int32_t* get_address_of__CameraClipInfo_23() { return &____CameraClipInfo_23; }
	inline void set__CameraClipInfo_23(int32_t value)
	{
		____CameraClipInfo_23 = value;
	}

	inline static int32_t get_offset_of__ProjectToPixelMatrix_24() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ProjectToPixelMatrix_24)); }
	inline int32_t get__ProjectToPixelMatrix_24() const { return ____ProjectToPixelMatrix_24; }
	inline int32_t* get_address_of__ProjectToPixelMatrix_24() { return &____ProjectToPixelMatrix_24; }
	inline void set__ProjectToPixelMatrix_24(int32_t value)
	{
		____ProjectToPixelMatrix_24 = value;
	}

	inline static int32_t get_offset_of__WorldToCameraMatrix_25() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____WorldToCameraMatrix_25)); }
	inline int32_t get__WorldToCameraMatrix_25() const { return ____WorldToCameraMatrix_25; }
	inline int32_t* get_address_of__WorldToCameraMatrix_25() { return &____WorldToCameraMatrix_25; }
	inline void set__WorldToCameraMatrix_25(int32_t value)
	{
		____WorldToCameraMatrix_25 = value;
	}

	inline static int32_t get_offset_of__CameraToWorldMatrix_26() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CameraToWorldMatrix_26)); }
	inline int32_t get__CameraToWorldMatrix_26() const { return ____CameraToWorldMatrix_26; }
	inline int32_t* get_address_of__CameraToWorldMatrix_26() { return &____CameraToWorldMatrix_26; }
	inline void set__CameraToWorldMatrix_26(int32_t value)
	{
		____CameraToWorldMatrix_26 = value;
	}

	inline static int32_t get_offset_of__Axis_27() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____Axis_27)); }
	inline int32_t get__Axis_27() const { return ____Axis_27; }
	inline int32_t* get_address_of__Axis_27() { return &____Axis_27; }
	inline void set__Axis_27(int32_t value)
	{
		____Axis_27 = value;
	}

	inline static int32_t get_offset_of__CurrentMipLevel_28() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CurrentMipLevel_28)); }
	inline int32_t get__CurrentMipLevel_28() const { return ____CurrentMipLevel_28; }
	inline int32_t* get_address_of__CurrentMipLevel_28() { return &____CurrentMipLevel_28; }
	inline void set__CurrentMipLevel_28(int32_t value)
	{
		____CurrentMipLevel_28 = value;
	}

	inline static int32_t get_offset_of__NormalAndRoughnessTexture_29() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____NormalAndRoughnessTexture_29)); }
	inline int32_t get__NormalAndRoughnessTexture_29() const { return ____NormalAndRoughnessTexture_29; }
	inline int32_t* get_address_of__NormalAndRoughnessTexture_29() { return &____NormalAndRoughnessTexture_29; }
	inline void set__NormalAndRoughnessTexture_29(int32_t value)
	{
		____NormalAndRoughnessTexture_29 = value;
	}

	inline static int32_t get_offset_of__HitPointTexture_30() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HitPointTexture_30)); }
	inline int32_t get__HitPointTexture_30() const { return ____HitPointTexture_30; }
	inline int32_t* get_address_of__HitPointTexture_30() { return &____HitPointTexture_30; }
	inline void set__HitPointTexture_30(int32_t value)
	{
		____HitPointTexture_30 = value;
	}

	inline static int32_t get_offset_of__BlurTexture_31() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____BlurTexture_31)); }
	inline int32_t get__BlurTexture_31() const { return ____BlurTexture_31; }
	inline int32_t* get_address_of__BlurTexture_31() { return &____BlurTexture_31; }
	inline void set__BlurTexture_31(int32_t value)
	{
		____BlurTexture_31 = value;
	}

	inline static int32_t get_offset_of__FilteredReflections_32() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FilteredReflections_32)); }
	inline int32_t get__FilteredReflections_32() const { return ____FilteredReflections_32; }
	inline int32_t* get_address_of__FilteredReflections_32() { return &____FilteredReflections_32; }
	inline void set__FilteredReflections_32(int32_t value)
	{
		____FilteredReflections_32 = value;
	}

	inline static int32_t get_offset_of__FinalReflectionTexture_33() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FinalReflectionTexture_33)); }
	inline int32_t get__FinalReflectionTexture_33() const { return ____FinalReflectionTexture_33; }
	inline int32_t* get_address_of__FinalReflectionTexture_33() { return &____FinalReflectionTexture_33; }
	inline void set__FinalReflectionTexture_33(int32_t value)
	{
		____FinalReflectionTexture_33 = value;
	}

	inline static int32_t get_offset_of__TempTexture_34() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TempTexture_34)); }
	inline int32_t get__TempTexture_34() const { return ____TempTexture_34; }
	inline int32_t* get_address_of__TempTexture_34() { return &____TempTexture_34; }
	inline void set__TempTexture_34(int32_t value)
	{
		____TempTexture_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2970573890_H
#ifndef UNIFORMS_T2205824134_H
#define UNIFORMS_T2205824134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteComponent/Uniforms
struct  Uniforms_t2205824134  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2205824134_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Color
	int32_t ____Vignette_Color_0;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Center
	int32_t ____Vignette_Center_1;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Settings
	int32_t ____Vignette_Settings_2;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Mask
	int32_t ____Vignette_Mask_3;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Opacity
	int32_t ____Vignette_Opacity_4;

public:
	inline static int32_t get_offset_of__Vignette_Color_0() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Color_0)); }
	inline int32_t get__Vignette_Color_0() const { return ____Vignette_Color_0; }
	inline int32_t* get_address_of__Vignette_Color_0() { return &____Vignette_Color_0; }
	inline void set__Vignette_Color_0(int32_t value)
	{
		____Vignette_Color_0 = value;
	}

	inline static int32_t get_offset_of__Vignette_Center_1() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Center_1)); }
	inline int32_t get__Vignette_Center_1() const { return ____Vignette_Center_1; }
	inline int32_t* get_address_of__Vignette_Center_1() { return &____Vignette_Center_1; }
	inline void set__Vignette_Center_1(int32_t value)
	{
		____Vignette_Center_1 = value;
	}

	inline static int32_t get_offset_of__Vignette_Settings_2() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Settings_2)); }
	inline int32_t get__Vignette_Settings_2() const { return ____Vignette_Settings_2; }
	inline int32_t* get_address_of__Vignette_Settings_2() { return &____Vignette_Settings_2; }
	inline void set__Vignette_Settings_2(int32_t value)
	{
		____Vignette_Settings_2 = value;
	}

	inline static int32_t get_offset_of__Vignette_Mask_3() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Mask_3)); }
	inline int32_t get__Vignette_Mask_3() const { return ____Vignette_Mask_3; }
	inline int32_t* get_address_of__Vignette_Mask_3() { return &____Vignette_Mask_3; }
	inline void set__Vignette_Mask_3(int32_t value)
	{
		____Vignette_Mask_3 = value;
	}

	inline static int32_t get_offset_of__Vignette_Opacity_4() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Opacity_4)); }
	inline int32_t get__Vignette_Opacity_4() const { return ____Vignette_Opacity_4; }
	inline int32_t* get_address_of__Vignette_Opacity_4() { return &____Vignette_Opacity_4; }
	inline void set__Vignette_Opacity_4(int32_t value)
	{
		____Vignette_Opacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2205824134_H
#ifndef UNIFORMS_T3024963833_H
#define UNIFORMS_T3024963833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TaaComponent/Uniforms
struct  Uniforms_t3024963833  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3024963833_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_Jitter
	int32_t ____Jitter_0;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_SharpenParameters
	int32_t ____SharpenParameters_1;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_FinalBlendParameters
	int32_t ____FinalBlendParameters_2;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_HistoryTex
	int32_t ____HistoryTex_3;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_MainTex
	int32_t ____MainTex_4;

public:
	inline static int32_t get_offset_of__Jitter_0() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____Jitter_0)); }
	inline int32_t get__Jitter_0() const { return ____Jitter_0; }
	inline int32_t* get_address_of__Jitter_0() { return &____Jitter_0; }
	inline void set__Jitter_0(int32_t value)
	{
		____Jitter_0 = value;
	}

	inline static int32_t get_offset_of__SharpenParameters_1() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____SharpenParameters_1)); }
	inline int32_t get__SharpenParameters_1() const { return ____SharpenParameters_1; }
	inline int32_t* get_address_of__SharpenParameters_1() { return &____SharpenParameters_1; }
	inline void set__SharpenParameters_1(int32_t value)
	{
		____SharpenParameters_1 = value;
	}

	inline static int32_t get_offset_of__FinalBlendParameters_2() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____FinalBlendParameters_2)); }
	inline int32_t get__FinalBlendParameters_2() const { return ____FinalBlendParameters_2; }
	inline int32_t* get_address_of__FinalBlendParameters_2() { return &____FinalBlendParameters_2; }
	inline void set__FinalBlendParameters_2(int32_t value)
	{
		____FinalBlendParameters_2 = value;
	}

	inline static int32_t get_offset_of__HistoryTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____HistoryTex_3)); }
	inline int32_t get__HistoryTex_3() const { return ____HistoryTex_3; }
	inline int32_t* get_address_of__HistoryTex_3() { return &____HistoryTex_3; }
	inline void set__HistoryTex_3(int32_t value)
	{
		____HistoryTex_3 = value;
	}

	inline static int32_t get_offset_of__MainTex_4() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____MainTex_4)); }
	inline int32_t get__MainTex_4() const { return ____MainTex_4; }
	inline int32_t* get_address_of__MainTex_4() { return &____MainTex_4; }
	inline void set__MainTex_4(int32_t value)
	{
		____MainTex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3024963833_H
#ifndef UNIFORMS_T1850622510_H
#define UNIFORMS_T1850622510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FxaaComponent/Uniforms
struct  Uniforms_t1850622510  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1850622510_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.FxaaComponent/Uniforms::_QualitySettings
	int32_t ____QualitySettings_0;
	// System.Int32 UnityEngine.PostProcessing.FxaaComponent/Uniforms::_ConsoleSettings
	int32_t ____ConsoleSettings_1;

public:
	inline static int32_t get_offset_of__QualitySettings_0() { return static_cast<int32_t>(offsetof(Uniforms_t1850622510_StaticFields, ____QualitySettings_0)); }
	inline int32_t get__QualitySettings_0() const { return ____QualitySettings_0; }
	inline int32_t* get_address_of__QualitySettings_0() { return &____QualitySettings_0; }
	inline void set__QualitySettings_0(int32_t value)
	{
		____QualitySettings_0 = value;
	}

	inline static int32_t get_offset_of__ConsoleSettings_1() { return static_cast<int32_t>(offsetof(Uniforms_t1850622510_StaticFields, ____ConsoleSettings_1)); }
	inline int32_t get__ConsoleSettings_1() const { return ____ConsoleSettings_1; }
	inline int32_t* get_address_of__ConsoleSettings_1() { return &____ConsoleSettings_1; }
	inline void set__ConsoleSettings_1(int32_t value)
	{
		____ConsoleSettings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1850622510_H
#ifndef UNIFORMS_T3629868803_H
#define UNIFORMS_T3629868803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms
struct  Uniforms_t3629868803  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3629868803_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_DepthOfFieldTex
	int32_t ____DepthOfFieldTex_0;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_Distance
	int32_t ____Distance_1;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_LensCoeff
	int32_t ____LensCoeff_2;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_MaxCoC
	int32_t ____MaxCoC_3;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_RcpMaxCoC
	int32_t ____RcpMaxCoC_4;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_RcpAspect
	int32_t ____RcpAspect_5;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_MainTex
	int32_t ____MainTex_6;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_HistoryCoC
	int32_t ____HistoryCoC_7;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_HistoryWeight
	int32_t ____HistoryWeight_8;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_DepthOfFieldParams
	int32_t ____DepthOfFieldParams_9;

public:
	inline static int32_t get_offset_of__DepthOfFieldTex_0() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____DepthOfFieldTex_0)); }
	inline int32_t get__DepthOfFieldTex_0() const { return ____DepthOfFieldTex_0; }
	inline int32_t* get_address_of__DepthOfFieldTex_0() { return &____DepthOfFieldTex_0; }
	inline void set__DepthOfFieldTex_0(int32_t value)
	{
		____DepthOfFieldTex_0 = value;
	}

	inline static int32_t get_offset_of__Distance_1() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____Distance_1)); }
	inline int32_t get__Distance_1() const { return ____Distance_1; }
	inline int32_t* get_address_of__Distance_1() { return &____Distance_1; }
	inline void set__Distance_1(int32_t value)
	{
		____Distance_1 = value;
	}

	inline static int32_t get_offset_of__LensCoeff_2() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____LensCoeff_2)); }
	inline int32_t get__LensCoeff_2() const { return ____LensCoeff_2; }
	inline int32_t* get_address_of__LensCoeff_2() { return &____LensCoeff_2; }
	inline void set__LensCoeff_2(int32_t value)
	{
		____LensCoeff_2 = value;
	}

	inline static int32_t get_offset_of__MaxCoC_3() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____MaxCoC_3)); }
	inline int32_t get__MaxCoC_3() const { return ____MaxCoC_3; }
	inline int32_t* get_address_of__MaxCoC_3() { return &____MaxCoC_3; }
	inline void set__MaxCoC_3(int32_t value)
	{
		____MaxCoC_3 = value;
	}

	inline static int32_t get_offset_of__RcpMaxCoC_4() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____RcpMaxCoC_4)); }
	inline int32_t get__RcpMaxCoC_4() const { return ____RcpMaxCoC_4; }
	inline int32_t* get_address_of__RcpMaxCoC_4() { return &____RcpMaxCoC_4; }
	inline void set__RcpMaxCoC_4(int32_t value)
	{
		____RcpMaxCoC_4 = value;
	}

	inline static int32_t get_offset_of__RcpAspect_5() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____RcpAspect_5)); }
	inline int32_t get__RcpAspect_5() const { return ____RcpAspect_5; }
	inline int32_t* get_address_of__RcpAspect_5() { return &____RcpAspect_5; }
	inline void set__RcpAspect_5(int32_t value)
	{
		____RcpAspect_5 = value;
	}

	inline static int32_t get_offset_of__MainTex_6() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____MainTex_6)); }
	inline int32_t get__MainTex_6() const { return ____MainTex_6; }
	inline int32_t* get_address_of__MainTex_6() { return &____MainTex_6; }
	inline void set__MainTex_6(int32_t value)
	{
		____MainTex_6 = value;
	}

	inline static int32_t get_offset_of__HistoryCoC_7() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____HistoryCoC_7)); }
	inline int32_t get__HistoryCoC_7() const { return ____HistoryCoC_7; }
	inline int32_t* get_address_of__HistoryCoC_7() { return &____HistoryCoC_7; }
	inline void set__HistoryCoC_7(int32_t value)
	{
		____HistoryCoC_7 = value;
	}

	inline static int32_t get_offset_of__HistoryWeight_8() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____HistoryWeight_8)); }
	inline int32_t get__HistoryWeight_8() const { return ____HistoryWeight_8; }
	inline int32_t* get_address_of__HistoryWeight_8() { return &____HistoryWeight_8; }
	inline void set__HistoryWeight_8(int32_t value)
	{
		____HistoryWeight_8 = value;
	}

	inline static int32_t get_offset_of__DepthOfFieldParams_9() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____DepthOfFieldParams_9)); }
	inline int32_t get__DepthOfFieldParams_9() const { return ____DepthOfFieldParams_9; }
	inline int32_t* get_address_of__DepthOfFieldParams_9() { return &____DepthOfFieldParams_9; }
	inline void set__DepthOfFieldParams_9(int32_t value)
	{
		____DepthOfFieldParams_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3629868803_H
#ifndef UNIFORMS_T3075607151_H
#define UNIFORMS_T3075607151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingComponent/Uniforms
struct  Uniforms_t3075607151  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3075607151_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LutParams
	int32_t ____LutParams_0;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_NeutralTonemapperParams1
	int32_t ____NeutralTonemapperParams1_1;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_NeutralTonemapperParams2
	int32_t ____NeutralTonemapperParams2_2;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_HueShift
	int32_t ____HueShift_3;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Saturation
	int32_t ____Saturation_4;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Contrast
	int32_t ____Contrast_5;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Balance
	int32_t ____Balance_6;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Lift
	int32_t ____Lift_7;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_InvGamma
	int32_t ____InvGamma_8;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Gain
	int32_t ____Gain_9;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Slope
	int32_t ____Slope_10;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Power
	int32_t ____Power_11;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Offset
	int32_t ____Offset_12;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerRed
	int32_t ____ChannelMixerRed_13;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerGreen
	int32_t ____ChannelMixerGreen_14;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerBlue
	int32_t ____ChannelMixerBlue_15;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Curves
	int32_t ____Curves_16;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LogLut
	int32_t ____LogLut_17;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LogLut_Params
	int32_t ____LogLut_Params_18;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ExposureEV
	int32_t ____ExposureEV_19;

public:
	inline static int32_t get_offset_of__LutParams_0() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LutParams_0)); }
	inline int32_t get__LutParams_0() const { return ____LutParams_0; }
	inline int32_t* get_address_of__LutParams_0() { return &____LutParams_0; }
	inline void set__LutParams_0(int32_t value)
	{
		____LutParams_0 = value;
	}

	inline static int32_t get_offset_of__NeutralTonemapperParams1_1() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____NeutralTonemapperParams1_1)); }
	inline int32_t get__NeutralTonemapperParams1_1() const { return ____NeutralTonemapperParams1_1; }
	inline int32_t* get_address_of__NeutralTonemapperParams1_1() { return &____NeutralTonemapperParams1_1; }
	inline void set__NeutralTonemapperParams1_1(int32_t value)
	{
		____NeutralTonemapperParams1_1 = value;
	}

	inline static int32_t get_offset_of__NeutralTonemapperParams2_2() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____NeutralTonemapperParams2_2)); }
	inline int32_t get__NeutralTonemapperParams2_2() const { return ____NeutralTonemapperParams2_2; }
	inline int32_t* get_address_of__NeutralTonemapperParams2_2() { return &____NeutralTonemapperParams2_2; }
	inline void set__NeutralTonemapperParams2_2(int32_t value)
	{
		____NeutralTonemapperParams2_2 = value;
	}

	inline static int32_t get_offset_of__HueShift_3() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____HueShift_3)); }
	inline int32_t get__HueShift_3() const { return ____HueShift_3; }
	inline int32_t* get_address_of__HueShift_3() { return &____HueShift_3; }
	inline void set__HueShift_3(int32_t value)
	{
		____HueShift_3 = value;
	}

	inline static int32_t get_offset_of__Saturation_4() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Saturation_4)); }
	inline int32_t get__Saturation_4() const { return ____Saturation_4; }
	inline int32_t* get_address_of__Saturation_4() { return &____Saturation_4; }
	inline void set__Saturation_4(int32_t value)
	{
		____Saturation_4 = value;
	}

	inline static int32_t get_offset_of__Contrast_5() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Contrast_5)); }
	inline int32_t get__Contrast_5() const { return ____Contrast_5; }
	inline int32_t* get_address_of__Contrast_5() { return &____Contrast_5; }
	inline void set__Contrast_5(int32_t value)
	{
		____Contrast_5 = value;
	}

	inline static int32_t get_offset_of__Balance_6() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Balance_6)); }
	inline int32_t get__Balance_6() const { return ____Balance_6; }
	inline int32_t* get_address_of__Balance_6() { return &____Balance_6; }
	inline void set__Balance_6(int32_t value)
	{
		____Balance_6 = value;
	}

	inline static int32_t get_offset_of__Lift_7() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Lift_7)); }
	inline int32_t get__Lift_7() const { return ____Lift_7; }
	inline int32_t* get_address_of__Lift_7() { return &____Lift_7; }
	inline void set__Lift_7(int32_t value)
	{
		____Lift_7 = value;
	}

	inline static int32_t get_offset_of__InvGamma_8() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____InvGamma_8)); }
	inline int32_t get__InvGamma_8() const { return ____InvGamma_8; }
	inline int32_t* get_address_of__InvGamma_8() { return &____InvGamma_8; }
	inline void set__InvGamma_8(int32_t value)
	{
		____InvGamma_8 = value;
	}

	inline static int32_t get_offset_of__Gain_9() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Gain_9)); }
	inline int32_t get__Gain_9() const { return ____Gain_9; }
	inline int32_t* get_address_of__Gain_9() { return &____Gain_9; }
	inline void set__Gain_9(int32_t value)
	{
		____Gain_9 = value;
	}

	inline static int32_t get_offset_of__Slope_10() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Slope_10)); }
	inline int32_t get__Slope_10() const { return ____Slope_10; }
	inline int32_t* get_address_of__Slope_10() { return &____Slope_10; }
	inline void set__Slope_10(int32_t value)
	{
		____Slope_10 = value;
	}

	inline static int32_t get_offset_of__Power_11() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Power_11)); }
	inline int32_t get__Power_11() const { return ____Power_11; }
	inline int32_t* get_address_of__Power_11() { return &____Power_11; }
	inline void set__Power_11(int32_t value)
	{
		____Power_11 = value;
	}

	inline static int32_t get_offset_of__Offset_12() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Offset_12)); }
	inline int32_t get__Offset_12() const { return ____Offset_12; }
	inline int32_t* get_address_of__Offset_12() { return &____Offset_12; }
	inline void set__Offset_12(int32_t value)
	{
		____Offset_12 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerRed_13() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerRed_13)); }
	inline int32_t get__ChannelMixerRed_13() const { return ____ChannelMixerRed_13; }
	inline int32_t* get_address_of__ChannelMixerRed_13() { return &____ChannelMixerRed_13; }
	inline void set__ChannelMixerRed_13(int32_t value)
	{
		____ChannelMixerRed_13 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerGreen_14() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerGreen_14)); }
	inline int32_t get__ChannelMixerGreen_14() const { return ____ChannelMixerGreen_14; }
	inline int32_t* get_address_of__ChannelMixerGreen_14() { return &____ChannelMixerGreen_14; }
	inline void set__ChannelMixerGreen_14(int32_t value)
	{
		____ChannelMixerGreen_14 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerBlue_15() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerBlue_15)); }
	inline int32_t get__ChannelMixerBlue_15() const { return ____ChannelMixerBlue_15; }
	inline int32_t* get_address_of__ChannelMixerBlue_15() { return &____ChannelMixerBlue_15; }
	inline void set__ChannelMixerBlue_15(int32_t value)
	{
		____ChannelMixerBlue_15 = value;
	}

	inline static int32_t get_offset_of__Curves_16() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Curves_16)); }
	inline int32_t get__Curves_16() const { return ____Curves_16; }
	inline int32_t* get_address_of__Curves_16() { return &____Curves_16; }
	inline void set__Curves_16(int32_t value)
	{
		____Curves_16 = value;
	}

	inline static int32_t get_offset_of__LogLut_17() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LogLut_17)); }
	inline int32_t get__LogLut_17() const { return ____LogLut_17; }
	inline int32_t* get_address_of__LogLut_17() { return &____LogLut_17; }
	inline void set__LogLut_17(int32_t value)
	{
		____LogLut_17 = value;
	}

	inline static int32_t get_offset_of__LogLut_Params_18() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LogLut_Params_18)); }
	inline int32_t get__LogLut_Params_18() const { return ____LogLut_Params_18; }
	inline int32_t* get_address_of__LogLut_Params_18() { return &____LogLut_Params_18; }
	inline void set__LogLut_Params_18(int32_t value)
	{
		____LogLut_Params_18 = value;
	}

	inline static int32_t get_offset_of__ExposureEV_19() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ExposureEV_19)); }
	inline int32_t get__ExposureEV_19() const { return ____ExposureEV_19; }
	inline int32_t* get_address_of__ExposureEV_19() { return &____ExposureEV_19; }
	inline void set__ExposureEV_19(int32_t value)
	{
		____ExposureEV_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3075607151_H
#ifndef UNIFORMS_T3745258951_H
#define UNIFORMS_T3745258951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringComponent/Uniforms
struct  Uniforms_t3745258951  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3745258951_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent/Uniforms::_DitheringTex
	int32_t ____DitheringTex_0;
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent/Uniforms::_DitheringCoords
	int32_t ____DitheringCoords_1;

public:
	inline static int32_t get_offset_of__DitheringTex_0() { return static_cast<int32_t>(offsetof(Uniforms_t3745258951_StaticFields, ____DitheringTex_0)); }
	inline int32_t get__DitheringTex_0() const { return ____DitheringTex_0; }
	inline int32_t* get_address_of__DitheringTex_0() { return &____DitheringTex_0; }
	inline void set__DitheringTex_0(int32_t value)
	{
		____DitheringTex_0 = value;
	}

	inline static int32_t get_offset_of__DitheringCoords_1() { return static_cast<int32_t>(offsetof(Uniforms_t3745258951_StaticFields, ____DitheringCoords_1)); }
	inline int32_t get__DitheringCoords_1() const { return ____DitheringCoords_1; }
	inline int32_t* get_address_of__DitheringCoords_1() { return &____DitheringCoords_1; }
	inline void set__DitheringCoords_1(int32_t value)
	{
		____DitheringCoords_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3745258951_H
#ifndef UNIFORMS_T459708260_H
#define UNIFORMS_T459708260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogComponent/Uniforms
struct  Uniforms_t459708260  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t459708260_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_FogColor
	int32_t ____FogColor_0;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_Density
	int32_t ____Density_1;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_Start
	int32_t ____Start_2;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_End
	int32_t ____End_3;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_TempRT
	int32_t ____TempRT_4;

public:
	inline static int32_t get_offset_of__FogColor_0() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____FogColor_0)); }
	inline int32_t get__FogColor_0() const { return ____FogColor_0; }
	inline int32_t* get_address_of__FogColor_0() { return &____FogColor_0; }
	inline void set__FogColor_0(int32_t value)
	{
		____FogColor_0 = value;
	}

	inline static int32_t get_offset_of__Density_1() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____Density_1)); }
	inline int32_t get__Density_1() const { return ____Density_1; }
	inline int32_t* get_address_of__Density_1() { return &____Density_1; }
	inline void set__Density_1(int32_t value)
	{
		____Density_1 = value;
	}

	inline static int32_t get_offset_of__Start_2() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____Start_2)); }
	inline int32_t get__Start_2() const { return ____Start_2; }
	inline int32_t* get_address_of__Start_2() { return &____Start_2; }
	inline void set__Start_2(int32_t value)
	{
		____Start_2 = value;
	}

	inline static int32_t get_offset_of__End_3() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____End_3)); }
	inline int32_t get__End_3() const { return ____End_3; }
	inline int32_t* get_address_of__End_3() { return &____End_3; }
	inline void set__End_3(int32_t value)
	{
		____End_3 = value;
	}

	inline static int32_t get_offset_of__TempRT_4() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____TempRT_4)); }
	inline int32_t get__TempRT_4() const { return ____TempRT_4; }
	inline int32_t* get_address_of__TempRT_4() { return &____TempRT_4; }
	inline void set__TempRT_4(int32_t value)
	{
		____TempRT_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T459708260_H
#ifndef UNIFORMS_T95810089_H
#define UNIFORMS_T95810089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms
struct  Uniforms_t95810089  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t95810089_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_Params
	int32_t ____Params_0;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_Speed
	int32_t ____Speed_1;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_ScaleOffsetRes
	int32_t ____ScaleOffsetRes_2;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_ExposureCompensation
	int32_t ____ExposureCompensation_3;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_AutoExposure
	int32_t ____AutoExposure_4;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_DebugWidth
	int32_t ____DebugWidth_5;

public:
	inline static int32_t get_offset_of__Params_0() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____Params_0)); }
	inline int32_t get__Params_0() const { return ____Params_0; }
	inline int32_t* get_address_of__Params_0() { return &____Params_0; }
	inline void set__Params_0(int32_t value)
	{
		____Params_0 = value;
	}

	inline static int32_t get_offset_of__Speed_1() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____Speed_1)); }
	inline int32_t get__Speed_1() const { return ____Speed_1; }
	inline int32_t* get_address_of__Speed_1() { return &____Speed_1; }
	inline void set__Speed_1(int32_t value)
	{
		____Speed_1 = value;
	}

	inline static int32_t get_offset_of__ScaleOffsetRes_2() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____ScaleOffsetRes_2)); }
	inline int32_t get__ScaleOffsetRes_2() const { return ____ScaleOffsetRes_2; }
	inline int32_t* get_address_of__ScaleOffsetRes_2() { return &____ScaleOffsetRes_2; }
	inline void set__ScaleOffsetRes_2(int32_t value)
	{
		____ScaleOffsetRes_2 = value;
	}

	inline static int32_t get_offset_of__ExposureCompensation_3() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____ExposureCompensation_3)); }
	inline int32_t get__ExposureCompensation_3() const { return ____ExposureCompensation_3; }
	inline int32_t* get_address_of__ExposureCompensation_3() { return &____ExposureCompensation_3; }
	inline void set__ExposureCompensation_3(int32_t value)
	{
		____ExposureCompensation_3 = value;
	}

	inline static int32_t get_offset_of__AutoExposure_4() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____AutoExposure_4)); }
	inline int32_t get__AutoExposure_4() const { return ____AutoExposure_4; }
	inline int32_t* get_address_of__AutoExposure_4() { return &____AutoExposure_4; }
	inline void set__AutoExposure_4(int32_t value)
	{
		____AutoExposure_4 = value;
	}

	inline static int32_t get_offset_of__DebugWidth_5() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____DebugWidth_5)); }
	inline int32_t get__DebugWidth_5() const { return ____DebugWidth_5; }
	inline int32_t* get_address_of__DebugWidth_5() { return &____DebugWidth_5; }
	inline void set__DebugWidth_5(int32_t value)
	{
		____DebugWidth_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T95810089_H
#ifndef MMFADE_T3008706013_H
#define MMFADE_T3008706013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMFade
struct  MMFade_t3008706013  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMFADE_T3008706013_H
#ifndef MMLAYERS_T3559366371_H
#define MMLAYERS_T3559366371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMLayers
struct  MMLayers_t3559366371  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMLAYERS_T3559366371_H
#ifndef MMDEBUG_T3652739535_H
#define MMDEBUG_T3652739535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMDebug
struct  MMDebug_t3652739535  : public RuntimeObject
{
public:

public:
};

struct MMDebug_t3652739535_StaticFields
{
public:
	// MoreMountains.Tools.MMConsole MoreMountains.Tools.MMDebug::_console
	MMConsole_t3000489708 * ____console_0;

public:
	inline static int32_t get_offset_of__console_0() { return static_cast<int32_t>(offsetof(MMDebug_t3652739535_StaticFields, ____console_0)); }
	inline MMConsole_t3000489708 * get__console_0() const { return ____console_0; }
	inline MMConsole_t3000489708 ** get_address_of__console_0() { return &____console_0; }
	inline void set__console_0(MMConsole_t3000489708 * value)
	{
		____console_0 = value;
		Il2CppCodeGenWriteBarrier((&____console_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMDEBUG_T3652739535_H
#ifndef MMLISTS_T1411150679_H
#define MMLISTS_T1411150679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MMLists
struct  MMLists_t1411150679  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMLISTS_T1411150679_H
#ifndef MMHELPERS_T3933556615_H
#define MMHELPERS_T3933556615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMHelpers
struct  MMHelpers_t3933556615  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMHELPERS_T3933556615_H
#ifndef IMBUTTON_T3867185334_H
#define IMBUTTON_T3867185334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMInput/IMButton
struct  IMButton_t3867185334  : public RuntimeObject
{
public:
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates> MoreMountains.Tools.MMInput/IMButton::<State>k__BackingField
	MMStateMachine_1_t2171884985 * ___U3CStateU3Ek__BackingField_0;
	// System.String MoreMountains.Tools.MMInput/IMButton::ButtonID
	String_t* ___ButtonID_1;
	// MoreMountains.Tools.MMInput/IMButton/ButtonDownMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonDownMethod
	ButtonDownMethodDelegate_t3308383149 * ___ButtonDownMethod_2;
	// MoreMountains.Tools.MMInput/IMButton/ButtonPressedMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonPressedMethod
	ButtonPressedMethodDelegate_t2485678181 * ___ButtonPressedMethod_3;
	// MoreMountains.Tools.MMInput/IMButton/ButtonUpMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonUpMethod
	ButtonUpMethodDelegate_t939453058 * ___ButtonUpMethod_4;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IMButton_t3867185334, ___U3CStateU3Ek__BackingField_0)); }
	inline MMStateMachine_1_t2171884985 * get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline MMStateMachine_1_t2171884985 ** get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(MMStateMachine_1_t2171884985 * value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_ButtonID_1() { return static_cast<int32_t>(offsetof(IMButton_t3867185334, ___ButtonID_1)); }
	inline String_t* get_ButtonID_1() const { return ___ButtonID_1; }
	inline String_t** get_address_of_ButtonID_1() { return &___ButtonID_1; }
	inline void set_ButtonID_1(String_t* value)
	{
		___ButtonID_1 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonID_1), value);
	}

	inline static int32_t get_offset_of_ButtonDownMethod_2() { return static_cast<int32_t>(offsetof(IMButton_t3867185334, ___ButtonDownMethod_2)); }
	inline ButtonDownMethodDelegate_t3308383149 * get_ButtonDownMethod_2() const { return ___ButtonDownMethod_2; }
	inline ButtonDownMethodDelegate_t3308383149 ** get_address_of_ButtonDownMethod_2() { return &___ButtonDownMethod_2; }
	inline void set_ButtonDownMethod_2(ButtonDownMethodDelegate_t3308383149 * value)
	{
		___ButtonDownMethod_2 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonDownMethod_2), value);
	}

	inline static int32_t get_offset_of_ButtonPressedMethod_3() { return static_cast<int32_t>(offsetof(IMButton_t3867185334, ___ButtonPressedMethod_3)); }
	inline ButtonPressedMethodDelegate_t2485678181 * get_ButtonPressedMethod_3() const { return ___ButtonPressedMethod_3; }
	inline ButtonPressedMethodDelegate_t2485678181 ** get_address_of_ButtonPressedMethod_3() { return &___ButtonPressedMethod_3; }
	inline void set_ButtonPressedMethod_3(ButtonPressedMethodDelegate_t2485678181 * value)
	{
		___ButtonPressedMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonPressedMethod_3), value);
	}

	inline static int32_t get_offset_of_ButtonUpMethod_4() { return static_cast<int32_t>(offsetof(IMButton_t3867185334, ___ButtonUpMethod_4)); }
	inline ButtonUpMethodDelegate_t939453058 * get_ButtonUpMethod_4() const { return ___ButtonUpMethod_4; }
	inline ButtonUpMethodDelegate_t939453058 ** get_address_of_ButtonUpMethod_4() { return &___ButtonUpMethod_4; }
	inline void set_ButtonUpMethod_4(ButtonUpMethodDelegate_t939453058 * value)
	{
		___ButtonUpMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonUpMethod_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMBUTTON_T3867185334_H
#ifndef U3CFADECANVASGROUPU3EC__ITERATOR3_T365063154_H
#define U3CFADECANVASGROUPU3EC__ITERATOR3_T365063154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3
struct  U3CFadeCanvasGroupU3Ec__Iterator3_t365063154  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::target
	CanvasGroup_t4083511760 * ___target_0;
	// System.Single MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::<currentAlpha>__0
	float ___U3CcurrentAlphaU3E__0_1;
	// System.Single MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::<t>__0
	float ___U3CtU3E__0_2;
	// System.Single MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::targetAlpha
	float ___targetAlpha_3;
	// System.Single MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::<newAlpha>__1
	float ___U3CnewAlphaU3E__1_4;
	// System.Single MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::duration
	float ___duration_5;
	// System.Object MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 MoreMountains.Tools.MMFade/<FadeCanvasGroup>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___target_0)); }
	inline CanvasGroup_t4083511760 * get_target_0() const { return ___target_0; }
	inline CanvasGroup_t4083511760 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CanvasGroup_t4083511760 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentAlphaU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___U3CcurrentAlphaU3E__0_1)); }
	inline float get_U3CcurrentAlphaU3E__0_1() const { return ___U3CcurrentAlphaU3E__0_1; }
	inline float* get_address_of_U3CcurrentAlphaU3E__0_1() { return &___U3CcurrentAlphaU3E__0_1; }
	inline void set_U3CcurrentAlphaU3E__0_1(float value)
	{
		___U3CcurrentAlphaU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___U3CtU3E__0_2)); }
	inline float get_U3CtU3E__0_2() const { return ___U3CtU3E__0_2; }
	inline float* get_address_of_U3CtU3E__0_2() { return &___U3CtU3E__0_2; }
	inline void set_U3CtU3E__0_2(float value)
	{
		___U3CtU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_targetAlpha_3() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___targetAlpha_3)); }
	inline float get_targetAlpha_3() const { return ___targetAlpha_3; }
	inline float* get_address_of_targetAlpha_3() { return &___targetAlpha_3; }
	inline void set_targetAlpha_3(float value)
	{
		___targetAlpha_3 = value;
	}

	inline static int32_t get_offset_of_U3CnewAlphaU3E__1_4() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___U3CnewAlphaU3E__1_4)); }
	inline float get_U3CnewAlphaU3E__1_4() const { return ___U3CnewAlphaU3E__1_4; }
	inline float* get_address_of_U3CnewAlphaU3E__1_4() { return &___U3CnewAlphaU3E__1_4; }
	inline void set_U3CnewAlphaU3E__1_4(float value)
	{
		___U3CnewAlphaU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CFadeCanvasGroupU3Ec__Iterator3_t365063154, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADECANVASGROUPU3EC__ITERATOR3_T365063154_H
#ifndef MMGUI_T3437079187_H
#define MMGUI_T3437079187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMGUI
struct  MMGUI_t3437079187  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMGUI_T3437079187_H
#ifndef POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#define POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentBase
struct  PostProcessingComponentBase_t2731103827  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingComponentBase::context
	PostProcessingContext_t2014408948 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(PostProcessingComponentBase_t2731103827, ___context_0)); }
	inline PostProcessingContext_t2014408948 * get_context_0() const { return ___context_0; }
	inline PostProcessingContext_t2014408948 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(PostProcessingContext_t2014408948 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifndef MMMOVEMENT_T3250796725_H
#define MMMOVEMENT_T3250796725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMMovement
struct  MMMovement_t3250796725  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMMOVEMENT_T3250796725_H
#ifndef MMANIMATOR_T3222997085_H
#define MMANIMATOR_T3222997085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMAnimator
struct  MMAnimator_t3222997085  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMANIMATOR_T3222997085_H
#ifndef MMMATHS_T519709266_H
#define MMMATHS_T519709266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMMaths
struct  MMMaths_t519709266  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMMATHS_T519709266_H
#ifndef FRAME_T295908221_H
#define FRAME_T295908221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame
struct  Frame_t295908221 
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::lumaTexture
	RenderTexture_t2108887433 * ___lumaTexture_0;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::chromaTexture
	RenderTexture_t2108887433 * ___chromaTexture_1;
	// System.Single UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::m_Time
	float ___m_Time_2;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::m_MRT
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_MRT_3;

public:
	inline static int32_t get_offset_of_lumaTexture_0() { return static_cast<int32_t>(offsetof(Frame_t295908221, ___lumaTexture_0)); }
	inline RenderTexture_t2108887433 * get_lumaTexture_0() const { return ___lumaTexture_0; }
	inline RenderTexture_t2108887433 ** get_address_of_lumaTexture_0() { return &___lumaTexture_0; }
	inline void set_lumaTexture_0(RenderTexture_t2108887433 * value)
	{
		___lumaTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___lumaTexture_0), value);
	}

	inline static int32_t get_offset_of_chromaTexture_1() { return static_cast<int32_t>(offsetof(Frame_t295908221, ___chromaTexture_1)); }
	inline RenderTexture_t2108887433 * get_chromaTexture_1() const { return ___chromaTexture_1; }
	inline RenderTexture_t2108887433 ** get_address_of_chromaTexture_1() { return &___chromaTexture_1; }
	inline void set_chromaTexture_1(RenderTexture_t2108887433 * value)
	{
		___chromaTexture_1 = value;
		Il2CppCodeGenWriteBarrier((&___chromaTexture_1), value);
	}

	inline static int32_t get_offset_of_m_Time_2() { return static_cast<int32_t>(offsetof(Frame_t295908221, ___m_Time_2)); }
	inline float get_m_Time_2() const { return ___m_Time_2; }
	inline float* get_address_of_m_Time_2() { return &___m_Time_2; }
	inline void set_m_Time_2(float value)
	{
		___m_Time_2 = value;
	}

	inline static int32_t get_offset_of_m_MRT_3() { return static_cast<int32_t>(offsetof(Frame_t295908221, ___m_MRT_3)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_MRT_3() const { return ___m_MRT_3; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_MRT_3() { return &___m_MRT_3; }
	inline void set_m_MRT_3(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_MRT_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame
struct Frame_t295908221_marshaled_pinvoke
{
	RenderTexture_t2108887433 * ___lumaTexture_0;
	RenderTexture_t2108887433 * ___chromaTexture_1;
	float ___m_Time_2;
	RenderTargetIdentifier_t2079184500 * ___m_MRT_3;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame
struct Frame_t295908221_marshaled_com
{
	RenderTexture_t2108887433 * ___lumaTexture_0;
	RenderTexture_t2108887433 * ___chromaTexture_1;
	float ___m_Time_2;
	RenderTargetIdentifier_t2079184500 * ___m_MRT_3;
};
#endif // FRAME_T295908221_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef POSTPROCESSINGCOMPONENT_1_T446058690_H
#define POSTPROCESSINGCOMPONENT_1_T446058690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.GrainModel>
struct  PostProcessingComponent_1_t446058690  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	GrainModel_t1152882488 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t446058690, ___U3CmodelU3Ek__BackingField_1)); }
	inline GrainModel_t1152882488 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline GrainModel_t1152882488 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(GrainModel_t1152882488 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T446058690_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2373462325_H
#define POSTPROCESSINGCOMPONENT_1_T2373462325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.MotionBlurModel>
struct  PostProcessingComponent_1_t2373462325  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	MotionBlurModel_t3080286123 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2373462325, ___U3CmodelU3Ek__BackingField_1)); }
	inline MotionBlurModel_t3080286123 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline MotionBlurModel_t3080286123 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(MotionBlurModel_t3080286123 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2373462325_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2319520934_H
#define POSTPROCESSINGCOMPONENT_1_T2319520934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>
struct  PostProcessingComponent_1_t2319520934  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ScreenSpaceReflectionModel_t3026344732 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2319520934, ___U3CmodelU3Ek__BackingField_1)); }
	inline ScreenSpaceReflectionModel_t3026344732 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ScreenSpaceReflectionModel_t3026344732 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ScreenSpaceReflectionModel_t3026344732 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2319520934_H
#ifndef FXAAQUALITYSETTINGS_T1558211909_H
#define FXAAQUALITYSETTINGS_T1558211909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings
struct  FxaaQualitySettings_t1558211909 
{
public:
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::subpixelAliasingRemovalAmount
	float ___subpixelAliasingRemovalAmount_0;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::edgeDetectionThreshold
	float ___edgeDetectionThreshold_1;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::minimumRequiredLuminance
	float ___minimumRequiredLuminance_2;

public:
	inline static int32_t get_offset_of_subpixelAliasingRemovalAmount_0() { return static_cast<int32_t>(offsetof(FxaaQualitySettings_t1558211909, ___subpixelAliasingRemovalAmount_0)); }
	inline float get_subpixelAliasingRemovalAmount_0() const { return ___subpixelAliasingRemovalAmount_0; }
	inline float* get_address_of_subpixelAliasingRemovalAmount_0() { return &___subpixelAliasingRemovalAmount_0; }
	inline void set_subpixelAliasingRemovalAmount_0(float value)
	{
		___subpixelAliasingRemovalAmount_0 = value;
	}

	inline static int32_t get_offset_of_edgeDetectionThreshold_1() { return static_cast<int32_t>(offsetof(FxaaQualitySettings_t1558211909, ___edgeDetectionThreshold_1)); }
	inline float get_edgeDetectionThreshold_1() const { return ___edgeDetectionThreshold_1; }
	inline float* get_address_of_edgeDetectionThreshold_1() { return &___edgeDetectionThreshold_1; }
	inline void set_edgeDetectionThreshold_1(float value)
	{
		___edgeDetectionThreshold_1 = value;
	}

	inline static int32_t get_offset_of_minimumRequiredLuminance_2() { return static_cast<int32_t>(offsetof(FxaaQualitySettings_t1558211909, ___minimumRequiredLuminance_2)); }
	inline float get_minimumRequiredLuminance_2() const { return ___minimumRequiredLuminance_2; }
	inline float* get_address_of_minimumRequiredLuminance_2() { return &___minimumRequiredLuminance_2; }
	inline void set_minimumRequiredLuminance_2(float value)
	{
		___minimumRequiredLuminance_2 = value;
	}
};

struct FxaaQualitySettings_t1558211909_StaticFields
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings[] UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::presets
	FxaaQualitySettingsU5BU5D_t3129287688* ___presets_3;

public:
	inline static int32_t get_offset_of_presets_3() { return static_cast<int32_t>(offsetof(FxaaQualitySettings_t1558211909_StaticFields, ___presets_3)); }
	inline FxaaQualitySettingsU5BU5D_t3129287688* get_presets_3() const { return ___presets_3; }
	inline FxaaQualitySettingsU5BU5D_t3129287688** get_address_of_presets_3() { return &___presets_3; }
	inline void set_presets_3(FxaaQualitySettingsU5BU5D_t3129287688* value)
	{
		___presets_3 = value;
		Il2CppCodeGenWriteBarrier((&___presets_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAAQUALITYSETTINGS_T1558211909_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef TAASETTINGS_T2709374970_H
#define TAASETTINGS_T2709374970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/TaaSettings
struct  TaaSettings_t2709374970 
{
public:
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::jitterSpread
	float ___jitterSpread_0;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::sharpen
	float ___sharpen_1;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::stationaryBlending
	float ___stationaryBlending_2;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::motionBlending
	float ___motionBlending_3;

public:
	inline static int32_t get_offset_of_jitterSpread_0() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___jitterSpread_0)); }
	inline float get_jitterSpread_0() const { return ___jitterSpread_0; }
	inline float* get_address_of_jitterSpread_0() { return &___jitterSpread_0; }
	inline void set_jitterSpread_0(float value)
	{
		___jitterSpread_0 = value;
	}

	inline static int32_t get_offset_of_sharpen_1() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___sharpen_1)); }
	inline float get_sharpen_1() const { return ___sharpen_1; }
	inline float* get_address_of_sharpen_1() { return &___sharpen_1; }
	inline void set_sharpen_1(float value)
	{
		___sharpen_1 = value;
	}

	inline static int32_t get_offset_of_stationaryBlending_2() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___stationaryBlending_2)); }
	inline float get_stationaryBlending_2() const { return ___stationaryBlending_2; }
	inline float* get_address_of_stationaryBlending_2() { return &___stationaryBlending_2; }
	inline void set_stationaryBlending_2(float value)
	{
		___stationaryBlending_2 = value;
	}

	inline static int32_t get_offset_of_motionBlending_3() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___motionBlending_3)); }
	inline float get_motionBlending_3() const { return ___motionBlending_3; }
	inline float* get_address_of_motionBlending_3() { return &___motionBlending_3; }
	inline void set_motionBlending_3(float value)
	{
		___motionBlending_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAASETTINGS_T2709374970_H
#ifndef FXAACONSOLESETTINGS_T950185286_H
#define FXAACONSOLESETTINGS_T950185286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings
struct  FxaaConsoleSettings_t950185286 
{
public:
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::subpixelSpreadAmount
	float ___subpixelSpreadAmount_0;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::edgeSharpnessAmount
	float ___edgeSharpnessAmount_1;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::edgeDetectionThreshold
	float ___edgeDetectionThreshold_2;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::minimumRequiredLuminance
	float ___minimumRequiredLuminance_3;

public:
	inline static int32_t get_offset_of_subpixelSpreadAmount_0() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286, ___subpixelSpreadAmount_0)); }
	inline float get_subpixelSpreadAmount_0() const { return ___subpixelSpreadAmount_0; }
	inline float* get_address_of_subpixelSpreadAmount_0() { return &___subpixelSpreadAmount_0; }
	inline void set_subpixelSpreadAmount_0(float value)
	{
		___subpixelSpreadAmount_0 = value;
	}

	inline static int32_t get_offset_of_edgeSharpnessAmount_1() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286, ___edgeSharpnessAmount_1)); }
	inline float get_edgeSharpnessAmount_1() const { return ___edgeSharpnessAmount_1; }
	inline float* get_address_of_edgeSharpnessAmount_1() { return &___edgeSharpnessAmount_1; }
	inline void set_edgeSharpnessAmount_1(float value)
	{
		___edgeSharpnessAmount_1 = value;
	}

	inline static int32_t get_offset_of_edgeDetectionThreshold_2() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286, ___edgeDetectionThreshold_2)); }
	inline float get_edgeDetectionThreshold_2() const { return ___edgeDetectionThreshold_2; }
	inline float* get_address_of_edgeDetectionThreshold_2() { return &___edgeDetectionThreshold_2; }
	inline void set_edgeDetectionThreshold_2(float value)
	{
		___edgeDetectionThreshold_2 = value;
	}

	inline static int32_t get_offset_of_minimumRequiredLuminance_3() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286, ___minimumRequiredLuminance_3)); }
	inline float get_minimumRequiredLuminance_3() const { return ___minimumRequiredLuminance_3; }
	inline float* get_address_of_minimumRequiredLuminance_3() { return &___minimumRequiredLuminance_3; }
	inline void set_minimumRequiredLuminance_3(float value)
	{
		___minimumRequiredLuminance_3 = value;
	}
};

struct FxaaConsoleSettings_t950185286_StaticFields
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings[] UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::presets
	FxaaConsoleSettingsU5BU5D_t1126991587* ___presets_4;

public:
	inline static int32_t get_offset_of_presets_4() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286_StaticFields, ___presets_4)); }
	inline FxaaConsoleSettingsU5BU5D_t1126991587* get_presets_4() const { return ___presets_4; }
	inline FxaaConsoleSettingsU5BU5D_t1126991587** get_address_of_presets_4() { return &___presets_4; }
	inline void set_presets_4(FxaaConsoleSettingsU5BU5D_t1126991587* value)
	{
		___presets_4 = value;
		Il2CppCodeGenWriteBarrier((&___presets_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAACONSOLESETTINGS_T950185286_H
#ifndef POSTPROCESSINGCOMPONENT_1_T963284282_H
#define POSTPROCESSINGCOMPONENT_1_T963284282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.UserLutModel>
struct  PostProcessingComponent_1_t963284282  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	UserLutModel_t1670108080 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t963284282, ___U3CmodelU3Ek__BackingField_1)); }
	inline UserLutModel_t1670108080 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline UserLutModel_t1670108080 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(UserLutModel_t1670108080 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T963284282_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2138693379_H
#define POSTPROCESSINGCOMPONENT_1_T2138693379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.VignetteModel>
struct  PostProcessingComponent_1_t2138693379  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	VignetteModel_t2845517177 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2138693379, ___U3CmodelU3Ek__BackingField_1)); }
	inline VignetteModel_t2845517177 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline VignetteModel_t2845517177 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(VignetteModel_t2845517177 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2138693379_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef POSTPROCESSINGCOMPONENT_1_T814315590_H
#define POSTPROCESSINGCOMPONENT_1_T814315590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AntialiasingModel>
struct  PostProcessingComponent_1_t814315590  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AntialiasingModel_t1521139388 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t814315590, ___U3CmodelU3Ek__BackingField_1)); }
	inline AntialiasingModel_t1521139388 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AntialiasingModel_t1521139388 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AntialiasingModel_t1521139388 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T814315590_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3830967410_H
#define POSTPROCESSINGCOMPONENT_1_T3830967410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.EyeAdaptationModel>
struct  PostProcessingComponent_1_t3830967410  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	EyeAdaptationModel_t242823912 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3830967410, ___U3CmodelU3Ek__BackingField_1)); }
	inline EyeAdaptationModel_t242823912 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline EyeAdaptationModel_t242823912 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(EyeAdaptationModel_t242823912 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3830967410_H
#ifndef POSTPROCESSINGCOMPONENT_1_T1722181598_H
#define POSTPROCESSINGCOMPONENT_1_T1722181598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DitheringModel>
struct  PostProcessingComponent_1_t1722181598  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	DitheringModel_t2429005396 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t1722181598, ___U3CmodelU3Ek__BackingField_1)); }
	inline DitheringModel_t2429005396 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline DitheringModel_t2429005396 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(DitheringModel_t2429005396 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T1722181598_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2913864951_H
#define POSTPROCESSINGCOMPONENT_1_T2913864951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.FogModel>
struct  PostProcessingComponent_1_t2913864951  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	FogModel_t3620688749 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2913864951, ___U3CmodelU3Ek__BackingField_1)); }
	inline FogModel_t3620688749 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline FogModel_t3620688749 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(FogModel_t3620688749 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2913864951_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3977614564_H
#define POSTPROCESSINGCOMPONENT_1_T3977614564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponent_1_t3977614564  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AmbientOcclusionModel_t389471066 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3977614564, ___U3CmodelU3Ek__BackingField_1)); }
	inline AmbientOcclusionModel_t389471066 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AmbientOcclusionModel_t389471066 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AmbientOcclusionModel_t389471066 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3977614564_H
#ifndef POSTPROCESSINGCOMPONENT_1_T1392904062_H
#define POSTPROCESSINGCOMPONENT_1_T1392904062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponent_1_t1392904062  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BloomModel_t2099727860 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t1392904062, ___U3CmodelU3Ek__BackingField_1)); }
	inline BloomModel_t2099727860 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BloomModel_t2099727860 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BloomModel_t2099727860 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T1392904062_H
#ifndef POSTPROCESSINGCOMPONENT_1_T755795042_H
#define POSTPROCESSINGCOMPONENT_1_T755795042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct  PostProcessingComponent_1_t755795042  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BuiltinDebugViewsModel_t1462618840 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t755795042, ___U3CmodelU3Ek__BackingField_1)); }
	inline BuiltinDebugViewsModel_t1462618840 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BuiltinDebugViewsModel_t1462618840 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BuiltinDebugViewsModel_t1462618840 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T755795042_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3256576055_H
#define POSTPROCESSINGCOMPONENT_1_T3256576055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct  PostProcessingComponent_1_t3256576055  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ChromaticAberrationModel_t3963399853 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3256576055, ___U3CmodelU3Ek__BackingField_1)); }
	inline ChromaticAberrationModel_t3963399853 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ChromaticAberrationModel_t3963399853 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ChromaticAberrationModel_t3963399853 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3256576055_H
#ifndef POSTPROCESSINGCOMPONENT_1_T4102210828_H
#define POSTPROCESSINGCOMPONENT_1_T4102210828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DepthOfFieldModel>
struct  PostProcessingComponent_1_t4102210828  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	DepthOfFieldModel_t514067330 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t4102210828, ___U3CmodelU3Ek__BackingField_1)); }
	inline DepthOfFieldModel_t514067330 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline DepthOfFieldModel_t514067330 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(DepthOfFieldModel_t514067330 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T4102210828_H
#ifndef POSTPROCESSINGCOMPONENT_1_T741224383_H
#define POSTPROCESSINGCOMPONENT_1_T741224383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ColorGradingModel>
struct  PostProcessingComponent_1_t741224383  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ColorGradingModel_t1448048181 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t741224383, ___U3CmodelU3Ek__BackingField_1)); }
	inline ColorGradingModel_t1448048181 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ColorGradingModel_t1448048181 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ColorGradingModel_t1448048181 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T741224383_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.VignetteModel>
struct  PostProcessingComponentRenderTexture_1_t118834922  : public PostProcessingComponent_1_t2138693379
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.UserLutModel>
struct  PostProcessingComponentRenderTexture_1_t3238393121  : public PostProcessingComponent_1_t963284282
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#ifndef SAMPLECOUNT_T1158000259_H
#define SAMPLECOUNT_T1158000259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount
struct  SampleCount_t1158000259 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleCount_t1158000259, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECOUNT_T1158000259_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef METHOD_T287042102_H
#define METHOD_T287042102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/Method
struct  Method_t287042102 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AntialiasingModel/Method::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Method_t287042102, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T287042102_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.GrainModel>
struct  PostProcessingComponentRenderTexture_1_t2721167529  : public PostProcessingComponent_1_t446058690
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#ifndef FXAAPRESET_T2149486832_H
#define FXAAPRESET_T2149486832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset
struct  FxaaPreset_t2149486832 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FxaaPreset_t2149486832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAAPRESET_T2149486832_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponentCommandBuffer_1_t1664772955  : public PostProcessingComponent_1_t3977614564
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DepthOfFieldModel>
struct  PostProcessingComponentRenderTexture_1_t2082352371  : public PostProcessingComponent_1_t4102210828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DitheringModel>
struct  PostProcessingComponentRenderTexture_1_t3997290437  : public PostProcessingComponent_1_t1722181598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ColorGradingModel>
struct  PostProcessingComponentRenderTexture_1_t3016333222  : public PostProcessingComponent_1_t741224383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct  PostProcessingComponentCommandBuffer_1_t2737920729  : public PostProcessingComponent_1_t755795042
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct  PostProcessingComponentRenderTexture_1_t1236717598  : public PostProcessingComponent_1_t3256576055
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponentRenderTexture_1_t3668012901  : public PostProcessingComponent_1_t1392904062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.MotionBlurModel>
struct  PostProcessingComponentCommandBuffer_1_t60620716  : public PostProcessingComponent_1_t2373462325
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>
struct  PostProcessingComponentCommandBuffer_1_t6679325  : public PostProcessingComponent_1_t2319520934
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.AntialiasingModel>
struct  PostProcessingComponentRenderTexture_1_t3089424429  : public PostProcessingComponent_1_t814315590
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.EyeAdaptationModel>
struct  PostProcessingComponentRenderTexture_1_t1811108953  : public PostProcessingComponent_1_t3830967410
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.FogModel>
struct  PostProcessingComponentCommandBuffer_1_t601023342  : public PostProcessingComponent_1_t2913864951
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#ifndef PASS_T2620402414_H
#define PASS_T2620402414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/Pass
struct  Pass_t2620402414 
{
public:
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Pass::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pass_t2620402414, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T2620402414_H
#ifndef GETSETATTRIBUTE_T1349027187_H
#define GETSETATTRIBUTE_T1349027187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GetSetAttribute
struct  GetSetAttribute_t1349027187  : public PropertyAttribute_t3677895545
{
public:
	// System.String UnityEngine.PostProcessing.GetSetAttribute::name
	String_t* ___name_0;
	// System.Boolean UnityEngine.PostProcessing.GetSetAttribute::dirty
	bool ___dirty_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GetSetAttribute_t1349027187, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(GetSetAttribute_t1349027187, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSETATTRIBUTE_T1349027187_H
#ifndef WAYSTODETERMINEBOUNDS_T264359748_H
#define WAYSTODETERMINEBOUNDS_T264359748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.ObjectBounds/WaysToDetermineBounds
struct  WaysToDetermineBounds_t264359748 
{
public:
	// System.Int32 MoreMountains.Tools.ObjectBounds/WaysToDetermineBounds::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaysToDetermineBounds_t264359748, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYSTODETERMINEBOUNDS_T264359748_H
#ifndef POOLINGMETHODS_T920590136_H
#define POOLINGMETHODS_T920590136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PoolingMethods
struct  PoolingMethods_t920590136 
{
public:
	// System.Int32 MoreMountains.Tools.PoolingMethods::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PoolingMethods_t920590136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLINGMETHODS_T920590136_H
#ifndef OCCLUSIONSOURCE_T4221238007_H
#define OCCLUSIONSOURCE_T4221238007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource
struct  OcclusionSource_t4221238007 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OcclusionSource_t4221238007, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCCLUSIONSOURCE_T4221238007_H
#ifndef PASS_T2117482_H
#define PASS_T2117482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Pass
struct  Pass_t2117482 
{
public:
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Pass::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pass_t2117482, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T2117482_H
#ifndef TRACKBALLATTRIBUTE_T219960417_H
#define TRACKBALLATTRIBUTE_T219960417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballAttribute
struct  TrackballAttribute_t219960417  : public PropertyAttribute_t3677895545
{
public:
	// System.String UnityEngine.PostProcessing.TrackballAttribute::method
	String_t* ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(TrackballAttribute_t219960417, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLATTRIBUTE_T219960417_H
#ifndef TRACKBALLGROUPATTRIBUTE_T624107828_H
#define TRACKBALLGROUPATTRIBUTE_T624107828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballGroupAttribute
struct  TrackballGroupAttribute_t624107828  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLGROUPATTRIBUTE_T624107828_H
#ifndef U3CMOVEFROMTOU3EC__ITERATOR0_T1354840102_H
#define U3CMOVEFROMTOU3EC__ITERATOR0_T1354840102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0
struct  U3CMoveFromToU3Ec__Iterator0_t1354840102  : public RuntimeObject
{
public:
	// System.Single MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityEngine.GameObject MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::movingObject
	GameObject_t1113636619 * ___movingObject_1;
	// UnityEngine.Vector3 MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::pointB
	Vector3_t3722313464  ___pointB_2;
	// System.Single MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::<distance>__0
	float ___U3CdistanceU3E__0_3;
	// System.Single MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::approximationDistance
	float ___approximationDistance_4;
	// System.Single MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::time
	float ___time_5;
	// UnityEngine.Vector3 MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::pointA
	Vector3_t3722313464  ___pointA_6;
	// System.Object MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 MoreMountains.Tools.MMMovement/<MoveFromTo>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_movingObject_1() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___movingObject_1)); }
	inline GameObject_t1113636619 * get_movingObject_1() const { return ___movingObject_1; }
	inline GameObject_t1113636619 ** get_address_of_movingObject_1() { return &___movingObject_1; }
	inline void set_movingObject_1(GameObject_t1113636619 * value)
	{
		___movingObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___movingObject_1), value);
	}

	inline static int32_t get_offset_of_pointB_2() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___pointB_2)); }
	inline Vector3_t3722313464  get_pointB_2() const { return ___pointB_2; }
	inline Vector3_t3722313464 * get_address_of_pointB_2() { return &___pointB_2; }
	inline void set_pointB_2(Vector3_t3722313464  value)
	{
		___pointB_2 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3E__0_3() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___U3CdistanceU3E__0_3)); }
	inline float get_U3CdistanceU3E__0_3() const { return ___U3CdistanceU3E__0_3; }
	inline float* get_address_of_U3CdistanceU3E__0_3() { return &___U3CdistanceU3E__0_3; }
	inline void set_U3CdistanceU3E__0_3(float value)
	{
		___U3CdistanceU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_approximationDistance_4() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___approximationDistance_4)); }
	inline float get_approximationDistance_4() const { return ___approximationDistance_4; }
	inline float* get_address_of_approximationDistance_4() { return &___approximationDistance_4; }
	inline void set_approximationDistance_4(float value)
	{
		___approximationDistance_4 = value;
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___time_5)); }
	inline float get_time_5() const { return ___time_5; }
	inline float* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(float value)
	{
		___time_5 = value;
	}

	inline static int32_t get_offset_of_pointA_6() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___pointA_6)); }
	inline Vector3_t3722313464  get_pointA_6() const { return ___pointA_6; }
	inline Vector3_t3722313464 * get_address_of_pointA_6() { return &___pointA_6; }
	inline void set_pointA_6(Vector3_t3722313464  value)
	{
		___pointA_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ec__Iterator0_t1354840102, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVEFROMTOU3EC__ITERATOR0_T1354840102_H
#ifndef U3CFADEIMAGEU3EC__ITERATOR0_T2472898010_H
#define U3CFADEIMAGEU3EC__ITERATOR0_T2472898010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0
struct  U3CFadeImageU3Ec__Iterator0_t2472898010  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::target
	Image_t2670269651 * ___target_0;
	// System.Single MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::<alpha>__0
	float ___U3CalphaU3E__0_1;
	// System.Single MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::<t>__1
	float ___U3CtU3E__1_2;
	// UnityEngine.Color MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::color
	Color_t2555686324  ___color_3;
	// UnityEngine.Color MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::<newColor>__2
	Color_t2555686324  ___U3CnewColorU3E__2_4;
	// System.Single MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::duration
	float ___duration_5;
	// System.Object MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 MoreMountains.Tools.MMFade/<FadeImage>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___target_0)); }
	inline Image_t2670269651 * get_target_0() const { return ___target_0; }
	inline Image_t2670269651 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t2670269651 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___U3CalphaU3E__0_1)); }
	inline float get_U3CalphaU3E__0_1() const { return ___U3CalphaU3E__0_1; }
	inline float* get_address_of_U3CalphaU3E__0_1() { return &___U3CalphaU3E__0_1; }
	inline void set_U3CalphaU3E__0_1(float value)
	{
		___U3CalphaU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_2() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___U3CtU3E__1_2)); }
	inline float get_U3CtU3E__1_2() const { return ___U3CtU3E__1_2; }
	inline float* get_address_of_U3CtU3E__1_2() { return &___U3CtU3E__1_2; }
	inline void set_U3CtU3E__1_2(float value)
	{
		___U3CtU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___color_3)); }
	inline Color_t2555686324  get_color_3() const { return ___color_3; }
	inline Color_t2555686324 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color_t2555686324  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_U3CnewColorU3E__2_4() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___U3CnewColorU3E__2_4)); }
	inline Color_t2555686324  get_U3CnewColorU3E__2_4() const { return ___U3CnewColorU3E__2_4; }
	inline Color_t2555686324 * get_address_of_U3CnewColorU3E__2_4() { return &___U3CnewColorU3E__2_4; }
	inline void set_U3CnewColorU3E__2_4(Color_t2555686324  value)
	{
		___U3CnewColorU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CFadeImageU3Ec__Iterator0_t2472898010, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEIMAGEU3EC__ITERATOR0_T2472898010_H
#ifndef U3CFADETEXTU3EC__ITERATOR1_T1686573558_H
#define U3CFADETEXTU3EC__ITERATOR1_T1686573558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMFade/<FadeText>c__Iterator1
struct  U3CFadeTextU3Ec__Iterator1_t1686573558  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::target
	Text_t1901882714 * ___target_0;
	// System.Single MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::<alpha>__0
	float ___U3CalphaU3E__0_1;
	// System.Single MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::<t>__1
	float ___U3CtU3E__1_2;
	// UnityEngine.Color MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::color
	Color_t2555686324  ___color_3;
	// UnityEngine.Color MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::<newColor>__2
	Color_t2555686324  ___U3CnewColorU3E__2_4;
	// System.Single MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::duration
	float ___duration_5;
	// System.Object MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 MoreMountains.Tools.MMFade/<FadeText>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___U3CalphaU3E__0_1)); }
	inline float get_U3CalphaU3E__0_1() const { return ___U3CalphaU3E__0_1; }
	inline float* get_address_of_U3CalphaU3E__0_1() { return &___U3CalphaU3E__0_1; }
	inline void set_U3CalphaU3E__0_1(float value)
	{
		___U3CalphaU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_2() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___U3CtU3E__1_2)); }
	inline float get_U3CtU3E__1_2() const { return ___U3CtU3E__1_2; }
	inline float* get_address_of_U3CtU3E__1_2() { return &___U3CtU3E__1_2; }
	inline void set_U3CtU3E__1_2(float value)
	{
		___U3CtU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___color_3)); }
	inline Color_t2555686324  get_color_3() const { return ___color_3; }
	inline Color_t2555686324 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color_t2555686324  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_U3CnewColorU3E__2_4() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___U3CnewColorU3E__2_4)); }
	inline Color_t2555686324  get_U3CnewColorU3E__2_4() const { return ___U3CnewColorU3E__2_4; }
	inline Color_t2555686324 * get_address_of_U3CnewColorU3E__2_4() { return &___U3CnewColorU3E__2_4; }
	inline void set_U3CnewColorU3E__2_4(Color_t2555686324  value)
	{
		___U3CnewColorU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CFadeTextU3Ec__Iterator1_t1686573558, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADETEXTU3EC__ITERATOR1_T1686573558_H
#ifndef MINATTRIBUTE_T4172004135_H
#define MINATTRIBUTE_T4172004135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MinAttribute
struct  MinAttribute_t4172004135  : public PropertyAttribute_t3677895545
{
public:
	// System.Single UnityEngine.PostProcessing.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t4172004135, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T4172004135_H
#ifndef U3CFADESPRITEU3EC__ITERATOR2_T3661250087_H
#define U3CFADESPRITEU3EC__ITERATOR2_T3661250087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2
struct  U3CFadeSpriteU3Ec__Iterator2_t3661250087  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::target
	SpriteRenderer_t3235626157 * ___target_0;
	// System.Single MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::<alpha>__0
	float ___U3CalphaU3E__0_1;
	// System.Single MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::<t>__0
	float ___U3CtU3E__0_2;
	// UnityEngine.Color MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::color
	Color_t2555686324  ___color_3;
	// UnityEngine.Color MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::<newColor>__1
	Color_t2555686324  ___U3CnewColorU3E__1_4;
	// System.Single MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::duration
	float ___duration_5;
	// UnityEngine.Color MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::<finalColor>__0
	Color_t2555686324  ___U3CfinalColorU3E__0_6;
	// System.Object MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::$disposing
	bool ___U24disposing_8;
	// System.Int32 MoreMountains.Tools.MMFade/<FadeSprite>c__Iterator2::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___target_0)); }
	inline SpriteRenderer_t3235626157 * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3235626157 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3235626157 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___U3CalphaU3E__0_1)); }
	inline float get_U3CalphaU3E__0_1() const { return ___U3CalphaU3E__0_1; }
	inline float* get_address_of_U3CalphaU3E__0_1() { return &___U3CalphaU3E__0_1; }
	inline void set_U3CalphaU3E__0_1(float value)
	{
		___U3CalphaU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___U3CtU3E__0_2)); }
	inline float get_U3CtU3E__0_2() const { return ___U3CtU3E__0_2; }
	inline float* get_address_of_U3CtU3E__0_2() { return &___U3CtU3E__0_2; }
	inline void set_U3CtU3E__0_2(float value)
	{
		___U3CtU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___color_3)); }
	inline Color_t2555686324  get_color_3() const { return ___color_3; }
	inline Color_t2555686324 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color_t2555686324  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_U3CnewColorU3E__1_4() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___U3CnewColorU3E__1_4)); }
	inline Color_t2555686324  get_U3CnewColorU3E__1_4() const { return ___U3CnewColorU3E__1_4; }
	inline Color_t2555686324 * get_address_of_U3CnewColorU3E__1_4() { return &___U3CnewColorU3E__1_4; }
	inline void set_U3CnewColorU3E__1_4(Color_t2555686324  value)
	{
		___U3CnewColorU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_U3CfinalColorU3E__0_6() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___U3CfinalColorU3E__0_6)); }
	inline Color_t2555686324  get_U3CfinalColorU3E__0_6() const { return ___U3CfinalColorU3E__0_6; }
	inline Color_t2555686324 * get_address_of_U3CfinalColorU3E__0_6() { return &___U3CfinalColorU3E__0_6; }
	inline void set_U3CfinalColorU3E__0_6(Color_t2555686324  value)
	{
		___U3CfinalColorU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CFadeSpriteU3Ec__Iterator2_t3661250087, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADESPRITEU3EC__ITERATOR2_T3661250087_H
#ifndef BUTTONSTATES_T4048788823_H
#define BUTTONSTATES_T4048788823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMInput/ButtonStates
struct  ButtonStates_t4048788823 
{
public:
	// System.Int32 MoreMountains.Tools.MMInput/ButtonStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ButtonStates_t4048788823, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATES_T4048788823_H
#ifndef PASSINDEX_T1642913883_H
#define PASSINDEX_T1642913883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/PassIndex
struct  PassIndex_t1642913883 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/PassIndex::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PassIndex_t1642913883, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSINDEX_T1642913883_H
#ifndef U3CFLICKERU3EC__ITERATOR0_T2968401774_H
#define U3CFLICKERU3EC__ITERATOR0_T2968401774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMImage/<Flicker>c__Iterator0
struct  U3CFlickerU3Ec__Iterator0_t2968401774  : public RuntimeObject
{
public:
	// UnityEngine.Renderer MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::renderer
	Renderer_t2627027031 * ___renderer_0;
	// UnityEngine.Color MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::initialColor
	Color_t2555686324  ___initialColor_1;
	// UnityEngine.Color MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::flickerColor
	Color_t2555686324  ___flickerColor_2;
	// System.Single MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::flickerDuration
	float ___flickerDuration_3;
	// System.Single MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::<flickerStop>__0
	float ___U3CflickerStopU3E__0_4;
	// System.Single MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::flickerSpeed
	float ___flickerSpeed_5;
	// System.Object MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 MoreMountains.Tools.MMImage/<Flicker>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_renderer_0() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___renderer_0)); }
	inline Renderer_t2627027031 * get_renderer_0() const { return ___renderer_0; }
	inline Renderer_t2627027031 ** get_address_of_renderer_0() { return &___renderer_0; }
	inline void set_renderer_0(Renderer_t2627027031 * value)
	{
		___renderer_0 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_0), value);
	}

	inline static int32_t get_offset_of_initialColor_1() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___initialColor_1)); }
	inline Color_t2555686324  get_initialColor_1() const { return ___initialColor_1; }
	inline Color_t2555686324 * get_address_of_initialColor_1() { return &___initialColor_1; }
	inline void set_initialColor_1(Color_t2555686324  value)
	{
		___initialColor_1 = value;
	}

	inline static int32_t get_offset_of_flickerColor_2() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___flickerColor_2)); }
	inline Color_t2555686324  get_flickerColor_2() const { return ___flickerColor_2; }
	inline Color_t2555686324 * get_address_of_flickerColor_2() { return &___flickerColor_2; }
	inline void set_flickerColor_2(Color_t2555686324  value)
	{
		___flickerColor_2 = value;
	}

	inline static int32_t get_offset_of_flickerDuration_3() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___flickerDuration_3)); }
	inline float get_flickerDuration_3() const { return ___flickerDuration_3; }
	inline float* get_address_of_flickerDuration_3() { return &___flickerDuration_3; }
	inline void set_flickerDuration_3(float value)
	{
		___flickerDuration_3 = value;
	}

	inline static int32_t get_offset_of_U3CflickerStopU3E__0_4() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___U3CflickerStopU3E__0_4)); }
	inline float get_U3CflickerStopU3E__0_4() const { return ___U3CflickerStopU3E__0_4; }
	inline float* get_address_of_U3CflickerStopU3E__0_4() { return &___U3CflickerStopU3E__0_4; }
	inline void set_U3CflickerStopU3E__0_4(float value)
	{
		___U3CflickerStopU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_flickerSpeed_5() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___flickerSpeed_5)); }
	inline float get_flickerSpeed_5() const { return ___flickerSpeed_5; }
	inline float* get_address_of_flickerSpeed_5() { return &___flickerSpeed_5; }
	inline void set_flickerSpeed_5(float value)
	{
		___flickerSpeed_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator0_t2968401774, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLICKERU3EC__ITERATOR0_T2968401774_H
#ifndef CHROMATICABERRATIONCOMPONENT_T1647263118_H
#define CHROMATICABERRATIONCOMPONENT_T1647263118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationComponent
struct  ChromaticAberrationComponent_t1647263118  : public PostProcessingComponentRenderTexture_1_t1236717598
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ChromaticAberrationComponent::m_SpectrumLut
	Texture2D_t3840446185 * ___m_SpectrumLut_2;

public:
	inline static int32_t get_offset_of_m_SpectrumLut_2() { return static_cast<int32_t>(offsetof(ChromaticAberrationComponent_t1647263118, ___m_SpectrumLut_2)); }
	inline Texture2D_t3840446185 * get_m_SpectrumLut_2() const { return ___m_SpectrumLut_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_SpectrumLut_2() { return &___m_SpectrumLut_2; }
	inline void set_m_SpectrumLut_2(Texture2D_t3840446185 * value)
	{
		___m_SpectrumLut_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpectrumLut_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATIONCOMPONENT_T1647263118_H
#ifndef BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#define BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent
struct  BuiltinDebugViewsComponent_t2123147871  : public PostProcessingComponentCommandBuffer_1_t2737920729
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray UnityEngine.PostProcessing.BuiltinDebugViewsComponent::m_Arrows
	ArrowArray_t303178545 * ___m_Arrows_3;

public:
	inline static int32_t get_offset_of_m_Arrows_3() { return static_cast<int32_t>(offsetof(BuiltinDebugViewsComponent_t2123147871, ___m_Arrows_3)); }
	inline ArrowArray_t303178545 * get_m_Arrows_3() const { return ___m_Arrows_3; }
	inline ArrowArray_t303178545 ** get_address_of_m_Arrows_3() { return &___m_Arrows_3; }
	inline void set_m_Arrows_3(ArrowArray_t303178545 * value)
	{
		___m_Arrows_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arrows_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#ifndef BLOOMCOMPONENT_T3791419130_H
#define BLOOMCOMPONENT_T3791419130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent
struct  BloomComponent_t3791419130  : public PostProcessingComponentRenderTexture_1_t3668012901
{
public:
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer1
	RenderTextureU5BU5D_t4111643188* ___m_BlurBuffer1_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer2
	RenderTextureU5BU5D_t4111643188* ___m_BlurBuffer2_4;

public:
	inline static int32_t get_offset_of_m_BlurBuffer1_3() { return static_cast<int32_t>(offsetof(BloomComponent_t3791419130, ___m_BlurBuffer1_3)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_BlurBuffer1_3() const { return ___m_BlurBuffer1_3; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_BlurBuffer1_3() { return &___m_BlurBuffer1_3; }
	inline void set_m_BlurBuffer1_3(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_BlurBuffer1_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer1_3), value);
	}

	inline static int32_t get_offset_of_m_BlurBuffer2_4() { return static_cast<int32_t>(offsetof(BloomComponent_t3791419130, ___m_BlurBuffer2_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_BlurBuffer2_4() const { return ___m_BlurBuffer2_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_BlurBuffer2_4() { return &___m_BlurBuffer2_4; }
	inline void set_m_BlurBuffer2_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_BlurBuffer2_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMCOMPONENT_T3791419130_H
#ifndef VIGNETTECOMPONENT_T3243642943_H
#define VIGNETTECOMPONENT_T3243642943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteComponent
struct  VignetteComponent_t3243642943  : public PostProcessingComponentRenderTexture_1_t118834922
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTECOMPONENT_T3243642943_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#define AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent
struct  AmbientOcclusionComponent_t4130625043  : public PostProcessingComponentCommandBuffer_1_t1664772955
{
public:
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.PostProcessing.AmbientOcclusionComponent::m_MRT
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_MRT_4;

public:
	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(AmbientOcclusionComponent_t4130625043, ___m_MRT_4)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#ifndef TAACOMPONENT_T3791749658_H
#define TAACOMPONENT_T3791749658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TaaComponent
struct  TaaComponent_t3791749658  : public PostProcessingComponentRenderTexture_1_t3089424429
{
public:
	// UnityEngine.RenderBuffer[] UnityEngine.PostProcessing.TaaComponent::m_MRT
	RenderBufferU5BU5D_t1615831949* ___m_MRT_4;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent::m_SampleIndex
	int32_t ___m_SampleIndex_5;
	// System.Boolean UnityEngine.PostProcessing.TaaComponent::m_ResetHistory
	bool ___m_ResetHistory_6;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.TaaComponent::m_HistoryTexture
	RenderTexture_t2108887433 * ___m_HistoryTexture_7;

public:
	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_MRT_4)); }
	inline RenderBufferU5BU5D_t1615831949* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderBufferU5BU5D_t1615831949** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderBufferU5BU5D_t1615831949* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}

	inline static int32_t get_offset_of_m_SampleIndex_5() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_SampleIndex_5)); }
	inline int32_t get_m_SampleIndex_5() const { return ___m_SampleIndex_5; }
	inline int32_t* get_address_of_m_SampleIndex_5() { return &___m_SampleIndex_5; }
	inline void set_m_SampleIndex_5(int32_t value)
	{
		___m_SampleIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_ResetHistory_6() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_ResetHistory_6)); }
	inline bool get_m_ResetHistory_6() const { return ___m_ResetHistory_6; }
	inline bool* get_address_of_m_ResetHistory_6() { return &___m_ResetHistory_6; }
	inline void set_m_ResetHistory_6(bool value)
	{
		___m_ResetHistory_6 = value;
	}

	inline static int32_t get_offset_of_m_HistoryTexture_7() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_HistoryTexture_7)); }
	inline RenderTexture_t2108887433 * get_m_HistoryTexture_7() const { return ___m_HistoryTexture_7; }
	inline RenderTexture_t2108887433 ** get_address_of_m_HistoryTexture_7() { return &___m_HistoryTexture_7; }
	inline void set_m_HistoryTexture_7(RenderTexture_t2108887433 * value)
	{
		___m_HistoryTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryTexture_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAACOMPONENT_T3791749658_H
#ifndef SETTINGS_T3016786575_H
#define SETTINGS_T3016786575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct  Settings_t3016786575 
{
public:
	// System.Single UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::intensity
	float ___intensity_0;
	// System.Single UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::radius
	float ___radius_1;
	// UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::sampleCount
	int32_t ___sampleCount_2;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::downsampling
	bool ___downsampling_3;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::forceForwardCompatibility
	bool ___forceForwardCompatibility_4;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::ambientOnly
	bool ___ambientOnly_5;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::highPrecision
	bool ___highPrecision_6;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}

	inline static int32_t get_offset_of_radius_1() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___radius_1)); }
	inline float get_radius_1() const { return ___radius_1; }
	inline float* get_address_of_radius_1() { return &___radius_1; }
	inline void set_radius_1(float value)
	{
		___radius_1 = value;
	}

	inline static int32_t get_offset_of_sampleCount_2() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___sampleCount_2)); }
	inline int32_t get_sampleCount_2() const { return ___sampleCount_2; }
	inline int32_t* get_address_of_sampleCount_2() { return &___sampleCount_2; }
	inline void set_sampleCount_2(int32_t value)
	{
		___sampleCount_2 = value;
	}

	inline static int32_t get_offset_of_downsampling_3() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___downsampling_3)); }
	inline bool get_downsampling_3() const { return ___downsampling_3; }
	inline bool* get_address_of_downsampling_3() { return &___downsampling_3; }
	inline void set_downsampling_3(bool value)
	{
		___downsampling_3 = value;
	}

	inline static int32_t get_offset_of_forceForwardCompatibility_4() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___forceForwardCompatibility_4)); }
	inline bool get_forceForwardCompatibility_4() const { return ___forceForwardCompatibility_4; }
	inline bool* get_address_of_forceForwardCompatibility_4() { return &___forceForwardCompatibility_4; }
	inline void set_forceForwardCompatibility_4(bool value)
	{
		___forceForwardCompatibility_4 = value;
	}

	inline static int32_t get_offset_of_ambientOnly_5() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___ambientOnly_5)); }
	inline bool get_ambientOnly_5() const { return ___ambientOnly_5; }
	inline bool* get_address_of_ambientOnly_5() { return &___ambientOnly_5; }
	inline void set_ambientOnly_5(bool value)
	{
		___ambientOnly_5 = value;
	}

	inline static int32_t get_offset_of_highPrecision_6() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___highPrecision_6)); }
	inline bool get_highPrecision_6() const { return ___highPrecision_6; }
	inline bool* get_address_of_highPrecision_6() { return &___highPrecision_6; }
	inline void set_highPrecision_6(bool value)
	{
		___highPrecision_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct Settings_t3016786575_marshaled_pinvoke
{
	float ___intensity_0;
	float ___radius_1;
	int32_t ___sampleCount_2;
	int32_t ___downsampling_3;
	int32_t ___forceForwardCompatibility_4;
	int32_t ___ambientOnly_5;
	int32_t ___highPrecision_6;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct Settings_t3016786575_marshaled_com
{
	float ___intensity_0;
	float ___radius_1;
	int32_t ___sampleCount_2;
	int32_t ___downsampling_3;
	int32_t ___forceForwardCompatibility_4;
	int32_t ___ambientOnly_5;
	int32_t ___highPrecision_6;
};
#endif // SETTINGS_T3016786575_H
#ifndef MOTIONBLURCOMPONENT_T3686516877_H
#define MOTIONBLURCOMPONENT_T3686516877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent
struct  MotionBlurComponent_t3686516877  : public PostProcessingComponentCommandBuffer_1_t60620716
{
public:
	// UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter UnityEngine.PostProcessing.MotionBlurComponent::m_ReconstructionFilter
	ReconstructionFilter_t705677647 * ___m_ReconstructionFilter_2;
	// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter UnityEngine.PostProcessing.MotionBlurComponent::m_FrameBlendingFilter
	FrameBlendingFilter_t2699796096 * ___m_FrameBlendingFilter_3;
	// System.Boolean UnityEngine.PostProcessing.MotionBlurComponent::m_FirstFrame
	bool ___m_FirstFrame_4;

public:
	inline static int32_t get_offset_of_m_ReconstructionFilter_2() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_ReconstructionFilter_2)); }
	inline ReconstructionFilter_t705677647 * get_m_ReconstructionFilter_2() const { return ___m_ReconstructionFilter_2; }
	inline ReconstructionFilter_t705677647 ** get_address_of_m_ReconstructionFilter_2() { return &___m_ReconstructionFilter_2; }
	inline void set_m_ReconstructionFilter_2(ReconstructionFilter_t705677647 * value)
	{
		___m_ReconstructionFilter_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReconstructionFilter_2), value);
	}

	inline static int32_t get_offset_of_m_FrameBlendingFilter_3() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_FrameBlendingFilter_3)); }
	inline FrameBlendingFilter_t2699796096 * get_m_FrameBlendingFilter_3() const { return ___m_FrameBlendingFilter_3; }
	inline FrameBlendingFilter_t2699796096 ** get_address_of_m_FrameBlendingFilter_3() { return &___m_FrameBlendingFilter_3; }
	inline void set_m_FrameBlendingFilter_3(FrameBlendingFilter_t2699796096 * value)
	{
		___m_FrameBlendingFilter_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_FrameBlendingFilter_3), value);
	}

	inline static int32_t get_offset_of_m_FirstFrame_4() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_FirstFrame_4)); }
	inline bool get_m_FirstFrame_4() const { return ___m_FirstFrame_4; }
	inline bool* get_address_of_m_FirstFrame_4() { return &___m_FirstFrame_4; }
	inline void set_m_FirstFrame_4(bool value)
	{
		___m_FirstFrame_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURCOMPONENT_T3686516877_H
#ifndef USERLUTCOMPONENT_T2843161776_H
#define USERLUTCOMPONENT_T2843161776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutComponent
struct  UserLutComponent_t2843161776  : public PostProcessingComponentRenderTexture_1_t3238393121
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERLUTCOMPONENT_T2843161776_H
#ifndef RECONSTRUCTIONFILTER_T705677647_H
#define RECONSTRUCTIONFILTER_T705677647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter
struct  ReconstructionFilter_t705677647  : public RuntimeObject
{
public:
	// UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter::m_VectorRTFormat
	int32_t ___m_VectorRTFormat_0;
	// UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter::m_PackedRTFormat
	int32_t ___m_PackedRTFormat_1;

public:
	inline static int32_t get_offset_of_m_VectorRTFormat_0() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t705677647, ___m_VectorRTFormat_0)); }
	inline int32_t get_m_VectorRTFormat_0() const { return ___m_VectorRTFormat_0; }
	inline int32_t* get_address_of_m_VectorRTFormat_0() { return &___m_VectorRTFormat_0; }
	inline void set_m_VectorRTFormat_0(int32_t value)
	{
		___m_VectorRTFormat_0 = value;
	}

	inline static int32_t get_offset_of_m_PackedRTFormat_1() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t705677647, ___m_PackedRTFormat_1)); }
	inline int32_t get_m_PackedRTFormat_1() const { return ___m_PackedRTFormat_1; }
	inline int32_t* get_address_of_m_PackedRTFormat_1() { return &___m_PackedRTFormat_1; }
	inline void set_m_PackedRTFormat_1(int32_t value)
	{
		___m_PackedRTFormat_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFILTER_T705677647_H
#ifndef FRAMEBLENDINGFILTER_T2699796096_H
#define FRAMEBLENDINGFILTER_T2699796096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter
struct  FrameBlendingFilter_t2699796096  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::m_UseCompression
	bool ___m_UseCompression_0;
	// UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::m_RawTextureFormat
	int32_t ___m_RawTextureFormat_1;
	// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame[] UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::m_FrameList
	FrameU5BU5D_t1363420656* ___m_FrameList_2;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::m_LastFrameCount
	int32_t ___m_LastFrameCount_3;

public:
	inline static int32_t get_offset_of_m_UseCompression_0() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t2699796096, ___m_UseCompression_0)); }
	inline bool get_m_UseCompression_0() const { return ___m_UseCompression_0; }
	inline bool* get_address_of_m_UseCompression_0() { return &___m_UseCompression_0; }
	inline void set_m_UseCompression_0(bool value)
	{
		___m_UseCompression_0 = value;
	}

	inline static int32_t get_offset_of_m_RawTextureFormat_1() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t2699796096, ___m_RawTextureFormat_1)); }
	inline int32_t get_m_RawTextureFormat_1() const { return ___m_RawTextureFormat_1; }
	inline int32_t* get_address_of_m_RawTextureFormat_1() { return &___m_RawTextureFormat_1; }
	inline void set_m_RawTextureFormat_1(int32_t value)
	{
		___m_RawTextureFormat_1 = value;
	}

	inline static int32_t get_offset_of_m_FrameList_2() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t2699796096, ___m_FrameList_2)); }
	inline FrameU5BU5D_t1363420656* get_m_FrameList_2() const { return ___m_FrameList_2; }
	inline FrameU5BU5D_t1363420656** get_address_of_m_FrameList_2() { return &___m_FrameList_2; }
	inline void set_m_FrameList_2(FrameU5BU5D_t1363420656* value)
	{
		___m_FrameList_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FrameList_2), value);
	}

	inline static int32_t get_offset_of_m_LastFrameCount_3() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t2699796096, ___m_LastFrameCount_3)); }
	inline int32_t get_m_LastFrameCount_3() const { return ___m_LastFrameCount_3; }
	inline int32_t* get_address_of_m_LastFrameCount_3() { return &___m_LastFrameCount_3; }
	inline void set_m_LastFrameCount_3(int32_t value)
	{
		___m_LastFrameCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEBLENDINGFILTER_T2699796096_H
#ifndef SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#define SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent
struct  ScreenSpaceReflectionComponent_t856094247  : public PostProcessingComponentCommandBuffer_1_t6679325
{
public:
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_HighlightSuppression
	bool ___k_HighlightSuppression_2;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_TraceBehindObjects
	bool ___k_TraceBehindObjects_3;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_TreatBackfaceHitAsMiss
	bool ___k_TreatBackfaceHitAsMiss_4;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_BilateralUpsample
	bool ___k_BilateralUpsample_5;
	// System.Int32[] UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::m_ReflectionTextures
	Int32U5BU5D_t385246372* ___m_ReflectionTextures_6;

public:
	inline static int32_t get_offset_of_k_HighlightSuppression_2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_HighlightSuppression_2)); }
	inline bool get_k_HighlightSuppression_2() const { return ___k_HighlightSuppression_2; }
	inline bool* get_address_of_k_HighlightSuppression_2() { return &___k_HighlightSuppression_2; }
	inline void set_k_HighlightSuppression_2(bool value)
	{
		___k_HighlightSuppression_2 = value;
	}

	inline static int32_t get_offset_of_k_TraceBehindObjects_3() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_TraceBehindObjects_3)); }
	inline bool get_k_TraceBehindObjects_3() const { return ___k_TraceBehindObjects_3; }
	inline bool* get_address_of_k_TraceBehindObjects_3() { return &___k_TraceBehindObjects_3; }
	inline void set_k_TraceBehindObjects_3(bool value)
	{
		___k_TraceBehindObjects_3 = value;
	}

	inline static int32_t get_offset_of_k_TreatBackfaceHitAsMiss_4() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_TreatBackfaceHitAsMiss_4)); }
	inline bool get_k_TreatBackfaceHitAsMiss_4() const { return ___k_TreatBackfaceHitAsMiss_4; }
	inline bool* get_address_of_k_TreatBackfaceHitAsMiss_4() { return &___k_TreatBackfaceHitAsMiss_4; }
	inline void set_k_TreatBackfaceHitAsMiss_4(bool value)
	{
		___k_TreatBackfaceHitAsMiss_4 = value;
	}

	inline static int32_t get_offset_of_k_BilateralUpsample_5() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_BilateralUpsample_5)); }
	inline bool get_k_BilateralUpsample_5() const { return ___k_BilateralUpsample_5; }
	inline bool* get_address_of_k_BilateralUpsample_5() { return &___k_BilateralUpsample_5; }
	inline void set_k_BilateralUpsample_5(bool value)
	{
		___k_BilateralUpsample_5 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionTextures_6() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___m_ReflectionTextures_6)); }
	inline Int32U5BU5D_t385246372* get_m_ReflectionTextures_6() const { return ___m_ReflectionTextures_6; }
	inline Int32U5BU5D_t385246372** get_address_of_m_ReflectionTextures_6() { return &___m_ReflectionTextures_6; }
	inline void set_m_ReflectionTextures_6(Int32U5BU5D_t385246372* value)
	{
		___m_ReflectionTextures_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTextures_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#ifndef FXAASETTINGS_T1280675075_H
#define FXAASETTINGS_T1280675075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings
struct  FxaaSettings_t1280675075 
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings::preset
	int32_t ___preset_0;

public:
	inline static int32_t get_offset_of_preset_0() { return static_cast<int32_t>(offsetof(FxaaSettings_t1280675075, ___preset_0)); }
	inline int32_t get_preset_0() const { return ___preset_0; }
	inline int32_t* get_address_of_preset_0() { return &___preset_0; }
	inline void set_preset_0(int32_t value)
	{
		___preset_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAASETTINGS_T1280675075_H
#ifndef FOGCOMPONENT_T3400726830_H
#define FOGCOMPONENT_T3400726830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogComponent
struct  FogComponent_t3400726830  : public PostProcessingComponentCommandBuffer_1_t601023342
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGCOMPONENT_T3400726830_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef DITHERINGCOMPONENT_T277621267_H
#define DITHERINGCOMPONENT_T277621267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringComponent
struct  DitheringComponent_t277621267  : public PostProcessingComponentRenderTexture_1_t3997290437
{
public:
	// UnityEngine.Texture2D[] UnityEngine.PostProcessing.DitheringComponent::noiseTextures
	Texture2DU5BU5D_t149664596* ___noiseTextures_2;
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent::textureIndex
	int32_t ___textureIndex_3;

public:
	inline static int32_t get_offset_of_noiseTextures_2() { return static_cast<int32_t>(offsetof(DitheringComponent_t277621267, ___noiseTextures_2)); }
	inline Texture2DU5BU5D_t149664596* get_noiseTextures_2() const { return ___noiseTextures_2; }
	inline Texture2DU5BU5D_t149664596** get_address_of_noiseTextures_2() { return &___noiseTextures_2; }
	inline void set_noiseTextures_2(Texture2DU5BU5D_t149664596* value)
	{
		___noiseTextures_2 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTextures_2), value);
	}

	inline static int32_t get_offset_of_textureIndex_3() { return static_cast<int32_t>(offsetof(DitheringComponent_t277621267, ___textureIndex_3)); }
	inline int32_t get_textureIndex_3() const { return ___textureIndex_3; }
	inline int32_t* get_address_of_textureIndex_3() { return &___textureIndex_3; }
	inline void set_textureIndex_3(int32_t value)
	{
		___textureIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERINGCOMPONENT_T277621267_H
#ifndef DEPTHOFFIELDCOMPONENT_T554756766_H
#define DEPTHOFFIELDCOMPONENT_T554756766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldComponent
struct  DepthOfFieldComponent_t554756766  : public PostProcessingComponentRenderTexture_1_t2082352371
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.DepthOfFieldComponent::m_CoCHistory
	RenderTexture_t2108887433 * ___m_CoCHistory_3;
	// UnityEngine.RenderBuffer[] UnityEngine.PostProcessing.DepthOfFieldComponent::m_MRT
	RenderBufferU5BU5D_t1615831949* ___m_MRT_4;

public:
	inline static int32_t get_offset_of_m_CoCHistory_3() { return static_cast<int32_t>(offsetof(DepthOfFieldComponent_t554756766, ___m_CoCHistory_3)); }
	inline RenderTexture_t2108887433 * get_m_CoCHistory_3() const { return ___m_CoCHistory_3; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CoCHistory_3() { return &___m_CoCHistory_3; }
	inline void set_m_CoCHistory_3(RenderTexture_t2108887433 * value)
	{
		___m_CoCHistory_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoCHistory_3), value);
	}

	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(DepthOfFieldComponent_t554756766, ___m_MRT_4)); }
	inline RenderBufferU5BU5D_t1615831949* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderBufferU5BU5D_t1615831949** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderBufferU5BU5D_t1615831949* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDCOMPONENT_T554756766_H
#ifndef EYEADAPTATIONCOMPONENT_T3394805121_H
#define EYEADAPTATIONCOMPONENT_T3394805121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationComponent
struct  EyeAdaptationComponent_t3394805121  : public PostProcessingComponentRenderTexture_1_t1811108953
{
public:
	// UnityEngine.ComputeShader UnityEngine.PostProcessing.EyeAdaptationComponent::m_EyeCompute
	ComputeShader_t317220254 * ___m_EyeCompute_2;
	// UnityEngine.ComputeBuffer UnityEngine.PostProcessing.EyeAdaptationComponent::m_HistogramBuffer
	ComputeBuffer_t1033194329 * ___m_HistogramBuffer_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.EyeAdaptationComponent::m_AutoExposurePool
	RenderTextureU5BU5D_t4111643188* ___m_AutoExposurePool_4;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent::m_AutoExposurePingPing
	int32_t ___m_AutoExposurePingPing_5;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.EyeAdaptationComponent::m_CurrentAutoExposure
	RenderTexture_t2108887433 * ___m_CurrentAutoExposure_6;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.EyeAdaptationComponent::m_DebugHistogram
	RenderTexture_t2108887433 * ___m_DebugHistogram_7;
	// System.Boolean UnityEngine.PostProcessing.EyeAdaptationComponent::m_FirstFrame
	bool ___m_FirstFrame_9;

public:
	inline static int32_t get_offset_of_m_EyeCompute_2() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_EyeCompute_2)); }
	inline ComputeShader_t317220254 * get_m_EyeCompute_2() const { return ___m_EyeCompute_2; }
	inline ComputeShader_t317220254 ** get_address_of_m_EyeCompute_2() { return &___m_EyeCompute_2; }
	inline void set_m_EyeCompute_2(ComputeShader_t317220254 * value)
	{
		___m_EyeCompute_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EyeCompute_2), value);
	}

	inline static int32_t get_offset_of_m_HistogramBuffer_3() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_HistogramBuffer_3)); }
	inline ComputeBuffer_t1033194329 * get_m_HistogramBuffer_3() const { return ___m_HistogramBuffer_3; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_HistogramBuffer_3() { return &___m_HistogramBuffer_3; }
	inline void set_m_HistogramBuffer_3(ComputeBuffer_t1033194329 * value)
	{
		___m_HistogramBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistogramBuffer_3), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePool_4() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_AutoExposurePool_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_AutoExposurePool_4() const { return ___m_AutoExposurePool_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_AutoExposurePool_4() { return &___m_AutoExposurePool_4; }
	inline void set_m_AutoExposurePool_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_AutoExposurePool_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AutoExposurePool_4), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePingPing_5() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_AutoExposurePingPing_5)); }
	inline int32_t get_m_AutoExposurePingPing_5() const { return ___m_AutoExposurePingPing_5; }
	inline int32_t* get_address_of_m_AutoExposurePingPing_5() { return &___m_AutoExposurePingPing_5; }
	inline void set_m_AutoExposurePingPing_5(int32_t value)
	{
		___m_AutoExposurePingPing_5 = value;
	}

	inline static int32_t get_offset_of_m_CurrentAutoExposure_6() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_CurrentAutoExposure_6)); }
	inline RenderTexture_t2108887433 * get_m_CurrentAutoExposure_6() const { return ___m_CurrentAutoExposure_6; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CurrentAutoExposure_6() { return &___m_CurrentAutoExposure_6; }
	inline void set_m_CurrentAutoExposure_6(RenderTexture_t2108887433 * value)
	{
		___m_CurrentAutoExposure_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentAutoExposure_6), value);
	}

	inline static int32_t get_offset_of_m_DebugHistogram_7() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_DebugHistogram_7)); }
	inline RenderTexture_t2108887433 * get_m_DebugHistogram_7() const { return ___m_DebugHistogram_7; }
	inline RenderTexture_t2108887433 ** get_address_of_m_DebugHistogram_7() { return &___m_DebugHistogram_7; }
	inline void set_m_DebugHistogram_7(RenderTexture_t2108887433 * value)
	{
		___m_DebugHistogram_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugHistogram_7), value);
	}

	inline static int32_t get_offset_of_m_FirstFrame_9() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_FirstFrame_9)); }
	inline bool get_m_FirstFrame_9() const { return ___m_FirstFrame_9; }
	inline bool* get_address_of_m_FirstFrame_9() { return &___m_FirstFrame_9; }
	inline void set_m_FirstFrame_9(bool value)
	{
		___m_FirstFrame_9 = value;
	}
};

struct EyeAdaptationComponent_t3394805121_StaticFields
{
public:
	// System.UInt32[] UnityEngine.PostProcessing.EyeAdaptationComponent::s_EmptyHistogramBuffer
	UInt32U5BU5D_t2770800703* ___s_EmptyHistogramBuffer_8;

public:
	inline static int32_t get_offset_of_s_EmptyHistogramBuffer_8() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121_StaticFields, ___s_EmptyHistogramBuffer_8)); }
	inline UInt32U5BU5D_t2770800703* get_s_EmptyHistogramBuffer_8() const { return ___s_EmptyHistogramBuffer_8; }
	inline UInt32U5BU5D_t2770800703** get_address_of_s_EmptyHistogramBuffer_8() { return &___s_EmptyHistogramBuffer_8; }
	inline void set_s_EmptyHistogramBuffer_8(UInt32U5BU5D_t2770800703* value)
	{
		___s_EmptyHistogramBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyHistogramBuffer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONCOMPONENT_T3394805121_H
#ifndef GRAINCOMPONENT_T866324317_H
#define GRAINCOMPONENT_T866324317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainComponent
struct  GrainComponent_t866324317  : public PostProcessingComponentRenderTexture_1_t2721167529
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.GrainComponent::m_GrainLookupRT
	RenderTexture_t2108887433 * ___m_GrainLookupRT_2;

public:
	inline static int32_t get_offset_of_m_GrainLookupRT_2() { return static_cast<int32_t>(offsetof(GrainComponent_t866324317, ___m_GrainLookupRT_2)); }
	inline RenderTexture_t2108887433 * get_m_GrainLookupRT_2() const { return ___m_GrainLookupRT_2; }
	inline RenderTexture_t2108887433 ** get_address_of_m_GrainLookupRT_2() { return &___m_GrainLookupRT_2; }
	inline void set_m_GrainLookupRT_2(RenderTexture_t2108887433 * value)
	{
		___m_GrainLookupRT_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GrainLookupRT_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINCOMPONENT_T866324317_H
#ifndef COLORGRADINGCOMPONENT_T1715259467_H
#define COLORGRADINGCOMPONENT_T1715259467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingComponent
struct  ColorGradingComponent_t1715259467  : public PostProcessingComponentRenderTexture_1_t3016333222
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ColorGradingComponent::m_GradingCurves
	Texture2D_t3840446185 * ___m_GradingCurves_5;

public:
	inline static int32_t get_offset_of_m_GradingCurves_5() { return static_cast<int32_t>(offsetof(ColorGradingComponent_t1715259467, ___m_GradingCurves_5)); }
	inline Texture2D_t3840446185 * get_m_GradingCurves_5() const { return ___m_GradingCurves_5; }
	inline Texture2D_t3840446185 ** get_address_of_m_GradingCurves_5() { return &___m_GradingCurves_5; }
	inline void set_m_GradingCurves_5(Texture2D_t3840446185 * value)
	{
		___m_GradingCurves_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradingCurves_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGCOMPONENT_T1715259467_H
#ifndef FXAACOMPONENT_T1312385771_H
#define FXAACOMPONENT_T1312385771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FxaaComponent
struct  FxaaComponent_t1312385771  : public PostProcessingComponentRenderTexture_1_t3089424429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAACOMPONENT_T1312385771_H
#ifndef BUTTONDOWNMETHODDELEGATE_T3308383149_H
#define BUTTONDOWNMETHODDELEGATE_T3308383149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMInput/IMButton/ButtonDownMethodDelegate
struct  ButtonDownMethodDelegate_t3308383149  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONDOWNMETHODDELEGATE_T3308383149_H
#ifndef BUTTONPRESSEDMETHODDELEGATE_T2485678181_H
#define BUTTONPRESSEDMETHODDELEGATE_T2485678181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMInput/IMButton/ButtonPressedMethodDelegate
struct  ButtonPressedMethodDelegate_t2485678181  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONPRESSEDMETHODDELEGATE_T2485678181_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef BUTTONUPMETHODDELEGATE_T939453058_H
#define BUTTONUPMETHODDELEGATE_T939453058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMInput/IMButton/ButtonUpMethodDelegate
struct  ButtonUpMethodDelegate_t939453058  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONUPMETHODDELEGATE_T939453058_H
#ifndef EVENTS_T2152452284_H
#define EVENTS_T2152452284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PoolableObject/Events
struct  Events_t2152452284  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTS_T2152452284_H
#ifndef SETTINGS_T4292431647_H
#define SETTINGS_T4292431647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/Settings
struct  Settings_t4292431647 
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/Method UnityEngine.PostProcessing.AntialiasingModel/Settings::method
	int32_t ___method_0;
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings UnityEngine.PostProcessing.AntialiasingModel/Settings::fxaaSettings
	FxaaSettings_t1280675075  ___fxaaSettings_1;
	// UnityEngine.PostProcessing.AntialiasingModel/TaaSettings UnityEngine.PostProcessing.AntialiasingModel/Settings::taaSettings
	TaaSettings_t2709374970  ___taaSettings_2;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___method_0)); }
	inline int32_t get_method_0() const { return ___method_0; }
	inline int32_t* get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(int32_t value)
	{
		___method_0 = value;
	}

	inline static int32_t get_offset_of_fxaaSettings_1() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___fxaaSettings_1)); }
	inline FxaaSettings_t1280675075  get_fxaaSettings_1() const { return ___fxaaSettings_1; }
	inline FxaaSettings_t1280675075 * get_address_of_fxaaSettings_1() { return &___fxaaSettings_1; }
	inline void set_fxaaSettings_1(FxaaSettings_t1280675075  value)
	{
		___fxaaSettings_1 = value;
	}

	inline static int32_t get_offset_of_taaSettings_2() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___taaSettings_2)); }
	inline TaaSettings_t2709374970  get_taaSettings_2() const { return ___taaSettings_2; }
	inline TaaSettings_t2709374970 * get_address_of_taaSettings_2() { return &___taaSettings_2; }
	inline void set_taaSettings_2(TaaSettings_t2709374970  value)
	{
		___taaSettings_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T4292431647_H
#ifndef AMBIENTOCCLUSIONMODEL_T389471066_H
#define AMBIENTOCCLUSIONMODEL_T389471066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel
struct  AmbientOcclusionModel_t389471066  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.AmbientOcclusionModel/Settings UnityEngine.PostProcessing.AmbientOcclusionModel::m_Settings
	Settings_t3016786575  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(AmbientOcclusionModel_t389471066, ___m_Settings_1)); }
	inline Settings_t3016786575  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t3016786575 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t3016786575  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONMODEL_T389471066_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ANTIALIASINGMODEL_T1521139388_H
#define ANTIALIASINGMODEL_T1521139388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel
struct  AntialiasingModel_t1521139388  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/Settings UnityEngine.PostProcessing.AntialiasingModel::m_Settings
	Settings_t4292431647  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(AntialiasingModel_t1521139388, ___m_Settings_1)); }
	inline Settings_t4292431647  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4292431647 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4292431647  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIALIASINGMODEL_T1521139388_H
#ifndef MMIMAGE_T1525614539_H
#define MMIMAGE_T1525614539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMImage
struct  MMImage_t1525614539  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMIMAGE_T1525614539_H
#ifndef MMINPUT_T2244168049_H
#define MMINPUT_T2244168049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMInput
struct  MMInput_t2244168049  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMINPUT_T2244168049_H
#ifndef MMCONSOLE_T3000489708_H
#define MMCONSOLE_T3000489708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMConsole
struct  MMConsole_t3000489708  : public MonoBehaviour_t3962482529
{
public:
	// System.String MoreMountains.Tools.MMConsole::_messageStack
	String_t* ____messageStack_2;
	// System.Int32 MoreMountains.Tools.MMConsole::_numberOfMessages
	int32_t ____numberOfMessages_3;
	// System.Boolean MoreMountains.Tools.MMConsole::_messageStackHasBeenDisplayed
	bool ____messageStackHasBeenDisplayed_4;
	// System.Int32 MoreMountains.Tools.MMConsole::_largestMessageLength
	int32_t ____largestMessageLength_5;
	// System.Int32 MoreMountains.Tools.MMConsole::_marginTop
	int32_t ____marginTop_6;
	// System.Int32 MoreMountains.Tools.MMConsole::_marginLeft
	int32_t ____marginLeft_7;
	// System.Int32 MoreMountains.Tools.MMConsole::_padding
	int32_t ____padding_8;
	// System.Int32 MoreMountains.Tools.MMConsole::_fontSize
	int32_t ____fontSize_9;
	// System.Int32 MoreMountains.Tools.MMConsole::_characterHeight
	int32_t ____characterHeight_10;
	// System.Int32 MoreMountains.Tools.MMConsole::_characterWidth
	int32_t ____characterWidth_11;

public:
	inline static int32_t get_offset_of__messageStack_2() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____messageStack_2)); }
	inline String_t* get__messageStack_2() const { return ____messageStack_2; }
	inline String_t** get_address_of__messageStack_2() { return &____messageStack_2; }
	inline void set__messageStack_2(String_t* value)
	{
		____messageStack_2 = value;
		Il2CppCodeGenWriteBarrier((&____messageStack_2), value);
	}

	inline static int32_t get_offset_of__numberOfMessages_3() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____numberOfMessages_3)); }
	inline int32_t get__numberOfMessages_3() const { return ____numberOfMessages_3; }
	inline int32_t* get_address_of__numberOfMessages_3() { return &____numberOfMessages_3; }
	inline void set__numberOfMessages_3(int32_t value)
	{
		____numberOfMessages_3 = value;
	}

	inline static int32_t get_offset_of__messageStackHasBeenDisplayed_4() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____messageStackHasBeenDisplayed_4)); }
	inline bool get__messageStackHasBeenDisplayed_4() const { return ____messageStackHasBeenDisplayed_4; }
	inline bool* get_address_of__messageStackHasBeenDisplayed_4() { return &____messageStackHasBeenDisplayed_4; }
	inline void set__messageStackHasBeenDisplayed_4(bool value)
	{
		____messageStackHasBeenDisplayed_4 = value;
	}

	inline static int32_t get_offset_of__largestMessageLength_5() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____largestMessageLength_5)); }
	inline int32_t get__largestMessageLength_5() const { return ____largestMessageLength_5; }
	inline int32_t* get_address_of__largestMessageLength_5() { return &____largestMessageLength_5; }
	inline void set__largestMessageLength_5(int32_t value)
	{
		____largestMessageLength_5 = value;
	}

	inline static int32_t get_offset_of__marginTop_6() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____marginTop_6)); }
	inline int32_t get__marginTop_6() const { return ____marginTop_6; }
	inline int32_t* get_address_of__marginTop_6() { return &____marginTop_6; }
	inline void set__marginTop_6(int32_t value)
	{
		____marginTop_6 = value;
	}

	inline static int32_t get_offset_of__marginLeft_7() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____marginLeft_7)); }
	inline int32_t get__marginLeft_7() const { return ____marginLeft_7; }
	inline int32_t* get_address_of__marginLeft_7() { return &____marginLeft_7; }
	inline void set__marginLeft_7(int32_t value)
	{
		____marginLeft_7 = value;
	}

	inline static int32_t get_offset_of__padding_8() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____padding_8)); }
	inline int32_t get__padding_8() const { return ____padding_8; }
	inline int32_t* get_address_of__padding_8() { return &____padding_8; }
	inline void set__padding_8(int32_t value)
	{
		____padding_8 = value;
	}

	inline static int32_t get_offset_of__fontSize_9() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____fontSize_9)); }
	inline int32_t get__fontSize_9() const { return ____fontSize_9; }
	inline int32_t* get_address_of__fontSize_9() { return &____fontSize_9; }
	inline void set__fontSize_9(int32_t value)
	{
		____fontSize_9 = value;
	}

	inline static int32_t get_offset_of__characterHeight_10() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____characterHeight_10)); }
	inline int32_t get__characterHeight_10() const { return ____characterHeight_10; }
	inline int32_t* get_address_of__characterHeight_10() { return &____characterHeight_10; }
	inline void set__characterHeight_10(int32_t value)
	{
		____characterHeight_10 = value;
	}

	inline static int32_t get_offset_of__characterWidth_11() { return static_cast<int32_t>(offsetof(MMConsole_t3000489708, ____characterWidth_11)); }
	inline int32_t get__characterWidth_11() const { return ____characterWidth_11; }
	inline int32_t* get_address_of__characterWidth_11() { return &____characterWidth_11; }
	inline void set__characterWidth_11(int32_t value)
	{
		____characterWidth_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMCONSOLE_T3000489708_H
#ifndef AUTODESTROYPARTICLESYSTEM_T988498569_H
#define AUTODESTROYPARTICLESYSTEM_T988498569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.AutoDestroyParticleSystem
struct  AutoDestroyParticleSystem_t988498569  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MoreMountains.Tools.AutoDestroyParticleSystem::DestroyParent
	bool ___DestroyParent_2;
	// System.Single MoreMountains.Tools.AutoDestroyParticleSystem::DestroyDelay
	float ___DestroyDelay_3;
	// UnityEngine.ParticleSystem MoreMountains.Tools.AutoDestroyParticleSystem::_particleSystem
	ParticleSystem_t1800779281 * ____particleSystem_4;
	// System.Single MoreMountains.Tools.AutoDestroyParticleSystem::_startTime
	float ____startTime_5;

public:
	inline static int32_t get_offset_of_DestroyParent_2() { return static_cast<int32_t>(offsetof(AutoDestroyParticleSystem_t988498569, ___DestroyParent_2)); }
	inline bool get_DestroyParent_2() const { return ___DestroyParent_2; }
	inline bool* get_address_of_DestroyParent_2() { return &___DestroyParent_2; }
	inline void set_DestroyParent_2(bool value)
	{
		___DestroyParent_2 = value;
	}

	inline static int32_t get_offset_of_DestroyDelay_3() { return static_cast<int32_t>(offsetof(AutoDestroyParticleSystem_t988498569, ___DestroyDelay_3)); }
	inline float get_DestroyDelay_3() const { return ___DestroyDelay_3; }
	inline float* get_address_of_DestroyDelay_3() { return &___DestroyDelay_3; }
	inline void set_DestroyDelay_3(float value)
	{
		___DestroyDelay_3 = value;
	}

	inline static int32_t get_offset_of__particleSystem_4() { return static_cast<int32_t>(offsetof(AutoDestroyParticleSystem_t988498569, ____particleSystem_4)); }
	inline ParticleSystem_t1800779281 * get__particleSystem_4() const { return ____particleSystem_4; }
	inline ParticleSystem_t1800779281 ** get_address_of__particleSystem_4() { return &____particleSystem_4; }
	inline void set__particleSystem_4(ParticleSystem_t1800779281 * value)
	{
		____particleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&____particleSystem_4), value);
	}

	inline static int32_t get_offset_of__startTime_5() { return static_cast<int32_t>(offsetof(AutoDestroyParticleSystem_t988498569, ____startTime_5)); }
	inline float get__startTime_5() const { return ____startTime_5; }
	inline float* get_address_of__startTime_5() { return &____startTime_5; }
	inline void set__startTime_5(float value)
	{
		____startTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTODESTROYPARTICLESYSTEM_T988498569_H
#ifndef RIGIDBODYINTERFACE_T2126323197_H
#define RIGIDBODYINTERFACE_T2126323197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.RigidbodyInterface
struct  RigidbodyInterface_t2126323197  : public MonoBehaviour_t3962482529
{
public:
	// System.String MoreMountains.Tools.RigidbodyInterface::_mode
	String_t* ____mode_2;
	// UnityEngine.Rigidbody2D MoreMountains.Tools.RigidbodyInterface::_rigidbody2D
	Rigidbody2D_t939494601 * ____rigidbody2D_3;
	// UnityEngine.Rigidbody MoreMountains.Tools.RigidbodyInterface::_rigidbody
	Rigidbody_t3916780224 * ____rigidbody_4;
	// UnityEngine.Collider2D MoreMountains.Tools.RigidbodyInterface::_collider2D
	Collider2D_t2806799626 * ____collider2D_5;
	// UnityEngine.Collider MoreMountains.Tools.RigidbodyInterface::_collider
	Collider_t1773347010 * ____collider_6;
	// UnityEngine.Bounds MoreMountains.Tools.RigidbodyInterface::_colliderBounds
	Bounds_t2266837910  ____colliderBounds_7;

public:
	inline static int32_t get_offset_of__mode_2() { return static_cast<int32_t>(offsetof(RigidbodyInterface_t2126323197, ____mode_2)); }
	inline String_t* get__mode_2() const { return ____mode_2; }
	inline String_t** get_address_of__mode_2() { return &____mode_2; }
	inline void set__mode_2(String_t* value)
	{
		____mode_2 = value;
		Il2CppCodeGenWriteBarrier((&____mode_2), value);
	}

	inline static int32_t get_offset_of__rigidbody2D_3() { return static_cast<int32_t>(offsetof(RigidbodyInterface_t2126323197, ____rigidbody2D_3)); }
	inline Rigidbody2D_t939494601 * get__rigidbody2D_3() const { return ____rigidbody2D_3; }
	inline Rigidbody2D_t939494601 ** get_address_of__rigidbody2D_3() { return &____rigidbody2D_3; }
	inline void set__rigidbody2D_3(Rigidbody2D_t939494601 * value)
	{
		____rigidbody2D_3 = value;
		Il2CppCodeGenWriteBarrier((&____rigidbody2D_3), value);
	}

	inline static int32_t get_offset_of__rigidbody_4() { return static_cast<int32_t>(offsetof(RigidbodyInterface_t2126323197, ____rigidbody_4)); }
	inline Rigidbody_t3916780224 * get__rigidbody_4() const { return ____rigidbody_4; }
	inline Rigidbody_t3916780224 ** get_address_of__rigidbody_4() { return &____rigidbody_4; }
	inline void set__rigidbody_4(Rigidbody_t3916780224 * value)
	{
		____rigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&____rigidbody_4), value);
	}

	inline static int32_t get_offset_of__collider2D_5() { return static_cast<int32_t>(offsetof(RigidbodyInterface_t2126323197, ____collider2D_5)); }
	inline Collider2D_t2806799626 * get__collider2D_5() const { return ____collider2D_5; }
	inline Collider2D_t2806799626 ** get_address_of__collider2D_5() { return &____collider2D_5; }
	inline void set__collider2D_5(Collider2D_t2806799626 * value)
	{
		____collider2D_5 = value;
		Il2CppCodeGenWriteBarrier((&____collider2D_5), value);
	}

	inline static int32_t get_offset_of__collider_6() { return static_cast<int32_t>(offsetof(RigidbodyInterface_t2126323197, ____collider_6)); }
	inline Collider_t1773347010 * get__collider_6() const { return ____collider_6; }
	inline Collider_t1773347010 ** get_address_of__collider_6() { return &____collider_6; }
	inline void set__collider_6(Collider_t1773347010 * value)
	{
		____collider_6 = value;
		Il2CppCodeGenWriteBarrier((&____collider_6), value);
	}

	inline static int32_t get_offset_of__colliderBounds_7() { return static_cast<int32_t>(offsetof(RigidbodyInterface_t2126323197, ____colliderBounds_7)); }
	inline Bounds_t2266837910  get__colliderBounds_7() const { return ____colliderBounds_7; }
	inline Bounds_t2266837910 * get_address_of__colliderBounds_7() { return &____colliderBounds_7; }
	inline void set__colliderBounds_7(Bounds_t2266837910  value)
	{
		____colliderBounds_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYINTERFACE_T2126323197_H
#ifndef CHANGEFOGCOLOR_T270202886_H
#define CHANGEFOGCOLOR_T270202886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.ChangeFogColor
struct  ChangeFogColor_t270202886  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color MoreMountains.Tools.ChangeFogColor::FogColor
	Color_t2555686324  ___FogColor_2;

public:
	inline static int32_t get_offset_of_FogColor_2() { return static_cast<int32_t>(offsetof(ChangeFogColor_t270202886, ___FogColor_2)); }
	inline Color_t2555686324  get_FogColor_2() const { return ___FogColor_2; }
	inline Color_t2555686324 * get_address_of_FogColor_2() { return &___FogColor_2; }
	inline void set_FogColor_2(Color_t2555686324  value)
	{
		___FogColor_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGEFOGCOLOR_T270202886_H
#ifndef VISIBLEPARTICLE_T1758110514_H
#define VISIBLEPARTICLE_T1758110514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.VisibleParticle
struct  VisibleParticle_t1758110514  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBLEPARTICLE_T1758110514_H
#ifndef MMTRAILRENDERERSORTINGLAYER_T1109254816_H
#define MMTRAILRENDERERSORTINGLAYER_T1109254816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTrailRendererSortingLayer
struct  MMTrailRendererSortingLayer_t1109254816  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMTRAILRENDERERSORTINGLAYER_T1109254816_H
#ifndef MMTIME_T1029019214_H
#define MMTIME_T1029019214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMTime
struct  MMTime_t1029019214  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMTIME_T1029019214_H
#ifndef OBJECTBOUNDS_T107927819_H
#define OBJECTBOUNDS_T107927819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.ObjectBounds
struct  ObjectBounds_t107927819  : public MonoBehaviour_t3962482529
{
public:
	// MoreMountains.Tools.ObjectBounds/WaysToDetermineBounds MoreMountains.Tools.ObjectBounds::BoundsBasedOn
	int32_t ___BoundsBasedOn_2;
	// UnityEngine.Vector3 MoreMountains.Tools.ObjectBounds::<Size>k__BackingField
	Vector3_t3722313464  ___U3CSizeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_BoundsBasedOn_2() { return static_cast<int32_t>(offsetof(ObjectBounds_t107927819, ___BoundsBasedOn_2)); }
	inline int32_t get_BoundsBasedOn_2() const { return ___BoundsBasedOn_2; }
	inline int32_t* get_address_of_BoundsBasedOn_2() { return &___BoundsBasedOn_2; }
	inline void set_BoundsBasedOn_2(int32_t value)
	{
		___BoundsBasedOn_2 = value;
	}

	inline static int32_t get_offset_of_U3CSizeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectBounds_t107927819, ___U3CSizeU3Ek__BackingField_3)); }
	inline Vector3_t3722313464  get_U3CSizeU3Ek__BackingField_3() const { return ___U3CSizeU3Ek__BackingField_3; }
	inline Vector3_t3722313464 * get_address_of_U3CSizeU3Ek__BackingField_3() { return &___U3CSizeU3Ek__BackingField_3; }
	inline void set_U3CSizeU3Ek__BackingField_3(Vector3_t3722313464  value)
	{
		___U3CSizeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTBOUNDS_T107927819_H
#ifndef MMBOUNDS_T2287521663_H
#define MMBOUNDS_T2287521663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MMBounds
struct  MMBounds_t2287521663  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMBOUNDS_T2287521663_H
#ifndef OBJECTPOOLER_T2790389498_H
#define OBJECTPOOLER_T2790389498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.ObjectPooler
struct  ObjectPooler_t2790389498  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean MoreMountains.Tools.ObjectPooler::MutualizeWaitingPools
	bool ___MutualizeWaitingPools_3;
	// System.Boolean MoreMountains.Tools.ObjectPooler::NestWaitingPool
	bool ___NestWaitingPool_4;
	// UnityEngine.GameObject MoreMountains.Tools.ObjectPooler::_waitingPool
	GameObject_t1113636619 * ____waitingPool_5;

public:
	inline static int32_t get_offset_of_MutualizeWaitingPools_3() { return static_cast<int32_t>(offsetof(ObjectPooler_t2790389498, ___MutualizeWaitingPools_3)); }
	inline bool get_MutualizeWaitingPools_3() const { return ___MutualizeWaitingPools_3; }
	inline bool* get_address_of_MutualizeWaitingPools_3() { return &___MutualizeWaitingPools_3; }
	inline void set_MutualizeWaitingPools_3(bool value)
	{
		___MutualizeWaitingPools_3 = value;
	}

	inline static int32_t get_offset_of_NestWaitingPool_4() { return static_cast<int32_t>(offsetof(ObjectPooler_t2790389498, ___NestWaitingPool_4)); }
	inline bool get_NestWaitingPool_4() const { return ___NestWaitingPool_4; }
	inline bool* get_address_of_NestWaitingPool_4() { return &___NestWaitingPool_4; }
	inline void set_NestWaitingPool_4(bool value)
	{
		___NestWaitingPool_4 = value;
	}

	inline static int32_t get_offset_of__waitingPool_5() { return static_cast<int32_t>(offsetof(ObjectPooler_t2790389498, ____waitingPool_5)); }
	inline GameObject_t1113636619 * get__waitingPool_5() const { return ____waitingPool_5; }
	inline GameObject_t1113636619 ** get_address_of__waitingPool_5() { return &____waitingPool_5; }
	inline void set__waitingPool_5(GameObject_t1113636619 * value)
	{
		____waitingPool_5 = value;
		Il2CppCodeGenWriteBarrier((&____waitingPool_5), value);
	}
};

struct ObjectPooler_t2790389498_StaticFields
{
public:
	// MoreMountains.Tools.ObjectPooler MoreMountains.Tools.ObjectPooler::Instance
	ObjectPooler_t2790389498 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(ObjectPooler_t2790389498_StaticFields, ___Instance_2)); }
	inline ObjectPooler_t2790389498 * get_Instance_2() const { return ___Instance_2; }
	inline ObjectPooler_t2790389498 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(ObjectPooler_t2790389498 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOLER_T2790389498_H
#ifndef POOLABLEOBJECT_T3099519598_H
#define POOLABLEOBJECT_T3099519598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.PoolableObject
struct  PoolableObject_t3099519598  : public ObjectBounds_t107927819
{
public:
	// MoreMountains.Tools.PoolableObject/Events MoreMountains.Tools.PoolableObject::OnSpawnComplete
	Events_t2152452284 * ___OnSpawnComplete_4;
	// System.Single MoreMountains.Tools.PoolableObject::LifeTime
	float ___LifeTime_5;

public:
	inline static int32_t get_offset_of_OnSpawnComplete_4() { return static_cast<int32_t>(offsetof(PoolableObject_t3099519598, ___OnSpawnComplete_4)); }
	inline Events_t2152452284 * get_OnSpawnComplete_4() const { return ___OnSpawnComplete_4; }
	inline Events_t2152452284 ** get_address_of_OnSpawnComplete_4() { return &___OnSpawnComplete_4; }
	inline void set_OnSpawnComplete_4(Events_t2152452284 * value)
	{
		___OnSpawnComplete_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnSpawnComplete_4), value);
	}

	inline static int32_t get_offset_of_LifeTime_5() { return static_cast<int32_t>(offsetof(PoolableObject_t3099519598, ___LifeTime_5)); }
	inline float get_LifeTime_5() const { return ___LifeTime_5; }
	inline float* get_address_of_LifeTime_5() { return &___LifeTime_5; }
	inline void set_LifeTime_5(float value)
	{
		___LifeTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLABLEOBJECT_T3099519598_H
#ifndef MULTIPLEOBJECTPOOLER_T1548776533_H
#define MULTIPLEOBJECTPOOLER_T1548776533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.MultipleObjectPooler
struct  MultipleObjectPooler_t1548776533  : public ObjectPooler_t2790389498
{
public:
	// System.Collections.Generic.List`1<MoreMountains.Tools.MultipleObjectPoolerObject> MoreMountains.Tools.MultipleObjectPooler::Pool
	List_1_t3539521104 * ___Pool_6;
	// MoreMountains.Tools.PoolingMethods MoreMountains.Tools.MultipleObjectPooler::PoolingMethod
	int32_t ___PoolingMethod_7;
	// System.Boolean MoreMountains.Tools.MultipleObjectPooler::CanPoolSameObjectTwice
	bool ___CanPoolSameObjectTwice_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.Tools.MultipleObjectPooler::_pooledGameObjects
	List_1_t2585711361 * ____pooledGameObjects_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.Tools.MultipleObjectPooler::_pooledGameObjectsOriginalOrder
	List_1_t2585711361 * ____pooledGameObjectsOriginalOrder_10;
	// System.Collections.Generic.List`1<MoreMountains.Tools.MultipleObjectPoolerObject> MoreMountains.Tools.MultipleObjectPooler::_randomizedPool
	List_1_t3539521104 * ____randomizedPool_11;
	// System.String MoreMountains.Tools.MultipleObjectPooler::_lastPooledObjectName
	String_t* ____lastPooledObjectName_12;
	// System.Int32 MoreMountains.Tools.MultipleObjectPooler::_currentIndex
	int32_t ____currentIndex_13;

public:
	inline static int32_t get_offset_of_Pool_6() { return static_cast<int32_t>(offsetof(MultipleObjectPooler_t1548776533, ___Pool_6)); }
	inline List_1_t3539521104 * get_Pool_6() const { return ___Pool_6; }
	inline List_1_t3539521104 ** get_address_of_Pool_6() { return &___Pool_6; }
	inline void set_Pool_6(List_1_t3539521104 * value)
	{
		___Pool_6 = value;
		Il2CppCodeGenWriteBarrier((&___Pool_6), value);
	}

	inline static int32_t get_offset_of_PoolingMethod_7() { return static_cast<int32_t>(offsetof(MultipleObjectPooler_t1548776533, ___PoolingMethod_7)); }
	inline int32_t get_PoolingMethod_7() const { return ___PoolingMethod_7; }
	inline int32_t* get_address_of_PoolingMethod_7() { return &___PoolingMethod_7; }
	inline void set_PoolingMethod_7(int32_t value)
	{
		___PoolingMethod_7 = value;
	}

	inline static int32_t get_offset_of_CanPoolSameObjectTwice_8() { return static_cast<int32_t>(offsetof(MultipleObjectPooler_t1548776533, ___CanPoolSameObjectTwice_8)); }
	inline bool get_CanPoolSameObjectTwice_8() const { return ___CanPoolSameObjectTwice_8; }
	inline bool* get_address_of_CanPoolSameObjectTwice_8() { return &___CanPoolSameObjectTwice_8; }
	inline void set_CanPoolSameObjectTwice_8(bool value)
	{
		___CanPoolSameObjectTwice_8 = value;
	}

	inline static int32_t get_offset_of__pooledGameObjects_9() { return static_cast<int32_t>(offsetof(MultipleObjectPooler_t1548776533, ____pooledGameObjects_9)); }
	inline List_1_t2585711361 * get__pooledGameObjects_9() const { return ____pooledGameObjects_9; }
	inline List_1_t2585711361 ** get_address_of__pooledGameObjects_9() { return &____pooledGameObjects_9; }
	inline void set__pooledGameObjects_9(List_1_t2585711361 * value)
	{
		____pooledGameObjects_9 = value;
		Il2CppCodeGenWriteBarrier((&____pooledGameObjects_9), value);
	}

	inline static int32_t get_offset_of__pooledGameObjectsOriginalOrder_10() { return static_cast<int32_t>(offsetof(MultipleObjectPooler_t1548776533, ____pooledGameObjectsOriginalOrder_10)); }
	inline List_1_t2585711361 * get__pooledGameObjectsOriginalOrder_10() const { return ____pooledGameObjectsOriginalOrder_10; }
	inline List_1_t2585711361 ** get_address_of__pooledGameObjectsOriginalOrder_10() { return &____pooledGameObjectsOriginalOrder_10; }
	inline void set__pooledGameObjectsOriginalOrder_10(List_1_t2585711361 * value)
	{
		____pooledGameObjectsOriginalOrder_10 = value;
		Il2CppCodeGenWriteBarrier((&____pooledGameObjectsOriginalOrder_10), value);
	}

	inline static int32_t get_offset_of__randomizedPool_11() { return static_cast<int32_t>(offsetof(MultipleObjectPooler_t1548776533, ____randomizedPool_11)); }
	inline List_1_t3539521104 * get__randomizedPool_11() const { return ____randomizedPool_11; }
	inline List_1_t3539521104 ** get_address_of__randomizedPool_11() { return &____randomizedPool_11; }
	inline void set__randomizedPool_11(List_1_t3539521104 * value)
	{
		____randomizedPool_11 = value;
		Il2CppCodeGenWriteBarrier((&____randomizedPool_11), value);
	}

	inline static int32_t get_offset_of__lastPooledObjectName_12() { return static_cast<int32_t>(offsetof(MultipleObjectPooler_t1548776533, ____lastPooledObjectName_12)); }
	inline String_t* get__lastPooledObjectName_12() const { return ____lastPooledObjectName_12; }
	inline String_t** get_address_of__lastPooledObjectName_12() { return &____lastPooledObjectName_12; }
	inline void set__lastPooledObjectName_12(String_t* value)
	{
		____lastPooledObjectName_12 = value;
		Il2CppCodeGenWriteBarrier((&____lastPooledObjectName_12), value);
	}

	inline static int32_t get_offset_of__currentIndex_13() { return static_cast<int32_t>(offsetof(MultipleObjectPooler_t1548776533, ____currentIndex_13)); }
	inline int32_t get__currentIndex_13() const { return ____currentIndex_13; }
	inline int32_t* get_address_of__currentIndex_13() { return &____currentIndex_13; }
	inline void set__currentIndex_13(int32_t value)
	{
		____currentIndex_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPLEOBJECTPOOLER_T1548776533_H
#ifndef SIMPLEOBJECTPOOLER_T3637768830_H
#define SIMPLEOBJECTPOOLER_T3637768830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.Tools.SimpleObjectPooler
struct  SimpleObjectPooler_t3637768830  : public ObjectPooler_t2790389498
{
public:
	// UnityEngine.GameObject MoreMountains.Tools.SimpleObjectPooler::GameObjectToPool
	GameObject_t1113636619 * ___GameObjectToPool_6;
	// System.Int32 MoreMountains.Tools.SimpleObjectPooler::PoolSize
	int32_t ___PoolSize_7;
	// System.Boolean MoreMountains.Tools.SimpleObjectPooler::PoolCanExpand
	bool ___PoolCanExpand_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.Tools.SimpleObjectPooler::_pooledGameObjects
	List_1_t2585711361 * ____pooledGameObjects_9;

public:
	inline static int32_t get_offset_of_GameObjectToPool_6() { return static_cast<int32_t>(offsetof(SimpleObjectPooler_t3637768830, ___GameObjectToPool_6)); }
	inline GameObject_t1113636619 * get_GameObjectToPool_6() const { return ___GameObjectToPool_6; }
	inline GameObject_t1113636619 ** get_address_of_GameObjectToPool_6() { return &___GameObjectToPool_6; }
	inline void set_GameObjectToPool_6(GameObject_t1113636619 * value)
	{
		___GameObjectToPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameObjectToPool_6), value);
	}

	inline static int32_t get_offset_of_PoolSize_7() { return static_cast<int32_t>(offsetof(SimpleObjectPooler_t3637768830, ___PoolSize_7)); }
	inline int32_t get_PoolSize_7() const { return ___PoolSize_7; }
	inline int32_t* get_address_of_PoolSize_7() { return &___PoolSize_7; }
	inline void set_PoolSize_7(int32_t value)
	{
		___PoolSize_7 = value;
	}

	inline static int32_t get_offset_of_PoolCanExpand_8() { return static_cast<int32_t>(offsetof(SimpleObjectPooler_t3637768830, ___PoolCanExpand_8)); }
	inline bool get_PoolCanExpand_8() const { return ___PoolCanExpand_8; }
	inline bool* get_address_of_PoolCanExpand_8() { return &___PoolCanExpand_8; }
	inline void set_PoolCanExpand_8(bool value)
	{
		___PoolCanExpand_8 = value;
	}

	inline static int32_t get_offset_of__pooledGameObjects_9() { return static_cast<int32_t>(offsetof(SimpleObjectPooler_t3637768830, ____pooledGameObjects_9)); }
	inline List_1_t2585711361 * get__pooledGameObjects_9() const { return ____pooledGameObjects_9; }
	inline List_1_t2585711361 ** get_address_of__pooledGameObjects_9() { return &____pooledGameObjects_9; }
	inline void set__pooledGameObjects_9(List_1_t2585711361 * value)
	{
		____pooledGameObjects_9 = value;
		Il2CppCodeGenWriteBarrier((&____pooledGameObjects_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEOBJECTPOOLER_T3637768830_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (MMAnimator_t3222997085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (MMBounds_t2287521663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (MMConsole_t3000489708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[10] = 
{
	MMConsole_t3000489708::get_offset_of__messageStack_2(),
	MMConsole_t3000489708::get_offset_of__numberOfMessages_3(),
	MMConsole_t3000489708::get_offset_of__messageStackHasBeenDisplayed_4(),
	MMConsole_t3000489708::get_offset_of__largestMessageLength_5(),
	MMConsole_t3000489708::get_offset_of__marginTop_6(),
	MMConsole_t3000489708::get_offset_of__marginLeft_7(),
	MMConsole_t3000489708::get_offset_of__padding_8(),
	MMConsole_t3000489708::get_offset_of__fontSize_9(),
	MMConsole_t3000489708::get_offset_of__characterHeight_10(),
	MMConsole_t3000489708::get_offset_of__characterWidth_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (MMDebug_t3652739535), -1, sizeof(MMDebug_t3652739535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2003[1] = 
{
	MMDebug_t3652739535_StaticFields::get_offset_of__console_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (MMFade_t3008706013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (U3CFadeImageU3Ec__Iterator0_t2472898010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[9] = 
{
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_target_0(),
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_U3CalphaU3E__0_1(),
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_U3CtU3E__1_2(),
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_color_3(),
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_U3CnewColorU3E__2_4(),
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_duration_5(),
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_U24current_6(),
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_U24disposing_7(),
	U3CFadeImageU3Ec__Iterator0_t2472898010::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (U3CFadeTextU3Ec__Iterator1_t1686573558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[9] = 
{
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_target_0(),
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_U3CalphaU3E__0_1(),
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_U3CtU3E__1_2(),
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_color_3(),
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_U3CnewColorU3E__2_4(),
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_duration_5(),
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_U24current_6(),
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_U24disposing_7(),
	U3CFadeTextU3Ec__Iterator1_t1686573558::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (U3CFadeSpriteU3Ec__Iterator2_t3661250087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[10] = 
{
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_target_0(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_U3CalphaU3E__0_1(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_U3CtU3E__0_2(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_color_3(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_U3CnewColorU3E__1_4(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_duration_5(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_U3CfinalColorU3E__0_6(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_U24current_7(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_U24disposing_8(),
	U3CFadeSpriteU3Ec__Iterator2_t3661250087::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CFadeCanvasGroupU3Ec__Iterator3_t365063154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[9] = 
{
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_target_0(),
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_U3CcurrentAlphaU3E__0_1(),
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_U3CtU3E__0_2(),
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_targetAlpha_3(),
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_U3CnewAlphaU3E__1_4(),
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_duration_5(),
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_U24current_6(),
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_U24disposing_7(),
	U3CFadeCanvasGroupU3Ec__Iterator3_t365063154::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (MMGUI_t3437079187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (MMHelpers_t3933556615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (MMImage_t1525614539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U3CFlickerU3Ec__Iterator0_t2968401774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[9] = 
{
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_renderer_0(),
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_initialColor_1(),
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_flickerColor_2(),
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_flickerDuration_3(),
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_U3CflickerStopU3E__0_4(),
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_flickerSpeed_5(),
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_U24current_6(),
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_U24disposing_7(),
	U3CFlickerU3Ec__Iterator0_t2968401774::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (MMInput_t2244168049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (ButtonStates_t4048788823)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[5] = 
{
	ButtonStates_t4048788823::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (IMButton_t3867185334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[5] = 
{
	IMButton_t3867185334::get_offset_of_U3CStateU3Ek__BackingField_0(),
	IMButton_t3867185334::get_offset_of_ButtonID_1(),
	IMButton_t3867185334::get_offset_of_ButtonDownMethod_2(),
	IMButton_t3867185334::get_offset_of_ButtonPressedMethod_3(),
	IMButton_t3867185334::get_offset_of_ButtonUpMethod_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (ButtonDownMethodDelegate_t3308383149), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (ButtonPressedMethodDelegate_t2485678181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (ButtonUpMethodDelegate_t939453058), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (MMLayers_t3559366371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (MMLists_t1411150679), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (MMMaths_t519709266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (MMMovement_t3250796725), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (U3CMoveFromToU3Ec__Iterator0_t1354840102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[10] = 
{
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_U3CtU3E__0_0(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_movingObject_1(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_pointB_2(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_U3CdistanceU3E__0_3(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_approximationDistance_4(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_time_5(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_pointA_6(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_U24current_7(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_U24disposing_8(),
	U3CMoveFromToU3Ec__Iterator0_t1354840102::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (MMTime_t1029019214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (ObjectBounds_t107927819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[2] = 
{
	ObjectBounds_t107927819::get_offset_of_BoundsBasedOn_2(),
	ObjectBounds_t107927819::get_offset_of_U3CSizeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (WaysToDetermineBounds_t264359748)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[5] = 
{
	WaysToDetermineBounds_t264359748::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (MultipleObjectPoolerObject_t2067446362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[4] = 
{
	MultipleObjectPoolerObject_t2067446362::get_offset_of_GameObjectToPool_0(),
	MultipleObjectPoolerObject_t2067446362::get_offset_of_PoolSize_1(),
	MultipleObjectPoolerObject_t2067446362::get_offset_of_PoolCanExpand_2(),
	MultipleObjectPoolerObject_t2067446362::get_offset_of_Enabled_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (PoolingMethods_t920590136)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[5] = 
{
	PoolingMethods_t920590136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (MultipleObjectPooler_t1548776533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[8] = 
{
	MultipleObjectPooler_t1548776533::get_offset_of_Pool_6(),
	MultipleObjectPooler_t1548776533::get_offset_of_PoolingMethod_7(),
	MultipleObjectPooler_t1548776533::get_offset_of_CanPoolSameObjectTwice_8(),
	MultipleObjectPooler_t1548776533::get_offset_of__pooledGameObjects_9(),
	MultipleObjectPooler_t1548776533::get_offset_of__pooledGameObjectsOriginalOrder_10(),
	MultipleObjectPooler_t1548776533::get_offset_of__randomizedPool_11(),
	MultipleObjectPooler_t1548776533::get_offset_of__lastPooledObjectName_12(),
	MultipleObjectPooler_t1548776533::get_offset_of__currentIndex_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (ObjectPooler_t2790389498), -1, sizeof(ObjectPooler_t2790389498_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2030[4] = 
{
	ObjectPooler_t2790389498_StaticFields::get_offset_of_Instance_2(),
	ObjectPooler_t2790389498::get_offset_of_MutualizeWaitingPools_3(),
	ObjectPooler_t2790389498::get_offset_of_NestWaitingPool_4(),
	ObjectPooler_t2790389498::get_offset_of__waitingPool_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (PoolableObject_t3099519598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[2] = 
{
	PoolableObject_t3099519598::get_offset_of_OnSpawnComplete_4(),
	PoolableObject_t3099519598::get_offset_of_LifeTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (Events_t2152452284), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (SimpleObjectPooler_t3637768830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[4] = 
{
	SimpleObjectPooler_t3637768830::get_offset_of_GameObjectToPool_6(),
	SimpleObjectPooler_t3637768830::get_offset_of_PoolSize_7(),
	SimpleObjectPooler_t3637768830::get_offset_of_PoolCanExpand_8(),
	SimpleObjectPooler_t3637768830::get_offset_of__pooledGameObjects_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (AutoDestroyParticleSystem_t988498569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[4] = 
{
	AutoDestroyParticleSystem_t988498569::get_offset_of_DestroyParent_2(),
	AutoDestroyParticleSystem_t988498569::get_offset_of_DestroyDelay_3(),
	AutoDestroyParticleSystem_t988498569::get_offset_of__particleSystem_4(),
	AutoDestroyParticleSystem_t988498569::get_offset_of__startTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (ChangeFogColor_t270202886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[1] = 
{
	ChangeFogColor_t270202886::get_offset_of_FogColor_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (MMTrailRendererSortingLayer_t1109254816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (VisibleParticle_t1758110514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (RigidbodyInterface_t2126323197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[6] = 
{
	RigidbodyInterface_t2126323197::get_offset_of__mode_2(),
	RigidbodyInterface_t2126323197::get_offset_of__rigidbody2D_3(),
	RigidbodyInterface_t2126323197::get_offset_of__rigidbody_4(),
	RigidbodyInterface_t2126323197::get_offset_of__collider2D_5(),
	RigidbodyInterface_t2126323197::get_offset_of__collider_6(),
	RigidbodyInterface_t2126323197::get_offset_of__colliderBounds_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (SaveManager_t380343877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (GetSetAttribute_t1349027187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[2] = 
{
	GetSetAttribute_t1349027187::get_offset_of_name_0(),
	GetSetAttribute_t1349027187::get_offset_of_dirty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (MinAttribute_t4172004135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[1] = 
{
	MinAttribute_t4172004135::get_offset_of_min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (TrackballAttribute_t219960417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[1] = 
{
	TrackballAttribute_t219960417::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (TrackballGroupAttribute_t624107828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (AmbientOcclusionComponent_t4130625043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[3] = 
{
	0,
	0,
	AmbientOcclusionComponent_t4130625043::get_offset_of_m_MRT_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (Uniforms_t3797733410), -1, sizeof(Uniforms_t3797733410_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2051[9] = 
{
	Uniforms_t3797733410_StaticFields::get_offset_of__Intensity_0(),
	Uniforms_t3797733410_StaticFields::get_offset_of__Radius_1(),
	Uniforms_t3797733410_StaticFields::get_offset_of__Downsample_2(),
	Uniforms_t3797733410_StaticFields::get_offset_of__SampleCount_3(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture1_4(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture2_5(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture_6(),
	Uniforms_t3797733410_StaticFields::get_offset_of__MainTex_7(),
	Uniforms_t3797733410_StaticFields::get_offset_of__TempRT_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (OcclusionSource_t4221238007)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2052[4] = 
{
	OcclusionSource_t4221238007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (BloomComponent_t3791419130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[3] = 
{
	0,
	BloomComponent_t3791419130::get_offset_of_m_BlurBuffer1_3(),
	BloomComponent_t3791419130::get_offset_of_m_BlurBuffer2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (Uniforms_t4164805197), -1, sizeof(Uniforms_t4164805197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2054[10] = 
{
	Uniforms_t4164805197_StaticFields::get_offset_of__AutoExposure_0(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Threshold_1(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Curve_2(),
	Uniforms_t4164805197_StaticFields::get_offset_of__PrefilterOffs_3(),
	Uniforms_t4164805197_StaticFields::get_offset_of__SampleScale_4(),
	Uniforms_t4164805197_StaticFields::get_offset_of__BaseTex_5(),
	Uniforms_t4164805197_StaticFields::get_offset_of__BloomTex_6(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_Settings_7(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_DirtTex_8(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_DirtIntensity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (BuiltinDebugViewsComponent_t2123147871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[2] = 
{
	0,
	BuiltinDebugViewsComponent_t2123147871::get_offset_of_m_Arrows_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (Uniforms_t2158582951), -1, sizeof(Uniforms_t2158582951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2056[7] = 
{
	Uniforms_t2158582951_StaticFields::get_offset_of__DepthScale_0(),
	Uniforms_t2158582951_StaticFields::get_offset_of__TempRT_1(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Opacity_2(),
	Uniforms_t2158582951_StaticFields::get_offset_of__MainTex_3(),
	Uniforms_t2158582951_StaticFields::get_offset_of__TempRT2_4(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Amplitude_5(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Scale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (Pass_t2117482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2057[6] = 
{
	Pass_t2117482::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (ArrowArray_t303178545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[3] = 
{
	ArrowArray_t303178545::get_offset_of_U3CmeshU3Ek__BackingField_0(),
	ArrowArray_t303178545::get_offset_of_U3CcolumnCountU3Ek__BackingField_1(),
	ArrowArray_t303178545::get_offset_of_U3CrowCountU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (ChromaticAberrationComponent_t1647263118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[1] = 
{
	ChromaticAberrationComponent_t1647263118::get_offset_of_m_SpectrumLut_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (Uniforms_t424361460), -1, sizeof(Uniforms_t424361460_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2060[2] = 
{
	Uniforms_t424361460_StaticFields::get_offset_of__ChromaticAberration_Amount_0(),
	Uniforms_t424361460_StaticFields::get_offset_of__ChromaticAberration_Spectrum_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (ColorGradingComponent_t1715259467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[4] = 
{
	0,
	0,
	0,
	ColorGradingComponent_t1715259467::get_offset_of_m_GradingCurves_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (Uniforms_t3075607151), -1, sizeof(Uniforms_t3075607151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2062[20] = 
{
	Uniforms_t3075607151_StaticFields::get_offset_of__LutParams_0(),
	Uniforms_t3075607151_StaticFields::get_offset_of__NeutralTonemapperParams1_1(),
	Uniforms_t3075607151_StaticFields::get_offset_of__NeutralTonemapperParams2_2(),
	Uniforms_t3075607151_StaticFields::get_offset_of__HueShift_3(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Saturation_4(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Contrast_5(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Balance_6(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Lift_7(),
	Uniforms_t3075607151_StaticFields::get_offset_of__InvGamma_8(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Gain_9(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Slope_10(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Power_11(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Offset_12(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerRed_13(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerGreen_14(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerBlue_15(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Curves_16(),
	Uniforms_t3075607151_StaticFields::get_offset_of__LogLut_17(),
	Uniforms_t3075607151_StaticFields::get_offset_of__LogLut_Params_18(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ExposureEV_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (DepthOfFieldComponent_t554756766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[4] = 
{
	0,
	DepthOfFieldComponent_t554756766::get_offset_of_m_CoCHistory_3(),
	DepthOfFieldComponent_t554756766::get_offset_of_m_MRT_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (Uniforms_t3629868803), -1, sizeof(Uniforms_t3629868803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2064[10] = 
{
	Uniforms_t3629868803_StaticFields::get_offset_of__DepthOfFieldTex_0(),
	Uniforms_t3629868803_StaticFields::get_offset_of__Distance_1(),
	Uniforms_t3629868803_StaticFields::get_offset_of__LensCoeff_2(),
	Uniforms_t3629868803_StaticFields::get_offset_of__MaxCoC_3(),
	Uniforms_t3629868803_StaticFields::get_offset_of__RcpMaxCoC_4(),
	Uniforms_t3629868803_StaticFields::get_offset_of__RcpAspect_5(),
	Uniforms_t3629868803_StaticFields::get_offset_of__MainTex_6(),
	Uniforms_t3629868803_StaticFields::get_offset_of__HistoryCoC_7(),
	Uniforms_t3629868803_StaticFields::get_offset_of__HistoryWeight_8(),
	Uniforms_t3629868803_StaticFields::get_offset_of__DepthOfFieldParams_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (DitheringComponent_t277621267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[3] = 
{
	DitheringComponent_t277621267::get_offset_of_noiseTextures_2(),
	DitheringComponent_t277621267::get_offset_of_textureIndex_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (Uniforms_t3745258951), -1, sizeof(Uniforms_t3745258951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2066[2] = 
{
	Uniforms_t3745258951_StaticFields::get_offset_of__DitheringTex_0(),
	Uniforms_t3745258951_StaticFields::get_offset_of__DitheringCoords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (EyeAdaptationComponent_t3394805121), -1, sizeof(EyeAdaptationComponent_t3394805121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2067[11] = 
{
	EyeAdaptationComponent_t3394805121::get_offset_of_m_EyeCompute_2(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_HistogramBuffer_3(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_AutoExposurePool_4(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_AutoExposurePingPing_5(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_CurrentAutoExposure_6(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_DebugHistogram_7(),
	EyeAdaptationComponent_t3394805121_StaticFields::get_offset_of_s_EmptyHistogramBuffer_8(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_FirstFrame_9(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (Uniforms_t95810089), -1, sizeof(Uniforms_t95810089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2068[6] = 
{
	Uniforms_t95810089_StaticFields::get_offset_of__Params_0(),
	Uniforms_t95810089_StaticFields::get_offset_of__Speed_1(),
	Uniforms_t95810089_StaticFields::get_offset_of__ScaleOffsetRes_2(),
	Uniforms_t95810089_StaticFields::get_offset_of__ExposureCompensation_3(),
	Uniforms_t95810089_StaticFields::get_offset_of__AutoExposure_4(),
	Uniforms_t95810089_StaticFields::get_offset_of__DebugWidth_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (FogComponent_t3400726830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (Uniforms_t459708260), -1, sizeof(Uniforms_t459708260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2070[5] = 
{
	Uniforms_t459708260_StaticFields::get_offset_of__FogColor_0(),
	Uniforms_t459708260_StaticFields::get_offset_of__Density_1(),
	Uniforms_t459708260_StaticFields::get_offset_of__Start_2(),
	Uniforms_t459708260_StaticFields::get_offset_of__End_3(),
	Uniforms_t459708260_StaticFields::get_offset_of__TempRT_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (FxaaComponent_t1312385771), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (Uniforms_t1850622510), -1, sizeof(Uniforms_t1850622510_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2072[2] = 
{
	Uniforms_t1850622510_StaticFields::get_offset_of__QualitySettings_0(),
	Uniforms_t1850622510_StaticFields::get_offset_of__ConsoleSettings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (GrainComponent_t866324317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[1] = 
{
	GrainComponent_t866324317::get_offset_of_m_GrainLookupRT_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (Uniforms_t1442519687), -1, sizeof(Uniforms_t1442519687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2074[4] = 
{
	Uniforms_t1442519687_StaticFields::get_offset_of__Grain_Params1_0(),
	Uniforms_t1442519687_StaticFields::get_offset_of__Grain_Params2_1(),
	Uniforms_t1442519687_StaticFields::get_offset_of__GrainTex_2(),
	Uniforms_t1442519687_StaticFields::get_offset_of__Phase_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (MotionBlurComponent_t3686516877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[3] = 
{
	MotionBlurComponent_t3686516877::get_offset_of_m_ReconstructionFilter_2(),
	MotionBlurComponent_t3686516877::get_offset_of_m_FrameBlendingFilter_3(),
	MotionBlurComponent_t3686516877::get_offset_of_m_FirstFrame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (Uniforms_t589754008), -1, sizeof(Uniforms_t589754008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2076[26] = 
{
	Uniforms_t589754008_StaticFields::get_offset_of__VelocityScale_0(),
	Uniforms_t589754008_StaticFields::get_offset_of__MaxBlurRadius_1(),
	Uniforms_t589754008_StaticFields::get_offset_of__RcpMaxBlurRadius_2(),
	Uniforms_t589754008_StaticFields::get_offset_of__VelocityTex_3(),
	Uniforms_t589754008_StaticFields::get_offset_of__MainTex_4(),
	Uniforms_t589754008_StaticFields::get_offset_of__Tile2RT_5(),
	Uniforms_t589754008_StaticFields::get_offset_of__Tile4RT_6(),
	Uniforms_t589754008_StaticFields::get_offset_of__Tile8RT_7(),
	Uniforms_t589754008_StaticFields::get_offset_of__TileMaxOffs_8(),
	Uniforms_t589754008_StaticFields::get_offset_of__TileMaxLoop_9(),
	Uniforms_t589754008_StaticFields::get_offset_of__TileVRT_10(),
	Uniforms_t589754008_StaticFields::get_offset_of__NeighborMaxTex_11(),
	Uniforms_t589754008_StaticFields::get_offset_of__LoopCount_12(),
	Uniforms_t589754008_StaticFields::get_offset_of__TempRT_13(),
	Uniforms_t589754008_StaticFields::get_offset_of__History1LumaTex_14(),
	Uniforms_t589754008_StaticFields::get_offset_of__History2LumaTex_15(),
	Uniforms_t589754008_StaticFields::get_offset_of__History3LumaTex_16(),
	Uniforms_t589754008_StaticFields::get_offset_of__History4LumaTex_17(),
	Uniforms_t589754008_StaticFields::get_offset_of__History1ChromaTex_18(),
	Uniforms_t589754008_StaticFields::get_offset_of__History2ChromaTex_19(),
	Uniforms_t589754008_StaticFields::get_offset_of__History3ChromaTex_20(),
	Uniforms_t589754008_StaticFields::get_offset_of__History4ChromaTex_21(),
	Uniforms_t589754008_StaticFields::get_offset_of__History1Weight_22(),
	Uniforms_t589754008_StaticFields::get_offset_of__History2Weight_23(),
	Uniforms_t589754008_StaticFields::get_offset_of__History3Weight_24(),
	Uniforms_t589754008_StaticFields::get_offset_of__History4Weight_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (Pass_t2620402414)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2077[10] = 
{
	Pass_t2620402414::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (ReconstructionFilter_t705677647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[2] = 
{
	ReconstructionFilter_t705677647::get_offset_of_m_VectorRTFormat_0(),
	ReconstructionFilter_t705677647::get_offset_of_m_PackedRTFormat_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (FrameBlendingFilter_t2699796096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[4] = 
{
	FrameBlendingFilter_t2699796096::get_offset_of_m_UseCompression_0(),
	FrameBlendingFilter_t2699796096::get_offset_of_m_RawTextureFormat_1(),
	FrameBlendingFilter_t2699796096::get_offset_of_m_FrameList_2(),
	FrameBlendingFilter_t2699796096::get_offset_of_m_LastFrameCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (Frame_t295908221)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[4] = 
{
	Frame_t295908221::get_offset_of_lumaTexture_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t295908221::get_offset_of_chromaTexture_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t295908221::get_offset_of_m_Time_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t295908221::get_offset_of_m_MRT_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (ScreenSpaceReflectionComponent_t856094247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[5] = 
{
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_k_HighlightSuppression_2(),
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_k_TraceBehindObjects_3(),
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_k_TreatBackfaceHitAsMiss_4(),
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_k_BilateralUpsample_5(),
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_m_ReflectionTextures_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (Uniforms_t2970573890), -1, sizeof(Uniforms_t2970573890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2082[35] = 
{
	Uniforms_t2970573890_StaticFields::get_offset_of__RayStepSize_0(),
	Uniforms_t2970573890_StaticFields::get_offset_of__AdditiveReflection_1(),
	Uniforms_t2970573890_StaticFields::get_offset_of__BilateralUpsampling_2(),
	Uniforms_t2970573890_StaticFields::get_offset_of__TreatBackfaceHitAsMiss_3(),
	Uniforms_t2970573890_StaticFields::get_offset_of__AllowBackwardsRays_4(),
	Uniforms_t2970573890_StaticFields::get_offset_of__TraceBehindObjects_5(),
	Uniforms_t2970573890_StaticFields::get_offset_of__MaxSteps_6(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FullResolutionFiltering_7(),
	Uniforms_t2970573890_StaticFields::get_offset_of__HalfResolution_8(),
	Uniforms_t2970573890_StaticFields::get_offset_of__HighlightSuppression_9(),
	Uniforms_t2970573890_StaticFields::get_offset_of__PixelsPerMeterAtOneMeter_10(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ScreenEdgeFading_11(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ReflectionBlur_12(),
	Uniforms_t2970573890_StaticFields::get_offset_of__MaxRayTraceDistance_13(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FadeDistance_14(),
	Uniforms_t2970573890_StaticFields::get_offset_of__LayerThickness_15(),
	Uniforms_t2970573890_StaticFields::get_offset_of__SSRMultiplier_16(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FresnelFade_17(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FresnelFadePower_18(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ReflectionBufferSize_19(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ScreenSize_20(),
	Uniforms_t2970573890_StaticFields::get_offset_of__InvScreenSize_21(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ProjInfo_22(),
	Uniforms_t2970573890_StaticFields::get_offset_of__CameraClipInfo_23(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ProjectToPixelMatrix_24(),
	Uniforms_t2970573890_StaticFields::get_offset_of__WorldToCameraMatrix_25(),
	Uniforms_t2970573890_StaticFields::get_offset_of__CameraToWorldMatrix_26(),
	Uniforms_t2970573890_StaticFields::get_offset_of__Axis_27(),
	Uniforms_t2970573890_StaticFields::get_offset_of__CurrentMipLevel_28(),
	Uniforms_t2970573890_StaticFields::get_offset_of__NormalAndRoughnessTexture_29(),
	Uniforms_t2970573890_StaticFields::get_offset_of__HitPointTexture_30(),
	Uniforms_t2970573890_StaticFields::get_offset_of__BlurTexture_31(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FilteredReflections_32(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FinalReflectionTexture_33(),
	Uniforms_t2970573890_StaticFields::get_offset_of__TempTexture_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (PassIndex_t1642913883)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[10] = 
{
	PassIndex_t1642913883::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (TaaComponent_t3791749658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[6] = 
{
	0,
	0,
	TaaComponent_t3791749658::get_offset_of_m_MRT_4(),
	TaaComponent_t3791749658::get_offset_of_m_SampleIndex_5(),
	TaaComponent_t3791749658::get_offset_of_m_ResetHistory_6(),
	TaaComponent_t3791749658::get_offset_of_m_HistoryTexture_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (Uniforms_t3024963833), -1, sizeof(Uniforms_t3024963833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2085[5] = 
{
	Uniforms_t3024963833_StaticFields::get_offset_of__Jitter_0(),
	Uniforms_t3024963833_StaticFields::get_offset_of__SharpenParameters_1(),
	Uniforms_t3024963833_StaticFields::get_offset_of__FinalBlendParameters_2(),
	Uniforms_t3024963833_StaticFields::get_offset_of__HistoryTex_3(),
	Uniforms_t3024963833_StaticFields::get_offset_of__MainTex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (UserLutComponent_t2843161776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (Uniforms_t1046717683), -1, sizeof(Uniforms_t1046717683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2087[2] = 
{
	Uniforms_t1046717683_StaticFields::get_offset_of__UserLut_0(),
	Uniforms_t1046717683_StaticFields::get_offset_of__UserLut_Params_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (VignetteComponent_t3243642943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (Uniforms_t2205824134), -1, sizeof(Uniforms_t2205824134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2089[5] = 
{
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Color_0(),
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Center_1(),
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Settings_2(),
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Mask_3(),
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Opacity_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (AmbientOcclusionModel_t389471066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[1] = 
{
	AmbientOcclusionModel_t389471066::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (SampleCount_t1158000259)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2091[5] = 
{
	SampleCount_t1158000259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (Settings_t3016786575)+ sizeof (RuntimeObject), sizeof(Settings_t3016786575_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[7] = 
{
	Settings_t3016786575::get_offset_of_intensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_radius_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_sampleCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_downsampling_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_forceForwardCompatibility_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_ambientOnly_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_highPrecision_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (AntialiasingModel_t1521139388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[1] = 
{
	AntialiasingModel_t1521139388::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (Method_t287042102)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2094[3] = 
{
	Method_t287042102::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (FxaaPreset_t2149486832)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[6] = 
{
	FxaaPreset_t2149486832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (FxaaQualitySettings_t1558211909)+ sizeof (RuntimeObject), sizeof(FxaaQualitySettings_t1558211909 ), sizeof(FxaaQualitySettings_t1558211909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2096[4] = 
{
	FxaaQualitySettings_t1558211909::get_offset_of_subpixelAliasingRemovalAmount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaQualitySettings_t1558211909::get_offset_of_edgeDetectionThreshold_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaQualitySettings_t1558211909::get_offset_of_minimumRequiredLuminance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaQualitySettings_t1558211909_StaticFields::get_offset_of_presets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (FxaaConsoleSettings_t950185286)+ sizeof (RuntimeObject), sizeof(FxaaConsoleSettings_t950185286 ), sizeof(FxaaConsoleSettings_t950185286_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2097[5] = 
{
	FxaaConsoleSettings_t950185286::get_offset_of_subpixelSpreadAmount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaConsoleSettings_t950185286::get_offset_of_edgeSharpnessAmount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaConsoleSettings_t950185286::get_offset_of_edgeDetectionThreshold_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaConsoleSettings_t950185286::get_offset_of_minimumRequiredLuminance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaConsoleSettings_t950185286_StaticFields::get_offset_of_presets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (FxaaSettings_t1280675075)+ sizeof (RuntimeObject), sizeof(FxaaSettings_t1280675075 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[1] = 
{
	FxaaSettings_t1280675075::get_offset_of_preset_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (TaaSettings_t2709374970)+ sizeof (RuntimeObject), sizeof(TaaSettings_t2709374970 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[4] = 
{
	TaaSettings_t2709374970::get_offset_of_jitterSpread_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaaSettings_t2709374970::get_offset_of_sharpen_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaaSettings_t2709374970::get_offset_of_stationaryBlending_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaaSettings_t2709374970::get_offset_of_motionBlending_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
